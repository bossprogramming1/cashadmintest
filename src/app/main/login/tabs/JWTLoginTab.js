import { yupResolver } from '@hookform/resolvers/yup';
import _ from '@lodash';
import Button from '@material-ui/core/Button';
import Icon from '@material-ui/core/Icon';
import IconButton from '@material-ui/core/IconButton';
import InputAdornment from '@material-ui/core/InputAdornment';
import TextField from '@material-ui/core/TextField';
import { submitLogin } from 'app/auth/store/loginSlice';
import { useEffect, useState } from 'react';
import { Controller, useForm } from 'react-hook-form';
import { useDispatch, useSelector } from 'react-redux';

import * as yup from 'yup';
import { Box } from '@material-ui/core';
import { Autocomplete } from '@mui/material';
import countryCodes from 'app/@data/countrycodes';
import removeCountryCode from 'app/@helpers/removeCountryCode';
import { getUsers } from 'app/store/dataSlice';

/**
 * Form Validation Schema
 */
const schema = yup.object().shape({
	password: yup
		.string()
		.required('Please enter your password.')
		.min(4, 'Password is too short - should be 4 chars minimum.')
});

const defaultValues = {
	username: '',
	password: ''
};

function JWTLoginTab(props) {
	const dispatch = useDispatch();
	const login = useSelector(({ auth }) => auth.login);
	const { control, setValue, formState, handleSubmit, reset, watch, trigger, setError, getValues } = useForm({
		mode: 'onChange',
		defaultValues,
		resolver: yupResolver(schema)
	});

	const { isValid, dirtyFields, errors } = formState;
	const [showPassword, setShowPassword] = useState(false);
	const updateEmployeeData = useSelector(state => state.data.users);
	useEffect(() => {
		dispatch(getUsers());
	}, []);

	useEffect(() => {
		setValue('username', 'admin@cashconnectbd.com', { shouldDirty: true, shouldValidate: true });
		setValue('password', 'bluebayit7811', { shouldDirty: true, shouldValidate: true });
	}, [reset, setValue, trigger]);
	const getCountryCode1 = watch('country_code1');
	const getCountryCode2 = watch('country_code2');

	function onSubmit(model) {
		const data = getValues();

		dispatch(submitLogin(data, { ...model, router: props.history })).then(() => {
			window.location.reload(true);
		});
	}

	return (
		<div className="w-full">
			<form className="flex flex-col justify-center w-full" onSubmit={handleSubmit(onSubmit)}>
				<Controller
					name="username"
					control={control}
					render={({ field }) => (
						<TextField
							{...field}
							className="mt-8 mb-16"
							error={!!errors.username}
							helperText={errors?.username?.message}
							label="User Name"
							placeholder="Email or Phone Number"
							id="username"
							variant="outlined"
							fullWidth
							InputLabelProps={field.value && { shrink: true }}
						/>
					)}
				/>

				<Controller
					name="password"
					control={control}
					render={({ field }) => (
						<TextField
							{...field}
							className="mb-16"
							label="Password"
							type="password"
							error={!!errors.password}
							helperText={errors?.password?.message}
							variant="outlined"
							InputProps={{
								className: 'pr-2',
								type: showPassword ? 'text' : 'password',
								endAdornment: (
									<InputAdornment position="end">
										<IconButton onClick={() => setShowPassword(!showPassword)}>
											<Icon className="text-20" color="action">
												{showPassword ? 'visibility' : 'visibility_off'}
											</Icon>
										</IconButton>
									</InputAdornment>
								)
							}}
							required
						/>
					)}
				/>

				<Button
					type="submit"
					variant="contained"
					color="primary"
					className="w-full mx-auto mt-16"
					aria-label="LOG IN"
					disabled={_.isEmpty(dirtyFields) || !isValid}
					value="legacy"
				>
					Login
				</Button>
			</form>

			{/* <table className="w-full mt-32 text-center">
				<thead className="mb-4">
					<tr>
						<th>
							<Typography className="font-semibold text-11" color="textSecondary">
								Role
							</Typography>
						</th>
						<th>
							<Typography className="font-semibold text-11" color="textSecondary">
								Email
							</Typography>
						</th>
						<th>
							<Typography className="font-semibold text-11" color="textSecondary">
								Password
							</Typography>
						</th>
					</tr>
				</thead>
				<tbody>
					<tr>
						<td>
							<Typography className="font-medium text-11" color="textSecondary">
								Admin
							</Typography>
						</td>
						<td>
							<Typography className="text-11">admin@fusetheme.com</Typography>
						</td>
						<td>
							<Typography className="text-11">admin</Typography>
						</td>
					</tr>
					<tr>
						<td>
							<Typography className="font-medium text-11" color="textSecondary">
								Staff
							</Typography>
						</td>
						<td>
							<Typography className="text-11">staff@fusetheme.com</Typography>
						</td>
						<td>
							<Typography className="text-11">staff</Typography>
						</td>
					</tr>
				</tbody>
			</table> */}
		</div>
	);
}

export default JWTLoginTab;
