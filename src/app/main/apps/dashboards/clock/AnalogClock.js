import React, { useEffect, useState } from 'react';
import './AnalogClock.css';

function AnalogClock() {
	const [time, setTime] = useState(new Date());

	useEffect(() => {
		const interval = setInterval(() => {
			setTime(new Date());
		}, 1000);

		return () => clearInterval(interval);
	}, []);

	const getRotation = (unit, value) => {
		const totalUnits = unit === 'hour' ? 12 : 60;
		return `rotate(${(value / totalUnits) * 360 - 90}deg)`;
	};

	return (
		<div className="clock-container">
			<div className="clock">
				{Array.from({ length: 12 }).map((_, index) => (
					<div key={index + 1} className="number" style={{ transform: getRotation('hour', index + 1) }}>
						{index + 1}
					</div>
				))}
				<div className="hour-hand" style={{ transform: getRotation('hour', time.getHours()) }} />
				<div className="minute-hand" style={{ transform: getRotation('minute', time.getMinutes()) }} />
				<div className="second-hand" style={{ transform: getRotation('second', time.getSeconds()) }} />
			</div>
		</div>
	);
}

export default AnalogClock;
