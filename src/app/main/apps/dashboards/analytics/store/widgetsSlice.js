/* eslint-disable no-return-await */
import { createSlice, createAsyncThunk, createEntityAdapter, current } from '@reduxjs/toolkit';
import axios from 'axios';

export const getWidgets = createAsyncThunk('analyticsDashboardApp/widgets/getWidgets', async () => {
	const response = await axios.get('/api/analytics-dashboard-app/widgets');
	const data = await response.data;

	return data;
});
export const getWidgets1 = createAsyncThunk('analyticsDashboardApp/widgets/getWidgets', async () => {
	const response = await axios.get('/api/analytics-dashboard-app/widgets');
	const data = await response.data;

	return await data;
});

const widgetsAdapter = createEntityAdapter({});

export const { selectEntities: selectWidgetsEntities, selectById: selectWidgetById } = widgetsAdapter.getSelectors(
	state => state.analyticsDashboardApp.widgets
);

const widgetsSlice = createSlice({
	name: 'analyticsDashboardApp/widgets',
	initialState: widgetsAdapter.getInitialState({}),
	reducers: {},
	extraReducers: {
		[getWidgets1.fulfilled]: (state, action) => {
			state.widgets.entities.widget1 = {};
		}
	}
});

export default widgetsSlice.reducer;
