import { createEntityAdapter, createSlice, createAsyncThunk, current } from '@reduxjs/toolkit';
import {
	GET_ALL_CATEGORIZED_ORDER_ITEM_COUNT,
	GET_ALL_EMPLOYEE_WITHOUT_PAGINATION,
	GET_BEST_SELLER_AMOUNT,
	GET_BEST_SELLER_QUANTITY,
	GET_LATEST_CUSTOMERS,
	GET_ORDERS_CHART,
	GET_ORDERS_COUNT_PAID,
	GET_ORDERS_COUNT_UNPAID,
	GET_REGISTERED_CUSTOMERS,
	GET_TOTAL_INCOMPLETE_ORDERS,
	GET_TOTAL_LATEST_ORDERS,
	GET_TOTAL_LOW_STOCKS,
	GET_TOTAL_ORDERS
} from 'app/constant/constants';
import axios from 'axios';

export const getWidgets = createAsyncThunk('projectDashboardApp/widgets/getWidgets', async () => {
	const response = await fetch('/api/project-dashboard-app/widgets');
	const data = await response.json();
	return data;
});
export const getWidget1 = createAsyncThunk('projectDashboardApp/widgets/getWidget1', async () => {
	const response = await fetch(`${GET_ORDERS_COUNT_PAID}`, {
		headers: {
			'Content-type': 'application/json',
			Authorization: localStorage.getItem('jwt_access_token')
		}
	});

	const data = await response.json();

	return data.total_paid_orders;
});
export const getWidget2 = createAsyncThunk('projectDashboardApp/widgets/getWidget2', async () => {
	const response = await fetch(`${GET_ORDERS_COUNT_UNPAID}`, {
		headers: {
			'Content-type': 'application/json',
			Authorization: localStorage.getItem('jwt_access_token')
		}
	});

	const data = await response.json();

	return data.total_unpaid_orders;
});
export const getWidget3 = createAsyncThunk('projectDashboardApp/widgets/getWidget3', async () => {
	const response = await fetch(`${GET_REGISTERED_CUSTOMERS}`, {
		headers: {
			'Content-type': 'application/json',
			Authorization: localStorage.getItem('jwt_access_token')
		}
	});

	const data = await response.json();

	return data.total_customers;
});
export const getWidget4 = createAsyncThunk('projectDashboardApp/widgets/getWidget4', async () => {
	const response = await fetch(`${GET_TOTAL_LOW_STOCKS}`, {
		headers: {
			'Content-type': 'application/json',
			Authorization: localStorage.getItem('jwt_access_token')
		}
	});

	const data = await response.json();

	return data.total_low_stock_products;
});
export const getWidget14 = createAsyncThunk('projectDashboardApp/widgets/getWidget14', async () => {
	const response = await fetch(`${GET_LATEST_CUSTOMERS}`, {
		headers: {
			'Content-type': 'application/json',
			Authorization: localStorage.getItem('jwt_access_token')
		}
	});

	const data = await response.json();

	return data.total_elements;
});
export const getWidget5 = createAsyncThunk('projectDashboardApp/widgets/getWidget5', async parameter => {
	const response = await axios.get(
		GET_ORDERS_CHART,

		{
			headers: {
				'Content-type': 'application/json',
				Authorization: localStorage.getItem('jwt_access_token')
			}
		}
	);

	return response.data;
});
export const getWidget6 = createAsyncThunk('projectDashboardApp/widgets/getWidget6', async parameter => {
	const { page, size } = parameter;

	const response = await axios.get(
		GET_TOTAL_LATEST_ORDERS,
		{ params: { page, size } },
		{
			headers: {
				'Content-type': 'application/json',
				Authorization: localStorage.getItem('jwt_access_token')
			}
		}
	);

	return response.data;
});

export const getWidget7 = createAsyncThunk('projectDashboardApp/widgets/getWidget7', async parameter => {
	const { page, size } = parameter;

	const response = await axios.get(
		GET_TOTAL_ORDERS,
		{ params: { page, size } },
		{
			headers: {
				'Content-type': 'application/json',
				Authorization: localStorage.getItem('jwt_access_token')
			}
		}
	);

	return response.data;
});
export const getWidget8 = createAsyncThunk('projectDashboardApp/widgets/getWidget8', async parameter => {
	const { page, size } = parameter;

	const response = await axios.get(
		GET_BEST_SELLER_AMOUNT,
		{ params: { page, size } },
		{
			headers: {
				'Content-type': 'application/json',
				Authorization: localStorage.getItem('jwt_access_token')
			}
		}
	);

	return response.data;
});
export const getWidget9 = createAsyncThunk('projectDashboardApp/widgets/getWidget9', async parameter => {
	const { page, size } = parameter;

	const response = await axios.get(
		GET_BEST_SELLER_QUANTITY,
		{ params: { page, size } },
		{
			headers: {
				'Content-type': 'application/json',
				Authorization: localStorage.getItem('jwt_access_token')
			}
		}
	);

	return response.data;
});

export const getWidget11 = createAsyncThunk('projectDashboardApp/widgets/getWidget11', async () => {
	const response = await fetch(GET_ALL_EMPLOYEE_WITHOUT_PAGINATION, {
		headers: {
			'Content-type': 'application/json',
			Authorization: localStorage.getItem('jwt_access_token')
		}
	});

	const data = await response.json();

	return data.employees;
});
export const getWidget12 = createAsyncThunk('projectDashboardApp/widgets/getWidget12', async parameter => {
	const { page, size } = parameter;

	const response = await axios.get(
		GET_TOTAL_INCOMPLETE_ORDERS,
		{ params: { page, size } },
		{
			headers: {
				'Content-type': 'application/json',
				Authorization: localStorage.getItem('jwt_access_token')
			}
		}
	);

	return response.data;
});
export const getWidget13 = createAsyncThunk('projectDashboardApp/widgets/getWidget13', async parameter => {
	const { page, size } = parameter;

	const response = await axios.get(
		GET_ALL_CATEGORIZED_ORDER_ITEM_COUNT,
		{ params: { page, size } },
		{
			headers: {
				'Content-type': 'application/json',
				Authorization: localStorage.getItem('jwt_access_token')
			}
		}
	);

	return response.data;
});
const widgetsAdapter = createEntityAdapter({});

export const { selectEntities: selectWidgets, selectById: selectWidgetById } = widgetsAdapter.getSelectors(
	state => state.projectDashboardApp.widgets
);
export const { selectAll: selectOrders, selectById: selectOrderById } = widgetsAdapter.getSelectors(
	state => state.projectDashboardApp.widgets
);

const widgetsSlice = createSlice({
	name: 'projectDashboardApp/widgets',
	initialState: widgetsAdapter.getInitialState(),
	reducers: {},
	extraReducers: {
		[getWidgets.fulfilled]: (state, action) => widgetsAdapter.setAll(state, action.payload),
		[getWidget1.fulfilled]: (state, action) => {
			state.entities.widget1 = {
				id: 'widget1',
				title: 'Paid Orders',

				data: {
					name: 'Paid Orders',
					count: action.payload
				},
				detail: 'You can show some detailed information about this widget in here.'
			};
		},
		[getWidget2.fulfilled]: (state, action) => {
			state.entities.widget2 = {
				id: 'widget2',
				title: 'Unpaid Orders',
				data: {
					name: 'Unpaid Orders',
					count: action.payload,
					extra: {
						name: "Yesterday's overdue",
						count: 2
					}
				},
				detail: 'You can show some detailed information about this widget in here.'
			};
		},
		[getWidget3.fulfilled]: (state, action) => {
			state.entities.widget3 = {
				id: 'widget3',
				title: 'Registered',
				data: {
					name: 'Customers',
					count: action.payload,
					extra: {
						name: 'Closed today',
						count: 0
					}
				},
				detail: 'You can show some detailed information about this widget in here.'
			};
		},
		[getWidget4.fulfilled]: (state, action) => {
			state.entities.widget4 = {
				id: 'widget4',
				title: 'Low stocks',
				data: {
					name: 'Low Stocks',
					count: action.payload,
					extra: {
						name: 'Implemented',
						count: 8
					}
				},
				detail: 'You can show some detailed information about this widget in here.'
			};
		},
		[getWidget14.fulfilled]: (state, action) => {
			state.entities.widget14 = {
				id: 'widget14',
				title: 'New Customer',
				data: {
					name: 'New Customer',
					count: action.payload,
					extra: {
						name: 'Implemented',
						count: 8
					}
				},
				detail: 'You can show some detailed information about this widget in here.'
			};
		},

		[getWidget5.fulfilled]: (state, action) => {
			state.entities.widget5 = action.payload || [];
		},
		[getWidget6.fulfilled]: (state, action) => {
			state.entities.widget6 = action.payload || [];
		},
		[getWidget7.fulfilled]: (state, action) => {
			state.entities.widget7 = action.payload || [];
		},
		[getWidget8.fulfilled]: (state, action) => {
			state.entities.widget8 = action.payload.vendors || [];
		},
		[getWidget9.fulfilled]: (state, action) => {
			state.entities.widget9 = action.payload.vendors || [];
		},
		[getWidget11.fulfilled]: (state, action) => {
			state.entities.widget11 = action.payload || [];
		},
		[getWidget12.fulfilled]: (state, action) => {
			state.entities.widget12 = action.payload || [];
		},

		[getWidget13.fulfilled]: (state, action) => {
			state.entities.widget13 = action.payload || [];
		}
	}
});

export default widgetsSlice.reducer;
