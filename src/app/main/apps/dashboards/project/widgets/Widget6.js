/* eslint-disable import/no-extraneous-dependencies */
import Paper from '@material-ui/core/Paper';
import Select from '@material-ui/core/Select';
import { createTheme, ThemeProvider } from '@mui/material/styles';
import Typography from '@material-ui/core/Typography';
import _ from '@lodash';
import { useEffect, memo, useState } from 'react';
import ShoppingCartIcon from '@mui/icons-material/ShoppingCart';
import { format } from 'date-fns';
import { v4 as uuid } from 'uuid';
import {
	Box,
	Button,
	Card,
	CardHeader,
	Table,
	TableBody,
	TableCell,
	TableHead,
	TableRow,
	TableSortLabel,
	Tooltip
} from '@material-ui/core';
import ArrowRightIcon from '@mui/icons-material/ArrowRight';
import { makeStyles } from '@material-ui/core/styles';
import { useDispatch } from 'react-redux';
import history from '@history';
import Pagination from '@material-ui/lab/Pagination';
import { getWidget6 } from '../store/widgetsSlice';
import { StatusColor } from './StatusColor';

const useStyles = makeStyles(theme => ({
	tablecell: {
		fontSize: '50px'
	},
	root: {
		display: 'flex',
		flexDirection: 'column',
		justifyContent: 'space-between',
		height: '513px' // Set a fixed height for the container
	},
	tableContainer: {
		flex: 1, // Let the table container take the remaining vertical space
		overflowY: 'auto' // Enable vertical scrolling when the table content overflows
	},
	paginationContainer: {
		marginTop: theme.spacing(2),
		paddingLeft: '20px',
		display: 'flex',
		justifyContent: 'space-between'
	}
}));

function Widget6(props) {
	const dispatch = useDispatch();

	const [page, setPage] = useState(1);
	const [totalPages, setTotalPages] = useState(0);
	const classes = useStyles(props);

	useEffect(() => {
		dispatch(getWidget6({ page, size: 6 })).then(action => {
			setPage(action.payload?.page);
			setTotalPages(action.payload?.total_pages);
		});
	}, []);

	const handlePagination = (page, hanglePage) => {
		dispatch(getWidget6({ page: hanglePage, size: 6 })).then(action => {
			setPage(action.payload?.page);
			setTotalPages(action.payload?.total_pages);
		});
	};

	return (
		<Paper className="w-full rounded-40 shadow" style={{ height: '546px' }}>
			{/* sx={{ minWidth: 800 }} */}
			<Box className={classes.root}>
				<div className="flex items-center justify-between p-20">
					<Typography className="text-16 font-medium">
						{' '}
						<ShoppingCartIcon />
						Latest Oders
					</Typography>
				</div>

				<Box className={classes.tableContainer}>
					<Table>
						<TableHead>
							<TableRow style={{ fontSize: '16px' }}>
								<TableCell className="whitespace-nowrap text-16 font-medium">Order Ref</TableCell>
								<TableCell className="whitespace-nowrap text-16 font-medium">Customer</TableCell>
								<TableCell sortDirection="desc" className="text-16 font-medium">
									<Tooltip enterDelay={300} title="Date">
										<TableSortLabel active direction="desc">
											Date
										</TableSortLabel>
									</Tooltip>
								</TableCell>
								<TableCell className="whitespace-nowrap text-16 font-medium">Status</TableCell>
							</TableRow>
						</TableHead>
						<TableBody>
							{props?.widget?.orders?.map(order => {
								return (
									<TableRow hover key={order?.id}>
										<TableCell className="whitespace-nowrap">{order?.order_no}</TableCell>
										<TableCell className="whitespace-nowrap">{order.user?.first_name}</TableCell>
										<TableCell className="whitespace-nowrap">
											{order?.order_date && format(new Date(order?.order_date), 'dd-MM-yyyy')}
										</TableCell>
										<TableCell className="whitespace-nowrap">
											<StatusColor
												color={
													(order.order_status === 'processing' && 'info') ||
													(order.order_status === 'packaging' && 'error') ||
													(order.order_status === 'delivered' && 'success') ||
													'warning'
												}
											>
												{order?.order_status}
											</StatusColor>
										</TableCell>
									</TableRow>
								);
							})}
						</TableBody>
					</Table>
				</Box>

				<div className={classes.paginationContainer}>
					<Pagination
						size="small"
						count={totalPages}
						page={page}
						defaultPage={1}
						color="primary"
						// showFirstButton
						// showLastButton
						variant="outlined"
						shape="rounded"
						siblingCount={0}
						onChange={handlePagination}
						style={{ alignSelf: 'flex-end' }} // Align the pagination to the bottom
					/>
					<Button
						color="primary"
						endIcon={<ArrowRightIcon fontSize="small" />}
						size="medium"
						variant="text"
						onClick={() => {
							history.push(`/apps/order-managements/orders`);
						}}
					>
						View all
					</Button>
				</div>
			</Box>
		</Paper>
	);
}

export default memo(Widget6);
