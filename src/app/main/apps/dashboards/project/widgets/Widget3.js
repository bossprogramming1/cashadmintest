import history from '@history';
import Icon from '@material-ui/core/Icon';
import IconButton from '@material-ui/core/IconButton';
import Paper from '@material-ui/core/Paper';
import Typography from '@material-ui/core/Typography';
import { memo, useEffect } from 'react';
import { useDispatch } from 'react-redux';
import ArrowRightIcon from '@mui/icons-material/ArrowRight';
import { Button } from '@mui/material';
import { getWidget3 } from '../store/widgetsSlice';

function Widget3(props) {
	const dispatch = useDispatch();

	useEffect(() => {
		dispatch(getWidget3());
	}, []);
	return (
		<Paper className="w-full rounded-20 shadow flex flex-col justify-between">
			<Typography className="text-16  text-center p-16 font-medium" color="textSecondary">
				{props.widget?.title}
			</Typography>
			{/* <IconButton aria-label="more">
					<Icon>more_vert</Icon>
				</IconButton> */}
			<div className="text-center py-12">
				<Typography className="text-72 font-semibold leading-none text-green tracking-tighter">
					{props.widget?.data?.count}
				</Typography>
				<Typography className="text-18 font-normal text-green-800">{props.widget?.data?.name}</Typography>
			</div>
			<Button
				color="info"
				endIcon={<ArrowRightIcon fontSize="small" />}
				size="medium"
				variant="text"
				onClick={() => {
					history.push(`/apps/customer-management/customers`);
				}}
			>
				View all
			</Button>
		</Paper>
	);
}

export default memo(Widget3);
