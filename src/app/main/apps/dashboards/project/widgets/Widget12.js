/* eslint-disable import/order */
/* eslint-disable import/no-extraneous-dependencies */
import Paper from '@material-ui/core/Paper';
import Select from '@material-ui/core/Select';
import { createTheme, ThemeProvider } from '@mui/material/styles';
import Typography from '@material-ui/core/Typography';
import _ from '@lodash';
import { useEffect, memo, useState } from 'react';
import ReactApexChart from 'react-apexcharts';
import { format } from 'date-fns';
import { v4 as uuid } from 'uuid';
import {
	Box,
	Button,
	Card,
	CardHeader,
	Table,
	TableBody,
	TableCell,
	TableHead,
	TableRow,
	TableSortLabel,
	Tooltip
} from '@material-ui/core';
import ArrowRightIcon from '@mui/icons-material/ArrowRight';
import { makeStyles } from '@material-ui/core/styles';
import { useDispatch } from 'react-redux';
import { getWidget12 } from '../store/widgetsSlice';
import history from '@history';
import { StatusColor } from './StatusColor';
import ShoppingCartIcon from '@mui/icons-material/ShoppingCart';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import {} from '@fortawesome/free-solid-svg-icons';
import Pagination from '@material-ui/lab/Pagination';

function Widget12(props) {
	const dispatch = useDispatch();
	const [page, setPage] = useState(1);
	const [totalPages, setTotalPages] = useState(0);

	useEffect(() => {
		dispatch(getWidget12({ page, size: 4 })).then(action => {
			setPage(action.payload?.page);
			setTotalPages(action.payload?.total_pages);
		});
	}, []);

	const handlePagination = (page, hanglePage) => {
		dispatch(getWidget12({ page: hanglePage, size: 4 })).then(action => {
			setPage(action.payload?.page);
			setTotalPages(action.payload?.total_pages);
		});
	};

	return (
		<Paper className="w-full rounded-40 shadow">
			<div className="flex items-center justify-between p-20 h-64">
				<Typography className="text-16 font-medium whitespace-nowrap">
					{' '}
					<ShoppingCartIcon /> Incomplete Total Order
				</Typography>
			</div>
			<Box>
				<Table>
					<TableHead>
						<TableRow style={{ fontSize: '16px' }}>
							<TableCell className="whitespace-nowrap text-16 font-medium">Order Status</TableCell>
							<TableCell className="whitespace-nowrap text-16 font-medium">Count</TableCell>
							<TableCell className="whitespace-nowrap text-16 font-medium">Amount</TableCell>
						</TableRow>
					</TableHead>

					<TableBody>
						{props?.widget?.map(order => {
							return (
								<TableRow hover key={order?.status}>
									<TableCell>
										<StatusColor
											color={
												(order?.status === 'unpaid' && 'secondary') ||
												(order?.status === 'processing' && 'info') ||
												(order?.status === 'on the way' && 'warning') ||
												'error'
											}
										>
											{order?.status}
										</StatusColor>
									</TableCell>

									<TableCell className="whitespace-nowrap">{order?.count}</TableCell>
									<TableCell className="whitespace-nowrap">
										<span>&#2547; {order?.amount}</span>
									</TableCell>
								</TableRow>
							);
						})}
						<TableRow style={{ visibility: 'hidden' }}>
							<TableCell style={{ border: 'none' }} className="whitespace-nowrap">
								fsdfsdf
							</TableCell>
						</TableRow>
						<TableRow style={{ visibility: 'hidden' }}>
							<TableCell style={{ border: 'none' }} className="whitespace-nowrap">
								fsdfsdf
							</TableCell>
						</TableRow>
					</TableBody>
				</Table>
			</Box>
			{/* <Box
				sx={{
					display: 'flex',
					justifyContent: 'flex-end',
					p: 2
				}}
			>
				<Pagination
					style={{ visibility: 'hidden' }}
					count={totalPages}
					page={page}
					defaultPage={1}
					color="primary"
					showFirstButton
					showLastButton
					variant="outlined"
					shape="rounded"
					onChange={handlePagination}
				/>
				<Button
					style={{ marginLeft: '-123px' }}
					color="primary"
					endIcon={<ArrowRightIcon fontSize="small" />}
					size="medium"
					variant="text"
					onClick={() => {
						history.push(`/apps/order-managements/orders`);
					}}
				>
					View all
				</Button>
			</Box> */}
		</Paper>
	);
}

export default memo(Widget12);
