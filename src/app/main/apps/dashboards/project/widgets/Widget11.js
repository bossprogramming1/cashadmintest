import Avatar from '@material-ui/core/Avatar';
import Paper from '@material-ui/core/Paper';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Typography from '@material-ui/core/Typography';
import { memo, useEffect } from 'react';
import { useDispatch } from 'react-redux';
import { Box } from '@mui/material';
import { BASE_URL } from 'app/constant/constants';
import { getWidget11 } from '../store/widgetsSlice';

function Widget11(props) {
	const dispatch = useDispatch();

	useEffect(() => {
		dispatch(getWidget11());
	}, []);

	return (
		<Paper className="w-full rounded-40 shadow">
			{/* sx={{ minWidth: 800 }} */}
			<div className="flex items-center justify-between p-20 h-64">
				<Typography className="text-16 font-medium"> Employee List </Typography>
			</div>
			<Box>
				<Table>
					<TableHead>
						<TableRow style={{ fontSize: '16px' }}>
							<TableCell className="whitespace-nowrap text-16 font-medium">ID</TableCell>
							<TableCell className="whitespace-nowrap text-16 font-medium">Image </TableCell>
							<TableCell className="whitespace-nowrap text-16 font-medium">Name</TableCell>
							<TableCell className="whitespace-nowrap text-16 font-medium">Phone</TableCell>
							<TableCell className="whitespace-nowrap text-16 font-medium">Email</TableCell>
							<TableCell className="whitespace-nowrap text-16 font-medium">City</TableCell>
							<TableCell className="whitespace-nowrap text-16 font-medium">Country</TableCell>
						</TableRow>
					</TableHead>

					<TableBody>
						{props.widget?.map(e => {
							return (
								<TableRow hover key={e?.id}>
									<TableCell className="whitespace-nowrap p-4 md:p-16" component="th" scope="row">
										{e?.id}
									</TableCell>

									<TableCell
										className="w-52 px-4 md:px-0 h-52"
										component="th"
										scope="row"
										padding="none"
									>
										<img
											className="h-full block rounded"
											style={{ borderRadius: '15px' }}
											src={`${BASE_URL}${e.image}`}
											alt={e?.name}
										/>
									</TableCell>
									<TableCell className="whitespace-nowrap p-4 md:p-16" component="th" scope="row">
										{e?.first_name} {e?.first_name}
									</TableCell>

									<TableCell className="whitespace-nowrap p-4 md:p-16" component="th" scope="row">
										{e?.primary_phone}
									</TableCell>
									<TableCell className="whitespace-nowrap p-4 md:p-16" component="th" scope="row">
										{e?.email}
									</TableCell>
									<TableCell className="whitespace-nowrap p-4 md:p-16" component="th" scope="row">
										{e?.city?.name}
									</TableCell>
									<TableCell className="whitespace-nowrap p-4 md:p-16" component="th" scope="row">
										{e?.country?.name}
									</TableCell>
								</TableRow>
							);
						})}
					</TableBody>
				</Table>
			</Box>
		</Paper>
	);
}

export default memo(Widget11);
