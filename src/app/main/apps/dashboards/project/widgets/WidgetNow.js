import Icon from '@material-ui/core/Icon';
import IconButton from '@material-ui/core/IconButton';
import Paper from '@material-ui/core/Paper';
import Typography from '@material-ui/core/Typography';
import format from 'date-fns/format';
import { memo, useEffect, useRef, useState } from 'react';
import AnalogClock from 'analog-clock-react';

let options = {
	width: '200px',
	border: true,
	borderColor: '#2e2e2e',
	baseColor: '#17a2b8',
	centerColor: '#459cff',
	centerBorderColor: '#ffffff',
	handColors: {
		second: '#d81c7a',
		minute: '#ffffff',
		hour: '#ffffff'
	}
};

function WidgetNow() {
	const [time, setTime] = useState(new Date());
	const intervalRef = useRef();

	useEffect(() => {
		intervalRef.current = setInterval(update, 1000);
		return () => {
			clearInterval(intervalRef.current);
		};
	});

	function update() {
		setTime(new Date());
	}
	const hours = time.getHours();
	const minutes = time.getMinutes().toString().padStart(2, '0');
	const seconds = time.getSeconds().toString().padStart(2, '0');
	const ampm = hours >= 12 ? 'PM' : 'AM';
	const formattedHours = hours % 12 || 12;

	return (
		<Paper className="w-full rounded-70 shadow flex flex-col justify-between mt-136 h-300">
			<div
				className="text-center px-20 pt-5 pb-32"
				style={{ display: 'flex', flexDirection: 'column', justifyContent: 'center', alignItems: 'center' }}
			>
				<div>
					{/* <AnalogClock {...options} /> */}
					<Typography className="text-60 leading-tight font-medium tracking-tighter" color="textSecondary">
						{/* {`${formattedHours}:${minutes}:${seconds} ${ampm}`} */}
						{`${formattedHours}:${minutes} ${ampm}`}
					</Typography>
				</div>
				<Typography className="text-24 leading-tight font-normal" color="textSecondary">
					{format(time, 'MMMM')}
				</Typography>
				<Typography className="text-72 leading-tight font-medium tracking-tighter" color="textSecondary">
					{format(time, 'd')}
				</Typography>
				<Typography className="mt-8 text-20 leading-tight font-semibold tracking-tighter" color="textSecondary">
					{format(time, 'Y')}
				</Typography>
			</div>
		</Paper>
	);
}

export default memo(WidgetNow);
