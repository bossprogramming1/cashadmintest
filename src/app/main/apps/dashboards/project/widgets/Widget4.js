import Icon from '@material-ui/core/Icon';
import IconButton from '@material-ui/core/IconButton';
import Paper from '@material-ui/core/Paper';
import Typography from '@material-ui/core/Typography';
import { memo, useEffect } from 'react';
import { useDispatch } from 'react-redux';
import ArrowRightIcon from '@mui/icons-material/ArrowRight';
import { Button } from '@mui/material';
import history from '@history';
import { getWidget4 } from '../store/widgetsSlice';

function Widget4(props) {
	const dispatch = useDispatch();

	useEffect(() => {
		dispatch(getWidget4());
	}, []);
	return (
		<Paper className="w-full rounded-20 shadow flex flex-col justify-between">
			<Typography className="text-16  text-center p-16 font-medium" color="textSecondary">
				{props.widget?.title}
			</Typography>
			{/* <IconButton aria-label="more">
					<Icon>more_vert</Icon>
				</IconButton> */}
			<div className="text-center py-12">
				<Typography className="text-72 font-semibold leading-none text-red tracking-tighter">
					{props.widget?.data?.count}
				</Typography>
				<Typography className="text-18 font-normal text-red-800">{props.widget?.data?.name}</Typography>
			</div>
			<Button
				color="info"
				endIcon={<ArrowRightIcon fontSize="small" />}
				size="medium"
				variant="text"
				onClick={() => {
					history.push(`/apps/report-management/low-stocks-reports`);
				}}
			>
				View all
			</Button>
		</Paper>
	);
}

export default memo(Widget4);
