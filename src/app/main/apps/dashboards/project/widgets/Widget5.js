import Divider from '@material-ui/core/Divider';
import Paper from '@material-ui/core/Paper';
import { useTheme } from '@material-ui/core/styles';
import Tab from '@material-ui/core/Tab';
import Tabs from '@material-ui/core/Tabs';
import Typography from '@material-ui/core/Typography';
import _ from '@lodash';
import { memo, useState, useEffect } from 'react';
import ReactApexChart from 'react-apexcharts';
import { Bar } from 'react-chartjs-2';
import { Box, Button } from '@mui/material';
import ArrowDropDownIcon from '@mui/icons-material/ArrowDropDown';
import ArrowRightIcon from '@mui/icons-material/ArrowRight';
import ShoppingCartIcon from '@mui/icons-material/ShoppingCart';
import { useDispatch } from 'react-redux';
import { getWidget5 } from '../store/widgetsSlice';

function Widget5(props) {
	const theme = useTheme();
	const dispatch = useDispatch();
	const [cancelData, setCancelData] = useState([]);
	const [orderData, setOrderData] = useState([]);
	const [dayLabel, setDayLabels] = useState([]);

	useEffect(() => {
		dispatch(getWidget5()).then(action => {
			setCancelData(action?.payload?.cancel_order);
			setOrderData(action?.payload?.total_order);
			setDayLabels(action?.payload?.dates);
			// const mainData = action?.payload?.monthly_response;
			// const data = Object.fromEntries(Object.entries(mainData).reverse());
			// const monthsArray = Object.keys(data).map(month => month.slice(0, 3).toUpperCase());
			// setYearlyLabels(monthsArray);
			// setYearlyDatas(Object.values(data));
		});
	}, []);

	const reversedDayLabel = dayLabel?.slice().reverse();
	const reversedOrderData = orderData?.slice().reverse();
	const reversedCancelData = cancelData?.slice().reverse();

	const data = {
		datasets: [
			{
				backgroundColor: '#3F51B5',
				barPercentage: 0.5,
				barThickness: 12,
				borderRadius: 4,
				categoryPercentage: 0.5,
				data: reversedOrderData,
				label: 'This year',
				maxBarThickness: 10
			},
			{
				backgroundColor: '#EEEEEE',
				barPercentage: 0.5,
				barThickness: 12,
				borderRadius: 4,
				categoryPercentage: 0.5,
				data: reversedCancelData,
				label: 'Last year',
				maxBarThickness: 10
			}
		],
		labels: reversedDayLabel
	};

	const options = {
		animation: false,
		cornerRadius: 20,
		layout: { padding: 0 },
		legend: { display: false },
		maintainAspectRatio: false,
		responsive: true,
		xAxes: [
			{
				ticks: {
					fontColor: theme.palette.text.secondary
				},
				gridLines: {
					display: false,
					drawBorder: false
				}
			}
		],
		yAxes: [
			{
				ticks: {
					fontColor: theme.palette.text.secondary,
					beginAtZero: true,
					min: 0
				},
				gridLines: {
					borderDash: [2],
					borderDashOffset: [2],
					color: theme.palette.divider,
					drawBorder: false,
					zeroLineBorderDash: [2],
					zeroLineBorderDashOffset: [2],
					zeroLineColor: theme.palette.divider
				}
			}
		],
		tooltips: {
			backgroundColor: theme.palette.background.paper,
			bodyFontColor: theme.palette.text.secondary,
			borderColor: theme.palette.divider,
			borderWidth: 1,
			enabled: true,
			footerFontColor: theme.palette.text.secondary,
			intersect: false,
			mode: 'index',
			titleFontColor: theme.palette.text.primary
		}
	};

	return (
		<Paper {...props} className="w-full rounded-40 shadow">
			<div className="flex items-center justify-between p-20 h-64">
				<Typography className="text-16 font-medium">
					<ShoppingCartIcon /> Oders
				</Typography>
				{/* <Button endIcon={<ArrowDropDownIcon fontSize="small" />} size="small">
					Last 7 days
				</Button> */}
			</div>

			<Box
				sx={{
					height: 400,
					position: 'relative'
				}}
			>
				<Bar data={data} options={options} />
			</Box>

			<Box
				sx={{
					display: 'flex',
					justifyContent: 'flex-end',
					p: 2
				}}
			>
				<Button color="primary" endIcon={<ArrowRightIcon fontSize="small" />} size="small">
					Overview
				</Button>
			</Box>
		</Paper>
	);
}

export default memo(Widget5);
