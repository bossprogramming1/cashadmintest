/* eslint-disable import/no-extraneous-dependencies */
/* eslint-disable import/order */
import Paper from '@material-ui/core/Paper';
import Select from '@material-ui/core/Select';
import { createTheme, ThemeProvider } from '@mui/material/styles';
import Typography from '@material-ui/core/Typography';
import _ from '@lodash';
import { useEffect, memo, useState } from 'react';
import ReactApexChart from 'react-apexcharts';
import { format } from 'date-fns';
import { v4 as uuid } from 'uuid';
import {
	Box,
	Button,
	Card,
	CardHeader,
	Table,
	TableBody,
	TableCell,
	TableHead,
	TableRow,
	TableSortLabel,
	Tooltip
} from '@material-ui/core';
import ArrowRightIcon from '@mui/icons-material/ArrowRight';
import { makeStyles } from '@material-ui/core/styles';
import { useDispatch } from 'react-redux';
import { getWidget9 } from '../store/widgetsSlice';
import history from '@history';
import { StatusColor } from './StatusColor';
import ShoppingCartIcon from '@mui/icons-material/ShoppingCart';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import {} from '@fortawesome/free-solid-svg-icons';
import Pagination from '@material-ui/lab/Pagination';

function Widget9(props) {
	const dispatch = useDispatch();
	const [page, setPage] = useState(1);
	const [totalPages, setTotalPages] = useState(0);

	useEffect(() => {
		dispatch(getWidget9({ page, size: 4 })).then(action => {
			setPage(action.payload?.page);
			setTotalPages(action.payload?.total_pages);
		});
	}, []);

	const handlePagination = (page, hanglePage) => {
		dispatch(getWidget9({ page: hanglePage, size: 4 })).then(action => {
			setPage(action.payload?.page);
			setTotalPages(action.payload?.total_pages);
		});
	};

	return (
		<Paper className="w-full rounded-40 shadow">
			{/* sx={{ minWidth: 800 }} */}
			<div className="flex items-center justify-between p-20 h-64">
				<Typography className="text-16 font-medium">
					{' '}
					<ShoppingCartIcon /> Best Seller Quantity
				</Typography>
			</div>
			<Box>
				<Table>
					<TableHead style={{ fontWeight: 600, fontSize: ' 1.1rem' }}>
						<TableRow>
							<TableCell className="whitespace-nowrap text-16 font-medium">Name</TableCell>
							<TableCell className="whitespace-nowrap text-16 font-medium">Total Items</TableCell>
							<TableCell className="whitespace-nowrap text-16 font-medium">Total Amount</TableCell>
						</TableRow>
					</TableHead>

					<TableBody>
						{props?.widget?.map(info => {
							return (
								<TableRow hover key={info?.id}>
									<TableCell>
										{info?.first_name} {info?.last_name}
									</TableCell>

									<TableCell>{info?.total_items}</TableCell>
									<TableCell>{info?.total_amount}</TableCell>
								</TableRow>
							);
						})}
					</TableBody>
				</Table>
				<Box
					sx={{
						display: 'flex',
						justifyContent: 'flex start',
						p: 2
					}}
				>
					<Pagination
						count={totalPages}
						page={page}
						defaultPage={1}
						color="primary"
						showFirstButton
						showLastButton
						variant="outlined"
						shape="rounded"
						onChange={handlePagination}
					/>
				</Box>
			</Box>
		</Paper>
	);
}

export default memo(Widget9);
