import FuseLoading from '@fuse/core/FuseLoading';
import FuseScrollbars from '@fuse/core/FuseScrollbars';
import _ from '@lodash';
import Checkbox from '@material-ui/core/Checkbox';
import { makeStyles } from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TablePagination from '@material-ui/core/TablePagination';
import TableRow from '@material-ui/core/TableRow';
import DeleteIcon from '@material-ui/icons/Delete';
import EditIcon from '@material-ui/icons/Edit';
import Pagination from '@material-ui/lab/Pagination';
import { BASE_URL, SEARCH_PRODUCT } from 'app/constant/constants';
import { useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { withRouter } from 'react-router-dom';
import { Tooltip } from '@material-ui/core';
import { getProducts, selectProducts } from '../store/productsSlice';
import ProductsTableHead from './ProductsTableHead';

const useStyles = makeStyles(theme => ({
	root: {
		display: 'flex',
		justifyContent: 'space-between',
		flexWrap: 'wrap',
		'& > *': {
			marginTop: theme.spacing(2),
			marginBottom: theme.spacing(3)
		}
	}
}));
function ProductsTable(props) {
	const classes = useStyles();
	const dispatch = useDispatch();
	const products = useSelector(selectProducts);
	const searchText = useSelector(({ eCommerceApp }) => eCommerceApp.products.searchText);
	const [loading, setLoading] = useState(true);
	const [selected, setSelected] = useState([]);
	const [data, setData] = useState(products);
	let serialNumber = 0;
	const [page, setPage] = useState(0);
	const [rowsPerPage, setRowsPerPage] = useState(30);
	const [pageAndSize, setPageAndSize] = useState({ page: 1, size: 30 });
	const totalPages = sessionStorage.getItem('total_products_pages');
	const totalElements = sessionStorage.getItem('total_products_elements');

	const [searchProduct, setSearchProduct] = useState([]);

	const [order, setOrder] = useState({
		direction: 'asc',
		id: null
	});

	//Get all products
	useEffect(() => {
		dispatch(getProducts(pageAndSize)).then(() => setLoading(false));
	}, [dispatch]);

	//searchProduct
	useEffect(() => {
		searchText !== '' && getSearchProduct();
	}, [searchText]);

	const getSearchProduct = () => {
		fetch(`${SEARCH_PRODUCT}?name=${searchText}`)
			.then(response => response.json())
			.then(productsData => {
				setSearchProduct(productsData.products);
			});
	};

	function handleRequestSort(event, property) {
		const id = property;
		let direction = 'desc';

		if (order.id === property && order.direction === 'desc') {
			direction = 'asc';
		}

		setOrder({
			direction,
			id
		});
	}

	function handleSelectAllClick(event) {
		if (event.target.checked) {
			setSelected(products.map(n => n.id));
			return;
		}
		setSelected([]);
	}

	function handleDeselect() {
		setSelected([]);
	}

	function handleClick(item) {
		props.history.push(`/apps/e-commerce/products/${item.id}/${item.handle}`);
	}

	function handleCheck(event, id) {
		const selectedIndex = selected.indexOf(id);
		let newSelected = [];

		if (selectedIndex === -1) {
			newSelected = newSelected.concat(selected, id);
		} else if (selectedIndex === 0) {
			newSelected = newSelected.concat(selected.slice(1));
		} else if (selectedIndex === selected.length - 1) {
			newSelected = newSelected.concat(selected.slice(0, -1));
		} else if (selectedIndex > 0) {
			newSelected = newSelected.concat(selected.slice(0, selectedIndex), selected.slice(selectedIndex + 1));
		}

		setSelected(newSelected);
	}

	//pagination
	const handlePagination = (e, handlePage) => {
		setPageAndSize({ ...pageAndSize, page: handlePage });
		setPage(handlePage - 1);
		dispatch(getProducts({ ...pageAndSize, page: handlePage }));
	};

	function handleChangePage(event, value) {
		setPage(value);
		setPageAndSize({ ...pageAndSize, page: value + 1 });
		dispatch(getProducts({ ...pageAndSize, page: value + 1 }));
	}

	function handleChangeRowsPerPage(event) {
		setRowsPerPage(event.target.value);
		setPageAndSize({ ...pageAndSize, size: event.target.value });
		dispatch(getProducts({ ...pageAndSize, size: event.target.value }));
	}

	//handleUpdateProduct
	function handleUpdateProduct(item, event) {
		localStorage.removeItem('deleteProduct');
		localStorage.setItem('updateProduct', event);
		props.history.push(`/apps/e-commerce/product/${item.id}/${item?.name}`);
	}
	function handleDeleteProduct(item, event) {
		localStorage.removeItem('updateProduct');
		localStorage.setItem('deleteProduct', event);
		props.history.push(`/apps/e-commerce/product/${item.id}/${item?.name}`);
	}

	if (loading) {
		return <FuseLoading />;
	}

	const user_role = localStorage.getItem('user_role');

	return (
		<div className="w-full flex flex-col">
			<FuseScrollbars className="flex-grow overflow-x-auto">
				<Table stickyHeader className="min-w-xl" aria-labelledby="tableTitle">
					<ProductsTableHead
						selectedProductIds={selected}
						order={order}
						onSelectAllClick={handleSelectAllClick}
						onRequestSort={handleRequestSort}
						rowCount={products.length}
						onMenuItemClick={handleDeselect}
					/>

					<TableBody>
						{_.orderBy(
							searchText !== '' && searchProduct ? searchProduct : products,
							[
								o => {
									switch (order.id) {
										case 'categories': {
											return o.categories[0];
										}
										default: {
											return o[order.id];
										}
									}
								}
							],
							[order.direction]
						).map(n => {
							const isSelected = selected.indexOf(n.id) !== -1;
							return (
								<TableRow
									//className="h-52 cursor-pointer"
									hover
									role="checkbox"
									aria-checked={isSelected}
									tabIndex={-1}
									key={n.id}
									selected={isSelected}
								>
									<TableCell className="whitespace-nowrap w-40 md:w-64 text-center" padding="none">
										<Checkbox
											checked={isSelected}
											onClick={event => event.stopPropagation()}
											onChange={event => handleCheck(event, n.id)}
										/>
									</TableCell>

									<TableCell className="whitespace-nowrap p-4 md:p-16" component="th" scope="row">
										{pageAndSize.page * pageAndSize.size - pageAndSize.size + serialNumber++}
									</TableCell>
									<TableCell className="whitespace-nowrap p-4 md:p-16" component="th" scope="row">
										{n?.product_code}
									</TableCell>

									<TableCell
										whitespace-nowrap
										className="w-52 h-52 whitespace-nowrap px-4 md:px-0"
										component="th"
										scope="row"
										padding="none"
									>
										{n.length > 0 && n.featuredImageId ? (
											<img
												className="block rounded"
												style={{ borderRadius: '50%', height: '50px', width: '50px' }}
												src={_.find(n.thumbnail, { id: n.featuredImageId }).url}
												alt={n?.name}
											/>
										) : (
											<img
												style={{ borderRadius: '50%', height: '50px', width: '50px' }}
												className=" block rounded"
												src={`${BASE_URL}${n.thumbnail}`}
												alt={n?.name}
											/>
										)}
									</TableCell>

									<TableCell className="whitespace-nowrap p-4 md:p-16" component="th" scope="row">
										{`${n?.name.length > 25 ? `${n?.name.slice(0, 25)} ...` : n?.name} `}
									</TableCell>

									<TableCell className="whitespace-nowrap p-4 md:p-16" component="th" scope="row">
										{n?.category?.name}
									</TableCell>

									<TableCell
										whitespace-nowrap
										className="p-4 md:p-16 truncate"
										component="th"
										scope="row"
									>
										{n.unit_price}
									</TableCell>

									<TableCell className="whitespace-nowrap p-0 md:p-0" component="th" scope="row">
										<div>
											<Tooltip title="Edit" placement="top" enterDelay={300}>
												<EditIcon
													className="h-52 cursor-pointer"
													style={{ color: 'green' }}
													onClick={event => handleUpdateProduct(n, 'updateProduct')}
												/>
											</Tooltip>
											<Tooltip
												title="Delete"
												placement="top"
												enterDelay={300}

												// popperProps={{
												// 	style: { marginTop: '-10px' }
												// }}
											>
												<DeleteIcon
													className="h-52 cursor-pointer"
													style={{
														color: 'red',
														visibility:
															user_role === 'ADMIN' || user_role === 'admin'
																? 'visible'
																: 'hidden'
													}}
													onClick={event => handleDeleteProduct(n, 'deleteProduct')}
												/>
											</Tooltip>
										</div>
									</TableCell>
								</TableRow>
							);
						})}
					</TableBody>
				</Table>
			</FuseScrollbars>
			<div className={classes.root} id="pagiContainer">
				<Pagination
					count={totalPages}
					page={page + 1}
					defaultPage={1}
					color="primary"
					showFirstButton
					showLastButton
					variant="outlined"
					shape="rounded"
					onChange={handlePagination}
				/>

				<TablePagination
					className="flex-shrink-0 border-t-1"
					rowsPerPageOptions={[30, 50, 100]}
					component="div"
					count={totalElements}
					rowsPerPage={rowsPerPage}
					page={page}
					backIconButtonProps={{
						'aria-label': 'Previous Page'
					}}
					nextIconButtonProps={{
						'aria-label': 'Next Page'
					}}
					onChangePage={handleChangePage}
					onChangeRowsPerPage={handleChangeRowsPerPage}
				/>
			</div>
		</div>
	);
}

export default withRouter(ProductsTable);
