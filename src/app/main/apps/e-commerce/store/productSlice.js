import { createAsyncThunk, createSlice } from '@reduxjs/toolkit';
import axios from 'axios';
import { addNotification } from 'app/fuse-layouts/shared-components/notificationPanel/store/dataSlice';
import NotificationModel from 'app/fuse-layouts/shared-components/notificationPanel/model/NotificationModel';
import {
	CREATE_PRODUCT,
	CREATE_PRODUCT_IMAGE,
	DELETE_PRODUCT,
	DELETE_PRODUCT_IMAGE,
	GET_PRODUCT,
	GET_PRODUCT_IMAGES,
	UPDATE_PRODUCT
} from '../../../../constant/constants';

export const getProduct = createAsyncThunk('eCommerceApp/product/getProduct', async params => {
	const authTOKEN = {
		headers: {
			'Content-type': 'application/json',
			Authorization: localStorage.getItem('jwt_access_token')
		}
	};
	const response = await axios.get(`${GET_PRODUCT}${params.productId}`, authTOKEN);
	const data = await response.data;
	return data === undefined ? null : data;
});

export const removeProduct = createAsyncThunk(
	'eCommerceApp/product/removeProduct',
	async (val, { dispatch, getState }) => {
		const authTOKEN = {
			headers: {
				'Content-type': 'application/json',
				Authorization: localStorage.getItem('jwt_access_token')
			}
		};
		const productId = val.id;

		try {
			const res = await axios.delete(`${DELETE_PRODUCT}${productId}`, authTOKEN);
			console.log('resRemoveProduct', res);
			return res; // Return the response if the request is successful.
		} catch (err) {
			if (err.response.status == 400) {
				console.log('Status code:', err.response); // Access the status code.
				dispatch(
					addNotification(
						NotificationModel({
							message: `${err.response.data.detail}`,
							options: { variant: 'error' },
							item_id: productId
						})
					)
				);
			}
			console.log('errRemove', err);
			throw err; // Rethrow the error if an error occurs.
		}
	}
);

//buildformdata
const buildFormData = (formData, data, parentKey) => {
	if (data && typeof data === 'object' && !(data instanceof Date) && !(data instanceof File)) {
		Object.keys(data).forEach(key => {
			buildFormData(formData, data[key], parentKey ? `${parentKey}[${key}]` : key);
		});
	} else {
		const value = data === null ? '' : data;

		formData.append(parentKey, value);
	}
};

//convertJsonToFormData
const jsonToFormData = data => {
	const formData = new FormData();

	buildFormData(formData, data);
	return formData;
};

export const productUpdate = createAsyncThunk(
	'employeeManagement/employee/updateProduct',
	async (productData, { dispatch, getState }) => {
		const productDataToFormData = jsonToFormData(productData);
		const authTOKEN = {
			headers: {
				'Content-type': 'application/json',
				Authorization: localStorage.getItem('jwt_access_token')
			}
		};
		const { product } = getState().eCommerceApp;

		const response = await axios.put(`${UPDATE_PRODUCT}${product.id}`, productDataToFormData, authTOKEN);
	}
);

export const saveProduct = createAsyncThunk(
	'eCommerceApp/product/saveProduct',
	async (productData, { dispatch, getState }) => {
		const productDataToFormData = jsonToFormData(productData);
		const authTOKEN = {
			headers: {
				'Content-type': 'multipart/form-data',
				Authorization: localStorage.getItem('jwt_access_token')
			}
		};
		const response = await axios.post(CREATE_PRODUCT, productDataToFormData, authTOKEN);
	}
);

//saveProductImage
export const saveProductImage = createAsyncThunk(
	'eCommerceApp/product/saveProduct',
	async (productImageData, { dispatch, getState }) => {
		const productImageDataToFormData = jsonToFormData(productImageData);
		const authTOKEN = {
			headers: {
				'content-type': 'multipart/form-data',
				Authorization: localStorage.getItem('jwt_access_token')
			}
		};
		const response = await axios.post(CREATE_PRODUCT_IMAGE, productImageDataToFormData, authTOKEN);
	}
);

//getProductImages
export const getProductImages = createAsyncThunk('eCommerceApp/products/getProductImages', async () => {
	const authTOKEN = {
		headers: {
			'Content-type': 'application/json',
			Authorization: localStorage.getItem('jwt_access_token')
		}
	};
	const response = await axios.get(GET_PRODUCT_IMAGES, authTOKEN);
	const data = await response.data.product_images;
	return data;
});

//removeProductImage
export const removeProductImage = createAsyncThunk(
	'eCommerceApp/product/removeProduct',
	async (val, { dispatch, getState }) => {
		const authTOKEN = {
			headers: {
				'Content-type': 'application/json',
				Authorization: localStorage.getItem('jwt_access_token')
			}
		};

		const response = await axios.delete(`${DELETE_PRODUCT_IMAGE}${val}`, authTOKEN);
		const data = await response;
		return data;
	}
);

const productSlice = createSlice({
	name: 'eCommerceApp/product',
	initialState: null,
	reducers: {
		resetProduct: () => null,
		newProduct: {
			reducer: (state, action) => action.payload,
			prepare: event => ({
				payload: {
					name: '',
					short_desc: '',
					full_desc: '',
					sku: '',
					gtin: '',
					old_price: 0,
					unit_price: 0,
					published: false,
					disabled: false,
					popular: false,
					featured: false,
					verified: false,
					thumbnail: '',
					rating: '',
					numReviews: 0,
					show_on_homepage: false,
					display_order: 0,
					allow_customer_review: false,
					mark_as_new: false,
					disable_buy_button: false,
					disable_wishlist_button: false,
					available_for_preorder: false,
					expire_info: '',
					admin_comment: '',
					attribute_set: 0,
					brand: 0,
					category: 0,
					manufacturer: 0,
					vendor: 0,
					verified_by: 0,
					barcode: 0,
					is_flash_deal: false,
					is_new_arrival: false,
					is_under_discount: false,
					image: ''
				}
			})
		}
	},
	extraReducers: {
		[getProduct.fulfilled]: (state, action) => action.payload,
		[saveProduct.fulfilled]: (state, action) => {
			localStorage.setItem('productAlert', 'saveProduct');
			return action.payload;
		},
		[removeProduct.fulfilled]: (state, action) => {
			localStorage.setItem('productAlert', 'deleteProduct');
			return action.payload;
		},
		[productUpdate.fulfilled]: () => {
			localStorage.setItem('productAlert', 'updateProduct');
		}
	}
});

export const { newProduct, resetProduct } = productSlice.actions;

export default productSlice.reducer;
