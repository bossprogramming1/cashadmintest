import { createSlice } from '@reduxjs/toolkit';
import {
	GET_USERS_WITHOUT_PAGINATION,
	GET_BRANDS_WITHOUT_PAGINATION,
	GET_EMPLOYEES_WITHOUT_PAGINATION,
	GET_ATTRIBUTE_SET_WITHOUT_PAGINATION,
	GET_CATEGORIES_WITHOUT_PAGINATION,
	GET_MANUFACTURERS_WITHOUT_PAGINATION,
	GET_CUSTOMER_WITHOUT_PAGINATION,
	GET_COLORS_WITHOUT_PAGINATION,
	GET_SIZES_WITHOUT_PAGINATION
} from '../../../../constant/constants';

export const getAttributeSet = () => dispatch => {
	fetch(GET_ATTRIBUTE_SET_WITHOUT_PAGINATION)
		.then(response => response.json())
		.then(data => dispatch(setAttributeSet(data?.attribute_sets)));
};

export const getBrand = () => dispatch => {
	fetch(GET_BRANDS_WITHOUT_PAGINATION)
		.then(response => response.json())
		.then(data => dispatch(setBrands(data?.brands)));
};

export const getCategory = () => dispatch => {
	fetch(GET_CATEGORIES_WITHOUT_PAGINATION)
		.then(response => response.json())
		.then(data => dispatch(setCategories(data?.categories)));
};
export const getManufacturer = () => dispatch => {
	fetch(GET_MANUFACTURERS_WITHOUT_PAGINATION)
		.then(response => response.json())
		.then(data => dispatch(setManufacturers(data?.manufacturers)));
};
export const getVendor = () => dispatch => {
	fetch(GET_CUSTOMER_WITHOUT_PAGINATION)
		.then(response => response.json())
		.then(data => dispatch(setVendors(data?.customers)));
};
export const getVerifiedBy = () => dispatch => {
	fetch(GET_EMPLOYEES_WITHOUT_PAGINATION)
		.then(response => response.json())
		.then(data => dispatch(setVerifiedBy(data?.employees)));
};
export const getAllColor = () => dispatch => {
	fetch(GET_COLORS_WITHOUT_PAGINATION)
		.then(response => response.json())
		.then(data => dispatch(setColors(data?.colors)));
};
export const getAllSize = () => dispatch => {
	fetch(GET_SIZES_WITHOUT_PAGINATION)
		.then(response => response.json())
		.then(data => dispatch(setSizes(data?.sizes)));
};

const productDataSlice = createSlice({
	name: 'eCommerceApp/productData',
	initialState: {
		Attribute_set: [],
		Brands: [],
		Categories: [],
		Manufacturers: [],
		Vendors: [],
		Verified_by: [],
		colors: [],
		sizes: []
	},
	reducers: {
		setAttributeSet: (state, action) => {
			state.Attribute_set = action.payload;
		},
		setBrands: (state, action) => {
			state.Brands = action.payload;
		},
		setCategories: (state, action) => {
			state.Categories = action.payload;
		},
		setManufacturers: (state, action) => {
			state.Manufacturers = action.payload;
		},
		setVendors: (state, action) => {
			state.Vendors = action.payload;
		},
		setVerifiedBy: (state, action) => {
			state.Verified_by = action.payload;
		},

		setColors: (state, action) => {
			state.colors = action.payload;
		},
		setSizes: (state, action) => {
			state.sizes = action.payload;
		}
	}
});
const { setAttributeSet, setBrands, setCategories, setManufacturers, setVendors, setVerifiedBy, setColors, setSizes } =
	productDataSlice.actions;
export default productDataSlice.reducer;
