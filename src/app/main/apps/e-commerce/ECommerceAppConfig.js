import { lazy } from 'react';

const ECommerceAppConfig = {
	settings: {
		layout: {}
	},
	routes: [
		{
			path: '/apps/e-commerce/products',
			component: lazy(() => import('./products/Products'))
		},
		{
			path: '/apps/e-commerce/product/:productId/:productName?',
			component: lazy(() => import('./product/Product'))
		},
		{
			path: '/apps/e-commerce/product/:productId',
			component: lazy(() => import('./product/Product'))
		},

		{
			path: '/apps/e-commerce/orders/:orderId',
			component: lazy(() => import('./order/Order'))
		},
		{
			path: '/apps/e-commerce/orders',
			component: lazy(() => import('./orders/Orders'))
		}
	]
};

export default ECommerceAppConfig;
