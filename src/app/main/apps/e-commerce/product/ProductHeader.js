import _ from '@lodash';
import Button from '@material-ui/core/Button';
import Icon from '@material-ui/core/Icon';
import { useTheme } from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';
import { motion } from 'framer-motion';
import React, { useState, useContext } from 'react';
import { useFormContext } from 'react-hook-form';
import { useDispatch, useSelector } from 'react-redux';
import { Link, useHistory, useParams } from 'react-router-dom';
import { productUpdate, removeProduct, saveProduct } from '../store/productSlice';
import { selectProducts } from '../store/productsSlice';
import EditorContext from './EditorContext';

function ProductHeader(props) {
	const [editorText, setEditorText] = useContext(EditorContext);
	const dispatch = useDispatch();
	const methods = useFormContext();
	const { formState, watch, getValues, setError } = methods;
	const { isValid, dirtyFields } = formState;
	const images = watch('thumbnail', []);
	const featuredImageId = watch('featuredImageId');
	const thumbnail = watch('thumbnail');
	const newProductName = watch('name');
	const theme = useTheme();
	const history = useHistory();
	const routeParams = useParams();
	const { productId } = routeParams;
	const [loading, setLoading] = useState(true);
	const deleteProduct = localStorage.getItem('deleteProduct');
	const updateProduct = localStorage.getItem('updateProduct');

	const products = useSelector(selectProducts);

	function handleSaveProduct() {
		const data = getValues();
		data.image = '';
		data.barcode = 0;
		data.is_flash_deal = false;
		data.is_new_arrival = false;
		data.is_under_discount = false;

		data.is_published = false;
		data.disable_wishlist_button = false;
		data.is_available_for_preorder = false;
		data.is_disabled = false;
		data.is_featured = false;
		data.is_popular = false;
		data.is_verified = false;
		data.num_reviews = 1;
		const productExist = products.find(({ name }) => name.toLowerCase() === data?.name.toLowerCase());
		if (productExist) {
			setError('name', {
				type: 'manual',
				message: 'This name already exist'
			});
		} else {
			dispatch(saveProduct(data)).then(action => {
				if (action.status === 200 || 201) {
					history.push('/apps/e-commerce/products');
				} else {
					('');
				}
			});
		}
	}

	function handleRemoveProduct() {
		dispatch(removeProduct(getValues())).then(action => {
			if (action.payload) {
				history.push('/apps/e-commerce/products');
			}
		});
	}

	function handleUpdateProduct() {
		const getUpdateData = getValues();
		getUpdateData.image = '';
		getUpdateData.barcode = getUpdateData.barcode || Math.floor(Math.random() * productId);

		dispatch(productUpdate(getUpdateData)).then(() => {
			history.push('/apps/e-commerce/products');
		});
	}

	const handleCancel = () => {
		localStorage.removeItem('productAlert');
		localStorage.removeItem('updateProduct');
		localStorage.removeItem('deleteProduct');
		history.push('/apps/e-commerce/products');
	};

	return (
		<div className="flex flex-1 w-full items-center justify-between">
			<div className="flex flex-col items-start max-w-full min-w-0">
				<motion.div initial={{ x: 20, opacity: 0 }} animate={{ x: 0, opacity: 1, transition: { delay: 0.3 } }}>
					<Typography
						className="flex items-center sm:mb-12"
						component={Link}
						role="button"
						to="/apps/e-commerce/products"
						color="inherit"
					>
						<Icon className="text-20">{theme.direction === 'ltr' ? 'arrow_back' : 'arrow_forward'}</Icon>
						<span className="hidden sm:flex mx-4 font-medium">Products</span>
					</Typography>
				</motion.div>

				<div className="flex items-center max-w-full">
					<div className="flex flex-col min-w-0 mx-8 sm:mc-16">
						<motion.div initial={{ x: -20 }} animate={{ x: 0, transition: { delay: 0.3 } }}>
							<Typography className="text-16 sm:text-20 truncate font-semibold">
								{newProductName || 'New Product'}
							</Typography>
							<Typography variant="caption" className="font-medium">
								Product Detail
							</Typography>
						</motion.div>
					</div>
				</div>
			</div>
			<motion.div
				className="flex"
				initial={{ opacity: 0, x: 20 }}
				animate={{ opacity: 1, x: 0, transition: { delay: 0.3 } }}
			>
				{deleteProduct === 'deleteProduct' && (
					<Typography className="mt-6" variant="subtitle2">
						Do you want to remove this Product?
					</Typography>
				)}
				{deleteProduct === 'deleteProduct' && (
					<Button
						className="whitespace-nowrap mx-4"
						variant="contained"
						color="secondary"
						onClick={handleRemoveProduct}
						startIcon={<Icon className="hidden sm:flex">delete</Icon>}
						style={{ backgroundColor: '#ea5b78', color: 'white' }}
					>
						Remove
					</Button>
				)}
				{deleteProduct !== 'deleteProduct' && routeParams.productId === 'new' && (
					<Button
						className="whitespace-nowrap mx-4"
						variant="contained"
						color="secondary"
						disabled={_.isEmpty(dirtyFields) || !isValid}
						onClick={handleSaveProduct}
					>
						Save
					</Button>
				)}
				{updateProduct === 'updateProduct' && deleteProduct !== 'deleteProduct' && (
					<Button
						className="whitespace-nowrap mx-4"
						color="secondary"
						variant="contained"
						//disabled={_.isEmpty(dirtyFields) || !isValid}
						style={{ backgroundColor: '#4dc08e', color: 'white' }}
						onClick={handleUpdateProduct}
					>
						Update
					</Button>
				)}
				<Button
					className="whitespace-nowrap mx-4"
					variant="contained"
					style={{ backgroundColor: '#FFAA4C', color: 'white' }}
					onClick={handleCancel}
				>
					Cancel
				</Button>
			</motion.div>
		</div>
	);
}

export default ProductHeader;
