import FusePageCarded from '@fuse/core/FusePageCarded';
import { useDeepCompareEffect } from '@fuse/hooks';
import { yupResolver } from '@hookform/resolvers/yup';
import Button from '@material-ui/core/Button';
import Tab from '@material-ui/core/Tab';
import Tabs from '@material-ui/core/Tabs';
import Typography from '@material-ui/core/Typography';
import withReducer from 'app/store/withReducer';
import { motion } from 'framer-motion';
import { useEffect, useState } from 'react';
import { FormProvider, useForm } from 'react-hook-form';
import { useDispatch, useSelector } from 'react-redux';
import { Link, useParams } from 'react-router-dom';
import * as yup from 'yup';
import reducer from '../store';
import { getProduct, newProduct, resetProduct } from '../store/productSlice';
import ProductHeader from './ProductHeader';
import AttributeSetTab from './tabs/AttributeSetTab';
import BasicInfoTab from './tabs/BasicInfoTab';
import ColorTab from './tabs/ColorTab';
import PricingTab from './tabs/PricingTab';
import ProductImagesTab from './tabs/ProductImagesTab';
import SettingTab from './tabs/SettingTab';
import SizeTab from './tabs/SizeTab';
import EditorContext from './EditorContext';

/**
 * Form Validation Schema
 */
const schema = yup.object().shape({
	name: yup.string().required('You must enter a product name'),

	category: yup.number().required('Category is required'),
	manufacturer: yup.number().required('Manufacturer is required'),
	vendor: yup.number().required('Vendor is required'),
	verified_by: yup.number().required('verified by is required')
});

function Product(props) {
	const [editorText, setEditorText] = useState({});
	const dispatch = useDispatch();
	const product = useSelector(({ eCommerceApp }) => eCommerceApp.product);

	const routeParams = useParams();
	const [tabValue, setTabValue] = useState(0);
	const [noProduct, setNoProduct] = useState(false);
	const methods = useForm({
		mode: 'onChange',
		defaultValues: {},
		resolver: yupResolver(schema)
	});
	const { reset, watch, control, onChange, formState } = methods;
	const form = watch();

	useDeepCompareEffect(() => {
		function updateProductState() {
			const { productId } = routeParams;
			if (productId === 'new') {
				/**
				 * Create New Product data
				 */
				localStorage.removeItem('deleteProduct');
				localStorage.removeItem('updateProduct');
				dispatch(newProduct());
			} else {
				/**
				 * Get Product data
				 */
				dispatch(getProduct(routeParams)).then(action => {
					/**
					 * If the requested product is not exist show message
					 */
					if (!action.payload) {
						setNoProduct(true);
					}
				});
			}
		}

		updateProductState();
	}, [dispatch, routeParams]);

	useEffect(() => {
		if (!product) {
			return;
		}
		/**
		 * Reset the form on product state changes
		 */
		product?.full_desc ? localStorage.setItem('fullDescription', product?.full_desc) : '';

		reset({
			...product,
			brand: product?.brand?.id,
			vendor: product?.vendor?.id,
			manufacturer: product?.manufacturer?.id,
			verified_by: product?.verified_by?.id,
			category: product?.category?.id
		});
	}, [product, reset]);

	useEffect(() => {
		return () => {
			/**
			 * Reset Product on component unload
			 */
			dispatch(resetProduct());
			setNoProduct(false);
		};
	}, [dispatch]);

	/**
	 * Tab Change
	 */
	function handleTabChange(event, value) {
		setTabValue(value);
	}
	console.log('udatedProduct', localStorage.getItem('updateProduct'));

	/**
	 * Show Message if the requested products is not exists
	 */
	if (noProduct) {
		return (
			<motion.div
				initial={{ opacity: 0 }}
				animate={{ opacity: 1, transition: { delay: 0.1 } }}
				className="flex flex-col flex-1 items-center justify-center h-full"
			>
				<Typography color="textSecondary" variant="h5">
					There is no such product!
				</Typography>
				<Button
					className="mt-24"
					component={Link}
					variant="outlined"
					to="/apps/e-commerce/products"
					color="inherit"
				>
					Go to Products Page
				</Button>
			</motion.div>
		);
	}

	/**
	 * Wait while product data is loading and form is setted
	 */

	return (
		<EditorContext.Provider value={[editorText, setEditorText]}>
			<FormProvider {...methods}>
				<FusePageCarded
					classes={{
						toolbar: 'p-0',
						header: 'min-h-75 h-64'
					}}
					header={<ProductHeader />}
					contentToolbar={
						<Tabs
							value={tabValue}
							onChange={handleTabChange}
							indicatorColor="primary"
							textColor="primary"
							variant="scrollable"
							scrollButtons="auto"
							classes={{ root: 'w-full h-64' }}
						>
							<Tab className="h-64" label="Basic Info" />
							<Tab className="h-64" label="Pricing" />
							<Tab className="h-64" label="Attribute Set" />
							{localStorage.getItem('updateProduct') && <Tab className="h-64" label="Product Images" />}
							{/* {localStorage.getItem('updateProduct') && <Tab className="h-64" label="Product Color" />} */}
							{/* {localStorage.getItem('updateProduct') && <Tab className="h-64" label="Product Size" />} */}
							{localStorage.getItem('updateProduct') && <Tab className="h-64" label="Setting" />}
						</Tabs>
					}
					content={
						<div className="p-16 sm:p-24">
							<div className={tabValue !== 0 ? 'hidden' : ''}>
								<BasicInfoTab />
							</div>

							<div className={tabValue !== 1 ? 'hidden' : ''}>
								<PricingTab />
							</div>

							<div className={tabValue !== 2 ? 'hidden' : ''}>
								<AttributeSetTab />
							</div>

							{localStorage.getItem('updateProduct') && (
								<div className={tabValue !== 3 ? 'hidden' : ''}>
									<ProductImagesTab productId={routeParams.productId} />
								</div>
							)}

							{/* <div className={tabValue !== 4 ? 'hidden' : ''}>
								<SizeTab productId={routeParams.productId} />
							</div> */}
							<div className={tabValue !== 4 ? 'hidden' : ''}>
								<SettingTab productId={routeParams.productId} />
							</div>
						</div>
					}
					innerScroll
				/>
			</FormProvider>
		</EditorContext.Provider>
	);
}

export default withReducer('eCommerceApp', reducer)(Product);
