import { FormControl } from '@material-ui/core/';
import Checkbox from '@material-ui/core/Checkbox';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import TextField from '@material-ui/core/TextField';
import React from 'react';
import { Controller, useFormContext } from 'react-hook-form';

const SettingTab = () => {
	const methods = useFormContext();
	const { control, formState, watch } = methods;
	const { errors } = formState;

	return (
		<div>
			<Controller
				name="barcode"
				control={control}
				render={({ field }) => (
					<TextField
						{...field}
						className="mt-8 mb-16"
						error={!!errors?.name}
						required
						InputLabelProps={{ shrink: true }}
						helperText={errors?.bar_code?.message}
						label="Bar Code"
						autoFocus
						id="barcode"
						variant="outlined"
						fullWidth
					/>
				)}
			/>
			<Controller
				name="is_flash_deal"
				control={control}
				render={({ field }) => (
					<FormControl>
						<FormControlLabel
							label="Is Flash Deal?"
							control={<Checkbox {...field} checked={field.value ? field.value : false} />}
						/>
					</FormControl>
				)}
			/>
			<br />
			<Controller
				name="is_new_arrival"
				control={control}
				render={({ field }) => (
					<FormControl>
						<FormControlLabel
							label="Is New Arrival?"
							control={<Checkbox {...field} checked={field.value ? field.value : false} />}
						/>
					</FormControl>
				)}
			/>
			<br />
			<Controller
				name="is_under_discount"
				control={control}
				render={({ field }) => (
					<FormControl>
						<FormControlLabel
							label="Is Under Discount?"
							control={<Checkbox {...field} checked={field.value ? field.value : false} />}
						/>
					</FormControl>
				)}
			/>
			<br />
			<Controller
				name="disable_buy_button"
				control={control}
				render={({ field }) => (
					<FormControl>
						<FormControlLabel
							label="Disable buy button?"
							control={<Checkbox {...field} checked={field.value ? field.value : false} />}
						/>
					</FormControl>
				)}
			/>
		</div>
	);
};

export default SettingTab;
