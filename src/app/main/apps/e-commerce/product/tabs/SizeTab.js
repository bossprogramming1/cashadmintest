import { yupResolver } from '@hookform/resolvers/yup';
import { TextField } from '@material-ui/core';
import { Autocomplete } from '@material-ui/lab';
import { CREATE_PRODUCT_SIZE, GET_PRODUCT_SIZE, GET_PRODUCT_SIZE_BY_ID } from 'app/constant/constants';
import React, { useEffect, useState } from 'react';
import { Controller, useForm } from 'react-hook-form';
import { useDispatch, useSelector } from 'react-redux';
import { useParams } from 'react-router-dom';
import * as yup from 'yup';
import { getAllSize } from '../../store/productDataSlice';
import ProductSizeTable from './ProductSizeTable';

//validation
const schema = yup.object().shape({
	size: yup.string().required('Size is required')
});
const SizeTab = props => {
	const { productId } = props;
	const routeParams = useParams();
	const dispatch = useDispatch();

	const {
		handleSubmit,
		control,
		reset,
		formState: { errors }
	} = useForm({
		mode: 'onChange',
		defaultValues: {},
		resolver: yupResolver(schema)
	});
	const [data, setData] = useState([]);
	const [productSize, setProductSize] = useState();
	const sizes = useSelector(({ eCommerceApp }) => eCommerceApp.productData.sizes);

	//form data submit
	const onSubmit = async (sizeData, e) => {
		const colorData = {
			size: parseInt(sizeData.size),
			product: parseInt(productId)
		};
		reset();

		const authTOKEN = {
			method: 'POST',
			headers: {
				'Content-type': 'application/json',
				Authorization: localStorage.getItem('jwt_access_token')
			},
			body: JSON.stringify(colorData)
		};

		fetch(CREATE_PRODUCT_SIZE, authTOKEN)
			.then(response => response.json())
			.then(responseData => getProductSizeData());
	};

	//get Product colorData
	useEffect(() => {
		getProductSizeData();
		dispatch(getAllSize());
	}, []);

	const getProductSizeData = () => {
		fetch(`${GET_PRODUCT_SIZE_BY_ID}/${productId}`, {
			headers: {
				'content-type': 'application/json',
				Authorization: localStorage.getItem('jwt_access_token')
			}
		})
			.then(response => response.json())
			.then(getSizeData => {
				setData(getSizeData?.size);
				setProductSize(getSizeData?.id);
			});
	};

	return (
		<div>
			<form onSubmit={handleSubmit(onSubmit)}>
				<Controller
					name="size"
					control={control}
					render={({ field: { onChange, value, name } }) => (
						<Autocomplete
							className="mt-8 mb-16"
							freeSolo
							value={value ? sizes.find(data => data.id === value) : null}
							options={sizes}
							getOptionLabel={option => `${option?.name}`}
							onChange={(event, newValue) => {
								onChange(newValue?.id);
							}}
							renderInput={params => (
								<TextField
									{...params}
									placeholder="Select Size"
									label="Select Size"
									error={!!errors?.size}
									helperText={errors?.size?.message}
									variant="outlined"
									autoFocus
									InputLabelProps={{
										shrink: true
									}}
								/>
							)}
						/>
					)}
				/>
				<input
					className="mb-16"
					type="submit"
					style={{
						height: '50px',
						width: '100px',
						backgroundColor: 'green',
						color: 'white',
						borderRadius: '5%',
						cursor: 'pointer'
					}}
				/>
			</form>
			{data?.length !== 0 && (
				<ProductSizeTable
					data={data}
					getProductSizeData={getProductSizeData}
					productId={productId}
					productSizeId={productSize}
				/>
			)}
		</div>
	);
};

export default SizeTab;
