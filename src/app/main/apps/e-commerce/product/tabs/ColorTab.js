import { yupResolver } from '@hookform/resolvers/yup';
import { TextField } from '@material-ui/core';
import React, { useEffect, useState } from 'react';
import { Controller, useForm } from 'react-hook-form';
//import { USERS_PASSWORDCHANGE } from '../../../../../constant/constants';
import { useHistory, useParams } from 'react-router-dom';
import * as yup from 'yup';
import { CREATE_PRODUCT_COLOR, GET_PRODUCT_COLORS_BY_ID } from 'app/constant/constants';
import ProductColorTable from './ProductColorTable';

//validation
const schema = yup.object().shape({
	name: yup.string().required('Name is required')
});
const ColorTab = props => {
	const { productId } = props;
	const routeParams = useParams();
	const { userId } = routeParams;
	const history = useHistory();
	const {
		handleSubmit,
		control,
		reset,
		formState: { errors }
	} = useForm({
		mode: 'onChange',
		defaultValues: {},
		resolver: yupResolver(schema)
	});
	const [text, settext] = useState('');
	const [data, setData] = useState([]);

	//form data submit
	const onSubmit = async (ColorData, e) => {
		const colorData = {
			name: ColorData?.name,
			product: productId
		};
		reset();
		const authTOKEN = {
			method: 'POST',
			headers: {
				'Content-type': 'application/json',
				Authorization: localStorage.getItem('jwt_access_token')
			},
			body: JSON.stringify(colorData)
		};

		fetch(CREATE_PRODUCT_COLOR, authTOKEN)
			.then(response => response.json())
			.then(colorsData => getProductColorData());
	};

	//get Product colorData
	useEffect(() => {
		getProductColorData();
	}, []);

	const getProductColorData = () => {
		fetch(GET_PRODUCT_COLORS_BY_ID + productId, {
			headers: {
				'content-type': 'application/json',
				Authorization: localStorage.getItem('jwt_access_token')
			}
		})
			.then(response => response.json())
			.then(allColors => setData(allColors?.product_colors));
	};

	return (
		<div>
			<form onSubmit={handleSubmit(onSubmit)}>
				<Controller
					name="name"
					control={control}
					render={({ field }) => (
						<TextField
							{...field}
							className="mt-8 mb-16"
							error={!!errors?.name}
							required
							helperText={errors?.name?.message}
							label="Color Name"
							id="name"
							variant="outlined"
							fullWidth
							InputLabelProps={{ shrink: true }}
						/>
					)}
				/>
				<input
					className="mb-16"
					type="submit"
					style={{
						height: '50px',
						width: '100px',
						backgroundColor: 'green',
						color: 'white',
						borderRadius: '5%',
						cursor: 'pointer'
					}}
				/>
			</form>
			{data?.length !== 0 && <ProductColorTable data={data} getProductColorData={getProductColorData} />}
		</div>
	);
};

export default ColorTab;
