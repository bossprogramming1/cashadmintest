/* eslint-disable import/order */
import Checkbox from '@material-ui/core/Checkbox';
import FormControl from '@material-ui/core/FormControl';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Icon from '@material-ui/core/Icon';
import InputAdornment from '@material-ui/core/InputAdornment';
import TextField from '@material-ui/core/TextField';
import { Autocomplete } from '@material-ui/lab';
import clsx from 'clsx';
import { useEffect, useRef, useState, useContext } from 'react';
import { Controller, useFormContext } from 'react-hook-form';
import { useDispatch, useSelector } from 'react-redux';
import { EditorState, convertToRaw, convertFromRaw } from 'draft-js';
// import { Editor } from 'react-draft-wysiwyg';
import draftToHtml from 'draftjs-to-html';
import { convertToHTML, convertFromHTML } from 'draft-convert';

import {
	getAttributeSet,
	getBrand,
	getCategory,
	getManufacturer,
	getVendor,
	getVerifiedBy
} from '../../store/productDataSlice';
import '../../../../../../../node_modules/react-draft-wysiwyg/dist/react-draft-wysiwyg.css';
import EditorContext from '../EditorContext';
import { BASE_URL } from 'app/constant/constants';
import { Editor } from '@tinymce/tinymce-react';
import { Typography } from '@material-ui/core';
import { condition } from 'app/@data/data';

function BasicInfoTab(props) {
	const product = useSelector(({ eCommerceApp }) => eCommerceApp.product);
	const [editorText, setEditorText] = useContext(EditorContext);
	const editorRefFullDes = useRef(null);
	const editorRefShortDes = useRef(null);
	const editorRefFeatures = useRef(null);

	const fullDesContent = () => {
		if (editorRefFullDes.current) {
			editorRefFullDes.current.getContent();
			setValue('full_desc', editorRefFullDes.current.getContent());
		}
	};
	const shortDesContent = () => {
		if (editorRefShortDes.current) {
			editorRefShortDes.current.getContent();
			setValue('short_desc', editorRefShortDes.current.getContent());
		}
	};
	const featureContent = () => {
		if (editorRefFeatures.current) {
			editorRefFeatures.current.getContent();
			setValue('features', editorRefFeatures.current.getContent());
		}
	};

	const methods = useFormContext();
	const { control, formState, watch, setValue } = methods;
	const { errors } = formState;
	const dispatch = useDispatch();
	const thumbnail = watch('thumbnail');

	const [previewImage, setPreviewImage] = useState();
	const parentIdRef = useRef(null);

	useEffect(() => {
		dispatch(getBrand());
		dispatch(getCategory());
		dispatch(getAttributeSet());
		dispatch(getManufacturer());
		dispatch(getVendor());
		dispatch(getVerifiedBy());
	}, []);

	const brands = useSelector(({ eCommerceApp }) => eCommerceApp.productData.Brands);
	const categories = useSelector(({ eCommerceApp }) => eCommerceApp.productData.Categories);
	const manufacturers = useSelector(({ eCommerceApp }) => eCommerceApp.productData.Manufacturers);
	const vendors = useSelector(({ eCommerceApp }) => eCommerceApp.productData.Vendors);
	const employees = useSelector(({ eCommerceApp }) => eCommerceApp.productData.Verified_by);

	const [editorState, setEditorState] = useState(EditorState.createEmpty());
	const [rawContent, setRawContent] = useState('');

	const onEditorStateChange = editorStatee => {
		const edit = {
			fullDescription: draftToHtml(convertToRaw(editorStatee.getCurrentContent())),

			editorStat: editorStatee
		};

		setEditorText(edit);
	};
	return (
		<div>
			<Controller
				name="name"
				control={control}
				render={({ field }) => (
					<TextField
						{...field}
						className="mt-8 mb-16"
						error={!!errors?.name}
						required
						InputLabelProps={{ shrink: true }}
						helperText={errors?.name?.message}
						label="Name"
						autoFocus
						id="name"
						variant="outlined"
						fullWidth
					/>
				)}
			/>
			<Typography style={{ fontSize: '10px' }} variant="p" component="p">
				Short Description
			</Typography>
			<Controller
				name="short_desc"
				control={control}
				render={({ field }) => (
					<Editor
						value={field.value}
						onInit={(evt, editor) => (editorRefShortDes.current = editor)}
						apiKey={'7csv5zhqne9pa8m8xmmbnyizrc9ytluyamh2x6kg9voi1o72'}
						placeholder="Full description"
						onEditorChange={shortDesContent}
						id="short_desc"
						init={{
							height: 200,
							placeholder: 'Short Description',

							plugins: [
								'advlist autolink lists link image charmap print preview anchor',
								'searchreplace visualblocks code fullscreen',
								'insertdatetime media table paste code help wordcount'
							],
							toolbar:
								'undo redo | formatselect | ' +
								'bold italic backcolor | alignleft aligncenter ' +
								'alignright alignjustify | bullist numlist outdent indent | ' +
								'removeformat | help',
							content_style: 'body { font-family:Helvetica,Arial,sans-serif; font-size:14px }'
						}}
					/>
				)}
			/>
			<br />
			<Typography style={{ fontSize: '10px' }} variant="p" component="p">
				Full Description
			</Typography>
			<Controller
				name="full_desc"
				control={control}
				render={({ field }) => (
					<Editor
						value={field.value}
						onInit={(evt, editor) => (editorRefFullDes.current = editor)}
						apiKey={'7csv5zhqne9pa8m8xmmbnyizrc9ytluyamh2x6kg9voi1o72'}
						placeholder="Full description"
						onEditorChange={fullDesContent}
						id="full_desc"
						init={{
							height: 200,
							placeholder: 'Full Description',

							plugins: [
								'advlist autolink lists link image charmap print preview anchor',
								'searchreplace visualblocks code fullscreen',
								'insertdatetime media table paste code help wordcount'
							],
							toolbar:
								'undo redo | formatselect | ' +
								'bold italic backcolor | alignleft aligncenter ' +
								'alignright alignjustify | bullist numlist outdent indent | ' +
								'removeformat | help',
							content_style: 'body { font-family:Helvetica,Arial,sans-serif; font-size:14px }'
						}}
					/>
				)}
			/>
			<br />
			<Typography style={{ fontSize: '10px' }} variant="p" component="p">
				Specification
			</Typography>
			<Controller
				name="features"
				control={control}
				render={({ field }) => (
					<Editor
						value={field.value}
						onInit={(evt, editor) => (editorRefFeatures.current = editor)}
						apiKey={'7csv5zhqne9pa8m8xmmbnyizrc9ytluyamh2x6kg9voi1o72'}
						placeholder="Features"
						onEditorChange={featureContent}
						id="features"
						init={{
							height: 200,
							placeholder: 'Specification',

							plugins: [
								'advlist autolink lists link image charmap print preview anchor',
								'searchreplace visualblocks code fullscreen',
								'insertdatetime media table paste code help wordcount'
							],
							toolbar:
								'undo redo | formatselect | ' +
								'bold italic backcolor | alignleft aligncenter ' +
								'alignright alignjustify | bullist numlist outdent indent | ' +
								'removeformat | help',
							content_style: 'body { font-family:Helvetica,Arial,sans-serif; font-size:14px }'
						}}
					/>
				)}
			/>
			<br />
			<Controller
				name="gtin"
				control={control}
				render={({ field }) => (
					<TextField
						{...field}
						className="mt-8 mb-16"
						id="gtin"
						label="Gtin"
						InputLabelProps={{ shrink: true }}
						variant="outlined"
						fullWidth
					/>
				)}
			/>

			<Controller
				name="brand"
				control={control}
				render={({ field: { onChange, value } }) => {
					return (
						<Autocomplete
							freeSolo
							value={value ? brands.find(brand => brand.id === value) : null}
							options={brands}
							getOptionLabel={option => `${option?.name}`}
							onChange={(event, newValue) => {
								onChange(newValue?.id);
							}}
							renderInput={params => (
								<TextField
									{...params}
									className="mt-8 mb-16"
									error={!!errors.brand}
									required
									InputLabelProps={{ shrink: true }}
									helperText={errors?.brand?.message}
									label="Brand"
									id="brand"
									variant="outlined"
								/>
							)}
						/>
					);
				}}
			/>
			<Controller
				name="category"
				control={control}
				render={({ field: { onChange, value } }) => (
					<Autocomplete
						freeSolo
						value={value ? categories?.find(category => category?.id === value) : null}
						options={categories}
						getOptionLabel={option => {
							parentIdRef.current = option?.parent;
							const parent = [];
							let parentstr = '';
							for (let i = 0; i < categories?.length; i++) {
								parent.push(
									`${
										option.parent
											? categories.find(data => data.id === parentIdRef.current)?.name
											: null
									}`
								);
								parentIdRef.current = option.parent
									? categories.find(data => data.id === parentIdRef.current)?.parent
									: null;
								parentIdRef.current ? null : (i = categories?.length);
							}
							parent.reverse();

							for (let i = 0; i < parent?.length; i++) {
								parentstr += `${i !== 0 ? '>>' : ''}${parent[i]} `;
							}

							return option.parent ? `${parentstr}>> ${option?.name} ` : `${option?.name} `;
						}}
						onChange={(event, newValue) => {
							onChange(newValue?.id);
						}}
						renderInput={params => (
							<TextField
								{...params}
								className="mt-8 mb-16"
								error={!!errors.category}
								required
								InputLabelProps={{ shrink: true }}
								helperText={errors?.category?.message}
								label="Category"
								variant="outlined"
							/>
						)}
					/>
				)}
			/>
			<Controller
				name="manufacturer"
				control={control}
				render={({ field: { onChange, value } }) => {
					return (
						<Autocomplete
							freeSolo
							value={value ? manufacturers.find(manufacturer => manufacturer.id === value) : null}
							options={manufacturers}
							getOptionLabel={option => `${option?.name}`}
							onChange={(event, newValue) => {
								onChange(newValue?.id);
							}}
							renderInput={params => (
								<TextField
									{...params}
									className="mt-8 mb-16"
									error={!!errors.manufacturer}
									required
									InputLabelProps={{ shrink: true }}
									helperText={errors?.manufacturer?.message}
									label="Manufacturer"
									id="manufacturer"
									variant="outlined"
								/>
							)}
						/>
					);
				}}
			/>

			<Controller
				name="vendor"
				control={control}
				render={({ field: { onChange, value } }) => (
					<Autocomplete
						freeSolo
						value={value ? vendors.find(user => user.id === value) : null}
						options={vendors}
						getOptionLabel={option =>
							`${option?.first_name} ${option?.last_name} ${option.email ? `[${option.email}]` : ''}`
						}
						onChange={(event, newValue) => {
							onChange(newValue?.id);
						}}
						renderInput={params => (
							<TextField
								{...params}
								className="mt-8 mb-16"
								error={!!errors.vendor}
								InputLabelProps={{ shrink: true }}
								helperText={errors?.vendor?.message}
								required
								label="Vendor"
								id="vendor"
								variant="outlined"
							/>
						)}
					/>
				)}
			/>
			<Controller
				name="verified_by"
				control={control}
				render={({ field: { onChange, value } }) => (
					<Autocomplete
						className="mt-8 mb-16"
						freeSolo
						value={value ? employees.find(user => user.id === value) : null}
						options={employees}
						getOptionLabel={option => `${option.email} `}
						onChange={(event, newValue) => {
							onChange(newValue?.id);
						}}
						renderInput={params => (
							<TextField
								{...params}
								label="Verified By"
								variant="outlined"
								required
								id="verified_by"
								InputLabelProps={{
									shrink: true
								}}
							/>
						)}
					/>
				)}
			/>
			<Controller
				name="is_published"
				control={control}
				render={({ field }) => (
					<FormControl>
						<FormControlLabel
							label="Published"
							control={<Checkbox {...field} checked={field.value ? field.value : false} />}
						/>
					</FormControl>
				)}
			/>
			<Controller
				name="is_disabled"
				control={control}
				render={({ field }) => (
					<FormControl>
						<FormControlLabel
							label="Disabled"
							control={<Checkbox {...field} checked={field.value ? field.value : false} />}
						/>
					</FormControl>
				)}
			/>
			<Controller
				name="is_popular"
				control={control}
				render={({ field }) => (
					<FormControl>
						<FormControlLabel
							label="Popular"
							control={<Checkbox {...field} checked={field.value ? field.value : false} />}
						/>
					</FormControl>
				)}
			/>
			<Controller
				name="is_featured"
				control={control}
				render={({ field }) => (
					<FormControl>
						<FormControlLabel
							label="Featured"
							control={<Checkbox {...field} checked={field.value ? field.value : false} />}
						/>
					</FormControl>
				)}
			/>
			<Controller
				name="is_verified"
				control={control}
				render={({ field }) => (
					<FormControl>
						<FormControlLabel
							label="Verified"
							control={<Checkbox {...field} checked={field.value ? field.value : false} />}
						/>
					</FormControl>
				)}
			/>
			<Controller
				name="rating"
				control={control}
				render={({ field }) => (
					<TextField
						{...field}
						className="mt-8 mb-16"
						label="Rating"
						id="rating"
						InputProps={{
							startAdornment: <InputAdornment position="start" />
						}}
						InputLabelProps={{ shrink: true }}
						type="number"
						variant="outlined"
						fullWidth
					/>
				)}
			/>
			<Controller
				name="num_reviews"
				control={control}
				render={({ field }) => (
					<TextField
						{...field}
						className="mt-8 mb-16"
						label="NumReviews"
						id="numReviews"
						InputProps={{
							startAdornment: <InputAdornment position="start" />
						}}
						InputLabelProps={{ shrink: true }}
						type="number"
						variant="outlined"
						fullWidth
					/>
				)}
			/>
			<Controller
				name="show_on_homepage"
				control={control}
				render={({ field }) => (
					<FormControl>
						<FormControlLabel
							label="Show on homepage"
							control={<Checkbox {...field} checked={field.value ? field.value : false} />}
						/>
					</FormControl>
				)}
			/>
			<Controller
				name="display_order"
				control={control}
				render={({ field }) => (
					<TextField
						{...field}
						className="mt-8 mb-16"
						label="Display Order"
						id="display_order"
						InputProps={{
							startAdornment: <InputAdornment position="start" />
						}}
						InputLabelProps={{ shrink: true }}
						type="number"
						variant="outlined"
						fullWidth
					/>
				)}
			/>
			<Controller
				name="condition"
				control={control}
				render={({ field: { onChange, value } }) => (
					<Autocomplete
						className="mt-8 mb-16"
						freeSolo
						value={value ? condition.find(user => user.id === value) : null}
						options={condition}
						getOptionLabel={option => `${option.name} `}
						onChange={(event, newValue) => {
							onChange(newValue?.id);
						}}
						renderInput={params => (
							<TextField
								{...params}
								label="Condition"
								variant="outlined"
								required
								id="condition"
								InputLabelProps={{
									shrink: true
								}}
							/>
						)}
					/>
				)}
			/>
			<Controller
				name="allow_customer_review"
				control={control}
				render={({ field }) => (
					<FormControl>
						<FormControlLabel
							label="Allow customer review"
							control={<Checkbox {...field} checked={field.value ? field.value : false} />}
						/>
					</FormControl>
				)}
			/>
			<Controller
				name="mark_as_new"
				control={control}
				render={({ field }) => (
					<FormControl>
						<FormControlLabel
							label="Mark as new"
							control={<Checkbox {...field} checked={field.value ? field.value : false} />}
						/>
					</FormControl>
				)}
			/>
			<Controller
				name="disable_buy_button"
				control={control}
				render={({ field }) => (
					<FormControl>
						<FormControlLabel
							label="Disable buy button"
							control={<Checkbox {...field} checked={field.value ? field.value : false} />}
						/>
					</FormControl>
				)}
			/>
			<Controller
				name="disable_wishlist_button"
				control={control}
				render={({ field }) => (
					<FormControl>
						<FormControlLabel
							label="Disable Wishlist button"
							control={<Checkbox {...field} checked={field.value ? field.value : false} />}
						/>
					</FormControl>
				)}
			/>
			<Controller
				name="is_available_for_preorder"
				control={control}
				render={({ field }) => (
					<FormControl>
						<FormControlLabel
							label="Available for preorder"
							control={<Checkbox {...field} checked={field.value ? field.value : false} />}
						/>
					</FormControl>
				)}
			/>
			<Controller
				name="expire_info"
				control={control}
				render={({ field }) => (
					<TextField
						{...field}
						className="mt-8 mb-16"
						id="expire_info"
						label="Expire Info"
						variant="outlined"
						InputLabelProps={{ shrink: true }}
						fullWidth
						multiline
						rows={6}
					/>
				)}
			/>
			<Controller
				name="admin_comment"
				control={control}
				render={({ field }) => (
					<TextField
						{...field}
						className="mt-8 mb-16"
						id="admin_comment"
						label="Admin Comment"
						variant="outlined"
						InputLabelProps={{ shrink: true }}
						fullWidth
						multiline
						rows={6}
					/>
				)}
			/>

			<div className="flex justify-center sm:justify-start flex-wrap -mx-16">
				<Controller
					name="thumbnail"
					control={control}
					render={({ field: { onChange, value } }) => (
						<label
							htmlFor="button-file"
							className={clsx(
								'flex items-center justify-center relative w-128 h-128 rounded-16 mx-12 mb-24 overflow-hidden cursor-pointer shadow hover:shadow-lg'
							)}
						>
							<input
								accept="image/*"
								className="hidden"
								id="button-file"
								type="file"
								onChange={async e => {
									const reader = new FileReader();
									reader.onload = () => {
										if (reader.readyState === 2) {
											setPreviewImage(reader?.result);
										}
									};
									reader.readAsDataURL(e.target.files[0]);

									const file = e.target.files[0];
									onChange(file);
								}}
							/>
							<Icon fontSize="large" color="action">
								cloud_upload
							</Icon>
						</label>
					)}
				/>
				{thumbnail && !previewImage && (
					<img src={`${BASE_URL}${thumbnail} `} style={{ width: '100px', height: '100px' }} alt="not found" />
				)}

				<div style={{ width: '100px', height: '100px' }}>
					<img src={previewImage} alt="notfound" />
				</div>
			</div>
		</div>
	);
}

export default BasicInfoTab;
