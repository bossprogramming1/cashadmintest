import InputAdornment from '@material-ui/core/InputAdornment';
import TextField from '@material-ui/core/TextField';
import { Controller, useFormContext } from 'react-hook-form';

function PricingTab(props) {
	const methods = useFormContext();
	const { control } = methods;

	return (
		<div>
			<Controller
				name="old_price"
				control={control}
				render={({ field }) => (
					<TextField
						{...field}
						className="mt-8 mb-16"
						label="Old Price"
						id="old_price"
						InputProps={{
							startAdornment: <InputAdornment position="start">$</InputAdornment>
						}}
						variant="outlined"
						autoFocus
						fullWidth
					/>
				)}
			/>

			<Controller
				name="unit_price"
				control={control}
				render={({ field }) => (
					<TextField
						{...field}
						className="mt-8 mb-16"
						label="Unit Price"
						id="unit_price"
						required
						InputProps={{
							startAdornment: <InputAdornment position="start">$</InputAdornment>
						}}
						variant="outlined"
						fullWidth
					/>
				)}
			/>
		</div>
	);
}

export default PricingTab;
