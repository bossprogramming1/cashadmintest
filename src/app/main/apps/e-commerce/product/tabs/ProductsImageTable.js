import FuseScrollbars from '@fuse/core/FuseScrollbars';
import _ from '@lodash';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TablePagination from '@material-ui/core/TablePagination';
import TableRow from '@material-ui/core/TableRow';
import DeleteIcon from '@material-ui/icons/Delete';
import { useState } from 'react';
import { useDispatch } from 'react-redux';
import * as React from 'react';

import Box from '@mui/material/Box';
import Modal from '@mui/material/Modal';
import Button from '@mui/material/Button';
import { makeStyles, styled, useTheme } from '@material-ui/core/styles';
import { BASE_URL } from 'app/constant/constants';
import ProductImageTableHead from './ProductImageTableHead';
import { removeProductImage } from '../../store/productSlice';

const useStyles = makeStyles(theme => ({
	hover: {
		background: 'none',
		color: 'black',
		fontWeight: 600,

		'&:hover': {
			color: 'white',
			fontWeight: 600,
			background: '#57c190'
		}
	}
}));

function ProductsImageTable(props) {
	const { getProductImages } = props;
	const dispatch = useDispatch();

	const [selected, setSelected] = useState([]);

	let serialNumber = 1;
	const [page, setPage] = useState(0);
	const [rowsPerPage, setRowsPerPage] = useState(10);
	const [order, setOrder] = useState({
		direction: 'asc',
		id: null
	});

	const classes = useStyles(props);
	const theme = useTheme();

	function handleRequestSort(event, property) {
		const id = property;
		let direction = 'desc';

		if (order.id === property && order.direction === 'desc') {
			direction = 'asc';
		}

		setOrder({
			direction,
			id
		});
	}

	function handleDeselect() {
		setSelected([]);
	}

	function handleClick(item) {
		props.history.push(`/apps/e-commerce/products/${item.id}/${item.handle}`);
	}

	function handleCheck(event, id) {
		const selectedIndex = selected.indexOf(id);
		let newSelected = [];

		if (selectedIndex === -1) {
			newSelected = newSelected.concat(selected, id);
		} else if (selectedIndex === 0) {
			newSelected = newSelected.concat(selected.slice(1));
		} else if (selectedIndex === selected.length - 1) {
			newSelected = newSelected.concat(selected.slice(0, -1));
		} else if (selectedIndex > 0) {
			newSelected = newSelected.concat(selected.slice(0, selectedIndex), selected.slice(selectedIndex + 1));
		}

		setSelected(newSelected);
	}

	function handleChangePage(event, value) {
		setPage(value);
	}

	function handleChangeRowsPerPage(event) {
		setRowsPerPage(event.target.value);
	}

	function handleRemoveProduct(item) {
		dispatch(removeProductImage(item.id)).then(action => {
			getProductImages();
			handleClose();
		});
	}
	// for Modal
	const style = {
		position: 'absolute',
		display: 'flex',
		alignItems: 'center',
		justifyContent: 'space-between',
		flexDirection: 'column',
		top: '50%',
		left: '50%',
		transform: 'translate(-50%, -50%)',
		width: 400,
		height: 150,
		bgcolor: 'background.paper',
		border: '2px solid #000',
		boxShadow: 24,
		textAlign: 'center',
		paddingTop: '50px',
		pt: 2,
		px: 4,
		pb: 3
	};
	const [open, setOpen] = React.useState(false);
	const [imgID, setImgId] = React.useState(0);
	const handleOpen = id => {
		setOpen(true);
		setImgId(id);
	};
	const handleClose = () => {
		setOpen(false);
		setImgId(0);
	};

	const user_role = localStorage.getItem('user_role');

	return (
		<div className="w-full flex flex-col">
			<FuseScrollbars className="flex-grow overflow-x-auto">
				<Modal
					open={open}
					onClose={handleClose}
					aria-labelledby="parent-modal-title"
					aria-describedby="parent-modal-description"
				>
					<Box sx={{ ...style, width: 400 }}>
						<h2 id="parent-modal-title">Are you want to delete?</h2>

						<Box>
							<Button
								onClick={event => handleRemoveProduct(imgID, 'deleteProduct')}
								style={{
									color: 'white',
									fontWeight: 600,
									background: '#57c190',
									marginRight: '20px'
								}}
							>
								Yes
							</Button>
							<Button
								style={{
									color: 'white',
									fontWeight: 600,
									background: '#57c190'
								}}
								onClick={handleClose}
							>
								No
							</Button>
						</Box>
					</Box>
				</Modal>
				<Table stickyHeader className="min-w-xl" aria-labelledby="tableTitle">
					<ProductImageTableHead
						order={order}
						onRequestSort={handleRequestSort}
						onMenuItemClick={handleDeselect}
					/>

					<TableBody>
						{_.orderBy(
							props.productImages,
							[
								o => {
									switch (order.id) {
										case 'categories': {
											return o.categories[0];
										}
										default: {
											return o[order.id];
										}
									}
								}
							],
							[order.direction]
						)
							.slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
							.map(n => {
								const isSelected = selected.indexOf(n.id) !== -1;
								return (
									<TableRow
										hover
										role="checkbox"
										aria-checked={isSelected}
										tabIndex={-1}
										key={n.id}
										selected={isSelected}
									>
										<TableCell className="whitespace-nowrap p-4 md:p-16" component="th" scope="row">
											{serialNumber++}
										</TableCell>

										<TableCell
											className="w-52 px-4 md:px-0"
											component="th"
											scope="row"
											padding="none"
										>
											{n.length > 0 && n.featuredImageId ? (
												<img
													className="w-full block rounded"
													src={_.find(n.thumbnail, { id: n.featuredImageId }).url}
													alt={n.id}
													style={{ width: '100px', height: '100px' }}
												/>
											) : (
												<img
													style={{ width: '100px', height: '100px' }}
													className="w-full block rounded"
													src={`${BASE_URL}${n.image}`}
													alt={n?.name}
												/>
											)}
										</TableCell>
										<TableCell className="whitespace-nowrap p-4 md:p-16" component="th" scope="row">
											{n?.color?.name || 'N/A'}
										</TableCell>

										<TableCell className="whitespace-nowrap p-4 md:p-16" component="th" scope="row">
											<Box>
												<DeleteIcon
													className="h-52 cursor-pointer"
													style={{
														color: 'red',
														visibility:
															user_role === 'ADMIN' || user_role === 'admin'
																? 'visible'
																: 'hidden'
													}}
													onClick={() => handleOpen(n)}
												/>
											</Box>
										</TableCell>
									</TableRow>
								);
							})}
					</TableBody>
				</Table>
			</FuseScrollbars>

			<TablePagination
				className="flex-shrink-0 border-t-1"
				component="div"
				rowsPerPage={rowsPerPage}
				page={page}
				backIconButtonProps={{
					'aria-label': 'Previous Page'
				}}
				nextIconButtonProps={{
					'aria-label': 'Next Page'
				}}
				onChangePage={handleChangePage}
				onChangeRowsPerPage={handleChangeRowsPerPage}
			/>
		</div>
	);
}

export default ProductsImageTable;
