import Box from '@material-ui/core/Box';
import TextField from '@material-ui/core/TextField';
import { Autocomplete } from '@material-ui/lab';
import React, { useEffect } from 'react';
import { Controller, useFormContext } from 'react-hook-form';
import { useDispatch, useSelector } from 'react-redux';
import { getAttributeSet } from '../../store/productDataSlice';

const AttributeSetTab = () => {
	const methods = useFormContext();
	const { control } = methods;
	const dispatch = useDispatch();

	useEffect(() => {
		dispatch(getAttributeSet());
	}, []);

	const attributeSet = useSelector(({ eCommerceApp }) => eCommerceApp.productData.Attribute_set);
	return (
		<Box>
			<Controller
				name="attribute_set"
				control={control}
				render={({ field: { onChange, value } }) => (
					<Autocomplete
						className="mt-8 mb-16"
						freeSolo
						value={value ? attributeSet.find(attribute => attribute.id === value) : null}
						options={attributeSet}
						getOptionLabel={option => `${option?.name}`}
						onChange={(event, newValue) => {
							onChange(newValue?.id);
						}}
						renderInput={params => (
							<TextField
								{...params}
								label="Attribute set"
								variant="outlined"
								InputLabelProps={{
									shrink: true
								}}
							/>
						)}
					/>
				)}
			/>
		</Box>
	);
};

export default AttributeSetTab;
