import FuseScrollbars from '@fuse/core/FuseScrollbars';
import _ from '@lodash';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TablePagination from '@material-ui/core/TablePagination';
import TableRow from '@material-ui/core/TableRow';
import DeleteIcon from '@material-ui/icons/Delete';
import { DELETE_PRODUCT_COLOR } from 'app/constant/constants';
import { useState } from 'react';
import { useDispatch } from 'react-redux';
import ProductColorTableHead from './ProductColorTableHead';

function ProductColorTable(props) {
	const colorData = props.data;
	const allProductColorData = props.getProductColorData;

	const dispatch = useDispatch();

	const [selected, setSelected] = useState([]);
	let serialNumber = 1;
	const [page, setPage] = useState(0);
	const [rowsPerPage, setRowsPerPage] = useState(10);
	const [order, setOrder] = useState({
		direction: 'asc',
		id: null
	});

	function handleRequestSort(event, property) {
		const id = property;
		let direction = 'desc';

		if (order.id === property && order.direction === 'desc') {
			direction = 'asc';
		}

		setOrder({
			direction,
			id
		});
	}

	function handleDeselect() {
		setSelected([]);
	}

	function handleClick(item) {
		props.history.push(`/apps/e-commerce/products/${item.id}/${item.handle}`);
	}

	function handleCheck(event, id) {
		const selectedIndex = selected.indexOf(id);
		let newSelected = [];

		if (selectedIndex === -1) {
			newSelected = newSelected.concat(selected, id);
		} else if (selectedIndex === 0) {
			newSelected = newSelected.concat(selected.slice(1));
		} else if (selectedIndex === selected.length - 1) {
			newSelected = newSelected.concat(selected.slice(0, -1));
		} else if (selectedIndex > 0) {
			newSelected = newSelected.concat(selected.slice(0, selectedIndex), selected.slice(selectedIndex + 1));
		}

		setSelected(newSelected);
	}

	function handleChangePage(event, value) {
		setPage(value);
	}

	function handleChangeRowsPerPage(event) {
		setRowsPerPage(event.target.value);
	}

	function handleRemoveProductColor(item) {
		fetch(`${DELETE_PRODUCT_COLOR}${item.id}`, {
			method: 'DELETE',
			headers: {
				'content-type': 'application/json',
				Authorization: localStorage.getItem('jwt_access_token')
			}
		})
			.then(response => response.json())
			.then(data => allProductColorData());
	}

	const user_role = localStorage.getItem('user_role');

	return (
		<div className="flex flex-col">
			<FuseScrollbars className="flex-grow overflow-x-auto">
				<Table width={200} stickyHeader className="min-w-xl" aria-labelledby="tableTitle">
					<ProductColorTableHead
						order={order}
						onRequestSort={handleRequestSort}
						onMenuItemClick={handleDeselect}
					/>

					<TableBody>
						{_.orderBy(
							colorData,
							[
								o => {
									switch (order.id) {
										case 'categories': {
											return o.categories[0];
										}
										default: {
											return o[order.id];
										}
									}
								}
							],
							[order.direction]
						)
							.slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
							.map(n => {
								const isSelected = selected.indexOf(n.id) !== -1;
								return (
									<TableRow
										hover
										role="checkbox"
										aria-checked={isSelected}
										tabIndex={-1}
										key={n.id}
										selected={isSelected}
									>
										<TableCell className="whitespace-nowrap p-4 md:p-16" component="th" scope="row">
											{serialNumber++}
										</TableCell>

										<TableCell className="whitespace-nowrap p-4 md:p-16" component="th" scope="row">
											{n?.name}
										</TableCell>

										<TableCell className="whitespace-nowrap p-4 md:p-16" component="th" scope="row">
											<div>
												<DeleteIcon
													className="h-52 cursor-pointer"
													style={{
														color: 'red',
														visibility:
															user_role === 'ADMIN' || user_role === 'admin'
																? 'visible'
																: 'hidden'
													}}
													onClick={event => handleRemoveProductColor(n)}
												/>
											</div>
										</TableCell>
									</TableRow>
								);
							})}
					</TableBody>
				</Table>
			</FuseScrollbars>

			<TablePagination
				className="flex-shrink-0 border-t-1"
				component="div"
				rowsPerPage={rowsPerPage}
				page={page}
				backIconButtonProps={{
					'aria-label': 'Previous Page'
				}}
				nextIconButtonProps={{
					'aria-label': 'Next Page'
				}}
				onChangePage={handleChangePage}
				onChangeRowsPerPage={handleChangeRowsPerPage}
			/>
		</div>
	);
}

export default ProductColorTable;
