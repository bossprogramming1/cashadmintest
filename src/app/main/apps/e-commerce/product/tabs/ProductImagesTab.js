import { TextField } from '@material-ui/core';
import { orange } from '@material-ui/core/colors';
import { makeStyles } from '@material-ui/core/styles';
import { Autocomplete } from '@material-ui/lab';
import { GET_COLOR_PRODUCT_ID, GET_PRODUCT_BY_ID } from 'app/constant/constants';
import Icon from '@material-ui/core/Icon';
import React, { useEffect, useState } from 'react';
import { Controller, useForm, useFormContext } from 'react-hook-form';
import { useDispatch, useSelector } from 'react-redux';
import { yupResolver } from '@hookform/resolvers/yup';
import * as yup from 'yup';
import clsx from 'clsx';
import { getAllColor } from '../../store/productDataSlice';
import { saveProductImage } from '../../store/productSlice';
import ProductsImageTable from './ProductsImageTable';

const useStyles = makeStyles(theme => ({
	productImageFeaturedStar: {
		position: 'absolute',
		top: 0,
		right: 0,
		color: orange[400],
		opacity: 0
	},
	productImageUpload: {
		transitionProperty: 'box-shadow',
		transitionDuration: theme.transitions.duration.short,
		transitionTimingFunction: theme.transitions.easing.easeInOut
	},
	productImageItem: {
		transitionProperty: 'box-shadow',
		transitionDuration: theme.transitions.duration.short,
		transitionTimingFunction: theme.transitions.easing.easeInOut,
		'&:hover': {
			'& $productImageFeaturedStar': {
				opacity: 0.8
			}
		},
		'&.featured': {
			pointerEvents: 'none',
			boxShadow: theme.shadows[3],
			'& $productImageFeaturedStar': {
				opacity: 1
			},
			'&:hover $productImageFeaturedStar': {
				opacity: 1
			}
		}
	}
}));
//validation
const schema = yup.object().shape({
	name: yup.string().required('Name is required')
});

function ProductImagesTab(props) {
	const dispatch = useDispatch();
	const classes = useStyles(props);
	const [previewImage, setPreviewImage] = useState();

	const {
		handleSubmit,
		control,
		reset,
		setValue,
		getValues,
		formState: { errors }
	} = useForm({
		mode: 'onChange',
		defaultValues: {},
		resolver: yupResolver(schema)
	});

	const [productImages, setProductImages] = useState([]);
	const [colors, setColors] = useState([]);

	//Get all product images
	useEffect(() => {
		getProductImagesAndColor();
		dispatch(getAllColor());
	}, []);

	const getProductImagesAndColor = () => {
		const authTOKEN = {
			headers: {
				'Content-type': 'application/json',
				Authorization: localStorage.getItem('jwt_access_token')
			}
		};
		fetch(`${GET_PRODUCT_BY_ID}${props.productId}`, authTOKEN)
			.then(res => res.json())
			.then(data => {
				setProductImages(data.product_images);
			});
		fetch(`${GET_COLOR_PRODUCT_ID}${props.productId}`, authTOKEN)
			.then(res => res.json())
			.then(data => {
				setColors(data);
			});
	};

	const onSubmit = async (ColorData, e) => {
		const { color, image } = getValues();
		dispatch(saveProductImage({ color, image, product: props.productId })).then(
			() => getProductImagesAndColor(),
			setPreviewImage('')
		);
	};

	return (
		<div>
			<div>
				<form onSubmit={handleSubmit(onSubmit)}>
					<Controller
						name="color"
						control={control}
						render={({ field: { onChange, value, name } }) => (
							<Autocomplete
								className="mt-8 mb-16"
								freeSolo
								value={value ? colors.find(data => data.id === value) : null}
								options={colors}
								getOptionLabel={option => `${option?.name}`}
								onChange={(event, newValue) => {
									onChange(newValue?.id);
								}}
								renderInput={params => (
									<TextField
										{...params}
										placeholder="Select Color"
										label="Select Color"
										error={!!errors?.color}
										helperText={errors?.color?.message}
										variant="outlined"
										autoFocus
										InputLabelProps={{
											shrink: true
										}}
									/>
								)}
							/>
						)}
					/>
					<div style={{ display: 'flex' }}>
						<Controller
							name="image"
							control={control}
							render={({ field: { onChange, value } }) => (
								<input
									type="file"
									id="file"
									name="image"
									style={{ marginBottom: '100px' }}
									onChange={async e => {
										const reader = new FileReader();
										reader.onload = () => {
											if (reader.readyState === 2) {
												setPreviewImage(reader.result);
											}
										};

										reader.readAsDataURL(e.target.files[0]);

										const file = e.target.files[0];
										onChange(file);
									}}
									multiple
								/>
							)}
						/>
						<div style={{ width: '100px', height: '100px' }}>
							<img src={previewImage} alt="Not Found" />
						</div>
					</div>

					<br />
					<input
						className="mb-16"
						type="submit"
						onClick={() => onSubmit()}
						style={{
							height: '50px',
							width: '100px',
							backgroundColor: 'green',
							color: 'white',
							borderRadius: '5%',
							cursor: 'pointer'
						}}
					/>
				</form>
			</div>
			{productImages?.length !== 0 ? (
				<ProductsImageTable productImages={productImages} getProductImages={getProductImagesAndColor} />
			) : null}
		</div>
	);
}

export default ProductImagesTab;
