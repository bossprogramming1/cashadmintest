import AnalyticsDashboardAppConfig from "./dashboards/analytics/AnalyticsDashboardAppConfig";
import ProjectDashboardAppConfig from "./dashboards/project/ProjectDashboardAppConfig";
import ECommerceAppConfig from "./e-commerce/ECommerceAppConfig";

const appsConfigs = [
	
	ECommerceAppConfig,
	AnalyticsDashboardAppConfig,
	ProjectDashboardAppConfig,
];

export default appsConfigs;
