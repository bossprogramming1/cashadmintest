import FusePageCarded from '@fuse/core/FusePageCarded';
import { ROLE_MENU_LIST } from 'app/constant/permission/permission';
import { getUserPermissions } from 'app/store/dataSlice';
import withReducer from 'app/store/withReducer';
import React, { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import PagenotFound from '../../Pagenotfound/PagenotFound';
import reducer from '../store/index';
import RoleMenusHeader from './RoleMenusHeader';
import RoleMenusTable from './RoleMenusTable';

const RoleMenus = () => {
	const UserPermissions = useSelector(state => state.data.UserPermissions);
	useEffect(() => {
		dispatch(getUserPermissions());
	}, []);
	const dispatch = useDispatch();
	return (
		<FusePageCarded
			classes={{
				content: 'flex',
				contentCard: 'overflow-hidden',
				header: 'min-h-74 h-64'
			}}
			header={UserPermissions.includes(ROLE_MENU_LIST) && <RoleMenusHeader />}
			content={UserPermissions.includes(ROLE_MENU_LIST) ? <RoleMenusTable /> : <PagenotFound />}
			innerScroll
		/>
	);
};
export default withReducer('roleMenusManagement', reducer)(RoleMenus);
