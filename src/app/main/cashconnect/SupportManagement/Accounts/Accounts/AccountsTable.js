import FuseScrollbars from '@fuse/core/FuseScrollbars';
import _ from '@lodash';
import clsx from 'clsx';
import Checkbox from '@material-ui/core/Checkbox';
import DeleteIcon from '@material-ui/icons/Delete';
import EditIcon from '@material-ui/icons/Edit';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TextField from '@material-ui/core/TextField';
import { Autocomplete } from '@material-ui/lab';
import { FormProvider, useForm, Controller, useFormContext } from 'react-hook-form';
import Typography from '@material-ui/core/Typography';
import Grid from '@material-ui/core/Grid';
import Modal from '@material-ui/core/Modal';
import Paper from '@material-ui/core/Paper';
import CloseIcon from '@mui/icons-material/Close';
import Box from '@material-ui/core/Box';
import ArrowForwardIcon from '@material-ui/icons/ArrowForward';
import { makeStyles, styled, useTheme } from '@material-ui/core/styles';
import TablePagination from '@material-ui/core/TablePagination';
import TableRow from '@material-ui/core/TableRow';
import { withRouter, Link } from 'react-router-dom';
import { useEffect, useState } from 'react';
import FuseLoading from '@fuse/core/FuseLoading';
import { useDispatch, useSelector } from 'react-redux';
import { format } from 'date-fns';
import { FormControl, FormControlLabel, Radio, RadioGroup } from '@material-ui/core';
import AccountsTableHead from './AccountsTableHead';
import { getAccounts, selectAccounts } from '../store/accountsSlice';
import { GET_TICKET_STATUS, UPDATE_TICKET_STATUS, UPDATE_TICKET, BASE_URL } from '../../../../../constant/constants';

const style = {
	position: 'absolute',
	top: '50%',
	left: '50%',
	transform: 'translate(-50%, -50%)',
	width: 400,
	bgcolor: 'background.paper',
	border: '2px solid #000',
	boxShadow: 24,
	p: 4
};

const useStyles = makeStyles(theme => ({
	hidden: {
		display: 'none'
	},
	tblContainer: {
		borderRadius: '0px'
	},
	table: {
		minWidth: 600
	},
	tableHead: {
		backgroundColor: theme.palette.primary[500]
	},
	dateOfSaleContainer: {
		display: 'flex',
		flexWrap: 'nowrap',
		justifyContent: 'space-between',
		minWidth: '250px'
	},
	dateOfSale: {
		width: '45px',
		background: theme.palette.background.paper,
		'& > div': {
			'&:before': {
				borderBottom: '0px solid black !important'
			}
		},
		'& input': {
			boxSizing: 'inherit',
			marginLeft: '-11px',
			marginTop: '2px'
		}
	},
	paytypeLabel: {
		'&& > label': {
			fontSize: '16px'
		}
	},
	paidAmount: {
		'&& > p': {
			marginTop: '-27px'
		}
	},
	modal: {
		position: 'relative',

		marginTop: '22%',
		marginLeft: '50%',
		transform: 'translate(-50%, -50%)',
		width: 350,
		height: 180,
		border: '2px solid #000',
		boxShadow: 24,
		p: 4,
		//color: theme.palette.background.paper,
		backgroundColor: theme.palette.background.paper
	},
	close: { position: 'absolute', top: '10', right: '10', color: 'red', fontSize: '20px' },
	paper: {
		marginTop: '3%',
		width: '100%',
		backgroundColor: theme.palette.grey,
		//border: `2px solid ${theme.palette.grey[800]}`,
		display: 'flex',
		justifyContent: 'space-between',
		alignItems: 'center',
		//boxShadow: theme.shadows[5],
		padding: theme.spacing(2, 4, 3),
		marginLeft: '20px',
		marginRight: '10px'
	},
	totalSummery: {
		display: 'flex',
		justifyContent: 'space-between'
	},
	underline: {
		borderBottom: '1px solid grey',
		marginTop: '4%',
		marginBottom: '1%'
	},
	textMarginBottom: {
		marginBottom: '2%',
		marginTop: '3%',
		marginLeft: '3%'
	},
	boxStyle: {
		display: 'flex',
		justifyContent: 'flex-start'
	}
}));

const AccountsTable = props => {
	const [ticketId, setTicketId] = useState('');
	const [customerId, setCustomerId] = useState('');

	//Modal
	const [open, setOpen] = useState(false);
	const handleOpen = (ticketIdx, customerId) => {
		setTicketId(ticketIdx);
		setCustomerId(customerId.created_by.id);

		setOpen(true);
	};
	const handleClose = () => setOpen(false);
	const methods = useForm({
		mode: 'onChange',
		defaultValues: {}
	});

	const { reset, watch, control, onChange, formState, getValues, setValue } = methods;
	const { errors } = formState;
	const classes = useStyles();
	const dispatch = useDispatch();
	const accounts = useSelector(selectAccounts);
	const searchText = useSelector(({ accountsManagement }) => accountsManagement.accounts.searchText);
	const [loading, setLoading] = useState(true);
	const [selected, setSelected] = useState([]);
	const [data, setData] = useState(accounts);
	const [page, setPage] = useState(0);
	const [rowsPerPage, setRowsPerPage] = useState(10);
	const [order, setOrder] = useState({
		direction: 'asc',
		id: null
	});
	const user_role = localStorage.getItem('user_role');
	//let serialNumber = 1;
	const [deleteItem, setDeleteItem] = useState('');
	useEffect(() => {
		dispatch(getAccounts()).then(() => setLoading(false));
	}, [dispatch]);

	useEffect(() => {
		if (searchText.length !== 0) {
			setData(_.filter(accounts, item => item?.name.toLowerCase().includes(searchText.toLowerCase())));
			setPage(0);
		} else {
			setData(accounts);
		}
	}, [accounts, searchText]);
	const [message, setMessage] = useState('');
	const onMessageHandle = event => {
		setMessage(event.target.value);
	};
	const [ticketStatus, setTicketStatus] = useState([]);
	const authTOKEN = {
		headers: {
			'Content-type': 'application/json',
			Authorization: localStorage.getItem('jwt_access_token')
		}
	};

	useEffect(() => {
		fetch(GET_TICKET_STATUS, authTOKEN)
			.then(res => res.json())
			.then(supportTicketStatus => setTicketStatus(supportTicketStatus?.ticket_statuses));
	}, []);

	function getTickets(n) {
		props.history.push(`/apps/account-management/${n.id}`);
	}

	//Change ticket status
	const handleChangeTicketStatus = () => {
		// const ticketStatuss = {
		// 	ticket_status: id,
		// 	user: customerId
		// };
		console.log('Support', getValues());
		const updateData = getValues();
		updateData.user = customerId;

		const authToken = {
			method: 'PUT',
			headers: {
				'Content-type': 'application/json',
				Authorization: localStorage.getItem('jwt_access_token')
			},
			body: JSON.stringify(updateData)
		};
		fetch(`${UPDATE_TICKET}${ticketId}`, authToken).then(() => {
			setOpen(false);
			dispatch(getAccounts()).then(() => setLoading(false));
		});
	};

	function handleRequestSort(supportEvent, property) {
		const id = property;
		let direction = 'desc';

		if (order.id === property && order.direction === 'desc') {
			direction = 'asc';
		}

		setOrder({
			direction,
			id
		});
	}

	function handleSelectAllClick(supportEvent) {
		if (supportEvent.target.checked) {
			setSelected(data.map(n => n.id));
			return;
		}
		setSelected([]);
	}

	function handleDeselect() {
		setSelected([]);
	}

	function handleUpdateTicketStatus(tickets, event) {
		localStorage.removeItem('deleteTicketEvent');
		localStorage.setItem('updateTicketStatus', event);
		props.history.push(`/apps/account-management/${tickets.id}`);
	}
	function handleDeleteTicket(tickets, event) {
		localStorage.removeItem('updateTicketStatus');
		localStorage.setItem('deleteTicketEvent', event);
		props.history.push(`/apps/account-management/${tickets.id}`);
	}

	function handleCheck(supportEvent, id) {
		const selectedIndex = selected.indexOf(id);
		let newSelected = [];

		if (selectedIndex === -1) {
			newSelected = newSelected.concat(selected, id);
		} else if (selectedIndex === 0) {
			newSelected = newSelected.concat(selected.slice(1));
		} else if (selectedIndex === selected.length - 1) {
			newSelected = newSelected.concat(selected.slice(0, -1));
		} else if (selectedIndex > 0) {
			newSelected = newSelected.concat(selected.slice(0, selectedIndex), selected.slice(selectedIndex + 1));
		}

		setSelected(newSelected);
	}

	function handleChangePage(supportEvent, value) {
		setPage(value);
	}

	function handleChangeRowsPerPage(supportEvent) {
		setRowsPerPage(supportEvent.target.value);
	}

	if (loading) {
		return <FuseLoading />;
	}
	return (
		<div className="w-full flex flex-col">
			<Modal
				open={open}
				// onClose={handleClose}
				aria-labelledby="modal-modal-title"
				aria-describedby="modal-modal-description"
			>
				<Box className={classes.modal}>
					<Box style={{ display: 'flex', justifyContent: 'flex-end', margin: '10px' }}>
						<CloseIcon
							style={{ fontSize: '20px', cursor: 'pointer' }}
							className={classes.close}
							onClick={handleClose}
						/>
					</Box>
					<Box style={{ display: 'flex', justifyContent: 'center', marginTop: '50px' }}>
						{/* {ticketStatus?.map(support => (
							<>
								<input
									style={{ margin: '5px' }}
									type="radio"
									id={support?.name}
									name="radio"
									value="30"
									onChange={() => handleChangeTicketStatus(support?.id)}
									checked={support?.id === ticketId} // Check if the id matches ticketId
								/>
								<label style={{ display: 'flex', alignItems: 'center' }} htmlFor={support?.name}>
									{support?.name === 'in_process'
										? 'In Process'
										: support?.name === 'closed'
										? 'Closed'
										: support?.name === 'open'
										? 'Open'
										: ' '}
								</label>
							</>
						))} */}
						<FormProvider {...methods}>
							<Controller
								name="ticket_status"
								control={control}
								render={({ field }) => (
									<FormControl component="fieldset">
										<RadioGroup
											row
											aria-label="position"
											name="position"
											defaultValue="top"
											onChange={event => {
												event.target.value;
												handleChangeTicketStatus();
											}}
										>
											<FormControlLabel
												{...field}
												value="open"
												control={
													<Radio
														checked={field.value === 'open' ? field.value : false}
														style={{ color: 'green' }}
													/>
												}
												label="Open"
											/>
											<FormControlLabel
												{...field}
												value="in_process"
												control={
													<Radio
														checked={field.value === 'in_process' ? field.value : false}
														style={{ color: '#22d3ee' }}
													/>
												}
												label="In Process"
											/>

											<FormControlLabel
												{...field}
												value="closed"
												control={
													<Radio
														checked={field.value === 'closed' ? field.value : false}
														style={{ color: 'red' }}
													/>
												}
												label="Closed"
											/>
										</RadioGroup>
									</FormControl>
								)}
							/>
						</FormProvider>
						{/* })} */}
					</Box>
				</Box>
			</Modal>
			<FuseScrollbars className="flex-grow overflow-x-auto">
				<Typography
					variant="h6"
					gutterBottom
					component="div"
					sx={{ m: 2 }}
					className={classes.textMarginBottom}
				>
					Support Ticket
				</Typography>

				{accounts?.map(support => (
					<Box>
						<Grid container xs={12} spacing={2}>
							<Paper
								className={classes.paper}
								//sx={{ maxHeight: 400, p: 4, borderRadius: 5 }}
							>
								<Box>
									{/* <Typography
                                        variant="h6"
                                        gutterBottom
                                        component="div"
                                    >
                                        {support?.ticket_department}
                                    </Typography> */}
									<div
										style={{
											display: 'flex',
											alignItems: 'center',
											marginBottom: '10px'
										}}
									>
										<img
											src={
												support?.user?.image
													? `${BASE_URL}${support?.user?.image}`
													: 'assets/images/logos/default.png'
											}
											style={{
												height: '50px',
												width: '50px',
												borderRadius: '50%',
												marginRight: '15px'
											}}
										></img>
										<div style={{ display: 'flex', justifyContent: 'space-evenly' }}>
											<Typography
												variant="p"
												gutterBottom
												component="div"
												style={{ marginRight: '5px' }}
											>
												{`${support?.user?.first_name} ${support?.user?.last_name}`}
											</Typography>
											<Typography
												variant="p"
												gutterBottom
												component="div"
												style={{ marginRight: '5px' }}
											>
												<span style={{ fontWeight: 700 }}>Subject: </span>
												{` ${support?.subject}`}
											</Typography>
										</div>
									</div>

									<Box className={classes.boxStyle}>
										<Typography
											variant="p"
											gutterBottom
											component="div"
											className={clsx(
												'inline text-12 font-semibold py-4 px-12 rounded-full truncate text-white',
												support?.ticket_status?.name == ('open' || 'Open')
													? 'bg-green-800'
													: support?.ticket_status?.name == ('closed' || 'Closed')
													? 'bg-red-800'
													: support?.ticket_status?.name ==
													  ('customer-reply' ||
															'Customer-reply' ||
															'Customer-Reply' ||
															'Customer Reply')
													? 'bg-blue-800'
													: support?.ticket_status?.name == ('answered' || 'Answered')
													? 'bg-purple-800'
													: support?.ticket_status?.name == ('in_process' || 'In process')
													? 'bg-orange-800'
													: ''
											)}
											style={{ marginRight: '5px' }}
										>
											{/* {_.isEmpty(ticketStatus) || ticketStatus.find(ticketSt => ticketSt?.name === support?.ticket_status)?.name} */}
											{support?.ticket_status?.name}
										</Typography>
										<Typography
											variant="p"
											gutterBottom
											component="div"
											className={clsx(
												'inline text-12 font-semibold py-4 px-12 rounded-full truncate text-white',
												support?.ticket_priority?.name === ('high' || 'High')
													? 'bg-green-400'
													: support?.ticket_priority?.name === ('normal' || 'Normal')
													? 'bg-orange-400'
													: support?.ticket_priority?.name === ('low' || 'Low')
													? 'bg-blue-400'
													: support?.ticket_priority?.name
													? 'bg-black'
													: ''
											)}
											style={{ marginRight: '5px' }}
										>
											{support?.ticket_priority?.name}
										</Typography>
										<Typography
											variant="p"
											gutterBottom
											component="div"
											style={{ color: 'grey', display: 'flex', alignItems: 'center' }}
										>
											{format(new Date(support?.created_at), 'MMM dd, yyyy')}
										</Typography>
									</Box>
								</Box>
								<div
									//className="flex items-center sm:mb-12"
									// component={Link}
									// role="button"
									// to="/apps/support-management/"
									color="inherit"
								>
									<EditIcon
										className="h-52 cursor-pointer"
										style={{ color: 'green' }}
										onClick={event => {
											handleOpen(support.id, support);
											setValue('ticket_status', support?.ticket_status?.name);
										}}
									/>

									<DeleteIcon
										className="h-52 cursor-pointer"
										style={{
											color: 'red',
											visibility:
												user_role === 'ADMIN' || user_role === 'admin' ? 'visible' : 'hidden'
										}}
										onClick={event => handleDeleteTicket(support, 'deleteTicketEvent')}
									/>

									<ArrowForwardIcon
										className="h-52 cursor-pointer"
										onClick={event => getTickets(support)}
									/>
								</div>
								{/* <Box>
                                <Link
                                    to="/"
                                    gutterBottom
                                    component="div"
                                >
                                    <ArrowForwardIcon />
                                </Link>
                            </Box> */}
							</Paper>
						</Grid>
					</Box>
				))}
			</FuseScrollbars>

			<TablePagination
				className="flex-shrink-0 border-t-1"
				rowsPerPageOptions={[5, 10, 25]}
				component="div"
				count={data.length}
				rowsPerPage={rowsPerPage}
				page={page}
				backIconButtonProps={{
					'aria-label': 'Previous Page'
				}}
				nextIconButtonProps={{
					'aria-label': 'Next Page'
				}}
				onChangePage={handleChangePage}
				onChangeRowsPerPage={handleChangeRowsPerPage}
			/>
		</div>
	);
};

export default withRouter(AccountsTable);
