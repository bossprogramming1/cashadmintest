import { createAsyncThunk, createEntityAdapter, createSlice } from '@reduxjs/toolkit';
import axios from 'axios';
import {
	GET_ACCOUNT_TICKETS,
	CREATE_TICKET_DETAIL,
	GET_TICKET_DETAILS,
	GET_TICKETS,
	GET_TICKET_DETAILS_BY_ID

	// GET_SUPPORTS,
	// DELETE_SUPPORT
} from '../../../../../constant/constants';

export const getAccounts = createAsyncThunk('supportManagement/supports/geSupports', async () => {
	const authTOKEN = {
		headers: {
			'Content-type': 'application/json',
			Authorization: localStorage.getItem('jwt_access_token')
		}
	};

	const response = axios.get(GET_ACCOUNT_TICKETS, authTOKEN);
	const data = await response;
	return data.data.tickets;
});

export const removeAccounts = createAsyncThunk(
	'eCommerceApp/products/removeSupports',
	async (saleIds, { dispatch, getState }) => {
		const authTOKEN = {
			headers: {
				'Content-type': 'application/json',
				Authorization: localStorage.getItem('jwt_access_token')
			}
		};
		await axios.delete(`${GET_TICKETS}`, { saleIds }, authTOKEN);

		return saleIds;
	}
);

const accountsAdapter = createEntityAdapter({});

export const { selectAll: selectAccounts, selectById: selectAccountById } = accountsAdapter.getSelectors(
	state => state.accountsManagement.accounts
);

const accountsSlice = createSlice({
	name: 'saleManagement/sales',
	initialState: accountsAdapter.getInitialState({
		searchText: ''
	}),
	reducers: {
		setAccountsSearchText: {
			reducer: (state, action) => {
				state.searchText = action.payload;
			},
			prepare: event => ({ payload: event.target.value || '' })
		}
	},
	extraReducers: {
		[getAccounts.fulfilled]: accountsAdapter.setAll
	}
});

export const { setData, setAccountsSearchText } = accountsSlice.actions;
export default accountsSlice.reducer;
