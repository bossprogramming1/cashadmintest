import { createSlice, createAsyncThunk } from '@reduxjs/toolkit';
import axios from 'axios';
import {
	CREATE_TICKET_DETAIL,
	GET_TICKET_DETAILS,
	GET_TICKETS,
	GET_TICKET_DETAILS_BY_ID,
	DELETE_TICKET
} from '../../../../../constant/constants';

export const getAccount = createAsyncThunk(
	'supportManagement/support/getSupport',
	async (params, { rejectWithValue }) => {
		const authTOKEN = {
			headers: {
				'Content-type': 'application/json',
				Authorization: localStorage.getItem('jwt_access_token')
			}
		};

		try {
			const response = await axios.get(`${GET_TICKET_DETAILS_BY_ID}${params}`, authTOKEN);
			const data = await response.data;
			return data === undefined ? null : data.ticket_details;
		} catch (err) {
			return rejectWithValue(params);
		}
	}
);

export const removeAccount = createAsyncThunk(
	'accountManagement/account/removeAccount',
	async (accountId, { dispatch, getState }) => {
		const authTOKEN = {
			headers: {
				'Content-type': 'application/json',
				Authorization: localStorage.getItem('jwt_access_token')
			}
		};

		//const supportId = val.id;
		await axios.delete(`${DELETE_TICKET}${accountId}`, authTOKEN);
	}
);

export const updateAccount = createAsyncThunk(
	'accountManagement/account/updateAccount',
	async (accountData, { dispatch, getState }) => {
		const { account } = getState().accountsManagement;
		const authTOKEN = {
			headers: {
				'Content-type': 'application/json',
				Authorization: localStorage.getItem('jwt_access_token')
			}
		};
		const response = await axios.put(`${CREATE_TICKET_DETAIL}${account.id}`, accountData, authTOKEN);
	}
);

//buildformdata
function buildFormData(formData, data, parentKey) {
	if (data && typeof data === 'object' && !(data instanceof Date) && !(data instanceof File)) {
		Object.keys(data).forEach(key => {
			buildFormData(formData, data[key], parentKey ? `${parentKey}[${key}]` : key);
		});
	} else {
		const value = data === null ? '' : data;

		formData.append(parentKey, value);
	}
}

function jsonToFormData(data) {
	const formData = new FormData();

	buildFormData(formData, data);

	return formData;
}

export const saveAccount = createAsyncThunk(
	'accountManagement/account/saveAccount',
	async (accountData, { dispatch, getState }) => {
		const accountDataToFormData = jsonToFormData(accountData);
		const authTOKEN = {
			headers: {
				'Content-type': 'application/json',
				Authorization: localStorage.getItem('jwt_access_token')
			}
		};
		const response = await axios.post(`${CREATE_TICKET_DETAIL}`, accountDataToFormData, authTOKEN);
	}
);

const accoutSlice = createSlice({
	name: 'saleManagement/sale',
	initialState: null,
	reducers: {
		resetAccount: () => null,
		newAccount: {
			reducer: (state, action) => action.payload,
			prepare: event => ({
				payload: {
					id: null,
					name: ''
				}
			})
		}
	},
	extraReducers: {
		[getAccount.fulfilled]: (state, action) => action.payload,
		[saveAccount.fulfilled]: (state, action) => {
			localStorage.setItem('saleAlert', 'saveSaleAlert');
			return action.payload;
		},
		[removeAccount.fulfilled]: () => {
			localStorage.setItem('saleAlert', 'deleteSaleAlert');
			return null;
		},
		[updateAccount.fulfilled]: () => {
			localStorage.setItem('saleAlert', 'updateSale');
		}
	}
});

export const { newAccount, resetAccount } = accoutSlice.actions;

export default accoutSlice.reducer;
