import { combineReducers } from '@reduxjs/toolkit';
import account from './accountSlice';
import accounts from './accountsSlice';

const reducer = combineReducers({
	account,
	accounts,
});

export default reducer;