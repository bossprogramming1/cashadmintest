import FusePageCarded from '@fuse/core/FusePageCarded';
import withReducer from 'app/store/withReducer';
import { FormProvider, useForm } from 'react-hook-form';
import { yupResolver } from '@hookform/resolvers/yup';
import * as yup from 'yup';
import { useDeepCompareEffect } from '@fuse/hooks';
import { useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { useParams } from 'react-router-dom';
import _ from '@lodash';
import AccountForm from './AccountForm';
import NewAccountHeader from './NewAccountHeader';
import reducer from '../store/index';
import { newAccount, resetAccount, getAccount } from '../store/accountSlice';

/**
 * Form Validation Schema
 */
const schema = yup.object().shape({
	name: yup.string().required('Name is required')
});

const NewAccount = () => {
	const dispatch = useDispatch();
	const account = useSelector(({ accountsManagement }) => accountsManagement.account);
	const [noAccount, setNoAccount] = useState(false);
	const methods = useForm({
		mode: 'onChange',
		defaultValues: {},
		resolver: yupResolver(schema)
	});
	const routeParams = useParams();

	const { reset, watch, control, onChange, formState } = methods;
	const form = watch();

	useDeepCompareEffect(() => {
		function updateAccountState() {
			const { accountId } = routeParams;

			if (accountId === 'new') {
				localStorage.removeItem('deleteTicketEvent');
				// localStorage.removeItem('event')
				/**
				 * Create New User data
				 */
				dispatch(newAccount());
			} else {
				/**
				 * Get User data
				 */

				dispatch(getAccount(accountId)).then(action => {
					/**
					 * If the requested product is not exist show message
					 */
					if (!action.payload) {
						setNoAccount(true);
					}
				});
			}
		}

		updateAccountState();
	}, [dispatch, routeParams]);

	useEffect(() => {
		if (!account) {
			return;
		}
		/**
		 * Reset the form on support state changes
		 */
		reset(account);
	}, [account, reset]);

	useEffect(() => {
		return () => {
			/**
			 * Reset Support on component unload
			 */
			dispatch(resetAccount());
			setNoAccount(false);
		};
	}, [dispatch]);

	return (
		<FormProvider {...methods}>
			<FusePageCarded
				classes={{
					content: 'flex',
					contentCard: 'overflow-hidden',
					header: 'min-h-52 h-52 sm:h-136 sm:min-h-136'
				}}
				header={<NewAccountHeader />}
				content={
					<div className="p-16 sm:p-24" style={{ width: '100%' }}>
						<AccountForm />
					</div>
				}
				innerScroll
			/>
		</FormProvider>
	);
};
export default withReducer('accountsManagement', reducer)(NewAccount);
