import React from 'react';
import withReducer from 'app/store/withReducer';
import FusePageCarded from '@fuse/core/FusePageCarded';
import { FormProvider, useForm } from 'react-hook-form';
import { yupResolver } from '@hookform/resolvers/yup';
import * as yup from 'yup';
import SalesTable from './SalesTable';
import SalesHeader from './SalesHeader';
import reducer from '../store/index';

const schema = yup.object().shape({
	name: yup.string().required('Name is required')
});

const Sales = () => {
	const methods = useForm({
		mode: 'onChange',
		defaultValues: {},
		resolver: yupResolver(schema)
	});
	//const routeParams = useParams();

	const { reset, watch, control, onChange, formState } = methods;
	const form = watch();
	return (
		<FormProvider {...methods}>
			<FusePageCarded
				classes={{
					content: 'flex',
					contentCard: 'overflow-hidden',
					header: 'min-h-52 h-52 sm:h-136 sm:min-h-136'
				}}
				header={<SalesHeader />}
				content={<SalesTable />}
				innerScroll
			/>
		</FormProvider>
	);
};

export default withReducer('salesManagement', reducer)(Sales);
