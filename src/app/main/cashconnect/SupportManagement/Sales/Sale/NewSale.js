import FusePageCarded from '@fuse/core/FusePageCarded';
import withReducer from 'app/store/withReducer';
import { FormProvider, useForm } from 'react-hook-form';
import { yupResolver } from '@hookform/resolvers/yup';
import * as yup from 'yup';
import { useDeepCompareEffect } from '@fuse/hooks';
import { useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { useParams } from 'react-router-dom';
import _ from '@lodash';
import SaleForm from './SaleForm';
import NewSaleHeader from './NewSaleHeader';
import reducer from '../store/index';
import { newSale, resetSale, getSale } from '../store/saleSlice';

/**
 * Form Validation Schema
 */
const schema = yup.object().shape({
	name: yup.string().required('Name is required')
});

const NewSale = () => {
	const dispatch = useDispatch();
	const sale = useSelector(({ salesManagement }) => salesManagement.sale);
	const [noSale, setNoSale] = useState(false);
	const methods = useForm({
		mode: 'onChange',
		defaultValues: {},
		resolver: yupResolver(schema)
	});
	const routeParams = useParams();

	const { reset, watch, control, onChange, formState } = methods;
	const form = watch();

	useDeepCompareEffect(() => {
		function updateSaleState() {
			const { saleId } = routeParams;

			if (saleId === 'new') {
				localStorage.removeItem('deleteTicketEvent');
				// localStorage.removeItem('event')
				/**
				 * Create New User data
				 */
				dispatch(newSale());
			} else {
				/**
				 * Get User data
				 */

				dispatch(getSale(saleId)).then(action => {
					/**
					 * If the requested product is not exist show message
					 */
					if (!action.payload) {
						setNoSale(true);
					}
				});
			}
		}

		updateSaleState();
	}, [dispatch, routeParams]);

	useEffect(() => {
		if (!sale) {
			return;
		}
		/**
		 * Reset the form on support state changes
		 */
		reset(sale);
	}, [sale, reset]);

	useEffect(() => {
		return () => {
			/**
			 * Reset Support on component unload
			 */
			dispatch(resetSale());
			setNoSale(false);
		};
	}, [dispatch]);

	return (
		<FormProvider {...methods}>
			<FusePageCarded
				classes={{
					content: 'flex',
					contentCard: 'overflow-hidden',
					header: 'min-h-52 h-52 sm:h-136 sm:min-h-136'
				}}
				header={<NewSaleHeader />}
				content={
					<div className="p-16 sm:p-24" style={{ width: '100%' }}>
						<SaleForm />
					</div>
				}
				innerScroll
			/>
		</FormProvider>
	);
};
export default withReducer('salesManagement', reducer)(NewSale);
