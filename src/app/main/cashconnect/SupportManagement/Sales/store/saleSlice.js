import { createSlice, createAsyncThunk } from '@reduxjs/toolkit';
import axios from 'axios';
import {
	CREATE_TICKET_DETAIL,
	GET_TICKET_DETAILS,
	GET_TICKETS,
	GET_TICKET_DETAILS_BY_ID,
	DELETE_TICKET

} from '../../../../../constant/constants';

export const getSale = createAsyncThunk('supportManagement/support/getSupport', async (params, { rejectWithValue }) => {
	const authTOKEN = {
		headers: {
			'Content-type': 'application/json',
			Authorization: localStorage.getItem('jwt_access_token')
		}
	};

	try {
		const response = await axios.get(`${GET_TICKET_DETAILS_BY_ID}${params}`, authTOKEN);
		const data = await response.data;
		return data === undefined ? null : data.ticket_details;
	} catch (err) {
		return rejectWithValue(params);
	}
});

export const removeSale = createAsyncThunk(
	'supportManagement/support/removeSupport',
	async (saleId, { dispatch, getState }) => {
		const authTOKEN = {
			headers: {
				'Content-type': 'application/json',
				Authorization: localStorage.getItem('jwt_access_token')
			}
		};
		await axios.delete(`${DELETE_TICKET}${saleId}`, authTOKEN);
	}
);

export const updateSale = createAsyncThunk(
	'saleManagement/sale/updateSale',
	async (saleData, { dispatch, getState }) => {
		const { sale } = getState().salesManagement;

		const authTOKEN = {
			headers: {
				'Content-type': 'application/json',
				Authorization: localStorage.getItem('jwt_access_token')
			}
		};
		const response = await axios.put(`${CREATE_TICKET_DETAIL}${sale.id}`, saleData, authTOKEN);
	}
);
//buildformdata
function buildFormData(formData, data, parentKey) {
	if (data && typeof data === 'object' && !(data instanceof Date) && !(data instanceof File)) {
		Object.keys(data).forEach(key => {
			buildFormData(formData, data[key], parentKey ? `${parentKey}[${key}]` : key);
		});
	} else {
		const value = data === null ? '' : data;

		formData.append(parentKey, value);
	}
}

function jsonToFormData(data) {
	const formData = new FormData();

	buildFormData(formData, data);

	return formData;
}
export const saveSale = createAsyncThunk('saleManagement/sale/saveSale', async (saleData, { dispatch, getState }) => {
	const saleDataToFormData = jsonToFormData(saleData);
	const authTOKEN = {
		headers: {
			'Content-type': 'application/json',
			Authorization: localStorage.getItem('jwt_access_token')
		}
	};
	const response = await axios.post(`${CREATE_TICKET_DETAIL}`, saleDataToFormData, authTOKEN);
});

const saleSlice = createSlice({
	name: 'saleManagement/sale',
	initialState: null,
	reducers: {
		resetSale: () => null,
		newSupport: {
			reducer: (state, action) => action.payload,
			prepare: event => ({
				payload: {
					id: null,
					name: ''
				}
			})
		}
	},
	extraReducers: {
		[getSale.fulfilled]: (state, action) => action.payload,
		[saveSale.fulfilled]: (state, action) => {
			localStorage.setItem('saleAlert', 'saveSaleAlert');
			return action.payload;
		},
		[removeSale.fulfilled]: () => {
			localStorage.setItem('saleAlert', 'deleteSaleAlert');
			return null;
		},
		[updateSale.fulfilled]: () => {
			localStorage.setItem('saleAlert', 'updateSale');
		}
	}
});

export const { newSale, resetSale } = saleSlice.actions;

export default saleSlice.reducer;
