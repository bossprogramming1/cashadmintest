import { createAsyncThunk, createEntityAdapter, createSlice } from '@reduxjs/toolkit';
import axios from 'axios';
import {
	GET_SALES_TICKETS,
	CREATE_TICKET_DETAIL,
	GET_TICKET_DETAILS,
	GET_TICKETS,
	GET_TICKET_DETAILS_BY_ID

} from '../../../../../constant/constants';

export const getSales = createAsyncThunk('supportManagement/supports/geSupports', async () => {
	const authTOKEN = {
		headers: {
			'Content-type': 'application/json',
			Authorization: localStorage.getItem('jwt_access_token')
		}
	};

	const response = axios.get(GET_SALES_TICKETS, authTOKEN);
	const data = await response;
	return data.data.tickets;
});

export const removeSales = createAsyncThunk(
	'eCommerceApp/products/removeSupports',
	async (saleIds, { dispatch, getState }) => {
		const authTOKEN = {
			headers: {
				'Content-type': 'application/json',
				Authorization: localStorage.getItem('jwt_access_token')
			}
		};
		await axios.delete(`${GET_TICKETS}`, { saleIds }, authTOKEN);

		return saleIds;
	}
);

const salesAdapter = createEntityAdapter({});

export const { selectAll: selectSales, selectById: selectSupportById } = salesAdapter.getSelectors(
	state => state.salesManagement.sales
);

const salesSlice = createSlice({
	name: 'saleManagement/sales',
	initialState: salesAdapter.getInitialState({
		searchText: ''
	}),
	reducers: {
		setSalesSearchText: {
			reducer: (state, action) => {
				state.searchText = action.payload;
			},
			prepare: event => ({ payload: event.target.value || '' })
		}
	},
	extraReducers: {
		[getSales.fulfilled]: salesAdapter.setAll
	}
});

export const { setData, setSalesSearchText } = salesSlice.actions;
export default salesSlice.reducer;
