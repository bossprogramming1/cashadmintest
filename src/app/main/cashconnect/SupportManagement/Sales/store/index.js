import { combineReducers } from '@reduxjs/toolkit';
import sale from './saleSlice';
import sales from './salesSlice';

const reducer = combineReducers({
	sale,
	sales,
});

export default reducer;