import { combineReducers } from '@reduxjs/toolkit';
import support from './supportSlice';
import supports from './supportsSlice';

const reducer = combineReducers({
	support,
	supports,
});

export default reducer;