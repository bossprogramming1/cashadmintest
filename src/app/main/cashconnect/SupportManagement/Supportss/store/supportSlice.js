import { createSlice, createAsyncThunk } from '@reduxjs/toolkit';
import axios from 'axios';
import {
	CREATE_TICKET_DETAIL,
	GET_TICKET_DETAILS,
	GET_TICKETS,
	GET_TICKET_DETAILS_BY_ID,
	DELETE_TICKET

} from '../../../../../constant/constants';

export const getSupport = createAsyncThunk(
	'supportManagement/support/getSupport',
	async (params, { rejectWithValue }) => {
		const authTOKEN = {
			headers: {
				'Content-type': 'application/json',
				Authorization: localStorage.getItem('jwt_access_token')
			}
		};

		try {
			const response = await axios.get(`${GET_TICKET_DETAILS_BY_ID}${params}`, authTOKEN);
			const data = await response.data;
			return data === undefined ? null : data.ticket_details;
		} catch (err) {
			return rejectWithValue(params);
		}
	}
);

export const removeSupport = createAsyncThunk(
	'supportManagement/support/removeSupport',
	async (supportId, { dispatch, getState }) => {
		const authTOKEN = {
			headers: {
				'Content-type': 'application/json',
				Authorization: localStorage.getItem('jwt_access_token')
			}
		};

		await axios.delete(`${DELETE_TICKET}${supportId}`, authTOKEN);
	}
);

export const updateSupport = createAsyncThunk(
	'supportManagement/support/updateSupport',
	async (supportData, { dispatch, getState }) => {
		const { support } = getState().employeesManagement;

		const authTOKEN = {
			headers: {
				'Content-type': 'application/json',
				Authorization: localStorage.getItem('jwt_access_token')
			}
		};
		const response = await axios.put(`${CREATE_TICKET_DETAIL}${support.id}`, supportData, authTOKEN);
	}
);
//buildformdata
function buildFormData(formData, data, parentKey) {
	if (data && typeof data === 'object' && !(data instanceof Date) && !(data instanceof File)) {
		Object.keys(data).forEach(key => {
			buildFormData(formData, data[key], parentKey ? `${parentKey}[${key}]` : key);
		});
	} else {
		const value = data === null ? '' : data;

		formData.append(parentKey, value);
	}
}

function jsonToFormData(data) {
	const formData = new FormData();

	buildFormData(formData, data);

	return formData;
}
export const saveSupport = createAsyncThunk(
	'supportManagement/support/saveSupport',
	async (supportData, { dispatch, getState }) => {
		const supportDataToFormData = jsonToFormData(supportData);
		const authTOKEN = {
			headers: {
				'Content-type': 'application/json',
				Authorization: localStorage.getItem('jwt_access_token')
			}
		};
		const response = await axios.post(`${CREATE_TICKET_DETAIL}`, supportDataToFormData, authTOKEN);
	}
);

const supportSlice = createSlice({
	name: 'supportManagement/support',
	initialState: null,
	reducers: {
		resetSupport: () => null,
		newSupport: {
			reducer: (state, action) => action.payload,
			prepare: event => ({
				payload: {
					id: null,
					name: ''
				}
			})
		}
	},
	extraReducers: {
		[getSupport.fulfilled]: (state, action) => action.payload,
		[saveSupport.fulfilled]: (state, action) => {
			localStorage.setItem('supportAlert', 'saveSupportAlert');
			return action.payload;
		},
		[removeSupport.fulfilled]: () => {
			localStorage.setItem('supportAlert', 'deleteSupportAlert');
			return null;
		},
		[updateSupport.fulfilled]: () => {
			localStorage.setItem('supportAlert', 'updateSupport');
		}
	}
});

export const { newSupport, resetSupport } = supportSlice.actions;

export default supportSlice.reducer;
