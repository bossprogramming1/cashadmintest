import { createAsyncThunk, createEntityAdapter, createSlice } from '@reduxjs/toolkit';
import axios from 'axios';
import {
	GET_SUPPORT_TICKETS,
	CREATE_TICKET_DETAIL,
	GET_TICKET_DETAILS,
	GET_TICKETS,
	GET_TICKET_DETAILS_BY_ID

} from '../../../../../constant/constants';

export const getSupports = createAsyncThunk('supportManagement/supports/geSupports', async () => {
	const authTOKEN = {
		headers: {
			'Content-type': 'application/json',
			Authorization: localStorage.getItem('jwt_access_token')
		}
	};

	const response = axios.get(GET_SUPPORT_TICKETS, authTOKEN);
	const data = await response;
	return data.data.tickets;
});

export const removeSupports = createAsyncThunk(
	'eCommerceApp/products/removeSupports',
	async (supportIds, { dispatch, getState }) => {
		const authTOKEN = {
			headers: {
				'Content-type': 'application/json',
				Authorization: localStorage.getItem('jwt_access_token')
			}
		};
		await axios.delete(`${GET_TICKETS}`, { supportIds }, authTOKEN);

		return supportIds;
	}
);

const supportsAdapter = createEntityAdapter({});

export const { selectAll: selectSupports, selectById: selectSupportById } = supportsAdapter.getSelectors(
	state => state.supportsManagement.supports
);

const supportsSlice = createSlice({
	name: 'supportManagement/supports',
	initialState: supportsAdapter.getInitialState({
		searchText: ''
	}),
	reducers: {
		setSupportsSearchText: {
			reducer: (state, action) => {
				state.searchText = action.payload;
			},
			prepare: event => ({ payload: event.target.value || '' })
		}
	},
	extraReducers: {
		[getSupports.fulfilled]: supportsAdapter.setAll
	}
});

export const { setData, setSupportsSearchText } = supportsSlice.actions;
export default supportsSlice.reducer;
