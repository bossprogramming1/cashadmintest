import FusePageCarded from '@fuse/core/FusePageCarded';
import withReducer from 'app/store/withReducer';
import { FormProvider, useForm } from 'react-hook-form';
import { yupResolver } from '@hookform/resolvers/yup';
import * as yup from 'yup';
import { useDeepCompareEffect } from '@fuse/hooks';
import { useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { useParams } from 'react-router-dom';
import _ from '@lodash';
import SupportForm from './SupportForm';
import NewSupportHeader from './NewSupportHeader';
import reducer from '../store/index';
import { newSupport, resetSupport, getSupport } from '../store/supportSlice';

/**
 * Form Validation Schema
 */
const schema = yup.object().shape({
	name: yup.string().required('Name is required')
});

const Support = () => {
	const dispatch = useDispatch();
	const support = useSelector(({ supportsManagement }) => supportsManagement.support);
	const [noSupport, setNoSupport] = useState(false);
	const methods = useForm({
		mode: 'onChange',
		defaultValues: {},
		resolver: yupResolver(schema)
	});
	const routeParams = useParams();

	const { reset, watch, control, onChange, formState } = methods;
	const form = watch();

	useDeepCompareEffect(() => {
		function updateSupportState() {
			const { supportId } = routeParams;

			if (supportId === 'new') {
				localStorage.removeItem('deleteTicketEvent');
				// localStorage.removeItem('event')
				/**
				 * Create New User data
				 */
				dispatch(newSupport());
			} else {
				/**
				 * Get User data
				 */

				dispatch(getSupport(supportId)).then(action => {
					/**
					 * If the requested product is not exist show message
					 */
					if (!action.payload) {
						setNoSupport(true);
					}
				});
			}
		}

		updateSupportState();
	}, [dispatch, routeParams]);

	useEffect(() => {
		if (!support) {
			return;
		}
		/**
		 * Reset the form on support state changes
		 */
		reset(support);
	}, [support, reset]);

	useEffect(() => {
		return () => {
			/**
			 * Reset Support on component unload
			 */
			dispatch(resetSupport());
			setNoSupport(false);
		};
	}, [dispatch]);

	return (
		<FormProvider {...methods}>
			<FusePageCarded
				classes={{
					content: 'flex',
					contentCard: 'overflow-hidden',
					header: 'min-h-52 h-52 sm:h-136 sm:min-h-136'
				}}
				header={<NewSupportHeader />}
				content={
					<div className="p-16 sm:p-24" style={{ width: '100%' }}>
						<SupportForm />
					</div>
				}
				innerScroll
			/>
		</FormProvider>
	);
};
export default withReducer('supportsManagement', reducer)(Support);
