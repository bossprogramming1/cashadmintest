import { createAsyncThunk, createEntityAdapter, createSlice } from '@reduxjs/toolkit';
import axios from 'axios';
import { DELETE_CUSTOMER, GET_CUSTOMERS, GET_LATEST_CUSTOMERS } from '../../../../constant/constants';

export const getCustomers = createAsyncThunk('customerManagement/customers/geCustomers', async pageAndSize => {
	axios.defaults.headers.common['Content-type'] = 'application/json';
	axios.defaults.headers.common.Authorization = localStorage.getItem('jwt_access_token');

	const response = axios.get(GET_CUSTOMERS, { params: pageAndSize });
	const data = await response;

	sessionStorage.setItem('total_customers_elements', data.data.total_elements);
	sessionStorage.setItem('total_customers_pages', data.data.total_pages);
	delete axios.defaults.headers.common['Content-type'];
	delete axios.defaults.headers.common.Authorization;

	return data.data.customers;
});
export const getLatestCustomers = createAsyncThunk(
	'customerManagement/customers/getLatestCustomers',
	async pageAndSize => {
		axios.defaults.headers.common['Content-type'] = 'application/json';
		axios.defaults.headers.common.Authorization = localStorage.getItem('jwt_access_token');

		const response = axios.get(GET_LATEST_CUSTOMERS, { params: pageAndSize });
		const data = await response;

		sessionStorage.setItem('total_customers_elements', data.data.total_elements);
		sessionStorage.setItem('total_customers_pages', data.data.total_pages);
		delete axios.defaults.headers.common['Content-type'];
		delete axios.defaults.headers.common.Authorization;

		return data.data.customers;
	}
);

export const removeCustomers = createAsyncThunk(
	'customerManagement/customers/removeCustomers',
	async (customerIds, { dispatch, getState }) => {
		const authTOKEN = {
			headers: {
				'Content-type': 'application/json',
				Authorization: localStorage.getItem('jwt_access_token')
			}
		};
		await axios.delete(`${DELETE_CUSTOMER}`, { customerIds }, authTOKEN);

		return customerIds;
	}
);

const customersAdapter = createEntityAdapter({});

export const { selectAll: selectCustomers, selectById: selectCustomerById } = customersAdapter.getSelectors(
	state => state.customersManagement.customers
);

const customersSlice = createSlice({
	name: 'customerManagement/customers',
	initialState: customersAdapter.getInitialState({
		searchText: ''
	}),
	reducers: {
		setCustomersSearchText: {
			reducer: (state, action) => {
				state.searchText = action.payload;
			},
			prepare: event => ({ payload: event.target.value || '' })
		}
	},
	extraReducers: {
		[getCustomers.fulfilled]: customersAdapter.setAll,
		[getLatestCustomers.fulfilled]: customersAdapter.setAll
	}
});

export const { setData, setCustomersSearchText } = customersSlice.actions;
export default customersSlice.reducer;
