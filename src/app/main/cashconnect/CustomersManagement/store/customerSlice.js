import { createAsyncThunk, createSlice } from '@reduxjs/toolkit';
import { CREATE_CUSTOMER, DELETE_CUSTOMER, GET_CUSTOMERID, UPDATE_CUSTOMER } from 'app/constant/constants';
import NotificationModel from 'app/fuse-layouts/shared-components/notificationPanel/model/NotificationModel';
import { addNotification } from 'app/fuse-layouts/shared-components/notificationPanel/store/dataSlice';
import axios from 'axios';

export const getCustomer = createAsyncThunk(
	'customerManagement/customer/getCustomer',
	async (params, { rejectWithValue }) => {
		const authTOKEN = {
			headers: {
				'Content-type': 'application/json',
				Authorization: localStorage.getItem('jwt_access_token')
			}
		};

		try {
			const response = await axios.get(`${GET_CUSTOMERID}${params}`, authTOKEN);
			const data = await response.data;
			return data === undefined ? null : data;
		} catch (err) {
			return rejectWithValue(params);
		}
	}
);

export const removeCustomer = createAsyncThunk(
	'customerManagement/customer/removeCustomer',
	async (val, { dispatch, getState }) => {
		const authTOKEN = {
			headers: {
				'Content-type': 'application/json',
				Authorization: localStorage.getItem('jwt_access_token')
			}
		};

		const customerId = val.id;

		try {
			const res = await axios.delete(`${DELETE_CUSTOMER}${customerId}`, authTOKEN);

			console.log('resRemoveProduct', res);
			return res; // Return the response if the request is successful.
		} catch (err) {
			if (err.response.status == 400) {
				console.log('Status code:', err.response); // Access the status code.
				dispatch(
					addNotification(
						NotificationModel({
							message: `${err.response.data.detail}`,
							options: { variant: 'error' },
							item_id: customerId
						})
					)
				);
			}
			console.log('errRemove', err);
			throw err; // Rethrow the error if an error occurs.
		}
	}
);

export const updateCustomer = createAsyncThunk(
	'customerManagement/customer/updateCustomer',
	async (customerData, { dispatch, getState }) => {
		const { customer } = getState().customersManagement;
		const convertCustomerDataToFormData = jsonToFormData(customerData);
		const authTOKEN = {
			headers: {
				'Content-type': 'multipart/form-data',
				Authorization: localStorage.getItem('jwt_access_token')
			}
		};
		const response = await axios.put(`${UPDATE_CUSTOMER}${customer.id}`, convertCustomerDataToFormData, authTOKEN);
	}
);

//buildformdata
const buildFormData = (formData, data, parentKey) => {
	if (data && typeof data === 'object' && !(data instanceof Date) && !(data instanceof File)) {
		Object.keys(data).forEach(key => {
			buildFormData(formData, data[key], parentKey ? `${parentKey}[${key}]` : key);
		});
	} else {
		const value = data === null ? '' : data;

		formData.append(parentKey, value);
	}
};

//convertJsonToFormData
const jsonToFormData = data => {
	const formData = new FormData();

	buildFormData(formData, data);
	return formData;
};

export const saveCustomer = createAsyncThunk(
	'customerManagement/customer/saveCustomer',
	async (customerData, { dispatch, getState }) => {
		const getFormDateFJ = jsonToFormData(customerData);

		const authTOKEN = {
			headers: {
				'Content-type': 'multipart/form-data',
				Authorization: localStorage.getItem('jwt_access_token')
			}
		};
		const response = await axios.post(`${CREATE_CUSTOMER}`, getFormDateFJ, authTOKEN);
	}
);

const customerSlice = createSlice({
	name: 'customerManagement/customer',
	initialState: null,
	reducers: {
		resetCustomer: () => null,
		newCustomer: {
			reducer: (state, action) => action.payload,
			prepare: event => ({
				payload: {
					first_name: '',
					last_name: '',
					username: '',
					email: '',
					password: '',
					confirmPassword: '',
					gender: 0,
					primary_phone: '',
					secondary_phone: '',
					date_of_birth: '',
					is_active: false,
					street_address_one: '',
					street_address_two: '',
					thana: 0,
					city: 0,
					country: 0,
					postal_code: '',
					nid: '',
					branch: 0,
					is_online: false,
					customer_type: 0,
					customer_credit_limit: 0,
					image: '',
					country_code2: '+880',
					show_country_code1: '+880',
					show_country_code2: '+880'
				}
			})
		}
	},
	extraReducers: {
		[getCustomer.fulfilled]: (state, action) => action.payload,
		[saveCustomer.fulfilled]: (state, action) => {
			localStorage.setItem('customerAlert', 'saveCustomer');
			return action.payload;
		},
		[removeCustomer.fulfilled]: (state, action) => {
			localStorage.setItem('customerAlert', 'deleteCustomer');
			return action.payload;
		},
		[updateCustomer.fulfilled]: () => {
			localStorage.setItem('customerAlert', 'updateCustomer');
		}
	}
});

export const { newCustomer, resetCustomer } = customerSlice.actions;

export default customerSlice.reducer;
