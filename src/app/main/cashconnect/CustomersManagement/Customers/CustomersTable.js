import FuseLoading from '@fuse/core/FuseLoading';
import FuseScrollbars from '@fuse/core/FuseScrollbars';
import _ from '@lodash';
import Checkbox from '@material-ui/core/Checkbox';
import { makeStyles } from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TablePagination from '@material-ui/core/TablePagination';
import TableRow from '@material-ui/core/TableRow';
import DeleteIcon from '@material-ui/icons/Delete';
import EditIcon from '@material-ui/icons/Edit';
import Pagination from '@material-ui/lab/Pagination';
import { motion } from 'framer-motion';
import LockOpenIcon from '@mui/icons-material/LockOpen';
import React, { useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { withRouter } from 'react-router-dom';
import { Tooltip, Typography } from '@material-ui/core';
import { useParams } from 'react-router';
import { BASE_URL, SEARCH_CUSTOMER } from '../../../../constant/constants';
import { getCustomers, getLatestCustomers, selectCustomers } from '../store/customersSlice';
import CustomersTableHead from './CustomersTableHead';

const useStyles = makeStyles(theme => ({
	root: {
		display: 'flex',
		justifyContent: 'space-between',
		flexWrap: 'wrap',
		'& > *': {
			marginTop: theme.spacing(2),
			marginBottom: theme.spacing(3)
		}
	}
}));

const CustomersTable = props => {
	const classes = useStyles();
	const dispatch = useDispatch();
	const customers = useSelector(selectCustomers);
	const searchText = useSelector(({ customersManagement }) => customersManagement.customers.searchText);
	const [searchCustomer, setSearchCustomer] = useState([]);
	const { latestCustomer } = useParams();

	const [loading, setLoading] = useState(true);
	const [selected, setSelected] = useState([]);
	const [data, setData] = useState(customers);
	const user_role = localStorage.getItem('user_role');
	const [page, setPage] = useState(0);
	const [rowsPerPage, setRowsPerPage] = useState(5);
	const [pageAndSize, setPageAndSize] = useState({ page: 1, size: 5 });
	const totalPages = sessionStorage.getItem('total_customers_pages');
	const totalElements = sessionStorage.getItem('total_customers_elements');
	const [order, setOrder] = useState({
		direction: 'asc',
		id: null
	});
	let serialNumber = 1;
	const [deleteItem, setDeleteItem] = useState('');
	useEffect(() => {
		if (latestCustomer === 'latest-customer') {
			dispatch(getLatestCustomers(pageAndSize)).then(() => setLoading(false));
		} else {
			dispatch(getCustomers(pageAndSize)).then(() => setLoading(false));
		}
	}, [dispatch, latestCustomer]);

	//searchCustomer
	useEffect(() => {
		searchText !== '' && getSearchCustomer();
	}, [searchText]);

	const getSearchCustomer = () => {
		fetch(`${SEARCH_CUSTOMER}?username=${searchText}`)
			.then(response => response.json())
			.then(searchedCustomersData => {
				setSearchCustomer(searchedCustomersData.customers);
			});
	};

	function handleRequestSort(customerEvent, property) {
		const id = property;
		let direction = 'desc';

		if (order.id === property && order.direction === 'desc') {
			direction = 'asc';
		}

		setOrder({
			direction,
			id
		});
	}

	function handleSelectAllClick(customerEvent) {
		if (customerEvent.target.checked) {
			setSelected(customers.map(n => n.id));
			return;
		}
		setSelected([]);
	}

	function handleDeselect() {
		setSelected([]);
	}

	//pagination
	const handlePagination = (e, handlePage) => {
		setPageAndSize({ ...pageAndSize, page: handlePage });
		setPage(handlePage - 1);
		dispatch(getCustomers({ ...pageAndSize, page: handlePage }));
	};

	function handleChangePage(event, value) {
		setPage(value);
		setPageAndSize({ ...pageAndSize, page: value + 1 });
		dispatch(getCustomers({ ...pageAndSize, page: value + 1 }));
	}

	function handleChangeRowsPerPage(event) {
		setRowsPerPage(event.target.value);
		setPageAndSize({ ...pageAndSize, size: event.target.value });
		dispatch(getCustomers({ ...pageAndSize, size: event.target.value }));
	}

	function handleUpdateCustomer(item, event) {
		localStorage.removeItem('customerEvent');
		localStorage.setItem('updateCustomer', event);
		props.history.push(`/apps/customer-management/customers/${item.id}/${item.first_name}`);
	}
	function handleDeleteCustomer(item, customerEvent) {
		localStorage.removeItem('updateCustomer');
		localStorage.removeItem('customerEvent');
		localStorage.setItem('customerEvent', customerEvent);
		props.history.push(`/apps/customer-management/customers/${item.id}/${item.first_name}`);
	}
	function handleOTPsend(item, otpEvent) {
		localStorage.removeItem('vendorEvent');
		localStorage.removeItem('updateVendor');
		localStorage.setItem('otpEvent', otpEvent);
		props.history.push(`/apps/customer-otp-management/customers/${item.id}/${item.first_name}`);
	}

	function handleCheck(customerEvent, id) {
		const selectedIndex = selected.indexOf(id);
		let newSelected = [];

		if (selectedIndex === -1) {
			newSelected = newSelected.concat(selected, id);
		} else if (selectedIndex === 0) {
			newSelected = newSelected.concat(selected.slice(1));
		} else if (selectedIndex === selected.length - 1) {
			newSelected = newSelected.concat(selected.slice(0, -1));
		} else if (selectedIndex > 0) {
			newSelected = newSelected.concat(selected.slice(0, selectedIndex), selected.slice(selectedIndex + 1));
		}

		setSelected(newSelected);
	}

	if (loading) {
		return <FuseLoading />;
	}
	if (customers?.length === 0) {
		return (
			<motion.div
				initial={{ opacity: 0 }}
				animate={{ opacity: 1, transition: { delay: 0.1 } }}
				className="flex flex-1 items-center justify-center h-full"
			>
				<Typography color="textSecondary" variant="h5">
					There are no customers!
				</Typography>
			</motion.div>
		);
	}

	return (
		<div className="w-full flex flex-col">
			<FuseScrollbars className="flex-grow overflow-x-auto">
				<Table stickyHeader className="min-w-xl" aria-labelledby="tableTitle">
					<CustomersTableHead
						selectedCustomerIds={selected}
						order={order}
						onSelectAllClick={handleSelectAllClick}
						onRequestSort={handleRequestSort}
						rowCount={customers.length}
						onMenuItemClick={handleDeselect}
					/>

					<TableBody>
						{_.orderBy(
							searchText !== '' && searchCustomer ? searchCustomer : customers,
							[
								o => {
									switch (order.id) {
										case 'categories': {
											return o.categories[0];
										}
										default: {
											return o[order.id];
										}
									}
								}
							],
							[order.direction]
						).map(n => {
							const isSelected = selected.indexOf(n.id) !== -1;
							return (
								<TableRow
									hover
									role="checkbox"
									aria-checked={isSelected}
									tabIndex={-1}
									key={n.id}
									selected={isSelected}
								>
									<TableCell className="whitespace-nowrap w-40 md:w-64 text-center" padding="none">
										<Checkbox
											checked={isSelected}
											onClick={customerEvent => customerEvent.stopPropagation()}
											onChange={customerEvent => handleCheck(customerEvent, n.id)}
										/>
									</TableCell>

									<TableCell className="whitespace-nowrap w-40 md:w-64" component="th" scope="row">
										{pageAndSize.page * pageAndSize.size - pageAndSize.size + serialNumber++}
									</TableCell>

									<TableCell
										whitespace-nowrap
										className="w-52 px-4 md:px-4"
										component="th"
										scope="row"
									>
										{n.length > 0 && n.featuredImageId ? (
											<img
												className="w-full block rounded"
												style={{ borderRadius: '50%', height: '50px', width: '50px' }}
												src={_.find(n.image, { id: n.featuredImageId }).url}
												alt={n?.name}
											/>
										) : (
											<img
												className="w-full block rounded"
												style={{ borderRadius: '50%', height: '50px', width: '50px' }}
												src={`${BASE_URL}${n.image}`}
												alt={n?.name}
											/>
										)}
									</TableCell>

									<TableCell className="whitespace-nowrap p-4 md:p-16" component="th" scope="row">
										{n?.branch?.name}
									</TableCell>

									<TableCell className="whitespace-nowrap p-4 md:p-16" component="th" scope="row">
										{`${n.first_name} ${n.last_name}`}
									</TableCell>

									<TableCell className="whitespace-nowrap p-4 md:p-16" component="th" scope="row">
										{n.username}
									</TableCell>
									<TableCell className="whitespace-nowrap p-4 md:p-16" component="th" scope="row">
										{n.email}
									</TableCell>

									<TableCell className="whitespace-nowrap p-4 md:p-16" component="th" scope="row">
										{n.primary_phone}
									</TableCell>

									<TableCell
										whitespace-nowrap
										className="p-4 md:p-16"
										align="center"
										component="th"
										scope="row"
									>
										<div>
											<Tooltip title="Edit" placement="top" enterDelay={300}>
												<EditIcon
													onClick={customerEvent => handleUpdateCustomer(n, 'Update')}
													className="h-52 cursor-pointer"
													style={{ color: 'green' }}
												/>
											</Tooltip>
											<Tooltip title="Delete" placement="top" enterDelay={300}>
												<DeleteIcon
													onClick={event => handleDeleteCustomer(n, 'Delete')}
													className="h-52 cursor-pointer"
													style={{
														color: 'red',
														visibility:
															user_role === 'ADMIN' || user_role === 'admin'
																? 'visible'
																: 'hidden'
													}}
												/>
											</Tooltip>
											<Tooltip title="Authorize Customer" placement="top" enterDelay={300}>
												<LockOpenIcon
													onClick={event => handleOTPsend(n, 'sendOTP')}
													className="h-52 cursor-pointer"
													style={{ color: 'black' }}
												/>
											</Tooltip>
										</div>
									</TableCell>
								</TableRow>
							);
						})}
					</TableBody>
				</Table>
			</FuseScrollbars>

			<div className={classes.root} id="pagiContainer">
				<Pagination
					count={totalPages}
					page={page + 1}
					defaultPage={1}
					color="primary"
					showFirstButton
					showLastButton
					variant="outlined"
					shape="rounded"
					onChange={handlePagination}
				/>

				<TablePagination
					className="flex-shrink-0 border-t-1"
					rowsPerPageOptions={[5, 10, 30, 50, 100]}
					component="div"
					count={totalElements}
					rowsPerPage={rowsPerPage}
					page={page}
					backIconButtonProps={{
						'aria-label': 'Previous Page'
					}}
					nextIconButtonProps={{
						'aria-label': 'Next Page'
					}}
					onChangePage={handleChangePage}
					onChangeRowsPerPage={handleChangeRowsPerPage}
				/>
			</div>
		</div>
	);
};

export default withRouter(CustomersTable);
