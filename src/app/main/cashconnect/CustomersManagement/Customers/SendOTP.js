import Box from '@material-ui/core/Box';
import { makeStyles } from '@material-ui/core/styles';
import TextField from '@material-ui/core/TextField';
import ArrowBackIcon from '@mui/icons-material/ArrowBack';

import React, { useEffect, useState } from 'react';
import { useParams, useHistory } from 'react-router-dom';
import { Button, Typography } from '@material-ui/core';
import axios from 'axios';
import { GET_CUSTOMERID, GET_PHONE_VARIFICATION_CODE_BY_VENDOR } from '../../../../constant/constants';

const useStyles = makeStyles(theme => ({
	hidden: {
		display: 'none'
	},
	productImageUpload: {
		transitionProperty: 'box-shadow',
		transitionDuration: theme.transitions.duration.short,
		transitionTimingFunction: theme.transitions.easing.easeInOut
	}
}));
function VendorForm(props) {
	const history = useHistory();
	const routeParams = useParams();
	const { customerId } = routeParams;
	const [customer, setCustomer] = useState([]);
	const [OTP, setSetOTP] = useState('');
	const authTOKEN = {
		headers: {
			'Content-type': 'application/json',
			Authorization: localStorage.getItem('jwt_access_token')
		}
	};

	const handleOtpSubmit = async () => {
		if (localStorage.getItem('otpEvent')) {
			axios
				.get(
					`${GET_PHONE_VARIFICATION_CODE_BY_VENDOR}?primary_phone=${customer.primary_phone}&phone_otp=${OTP}`,
					authTOKEN
				)
				.then(res => {});

			localStorage.removeItem('otpEvent');
			localStorage.setItem('sendConfirmOTP', 'sendConfirmOTP');
		}

		history.goBack();
	};

	useEffect(() => {
		fetch(`${GET_CUSTOMERID}${customerId}`, authTOKEN)
			.then(response => response.json())
			.then(data => setCustomer(data));
	}, []);

	return (
		<>
			<Box
				style={{
					display: 'flex',
					justifyContent: 'space-between',
					flexDirection: 'row',
					marginRight: '150px',
					marginLeft: '150px'
				}}
			>
				<Button onClick={() => history.goBack()} style={{ fontSize: '17px' }}>
					<ArrowBackIcon
						className="h-52 cursor-pointer"
						style={{
							color: 'black',
							textAlign: 'left',

							height: '30px',
							width: '30px'
						}}
					/>
					Go Back
				</Button>
			</Box>

			<Box>
				<div
					style={{
						display: 'flex',
						flexDirection: 'column',
						alignItems: 'center',
						marginTop: '80px'
					}}
				>
					<form>
						<Typography variant="h6">Send OTP</Typography>
						<br />
						<TextField
							style={{ width: '250px', margin: '5px' }}
							name="name"
							type="text"
							label="Phone Number"
							variant="outlined"
							InputLabelProps={{
								shrink: true
							}}
							value={`${customer.primary_phone || ''}`}
						/>
						<br />
						<TextField
							style={{ width: '250px', margin: '5px' }}
							name="phone_otp"
							type="text"
							label="OTP"
							variant="outlined"
							onChange={event => {
								setSetOTP(event.target.value);
							}}
							InputLabelProps={{
								shrink: true
							}}
						/>

						<br />
						<Button variant="contained" color="primary" onClick={handleOtpSubmit}>
							Send OTP
						</Button>
					</form>
				</div>
			</Box>
		</>
	);
}

export default VendorForm;
