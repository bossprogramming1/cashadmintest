import FusePageCarded from '@fuse/core/FusePageCarded';
import { useDeepCompareEffect } from '@fuse/hooks';
import { yupResolver } from '@hookform/resolvers/yup';
import { Button, Typography } from '@material-ui/core';
import {
	CUSTOMER_CREATE,
	CUSTOMER_DELETE,
	CUSTOMER_DETAILS,
	CUSTOMER_UPDATE
} from 'app/constant/permission/permission';
import { getUserPermissions } from 'app/store/dataSlice';
import withReducer from 'app/store/withReducer';
import { motion } from 'framer-motion';
import React, { useEffect, useState } from 'react';
import { FormProvider, useForm } from 'react-hook-form';
import { useDispatch, useSelector } from 'react-redux';
import { Link, useParams } from 'react-router-dom';
import * as yup from 'yup';
import PagenotFound from '../../Pagenotfound/PagenotFound';
import { getCustomer, newCustomer, resetCustomer } from '../store/customerSlice';
import reducer from '../store/index';
import CustomerForm from './CustomerForm';
import NewCustomerHeader from './NewCustomerHeader';

/**
 * Form Validation Schema
 */
const schema = yup.object().shape({
	first_name: yup.string().required('First name is required').min(5, 'First name must be at least 5 characters'),
	last_name: yup.string().required('Last name is required'),

	password: yup.string().min(6, 'Password must be at least 6 characters').required('Password is required'),
	confirmPassword: yup
		.string()
		.required('Confirm password is required')
		.oneOf([yup.ref('password'), null], 'Passwords must match'),
	primary_phone: yup.string().min(11, 'Please enter your number with 0').required('Phone number is required')
});

const Customer = () => {
	const dispatch = useDispatch();
	const customer = useSelector(({ customersManagement }) => customersManagement.customer);
	const [noCustomer, setNoCustomer] = useState(false);
	const methods = useForm({
		mode: 'onChange',
		defaultValues: {},
		resolver: yupResolver(schema)
	});
	const routeParams = useParams();
	const UserPermissions = useSelector(state => state.data.UserPermissions);
	useEffect(() => {
		dispatch(getUserPermissions());
	}, []);
	const { reset, watch, control, onChange, formState } = methods;
	const form = watch();

	useDeepCompareEffect(() => {
		function updateCustomerState() {
			const { customerId } = routeParams;

			if (customerId === 'new') {
				localStorage.removeItem('removeCustomer');
				localStorage.removeItem('updateCustomer');
				/**
				 * Create New User data
				 */
				dispatch(newCustomer());
			} else {
				/**
				 * Get User data
				 */

				dispatch(getCustomer(customerId)).then(action => {
					/**
					 * If the requested product is not exist show message
					 */
					if (!action.payload) {
						setNoCustomer(true);
					}
				});
			}
		}

		updateCustomerState();
	}, [dispatch, routeParams]);

	useEffect(() => {}, []);

	useEffect(() => {
		if (!customer) {
			return;
		}
		/**
		 * Reset the form on customer state changes
		 */
		reset({
			...customer,
			branch: customer?.branch?.id,
			thana: customer?.thana?.id,
			city: customer?.city?.id,
			country: customer?.country?.id,
			role: customer?.role?.id,
			customer_type: customer?.customer_type?.id,
			country_code1: '+880',
			country_code2: '+880',
			show_country_code1: '+880',
			show_country_code2: '+880'
		});
	}, [customer, reset]);

	useEffect(() => {
		return () => {
			/**
			 * Reset Customer on component unload
			 */
			dispatch(resetCustomer());
			setNoCustomer(false);
		};
	}, [dispatch]);

	/**
	 * Show Message if the requested products is not exists
	 */
	if (noCustomer) {
		return (
			<motion.div
				initial={{ opacity: 0 }}
				animate={{ opacity: 1, transition: { delay: 0.1 } }}
				className="flex flex-col flex-1 items-center justify-center h-full"
			>
				<Typography color="textSecondary" variant="h5">
					There is no such customer!
				</Typography>
				<Button
					className="mt-24"
					component={Link}
					variant="outlined"
					to="/apps/e-commerce/products"
					color="inherit"
				>
					Go to Customer Page
				</Button>
			</motion.div>
		);
	}

	return (
		<FormProvider {...methods}>
			{UserPermissions.includes(CUSTOMER_CREATE) ||
			UserPermissions.includes(CUSTOMER_UPDATE) ||
			UserPermissions.includes(CUSTOMER_DELETE) ||
			UserPermissions.includes(CUSTOMER_DETAILS) ? (
				<FusePageCarded
					classes={{
						toolbar: 'p-0',
						header: 'min-h-74 h-64'
					}}
					header={<NewCustomerHeader />}
					content={
						<div className="p-16 sm:p-24">
							<CustomerForm />
						</div>
					}
					innerScroll
				/>
			) : (
				<PagenotFound />
			)}
		</FormProvider>
	);
};
export default withReducer('customersManagement', reducer)(Customer);
