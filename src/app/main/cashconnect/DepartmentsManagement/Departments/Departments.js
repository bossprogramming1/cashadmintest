import FusePageCarded from '@fuse/core/FusePageCarded';
import { DEPARTMENT_LIST } from 'app/constant/permission/permission';
import { getUserPermissions } from 'app/store/dataSlice';
import withReducer from 'app/store/withReducer';
import React, { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import PagenotFound from '../../Pagenotfound/PagenotFound';
import reducer from '../store/index';
import DepartmentsHeader from './DepartmentsHeader';
import DepartmentsTable from './DepartmentsTable';

const Departments = () => {
	const UserPermissions = useSelector(state => state.data.UserPermissions);
	useEffect(() => {
		dispatch(getUserPermissions());
	}, []);
	const dispatch = useDispatch();
	return (
		<FusePageCarded
			classes={{
				content: 'flex',
				contentCard: 'overflow-hidden',
				header: 'min-h-74 h-64'
			}}
			header={UserPermissions.includes(DEPARTMENT_LIST) && <DepartmentsHeader />}
			content={UserPermissions.includes(DEPARTMENT_LIST) ? <DepartmentsTable /> : <PagenotFound />}
			innerScroll
		/>
	);
};
export default withReducer('departmentsManagement', reducer)(Departments);
