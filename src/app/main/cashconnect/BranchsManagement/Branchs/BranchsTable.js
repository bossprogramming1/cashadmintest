import FuseLoading from '@fuse/core/FuseLoading';
import FuseScrollbars from '@fuse/core/FuseScrollbars';
import _ from '@lodash';
import { Box, Icon, IconButton, makeStyles, Modal, Tooltip, Typography } from '@material-ui/core';
import Checkbox from '@material-ui/core/Checkbox';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TablePagination from '@material-ui/core/TablePagination';
import CloseIcon from '@mui/icons-material/Close';
import TableRow from '@material-ui/core/TableRow';
import DeleteIcon from '@material-ui/icons/Delete';
import EditIcon from '@material-ui/icons/Edit';
import { Pagination } from '@material-ui/lab';
import { rowsPerPageOptions } from 'app/@data/data';
import { SEARCH_BRANCH } from 'app/constant/constants';
import { motion } from 'framer-motion';
import React, { useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { withRouter } from 'react-router-dom';
import { getBranchs, selectBranchs } from '../store/branchsSlice';
import BranchsTableHead from './BranchsTableHead';

const useStyles = makeStyles(theme => ({
	root: {
		display: 'flex',
		justifyContent: 'space-between',
		flexWrap: 'nowrap',
		overflow: 'auto',
		minHeight: '35px'
	},
	toolbar: {
		'& > div': {
			minHeight: 'fit-content'
		}
	},
	modal: {
		position: 'relative',

		marginTop: '22%',
		marginLeft: '50%',
		transform: 'translate(-50%, -50%)',
		width: 'fit-content',
		height: 'fit-content',
		border: '2px solid #000',
		boxShadow: 24,
		p: 4,
		//color: theme.palette.background.paper,
		backgroundColor: theme.palette.background.paper
	},
	close: { position: 'absolute', top: '10', right: '0', color: 'red', fontSize: '20px', marginRight: '10px' },
	paper: {
		marginTop: '3%',
		width: '100%',
		backgroundColor: theme.palette.grey,
		//border: `2px solid ${theme.palette.grey[800]}`,
		display: 'flex',
		justifyContent: 'space-between',
		alignItems: 'center',
		//boxShadow: theme.shadows[5],
		padding: theme.spacing(2, 4, 3),
		marginLeft: '20px',
		marginRight: '10px'
	}
}));

const BranchsTable = props => {
	const dispatch = useDispatch();
	const classes = useStyles();
	const branchs = useSelector(selectBranchs);
	const searchText = useSelector(({ branchsManagement }) => branchsManagement.branchs.searchText);
	const [searchBranch, setSearchBranch] = useState([]);
	const [loading, setLoading] = useState(true);
	const [selected, setSelected] = useState([]);
	const [open, setOpen] = useState(false);
	const [mapUrl, setMapUrl] = useState({});
	const [page, setPage] = useState(0);
	const [rowsPerPage, setRowsPerPage] = useState(30);
	const [pageAndSize, setPageAndSize] = useState({ page: 1, size: 30 });
	const [order, setOrder] = useState({
		direction: 'asc',
		id: null
	});
	let serialNumber = 1;
	const user_role = localStorage.getItem('user_role');

	const totalPages = sessionStorage.getItem('total_branchs_pages');
	const totalElements = sessionStorage.getItem('total_branchs_elements');

	useEffect(() => {
		dispatch(getBranchs(pageAndSize)).then(() => setLoading(false));
	}, [dispatch]);

	useEffect(() => {
		searchText ? getSearchBranch() : setSearchBranch([]);
	}, [searchText]);

	const handleClose = () => setOpen(false);

	const getSearchBranch = () => {
		fetch(`${SEARCH_BRANCH}?name=${searchText}`)
			.then(response => response.json())
			.then(data => {
				setSearchBranch(data?.branchs);
			})
			.catch(() => setSearchBranch([]));
	};

	function handleRequestSort(branchEvent, property) {
		const id = property;
		let direction = 'desc';

		if (order.id === property && order.direction === 'desc') {
			direction = 'asc';
		}

		setOrder({
			direction,
			id
		});
	}

	function handleSelectAllClick(branchEvent) {
		if (branchEvent.target.checked) {
			setSelected(branchs.map(n => n.id));
			return;
		}
		setSelected([]);
	}

	function handleDeselect() {
		setSelected([]);
	}

	function handleUpdateBranch(item) {
		localStorage.removeItem('branchEvent');
		props.history.push(`/apps/branch-management/branchs/${item.id}/${item?.name}`);
	}
	function handleDeleteBranch(item, branchEvent) {
		localStorage.removeItem('branchEvent');
		localStorage.setItem('branchEvent', branchEvent);
		props.history.push(`/apps/branch-management/branchs/${item.id}/${item?.name}`);
	}

	function handleCheck(branchEvent, id) {
		const selectedIndex = selected.indexOf(id);
		let newSelected = [];

		if (selectedIndex === -1) {
			newSelected = newSelected.concat(selected, id);
		} else if (selectedIndex === 0) {
			newSelected = newSelected.concat(selected.slice(1));
		} else if (selectedIndex === selected.length - 1) {
			newSelected = newSelected.concat(selected.slice(0, -1));
		} else if (selectedIndex > 0) {
			newSelected = newSelected.concat(selected.slice(0, selectedIndex), selected.slice(selectedIndex + 1));
		}

		setSelected(newSelected);
	}

	//pagination
	const handlePagination = (e, handlePage) => {
		setPageAndSize({ ...pageAndSize, page: handlePage });
		setPage(handlePage - 1);
		dispatch(getBranchs({ ...pageAndSize, page: handlePage }));
	};

	function handleChangePage(menuEvent, value) {
		setPage(value);
		setPageAndSize({ ...pageAndSize, page: value + 1 });
		dispatch(getBranchs({ ...pageAndSize, page: value + 1 }));
	}

	function handleChangeRowsPerPage(menuEvent) {
		setRowsPerPage(menuEvent.target.value);
		setPageAndSize({ ...pageAndSize, size: menuEvent.target.value });
		dispatch(getBranchs({ ...pageAndSize, size: menuEvent.target.value }));
	}

	if (loading) {
		return <FuseLoading />;
	}

	if (branchs?.length === 0) {
		return (
			<motion.div
				initial={{ opacity: 0 }}
				animate={{ opacity: 1, transition: { delay: 0.1 } }}
				className="flex flex-1 items-center justify-center h-full"
			>
				<Typography color="textSecondary" variant="h5">
					There are no branch!
				</Typography>
			</motion.div>
		);
	}

	return (
		<div className="w-full flex flex-col">
			<FuseScrollbars className="flex-grow overflow-x-auto">
				<Table stickyHeader className="min-w-xl" aria-labelledby="tableTitle">
					<BranchsTableHead
						selectedBranchIds={selected}
						order={order}
						onSelectAllClick={handleSelectAllClick}
						onRequestSort={handleRequestSort}
						rowCount={branchs.length}
						onMenuItemClick={handleDeselect}
					/>

					<TableBody>
						{_.orderBy(
							searchText !== '' && !_.isEmpty(searchBranch) ? searchBranch : branchs,
							[
								o => {
									switch (order.id) {
										case 'categories': {
											return o.categories[0];
										}
										default: {
											return o[order.id];
										}
									}
								}
							],
							[order.direction]
						).map(n => {
							const isSelected = selected.indexOf(n.id) !== -1;
							return (
								<TableRow
									className="h-52 cursor-pointer"
									hover
									role="checkbox"
									aria-checked={isSelected}
									tabIndex={-1}
									key={n.id}
									selected={isSelected}
								>
									<TableCell className="whitespace-nowrap w-40 md:w-64 text-center" padding="none">
										<Checkbox
											checked={isSelected}
											onClick={branchEvent => branchEvent.stopPropagation()}
											onChange={branchEvent => handleCheck(branchEvent, n.id)}
										/>
									</TableCell>

									<TableCell className="whitespace-nowrap w-40 md:w-64" component="th" scope="row">
										{serialNumber++}
									</TableCell>

									<TableCell className="whitespace-nowrap p-4 md:p-16" component="th" scope="row">
										{n?.name}
									</TableCell>
									<TableCell className="whitespace-nowrap p-4 md:p-16" component="th" scope="row">
										{n?.is_active === true ? 'Actice' : 'Inactive'}
									</TableCell>

									<TableCell
										whitespace-nowrap
										className="p-4 md:p-16"
										align="center"
										component="th"
										scope="row"
									>
										<div>
											<Tooltip title="View Location" arrow>
												<IconButton
													disabled={!n?.map_location}
													onClick={() => {
														setOpen(true);
														setMapUrl(n);
													}}
													className="mx-8 cursor-pointer custom-edit-icon-style"
												>
													<Icon style={{ color: n?.map_location && '#22d3ee' }}>
														location_on
													</Icon>
												</IconButton>
											</Tooltip>
											<Tooltip title="Edit" placement="top" enterDelay={300}>
												<EditIcon
													onClick={branchEvent => handleUpdateBranch(n)}
													className="cursor-pointer"
													style={{ color: 'green' }}
												/>
											</Tooltip>

											<Tooltip title="Delete" placement="top" enterDelay={300}>
												<DeleteIcon
													onClick={event => handleDeleteBranch(n, 'Delete')}
													className="cursor-pointer"
													style={{
														color: 'red',
														visibility:
															user_role === 'ADMIN' || user_role === 'admin'
																? 'visible'
																: 'hidden'
													}}
												/>
											</Tooltip>
										</div>
									</TableCell>
								</TableRow>
							);
						})}
					</TableBody>
				</Table>
			</FuseScrollbars>
			<Modal
				open={open}
				onClose={handleClose}
				aria-labelledby="modal-modal-title"
				aria-describedby="modal-modal-description"
			>
				<Box className={classes.modal}>
					<Box style={{ margin: '10px', display: 'flex', alignItems: 'center', justifyContent: 'center' }}>
						<CloseIcon
							style={{ fontSize: '20px', cursor: 'pointer' }}
							className={classes.close}
							onClick={handleClose}
						/>
						<Typography variant="h6" className="text-center font-bold">
							CashConnect ({mapUrl?.name}) Branch Location
						</Typography>
					</Box>
					<Box style={{ display: 'flex', justifyContent: 'center', marginTop: '50px' }}>
						<iframe
							id="mapIframe"
							src={mapUrl?.map_location}
							width="800"
							height="300"
							style={{ border: 0 }}
							allowFullScreen=""
							loading="lazy"
							referrerPolicy="no-referrer-when-downgrade"
						></iframe>
					</Box>
				</Box>
			</Modal>

			<div className={classes.root} id="pagiContainer">
				<Pagination
					classes={{ ul: 'flex-nowrap' }}
					count={totalPages}
					page={page + 1}
					defaultPage={1}
					color="primary"
					showFirstButton
					showLastButton
					variant="outlined"
					shape="rounded"
					onChange={handlePagination}
				/>

				<TablePagination
					classes={{ root: 'overflow-visible' }}
					rowsPerPageOptions={rowsPerPageOptions}
					component="div"
					count={totalElements}
					rowsPerPage={rowsPerPage}
					page={page}
					className={classes.toolbar}
					backIconButtonProps={{
						'aria-label': 'Previous Page',
						className: 'py-0'
					}}
					nextIconButtonProps={{
						'aria-label': 'Next Page',
						className: 'py-0'
					}}
					onChangePage={handleChangePage}
					onChangeRowsPerPage={handleChangeRowsPerPage}
				/>
			</div>
		</div>
	);
};

export default withRouter(BranchsTable);
