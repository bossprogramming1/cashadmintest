import FusePageCarded from '@fuse/core/FusePageCarded';
import { BRANCH_LIST } from 'app/constant/permission/permission';
import { getUserPermissions } from 'app/store/dataSlice';
import withReducer from 'app/store/withReducer';
import React, { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import PagenotFound from '../../Pagenotfound/PagenotFound';
import reducer from '../store/index';
import BranchsHeader from './BranchsHeader';
import BranchsTable from './BranchsTable';

const Branchs = () => {
	const UserPermissions = useSelector(state => state.data.UserPermissions);
	useEffect(() => {
		dispatch(getUserPermissions());
	}, []);
	const dispatch = useDispatch();
	return (
		<FusePageCarded
			classes={{
				content: 'flex',
				contentCard: 'overflow-hidden',
				header: 'min-h-74 h-64'
			}}
			header={UserPermissions.includes(BRANCH_LIST) && <BranchsHeader />}
			content={UserPermissions.includes(BRANCH_LIST) ? <BranchsTable /> : <PagenotFound />}
			innerScroll
		/>
	);
};
export default withReducer('branchsManagement', reducer)(Branchs);
