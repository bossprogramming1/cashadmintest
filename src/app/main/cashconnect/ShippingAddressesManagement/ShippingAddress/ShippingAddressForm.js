import { Box } from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';
import TextField from '@material-ui/core/TextField';
import { Autocomplete } from '@material-ui/lab';
import React, { useEffect, useState } from 'react';
import { Controller, useFormContext } from 'react-hook-form';
import { useDispatch, useSelector } from 'react-redux';
import { useParams } from 'react-router-dom';
import FormControl from '@material-ui/core/FormControl';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Checkbox from '@material-ui/core/Checkbox';
import { getCities, getCountries, getThanas, getUsers } from 'app/store/dataSlice';
import countryCodes from 'app/@data/countrycodes';
import { GET_ORDERS } from '../../../../constant/constants';

const useStyles = makeStyles(theme => ({
	hidden: {
		display: 'none'
	}
}));

function ShippingAddressForm(props) {
	const dispatch = useDispatch();
	const users = useSelector(state => state.data.users);
	const thanas = useSelector(state => state.data.thanas);
	const cities = useSelector(state => state.data.cities);
	const countries = useSelector(state => state.data.countries);
	const userID = localStorage.getItem('UserID');
	const classes = useStyles(props);
	const methods = useFormContext();
	const { control, formState, watch } = methods;
	const { errors } = formState;
	const routeParams = useParams();
	const { shippingAddressId } = routeParams;
	const [orders, setOrders] = useState([]);
	const getCountryCode1 = watch('country_code1');
	useEffect(() => {
		dispatch(getUsers());
		dispatch(getThanas());
		dispatch(getCities());
		dispatch(getCountries());
	}, []);

	useEffect(() => {
		fetch(GET_ORDERS)
			.then(res => res.json())
			.then(data => setOrders(data.orders));
	}, []);

	return (
		<div>
			<Controller
				name="name"
				control={control}
				render={({ field }) => {
					return (
						<TextField
							{...field}
							autoFocus
							className="mt-8 mb-16"
							error={!!errors?.name}
							helperText={errors?.name?.message}
							label="Name"
							id="name"
							required
							variant="outlined"
							InputLabelProps={field.value && { shrink: true }}
							fullWidth
						/>
					);
				}}
			/>

			<Controller
				name="email"
				control={control}
				render={({ field }) => {
					return (
						<TextField
							{...field}
							className="mt-8 mb-16"
							error={!!errors.email}
							helperText={errors?.email?.message}
							label="Email"
							id="email"
							required
							variant="outlined"
							InputLabelProps={field.value && { shrink: true }}
							fullWidth
						/>
					);
				}}
			/>
			<Box style={{ display: 'flex' }}>
				<Controller
					name="country_code1"
					control={control}
					render={({ field: { onChange, value } }) => (
						<Autocomplete
							className="mt-8 mb-16"
							id="country-select-demo"
							sx={{ width: 300 }}
							value={value ? countryCodes.find(country => country.value === value) : null}
							options={countryCodes}
							autoHighlight
							getOptionLabel={option => option.label}
							renderOption={(prop, option) => {
								return (
									<Box component="li" sx={{ '& > img': { mr: 2, flexShrink: 0 } }} {...prop}>
										<img
											loading="lazy"
											width="20"
											src={`https://flagcdn.com/w20/${prop?.code?.toLowerCase()}.png`}
											srcSet={`https://flagcdn.com/w40/${prop?.code?.toLowerCase()}.png 2x`}
											alt=""
										/>
										{prop.label} ({prop.code}) +{prop.value}
									</Box>
								);
							}}
							onChange={(event, newValue) => {
								onChange(newValue?.value);
							}}
							renderInput={params => (
								<TextField
									{...params}
									label="Choose a country"
									variant="outlined"
									style={{ width: '150px' }}
									inputProps={{
										...params.inputProps,
										autoComplete: 'new-password' 
									}}
								/>
							)}
						/>
					)}
				/>
				<TextField
					name="show_country_code1"
					id="filled-read-only-input"
					label="Country Code"
					style={{ width: '150px' }}
					value={getCountryCode1 || ''}
					//defaultValue="Hello World"
					className="mt-8 mb-16"
					InputLabelProps={{ shrink: true }}
					InputProps={{
						readOnly: true
					}}
					variant="outlined"
				/>
				<Controller
					name="phone"
					control={control}
					render={({ field }) => (
						<TextField
							{...field}
							className="mt-8 mb-16"
							error={!!errors.phone}
							helperText={errors?.phone?.message}
							//onBlur={(event) => handleOnChange("phone", event)}
							label="Phone"
							id="primary_phone"
							variant="outlined"
							fullWidth
							InputLabelProps={{ shrink: true }}
						/>
					)}
				/>
			</Box>
			{/* <Controller
        name="phone"
        control={control}
        render={({ field }) => {
          return (<TextField
            {...field}
            className="mt-8 mb-16"
            error={!!errors.phone}
            helperText={errors?.phone?.message}
            label="Phone"
            id="phone"
            required
            variant="outlined"
            InputLabelProps={field.value && { shrink: true }}
            fullWidth
          />)
        }}
      /> */}

			<Controller
				name="shipping_price"
				control={control}
				render={({ field }) => {
					return (
						<TextField
							{...field}
							className="mt-8 mb-16"
							error={!!errors.shipping_price}
							helperText={errors?.shipping_price?.message}
							label="Shipping Price"
							id="shipping_price"
							required
							variant="outlined"
							InputLabelProps={field.value && { shrink: true }}
							fullWidth
						/>
					);
				}}
			/>

			{/* <Controller
				name="company"
				control={control}
				render={({ field }) => {
					return (
						<TextField
							{...field}
							className="mt-8 mb-16"
							error={!!errors.company}
							helperText={errors?.company?.message}
							label="Company"
							id="company"
							required
							variant="outlined"
							InputLabelProps={field.value && { shrink: true }}
							fullWidth
						/>
					);
				}}
			/> */}

			<Controller
				name="street_address"
				control={control}
				render={({ field }) => {
					return (
						<TextField
							{...field}
							className="mt-8 mb-16"
							error={!!errors.street_address}
							helperText={errors?.street_address?.message}
							label="Billing Address"
							id="street_address"
							required
							variant="outlined"
							InputLabelProps={field.value && { shrink: true }}
							fullWidth
						/>
					);
				}}
			/>

			<Controller
				name="zip_code"
				control={control}
				render={({ field }) => {
					return (
						<TextField
							{...field}
							className="mt-8 mb-16"
							error={!!errors.zip_code}
							helperText={errors?.zip_code?.message}
							label="Zip Code"
							id="zip_code"
							required
							variant="outlined"
							InputLabelProps={field.value && { shrink: true }}
							fullWidth
						/>
					);
				}}
			/>
			<Controller
				name="order"
				control={control}
				render={({ field: { onChange, value } }) => (
					<Autocomplete
						className="mt-8 mb-16"
						freeSolo
						value={value ? orders?.find(order => order?.id === value) : null}
						options={orders}
						getOptionLabel={option => `${option?.order_no}`}
						InputLabelProps={{ shrink: true }}
						onChange={(event, newValue) => {
							onChange(newValue?.id);
						}}
						//value={employee && employee.branch}
						//defaultValue={{ id: null, name: "Select a branch" }}
						renderInput={params => (
							<TextField
								{...params}
								placeholder="Select a order"
								label="Order"
								error={!!errors?.order}
								required
								helperText={errors?.order?.message}
								variant="outlined"
								InputLabelProps={{
									shrink: true
								}}
							/>
						)}
					/>
				)}
			/>
			<Controller
				name="user"
				control={control}
				render={({ field: { onChange, value } }) => (
					<Autocomplete
						className="mt-8 mb-16"
						freeSolo
						value={value ? users.find(user => user.id === value) : null}
						options={users}
						getOptionLabel={option => `${option.email}`}
						InputLabelProps={{ shrink: true }}
						onChange={(event, newValue) => {
							onChange(newValue?.id);
						}}
						//value={employee && employee.branch}
						//defaultValue={{ id: null, name: "Select a branch" }}
						renderInput={params => (
							<TextField
								{...params}
								placeholder="Select a user"
								label="User"
								error={!!errors.user}
								required
								helperText={errors?.user?.message}
								variant="outlined"
								InputLabelProps={{
									shrink: true
								}}
							/>
						)}
					/>
				)}
			/>
			<Controller
				name="country"
				control={control}
				render={({ field: { onChange, value } }) => (
					<Autocomplete
						className="mt-8 mb-16"
						freeSolo
						value={value ? countries.find(country => country.id === value) : null}
						options={countries}
						getOptionLabel={option => `${option?.name}`}
						onChange={(event, newValue) => {
							onChange(newValue?.id);
						}}
						//defaultValue={{ id: null, name: "Select a country" }}
						renderInput={params => (
							<TextField
								{...params}
								placeholder="Select a country"
								label="Country"
								variant="outlined"
								required
								InputLabelProps={{
									shrink: true
								}}
							/>
						)}
					/>
				)}
			/>

			<Controller
				name="city"
				control={control}
				render={({ field: { onChange, value } }) => (
					<Autocomplete
						className="mt-8 mb-16"
						freeSolo
						value={value ? cities.find(city => city.id === value) : null}
						options={cities}
						getOptionLabel={option => `${option?.name}`}
						onChange={(event, newValue) => {
							onChange(newValue?.id);
						}}
						//defaultValue={{ id: null, name: "Select a city" }}
						renderInput={params => (
							<TextField
								{...params}
								placeholder="Select a city"
								label="District"
								variant="outlined"
								required
								InputLabelProps={{
									shrink: true
								}}
							/>
						)}
					/>
				)}
			/>
			<Controller
				name="thana"
				control={control}
				render={({ field: { onChange, value } }) => (
					<Autocomplete
						className="mt-8 mb-16"
						freeSolo
						value={value ? thanas.find(thana => thana.id === value) : null}
						options={thanas}
						getOptionLabel={option => `${option?.name}`}
						//defaultValue={{ id: null, name: "Select a thana" }}
						onChange={(event, newValue) => {
							onChange(newValue?.id);
						}}
						renderInput={params => (
							<TextField
								{...params}
								placeholder="Select a police station"
								label="Police Station"
								variant="outlined"
								required
								InputLabelProps={{
									shrink: true
								}}
								InputProps={{ ...params.InputProps, type: 'search' }}
							/>
						)}
					/>
				)}
			/>
			<Controller
				name="same_as_billing"
				control={control}
				render={({ field }) => (
					<FormControl>
						<FormControlLabel
							label="Shipping Address Same as Billing Address"
							control={<Checkbox {...field} checked={field.value ? field.value : false} />}
						/>
					</FormControl>
				)}
			/>
		</div>
	);
}

export default ShippingAddressForm;
