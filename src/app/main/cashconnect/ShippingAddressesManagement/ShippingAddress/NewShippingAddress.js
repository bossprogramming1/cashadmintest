import FusePageCarded from '@fuse/core/FusePageCarded';
import { useDeepCompareEffect } from '@fuse/hooks';
import { yupResolver } from '@hookform/resolvers/yup';
import { Button, Typography } from '@material-ui/core';
import {
	SHIPPING_ADDRESS_CREATE,
	SHIPPING_ADDRESS_DELETE,
	SHIPPING_ADDRESS_DETAILS,
	SHIPPING_ADDRESS_UPDATE
} from 'app/constant/permission/permission';
import { getUserPermissions } from 'app/store/dataSlice';
import withReducer from 'app/store/withReducer';
import { motion } from 'framer-motion';
import React, { useEffect, useState } from 'react';
import { FormProvider, useForm } from 'react-hook-form';
import { useDispatch, useSelector } from 'react-redux';
import { Link, useParams } from 'react-router-dom';
import * as yup from 'yup';
import PagenotFound from '../../Pagenotfound/PagenotFound';
import reducer from '../store/index';
import { getShippingAddress, newShippingAddress, resetShippingAddress } from '../store/shippingAddressSlice';
import NewShippingAddressHeader from './NewShippingAddressHeader';
import ShippingAddressForm from './ShippingAddressForm';

/**
 * Form Validation Schema
 */
const schema = yup.object().shape({
	name: yup.string().required('Name is required'),
	email: yup.string().required('Email is required'),
	phone: yup.string().required('Phone is required'),
	street_address: yup.string().required('Address is required'),
	order: yup.number().required('Order is required')
});

const ShippingAddress = () => {
	const dispatch = useDispatch();
	const shippingAddress = useSelector(
		({ shippingAddressesManagement }) => shippingAddressesManagement.shippingAddress
	);

	const [noShippingAddress, setNoShippingAddress] = useState(false);
	const methods = useForm({
		mode: 'onChange',
		defaultValues: {},
		resolver: yupResolver(schema)
	});
	const routeParams = useParams();
	const UserPermissions = useSelector(state => state.data.UserPermissions);
	useEffect(() => {
		dispatch(getUserPermissions());
	}, []);
	const { reset, watch, control, onChange, formState } = methods;
	const form = watch();

	useDeepCompareEffect(() => {
		function updateShippingAddressState() {
			const { shippingAddressId } = routeParams;
			if (shippingAddressId === 'new') {
				localStorage.removeItem('event');
				/**
				 * Create New User data
				 */
				dispatch(newShippingAddress());
			} else {
				/**
				 * Get User data
				 */

				dispatch(getShippingAddress(shippingAddressId)).then(action => {
					/**
					 * If the requested product is not exist show message
					 */
					if (!action.payload) {
						setNoShippingAddress(true);
					}
				});
			}
		}

		updateShippingAddressState();
	}, [dispatch, routeParams]);

	useEffect(() => {
		if (!shippingAddress) {
			return;
		}
		/**
		 * Reset the form on shippingAddress state changes
		 */
		reset({
			...shippingAddress,
			city: shippingAddress?.city?.id,
			country: shippingAddress?.country?.id,
			order: shippingAddress?.order?.id,
			thana: shippingAddress?.thana?.id,
			user: shippingAddress?.user?.id,
			country_code1: '+880',
			show_country_code1: '+880'
		});
	}, [shippingAddress, reset]);

	useEffect(() => {
		return () => {
			/**
			 * Reset ShippingAddress on component unload
			 */
			dispatch(resetShippingAddress());
			setNoShippingAddress(false);
		};
	}, [dispatch]);

	/**
	 * Show Message if the requested products is not exists
	 */
	if (noShippingAddress) {
		return (
			<motion.div
				initial={{ opacity: 0 }}
				animate={{ opacity: 1, transition: { delay: 0.1 } }}
				className="flex flex-col flex-1 items-center justify-center h-full"
			>
				<Typography color="textSecondary" variant="h5">
					There is no such shippingAddress!
				</Typography>
				<Button
					className="mt-24"
					component={Link}
					variant="outlined"
					to="/apps/e-commerce/products"
					color="inherit"
				>
					Go to ShippingAddress Page
				</Button>
			</motion.div>
		);
	}

	return (
		<FormProvider {...methods}>
			{UserPermissions.includes(SHIPPING_ADDRESS_CREATE) ||
			UserPermissions.includes(SHIPPING_ADDRESS_UPDATE) ||
			UserPermissions.includes(SHIPPING_ADDRESS_DELETE) ||
			UserPermissions.includes(SHIPPING_ADDRESS_DETAILS) ? (
				<FusePageCarded
					classes={{
						toolbar: 'p-0',
						header: 'min-h-74 h-64'
					}}
					header={<NewShippingAddressHeader />}
					content={
						<div className="p-16 sm:p-24">
							<ShippingAddressForm />
						</div>
					}
					innerScroll
				/>
			) : (
				<PagenotFound />
			)}
		</FormProvider>
	);
};
export default withReducer('shippingAddressesManagement', reducer)(ShippingAddress);
