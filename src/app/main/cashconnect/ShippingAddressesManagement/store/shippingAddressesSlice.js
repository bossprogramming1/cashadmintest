import { createAsyncThunk, createEntityAdapter, createSlice } from '@reduxjs/toolkit';
import axios from 'axios';
import { DELETE_BILLING_ADDRESS, GET_BILLING_ADDRESSS } from '../../../../constant/constants';

export const getShippingAddresses = createAsyncThunk(
	'shippingAddressManagement/shippingAddresses/geShippingAddresses',
	async pageAndSize => {
		axios.defaults.headers.common['Content-type'] = 'application/json';
		axios.defaults.headers.common.Authorization = localStorage.getItem('jwt_access_token');

		const response = axios.get(GET_BILLING_ADDRESSS, { params: pageAndSize });
		const data = await response;

		sessionStorage.setItem('total_shippingaddress_elements', data.data.total_elements);
		sessionStorage.setItem('total_shippingaddress_pages', data.data.total_pages);
		delete axios.defaults.headers.common['Content-type'];
		delete axios.defaults.headers.common.Authorization;

		return data.data.shippingaddresses;
	}
);

export const removeShippingAddresses = createAsyncThunk(
	'shippingAddressManagement/shippingAddresses/removeShippingAddresses',
	async (shippingAddressIds, { dispatch, getState }) => {
		const authTOKEN = {
			headers: {
				'Content-type': 'application/json',
				Authorization: localStorage.getItem('jwt_access_token')
			}
		};
		await axios.delete(`${DELETE_BILLING_ADDRESS}`, { shippingAddressIds }, authTOKEN);

		return shippingAddressIds;
	}
);

const shippingAddressesAdapter = createEntityAdapter({});

export const { selectAll: selectShippingAddresses, selectById: selectShippingAddresesById } =
	shippingAddressesAdapter.getSelectors(state => state.shippingAddressesManagement.shippingAddresses);

const shippingAddressesSlice = createSlice({
	name: 'shippingAddressManagement/shippingAddresses',
	initialState: shippingAddressesAdapter.getInitialState({
		searchText: ''
	}),
	reducers: {
		setShippingAddressesSearchText: {
			reducer: (state, action) => {
				state.searchText = action.payload;
			},
			prepare: event => ({ payload: event.target.value || '' })
		}
	},
	extraReducers: {
		[getShippingAddresses.fulfilled]: shippingAddressesAdapter.setAll
	}
});

export const { setData, setShippingAddressesSearchText } = shippingAddressesSlice.actions;
export default shippingAddressesSlice.reducer;
