import { combineReducers } from '@reduxjs/toolkit';
import shippingAddresses from './shippingAddressesSlice';
import shippingAddress from './shippingAddressSlice';

const reducer = combineReducers({
	shippingAddress,
	shippingAddresses,
});

export default reducer;