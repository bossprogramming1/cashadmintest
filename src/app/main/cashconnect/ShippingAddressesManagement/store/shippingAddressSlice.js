import { createAsyncThunk, createSlice } from '@reduxjs/toolkit';
import axios from 'axios';
import {
	CREATE_BILLING_ADDRESS,
	DELETE_BILLING_ADDRESS,
	GET_BILLING_ADDRESS_ID,
	UPDATE_BILLING_ADDRESSS
} from '../../../../constant/constants';

export const getShippingAddress = createAsyncThunk(
	'shippingAddressManagement/shippingAddress/getShippingAddress',
	async (params, { rejectWithValue }) => {
		const authTOKEN = {
			headers: {
				'Content-type': 'application/json',
				Authorization: localStorage.getItem('jwt_access_token')
			}
		};

		try {
			const response = await axios.get(`${GET_BILLING_ADDRESS_ID}${params}`, authTOKEN);
			const data = await response.data;
			return data === undefined ? null : data;
		} catch (err) {
			return rejectWithValue(params);
		}
	}
);

export const removeShippingAddress = createAsyncThunk(
	'shippingAddressManagement/shippingAddress/removeShippingAddress',
	async (val, { dispatch, getState }) => {
		const authTOKEN = {
			headers: {
				'Content-type': 'application/json',
				Authorization: localStorage.getItem('jwt_access_token')
			}
		};

		const shippingAddressId = val.id;
		await axios.delete(`${DELETE_BILLING_ADDRESS}${shippingAddressId}`, authTOKEN);
	}
);

export const updateShippingAddress = createAsyncThunk(
	'shippingAddressManagement/shippingAddress/updateShippingAddress',
	async (shippingAddressData, { dispatch, getState }) => {
		const { shippingAddress } = getState().shippingAddressesManagement;

		const authTOKEN = {
			headers: {
				'Content-type': 'application/json',
				Authorization: localStorage.getItem('jwt_access_token')
			}
		};
		const response = await axios.put(
			`${UPDATE_BILLING_ADDRESSS}${shippingAddress.id}`,
			shippingAddressData,
			authTOKEN
		);
	}
);

export const saveShippingAddress = createAsyncThunk(
	'shippingAddressManagement/shippingAddress/saveShippingAddress',
	async (shippingAddressData, { dispatch, getState }) => {
		const authTOKEN = {
			headers: {
				'Content-type': 'application/json',
				Authorization: localStorage.getItem('jwt_access_token')
			}
		};
		const response = await axios.post(`${CREATE_BILLING_ADDRESS}`, shippingAddressData, authTOKEN);
	}
);

const shippingAddressSlice = createSlice({
	name: 'shippingAddressManagement/shippingAddress',
	initialState: null,
	reducers: {
		resetShippingAddress: () => null,
		newShippingAddress: {
			reducer: (state, action) => action.payload,
			prepare: event => ({
				payload: {
					name: '',
					email: '',
					phone: '',
					shipping_price: '',
					company: '',
					street_address: '',
					zip_code: '',
					order: 0,
					user: 0,
					thana: 0,
					city: 0,
					country: 0,
					country_code1: '+880',
					show_country_code1: '+880'
				}
			})
		}
	},
	extraReducers: {
		[getShippingAddress.fulfilled]: (state, action) => action.payload,
		[saveShippingAddress.fulfilled]: (state, action) => {
			localStorage.setItem('shippingAddressAlert', 'saveShippingAddress');
			return action.payload;
		},
		[removeShippingAddress.fulfilled]: () => {
			localStorage.setItem('shippingAddressAlert', 'deleteShippingAddress');
		},
		[updateShippingAddress.fulfilled]: () => {
			localStorage.setItem('shippingAddressAlert', 'updateShippingAddress');
		}
	}
});

export const { newShippingAddress, resetShippingAddress } = shippingAddressSlice.actions;

export default shippingAddressSlice.reducer;
