import FusePageCarded from '@fuse/core/FusePageCarded';
import { BILLING_ADDRESS_LIST } from 'app/constant/permission/permission';
import { getUserPermissions } from 'app/store/dataSlice';
import withReducer from 'app/store/withReducer';
import React, { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import PagenotFound from '../../Pagenotfound/PagenotFound';
import reducer from '../store/index';
import ShippingAddressesHeader from './ShippingAddressesHeader';
import ShippingAddressesTable from './ShippingAddressesTable';

const ShippingAddresss = () => {
	const UserPermissions = useSelector(state => state.data.UserPermissions);
	useEffect(() => {
		dispatch(getUserPermissions());
	}, []);
	const dispatch = useDispatch();
	return (
		<FusePageCarded
			classes={{
				content: 'flex',
				contentCard: 'overflow-hidden',
				header: 'min-h-74 h-64'
			}}
			header={UserPermissions.includes(BILLING_ADDRESS_LIST) && <ShippingAddressesHeader />}
			content={UserPermissions.includes(BILLING_ADDRESS_LIST) ? <ShippingAddressesTable /> : <PagenotFound />}
			innerScroll
		/>
	);
};
export default withReducer('shippingAddressesManagement', reducer)(ShippingAddresss);
