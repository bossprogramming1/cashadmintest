import FusePageCarded from '@fuse/core/FusePageCarded';
import { ORDER_LIST } from 'app/constant/permission/permission';
import { getUserPermissions } from 'app/store/dataSlice';
import withReducer from 'app/store/withReducer';
import React, { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import PagenotFound from '../../Pagenotfound/PagenotFound';
import reducer from '../store/index';
import OrdersHeader from './OrdersHeader';
import OrdersTable from './OrdersTable';

const Orders = () => {
	const UserPermissions = useSelector(state => state.data.UserPermissions);
	useEffect(() => {
		dispatch(getUserPermissions());
	}, []);
	const dispatch = useDispatch();
	return (
		<FusePageCarded
			classes={{
				content: 'flex',
				contentCard: 'overflow-hidden',
				header: 'min-h-74 h-64'
			}}
			header={UserPermissions.includes(ORDER_LIST) && <OrdersHeader />}
			content={UserPermissions.includes(ORDER_LIST) ? <OrdersTable /> : <PagenotFound />}
			innerScroll
		/>
	);
};
export default withReducer('ordersManagement', reducer)(Orders);
