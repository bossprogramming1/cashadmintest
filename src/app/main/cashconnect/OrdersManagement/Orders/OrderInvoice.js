import React, { useEffect, useState, useRef } from 'react';
import { makeStyles, styled, useTheme } from '@material-ui/core/styles';
import { withRouter, useParams, useHistory } from 'react-router-dom';
import Paper from '@material-ui/core/Paper';
import { Box, Typography, Button, Divider } from '@material-ui/core';
import TableContainer from '@material-ui/core/TableContainer';
import PrintIcon from '@material-ui/icons/Print';
import ArrowBackIcon from '@mui/icons-material/ArrowBack';
import { format } from 'date-fns';
//import ReactToPrint from 'react-to-print';
import { useReactToPrint } from 'react-to-print';
import Table from '@material-ui/core/Table';
import TableHead from '@material-ui/core/TableHead';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableRow from '@material-ui/core/TableRow';
import Barcode from 'react-barcode';
// import { useBarcode } from '@createnextapp/react-barcode';

import { BASE_URL, GET_INVOICE_BY_ORDERID, GET_SITESETTINGS } from 'app/constant/constants';

const useStyles = makeStyles(theme => ({
	hidden: {
		display: 'none'
	},
	tblContainer: {
		borderRadius: '0px',
		width: '700px',
		marginLeft: '150px',
		marginBottom: '50px'
	},
	table: {
		minWidth: 700
	},
	tableHead: {
		backgroundColor: theme.palette.primary[500]
	},
	dateOfSaleContainer: {
		display: 'flex',
		flexWrap: 'nowrap',
		justifyContent: 'space-between',
		minWidth: '250px'
	},
	paper: {
		marginTop: '10%',
		backgroundColor: theme.palette.background.paper,
		//border: `2px solid ${theme.palette.grey[800]}`,
		boxShadow: theme.shadows[5],
		padding: theme.spacing(2, 4, 3)
	},
	underline: {
		borderBottom: '1px solid grey',
		marginTop: '4%',
		marginBottom: '1%'
	},
	textMarginBottom: {
		marginBottom: '2%'
	}
}));

const barcodeConfig2 = {
	width: 1,
	height: 40,
	margin: 0,
	format: 'CODE128',
	displayValue: false
};

const OrderInvoice = props => {
	const [orderInvoice, setOrderInvoice] = useState({});
	const routeParams = useParams();
	const { orderId } = routeParams;
	const classes = useStyles(props);
	const Theme = useTheme();
	const PaperColor = Theme.palette.background.paper;
	const [showPrintBtn, setShowPrintBtn] = useState(true);
	const [generalData, setGeneralData] = useState({});
	let serialNumber = 1;
	const authTOKEN = {
		headers: {
			'Content-type': 'application/json',
			Authorization: localStorage.getItem('jwt_access_token')
		}
	};
	const history = useHistory();

	useEffect(() => {
		fetch(`${GET_INVOICE_BY_ORDERID}${orderId}`, authTOKEN)
			.then(response => response.json())
			.then(data => setOrderInvoice(data));
	}, []);

	//get general setting data
	useEffect(() => {
		const authTOKEN = {
			headers: {
				'Content-type': 'application/json',
				Authorization: localStorage.getItem('jwt_access_token')
			}
		};

		fetch(`${GET_SITESETTINGS}`, authTOKEN)
			.then(response => response.json())
			.then(data => setGeneralData(data.general_settings[0] || {}))
			.catch(() => setGeneralData({}));
	}, []);

	//print
	const componentRef = useRef();
	const handlePrint = useReactToPrint({
		content: () => componentRef.current
	});

	return (
		<>
			<Box
				style={{
					display: 'flex',
					justifyContent: 'space-between',
					flexDirection: 'row',
					marginRight: '150px',
					marginLeft: '150px'
				}}
			>
				<Button onClick={() => history.goBack()} style={{ fontSize: '20px' }}>
					<ArrowBackIcon
						className="h-52 cursor-pointer"
						style={{
							color: 'black',
							textAlign: 'left',

							height: '30px',
							width: '30px'
						}}
					/>
					Go Back
				</Button>
				<PrintIcon
					onClick={handlePrint}
					className="h-52 cursor-pointer"
					style={{
						color: '#6495ED',
						textAlign: 'right',
						height: '30px',
						width: '30px'
					}}
				/>
			</Box>
			<div
				ref={componentRef}
				style={{
					width: '100%',
					// marginLeft: 'auto',
					// marginRight: 'auto',
					padding: '6vh',
					fontSize: '10px'
				}}
			>
				<div
					style={{
						display: 'flex',
						justifyContent: 'space-between',
						bgcolor: 'background.paper'
					}}
				>
					<Box style={{ textAlign: 'left' }}>
						<Box style={{ textAlign: 'right' }}>
							<img
								style={{ height: '30px', width: '100px' }}
								src={`${BASE_URL}${generalData?.logo}`}
								alt="Not found"
							/>
						</Box>
						<Box>
							<Typography style={{ fontSize: '10px' }} variant="h6" component="div">
								{orderInvoice?.bill_from?.site_name}
							</Typography>
							<Typography
								style={{ fontSize: '10px', overflowWrap: ' break-word' }}
								variant="P"
								component="div"
							>
								{orderInvoice?.bill_from?.address}
							</Typography>
							<Typography style={{ fontSize: '10px' }} variant="P" component="div">
								Email: {orderInvoice?.bill_from?.email}
							</Typography>
							<Typography style={{ fontSize: '10px' }} variant="P" component="div">
								Phone: {orderInvoice?.bill_from?.phone}
							</Typography>
							<Typography style={{ fontSize: '10px' }} variant="P" component="div">
								Website: {orderInvoice?.bill_from?.site_address}
							</Typography>
						</Box>
					</Box>
					<Box>
						<Typography
							style={{ textAlign: 'right', marginTop: '-16px', marginRight: '5px', fontSize: '12px' }}
							variant="h5"
							component="div"
						>
							INVOICE
						</Typography>
						<Barcode value={orderInvoice?.order?.order_no} {...barcodeConfig2} />

						<div
							style={{
								display: 'flex',
								justifyContent: 'space-between',
								alignItems: 'center'
							}}
						>
							<Box style={{ textAlign: 'left' }}>
								<Typography style={{ fontSize: '10px' }} variant="P" component="div">
									INVOICE
								</Typography>
								<Typography style={{ fontSize: '10px' }} variant="P" component="div">
									ORDER DATE
								</Typography>
								<Typography style={{ fontSize: '10px' }} variant="P" component="div">
									INVOICE DATE
								</Typography>
								<Typography style={{ fontSize: '10px' }} variant="P" component="div">
									PAYMENT TYPE
								</Typography>
								{/* <Typography style={{ fontSize: '10px' }} variant="P" component="div">
									STATUS
								</Typography> */}
								{/* <Typography
									variant="P"
									component="div"
									style={{
										backgroundColor: '#D7DBDD',
										padding: '5px',
										fontWeight: '600',
										fontSize: '10px'
									}}
								>
									AMOUNT DUE
								</Typography> */}
							</Box>
							<Box style={{ textAlign: 'right' }}>
								<Typography style={{ fontSize: '10px' }} variant="P" component="div">
									{orderInvoice?.order?.order_no || 'No Order Found'}
								</Typography>
								<Typography style={{ fontSize: '10px' }} variant="P" component="div">
									{(orderInvoice?.order?.order_date &&
										format(new Date(orderInvoice?.order?.order_date), 'dd-MM-yyyy')) ||
										'No Date Found'}
								</Typography>
								<Typography style={{ fontSize: '10px' }} variant="P" component="div">
									{(orderInvoice?.order?.delivered_at &&
										format(new Date(orderInvoice?.order?.delivered_at), 'dd-MM-yyyy')) ||
										'No Date Found'}
								</Typography>
								<Typography style={{ fontSize: '10px' }} variant="P" component="div">
									{orderInvoice?.order?.payment_method?.name.toUpperCase()}
								</Typography>
								{/* <Typography style={{ fontSize: '10px' }} variant="P" component="div">
									{orderInvoice?.order?.order_status?.name.toUpperCase()}
								</Typography> */}
								{/* <Typography
									variant="P"
									component="div"
									style={{
										backgroundColor: '#D7DBDD',
										padding: '5px',
										fontWeight: '600',
										fontSize: '10px'
									}}
								>
									{orderInvoice?.order?.net_amount - orderInvoice?.order?.pay_amount || 0.0}
								</Typography> */}
							</Box>
						</div>
					</Box>
				</div>
				<Divider style={{ marginTop: '10px', marginBottom: '10px' }} />

				<div
					style={{
						display: 'flex',
						marginBottom: '10px',
						bgcolor: 'background.paper'
					}}
				>
					<Box>
						<Typography style={{ fontSize: '10px' }} variant="h6" component="div">
							Shipping Address
						</Typography>
						<Typography style={{ fontSize: '10px' }} variant="P" component="div">
							Name: {orderInvoice?.shipping?.name || 'No Name Found'}
						</Typography>
						<Typography
							style={{ fontSize: '10px', overflowWrap: ' break-word' }}
							variant="P"
							component="div"
						>
							Address: {orderInvoice?.shipping?.street_address}, {orderInvoice?.shipping?.thana?.name},{' '}
							{orderInvoice?.shipping?.city?.name}
						</Typography>
						<Typography style={{ fontSize: '10px' }} variant="P" component="div">
							Phone: {orderInvoice?.shipping?.phone}
						</Typography>
						<Typography style={{ fontSize: '10px' }} variant="P" component="div">
							Email: {orderInvoice?.shipping?.email}
						</Typography>
					</Box>
					<Box style={{ marginLeft: '150px' }}>
						<Typography style={{ fontSize: '10px' }} variant="h6" component="div">
							Billing Address
						</Typography>
						<Typography style={{ fontSize: '10px' }} variant="P" component="div">
							Name: {orderInvoice?.billing?.name || 'No Name Found'}
						</Typography>
						<Typography
							style={{ fontSize: '10px', overflowWrap: ' break-word' }}
							variant="P"
							component="div"
						>
							Address: {orderInvoice?.billing?.street_address}, {orderInvoice?.billing?.thana?.name},{' '}
							{orderInvoice?.billing?.city?.name}
						</Typography>
						<Typography style={{ fontSize: '10px' }} variant="P" component="div">
							Phone: {orderInvoice?.billing?.phone}
						</Typography>
						<Typography style={{ fontSize: '10px' }} variant="P" component="div">
							Email: {orderInvoice?.billing?.email}
						</Typography>
					</Box>
				</div>

				<Table
					aria-label="simple table"
					style={{
						width: '100%',
						marginLeft: 'auto',
						marginRight: 'auto',
						marginBottom: '50px'
					}}
				>
					<TableHead style={{ backgroundColor: '#D7DBDD' }}>
						<TableRow>
							<TableCell
								style={{
									fontSize: '10px',
									lineHeight: '0.228571rem;',
									padding: 0,
									borderRight: '1px solid white'
								}}
								align="center"
							>
								Sl_No
							</TableCell>
							<TableCell
								style={{
									fontSize: '10px',
									lineHeight: '0.228571rem;',
									padding: 0,
									borderRight: '1px solid white'
								}}
								align="center"
							>
								Item
							</TableCell>

							<TableCell
								style={{
									fontSize: '10px',
									lineHeight: '0.228571rem;',
									padding: 0,
									borderRight: '1px solid white'
								}}
								align="center"
							>
								Description
							</TableCell>
							{orderInvoice?.order_items?.map((item, idx) => (
								<TableCell
									style={{
										display: item.color ? 'table-cell' : 'none',
										fontSize: '10px',
										lineHeight: '0.228571rem;',
										padding: 0,
										borderRight: '1px solid white'
									}}
									align="center"
								>
									Color & Size
								</TableCell>
							))}
							<TableCell
								style={{
									fontSize: '10px',
									lineHeight: '0.228571rem;',
									padding: 0,
									borderRight: '1px solid white'
								}}
								align="center"
							>
								Quantity
							</TableCell>
							<TableCell
								style={{
									fontSize: '10px',
									lineHeight: '0.228571rem;',
									padding: 0,
									borderRight: '1px solid white'
								}}
								align="center"
							>
								Unit Cost
							</TableCell>
							<TableCell
								style={{
									fontSize: '10px',
									lineHeight: '0.228571rem;',
									padding: 0,
									borderRight: '1px solid white'
								}}
								align="center"
							>
								Line Total
							</TableCell>
						</TableRow>
					</TableHead>
					<TableBody>
						{orderInvoice?.order_items?.map((item, idx) => (
							<TableRow id={idx}>
								<TableCell style={{ fontSize: '10px', width: '35px' }} align="center">
									{serialNumber++}
								</TableCell>
								<TableCell style={{ fontSize: '10px', width: '70px' }} align="center">
									<img
										style={{ height: '40px', width: '40px' }}
										src={`${BASE_URL}${item?.product?.thumbnail}`}
										alt={item?.product?.name}
									/>
								</TableCell>
								<TableCell style={{ fontSize: '10px' }} align="center">
									{item?.product?.name}
								</TableCell>
								<TableCell
									style={{ display: item.color ? 'table-cell' : 'none', fontSize: '10px' }}
									align="center"
								>
									{`${item?.color?.name} ${item?.size ? `[${item?.size?.name}]` : ''}`}
								</TableCell>
								<TableCell style={{ fontSize: '10px' }} align="center">
									{item?.quantity}
								</TableCell>
								<TableCell style={{ fontSize: '10px' }} align="center">
									{item?.price}
								</TableCell>
								<TableCell style={{ fontSize: '10px' }} align="center">
									{item?.subtotal}
								</TableCell>
							</TableRow>
						))}
					</TableBody>
				</Table>
				<div
					style={{
						display: 'flex',
						justifyContent: 'space-between',
						marginTop: '50px',
						marginBottom: '50px',
						// marginBottom: '50px',
						bgcolor: 'background.paper'
					}}
				>
					<Box style={{ visibility: 'hidden' }}>
						<Typography variant="h6" component="div" style={{ fontWeight: '700', fontSize: '10px' }}>
							NOTES/MEMO
						</Typography>
						<Typography variant="P" component="div" style={{ fontSize: '10px' }}>
							Free Shipping with 30-day money-back guarantee.
						</Typography>
					</Box>
					<Box
						style={{
							display: 'flex',
							justifyContent: 'space-between',
							alignItems: 'center'
						}}
					>
						<Box style={{ textAlign: 'left', fontSize: '10px' }}>
							<Typography variant="P" component="div">
								SUBTOTAL
							</Typography>
							<Typography style={{ fontSize: '10px' }} variant="P" component="div">
								TAX
							</Typography>
							<Typography style={{ fontSize: '10px' }} variant="P" component="div">
								SHIPPING FEE
							</Typography>
							<Typography
								variant="P"
								component="div"
								style={{
									backgroundColor: '#D7DBDD',
									padding: '5px',
									fontWeight: '600',
									fontSize: '10px'
								}}
							>
								TOTAL
							</Typography>
						</Box>
						<Box style={{ textAlign: 'right', fontSize: '10px' }}>
							<Typography variant="P" component="div">
								{orderInvoice?.order?.net_amount}
							</Typography>
							<Typography style={{ fontSize: '10px' }} variant="P" component="div">
								{orderInvoice?.order?.tax_price}
							</Typography>
							<Typography style={{ fontSize: '10px' }} variant="P" component="div">
								{orderInvoice?.order?.tax_price}
							</Typography>
							<Typography
								variant="P"
								component="div"
								style={{
									backgroundColor: '#D7DBDD',
									padding: '5px',
									fontWeight: '600',
									fontSize: '10px'
								}}
							>
								{orderInvoice?.order?.grand_total === '0.00'
									? orderInvoice?.order?.net_amount
									: orderInvoice?.order?.grand_total}
							</Typography>
						</Box>
					</Box>
				</div>
			</div>
		</>
	);
};
export default withRouter(OrderInvoice);
