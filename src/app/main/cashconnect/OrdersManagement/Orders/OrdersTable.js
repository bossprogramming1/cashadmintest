/* eslint-disable import/extensions */
import FuseLoading from '@fuse/core/FuseLoading';
import FuseScrollbars from '@fuse/core/FuseScrollbars';
import _ from '@lodash';
import Checkbox from '@material-ui/core/Checkbox';
import { makeStyles } from '@material-ui/core/styles';
import ReceiptIcon from '@material-ui/icons/Receipt';
import EmailOutlinedIcon from '@material-ui/icons/EmailOutlined';
import PrintIcon from '@material-ui/icons/Print';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TablePagination from '@material-ui/core/TablePagination';
import TableRow from '@material-ui/core/TableRow';
import DeleteIcon from '@material-ui/icons/Delete';
import EditIcon from '@material-ui/icons/Edit';
import { motion } from 'framer-motion';
import Pagination from '@material-ui/lab/Pagination';
import clsx from 'clsx';
import React, { useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { withRouter, useParams } from 'react-router-dom';
import { getBranches, getOrdersStatus, getUsers } from 'app/store/dataSlice';
import { Icon, IconButton, Tooltip, Typography } from '@material-ui/core';
import { SEARCH_ORDER } from '../../../../constant/constants';
import { getOrders, selectOrders, setOrdersSearchText, getPaidOrders, getUnPaidOrders } from '../store/ordersSlice';
import OrdersTableHead from './OrdersTableHead';

const useStyles = makeStyles(theme => ({
	root: {
		display: 'flex',
		justifyContent: 'space-between',
		flexWrap: 'wrap',
		'& > *': {
			marginTop: theme.spacing(2),
			marginBottom: theme.spacing(3)
		}
	}
}));

const OrdersTable = props => {
	const branchs = useSelector(state => state.data.branches);
	const ordersStatus = useSelector(state => state.data.ordersStatus);
	const users = useSelector(state => state.data.users);
	const { paymentStatus } = useParams();
	const user_role = localStorage.getItem('user_role');
	const dispatch = useDispatch();
	const orders = useSelector(selectOrders);
	const searchText = useSelector(({ ordersManagement }) => ordersManagement.orders.searchText);
	const [searchOrder, setSearchOrder] = useState([]);
	const [loading, setLoading] = useState(true);
	const [selected, setSelected] = useState([]);
	const [data, setData] = useState(orders);
	const [page, setPage] = useState(0);
	const [rowsPerPage, setRowsPerPage] = useState(30);
	const [order, setOrder] = useState({
		direction: 'asc',
		id: null
	});
	let serialNumber = 1;
	const [parameter, setParameter] = useState({ page: 1, size: 30 });
	const totalPages = sessionStorage.getItem('orders_total_pages');
	const totalElements = sessionStorage.getItem('orders_total_elements');
	const classes = useStyles();

	useEffect(() => {
		const event = '';
		dispatch(getBranches());
		dispatch(getOrdersStatus());
		dispatch(getUsers());
		dispatch(setOrdersSearchText(event));
	}, []);

	useEffect(() => {
		if (paymentStatus === 'paid') {
			dispatch(getPaidOrders(parameter)).then(() => setLoading(false));
		} else if (paymentStatus === 'unPaid') {
			dispatch(getUnPaidOrders(parameter)).then(() => setLoading(false));
		} else {
			dispatch(getOrders(parameter)).then(() => setLoading(false));
		}
	}, [dispatch]);

	//search order
	useEffect(() => {
		searchText !== '' && getSearchOrder();
	}, [searchText]);

	const getSearchOrder = () => {
		fetch(`${SEARCH_ORDER}?keyword=${searchText}`, {
			headers: {
				'Content-type': 'application/json',
				Authorization: localStorage.getItem('jwt_access_token')
			}
		})
			.then(response => response.json())
			.then(searchedOrderData => {
				setSearchOrder(searchedOrderData.orders);
			});
	};

	function handleRequestSort(orderEvent, property) {
		const id = property;
		let direction = 'desc';

		if (order.id === property && order.direction === 'desc') {
			direction = 'asc';
		}

		setOrder({
			direction,
			id
		});
	}

	function handleSelectAllClick(orderEvent) {
		if (orderEvent.target.checked) {
			setSelected(orders.map(n => n?.id));
			return;
		}
		setSelected([]);
	}

	function handleDeselect() {
		setSelected([]);
	}

	function handleUpdateOrder(item, orderEvent) {
		const itemName = _.isEmpty(users) || users.find(user => user.id === item.user)?.first_name;
		localStorage.removeItem('orderEvent');
		localStorage.setItem('orderEvent', orderEvent);

		props.history.push(`/apps/order-management/orders/${item.id}`);
	}

	function handleOrderDetails(item, orderEvent) {
		const itemName = _.isEmpty(users) || users.find(user => user.id === item.user)?.first_name;
		localStorage.removeItem('orderEvent');
		localStorage.setItem('orderEvent', orderEvent);

		props.history.push(`/apps/order-managements/order/${item.id}/details`);
	}

	function handleDeleteOrder(item, orderEvent) {
		const itemName = _.isEmpty(users) || users.find(u => u.id === item.user)?.first_name;
		localStorage.removeItem('orderEvent');
		localStorage.setItem('orderEvent', orderEvent);
		props.history.push(`/apps/order-management/orders/${item.id}`);
	}

	//handlePrintInvoice
	function handlePrintInvoice(item, purchaseEvent) {
		props.history.push(`/apps/order-managements/order-invoice/${item.id}`);
	}
	function handleSendMail(item, emailEvent) {
		localStorage.removeItem('orderEvent');
		localStorage.setItem('emailEvent', emailEvent);

		props.history.push(
			`/apps/order-managements/send-email/${item.order_no}/${item?.user?.email}/${item.id}/${item?.user_phone_number}`
		);
	}

	function handleCheck(orderEvent, id) {
		const selectedIndex = selected.indexOf(id);
		let newSelected = [];

		if (selectedIndex === -1) {
			newSelected = newSelected.concat(selected, id);
		} else if (selectedIndex === 0) {
			newSelected = newSelected.concat(selected.slice(1));
		} else if (selectedIndex === selected.length - 1) {
			newSelected = newSelected.concat(selected.slice(0, -1));
		} else if (selectedIndex > 0) {
			newSelected = newSelected.concat(selected.slice(0, selectedIndex), selected.slice(selectedIndex + 1));
		}
		console.log('newSelected', newSelected);
		setSelected(newSelected);
	}

	const handlePagination = (e, handlePage) => {
		setParameter({ ...parameter, page: handlePage });
		setPage(handlePage - 1);
		if (paymentStatus === 'paid') {
			dispatch(getPaidOrders({ ...parameter, page: handlePage }));
		} else if (paymentStatus === 'unPaid') {
			dispatch(getUnPaidOrders({ ...parameter, page: handlePage }));
		} else {
			dispatch(getOrders({ ...parameter, page: handlePage }));
		}
	};

	function handleChangePage(orderEvent, value) {
		setPage(value);
		setParameter({ ...parameter, page: value + 1 });

		if (paymentStatus === 'paid') {
			dispatch(getPaidOrders({ ...parameter, page: value - 1 }));
		} else if (paymentStatus === 'unPaid') {
			dispatch(getUnPaidOrders({ ...parameter, page: value - 1 }));
		} else {
			dispatch(getOrders({ ...parameter, page: value - 1 }));
		}
	}

	function handleChangeRowsPerPage(orderEvent) {
		setRowsPerPage(orderEvent.target.value);
		setParameter({ ...parameter, size: orderEvent.target.value });
		if (paymentStatus === 'paid') {
			dispatch(getPaidOrders({ ...parameter, size: orderEvent.target.value }));
		} else if (paymentStatus === 'unPaid') {
			dispatch(getUnPaidOrders({ ...parameter, size: orderEvent.target.value }));
		} else {
			dispatch(getOrders({ ...parameter, size: orderEvent.target.value }));
		}
	}

	if (loading) {
		return <FuseLoading />;
	}

	if (orders.length === 0) {
		return (
			<motion.div
				initial={{ opacity: 0 }}
				animate={{ opacity: 1, transition: { delay: 0.1 } }}
				className="flex flex-1 items-center justify-center h-full"
			>
				<Typography color="textSecondary" variant="h5">
					There are no orders!
				</Typography>
			</motion.div>
		);
	}
	return (
		<div className="w-full flex flex-col">
			<FuseScrollbars className="flex-grow overflow-x-auto">
				<Table stickyHeader className="min-w-xl" aria-labelledby="tableTitle">
					<OrdersTableHead
						selectedOrderIds={selected}
						order={order}
						onSelectAllClick={handleSelectAllClick}
						onRequestSort={handleRequestSort}
						rowCount={orders.length}
						onMenuItemClick={handleDeselect}
					/>

					<TableBody>
						{_.orderBy(
							searchText !== '' && searchOrder ? searchOrder : orders,
							[
								o => {
									switch (order.id) {
										case 'categories': {
											return o.categories[0];
										}
										default: {
											return o[order.id];
										}
									}
								}
							],
							[order.direction]
						).map(n => {
							const isSelected = selected.indexOf(n?.id) !== -1;
							return (
								<TableRow
									hover
									role="checkbox"
									aria-checked={isSelected}
									tabIndex={-1}
									key={n?.id}
									selected={isSelected}
									style={{ background: n?.order_status?.name === 'cancelled' ? '#ef4d6926' : '' }}
								>
									<TableCell className="whitespace-nowrap w-40 md:w-64 text-center" padding="none">
										<Checkbox
											checked={isSelected}
											onClick={orderEvent => orderEvent.stopPropagation()}
											onChange={orderEvent => handleCheck(orderEvent, n?.id)}
										/>
									</TableCell>

									<TableCell
										whitespace-nowrap
										className="w-40 md:w-64"
										component="th"
										scope="row"
										padding="none"
									>
										{parameter.page * parameter.size - parameter.size + serialNumber++}
									</TableCell>

									<TableCell
										whitespace-nowrap
										className="p-4 md:p-16"
										component="th"
										scope="row"
										padding="none"
									>
										{n?.order_no}
									</TableCell>

									<TableCell
										whitespace-nowrap
										className="p-4 md:p-16"
										component="th"
										scope="row"
										padding="none"
									>
										<div
											style={{
												display:
													n?.order_status?.name === 'cancelled' ? 'flex' : 'inline-block',
												alignItems: 'center',
												width: n?.order_status?.name === 'cancelled' ? '250px' : ''
											}}
										>
											<div
												style={{ width: 'fit-content' }}
												className={clsx(
													`inline text-12 font-semibold py-4 px-12 rounded-full truncate ${
														n?.order_status?.name === ('cancelled' || 'Cancelled')
															? 'text-black'
															: 'text-white'
													}`,
													n?.order_status?.name === ('pending' || 'Pending')
														? 'bg-red-400'
														: n?.order_status?.name === ('processing' || 'Processing')
														? 'bg-orange-300'
														: n?.order_status?.name === ('packaging' || 'Packaging')
														? 'bg-purple-700'
														: n?.order_status?.name === ('on_the_way' || 'On_the_way')
														? 'bg-blue-200'
														: n?.order_status?.name === ('delivered' || 'Delivered')
														? 'bg-green-700'
														: n?.order_status?.name === ('cancelled' || 'Cancelled')
														? 'bg-gray-300'
														: ''
												)}
											>
												{n?.order_status?.name === 'cancelled'
													? `${n?.order_status?.name} `
													: n?.order_status?.name}
											</div>
											{n?.order_status?.name === 'cancelled' && (
												<div
													style={{
														width: 'fit-content',
														whiteSpace: 'nowrap',
														marginLeft: '10px'
													}}
												>
													{n?.order_status?.name === 'cancelled'
														? ` (by ${n.updated_by.first_name} ${n.updated_by.last_name})`
														: ''}
												</div>
											)}
										</div>
									</TableCell>

									<TableCell
										whitespace-nowrap
										className="p-4 md:p-16"
										component="th"
										scope="row"
										padding="none"
									>
										{n?.is_paid === true ? 'Paid' : n?.is_paid === false ? 'Pending' : 'Unknown'}
									</TableCell>

									<TableCell
										className="p-4 md:p-16 whitespace-nowrap"
										component="th"
										scope="row"
										padding="none"
									>
										{n?.comment || 'No Comment'}
									</TableCell>
									<TableCell
										className="p-4 md:p-16"
										component="th"
										scope="row"
										padding="none"
										// className={clsx(
										// 	'inline text-12 font-semibold py-4 px-12 rounded-full truncate text-white',
										// 	//n?.is_online === true ? "Yes" : 'bg-red' :
										// )}
									>
										{n?.is_online === true ? 'Yes' : 'No'}
									</TableCell>

									<TableCell
										whitespace-nowrap
										className="p-4 md:p-16"
										component="th"
										scope="row"
										padding="none"
									>
										{n?.user?.email}
									</TableCell>
									<TableCell
										whitespace-nowrap
										className="p-4 md:p-16"
										component="th"
										scope="row"
										padding="none"
									>
										{n?.user_phone_number}
									</TableCell>

									<TableCell
										whitespace-nowrap
										className="p-4 md:p-16"
										component="th"
										scope="row"
										padding="none"
									>
										{n?.branch?.name}
									</TableCell>

									<TableCell
										style={{ whiteSpace: 'nowrap' }}
										className="p-4 md:p-16"
										component="th"
										scope="row"
										padding="none"
									>
										{n?.created_at.slice(0, 10) /*moment(n?.created_at).format("dd/mm/yyyy")*/}
									</TableCell>

									<TableCell
										whitespace-nowrap
										className="p-4 md:p-16"
										component="th"
										scope="row"
										padding="none"
									>
										{n?.net_amount}
									</TableCell>

									{/* <TableCell 	className="w-52 px-4 md:px-4" component="th" scope="row">
											{n?.length > 0 && n?.featuredImageId ? (
												<img
													className="block rounded"
													style={{ borderRadius: '50%', height: '50px', width: '50px' }}
													src={_.find(n?.image, { id: n?.featuredImageId }).url}
													alt={n?.name}
												/>
											) : (
												<img
													className="w-full block rounded"
													style={{ borderRadius: '50%', height: '50px', width: '50px' }}
													src={`${BASE_URL}${n?.image}`}
													alt={n?.name}
												/>
											)}
									</TableCell> */}

									<TableCell
										className="p-0 md:p-0"
										align="center"
										component="th"
										scope="row"
										style={{ minWidth: '80px' }}
										padding="none"
									>
										<div className="flex">
											<Tooltip title="Edit" placement="top-start">
												<EditIcon
													onClick={orderEvent => handleOrderDetails(n, 'updateOrder')}
													className="h-52 cursor-pointer"
													style={{
														color: 'green',
														visibility:
															((n?.is_paid === true &&
																n?.order_status?.name ===
																	('delivered' || 'Delivered')) ||
																n?.order_status?.name ===
																	('cancelled' || 'Cancelled')) &&
															user_role !== 'ADMIN'
																? 'hidden'
																: 'visible'
													}}
												/>
											</Tooltip>

											<Tooltip title="Order Details" placement="top-start">
												<IconButton
													onClick={orderEvent => handleUpdateOrder(n, 'updateOrder')}
													className="h-52 cursor-pointer"
													style={{
														color: 'orange',
														padding: 'none',
														visibility:
															n?.is_paid === true &&
															n?.order_status?.name === ('delivered' || 'Delivered') &&
															user_role !== 'ADMIN'
																? 'hidden'
																: 'visible'
													}}
													disabled={n?.order_status?.name === 'cancelled'}
												>
													<Icon>visibility</Icon>
												</IconButton>
											</Tooltip>

											<Tooltip title="Delete" placement="top-start">
												<DeleteIcon
													onClick={event => handleDeleteOrder(n, 'Delete')}
													className="h-52 cursor-pointer"
													style={{
														color: 'red',
														visibility:
															((n?.is_paid === true &&
																n?.order_status?.name ===
																	('delivered' || 'Delivered')) ||
																n?.order_status?.name ===
																	('cancelled' || 'Cancelled')) &&
															user_role !== 'ADMIN'
																? 'hidden'
																: 'visible'
													}}
												/>
											</Tooltip>

											<Tooltip title="Invoice" placement="top-start">
												<ReceiptIcon
													onClick={invoiceEvent => handlePrintInvoice(n, 'invoicePurchase')}
													className="h-52 cursor-pointer"
													style={{
														color: '#6495ED',
														fontSize: '2.2285714285714286rem'
													}}
												/>
											</Tooltip>
											<Tooltip title="Resend Email" placement="top-start">
												<EmailOutlinedIcon
													onClick={invoiceEvent => handleSendMail(n, 'sendEmail')}
													className="h-52 cursor-pointer"
													style={{ color: '#0c3b69', fontSize: '2.2285714285714286rem' }}
												/>
											</Tooltip>
										</div>
									</TableCell>
								</TableRow>
							);
						})}
					</TableBody>
				</Table>
			</FuseScrollbars>

			<div className={classes.root}>
				<Pagination
					count={totalPages}
					page={page + 1}
					defaultPage={1}
					color="primary"
					showFirstButton
					showLastButton
					variant="outlined"
					shape="rounded"
					onChange={handlePagination}
				/>

				<TablePagination
					className="flex-shrink-0 border-t-1"
					rowsPerPageOptions={[30, 50, 100]}
					component="div"
					count={totalElements}
					rowsPerPage={rowsPerPage}
					page={page}
					backIconButtonProps={{
						'aria-label': 'Previous Page'
					}}
					nextIconButtonProps={{
						'aria-label': 'Next Page'
					}}
					onChangePage={handleChangePage}
					onChangeRowsPerPage={handleChangeRowsPerPage}
				/>
			</div>
		</div>
	);
};

export default withRouter(OrdersTable);
