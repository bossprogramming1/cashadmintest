import React, { useEffect, useState, useRef } from 'react';
import { yupResolver } from '@hookform/resolvers/yup';
import { makeStyles, styled, useTheme } from '@material-ui/core/styles';
import { withRouter, useParams, useHistory } from 'react-router-dom';
import Paper from '@material-ui/core/Paper';
import { Box, Typography, Button, TextField } from '@material-ui/core';
import TableContainer from '@material-ui/core/TableContainer';
import PrintIcon from '@material-ui/icons/Print';
import ArrowBackIcon from '@mui/icons-material/ArrowBack';
import { format } from 'date-fns';
//import ReactToPrint from 'react-to-print';
import { useReactToPrint } from 'react-to-print';
import Table from '@material-ui/core/Table';
import TableHead from '@material-ui/core/TableHead';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableRow from '@material-ui/core/TableRow';
import { BASE_URL, GET_INVOICE_BY_ORDERID, SEND_CONFIRMATION_SMS, SEND_ORDER_EMAIL } from 'app/constant/constants';
import * as yup from 'yup';

import axios from 'axios';
import { Controller, useForm } from 'react-hook-form';

const useStyles = makeStyles(theme => ({
	hidden: {
		display: 'none'
	},
	tblContainer: {
		borderRadius: '0px',
		width: '700px',
		marginLeft: '150px',
		marginBottom: '50px'
	},
	table: {
		minWidth: 700
	},
	tableHead: {
		backgroundColor: theme.palette.primary[500]
	},
	dateOfSaleContainer: {
		display: 'flex',
		flexWrap: 'nowrap',
		justifyContent: 'space-between',
		minWidth: '250px'
	},
	paper: {
		marginTop: '10%',
		backgroundColor: theme.palette.background.paper,
		//border: `2px solid ${theme.palette.grey[800]}`,
		boxShadow: theme.shadows[5],
		padding: theme.spacing(2, 4, 3)
	},
	underline: {
		borderBottom: '1px solid grey',
		marginTop: '4%',
		marginBottom: '1%'
	},
	textMarginBottom: {
		marginBottom: '2%'
	}
}));
/**
 * Form Validation Schema
 */
const schema = yup.object().shape({
	// email: yup.string().required('Please enter your email.'),
	// order_no: yup.string().required('Please enter your Order No.'),
	// message: yup.string().required('Please enter your message'),
	// number: yup.string().required('Please enter your Phone Number.')
});
const defaultValues = {
	order_no: '',
	email: '',
	message: '',
	number: ''
};

const OrderInvoice = props => {
	const [orderInvoice, setOrderInvoice] = useState({});

	const routeParams = useParams();
	const { email, orderNo, orderId, number } = routeParams;
	const classes = useStyles(props);
	const Theme = useTheme();
	const { control, setValue, errors, values, formState, handleSubmit, reset, watch, trigger, setError, getValues } =
		useForm({
			mode: 'onChange',
			defaultValues,
			resolver: yupResolver(schema)
		});
	const authTOKEN = {
		headers: {
			'Content-type': 'application/json',
			Authorization: localStorage.getItem('jwt_access_token')
		}
	};
	//sms
	const [totalCharacter, setTotalCharacter] = useState(0);
	const [smsPart, setSmsPart] = useState(0);
	const [remainingCharacter, setRemainingCharacter] = useState(160);
	const [smsPartMessage, setSmsPartMessage] = useState('');
	const [characterUse, setCharacterUse] = useState(0);

	function handleSMSPartCount() {
		setSmsPart(parseInt((totalCharacter + 1) / 160));
		setRemainingCharacter(159 - parseInt(totalCharacter % 160));
		setCharacterUse(parseInt(totalCharacter % 160) + 1);
	}
	const history = useHistory();

	useEffect(() => {
		setValue('order_no', `${orderNo}`, {
			shouldDirty: true,
			shouldValidate: true
		});
		setValue('number', `${number}`, {
			shouldDirty: true,
			shouldValidate: true
		});
		setValue('email', `${email}`, {
			shouldDirty: true,
			shouldValidate: true
		});
	}, [reset, setValue, trigger]);

	const onSubmit = async () => {
		const data = getValues();
		const messageData = { message: data.message, number: data.number };

		if (!data.message) {
			axios.get(`${SEND_ORDER_EMAIL}${orderId}/${data.email}`, authTOKEN).then(res => {});
			localStorage.removeItem('emailEvent');
			localStorage.setItem('sendConfirmEmail', 'sendConfirmEmail');
			history.goBack();
		}
		if (data.message) {
			if (smsPart <= 2) {
				axios.post(`${SEND_CONFIRMATION_SMS}`, messageData, authTOKEN).then(res => {});
				localStorage.removeItem('emailEvent');
				localStorage.setItem('sendConfirmEmail', 'sendConfirmEmail');
				history.goBack();
			} else {
				setSmsPartMessage('Your message is to long');
			}
		}
	};
	const onSubmit2 = async () => {
		const data = getValues();
		history.goBack();
	};

	return (
		<>
			<Box
				style={{
					display: 'flex',
					justifyContent: 'space-between',
					flexDirection: 'row',
					marginRight: '150px',
					marginLeft: '150px'
				}}
			>
				<Button onClick={() => history.goBack()} style={{ fontSize: '17px' }}>
					<ArrowBackIcon
						className="h-52 cursor-pointer"
						style={{
							color: 'black',
							textAlign: 'left',

							height: '30px',
							width: '30px'
						}}
					/>
					Go Back
				</Button>
			</Box>

			<Box style={{ display: 'flex', justifyContent: 'space-around' }}>
				<div
					style={{
						display: 'flex',
						flexDirection: 'column',
						alignItems: 'center',
						marginTop: '80px'
					}}
				>
					<form onSubmit={handleSubmit(onSubmit)}>
						<Typography variant="h6">Resend Invoice by Email</Typography>
						<br />
						<Controller
							name="order_no"
							control={control}
							render={({ field }) => (
								<TextField
									{...field}
									className="mt-8 mb-16"
									error={!!errors?.order_no}
									disabled
									helperText={errors?.order_no?.message}
									label="Order No"
									id="order_no"
									variant="outlined"
									fullWidth
									InputLabelProps={field.value && { shrink: true }}
								/>
							)}
						/>

						<Controller
							name="email"
							control={control}
							render={({ field }) => (
								<TextField
									{...field}
									className="mt-8 mb-16"
									error={!!errors?.email}
									helperText={errors?.email?.message}
									label="Email"
									id="email"
									variant="outlined"
									fullWidth
									InputLabelProps={field.value && { shrink: true }}
								/>
							)}
						/>

						<Button variant="contained" color="primary" type="submit">
							Send Email
						</Button>
					</form>
				</div>
				<div
					style={{
						display: 'flex',
						flexDirection: 'column',
						alignItems: 'center',
						marginTop: '80px'
					}}
				>
					<form onSubmit={handleSubmit(onSubmit)}>
						<Typography variant="h6">Resend Sms</Typography>
						<br />
						<Controller
							name="number"
							control={control}
							render={({ field }) => (
								<TextField
									{...field}
									className="mt-8 mb-16"
									error={!!errors?.number}
									helperText={errors?.number?.message}
									label="Phone Number"
									id="number"
									variant="outlined"
									fullWidth
									InputLabelProps={field.value && { shrink: true }}
								/>
							)}
						/>

						<Controller
							name="message"
							control={control}
							render={({ field }) => {
								return (
									<TextField
										{...field}
										className="mt-8 mb-16"
										label="Message"
										id="message"
										autoFocus
										multiline
										onChange={e => {
											setTotalCharacter(e.target.value?.length);
											handleSMSPartCount();
											setValue(`message`, e.target.value);
										}}
										rows={10}
										variant="outlined"
										InputLabelProps={field.value && { shrink: true }}
										fullWidth
									/>
								);
							}}
						/>

						<Button variant="contained" color="primary" type="submit">
							Send Sms
						</Button>
						<div>
							{smsPartMessage && (
								<p style={{ color: 'red', display: smsPart <= 2 ? 'none' : 'block' }}>
									{smsPartMessage}
								</p>
							)}
							<p>
								Character Use: {characterUse} &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Remaining Character :{' '}
								{remainingCharacter} &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; SMS Part : {smsPart}{' '}
							</p>
						</div>
					</form>
				</div>
			</Box>
		</>
	);
};
export default withRouter(OrderInvoice);
