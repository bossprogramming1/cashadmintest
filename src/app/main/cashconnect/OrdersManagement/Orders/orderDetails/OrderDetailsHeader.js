import Button from '@material-ui/core/Button';
import Icon from '@material-ui/core/Icon';
import { useTheme } from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';
import { motion } from 'framer-motion';
import React, { useState } from 'react';
import { useFormContext } from 'react-hook-form';
import { useDispatch } from 'react-redux';
import { Link, useHistory, useParams } from 'react-router-dom';
import { addNotification } from 'app/fuse-layouts/shared-components/notificationPanel/store/dataSlice';
import NotificationModel from 'app/fuse-layouts/shared-components/notificationPanel/model/NotificationModel';
import { updateOrder } from '../../store/orderSlice';

function OrderDetailsHeader(props) {
	const dispatch = useDispatch();
	const methods = useFormContext();
	const { formState, watch, getValues, setError } = methods;
	const { isValid, dirtyFields } = formState;
	const images = watch('thumbnail', []);
	const featuredImageId = watch('featuredImageId');
	const thumbnail = watch('thumbnail');
	const newProductName = watch('name');
	const theme = useTheme();
	const history = useHistory();
	const routeParams = useParams();
	const { orderId } = routeParams;
	const [loading, setLoading] = useState(true);
	const deleteProduct = localStorage.getItem('deleteProduct');
	const updateProduct = localStorage.getItem('updateProduct');
	function handleUpdateProduct() {
		const data = getValues();
		if (data?.is_paid == false && data.order_status.name == 'delivered') {
			dispatch(
				addNotification(
					NotificationModel({
						message: `You can't update order without payment`,
						options: { variant: 'error' },
						item_id: data.verified_by
					})
				)
			);
		} else {
			console.log('update_data', data);
			data.order_status = data.order_status.id;
			const { is_paid, ...filteredData } = data;
			dispatch(updateOrder({ ...filteredData, id: orderId })).then(() => {
				history.goBack();
			});
		}
	}

	const handleCancel = () => {
		localStorage.removeItem('orderAlert');
		localStorage.removeItem('sendConfirmEmail');
		localStorage.removeItem('orderEvent');
		history.goBack();
	};

	return (
		<div className="flex flex-1 w-full items-center justify-between">
			<div className="flex flex-col items-start max-w-full min-w-0">
				<motion.div initial={{ x: 20, opacity: 0 }} animate={{ x: 0, opacity: 1, transition: { delay: 0.3 } }}>
					<Typography className="flex items-center sm:mb-12" role="button" color="inherit">
						<Icon onClick={handleCancel} className="text-20">
							{theme.direction === 'ltr' ? 'arrow_back' : 'arrow_forward'}
						</Icon>
						<span onClick={handleCancel} className="hidden sm:flex mx-4 font-medium">
							Go to Orders
						</span>
					</Typography>
				</motion.div>

				<div className="flex items-center max-w-full">
					<div className="flex flex-col min-w-0 mx-8 sm:mc-16">
						<motion.div initial={{ x: -20 }} animate={{ x: 0, transition: { delay: 0.3 } }}>
							<Typography className="text-16 sm:text-20 truncate font-semibold">Orders</Typography>
							<Typography variant="caption" className="font-medium">
								Order Details
							</Typography>
						</motion.div>
					</div>
				</div>
			</div>
			<motion.div
				className="flex"
				initial={{ opacity: 0, x: 20 }}
				animate={{ opacity: 1, x: 0, transition: { delay: 0.3 } }}
			>
				<Button
					className="whitespace-nowrap mx-4"
					color="secondary"
					variant="contained"
					style={{ backgroundColor: '#4dc08e', color: 'white' }}
					onClick={handleUpdateProduct}
				>
					Update
				</Button>
				<Button
					className="whitespace-nowrap mx-4"
					variant="contained"
					style={{ backgroundColor: '#FFAA4C', color: 'white' }}
					onClick={handleCancel}
				>
					Cancel
				</Button>
			</motion.div>
		</div>
	);
}

export default OrderDetailsHeader;
