/* eslint-disable import/named */
import React, { useEffect, useState } from 'react';
import { makeStyles, styled, useTheme } from '@material-ui/core/styles';
import Grid from '@material-ui/core/Grid';
import Paper from '@material-ui/core/Paper';
import Table from '@material-ui/core/Table';
import _ from '@lodash';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import { useParams } from 'react-router-dom';
import { useDispatch, useSelector } from 'react-redux';
import Box from '@material-ui/core/Box';
import { Typography } from '@material-ui/core';

import { getCities, getCountries, getThanas } from 'app/store/dataSlice';
import { CUSTOMERORDERDETAILSWITHSHIPPINGADDRESS } from 'app/constant/constants';
import OrderedProductDetails from './OrderedProductDetails';

const useStyles = makeStyles(theme => ({
	hidden: {
		display: 'none'
	},
	tblContainer: {
		borderRadius: '0px'
	},
	table: {
		minWidth: 600
	},
	tableHead: {
		backgroundColor: theme.palette.primary[500]
	},
	dateOfSaleContainer: {
		display: 'flex',
		flexWrap: 'nowrap',
		justifyContent: 'space-between',
		minWidth: '250px'
	},
	dateOfSale: {
		width: '45px',
		background: theme.palette.background.paper,
		'& > div': {
			'&:before': {
				borderBottom: '0px solid black !important'
			}
		},
		'& input': {
			boxSizing: 'inherit',
			marginLeft: '-11px',
			marginTop: '2px'
		}
	},
	paytypeLabel: {
		'&& > label': {
			fontSize: '16px'
		}
	},
	paidAmount: {
		'&& > p': {
			marginTop: '-27px'
		}
	},
	modal: {
		position: 'absolute',
		display: 'flex',
		alignItems: 'center',
		justifyContent: 'center',
		marginTop: '15%',
		marginLeft: '35%',
		//transform: 'translate(-50%, -50%)',
		width: 350,
		height: 280,
		border: '2px solid #000',
		boxShadow: 24,
		p: 4,
		color: theme.palette.background.paper,
		backgroundColor: theme.palette.secondary.light
	},
	paper: {
		marginTop: '10%',
		backgroundColor: theme.palette.background.paper,
		//border: `2px solid ${theme.palette.grey[800]}`,
		boxShadow: theme.shadows[5],
		padding: theme.spacing(2, 4, 3)
	},
	totalSummery: {
		display: 'flex',
		justifyContent: 'space-between'
	},
	underline: {
		borderBottom: '1px solid grey',
		marginTop: '4%',
		marginBottom: '1%'
	},
	textMarginBottom: {
		marginBottom: '2%'
	}
}));

const BasicInfoTab = props => {
	const dispatch = useDispatch();
	const routeParams = useParams();
	const { orderId } = routeParams;
	const classes = useStyles(props);
	const Theme = useTheme();
	const PaperColor = Theme.palette.background.paper;
	const [orderDetailsWithShippingAddress, setOrderDetailsWithShippingAddress] = useState({});
	const cities = useSelector(state => state.data.cities);
	const countries = useSelector(state => state.data.countries);
	const thanas = useSelector(state => state.data.thanas);
	const authTOKEN = {
		headers: {
			'Content-type': 'application/json',
			Authorization: localStorage.getItem('jwt_access_token')
		}
	};

	useEffect(() => {
		dispatch(getCities());
		dispatch(getCountries());
		dispatch(getThanas());
	}, []);

	useEffect(() => {
		fetch(CUSTOMERORDERDETAILSWITHSHIPPINGADDRESS + orderId, authTOKEN)
			.then(response => response.json())
			.then(data => setOrderDetailsWithShippingAddress(data.order));
	}, []);

	return (
		<div>
			<Grid item xs={12} sx={{ m: 2 }}>
				<Div h="fit-content" pd="2" minHeight="272px" alignItems="flex-start" border>
					<TableContainer component={Paper} className={classes.tblContainer}>
						<Table className={classes.table} aria-label="simple table">
							<TableHead className={classes.tableHead}>
								<TableRow>
									<TableCell style={{ color: PaperColor }}>No.</TableCell>
									<TableCell style={{ color: PaperColor }} align="center">
										IMG
									</TableCell>
									<TableCell style={{ color: PaperColor }} align="center">
										Product Details
									</TableCell>
									<TableCell style={{ color: PaperColor }} align="center">
										Color & Size
									</TableCell>
									<TableCell style={{ color: PaperColor }} align="center">
										Quantity
									</TableCell>
									<TableCell style={{ color: PaperColor }} align="center">
										Price
									</TableCell>
									<TableCell style={{ color: PaperColor }} align="center">
										Total
									</TableCell>
								</TableRow>
							</TableHead>
							{orderDetailsWithShippingAddress?.order_items?.map((productDetails, i) => (
								<OrderedProductDetails key={productDetails.id} productDetails={productDetails} />
							))}
						</Table>
					</TableContainer>
				</Div>
			</Grid>
			<Box className="mt-16">
				<Grid container xs={12} spacing={2}>
					<Grid item xs={6}>
						<Paper className={classes.paper} sx={{ maxHeight: 400, my: 1, p: 4, borderRadius: 5 }}>
							<Typography variant="h6" gutterBottom component="div">
								Shipping Address
							</Typography>
							{orderDetailsWithShippingAddress?.shipping_address ? (
								<Typography variant="p" gutterBottom component="div">
									<span>Name: {orderDetailsWithShippingAddress?.shipping_address?.name} </span> <br />
									<span>
										Address: {orderDetailsWithShippingAddress?.shipping_address?.street_address}{' '}
									</span>{' '}
									<br />
									<span>Phone: {orderDetailsWithShippingAddress?.shipping_address?.phone} </span>{' '}
									<br />
									<span>Email: {orderDetailsWithShippingAddress?.shipping_address?.email} </span>{' '}
									<br />
								</Typography>
							) : (
								<Typography variant="p" gutterBottom component="div">
									Shipping address not found
								</Typography>
							)}
						</Paper>
					</Grid>
					<Grid item xs={6}>
						<Paper className={classes.paper} sx={{ maxHeight: 400, my: 1, p: 4, borderRadius: 5 }}>
							<Typography
								variant="h6"
								gutterBottom
								component="div"
								sx={{ m: 2 }}
								className={classes.textMarginBottom}
							>
								Total Summary
							</Typography>
							<>
								<Box className={classes.totalSummery}>
									<Typography
										variant="p"
										gutterBottom
										sx={{ m: 2 }}
										className={classes.textMarginBottom}
									>
										Subtotal:
									</Typography>
									<Typography
										variant="p"
										gutterBottom
										sx={{ m: 2 }}
										className={classes.textMarginBottom}
									>
										{orderDetailsWithShippingAddress?.net_amount}
									</Typography>
								</Box>
								<Box className={classes.totalSummery}>
									<Typography
										variant="p"
										gutterBottom
										sx={{ m: 2 }}
										className={classes.textMarginBottom}
									>
										Shipping fee:
									</Typography>
									<Typography
										variant="p"
										gutterBottom
										sx={{ m: 2 }}
										className={classes.textMarginBottom}
									>
										{orderDetailsWithShippingAddress?.shipping_price}
									</Typography>
								</Box>
								<Box className={classes.totalSummery}>
									<Typography variant="p" gutterBottom sx={{ m: 2 }}>
										Discount:
									</Typography>
									<Typography variant="p" gutterBottom sx={{ m: 2 }}>
										{orderDetailsWithShippingAddress?.discount_amount}
									</Typography>
								</Box>
								<Box className={classes.underline} />
								<Box className={classes.totalSummery}>
									<Typography variant="p" gutterBottom sx={{ m: 2 }}>
										Total:
									</Typography>
									<Typography variant="p" gutterBottom sx={{ m: 2 }} style={{ fontWeight: 'bold' }}>
										{orderDetailsWithShippingAddress?.net_amount}
									</Typography>
								</Box>
							</>
						</Paper>
					</Grid>
				</Grid>
			</Box>
		</div>
	);
};

export default BasicInfoTab;

const Div = styled('div')(props => ({
	background: props.bg,
	height: props.h === '1' ? '25px' : props.h === '2' ? '35px' : props.h === 3 ? '50px' : props.h && props.h,
	width: props.w ? props.w : '100%',
	display: 'flex',
	justifyContent: props.justCont ? props.justCont : 'space-between',
	alignItems: props.alignItems ? props.alignItems : 'center',
	padding: props.pd === '1' ? '10px' : props.pd === '2' ? '20px' : props.pd && props.pd,
	flexWrap: props.wrap && 'wrap',
	border: props.border && '2px solid gray',
	borderTop: `4px solid ${props.top}`,
	borderLeft: `4px solid ${props.left}`,
	minHeight: props.minHeight
}));
