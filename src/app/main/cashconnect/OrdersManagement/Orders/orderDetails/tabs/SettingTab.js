import _ from '@lodash';
import TextField from '@material-ui/core/TextField';
import { Autocomplete } from '@material-ui/lab';
import { getBranches, getEmployees, getOrdersStatus, getUsers } from 'app/store/dataSlice';
import React, { useEffect, useState } from 'react';
import { Controller, useFormContext } from 'react-hook-form';
import { useDispatch, useSelector } from 'react-redux';
import { useParams } from 'react-router-dom';
import { addNotification } from 'app/fuse-layouts/shared-components/notificationPanel/store/dataSlice';
import NotificationModel from 'app/fuse-layouts/shared-components/notificationPanel/model/NotificationModel';
import { GET_ORDERID } from '../../../../../../constant/constants';

const SettingTab = () => {
	const dispatch = useDispatch();
	const methods = useFormContext();
	const { control, formState, watch, setError, setValue } = methods;
	const { errors } = formState;
	const branches = useSelector(state => state.data.branches);
	const users = useSelector(state => state.data.users);
	const employees = useSelector(state => state.data.employees);
	const ordersStatus = useSelector(state => state.data.ordersStatus);
	const routeParams = useParams();
	const { orderId } = routeParams;
	const checkStatus = watch('order_status');

	const [order, setOrder] = useState({});
	console.log('checkStatus', checkStatus);
	useEffect(() => {
		dispatch(getBranches());
		dispatch(getUsers());
		dispatch(getEmployees());
		dispatch(getOrdersStatus());
	}, []);

	const authTOKEN = {
		headers: {
			'Content-type': 'application/json',
			Authorization: localStorage.getItem('jwt_access_token')
		}
	};

	useEffect(() => {
		fetch(GET_ORDERID + orderId, authTOKEN)
			.then(response => response.json())
			.then(data => setOrder(data));
	}, []);

	return (
		<div>
			{_.isEmpty(ordersStatus) || (
				<div>
					<Controller
						name="branch"
						control={control}
						render={({ field: { onChange, value } }) => (
							<Autocomplete
								className="mt-8 mb-16"
								freeSolo
								disabled={
									order?.order_status?.name === ('delivered' || 'Delivered')
										? true
										: false || order?.order_status?.name === ('cancelled' || 'Cancelled')
										? true
										: false
								}
								value={value ? branches.find(branch => branch.id === value) : null}
								options={branches}
								getOptionLabel={option => `${option?.name}`}
								InputLabelProps={{ shrink: true }}
								onChange={(event, newValue) => {
									onChange(newValue?.id);
								}}
								renderInput={params => (
									<TextField
										{...params}
										placeholder="Select a branch"
										label="Branch"
										error={!!errors.branch}
										helperText={errors?.branch?.message}
										variant="outlined"
										autoFocus
										InputLabelProps={{
											shrink: true
										}}
									/>
								)}
							/>
						)}
					/>

					<Controller
						name="order_status"
						control={control}
						render={({ field: { onChange, value } }) => (
							<Autocomplete
								className="mt-8 mb-16"
								freeSolo
								disabled={
									order?.order_status?.name === ('delivered' || 'Delivered')
										? true
										: false || order?.order_status?.name === ('cancelled' || 'Cancelled')
										? true
										: false
								}
								value={value ? ordersStatus.find(Status => Status.id === value.id) : null}
								options={ordersStatus}
								getOptionLabel={option => `${option?.name}`}
								InputLabelProps={{ shrink: true }}
								onChange={(event, newValue) => {
									console.log('orderDetails', order);
									if (order?.is_paid == false && newValue?.name == 'delivered') {
										dispatch(
											addNotification(
												NotificationModel({
													message: `You can't deliver order without payment`,
													options: { variant: 'error' },
													item_id: newValue?.id
												})
											)
										);
										setValue('is_paid', order?.is_paid);
										onChange(newValue);
									} else {
										onChange(newValue);
									}
								}}
								renderInput={params => (
									<TextField
										{...params}
										label="Order Status"
										error={!!errors.order_status}
										helperText={errors?.order_status?.message}
										variant="outlined"
										autoFocus
										InputLabelProps={{
											shrink: true
										}}
									/>
								)}
							/>
						)}
					/>
					{checkStatus?.name == ('delivered' || 'Delivered') && (
						<Controller
							name="delivered_by"
							control={control}
							render={({ field: { onChange, value } }) => (
								<Autocomplete
									className="mt-8 mb-16"
									freeSolo
									disabled={
										order?.order_status?.name === ('delivered' || 'Delivered')
											? true
											: false || order?.order_status?.name === ('cancelled' || 'Cancelled')
											? true
											: false
									}
									value={value ? employees.find(employee => employee.id === value) : null}
									options={employees}
									getOptionLabel={option =>
										`${option?.first_name} ${option?.last_name} [${option.email}]`
									}
									InputLabelProps={{ shrink: true }}
									onChange={(event, newValue) => {
										onChange(newValue?.id);
									}}
									renderInput={params => (
										<TextField
											{...params}
											placeholder="Delivered by"
											label="Delivered by"
											error={!value}
											helperText={'Delivery by Required'}
											variant="outlined"
											autoFocus
											InputLabelProps={{
												shrink: true
											}}
										/>
									)}
								/>
							)}
						/>
					)}
					<Controller
						name="verified_by"
						control={control}
						render={({ field: { onChange, value } }) => (
							<Autocomplete
								className="mt-8 mb-16"
								freeSolo
								disabled={
									order?.order_status?.name === ('delivered' || 'Delivered')
										? true
										: false || order?.order_status?.name === ('cancelled' || 'Cancelled')
										? true
										: false
								}
								value={value ? employees.find(employee => employee.id === value) : null}
								options={employees}
								getOptionLabel={option =>
									`${option?.first_name} ${option?.last_name} [${option.email}]`
								}
								InputLabelProps={{ shrink: true }}
								onChange={(event, newValue) => {
									onChange(newValue?.id);
								}}
								renderInput={params => (
									<TextField
										{...params}
										label="Verified by"
										error={!value}
										helperText={'Verified by Required'}
										variant="outlined"
										autoFocus
										InputLabelProps={{
											shrink: true
										}}
									/>
								)}
							/>
						)}
					/>
					{checkStatus?.name == ('cancelled' || 'Cancelled') && (
						<>
							<Controller
								name="cancelled_by"
								control={control}
								render={({ field: { onChange, value } }) => (
									<Autocomplete
										className="mt-8 mb-16"
										freeSolo
										disabled={
											order?.order_status?.name === ('delivered' || 'Delivered')
												? true
												: false || order?.order_status?.name === ('cancelled' || 'Cancelled')
												? true
												: false
										}
										value={value ? employees.find(employee => employee.id === value) : null}
										options={employees}
										getOptionLabel={option =>
											`${option?.first_name} ${option?.last_name} [${option.email}]`
										}
										InputLabelProps={{ shrink: true }}
										onChange={(event, newValue) => {
											onChange(newValue?.id);
										}}
										renderInput={params => (
											<TextField
												{...params}
												label="Cancelled by"
												error={!!errors.cancelled_by || !value}
												helperText={errors?.cancelled_by?.message}
												variant="outlined"
												autoFocus
												InputLabelProps={{
													shrink: true
												}}
											/>
										)}
									/>
								)}
							/>

							<Controller
								name="note"
								control={control}
								render={({ field }) => {
									return (
										<TextField
											{...field}
											size="small"
											error={!!errors.note}
											helperText={errors?.note?.message}
											label="Note"
											id="Note"
											multiline
											rows={4}
											disabled={
												order?.order_status?.name === ('delivered' || 'Delivered')
													? true
													: false ||
													  order?.order_status?.name === ('cancelled' || 'Cancelled')
													? true
													: false
											}
											required
											variant="outlined"
											InputLabelProps={
												field.value ? { shrink: true } : { style: { color: 'red' } }
											}
											fullWidth
										/>
									);
								}}
							/>
						</>
					)}
				</div>
			)}
		</div>
	);
};

export default SettingTab;
