import React from 'react';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableRow from '@material-ui/core/TableRow';
import { makeStyles, styled, useTheme } from '@material-ui/core/styles';
import { BASE_URL } from 'app/constant/constants';

const OrderedProductDetails = props => {
	const productDetails = props?.productDetails;
	const Theme = useTheme();
	const PrimaryBg = Theme.palette.primary[500];
	const PrimaryDark = Theme.palette.primary[700];
	const PrimaryLight = Theme.palette.primary[400];
	const PaperColor = Theme.palette.background.paper;
	const primaryBlack = Theme.palette.primary.dark;
	const cellStyle = { border: `1px solid ${Theme.palette.grey[600]}`, fontWeight: 'bold', color: primaryBlack };

	let serialNumber = 1;
	return (
		<TableBody>
			<TableRow key={productDetails.id}>
				<TableCell style={cellStyle} component="th" scope="row">
					{serialNumber++}
				</TableCell>
				<TableCell
					style={{
						...cellStyle,
						paddingTop: '0px',
						paddingBottom: '0px'
					}}
					align="center"
				>
					<img
						className="w-full block rounded"
						style={{
							borderRadius: '50%',
							height: '50px',
							width: '50px'
						}}
						src={`${BASE_URL}${
							productDetails?.color_image?.image
								? productDetails?.color_image?.image
								: productDetails?.product?.thumbnail
						}`}
						alt="Not Found"
					/>
				</TableCell>
				<TableCell style={cellStyle} align="center">
					{productDetails?.product?.name}
				</TableCell>
				<TableCell style={cellStyle} align="center">
					{`${productDetails?.color?.name} ${
						productDetails?.size?.name ? `[${productDetails?.size?.name}]` : ''
					}`}
				</TableCell>
				<TableCell style={cellStyle} align="center">
					{productDetails?.quantity}
				</TableCell>
				<TableCell style={cellStyle} align="center">
					{productDetails?.price}
				</TableCell>
				<TableCell style={cellStyle} align="center">
					{productDetails?.subtotal}
				</TableCell>
			</TableRow>
		</TableBody>
	);
};
export default OrderedProductDetails;
