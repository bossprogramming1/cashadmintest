import FusePageCarded from '@fuse/core/FusePageCarded';
import { yupResolver } from '@hookform/resolvers/yup';
import Button from '@material-ui/core/Button';
import Tab from '@material-ui/core/Tab';
import Tabs from '@material-ui/core/Tabs';
import Typography from '@material-ui/core/Typography';
import { getOrdersStatus } from 'app/store/dataSlice';
import { motion } from 'framer-motion';
import { useEffect, useState } from 'react';
import { FormProvider, useForm } from 'react-hook-form';
import { useDispatch, useSelector } from 'react-redux';
import { Link, useParams } from 'react-router-dom';
import * as yup from 'yup';
import { GET_A_CUSTOMER_ORDER, GET_ORDERID } from '../../../../../constant/constants';
import OrderDetailsHeader from './OrderDetailsHeader';
import BasicInfoTab from './tabs/BasicInfoTab';
import SettingTab from './tabs/SettingTab';

/**
 * Form Validation Schema
 */
const schema = yup.object().shape({
	verified_by: yup.string().required('Verified by Required')
});

function OrderDetails(props) {
	const ordersStatus = useSelector(state => state.data.ordersStatus);
	const [orderDetailsWithShippingAddress, setOrderDetailsWithShippingAddress] = useState({});
	const [order, setOrder] = useState({});
	const dispatch = useDispatch();
	const routeParams = useParams();
	const { orderId } = routeParams;
	const [tabValue, setTabValue] = useState(0);
	const [noProduct, setNoProduct] = useState(false);
	const methods = useForm({
		mode: 'onChange',
		defaultValues: {},
		resolver: yupResolver(schema)
	});
	const { reset, watch, control, onChange, formState } = methods;
	const form = watch();
	const authTOKEN = {
		headers: {
			'Content-type': 'application/json',
			Authorization: localStorage.getItem('jwt_access_token')
		}
	};
	useEffect(() => {
		dispatch(getOrdersStatus());
	}, []);
	useEffect(() => {
		fetch(GET_ORDERID + orderId, authTOKEN)
			.then(response => response.json())
			.then(data => setOrder(data));
	}, []);

	useEffect(() => {
		fetch(GET_A_CUSTOMER_ORDER + orderId, authTOKEN)
			.then(response => response.json())
			.then(data => {
				reset({
					branch: data?.branch?.id,
					delivered_by: data?.delivered_by?.id,
					verified_by: data?.verified_by?.id,
					note: data?.note,
					cancelled_by: data?.cancelled_by?.id,
					order_status: data?.order_status
				});
			});
	}, []);

	/**
	 * Tab Change
	 */
	function handleTabChange(event, value) {
		setTabValue(value);
	}

	/**
	 * Show Message if the requested products is not exists
	 */
	if (noProduct) {
		return (
			<motion.div
				initial={{ opacity: 0 }}
				animate={{ opacity: 1, transition: { delay: 0.1 } }}
				className="flex flex-col flex-1 items-center justify-center h-full"
			>
				<Typography color="textSecondary" variant="h5">
					There is no such product!
				</Typography>
				<Button
					className="mt-24"
					component={Link}
					variant="outlined"
					to="/apps/e-commerce/products"
					color="inherit"
				>
					Go to Products Page
				</Button>
			</motion.div>
		);
	}

	/**
	 * Wait while product data is loading and form is setted
	 */

	return (
		<FormProvider {...methods}>
			<FusePageCarded
				classes={{
					toolbar: 'p-0',
					header: 'min-h-74 h-64'
				}}
				header={<OrderDetailsHeader />}
				contentToolbar={
					<Tabs
						value={tabValue}
						onChange={handleTabChange}
						indicatorColor="primary"
						textColor="primary"
						variant="scrollable"
						scrollButtons="auto"
						classes={{ root: 'w-full h-64' }}
					>
						<Tab className="h-64" label="Basic Info" />
						<Tab className="h-64" label="Setting" />
					</Tabs>
				}
				content={
					<div className="p-16 sm:p-24">
						<div className={tabValue !== 0 ? 'hidden' : ''}>
							<BasicInfoTab />
						</div>

						<div className={tabValue !== 1 ? 'hidden' : ''}>
							<SettingTab />
						</div>
					</div>
				}
				innerScroll
			/>
		</FormProvider>
	);
}

export default OrderDetails;
