import { createAsyncThunk, createEntityAdapter, createSlice } from '@reduxjs/toolkit';
import axios from 'axios';
import { DELETE_ORDER, GET_ALL_PAID_ORDERS, GET_ALL_UNPAID_ORDERS, GET_ORDERS } from '../../../../constant/constants';

export const getOrders = createAsyncThunk('orderManagement/orders/geOrders', async parameter => {
	const { page, size } = parameter;
	axios.defaults.headers.common['Content-type'] = 'application/json';
	axios.defaults.headers.common.Authorization = localStorage.getItem('jwt_access_token');

	const response = axios.get(
		GET_ORDERS,
		{ params: { page, size } },
		{
			headers: {
				'Content-type': 'application/json',
				Authorization: localStorage.getItem('jwt_access_token')
			}
		}
	);
	const data = await response;
	sessionStorage.setItem('orders_total_elements', data.data.total_elements);
	sessionStorage.setItem('orders_total_pages', data.data.total_pages);
	delete axios.defaults.headers.common['Content-type'];
	delete axios.defaults.headers.common.Authorization;

	return data.data.orders;
});
export const getPaidOrders = createAsyncThunk('orderManagement/orders/getPaidOrders', async parameter => {
	const { page, size } = parameter;
	axios.defaults.headers.common['Content-type'] = 'application/json';
	axios.defaults.headers.common.Authorization = localStorage.getItem('jwt_access_token');

	const response = axios.get(
		GET_ALL_PAID_ORDERS,
		{ params: { page, size } },
		{
			headers: {
				'Content-type': 'application/json',
				Authorization: localStorage.getItem('jwt_access_token')
			}
		}
	);
	const data = await response;
	sessionStorage.setItem('orders_total_elements', data.data.total_elements);
	sessionStorage.setItem('orders_total_pages', data.data.total_pages);
	delete axios.defaults.headers.common['Content-type'];
	delete axios.defaults.headers.common.Authorization;

	return data.data.orders;
});
export const getUnPaidOrders = createAsyncThunk('orderManagement/orders/getUnPaidOrders', async parameter => {
	const { page, size } = parameter;
	axios.defaults.headers.common['Content-type'] = 'application/json';
	axios.defaults.headers.common.Authorization = localStorage.getItem('jwt_access_token');

	const response = axios.get(
		GET_ALL_UNPAID_ORDERS,
		{ params: { page, size } },
		{
			headers: {
				'Content-type': 'application/json',
				Authorization: localStorage.getItem('jwt_access_token')
			}
		}
	);
	const data = await response;
	sessionStorage.setItem('orders_total_elements', data.data.total_elements);
	sessionStorage.setItem('orders_total_pages', data.data.total_pages);
	delete axios.defaults.headers.common['Content-type'];
	delete axios.defaults.headers.common.Authorization;

	return data.data.orders;
});

export const removeOrders = createAsyncThunk(
	'orderManagement/orders/removeOrders',
	async (orderIds, { dispatch, getState }) => {
		const authTOKEN = {
			headers: {
				'Content-type': 'application/json',
				Authorization: localStorage.getItem('jwt_access_token')
			}
		};
		await axios.delete(`${DELETE_ORDER}`, { orderIds }, authTOKEN);

		return orderIds;
	}
);

const ordersAdapter = createEntityAdapter({});

export const { selectAll: selectOrders, selectById: selectOrderById } = ordersAdapter.getSelectors(
	state => state.ordersManagement.orders
);

const ordersSlice = createSlice({
	name: 'orderManagement/orders',
	initialState: ordersAdapter.getInitialState({
		searchText: ''
	}),
	reducers: {
		setOrdersSearchText: {
			reducer: (state, action) => {
				state.searchText = action.payload;
			},
			prepare: event => ({ payload: event?.target?.value || '' })
		}
	},
	extraReducers: {
		[getOrders.fulfilled]: ordersAdapter.setAll,
		[getPaidOrders.fulfilled]: ordersAdapter.setAll,
		[getUnPaidOrders.fulfilled]: ordersAdapter.setAll
	}
});

export const { setData, setOrdersSearchText } = ordersSlice.actions;
export default ordersSlice.reducer;
