import { createAsyncThunk, createSlice } from '@reduxjs/toolkit';
import axios from 'axios';
import {
	CREATE_CUSTOMER_ORDER,
	CREATE_ORDER,
	DELETE_ORDER,
	GET_ORDERID,
	UPDATE_CUSTOMER_ORDER
} from '../../../../constant/constants';

export const getOrder = createAsyncThunk('orderManagement/order/getOrder', async (params, { rejectWithValue }) => {
	const authTOKEN = {
		headers: {
			'Content-type': 'application/json',
			Authorization: localStorage.getItem('jwt_access_token')
		}
	};

	try {
		const response = await axios.get(`${GET_ORDERID}${params}`, authTOKEN);
		const data = await response.data;
		return data === undefined ? null : data;
	} catch (err) {
		return rejectWithValue(params);
	}
});

export const removeOrder = createAsyncThunk(
	'orderManagement/order/removeOrder',
	async (val, { dispatch, getState }) => {
		const authTOKEN = {
			headers: {
				'Content-type': 'application/json',
				Authorization: localStorage.getItem('jwt_access_token')
			}
		};

		const orderId = val.id;
		await axios.delete(`${DELETE_ORDER}${orderId}`, authTOKEN);
	}
);

export const updateOrder = createAsyncThunk(
	'orderManagement/order/updateOrder',
	async (orderData, { dispatch, getState }) => {
		const { order } = getState().ordersManagement;
		const { id } = orderData;
		console.log('orderData', orderData);
		const authTOKEN = {
			headers: {
				'Content-type': 'application/json',
				Authorization: localStorage.getItem('jwt_access_token')
			}
		};
		const response = await axios.put(`${UPDATE_CUSTOMER_ORDER}${id}`, orderData, authTOKEN);
	}
);

export const saveOrder = createAsyncThunk(
	'orderManagement/order/saveOrder',
	async (orderData, { dispatch, getState }) => {
		const authTOKEN = {
			headers: {
				'Content-type': 'application/json',
				Authorization: localStorage.getItem('jwt_access_token')
			}
		};
		const response = await axios.post(`${CREATE_ORDER}`, orderData, authTOKEN);
	}
);

export const saveCustomOrder = createAsyncThunk(
	'orderManagement/order/saveCustomOrder',
	async (orderData, { dispatch, getState }) => {
		const authTOKEN = {
			headers: {
				'Content-type': 'application/json',
				Authorization: localStorage.getItem('jwt_access_token')
			}
		};
		const response = axios.post(`${CREATE_CUSTOMER_ORDER}`, orderData, authTOKEN);
		const data = await response;

		return data.data;
	}
);

const orderSlice = createSlice({
	name: 'orderManagement/order',
	initialState: null,
	reducers: {
		resetOrder: () => null,
		newOrder: {
			reducer: (state, action) => action.payload,
			prepare: event => ({
				payload: {}
			})
		}
	},
	extraReducers: {
		[getOrder.fulfilled]: (state, action) => action.payload,
		[removeOrder.fulfilled]: () => {
			localStorage.setItem('orderAlert', 'deleteOrder');
		},
		[saveCustomOrder.fulfilled]: (state, action) => {
			localStorage.setItem('orderAlert', 'saveOrder');
		},
		[updateOrder.fulfilled]: (state, action) => {
			localStorage.setItem('orderAlert', 'updateOrder');
		}
	}
});

export const { newOrder, resetOrder } = orderSlice.actions;

export default orderSlice.reducer;
