import { combineReducers } from '@reduxjs/toolkit';
import order from './orderSlice';
import orders from './ordersSlice';

const reducer = combineReducers({
	order,
	orders,
});

export default reducer;