/* eslint-disable react/jsx-no-duplicate-props */
import _ from '@lodash';
import Backdrop from '@material-ui/core/Backdrop';
import FuseScrollbars from '@fuse/core/FuseScrollbars';
import Box from '@material-ui/core/Box';
import Button from '@material-ui/core/Button';
import Fade from '@material-ui/core/Fade';
import Grid from '@material-ui/core/Grid';
import Modal from '@material-ui/core/Modal';
import Paper from '@material-ui/core/Paper';
import { makeStyles, styled, useTheme } from '@material-ui/core/styles';
import { KeyboardDatePicker } from '@material-ui/pickers';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import TextField from '@material-ui/core/TextField';
import Typography from '@material-ui/core/Typography';
import DeleteIcon from '@material-ui/icons/Delete';
import DoneOutlineIcon from '@material-ui/icons/DoneOutline';
import EditIcon from '@material-ui/icons/Edit';
import { Autocomplete } from '@material-ui/lab';
import axios from 'axios';
import moment from 'moment';
import React, { useEffect, useState } from 'react';
import { Controller, useFormContext } from 'react-hook-form';
import { useDispatch, useSelector } from 'react-redux';
import { useParams, useHistory } from 'react-router-dom';
import CloseIcon from '@material-ui/icons/Close';
import {
	BASE_URL,
	CONFIRM_CUSTOMER_ORDER,
	GET_COLOR_PRODUCT_ID,
	GET_CUSTOMER_ORDER_DETAILS,
	GET_INVENTORY_BY_PRODUCT_ID,
	GET_SIZE_BY_PRODUCTID_COLORID,
	REMOVE_PRODUC_TITEM,
	SHIPPING_ADDRESS_BY_ORDERID,
	UPDATE_PRODUC_TITEM
} from 'app/constant/constants';
import {
	getBranches,
	getCities,
	getCountries,
	getPaymentMathods,
	getProducts,
	getThanas,
	getUsers
} from 'app/store/dataSlice';
import { Card, Icon, IconButton, Tooltip } from '@material-ui/core';
import { saveCustomOrder, updateOrder } from '../store/orderSlice';
import CustomerForm from '../../CustomersManagement/Customer/CustomerForm';
import { saveCustomer } from '../../CustomersManagement/store/customerSlice';
import sizeSlice from '../../SizesManagement/store/sizeSlice';

const style = {
	position: 'absolute',
	top: '50%',
	left: '50%',
	transform: 'translate(-50%, -50%)',
	width: 400,
	bgcolor: 'background.paper',
	border: '2px solid #000',
	boxShadow: 24,
	p: 4
};

const useStyles = makeStyles(theme => ({
	hidden: {
		display: 'none'
	},
	tblContainer: {
		borderRadius: '0px'
	},
	table: {
		minWidth: 600
	},
	tableHead: {
		backgroundColor: theme.palette.primary[500]
	},
	dateOfSaleContainer: {
		display: 'flex',
		flexWrap: 'nowrap',
		justifyContent: 'space-between',
		minWidth: '250px'
	},
	dateOfSale: {
		width: '100px',
		background: theme.palette.background.paper,
		'& > div': {
			'&:before': {
				borderBottom: '0px solid black !important'
			}
		},
		'& input': {
			boxSizing: 'inherit',
			marginLeft: '5px',
			marginTop: '4px'
		}
	},
	input: {
		'&::-webkit-outer-spin-button, &::-webkit-inner-spin-button': {
			'-webkit-appearance': 'none',
			margin: 0
		},
		'-moz-appearance': 'textfield' // Firefox
	},
	paytypeLabel: {
		'&& > label': {
			fontSize: '16px'
		}
	},
	pay_amount: {
		'&& > p': {
			marginTop: '-27px'
		}
	},
	modal: {
		position: 'absolute',
		display: 'flex',
		alignItems: 'center',
		justifyContent: 'center',
		marginTop: '15%',
		marginLeft: '35%',
		//transform: 'translate(-50%, -50%)',
		width: 350,
		height: 280,
		border: '2px solid #000',
		boxShadow: 24,
		p: 4,
		color: theme.palette.background.paper,
		backgroundColor: theme.palette.secondary.light
	},
	cusotmerModal: {
		margin: 'auto',
		backgroundColor: 'white',
		width: '800px',
		height: '600px',
		maxWidth: '800px',
		maxHeight: '600px',
		borderRadius: '20px',
		overflow: 'scroll'
	},
	paper: {
		backgroundColor: theme.palette.background.paper,
		border: `2px solid ${theme.palette.primary[800]}`,
		boxShadow: theme.shadows[5],
		padding: theme.spacing(2, 4, 3)
	}
}));

function OrderForm(props) {
	const history = useHistory();
	const [odrId, setOrderId] = useState(null);
	const orderIdFromState = useSelector(({ ordersManagement }) => ordersManagement?.order?.id);
	const [total, setTotal] = useState('0.00');
	const [grossTotal, setGrossTotal] = useState('0.00');
	const [returnPlusDue, setReturnPlusDue] = useState('0.0');
	const [bonusPlusDiscount, setBonusPlusDiscount] = useState('0.00');
	const [productListRow, setProductListRow] = useState([]);
	const [editableRowIds, setEditableRowIds] = useState({});
	const [getReturnDue, setGetReturnDue] = useState(false);
	const [doUpdate, setdoUpdate] = useState(false);
	const [openCardOption, setOpenCardOption] = useState(false);
	const [getOrder, setGetOrder] = useState(false);
	const [product_Id, setProduct_Id] = useState();
	const [stock, setStock] = useState();
	const [quantityError, setQuantityError] = useState('');
	const authTOKEN = {
		headers: {
			'Content-type': 'application/json',
			Authorization: localStorage.getItem('jwt_access_token')
		}
	};
	const [shippingAddress, setShippingAddress] = useState([]);
	const [open, setOpen] = React.useState(false);
	const handleOpen = () => {
		fetch(`${SHIPPING_ADDRESS_BY_ORDERID}${odrId || orderIdFromState}`, authTOKEN)
			.then(res => res.json())
			.then(data => {
				const prevShippingAddress = shippingAddress;
				prevShippingAddress.push(data);
				setShippingAddress(prevShippingAddress);
				setOpen(true);
			});
	};
	const handleClose = () => setOpen(false);
	const userID = localStorage.getItem('UserID');
	const user = useSelector(({ auth }) => auth.user);
	const order = useSelector(({ ordersManagement }) => ordersManagement.order);
	const dispatch = useDispatch();
	const branchs = useSelector(state => state.data.branches);
	const users = useSelector(state => state.data.users);
	const products = useSelector(state => state.data.products);
	const thanas = useSelector(state => state.data.thanas);
	const cities = useSelector(state => state.data.cities);
	const countries = useSelector(state => state.data.countries);
	const payTypes = useSelector(state => state.data.paymentMathods);
	let serialNumber = 1;
	const dateObj = new Date();
	const currentDate = `${dateObj.getFullYear()}-${(dateObj.getMonth() + 1).toString().padStart(2, 0)}-${dateObj
		.getDate()
		.toString()
		.padStart(2, 0)}`;

	const classes = useStyles(props);

	const methods = useFormContext();
	const { control, formState, reset, getValues, watch, setError, setValue } = methods;
	const formValues = getValues();
	const dateOfSale = formValues.order_date;
	const { errors } = formState;

	const branchId = watch('order_branch');
	const userId = watch('user');
	const shippingAddressNew = watch('shipping_address');
	const orderDate = watch('order_date');
	const isPaid = watch('is_paid');
	const productId = watch('product');
	const size = watch('size');
	const color = watch('color');
	const quantity = Number(watch('quantity'));
	const price = watch('price');
	const availableQuantity = watch('availableQuantity');
	const routeParams = useParams();
	const { orderId } = routeParams;
	const Theme = useTheme();
	const PrimaryBg = Theme.palette.primary[500];
	const PrimaryDark = Theme.palette.primary[700];
	const PrimaryLight = Theme.palette.primary[400];
	const PaperColor = Theme.palette.background.paper;
	const primaryBlack = Theme.palette.primary.dark;
	const [openModal, setOpenModal] = useState(false);
	const [colors, setColors] = useState([]);
	const [sizes, setSizes] = useState([]);

	const handleDelete = localStorage.getItem('orderEvent');

	useEffect(() => {
		dispatch(getBranches());
		dispatch(getUsers());
		dispatch(getProducts());
		dispatch(getPaymentMathods());
		dispatch(getThanas());
		dispatch(getCities());
		dispatch(getCountries());
		reset({ ...formValues, order_date: currentDate, quantity: 0, availableQuantity: 0 });
	}, []);

	//set updated item for update view
	useEffect(() => {
		orderIdFromState && setUpdatedItem();
	}, [orderIdFromState]);

	//update order
	useEffect(() => {
		if (doUpdate && routeParams.orderId !== 'new' && handleDelete !== 'Delete') {
			const data = getValues();
			data.branch = data.order_branch;

			dispatch(updateOrder(data));
		}
		setdoUpdate(false);
	}, [doUpdate]);

	//setReturnDue
	useEffect(() => {
		if (getReturnDue) {
			const newPaidAmount = watch('pay_amount') || 0;
			const returnDue = grossTotal - Number(newPaidAmount).toFixed(2);

			setReturnPlusDue(Number(returnDue).toFixed(2));
		}

		setGetReturnDue(false);
	}, [getReturnDue]);

	//order
	useEffect(() => {
		if (getOrder && handleDelete !== 'Delete') {
			console.log('branch', getValues().order_branch, branchId);

			dispatch(
				saveCustomOrder({
					branch: branchId,
					user: userId,
					order_date: orderDate?.slice(0, 10),
					product: productId,
					shipping_address: shippingAddressNew,
					quantity,
					size: size || null,
					color: color || null,
					price,
					is_online: false
				})
			).then(action => {
				if (action?.payload?.order_details?.id) {
					setTotal(action.payload?.order_details?.net_amount);
					const totalGross =
						Number(action.payload?.order_details?.net_amount).toFixed(2) -
						Number(bonusPlusDiscount).toFixed(2);
					setGrossTotal(totalGross.toFixed(2));
					setReturnPlusDue((totalGross.toFixed(2) - (watch('pay_amount') || 0)).toFixed(2));

					axios
						.get(`${GET_CUSTOMER_ORDER_DETAILS}${action.payload?.order_details?.id}`, authTOKEN)
						.then(response => {
							const rowList = [];
							response?.data?.map(data => {
								rowList.push({
									id: data.id,
									product_id: data?.product?.id,
									image: data?.product?.thumbnail,
									productdatails: data?.product?.name,
									quantity: data?.quantity,
									price: data?.price,
									size: data?.size,
									color: data?.color,
									totalPrice: data?.subtotal
								});
								return null;
							});
							setProductListRow(rowList);
						});
					setOrderId(action.payload?.order_details?.id);
				}
			});
			setGetOrder(false);
		}
	}, [getOrder, branchId, userId]);

	console.log('getValues', formValues, branchId);
	//set product item list
	useEffect(() => {
		let newEdiableRowIds = {};
		productListRow.map(data => {
			newEdiableRowIds = { ...newEdiableRowIds, [data.id]: false };
			return null;
		});
		setEditableRowIds(newEdiableRowIds);
	}, [productListRow]);

	//set Updated Item
	function setUpdatedItem() {
		axios.get(`${GET_CUSTOMER_ORDER_DETAILS}${odrId || orderIdFromState}`, authTOKEN).then(response => {
			const rowList = [];
			let totalPrice = 0;
			response?.data?.map(data => {
				rowList.push({
					id: data?.id,
					image: data.product?.thumbnail,
					product_id: data?.product?.id,
					productdatails: data.product?.name,
					quantity: data?.quantity,
					price: data?.price,
					size: data?.size,
					color: data?.color,
					totalPrice: data?.subtotal
				});
				totalPrice += Number(data?.subtotal);
				return null;
			});

			setTotal(totalPrice.toFixed(2));
			const totalGrossAmount = totalPrice.toFixed(2) - Number(bonusPlusDiscount).toFixed(2);
			setGrossTotal(totalGrossAmount.toFixed(2));
			setReturnPlusDue(Number(totalGrossAmount.toFixed(2) - (watch('pay_amount') || 0)).toFixed(2));
			setProductListRow(rowList);
		});
	}
	function updateProductItem(itemId) {
		console.log('updateProductItem', itemId);
		const productitemPrice = watch(`product_price${itemId.id}`);
		const productitemQuantity = watch(`product_quantity${itemId.id}`);

		setQuantityError('');
		fetch(`${GET_INVENTORY_BY_PRODUCT_ID}${itemId?.product_id}`)
			.then(res => res.json())
			.then(data => {
				const quantitty = data?.in_stock + itemId.quantity;

				if (quantitty >= productitemQuantity) {
					const data = {
						quantity: productitemQuantity,
						price: productitemPrice,
						size: itemId.size?.id || null,
						color: itemId.color?.id || null
					};

					axios
						.put(`${UPDATE_PRODUC_TITEM}${odrId || orderIdFromState}/${itemId.id}`, data, authTOKEN)
						.then(res => {
							localStorage.setItem('orderAlert', 'updateOrder');
							setUpdatedItem();
						});
				} else {
					setQuantityError('Stock Not Available');
				}
			});
	}

	//remove product item
	function removeProductItem(itmId) {
		axios.delete(`${REMOVE_PRODUC_TITEM}${odrId || orderIdFromState}/${itmId}`, authTOKEN).then(() => {
			setUpdatedItem();
		});
	}

	//confirmOrder
	const confirmOrder = () => {
		const confirmData = {
			pay_amount: watch('pay_amount'),
			payment_method: watch('payment_method'),
			delivered_at: watch('delivered_at')
		};

		if (confirmData.pay_amount && confirmData.payment_method && confirmData.delivered_at) {
			axios.post(`${CONFIRM_CUSTOMER_ORDER}${odrId || orderIdFromState}`, confirmData, authTOKEN).then(res => {
				localStorage.setItem('orderAlert', 'paymentOrder');
				history.push('/apps/order-managements/orders');
			});
		} else {
			if (!confirmData.payment_method) {
				setError('payment_method', {
					type: 'manual',
					message: 'Select A payment Mathod'
				});
			}
			if (!confirmData.pay_amount) {
				setError('pay_amount', {
					type: 'manual',
					message: 'Enter A Valid Amount'
				});
			}
			if (!confirmData.delivered_at) {
				setError('delivered_at', {
					type: 'manual',
					message: 'Delivery Date is required'
				});
			}
		}
	};

	//card option close
	const handleCloseCardModal = () => {
		setOpenCardOption(false);
	};

	//get inventory
	useEffect(() => {
		axios.get(`${GET_INVENTORY_BY_PRODUCT_ID}${product_Id}`, authTOKEN).then(res => {
			setStock(res?.data?.in_stock);
		});
	}, [product_Id]);

	function handleSaveCustomer() {
		console.log('data', getValues());
		const data = getValues();
		const newData = {
			first_name: data.first_name,
			last_name: data.last_name,
			country_code1: data.country_code1,
			primary_phone: data.primary_phone,
			thana: data.thana,
			postal_code: data.postal_code,
			email: data.email,
			password: data.password,
			confirmPassword: data.confirmPassword,
			country: data.country
		};
		dispatch(saveCustomer(newData)).then(() => {
			setOpenModal(false);
			dispatch(getUsers());
		});
	}

	// get color and size by through product
	const getProductColor = product => {
		const authTOKEN = {
			headers: {
				'Content-type': 'application/json',
				Authorization: localStorage.getItem('jwt_access_token')
			}
		};

		fetch(`${GET_COLOR_PRODUCT_ID}${product}`, authTOKEN)
			.then(res => res.json())
			.then(data => {
				setColors(data);
			});
	};
	const getProductSize = product => {
		const authTOKEN = {
			headers: {
				'Content-type': 'application/json',
				Authorization: localStorage.getItem('jwt_access_token')
			}
		};

		fetch(`${GET_SIZE_BY_PRODUCTID_COLORID}?product=${productId}&color=${product}`, {
			headers: {
				'content-type': 'application/json',
				Authorization: localStorage.getItem('jwt_access_token')
			}
		})
			.then(response => response.json())
			.then(getSizeData => {
				setSizes(getSizeData);
			});
	};

	const cellStyle = { border: `1px solid ${Theme.palette.grey[600]}`, fontWeight: 'bold', color: primaryBlack };

	console.log('oreserDetails', order);
	return (
		<>
			<Grid container spacing={1} className="mb-16">
				<Grid item xs={12}>
					<Div pd="2" h="40px">
						<Text fs="20px" color="dark">
							Inventory Sale
						</Text>
						{!order?.is_online && <Btn>New Sale</Btn>}
					</Div>
				</Grid>
				<Grid item xs={12}>
					<Div bg={PrimaryBg} h="2" justCont="center">
						<Text fs="17px">Customer Payment</Text>
					</Div>
				</Grid>

				{/* left... */}
				<Grid item xs={12} md={5} style={{ paddingLeft: '30px', marginTop: '20px' }}>
					<div>
						<Controller
							name={orderId === 'new' ? 'created_by' : 'updated_by'}
							control={control}
							defaultValue={userID}
							render={({ field }) => {
								return (
									<TextField
										{...field}
										size="small"
										className={classes.hidden}
										label="created by"
										id="created_by"
										error={false}
										required
										variant="outlined"
										fullWidth
									/>
								);
							}}
						/>

						<Controller
							name="order_branch"
							control={control}
							render={({ field: { onChange, value, name } }) => (
								<Autocomplete
									className="mt-8 mb-16"
									freeSolo
									value={value ? branchs.find(data => data.id === value) : null}
									options={branchs}
									getOptionLabel={option => `${option?.name}`}
									onChange={(event, newValue) => {
										onChange(newValue?.id);
										setdoUpdate(true);
									}}
									renderInput={params => (
										<TextField
											{...params}
											size="small"
											placeholder="Select Branch"
											label="Branch"
											error={!!errors.order_branch}
											required
											helperText={errors?.order_branch?.message}
											variant="outlined"
											autoFocus
											InputLabelProps={{
												shrink: true
											}}
										/>
									)}
								/>
							)}
						/>
						<Grid container spacing={2}>
							<Grid item xs={11} md={11} lg={11} xl={11}>
								<Controller
									name="user"
									control={control}
									render={({ field: { onChange, value, name } }) => (
										<Autocomplete
											className="mt-8 mb-16"
											freeSolo
											value={value ? users.find(data => data.id === value) : 'N/A'}
											options={users}
											getOptionLabel={option =>
												`${option.first_name} ${option.last_name} [${
													option.primary_phone ? option.primary_phone.substring(3) : ''
												}]`
											}
											onChange={(event, newValue) => {
												onChange(newValue?.id);
												setdoUpdate(true);
											}}
											renderInput={params => (
												<TextField
													{...params}
													size="small"
													placeholder="Select User"
													label="Customer"
													error={!!errors.user}
													required
													helperText={errors?.user?.message}
													variant="outlined"
													InputLabelProps={{
														shrink: true
													}}
												/>
											)}
										/>
									)}
								/>
							</Grid>
							<Grid item xs={1} md={1} lg={1} xl={1}>
								<Tooltip title="Add Customer" arrow>
									<IconButton
										className="mt-8 mb-16"
										edge="start"
										aria-label="icon"
										onClick={() => setOpenModal(true)}
										style={{
											background: '#dddddd',
											borderRadius: '5px',
											height: '37px',
											width: '42px'
											// marginLeft: '-14px'
										}}
									>
										<Icon style={{ color: 'green' }}>add_to_photos</Icon>
									</IconButton>
								</Tooltip>
							</Grid>
						</Grid>

						{orderId == 'new' && (
							<Controller
								name="shipping_address"
								control={control}
								render={({ field }) => {
									return (
										<TextField
											{...field}
											size="small"
											label="Shipping address"
											error={!!errors.shipping_address}
											helperText={errors?.shipping_address?.message}
											id="shipping_address"
											required
											variant="outlined"
											fullWidth
										/>
									);
								}}
							/>
						)}
						{orderId !== 'new' && (
							<Controller
								name="delivered_at"
								control={control}
								render={({ field }) => (
									<KeyboardDatePicker
										{...field}
										autoOk
										className="w-full mt-8 mb-16"
										variant="inline"
										inputVariant="outlined"
										label="Delivered Date"
										format="dd/MM/yyyy"
										error={!!errors.delivered_at}
										helperText={errors.delivered_at?.message || ''}
										onChange={value => {
											field.onChange(moment(value).format('YYYY-MM-DD'));
											setdoUpdate(true);
										}}
										InputAdornmentProps={{ position: 'start' }}
									/>
								)}
							/>
						)}
						{orderId !== 'new' && (
							<Box>
								{order?.user?.email && (
									<Typography
										variant="p"
										gutterBottom
										component="div"
										style={{
											padding: '8px',
											marginBottom: '8px',
											border: '1px solid grey',
											borderRadius: '5px'
										}}
									>
										{order?.user?.email}
									</Typography>
								)}
								{order?.user_phone_number && (
									<Typography
										variant="p"
										gutterBottom
										component="div"
										style={{
											padding: '8px',
											border: '1px solid grey',
											borderRadius: '5px'
										}}
									>
										{order?.user_phone_number}
									</Typography>
								)}
							</Box>
						)}
						<TextField
							size="small"
							label="Comment"
							value={order?.comment ? order?.comment : 'No Comment'}
							multiline
							rows={3}
							id="Comment"
							required
							variant="outlined"
							fullWidth
						/>
					</div>
					{/* {shippingAddress.map(address => (
						<Modal
							aria-labelledby="transition-modal-title"
							aria-describedby="transition-modal-description"
							className={classes.modal}
							open={open}
							onClose={handleClose}
							//BackdropComponent={Backdrop}
							// BackdropProps={{
							// 	timeout: 500,
							// }}
						>
							<Box>
								<Typography
									variant="h6"
									gutterBottom
									component="div"
									style={{
										padding: '6px',
										marginBottom: '8px',
										color: 'white',
										backgroundColor: '#6495ED',
										//border: '1px solid grey',
										borderRadius: '5px'
									}}
								>
									Shipping Address
								</Typography>
								<Typography id="transition-modal-title" variant="p" component="div">
									Order No: {address?.order}
								</Typography>
								<Typography id="transition-modal-title" variant="p" component="div">
									Name: {address?.name}
								</Typography>
								<Typography id="transition-modal-title" variant="p" component="div">
									Email: {address?.email}
								</Typography>
								<Typography id="transition-modal-title" variant="p" component="div">
									Phone: {address?.phone}
								</Typography>
								<Typography id="transition-modal-title" variant="p" component="div">
									address: {address?.street_address}
								</Typography>
								<Typography id="transition-modal-title" variant="p" component="div">
									Thana:{' '}
									{_.isEmpty(thanas) || thanas?.find(thana => thana.id === address.thana)?.name}
								</Typography>
								<Typography id="transition-modal-title" variant="p" component="div">
									City: {_.isEmpty(cities) || cities?.find(city => city.id === address.city)?.name}
								</Typography>
								<Typography
									id="transition-modal-title"
									variant="p"
									component="div"
									style={{ marginBottom: '8px' }}
								>
									Country:{' '}
									{_.isEmpty(countries) ||
										countries?.find(country => country.id === address.country)?.name}
								</Typography>
								<Box>
									<Button
										variant="outlined"
										style={{
											color: 'white',
											backgroundColor: 'green',
											marginRight: '5px'
										}}
										onClick={handleClose}
									>
										Ok
									</Button>
									<Button
										variant="outlined"
										style={{
											color: 'white',
											backgroundColor: 'purple'
										}}
										onClick={() => window.print()}
									>
										Print
									</Button>
								</Box>
							</Box>
						</Modal>
					))} */}
				</Grid>

				<Grid item xs={12} md={2} />

				{/* right... */}
				<Grid item xs={12} md={5}>
					<Grid container spacing={1}>
						<Grid item xs={6}>
							<Div h="1" top={PrimaryDark} justCont="flex-start" pd="2" border>
								<Text color="dark" bold>
									Total:
								</Text>
							</Div>
						</Grid>
						<Grid item xs={6}>
							<Div h="1" top={PrimaryDark} justCont="flex-start" pd="2" border>
								<Text color="dark" bold>
									{total}
								</Text>
							</Div>
						</Grid>
					</Grid>

					<Grid container spacing={1}>
						<Grid item xs={6}>
							<Div h="1" top={PrimaryDark} justCont="flex-start" pd="2" border>
								<Text color="dark" bold>
									Bonus Point + Discount :
								</Text>
							</Div>
						</Grid>
						<Grid item xs={6}>
							<Div h="1" top={PrimaryDark} justCont="flex-start" pd="2" border>
								<Text color="dark" bold>
									{bonusPlusDiscount}
								</Text>
							</Div>
						</Grid>
					</Grid>

					<Grid container spacing={1}>
						<Grid item xs={6}>
							<Div h="1" top={PrimaryDark} justCont="flex-start" pd="2" border>
								<Text color="dark" bold>
									Gross Total :
								</Text>
							</Div>
						</Grid>
						<Grid item xs={6}>
							<Div h="1" top={PrimaryDark} justCont="flex-start" pd="2" border>
								<Text color="dark" bold>
									{grossTotal}
								</Text>
							</Div>
						</Grid>
					</Grid>

					<Grid container spacing={1}>
						<Grid item xs={12}>
							<Controller
								name="payment_method"
								control={control}
								render={({ field: { onChange, value, name } }) => {
									return (
										<Autocomplete
											className="mt-8 mb-16"
											freeSolo
											value={value ? payTypes.find(data => data.id === value) : null}
											options={payTypes}
											getOptionLabel={option => `${option?.name}`}
											onChange={(event, newValue) => {
												onChange(newValue?.id);
												newValue?.name === 'Card' && setOpenCardOption(true);
												order?.payment_method && setdoUpdate(true);
											}}
											renderInput={params => (
												<TextField
													{...params}
													className={classes.paytypeLabel}
													size="small"
													placeholder="Select Payment Pathod"
													label="Pay Type"
													error={!!errors.payment_method}
													helperText={errors?.payment_method?.message}
													variant="outlined"
													required
													InputLabelProps={{
														shrink: true
													}}
												/>
											)}
										/>
									);
								}}
							/>
						</Grid>
					</Grid>

					<Modal
						aria-labelledby="transition-modal-title"
						aria-describedby="transition-modal-description"
						className={classes.modal}
						open={openCardOption}
						onClose={handleCloseCardModal}
						closeAfterTransition
						BackdropComponent={Backdrop}
						BackdropProps={{
							timeout: 500
						}}
					>
						<Fade in={openCardOption}>
							<div className={classes.paper}>
								<h2 id="transition-modal-title">Card Payment Option</h2>
								<p id="transition-modal-description" />
							</div>
						</Fade>
					</Modal>

					<Grid container spacing={1}>
						<Grid item xs={6}>
							<Div h="1" top={PrimaryDark} justCont="flex-start" pd="2" border>
								<Text color="dark" bold>
									Paid Amount :
								</Text>
							</Div>
						</Grid>
						<Grid item xs={6}>
							<Div h="1" top={PrimaryDark} justCont="flex-start" pd="2" border>
								<Input>
									<Controller
										name="pay_amount"
										control={control}
										render={({ field }) => {
											return (
												<TextField
													{...field}
													className={classes.pay_amount}
													onChange={(event, newValue) => {
														field.onChange(event);
														setGetReturnDue(true);
													}}
													type="number"
													size="small"
													error={!!errors?.pay_amount}
													helperText={errors?.pay_amount?.message}
													id="pay_amount"
													required
													variant="outlined"
													InputLabelProps={field.value && { shrink: true }}
													fullWidth
												/>
											);
										}}
									/>
								</Input>
							</Div>
						</Grid>
					</Grid>

					<Grid container spacing={1}>
						<Grid item xs={6}>
							<Div h="1" top={PrimaryDark} justCont="flex-start" pd="2" border>
								<Text color="dark" bold>
									Return(+)/Due() :
								</Text>
							</Div>
						</Grid>
						<Grid item xs={6}>
							<Div h="1" top={PrimaryDark} justCont="flex-start" pd="2" border>
								<Text color="dark" bold>
									{returnPlusDue}
								</Text>
							</Div>
						</Grid>
					</Grid>

					<Grid container spacing={1}>
						{/* {orderId !== 'new' ? (
							<Grid item xs={6}>
								<Div h="1" top={PrimaryDark} justCont="flex-start" pd="2" border>
									<Btn
										h="30px"
										style={{ backgroundColor: '#6495ED', color: 'white', width: '100%' }}
										onClick={handleOpen}
									>
										Shipping Address
									</Btn>
								</Div>
							</Grid>
						) : ( */}
						<Grid item xs={12}>
							<Div h="1" top={PrimaryDark} justCont="flex-start" pd="2" border>
								<Btn
									h="30px"
									style={{ backgroundColor: 'green', color: 'white', width: '100%' }}
									onClick={confirmOrder}
								>
									Bill Payment
								</Btn>
							</Div>
						</Grid>
						{/* )} */}
						{/* {orderId !== 'new' && ( */}
						{/* <Grid item xs={6}>
							<Div h="1" top={PrimaryDark} justCont="flex-start" pd="2" border>
								<Btn
									h="30px"
									style={{ backgroundColor: 'green', color: 'white' }}
									onClick={confirmOrder}
								>
									Bill Payment
								</Btn>
							</Div>
						</Grid> */}
						{/* )} */}
					</Grid>

					<Grid container spacing={1}>
						<Grid item xs={12}>
							<Div h="1" top={PrimaryDark} justCont="flex-start" pd="2" border>
								<Btn h="30px" style={{ width: '100%' }}>
									Summery Report
								</Btn>
							</Div>
						</Grid>
						{/* <Grid item xs={6}>
							<Div h="1" top={PrimaryDark} justCont="flex-start" pd="2" border>
								<Btn h="30px">New Sale</Btn>
							</Div>
						</Grid> */}
					</Grid>
				</Grid>

				{/* buttom... */}
				<Grid item xs={12}>
					<Div bg={PrimaryBg} h="2" pd="2">
						<Text>Sale Inventory</Text>
						<Text>{user.role || user.displayName}</Text>
						<div className={classes.dateOfSaleContainer}>
							{order?.is_online ? (
								''
							) : (
								<Controller
									name="order_date"
									control={control}
									render={({ field }) => (
										<TextField
											{...field}
											onChange={(event, newValue) => {
												field.onChange(event);
												setdoUpdate(true);
											}}
											value={currentDate}
											color="primary"
											className={classes.dateOfSale}
											id="order_date"
											type="date"
											defaultValue={currentDate}
											fullWidth
											InputLabelProps={{
												shrink: true
											}}
										/>
									)}
								/>
							)}
							<Text style={{ marginLeft: '10px' }}>
								Date Of Sale : {dateOfSale ? dateOfSale.slice(0, 10) : currentDate}
							</Text>
						</div>
					</Div>
				</Grid>

				{!order?.is_online && (
					<Grid item xs={2}>
						<Div bg={PrimaryBg} h="1" justCont="center">
							<Text>Bar Code</Text>
						</Div>
					</Grid>
				)}

				{!order?.is_online && (
					<Grid item xs={2}>
						<Div bg={PrimaryBg} h="1" justCont="center">
							<Text>Product Name</Text>
						</Div>
					</Grid>
				)}
				{!order?.is_online && (
					<Grid item xs={4}>
						<Div bg={PrimaryBg} h="1" justCont="center">
							<Text>Color & Size</Text>
						</Div>
					</Grid>
				)}
				{/* {!order?.is_online && (
					<Grid item xs={1}>
						<Div bg={PrimaryBg} h="1" justCont="center">
							<Text>Size</Text>
						</Div>
					</Grid>
				)} */}

				{!order?.is_online && (
					<Grid item xs={1}>
						<Div bg={PrimaryBg} h="1" justCont="center">
							<Text style={{ fontSize: '10px', fontWeight: 600 }}>Sale Qty</Text>
						</Div>
					</Grid>
				)}

				{!order?.is_online && (
					<Grid item xs={1}>
						<Div bg={PrimaryBg} h="1" justCont="center">
							<Text style={{ fontSize: '10px', fontWeight: 600 }}>Available Qty</Text>
						</Div>
					</Grid>
				)}

				{!order?.is_online && (
					<Grid item xs={2}>
						<Div bg={PrimaryBg} h="1" justCont="center">
							<Text>Price</Text>
						</Div>
					</Grid>
				)}

				{!order?.is_online && (
					<Grid item xs={2}>
						<Div bg={PrimaryBg} h="3" justCont="center">
							<Input bg={PaperColor} size="large" h="35px">
								<Controller
									name="barcode"
									control={control}
									render={({ field }) => {
										return (
											<TextField
												{...field}
												size="small"
												onBlur={event => {
													const productItem = products.find(
														data => data.barcode === event.target.value
													);
													const getQuantity = watch('quantity');
													reset({
														...formValues,
														price: productItem?.unit_price,
														product: productItem?.id,
														quantity: getQuantity || 0
													});
													setGetOrder(true);
												}}
												// error={!!errors.barcode}
												// helperText={errors?.barcode?.message}
												id="barcode"
												required
												variant="outlined"
												InputLabelProps={field.value && { shrink: true }}
												fullWidth
											/>
										);
									}}
								/>
							</Input>
						</Div>
					</Grid>
				)}

				{!order?.is_online && (
					<Grid item xs={2}>
						<Div bg={PrimaryBg} h="3" justCont="center">
							<Input bg={PaperColor} size="large">
								<Controller
									name="product"
									control={control}
									render={({ field: { onChange, value, name } }) => {
										const filteredProduct = products.filter(
											product => product.is_in_stock === true
										);
										return (
											<Autocomplete
												freeSolo
												value={value ? products.find(data => data.id === value) : null}
												options={filteredProduct}
												getOptionLabel={option => `${option?.name}`}
												onChange={(event, newValue) => {
													onChange(newValue?.id);
													const availableQuantityy = watch('availableQuantity');
													setQuantityError('');
													getProductColor(newValue?.id);
													fetch(`${GET_INVENTORY_BY_PRODUCT_ID}${newValue?.id}`)
														.then(res => res.json())
														.then(data => {
															if (data?.in_stock >= 1) {
																reset({
																	...formValues,
																	price: newValue?.unit_price,
																	product: newValue?.id,
																	quantity: quantity || 0,
																	availableQuantity: data.in_stock
																});
																// setGetOrder(true);
															} else {
																setQuantityError('Stock Not Available');
															}
														});
												}}
												renderInput={params => (
													<TextField
														{...params}
														size="small"
														placeholder="Select Products"
														error={!!errors.product}
														required
														helperText={errors?.product?.message}
														variant="outlined"
														InputLabelProps={{
															shrink: true
														}}
													/>
												)}
											/>
										);
									}}
								/>
							</Input>
						</Div>
					</Grid>
				)}
				{!order?.is_online && (
					<Grid item xs={2}>
						<Div bg={PrimaryBg} h="3" justCont="center">
							<Input bg={PaperColor} size="large">
								<Controller
									name="color"
									control={control}
									render={({ field: { onChange, value, name } }) => (
										<Autocomplete
											freeSolo
											value={value ? colors.find(data => data.id === value) : null}
											options={colors}
											disabled={colors.length === 0}
											getOptionLabel={option => `${option?.name}`}
											onChange={(event, newValue) => {
												onChange(newValue?.id);
												getProductSize(newValue?.id);
											}}
											renderInput={params => (
												<TextField
													{...params}
													size="small"
													placeholder={
														colors.length === 0 ? 'No Color Available' : 'Select Color'
													}
													error={!!errors.color}
													required
													helperText={errors?.color?.message}
													variant="outlined"
													InputLabelProps={{
														shrink: true
													}}
												/>
											)}
										/>
									)}
								/>
							</Input>
						</Div>
					</Grid>
				)}
				{!order?.is_online && (
					<Grid item xs={2}>
						<Div bg={PrimaryBg} h="3" justCont="center">
							<Input bg={PaperColor} size="large">
								<Controller
									name="size"
									control={control}
									render={({ field: { onChange, value, name } }) => (
										<Autocomplete
											freeSolo
											value={value ? sizes.find(data => data.id === value) : null}
											options={sizes}
											disabled={sizes.length === 0}
											getOptionLabel={option => `${option?.name}`}
											onChange={(event, newValue) => {
												onChange(newValue?.id);
											}}
											renderInput={params => (
												<TextField
													{...params}
													size="small"
													placeholder={
														sizes.length === 0 ? 'No Size Available' : 'Select Size'
													}
													error={!!errors.size}
													required
													helperText={errors?.size?.message}
													variant="outlined"
													InputLabelProps={{
														shrink: true
													}}
												/>
											)}
										/>
									)}
								/>
							</Input>
						</Div>
					</Grid>
				)}

				{!order?.is_online && (
					<Grid item xs={1}>
						<Div bg={PrimaryBg} h="3" justCont="center">
							<Input bg={PaperColor} size="small">
								<Controller
									name="quantity"
									control={control}
									render={({ field }) => {
										return (
											<TextField
												{...field}
												inputProps={{ min: 0, style: { textAlign: 'center' } }} // the change is here
												InputProps={{ classes: { input: classes.input } }} // Apply custom styles to hide the spinner
												type="number"
												size="small"
												error={!!errors.quantity}
												helperText={errors?.quantity?.message}
												id="quantity"
												required
												variant="outlined"
												InputLabelProps={field.value && { shrink: true }}
												fullWidth
												onChange={e => {
													const inputValue = parseInt(e.target.value, 10);

													if (inputValue > availableQuantity) {
														setQuantityError('Stock Not Available');
													} else {
														setQuantityError('');
														reset({
															...formValues,
															size,
															color
														});
														setGetOrder(true);
													}

													// Update the form value using setValue
													setValue('quantity', inputValue);

													console.log('New value:', inputValue);
												}}
											/>
										);
									}}
								/>
							</Input>
						</Div>
					</Grid>
				)}

				{!order?.is_online && (
					<Grid item xs={1}>
						<Div bg={PrimaryBg} h="3" justCont="center">
							<Input bg={PaperColor} size="small">
								<Controller
									name="availableQuantity"
									control={control}
									render={({ field }) => {
										return (
											<TextField
												{...field}
												size="small"
												inputProps={{ min: 0, style: { textAlign: 'center' } }} // the change is here
												value={availableQuantity}
												error={quantityError}
												helperText={quantityError ? '' : ''}
												id="availableQuantity"
												required
												variant="outlined"
												InputLabelProps={field.value && { shrink: true }}
												fullWidth
											/>
										);
									}}
								/>
							</Input>
						</Div>
					</Grid>
				)}

				{!order?.is_online && (
					<Grid item xs={2}>
						<Div bg={PrimaryBg} h="3" justCont="center">
							<Input bg={PaperColor} size="medium">
								<Controller
									name="price"
									control={control}
									render={({ field }) => {
										return (
											<TextField
												{...field}
												size="small"
												error={!!errors.price}
												helperText={errors?.price?.message}
												id="price"
												required
												variant="outlined"
												InputLabelProps={field.value && { shrink: true }}
												fullWidth
											/>
										);
									}}
								/>
							</Input>
						</Div>
					</Grid>
				)}

				<Grid item xs={10}>
					{quantityError && <Typography style={{ color: 'red' }}>{quantityError}</Typography>}
					<Div
						top={PrimaryDark}
						left={PrimaryDark}
						justCont="center"
						pd="1"
						h="fit-content"
						alignItems="space-between"
						border
					>
						<Grid container spacing={1}>
							<Grid item xs={12}>
								<Div bg={PrimaryBg} h="2" justCont="center">
									<Text>Use of Produts of this Sale</Text>
								</Div>
							</Grid>

							<Grid item xs={12}>
								<Div h="fit-content" pd="0" minHeight="272px" alignItems="flex-start">
									<TableContainer component={Paper} className={classes.tblContainer}>
										<Table className={classes.table} aria-label="simple table">
											<TableHead className={classes.tableHead}>
												<TableRow>
													<TableCell style={{ color: PaperColor }}>No.</TableCell>
													<TableCell
														whitespace-nowrap
														style={{ color: PaperColor }}
														align="center"
													>
														IMG
													</TableCell>
													<TableCell
														whitespace-nowrap
														style={{ color: PaperColor }}
														align="center"
													>
														Product Details
													</TableCell>
													<TableCell
														whitespace-nowrap
														style={{ color: PaperColor }}
														align="center"
													>
														Color
													</TableCell>
													<TableCell
														whitespace-nowrap
														style={{ color: PaperColor }}
														align="center"
													>
														Size
													</TableCell>
													<TableCell
														whitespace-nowrap
														style={{ color: PaperColor }}
														align="center"
													>
														Quantity
													</TableCell>
													<TableCell
														whitespace-nowrap
														style={{ color: PaperColor }}
														align="center"
													>
														Price
													</TableCell>
													<TableCell
														whitespace-nowrap
														style={{ color: PaperColor }}
														align="center"
													>
														Total
													</TableCell>
													<TableCell
														whitespace-nowrap
														style={{ color: PaperColor }}
														align="center"
													>
														Action
													</TableCell>
												</TableRow>
											</TableHead>

											<TableBody>
												{productListRow?.map((row, idx) => {
													console.log('productListRow', row);
													return (
														<TableRow key={row?.name}>
															<TableCell
																whitespace-nowrap
																style={cellStyle}
																component="th"
																scope="row"
															>
																{serialNumber++}
															</TableCell>
															<TableCell
																style={{
																	...cellStyle,
																	paddingTop: '0px',
																	paddingBottom: '0px'
																}}
																align="center"
															>
																<img
																	className="w-full block rounded"
																	style={{
																		borderRadius: '10%',
																		height: '50px',
																		width: '50px'
																	}}
																	src={`${BASE_URL}${row.image}`}
																	alt="Not Found"
																/>
															</TableCell>

															<TableCell
																whitespace-nowrap
																style={cellStyle}
																align="center"
															>
																{row.productdatails}
															</TableCell>
															<TableCell
																whitespace-nowrap
																style={cellStyle}
																align="center"
															>
																{`${row?.color?.name || null}`}
															</TableCell>

															<TableCell
																whitespace-nowrap
																style={cellStyle}
																align="center"
															>
																{`${row?.size?.name || null}`}
															</TableCell>

															<TableCell
																style={{ ...cellStyle, width: '15%' }}
																align="center"
															>
																{editableRowIds[row.id] ? (
																	<Controller
																		name={`product_quantity${row.id}`}
																		control={control}
																		render={({ field }) => {
																			return (
																				<TextField
																					{...field}
																					size="small"
																					error={quantityError}
																					helperText={quantityError ? '' : ''}
																					id={`product_quantity${row.id}`}
																					required
																					variant="outlined"
																					InputLabelProps={
																						field.value && { shrink: true }
																					}
																					fullWidth
																				/>
																			);
																		}}
																	/>
																) : (
																	row.quantity
																)}
															</TableCell>
															<TableCell
																style={{ ...cellStyle, width: '15%' }}
																align="center"
															>
																{editableRowIds[row.id] ? (
																	<Controller
																		name={`product_price${row.id}`}
																		control={control}
																		render={({ field }) => {
																			return (
																				<TextField
																					{...field}
																					size="small"
																					error={!!errors.product_price}
																					helperText={
																						errors?.product_price?.message
																					}
																					id={`product_price${row.id}`}
																					required
																					variant="outlined"
																					InputLabelProps={
																						field.value && { shrink: true }
																					}
																					fullWidth
																				/>
																			);
																		}}
																	/>
																) : (
																	row.price
																)}
															</TableCell>
															<TableCell
																whitespace-nowrap
																style={cellStyle}
																align="center"
															>
																{row.totalPrice}
															</TableCell>
															<TableCell
																whitespace-nowrap
																style={cellStyle}
																align="center"
															>
																{!order?.is_online && (
																	<div style={{ display: 'flex' }}>
																		{editableRowIds[row.id] ? (
																			<DoneOutlineIcon
																				style={{ color: 'green' }}
																				className="cursor-pointer"
																				onClick={e => {
																					setEditableRowIds({
																						...editableRowIds,
																						[row.id]: false
																					});
																					updateProductItem(row);
																				}}
																			/>
																		) : (
																			<EditIcon
																				style={{ color: 'green' }}
																				className="cursor-pointer"
																				onClick={e => {
																					setEditableRowIds({
																						...editableRowIds,
																						[row.id]: true
																					});
																					setProduct_Id(row?.product_id);
																					console.log('rowrow', row);
																					reset({
																						...formValues,
																						[`product_quantity${row.id}`]:
																							row.quantity,
																						[`product_price${row.id}`]:
																							row.price
																					});
																				}}
																			/>
																		)}{' '}
																		<DeleteIcon
																			style={{ color: 'red' }}
																			className="cursor-pointer"
																			onClick={() => removeProductItem(row.id)}
																		/>
																	</div>
																)}
															</TableCell>
														</TableRow>
													);
												})}
											</TableBody>
										</Table>
									</TableContainer>
								</Div>
							</Grid>
						</Grid>
					</Div>

					<Div bg={PrimaryBg} h="1" />
				</Grid>

				{!order?.is_online && (
					<Grid item xs={2}>
						<Div bg={PrimaryDark} h="2" justCont="center">
							<Text>Point &#38; Discount Panel</Text>
						</Div>
						<Div bg={PrimaryLight} h="350px" justCont="center" pd="2" wrap>
							<Div>
								<Text>* % Discount </Text>
								<Input size="medium" bg={PaperColor}>
									<Controller
										name="discount"
										control={control}
										render={({ field }) => {
											return (
												<TextField
													{...field}
													size="small"
													// error={!!errors.discount}
													// helperText={errors?.discount?.message}
													id="discount"
													required
													variant="outlined"
													InputLabelProps={field.value && { shrink: true }}
													fullWidth
												/>
											);
										}}
									/>
								</Input>
							</Div>
							<Div>
								<Text>* Coustom Dis </Text>
							</Div>
							<Div>
								<Text>Amount </Text>
								<Input size="medium" bg={PaperColor}>
									<Controller
										name="Amount"
										control={control}
										render={({ field }) => {
											return (
												<TextField
													{...field}
													size="small"
													// error={!!errors.Amount}
													// helperText={errors?.Amount?.message}
													id="Amount"
													required
													variant="outlined"
													InputLabelProps={field.value && { shrink: true }}
													fullWidth
												/>
											);
										}}
									/>
								</Input>
							</Div>
							<Div>
								<Text>Bonus Point : </Text>
								<Input size="medium" bg={PaperColor}>
									<Controller
										name="bonusPoint"
										control={control}
										render={({ field }) => {
											return (
												<TextField
													{...field}
													size="small"
													// error={!!errors.bonusPoint}
													// helperText={errors?.bonusPoint?.message}
													id="bonusPoint"
													required
													variant="outlined"
													InputLabelProps={field.value && { shrink: true }}
													fullWidth
												/>
											);
										}}
									/>
								</Input>
							</Div>
							<Div>
								<Text>Red Point : </Text>
								<Input size="medium" bg={PaperColor}>
									<Controller
										name="redPoint"
										control={control}
										render={({ field }) => {
											return (
												<TextField
													{...field}
													size="small"
													// error={!!errors.redPoint}
													// helperText={errors?.redPoint?.message}
													id="redPoint"
													required
													variant="outlined"
													InputLabelProps={field.value && { shrink: true }}
													fullWidth
												/>
											);
										}}
									/>
								</Input>
							</Div>
							<Div justCont="center">
								<Btn h="20" bg={PaperColor}>
									Re Calculate
								</Btn>
							</Div>
						</Div>
					</Grid>
				)}
			</Grid>
			<FuseScrollbars className="flex-grow overflow-x-auto">
				<Modal
					open={openModal}
					className={classes.cusotmerModal}
					onClose={() => {
						setOpenModal(false);
					}}
					keepMounted
				>
					<Card style={{ marginBottom: '10px' }}>
						<div>
							<div
								style={{ display: 'flex', justifyContent: 'space-between', padding: '10px' }}
								className=""
							>
								<Typography className="text-center m-10" variant="h5" component="div">
									Add Customer
								</Typography>
								<CloseIcon
									onClick={event => {
										setOpenModal(false);
										// reset({});
										// setValue('amount', '');
										// setValue('date', moment(new Date()).format('YYYY-MM-DD'));
										// setValue('cheque_no', '');
										// setBankOrCash(false);
									}}
									className="cursor-pointer custom-delete-icon-style"
									// style={{ color: 'red' }}
								/>
							</div>
							<div style={{ padding: '10px' }}>
								<CustomerForm fromOrder={true} />

								<div className="text-center">
									<Button
										// component={Link}
										to="/apps/noc-management/new"
										className="whitespace-nowrap text-center"
										variant="contained"
										color="secondary"
										onClick={() => handleSaveCustomer()}
									>
										<span className="hidden sm:flex">Save</span>{' '}
										<span className="flex sm:hidden">Save</span>
									</Button>
								</div>
							</div>
						</div>
					</Card>
				</Modal>
			</FuseScrollbars>
		</>
	);
}

export default OrderForm;

const btn = styled(({ color, ...other }) => <Button variant="outlined" {...other} />)({});
const Btn = styled(btn)(props => ({
	borderRadius: '3px',
	fontWeight: 'bold',
	borderWidth: '2px',
	height: props.h ? props.h : '40px',
	background: props.bg
}));

const Text = styled('b')(props => ({
	color: props.color === 'dark' ? 'black' : 'white',
	fontSize: props.fs ? props.fs : '15px',
	fontWeight: props.bold ? 'bold' : '500'
}));

const Input = styled('div')(props => ({
	background: props.bg,
	height: props.h ? props.h : '35px',
	width: props.size === 'large' ? '100%' : props.size === 'medium' ? '110px' : props.size === 'small' && '70px',
	margin: '10px',
	borderRadius: '2px',
	fontWeight: 'bold',
	color: props.color
}));

const Div = styled('div')(props => ({
	background: props.bg,
	height: props.h === '1' ? '25px' : props.h === '2' ? '35px' : props.h === 3 ? '50px' : props.h && props.h,
	width: props.w ? props.w : '100%',
	display: 'flex',
	justifyContent: props.justCont ? props.justCont : 'space-between',
	alignItems: props.alignItems ? props.alignItems : 'center',
	padding: props.pd === '1' ? '10px' : props.pd === '2' ? '20px' : props.pd && props.pd,
	flexWrap: props.wrap && 'wrap',
	border: props.border && '2px solid gray',
	borderTop: `4px solid ${props.top}`,
	borderLeft: `4px solid ${props.left}`,
	minHeight: props.minHeight
}));

// const PaidAmountTextField = styled(TextField)(props => ({

// }));
