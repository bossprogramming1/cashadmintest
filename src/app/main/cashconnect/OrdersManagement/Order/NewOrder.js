import FusePageCarded from '@fuse/core/FusePageCarded';
import { useDeepCompareEffect } from '@fuse/hooks';
import { yupResolver } from '@hookform/resolvers/yup';
import { Button, Typography } from '@material-ui/core';
import { ORDER_CREATE, ORDER_DELETE, ORDER_DETAILS, ORDER_UPDATE } from 'app/constant/permission/permission';
import { getUserPermissions } from 'app/store/dataSlice';
import withReducer from 'app/store/withReducer';
import { motion } from 'framer-motion';
import React, { useEffect, useState } from 'react';
import { FormProvider, useForm } from 'react-hook-form';
import { useDispatch, useSelector } from 'react-redux';
import { Link, useParams } from 'react-router-dom';
import * as yup from 'yup';
import PagenotFound from '../../Pagenotfound/PagenotFound';
import reducer from '../store/index';
import { getOrder, newOrder, resetOrder } from '../store/orderSlice';
import NewOrderHeader from './NewOrderHeader';
import OrderForm from './OrderForm';

/**
 * Form Validation Schema
 */
const schema = yup.object().shape({
	order_branch: yup.string().required('Branch is required'),
	user: yup.string().required('User is required'),
	product: yup.string().required('Product is required'),
	payment_method: yup.string().required('Select A Payment Mathod'),
	paidAmount: yup.string().required('Enter A Valid Amount')
});

const Order = () => {
	const dispatch = useDispatch();
	const order = useSelector(({ ordersManagement }) => ordersManagement.order);
	const [noOrder, setNoOrder] = useState(false);
	const methods = useForm({
		mode: 'onChange',
		defaultValues: {},
		resolver: yupResolver(schema)
	});
	const routeParams = useParams();
	const UserPermissions = useSelector(state => state.data.UserPermissions);
	useEffect(() => {
		dispatch(getUserPermissions());
	}, []);
	const { reset, watch, control, onChange, formState } = methods;
	const form = watch();
	const orderDate = watch('order_date');
	const quantity = watch('quantity');

	useDeepCompareEffect(() => {
		function updateOrderState() {
			const { orderId } = routeParams;

			if (orderId === 'new') {
				localStorage.removeItem('event');
				/**
				 * Create New User data
				 */
				dispatch(newOrder());
			} else {
				/**
				 * Get User data
				 */

				dispatch(getOrder(orderId)).then(action => {
					/**
					 * If the requested product is not exist show message
					 */
					if (!action.payload) {
						setNoOrder(true);
					}
				});
			}
		}

		updateOrderState();
	}, [dispatch, routeParams]);

	useEffect(() => {}, []);

	useEffect(() => {
		if (!order) {
			return;
		}
		/**
		 * Reset the form on order state changes
		 */
		reset({
			...order,
			order_date: order?.order_date || orderDate,
			quantity,
			user: order?.user?.id,
			order_branch: order?.branch?.id,
			is_paid: order?.is_paid,
			paidAmount: order?.pay_amount,
			payment_method: order?.payment_method?.id,
			note: order?.note,
			created_by: order?.created_by?.id,
			updated_by: order?.updated_by?.id,
			order_status: order?.order_status?.id
		});
	}, [order, reset]);

	useEffect(() => {
		return () => {
			/**
			 * Reset Order on component unload
			 */
			dispatch(resetOrder());
			setNoOrder(false);
		};
	}, [dispatch]);

	/**
	 * Show Message if the requested products is not exists
	 */
	if (noOrder) {
		return (
			<motion.div
				initial={{ opacity: 0 }}
				animate={{ opacity: 1, transition: { delay: 0.1 } }}
				className="flex flex-col flex-1 items-center justify-center h-full"
			>
				<Typography color="textSecondary" variant="h5">
					There is no such order!
				</Typography>
				<Button
					className="mt-24"
					component={Link}
					variant="outlined"
					to="/apps/order-management/orders"
					color="inherit"
				>
					Go to Order Page
				</Button>
			</motion.div>
		);
	}

	return (
		<FormProvider {...methods}>
			{UserPermissions.includes(ORDER_CREATE) ||
			UserPermissions.includes(ORDER_UPDATE) ||
			UserPermissions.includes(ORDER_DELETE) ||
			UserPermissions.includes(ORDER_DETAILS) ? (
				<FusePageCarded
					classes={{
						toolbar: 'p-0',
						header: 'min-h-74 h-64'
					}}
					header={<NewOrderHeader />}
					content={
						<div className="pt-16 sm:pt-24">
							<OrderForm />
						</div>
					}
					innerScroll
				/>
			) : (
				<PagenotFound />
			)}
		</FormProvider>
	);
};
export default withReducer('ordersManagement', reducer)(Order);
