import { createAsyncThunk, createEntityAdapter, createSlice } from '@reduxjs/toolkit';
import useUserInfo from 'app/@customHooks/useUserInfo';
import axios from 'axios';
import { DELETE_CHAT, GET_All_CHATS, GET_All_CHATS_FOR_EMPLOYEE } from '../../../../constant/constants';

export const getChats = createAsyncThunk('chatManagement/chats/getChats', async parameter => {
	const { page, chat } = parameter;
	const user_role = localStorage.getItem('user_role');

	axios.defaults.headers.common['Content-type'] = 'application/json';
	axios.defaults.headers.common.Authorization = localStorage.getItem('jwt_access_token');

	const response = axios.get(user_role === 'ADMIN' ? GET_All_CHATS : GET_All_CHATS_FOR_EMPLOYEE, {
		params: { page, chat }
	});
	const data = await response;

	sessionStorage.setItem('chats_total_elements', data.data.total_elements);
	sessionStorage.setItem('chats_total_pages', data.data.total_pages);
	delete axios.defaults.headers.common['Content-type'];
	delete axios.defaults.headers.common.Authorization;

	return data.data.chats;
});

export const removeChats = createAsyncThunk(
	'chatManagement/chats/removeChats',
	async (chatIds, { dispatch, getState }) => {
		const authTOKEN = {
			headers: {
				'Content-type': 'application/json',
				Authorization: localStorage.getItem('jwt_access_token')
			}
		};
		await axios.delete(`${DELETE_CHAT}`, { chatIds }, authTOKEN);

		return chatIds;
	}
);

const chatsAdapter = createEntityAdapter({});

export const { selectAll: selectChats, selectById: selectChatById } = chatsAdapter.getSelectors(
	state => state.chatsManagement.chats
);

const chatsSlice = createSlice({
	name: 'chatManagement/chats',
	initialState: chatsAdapter.getInitialState({
		searchText: ''
	}),
	reducers: {
		setChatsSearchText: {
			reducer: (state, action) => {
				state.searchText = action.payload;
			},
			prepare: event => ({ payload: event.target.value || '' })
		}
	},
	extraReducers: {
		[getChats.fulfilled]: chatsAdapter.setAll
	}
});

export const { setData, setChatsSearchText } = chatsSlice.actions;
export default chatsSlice.reducer;
