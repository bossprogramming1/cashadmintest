import { createAsyncThunk, createSlice } from '@reduxjs/toolkit';
import axios from 'axios';
import { DELETE_CHAT } from '../../../../constant/constants';

export const removeChat = createAsyncThunk('chatManagement/chat/removeChat', async val => {
	const authTOKEN = {
		headers: {
			'Content-type': 'application/json',
			Authorization: localStorage.getItem('jwt_access_token')
		}
	};

	const chatId = val.id;
	const response = await axios.delete(`${DELETE_CHAT}${chatId}`, authTOKEN);
	return response;
});

const chatSlice = createSlice({
	name: 'chatManagement/chat',
	initialState: null,
	reducers: {
		resetChat: () => null,
		newChat: {
			reducer: (state, action) => action.payload,
			prepare: event => ({
				payload: {}
			})
		}
	},
	extraReducers: {
		[removeChat.fulfilled]: (state, action) => action.payload
	}
});

export const { newChat, resetChat } = chatSlice.actions;

export default chatSlice.reducer;
