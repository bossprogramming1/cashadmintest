import _ from '@lodash';
import Button from '@material-ui/core/Button';
import Icon from '@material-ui/core/Icon';
import { useTheme } from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';
import { motion } from 'framer-motion';
import React from 'react';
import { useFormContext } from 'react-hook-form';
import { useDispatch } from 'react-redux';
import { Link, useHistory, useParams } from 'react-router-dom';
import { removeChat } from '../store/chatSlice';

const NewChatHeader = () => {
	const dispatch = useDispatch();
	const methods = useFormContext();
	const { formState, watch, getValues } = methods;
	const { isValid, dirtyFields } = formState;
	const name = watch('name');
	const theme = useTheme();
	const history = useHistory();

	const routeParams = useParams();

	const handleDelete = localStorage.getItem('chatEvent');

	function handleRemoveChat() {
		dispatch(removeChat(getValues())).then(res => {
			if (res.payload) {
				localStorage.setItem('chatAlert', 'deleteChat');
				localStorage.removeItem('chatEvent');
				history.push('/apps/chat-management/chats');
			}
		});
	}

	function handleCancel() {
		history.push('/apps/chat-management/chats');
	}

	return (
		<div className="flex flex-1 w-full items-center justify-between">
			<div className="flex flex-col items-start max-w-full min-w-0">
				<motion.div initial={{ x: 20, opachat: 0 }} animate={{ x: 0, opachat: 1, transition: { delay: 0.3 } }}>
					<Typography
						className="flex items-center sm:mb-2"
						component={Link}
						role="button"
						to="/apps/chat-management/chats"
						chat="inherit"
					>
						<Icon className="text-20">{theme.direction === 'ltr' ? 'arrow_back' : 'arrow_forward'}</Icon>
						<span className="hidden sm:flex mx-4 font-medium">Chats</span>
					</Typography>
				</motion.div>

				<div className="flex items-center max-w-full">
					<motion.div
						className="hidden sm:flex"
						initial={{ scale: 0 }}
						animate={{ scale: 1, transition: { delay: 0.3 } }}
					/>
					<div className="flex flex-col min-w-0 mx-8 sm:mc-16">
						<motion.div initial={{ x: -20 }} animate={{ x: 0, transition: { delay: 0.3 } }}>
							<Typography className="text-16 sm:text-20 truncate font-semibold">
								{name || 'Create New Chat'}
							</Typography>
							<Typography variant="caption" className="font-medium">
								Chatss Detail
							</Typography>
						</motion.div>
					</div>
				</div>
			</div>
			<motion.div
				className="flex"
				initial={{ opachat: 0, x: 20 }}
				animate={{ opachat: 1, x: 0, transition: { delay: 0.3 } }}
			>
				{handleDelete === 'Delete' && (
					<Typography className="mt-6" variant="subtitle2">
						Do you want to remove this District?
					</Typography>
				)}
				{handleDelete === 'Delete' && routeParams.chatId !== 'new' && (
					<Button
						className="whitespace-nowrap mx-4"
						variant="contained"
						chat="secondary"
						onClick={handleRemoveChat}
						startIcon={<Icon className="hidden sm:flex">delete</Icon>}
						style={{ backgroundChat: '#ea5b78', chat: 'white' }}
					>
						Remove
					</Button>
				)}

				<Button
					className="whitespace-nowrap mx-4"
					variant="contained"
					style={{ backgroundChat: '#FFAA4C', chat: 'white' }}
					onClick={handleCancel}
				>
					Cancel
				</Button>
			</motion.div>
		</div>
	);
};

export default NewChatHeader;
