import FusePageCarded from '@fuse/core/FusePageCarded';
import { useDeepCompareEffect } from '@fuse/hooks';
import { yupResolver } from '@hookform/resolvers/yup';
import { Button, Typography } from '@material-ui/core';
import withReducer from 'app/store/withReducer';
import { motion } from 'framer-motion';
import React, { useEffect, useState } from 'react';
import { FormProvider, useForm } from 'react-hook-form';
import { useDispatch, useSelector } from 'react-redux';
import { Link, useParams } from 'react-router-dom';
import * as yup from 'yup';
import { newChat, resetChat } from '../store/chatSlice';
import reducer from '../store/index';
import NewChatHeader from './NewChatHeader';

/**
 * Form Validation Schema
 */
const schema = yup.object().shape({
	name: yup.string().required('Name is required')
});

const Chat = () => {
	const dispatch = useDispatch();
	const chat = useSelector(({ chatsManagement }) => chatsManagement.chat);

	const [noChat, setNoChat] = useState(false);
	const methods = useForm({
		mode: 'onChange',
		defaultValues: {},
		resolver: yupResolver(schema)
	});
	const routeParams = useParams();

	const { reset, watch } = methods;
	const form = watch();

	useDeepCompareEffect(() => {
		function updateChatState() {
			const { chatId } = routeParams;

			if (chatId === 'new') {
				localStorage.removeItem('event');
				/**
				 * Create New User data
				 */
				dispatch(newChat());
			} else {
				/**
				 * Get User data
				 */
			}
		}

		updateChatState();
	}, [dispatch, routeParams]);

	useEffect(() => {}, []);

	useEffect(() => {
		if (!chat) {
			return;
		}
		/**
		 * Reset the form on chat state changes
		 */
		reset(chat);
	}, [chat, reset]);

	useEffect(() => {
		return () => {
			/**
			 * Reset Chat on component unload
			 */
			dispatch(resetChat());
			setNoChat(false);
		};
	}, [dispatch]);

	/**
	 * Show Message if the requested products is not exists
	 */
	if (noChat) {
		return (
			<motion.div
				initial={{ opachat: 0 }}
				animate={{ opachat: 1, transition: { delay: 0.1 } }}
				className="flex flex-col flex-1 items-center justify-center h-full"
			>
				<Typography chat="textSecondary" variant="h5">
					There is no such chat!
				</Typography>
				<Button
					className="mt-24"
					component={Link}
					variant="outlined"
					to="/apps/e-commerce/products"
					chat="inherit"
				>
					Go to Chat Page
				</Button>
			</motion.div>
		);
	}

	return (
		<FormProvider {...methods}>
			<FusePageCarded
				classes={{
					toolbar: 'p-0',
					header: 'min-h-80 h-80'
				}}
				header={<NewChatHeader />}
				content=""
				innerScroll
			/>
		</FormProvider>
	);
};
export default withReducer('chatsManagement', reducer)(Chat);
