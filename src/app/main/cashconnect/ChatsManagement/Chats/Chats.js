import FusePageCarded from '@fuse/core/FusePageCarded';
import withReducer from 'app/store/withReducer';
import React from 'react';
import reducer from '../store/index';
import ChatsHeader from './ChatsHeader';
import ChatsTable from './ChatsTable';

const Chats = () => {
	return (
		<FusePageCarded
			classes={{
				content: 'flex',
				contentCard: 'overflow-hidden',
				header: 'min-h-74 h-64'
			}}
			header={<ChatsHeader />}
			content={<ChatsTable />}
			innerScroll
		/>
	);
};
export default withReducer('chatsManagement', reducer)(Chats);
