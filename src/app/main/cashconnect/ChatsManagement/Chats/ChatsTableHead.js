import Checkbox from '@material-ui/core/Checkbox';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import TableSortLabel from '@material-ui/core/TableSortLabel';
import Tooltip from '@material-ui/core/Tooltip';
import React, { useState } from 'react';
import { useDispatch } from 'react-redux';

const ChatsTableHead = props => {
	const admin = localStorage.getItem('user_role');
	const rows = [
		{
			id: 'sl_no',
			align: 'left',
			disablePadding: true,
			label: 'SL_NO',
			sort: true
		},
		{
			id: 'sender',
			align: 'left',
			disablePadding: false,
			label: 'Sender',
			sort: true
		},
		{
			id: 'receiver',
			align: 'left',
			disablePadding: false,
			label: 'Receiver',
			sort: true
		},

		{
			id: 'action',
			align: 'center',
			disablePadding: false,
			label: `${admin === 'ADMIN' ? 'User Assign' : admin === 'user' ? 'Chat Start' : ''}`,
			sort: true
		},
		{
			id: 'action',
			align: 'center',
			disablePadding: false,
			label: 'Action',
			sort: true
		}
	];
	const { selectedChatIds } = props;
	const numSelected = selectedChatIds.length;
	const [selectedProductsMenu, setSelectedProductsMenu] = useState(null);
	const dispatch = useDispatch();
	const createSortHandler = property => event => {
		props.onRequestSort(event, property);
	};

	function openSelectedProductsMenu(event) {
		setSelectedProductsMenu(event.currentTarget);
	}

	function closeSelectedProductsMenu() {
		setSelectedProductsMenu(null);
	}

	return (
		<TableHead>
			<TableRow className="h-48 sm:h-64">
				<TableCell padding="none" className="w-40 md:w-64 text-center z-99">
					<Checkbox
						indeterminate={numSelected > 0 && numSelected < props.rowCount}
						checked={props.rowCount !== 0 && numSelected === props.rowCount}
						onChange={props.onSelectAllClick}
					/>
				</TableCell>
				{rows.map(row => {
					return (
						<TableCell
							className="p-4 md:p-16 whitespace-nowrap"
							key={row.id}
							align={row.align}
							padding={row.disablePadding ? 'none' : 'default'}
						>
							{row.sort && (
								<Tooltip
									title={row?.label}
									placement={row.align === 'right' ? 'bottom-end' : 'bottom-start'}
									enterDelay={300}
								>
									<TableSortLabel
										active={props.order.id === row.id}
										direction={props.order.direction}
										onClick={createSortHandler(row.id)}
										className="font-semibold"
									>
										{row.label}
									</TableSortLabel>
								</Tooltip>
							)}
						</TableCell>
					);
				}, this)}
			</TableRow>
		</TableHead>
	);
};

export default ChatsTableHead;
