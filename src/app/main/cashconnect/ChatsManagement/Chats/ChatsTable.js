import FuseLoading from '@fuse/core/FuseLoading';
import FuseScrollbars from '@fuse/core/FuseScrollbars';
import _ from '@lodash';
import { Typography } from '@material-ui/core';
import Checkbox from '@material-ui/core/Checkbox';
import { makeStyles } from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TablePagination from '@material-ui/core/TablePagination';
import TableRow from '@material-ui/core/TableRow';
import DeleteIcon from '@material-ui/icons/Delete';
import PersonAddIcon from '@mui/icons-material/PersonAdd';
import Pagination from '@material-ui/lab/Pagination';
import { rowsPerPageOptions } from 'app/@data/data';
import { motion } from 'framer-motion';
import React, { useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import DoneOutlineIcon from '@material-ui/icons/DoneOutline';
import { withRouter } from 'react-router-dom';
import Box from '@mui/material/Box';
import Button from '@mui/material/Button';
import Modal from '@mui/material/Modal';
import { Controller, FormProvider, useForm, useFormContext } from 'react-hook-form';
import TextField from '@material-ui/core/TextField';
import AdjustIcon from '@mui/icons-material/Adjust';
import Tooltip from '@mui/material/Tooltip';
import StartIcon from '@mui/icons-material/Start';
import { Autocomplete } from '@mui/material';
import { getEmployeesThroughChat } from 'app/store/dataSlice';
import axios from 'axios';
import { CHAT_ASSIGIN_TO_USER } from 'app/constant/constants';
import useUserInfo from 'app/@customHooks/useUserInfo';
import Chat from 'app/fuse-layouts/layout1/Chat';
import GroupAddIcon from '@mui/icons-material/GroupAdd';
import ChatsTableHead from './ChatsTableHead';
import { getChats, selectChats } from '../store/chatsSlice';

const useStyles = makeStyles(() => ({
	root: {
		display: 'flex',
		justifyContent: 'space-between',
		flexWrap: 'nowrap',
		overflow: 'auto',
		minHeight: '35px'
	},
	toolbar: {
		'& > div': {
			minHeight: 'fit-content'
		}
	}
}));

const style = {
	position: 'absolute',
	top: '50%',
	left: '50%',
	transform: 'translate(-50%, -50%)',
	width: 400,
	bgcolor: 'background.paper',
	border: '2px solid #000',
	boxShadow: 24,
	p: 4
};

const ChatsTable = props => {
	const dispatch = useDispatch();
	const chats = useSelector(selectChats);
	const searchText = useSelector(({ chatsManagement }) => chatsManagement.chats.searchText);
	const [searchChat, setSearchChat] = useState([]);
	const [loading, setLoading] = useState(true);
	const [selected, setSelected] = useState([]);
	const [page, setPage] = useState(0);
	const [senderId, setSenderId] = useState();
	const [rowsPerPage, setRowsPerPage] = useState(30);
	const [order, setOrder] = useState({
		direction: 'asc',
		id: null
	});
	const [chatId, setChatId] = useState();
	const { authToken, userRole } = useUserInfo();
	let serialNumber = 1;
	//for Modal
	const [open, setOpen] = React.useState(false);
	const handleOpen = e => {
		setChatId(e.id);
		setOpen(true);
	};
	const handleClose = () => setOpen(false);
	const methods = useForm({
		mode: 'onChange',
		defaultValues: {}
	});
	const { control, formState, getValues } = methods || {};

	const employees = useSelector(state => state.data.employeesChat);
	const [parameter, setParameter] = useState({ page: 1, chat: 30 });
	const totalPages = sessionStorage.getItem('chats_total_pages');
	const totalElements = sessionStorage.getItem('chats_total_elements');
	const classes = useStyles();

	useEffect(() => {
		dispatch(getChats(parameter)).then(() => setLoading(false));
		dispatch(getEmployeesThroughChat());
	}, [dispatch]);

	function handleRequestSort(chatEvent, property) {
		const id = property;
		let direction = 'desc';

		if (order.id === property && order.direction === 'desc') {
			direction = 'asc';
		}

		setOrder({
			direction,
			id
		});
	}
	function handleAssignChat() {
		const data = getValues();
		data.chat = chatId;
		axios.post(`${CHAT_ASSIGIN_TO_USER}`, data, authToken).then(res => {
			dispatch(getChats(parameter));
			handleClose();
		});
	}
	function handleChat(e) {
		localStorage.setItem('senderId', e.sender.id);
		setSenderId(e.sender.id);
	}

	function handleSelectAllClick(chatEvent) {
		if (chatEvent.target.checked) {
			setSelected(chats.map(n => n.id));
			return;
		}
		setSelected([]);
	}

	function handleDeselect() {
		setSelected([]);
	}

	function handleUpdateChat(item) {
		localStorage.removeItem('chatEvent');
		props.history.push(`/apps/chat-management/chats/${item.id}/${item?.name}`);
	}
	function handleDeleteChat(item, chatEvent) {
		localStorage.removeItem('chatEvent');
		localStorage.setItem('chatEvent', chatEvent);
		props.history.push(`/apps/chat-management/chats/${item.id}/${item?.name}`);
	}

	function handleCheck(chatEvent, id) {
		const selectedIndex = selected.indexOf(id);
		let newSelected = [];

		if (selectedIndex === -1) {
			newSelected = newSelected.concat(selected, id);
		} else if (selectedIndex === 0) {
			newSelected = newSelected.concat(selected.slice(1));
		} else if (selectedIndex === selected.length - 1) {
			newSelected = newSelected.concat(selected.slice(0, -1));
		} else if (selectedIndex > 0) {
			newSelected = newSelected.concat(selected.slice(0, selectedIndex), selected.slice(selectedIndex + 1));
		}

		setSelected(newSelected);
	}

	const handlePagination = (e, handlePage) => {
		setParameter({ ...parameter, page: handlePage });
		setPage(handlePage - 1);
		dispatch(getChats({ ...parameter, page: handlePage }));
	};

	function handleChangePage(chatEvent, value) {
		setPage(value);
		setParameter({ ...parameter, page: value + 1 });
		dispatch(getChats({ ...parameter, page: value - 1 }));
	}

	function handleChangeRowsPerPage(chatEvent) {
		setRowsPerPage(chatEvent.target.value);
		setParameter({ ...parameter, chat: chatEvent.target.value });
		dispatch(getChats({ ...parameter, chat: chatEvent.target.value }));
	}

	if (loading) {
		return <FuseLoading />;
	}

	if (chats?.length === 0) {
		return (
			<motion.div
				initial={{ opachat: 0 }}
				animate={{ opachat: 1, transition: { delay: 0.1 } }}
				className="flex flex-1 items-center justify-center h-full"
			>
				<Typography chat="textSecondary" variant="h5">
					There are no chats!
				</Typography>
			</motion.div>
		);
	}

	return (
		<>
			<div>
				<Modal
					open={open}
					onClose={handleClose}
					aria-labelledby="modal-modal-title"
					aria-describedby="modal-modal-description"
				>
					<Box sx={style}>
						<FormProvider {...methods}>
							<Controller
								name="user"
								control={control}
								render={({ field: { onChange, value, name } }) => (
									<Autocomplete
										className="mt-8 mb-16"
										freeSolo
										value={value ? employees.find(data => data.id === value) : null}
										options={employees}
										getOptionLabel={option => `${option?.first_name} ${option?.last_name}`}
										onChange={(event, newValue) => {
											onChange(newValue?.id);
										}}
										renderInput={params => (
											<TextField
												{...params}
												placeholder="Select User"
												label="Assign User"
												required
												variant="outlined"
												autoFocus
												InputLabelProps={{
													shrink: true
												}}
											/>
										)}
									/>
								)}
							/>
						</FormProvider>
						<Button variant="contained" color="secondary" onClick={handleAssignChat}>
							Assign User
						</Button>
					</Box>
				</Modal>
			</div>
			<div className="w-full flex flex-col">
				<FuseScrollbars className="flex-grow overflow-x-auto">
					<Table stickyHeader className="min-w-xl" aria-labelledby="tableTitle">
						<ChatsTableHead
							selectedChatIds={selected}
							order={order}
							onSelectAllClick={handleSelectAllClick}
							onRequestSort={handleRequestSort}
							rowCount={chats.length}
							onMenuItemClick={handleDeselect}
						/>

						<TableBody>
							{_.orderBy(
								searchText !== '' && !_.isEmpty(searchChat) ? searchChat : chats,
								[
									o => {
										switch (order.id) {
											case 'categories': {
												return o.categories[0];
											}
											default: {
												return o[order.id];
											}
										}
									}
								],
								[order.direction]
							).map(n => {
								const isSelected = selected.indexOf(n.id) !== -1;
								return (
									<TableRow
										className="h-52 cursor-pointer"
										hover
										role="checkbox"
										aria-checked={isSelected}
										tabIndex={-1}
										key={n.id}
										selected={isSelected}
									>
										<TableCell className="w-40 md:w-64 text-center" padding="none">
											<Checkbox
												checked={isSelected}
												onClick={chatEvent => chatEvent.stopPropagation()}
												onChange={chatEvent => handleCheck(chatEvent, n.id)}
											/>
										</TableCell>

										<TableCell className="w-40 md:w-64" component="th" scope="row">
											{parameter.page * parameter.chat - parameter.chat + serialNumber++}
										</TableCell>

										<TableCell className="p-4 md:p-16" component="th" scope="row">
											{n?.sender?.first_name} {n?.sender?.last_name}
										</TableCell>
										<TableCell className="p-4 md:p-16" component="th" scope="row">
											{n?.receiver?.first_name} {n?.receiver?.last_name}
										</TableCell>
										{userRole === 'ADMIN' ? (
											<TableCell
												className="p-4 md:p-16"
												align="center"
												component="th"
												scope="row"
											>
												<div style={{ display: 'flex', justifyContent: 'space-around' }}>
													{n.active == true && (
														<Tooltip title="User Assigned" placement="top">
															<DoneOutlineIcon
																className="cursor-pointer"
																style={{
																	color: 'green',
																	fontSize: '30px',
																	display: n.receiver ? 'block' : 'none'
																}}
															/>
														</Tooltip>
													)}

													<Tooltip title="Assing a User" placement="top">
														<PersonAddIcon
															onClick={event => handleOpen(n)}
															className="cursor-pointer"
															style={{ color: '#ff6a00', fontSize: '30px' }}
														/>
													</Tooltip>
													<Tooltip
														title={`${
															n.active == true ? `Sender is Online` : 'Sender is Ofline'
														}`}
														placement="top"
													>
														<AdjustIcon
															className="cursor-pointer"
															style={{
																color: n.active == true ? 'green' : 'red',
																fontSize: '30px'
															}}
														/>
													</Tooltip>
												</div>
											</TableCell>
										) : (
											<TableCell
												className="p-4 md:p-16"
												align="center"
												component="th"
												scope="row"
											>
												{n?.sender?.id == senderId ? (
													<div style={{ display: 'flex', justifyContent: 'space-around' }}>
														<Tooltip title="Chat is running" placement="top">
															<StartIcon
																className="cursor-pointer"
																style={{ color: 'green', fontSize: '30px' }}
															/>
														</Tooltip>
														<Tooltip
															title={`${
																n.active == true
																	? `Sender is Online`
																	: 'Sender is Ofline'
															}`}
															placement="top"
														>
															<AdjustIcon
																className="cursor-pointer"
																style={{
																	color: n.active == true ? 'green' : 'red',
																	fontSize: '30px'
																}}
															/>
														</Tooltip>
													</div>
												) : (
													<div style={{ display: 'flex', justifyContent: 'space-around' }}>
														<Tooltip title="Please start chat" placement="top">
															<GroupAddIcon
																onClick={event => handleChat(n)}
																className="cursor-pointer"
																style={{ color: 'red', fontSize: '30px' }}
															/>
														</Tooltip>
														<Tooltip
															title={`${
																n.active == true
																	? `Sender is Online`
																	: 'Sender is Ofline'
															}`}
															placement="top"
														>
															<AdjustIcon
																className="cursor-pointer"
																style={{
																	color: n.active == true ? 'green' : 'red',
																	fontSize: '30px'
																}}
															/>
														</Tooltip>
													</div>
												)}
											</TableCell>
										)}

										<TableCell className="p-4 md:p-16" align="center" component="th" scope="row">
											<div>
												<DeleteIcon
													onClick={event => handleDeleteChat(n, 'Delete')}
													className="cursor-pointer"
													style={{ color: 'red' }}
												/>
											</div>
										</TableCell>
									</TableRow>
								);
							})}
						</TableBody>
						{/* <Chat senderId={senderId} /> */}
					</Table>
				</FuseScrollbars>

				<div className={classes.root}>
					<Pagination
						classes={{ ul: 'flex-nowrap' }}
						count={totalPages}
						defaultPage={1}
						chat="primary"
						showFirstButton
						showLastButton
						variant="outlined"
						shape="rounded"
						onChange={handlePagination}
					/>

					<TablePagination
						classes={{ root: 'overflow-visible' }}
						rowsPerPageOptions={rowsPerPageOptions}
						component="div"
						count={totalElements}
						rowsPerPage={rowsPerPage}
						page={page}
						className={classes.toolbar}
						backIconButtonProps={{
							'aria-label': 'Previous Page',
							className: 'py-0'
						}}
						nextIconButtonProps={{
							'aria-label': 'Next Page',
							className: 'py-0'
						}}
						onChangePage={handleChangePage}
						onChangeRowsPerPage={handleChangeRowsPerPage}
					/>
				</div>
			</div>
		</>
	);
};

export default withRouter(ChatsTable);
