import { React, useEffect, useState } from 'react';
import { useFormContext, Controller } from 'react-hook-form';
import TextField from '@material-ui/core/TextField';
import { makeStyles } from '@material-ui/core/styles';
import { useParams } from 'react-router-dom';
import { useDispatch, useSelector } from 'react-redux';
import { Autocomplete } from '@material-ui/lab';
import { Grid, InputAdornment, Typography } from '@material-ui/core';
import { format } from 'date-fns';
import { getProducts } from 'app/store/dataSlice';
import CustomDatePicker from 'app/@components/CustomDatePicker';
import { discountValue } from 'app/@data/data';

const useStyles = makeStyles(theme => ({
	hidden: {
		display: 'none'
	}
}));

function DiscountForm(props) {
	const products = useSelector(state => state.data.products);
	const [unitPrice, setUnitPrice] = useState('');
	const userID = localStorage.getItem('UserID');
	const classes = useStyles(props);
	const methods = useFormContext();
	const { control, formState, getValues, watch, setValue } = methods;
	const { errors } = formState;
	const routeParams = useParams();
	const { discountId } = routeParams;
	const discount_type = watch('discount_type');
	const dispatch = useDispatch();
	const d = new Date();
	useEffect(() => {
		dispatch(getProducts());
	}, []);

	// useEffect(() => {
	// 	debugger;
	// 	if (discount_type === 'percentage') {
	// 		setDiscountType(true);
	// 	}
	// 	// else {
	// 	// 	setDiscountType(false);
	// 	// }
	// }, [discount_type]);

	return (
		<div>
			<Controller
				name="product"
				control={control}
				render={({ field: { onChange, value, name } }) => (
					<Autocomplete
						className="mt-8 mb-16"
						freeSolo
						value={value ? products.find(data => data.id === value) : null}
						options={products}
						getOptionLabel={option => `${option?.name}`}
						onChange={(event, newValue) => {
							onChange(newValue?.id);
							setUnitPrice(newValue?.unit_price);
						}}
						renderInput={params => (
							<TextField
								{...params}
								placeholder="Select a Product"
								label="Product"
								error={!!errors.product}
								required
								helperText={errors?.product?.message}
								variant="outlined"
								autoFocus
								InputLabelProps={{
									shrink: true
								}}
							/>
						)}
					/>
				)}
			/>
			{unitPrice && (
				<Typography className="mb-4">
					Unit Price: <span className="text-green-800 font-bold"> {unitPrice}</span>{' '}
				</Typography>
			)}
			<Grid container spacing={2}>
				<Grid item xs={4}>
					<Controller
						name="discount_type"
						control={control}
						render={({ field: { onChange, value, name } }) => (
							<Autocomplete
								className="mt-8 mb-16"
								freeSolo
								value={value ? discountValue.find(data => data.id === value) : null}
								options={discountValue}
								getOptionLabel={option => `${option?.name}`}
								onChange={(event, newValue) => {
									onChange(newValue?.id);
									// if (newValue?.id == 'percentage') {
									// 	setDiscountType(true);
									// } else {
									// 	setDiscountType(false);
									// }
								}}
								renderInput={params => (
									<TextField
										{...params}
										placeholder="Select a discount type"
										label="Discount type"
										error={!!errors.discount_type}
										required
										helperText={errors?.discount_type?.message}
										variant="outlined"
										autoFocus
										InputLabelProps={{
											shrink: true
										}}
									/>
								)}
							/>
						)}
					/>
				</Grid>
				<Grid item xs={8}>
					{discount_type === 'percentage' && (
						<Controller
							name="discount_percent"
							control={control}
							render={({ field }) => (
								<TextField
									{...field}
									className="mt-8 mb-16"
									label="Discount Percent"
									id="discount_percent"
									error={!!errors.discount_percent}
									required
									helperText={errors?.discount_percent?.message}
									InputProps={{
										startAdornment: <InputAdornment position="start" />
									}}
									type="number"
									variant="outlined"
									autoFocus
									fullWidth
								/>
							)}
						/>
					)}
					{discount_type === 'value' && (
						<Controller
							name="discount_value"
							control={control}
							render={({ field }) => (
								<TextField
									{...field}
									className="mt-8 mb-16"
									label="Discount Value"
									id="discount_value"
									error={!!errors.discount_value}
									required
									helperText={errors?.discount_value?.message}
									InputProps={{
										startAdornment: <InputAdornment position="start" />
									}}
									variant="outlined"
									autoFocus
									fullWidth
								/>
							)}
						/>
					)}
				</Grid>
				<Grid item xs={6}>
					<Controller
						name="start_date"
						control={control}
						render={({ field }) => <CustomDatePicker field={field} label="Start Date" />}
					/>
				</Grid>
				<Grid item xs={6}>
					<Controller
						name="end_date"
						control={control}
						render={({ field }) => <CustomDatePicker field={field} label="End Date" />}
					/>
				</Grid>
			</Grid>

			{/* <Controller
				name="start_date"
				control={control}
				render={({ field }) => (
					<TextField
						{...field}
						className="mt-8 mb-16"
						id="date"
						label="Start Date"
						error={!!errors.start_date}
						required
						helperText={errors?.start_date?.message}
						type="date"
						//defaultValue={format(new Date(d), "mm/dd/yyyy")}
						fullWidth
						InputLabelProps={{
							shrink: true
						}}
					/>
				)}
			/> */}

			{/* <Controller
				name="end_date"
				control={control}
				render={({ field }) => (
					<TextField
						{...field}
						className="mt-8 mb-16"
						id="date"
						label="End Date"
						error={!!errors.end_date}
						required
						helperText={errors?.end_date?.message}
						type="date"
						//defaultValue="2017-05-24"
						fullWidth
						InputLabelProps={{
							shrink: true
						}}
					/>
				)}
			/> */}
		</div>
	);
}

export default DiscountForm;
