import FusePageCarded from '@fuse/core/FusePageCarded';
import withReducer from 'app/store/withReducer';
import { FormProvider, useForm } from 'react-hook-form';
import { yupResolver } from '@hookform/resolvers/yup';
import * as yup from 'yup';
import { useDeepCompareEffect } from '@fuse/hooks';
import { useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { useParams } from 'react-router-dom';
import _ from '@lodash';
import { format } from 'date-fns';
import { getUserPermissions } from 'app/store/dataSlice';
import {
	DISCOUNT_CREATE,
	DISCOUNT_DELETE,
	DISCOUNT_DETAILS,
	DISCOUNT_UPDATE
} from 'app/constant/permission/permission';
import reducer from '../store/index';
import DiscountForm from './DiscountForm';
import NewDiscountHeader from './NewDiscountHeader';
import { newDiscount, resetDiscount, getDiscount } from '../store/discountSlice';
import PagenotFound from '../../Pagenotfound/PagenotFound';

/**
 * Form Validation Schema
 */
const schema = yup.object().shape({
	// discount_percent: yup.number().required('Discount percent is required'),
	product: yup.number().required('product is required')
	// start_date: yup.date().required('start date is required')
	// end_date: yup.date()
	//     .when("start_date",
	//         (start_date, yupp) => start_date && yupp.min(start_date, "End time cannot be before start time"))
});

const Discount = () => {
	const dispatch = useDispatch();
	const discount = useSelector(({ discountsManagement }) => discountsManagement.discount);
	const [noDiscount, setNoDiscount] = useState(false);
	const methods = useForm({
		mode: 'onChange',
		defaultValues: {},
		resolver: yupResolver(schema)
	});
	const routeParams = useParams();
	const { reset, watch, control, onChange, formState } = methods;
	const form = watch();
	const UserPermissions = useSelector(state => state.data.UserPermissions);
	useEffect(() => {
		dispatch(getUserPermissions());
	}, []);
	useDeepCompareEffect(() => {
		function updateDiscountState() {
			const { discountId } = routeParams;

			if (discountId === 'new') {
				localStorage.removeItem('event');
				/**
				 * Create New User data
				 */
				dispatch(newDiscount());
			} else {
				/**
				 * Get User data
				 */

				dispatch(getDiscount(discountId)).then(action => {
					/**
					 * If the requested product is not exist show message
					 */
					if (!action.payload) {
						setNoDiscount(true);
					}
				});
			}
		}

		updateDiscountState();
	}, [dispatch, routeParams]);

	useEffect(() => {
		if (!discount) {
			return;
		}
		/**
		 * Reset the form on discount state changes
		 */
		reset({ ...discount, product: discount?.product?.id });
	}, [discount, reset]);

	useEffect(() => {
		return () => {
			/**
			 * Reset Discount on component unload
			 */
			dispatch(resetDiscount());
			setNoDiscount(false);
		};
	}, [dispatch]);

	return (
		<FormProvider {...methods}>
			{UserPermissions.includes(DISCOUNT_CREATE) ||
			UserPermissions.includes(DISCOUNT_UPDATE) ||
			UserPermissions.includes(DISCOUNT_DELETE) ||
			UserPermissions.includes(DISCOUNT_DETAILS) ? (
				<FusePageCarded
					classes={{
						content: 'flex',
						contentCard: 'overflow-hidden',
						header: 'min-h-52 h-52 sm:h-136 sm:min-h-136'
					}}
					header={<NewDiscountHeader />}
					content={
						<div className="p-16 sm:p-24" style={{ width: '100%' }}>
							<DiscountForm />
						</div>
					}
					innerScroll
				/>
			) : (
				<PagenotFound />
			)}
		</FormProvider>
	);
};
export default withReducer('discountsManagement', reducer)(Discount);
