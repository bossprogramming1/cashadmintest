import { combineReducers } from '@reduxjs/toolkit';
import discount from './discountSlice';
import discounts from './discountsSlice';


const reducer = combineReducers({
    discount,
    discounts
});

export default reducer;