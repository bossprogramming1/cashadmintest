import { createSlice, createAsyncThunk } from '@reduxjs/toolkit';
import axios from 'axios';
import { addNotification } from 'app/fuse-layouts/shared-components/notificationPanel/store/dataSlice';
import NotificationModel from 'app/fuse-layouts/shared-components/notificationPanel/model/NotificationModel';
import { CREATE_DISCOUNT, GET_DISCOUNTID, UPDATE_DISCOUNT, DELETE_DISCOUNT } from '../../../../constant/constants';

export const getDiscount = createAsyncThunk(
	'discountManagement/discount/getDiscount',
	async (params, { rejectWithValue }) => {
		const authTOKEN = {
			headers: {
				'Content-type': 'application/json',
				Authorization: localStorage.getItem('jwt_access_token')
			}
		};

		try {
			const response = await axios.get(`${GET_DISCOUNTID}${params}`, authTOKEN);
			const data = await response.data;
			return data === undefined ? null : data;
		} catch (err) {
			return rejectWithValue(params);
		}
	}
);

export const removeDiscount = createAsyncThunk(
	'discountManagement/discount/removeDiscount',
	async (val, { dispatch, getState }) => {
		const authTOKEN = {
			headers: {
				'Content-type': 'application/json',
				Authorization: localStorage.getItem('jwt_access_token')
			}
		};

		const discountId = val.id;
		const response = await axios.delete(`${DELETE_DISCOUNT}${discountId}`, authTOKEN);
		return response;
	}
);

export const updateDiscount = createAsyncThunk(
	'discountManagement/discount/updateDiscount',
	async (discountData, { dispatch, getState }) => {
		const { discount } = getState().discountsManagement;

		const authTOKEN = {
			headers: {
				'Content-type': 'application/json',
				Authorization: localStorage.getItem('jwt_access_token')
			}
		};
		const response = await axios.put(`${UPDATE_DISCOUNT}${discount.id}`, discountData, authTOKEN);
		return response;
	}
);

export const saveDiscount = createAsyncThunk(
	'discountManagement/discount/saveDiscount',
	async (discountData, { dispatch, getState }) => {
		try {
			const authTOKEN = {
				headers: {
					'Content-type': 'application/json',
					Authorization: localStorage.getItem('jwt_access_token')
				}
			};

			const response = await axios.post(`${CREATE_DISCOUNT}`, discountData, authTOKEN);
			return response;
		} catch (error) {
			console.error('Error saving discount:', error.response);

			dispatch(
				addNotification(
					NotificationModel({
						message: `${error.response.data.product ? 'Already added this product' : ''}`,
						options: { variant: 'error' },
						item_id: discountData?.product
					})
				)
			);

			throw error;
		}
	}
);

const discountSlice = createSlice({
	name: 'discountManagement/discount',
	initialState: null,
	reducers: {
		resetDiscount: () => null,
		newDiscount: {
			reducer: (state, action) => action.payload,
			prepare: event => ({
				payload: {
					discount_percent: '',
					discount_type: 'value',
					start_date: '',
					end_date: '',
					product: 0
				}
			})
		}
	},
	extraReducers: {
		[getDiscount.fulfilled]: (state, action) => action.payload,
		[saveDiscount.fulfilled]: (state, action) => {
			localStorage.setItem('discountAlert', 'saveDiscount');
			return action.payload;
		},
		[removeDiscount.fulfilled]: () => {
			localStorage.setItem('discountAlert', 'deleteDiscount');
			return null;
		},
		[updateDiscount.fulfilled]: () => {
			localStorage.setItem('discountAlert', 'updateDiscount');
		}
	}
});

export const { newDiscount, resetDiscount } = discountSlice.actions;
export default discountSlice.reducer;
