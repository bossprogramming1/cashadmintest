import { createAsyncThunk, createEntityAdapter, createSlice } from '@reduxjs/toolkit';
import { DELETE_DISCOUNT, GET_DISCOUNTS } from 'app/constant/constants';
import axios from 'axios';

export const getDiscounts = createAsyncThunk('discountManagement/discounts/geDiscounts', async () => {
	const authTOKEN = {
		headers: {
			'Content-type': 'application/json',
			Authorization: localStorage.getItem('jwt_access_token')
		}
	};

	const response = axios.get(GET_DISCOUNTS, authTOKEN);
	const data = await response;
	return data.data._discounts;
});

export const removeDiscounts = createAsyncThunk(
	'eCommerceApp/products/removeDiscounts',
	async (discountIds, { dispatch, getState }) => {
		const authTOKEN = {
			headers: {
				'Content-type': 'application/json',
				Authorization: localStorage.getItem('jwt_access_token')
			}
		};
		await axios.delete(`${DELETE_DISCOUNT}`, { discountIds }, authTOKEN);

		return discountIds;
	}
);

const discountsAdapter = createEntityAdapter({});

export const { selectAll: selectDiscounts, selectById: selectDiscountById } = discountsAdapter.getSelectors(
	state => state.discountsManagement.discounts
);

const discountsSlice = createSlice({
	name: 'discountManagement/discounts',
	initialState: discountsAdapter.getInitialState({
		searchText: ''
	}),
	reducers: {
		setDiscountsSearchText: {
			reducer: (state, action) => {
				state.searchText = action.payload;
			},
			prepare: event => ({ payload: event.target.value || '' })
		}
	},
	extraReducers: {
		[getDiscounts.fulfilled]: discountsAdapter.setAll
	}
});

export const { setData, setDiscountsSearchText } = discountsSlice.actions;
export default discountsSlice.reducer;
