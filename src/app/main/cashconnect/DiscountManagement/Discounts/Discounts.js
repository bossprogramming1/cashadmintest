import React, { useEffect } from 'react';
import FusePageCarded from '@fuse/core/FusePageCarded';
import withReducer from 'app/store/withReducer';
import { useDispatch, useSelector } from 'react-redux';
import { getUserPermissions } from 'app/store/dataSlice';
import { DISCOUNT_TYPE_LIST } from 'app/constant/permission/permission';
import reducer from '../store/index';
import DiscountsHeader from './DiscountsHeader';
import DiscountsTable from './DiscountsTable';
import PagenotFound from '../../Pagenotfound/PagenotFound';

const Discounts = () => {
	const UserPermissions = useSelector(state => state.data.UserPermissions);
	useEffect(() => {
		dispatch(getUserPermissions());
	}, []);
	const dispatch = useDispatch();
	return (
		<FusePageCarded
			classes={{
				content: 'flex',
				contentCard: 'overflow-hidden',
				header: 'min-h-52 h-52 sm:h-136 sm:min-h-136'
			}}
			header={UserPermissions.includes(DISCOUNT_TYPE_LIST) && <DiscountsHeader />}
			content={UserPermissions.includes(DISCOUNT_TYPE_LIST) ? <DiscountsTable /> : <PagenotFound />}
			innerScroll
		/>
	);
};
export default withReducer('discountsManagement', reducer)(Discounts);
