import FuseScrollbars from '@fuse/core/FuseScrollbars';
import _ from '@lodash';
import Checkbox from '@material-ui/core/Checkbox';
import DeleteIcon from '@material-ui/icons/Delete';
import EditIcon from '@material-ui/icons/Edit';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TablePagination from '@material-ui/core/TablePagination';
import TableRow from '@material-ui/core/TableRow';
import { format } from 'date-fns';
import { withRouter } from 'react-router-dom';
import { useEffect, useState } from 'react';
import FuseLoading from '@fuse/core/FuseLoading';
import { useDispatch, useSelector } from 'react-redux';
import { motion } from 'framer-motion';
import { SEARCH_DISCOUNT } from 'app/constant/constants';
import { Tooltip, Typography } from '@material-ui/core';
import DiscountsTableHead from './DiscountsTableHead';
import { getDiscounts, selectDiscounts } from '../store/discountsSlice';

const DiscountsTable = props => {
	const dispatch = useDispatch();
	const discounts = useSelector(selectDiscounts);
	const searchText = useSelector(({ discountsManagement }) => discountsManagement.discounts.searchText);
	const [loading, setLoading] = useState(true);
	const [selected, setSelected] = useState([]);
	// const [data, setData] = useState(discounts);
	const [searchDiscount, setSearchDiscount] = useState([]);
	const user_role = localStorage.getItem('user_role');

	const [page, setPage] = useState(0);
	const [rowsPerPage, setRowsPerPage] = useState(10);
	const [order, setOrder] = useState({
		direction: 'asc',
		id: null
	});
	let serialNumber = 1;
	const [deleteItem, setDeleteItem] = useState('');
	useEffect(() => {
		dispatch(getDiscounts()).then(() => setLoading(false));
	}, [dispatch]);

	// useEffect(() => {
	// 	if (searchText.length !== 0) {
	// 		setData(_.filter(discounts, item => item?.name.toLowerCase().includes(searchText.toLowerCase())));
	// 		setPage(0);
	// 	} else {
	// 		setData(discounts);
	// 	}
	// }, [discounts, searchText]);

	//searchVendor
	useEffect(() => {
		searchText !== '' && getSearchVendor();
	}, [searchText]);

	const getSearchVendor = () => {
		fetch(`${SEARCH_DISCOUNT}?keyword=${searchText}`, {
			headers: {
				'Content-type': 'application/json',
				Authorization: localStorage.getItem('jwt_access_token')
			}
		})
			.then(response => response.json())
			.then(searchedDiscountData => {
				setSearchDiscount(searchedDiscountData.discounts);
			});
	};

	function handleRequestSort(discountEvent, property) {
		const id = property;
		let direction = 'desc';

		if (order.id === property && order.direction === 'desc') {
			direction = 'asc';
		}

		setOrder({
			direction,
			id
		});
	}

	function handleSelectAllClick(discountEvent) {
		if (discountEvent.target.checked) {
			setSelected(discounts.map(n => n.id));
			return;
		}
		setSelected([]);
	}

	function handleDeselect() {
		setSelected([]);
	}

	function handleUpdateDiscount(item) {
		localStorage.removeItem('discountEvent');
		props.history.push(`/apps/discount-management/discounts/${item.id}/${item?.name}`);
	}
	function handleDeleteDiscount(item, discountEvent) {
		localStorage.removeItem('discountEvent');
		localStorage.setItem('discountEvent', discountEvent);
		props.history.push(`/apps/discount-management/discounts/${item.id}/${item?.name}`);
	}

	function handleCheck(discountEvent, id) {
		const selectedIndex = selected.indexOf(id);
		let newSelected = [];

		if (selectedIndex === -1) {
			newSelected = newSelected.concat(selected, id);
		} else if (selectedIndex === 0) {
			newSelected = newSelected.concat(selected.slice(1));
		} else if (selectedIndex === selected.length - 1) {
			newSelected = newSelected.concat(selected.slice(0, -1));
		} else if (selectedIndex > 0) {
			newSelected = newSelected.concat(selected.slice(0, selectedIndex), selected.slice(selectedIndex + 1));
		}

		setSelected(newSelected);
	}

	function handleChangePage(discountEvent, value) {
		setPage(value);
	}

	function handleChangeRowsPerPage(discountEvent) {
		setRowsPerPage(discountEvent.target.value);
	}

	if (loading) {
		return <FuseLoading />;
	}

	if (discounts.length === 0) {
		return (
			<motion.div
				initial={{ opacity: 0 }}
				animate={{ opacity: 1, transition: { delay: 0.1 } }}
				className="flex flex-1 items-center justify-center h-full"
			>
				<Typography color="textSecondary" variant="h5">
					There are no discounted products!
				</Typography>
			</motion.div>
		);
	}

	return (
		<div className="w-full flex flex-col">
			<FuseScrollbars className="flex-grow overflow-x-auto">
				<Table stickyHeader className="min-w-xl" aria-labelledby="tableTitle">
					<DiscountsTableHead
						selectedDiscountIds={selected}
						order={order}
						onSelectAllClick={handleSelectAllClick}
						onRequestSort={handleRequestSort}
						rowCount={discounts.length}
						onMenuItemClick={handleDeselect}
					/>

					<TableBody>
						{_.orderBy(
							searchText !== '' && searchDiscount ? searchDiscount : discounts,
							[
								o => {
									switch (order.id) {
										case 'categories': {
											return o.categories[0];
										}
										default: {
											return o[order.id];
										}
									}
								}
							],
							[order.direction]
						)
							.slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
							.map(n => {
								const isSelected = selected.indexOf(n.id) !== -1;
								return (
									<TableRow
										hover
										role="checkbox"
										aria-checked={isSelected}
										tabIndex={-1}
										key={n.id}
										selected={isSelected}
									>
										<TableCell
											whitespace-nowrap
											className="whitespace-nowrap p-4 md:p-16"
											component="th"
											scope="row"
											padding="none"
										>
											<Checkbox
												checked={isSelected}
												onClick={discountEvent => discountEvent.stopPropagation()}
												onChange={discountEvent => handleCheck(discountEvent, n.id)}
											/>
										</TableCell>

										<TableCell
											whitespace-nowrap
											className="whitespace-nowrap p-4 md:p-16"
											component="th"
											scope="row"
											padding="none"
										>
											{serialNumber++}
										</TableCell>
										<TableCell
											whitespace-nowrap
											className="whitespace-nowrap p-4 md:p-16"
											component="th"
											scope="row"
											padding="none"
										>
											{n?.start_date && format(new Date(n?.start_date), 'dd-MM-yyyy')}
										</TableCell>
										<TableCell
											whitespace-nowrap
											className="whitespace-nowrap p-4 md:p-16"
											component="th"
											scope="row"
											padding="none"
										>
											{n?.end_date && format(new Date(n?.end_date), 'dd-MM-yyyy')}
										</TableCell>

										<TableCell
											whitespace-nowrap
											className="whitespace-nowrap p-4 md:p-16"
											component="th"
											scope="row"
											padding="none"
										>
											{n.product?.name}
										</TableCell>
										<TableCell
											whitespace-nowrap
											className="whitespace-nowrap p-4 md:p-16"
											component="th"
											scope="row"
											padding="none"
										>
											{n?.discount_type}
										</TableCell>
										<TableCell
											whitespace-nowrap
											className="whitespace-nowrap p-4 md:p-16"
											component="th"
											scope="row"
											padding="none"
										>
											{n?.discount_type == 'value'
												? `৳ ${n.discount_value}`
												: `${n.discount_percent}%`}
										</TableCell>
										<TableCell
											whitespace-nowrap
											className="whitespace-nowrap p-4 md:p-16"
											component="th"
											scope="row"
											padding="none"
										>
											{n?.product?.unit_price}
										</TableCell>
										<TableCell
											whitespace-nowrap
											className="whitespace-nowrap p-4 md:p-16"
											component="th"
											scope="row"
											padding="none"
										>
											{n?.discounted_price
												? n?.discounted_price?.toFixed(2)
												: 'No discount available'}
										</TableCell>

										<TableCell
											align="center"
											whitespace-nowrap
											className="whitespace-nowrap p-4 md:p-16"
											component="th"
											scope="row"
											padding="none"
										>
											<div>
												<Tooltip title="Edit" placement="top" enterDelay={300}>
													<EditIcon
														onClick={discountEvent => handleUpdateDiscount(n)}
														className="h-52 cursor-pointer"
														style={{ color: 'green' }}
													/>
												</Tooltip>

												<Tooltip title="Delete" placement="top" enterDelay={300}>
													<DeleteIcon
														onClick={event => handleDeleteDiscount(n, 'Delete')}
														className="h-52 cursor-pointer"
														style={{
															color: 'red',
															visibility:
																user_role === 'ADMIN' || user_role === 'admin'
																	? 'visible'
																	: 'hidden'
														}}
													/>
												</Tooltip>
											</div>
										</TableCell>
									</TableRow>
								);
							})}
					</TableBody>
				</Table>
			</FuseScrollbars>

			<TablePagination
				className="flex-shrink-0 border-t-1"
				rowsPerPageOptions={[5, 10, 25]}
				component="div"
				count={discounts.length}
				rowsPerPage={rowsPerPage}
				page={page}
				backIconButtonProps={{
					'aria-label': 'Previous Page'
				}}
				nextIconButtonProps={{
					'aria-label': 'Next Page'
				}}
				onChangePage={handleChangePage}
				onChangeRowsPerPage={handleChangeRowsPerPage}
			/>
		</div>
	);
};

export default withRouter(DiscountsTable);
