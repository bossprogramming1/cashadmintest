import Checkbox from '@material-ui/core/Checkbox';
import Icon from '@material-ui/core/Icon';
import IconButton from '@material-ui/core/IconButton';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import Menu from '@material-ui/core/Menu';
import MenuItem from '@material-ui/core/MenuItem';
import MenuList from '@material-ui/core/MenuList';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import TableSortLabel from '@material-ui/core/TableSortLabel';
import Tooltip from '@material-ui/core/Tooltip';
import clsx from 'clsx';
import { useDispatch } from 'react-redux';
import { useState } from 'react';
import { removeDiscounts } from '../store/discountsSlice';

const rows = [
	{
		id: 'sl_no',
		align: 'left',
		disablePadding: false,
		label: 'SL_NO',
		sort: true
	},
	{
		id: 'start_date',
		align: 'left',
		disablePadding: false,
		label: 'Start Date',
		sort: true
	},
	{
		id: 'end_date',
		align: 'left',
		disablePadding: false,
		label: 'End Date',
		sort: true
	},
	{
		id: 'product_name',
		align: 'left',
		disablePadding: false,
		label: 'Product Name',
		sort: true
	},
	{
		id: 'discount_type',
		align: 'left',
		disablePadding: false,
		label: 'Discount Type',
		sort: true
	},
	{
		id: 'discount_percent',
		align: 'left',
		disablePadding: false,
		label: 'Discount Percent/Value',
		sort: true
	},
	{
		id: 'unit_price',
		align: 'left',
		disablePadding: false,
		label: 'Unit Price',
		sort: true
	},
	{
		id: 'discount_price',
		align: 'left',
		disablePadding: false,
		label: 'Discounted Price',
		sort: true
	},

	{
		id: 'action',
		align: 'center',
		disablePadding: false,
		label: 'Action',
		sort: true
	}
];

const DiscountsTableHead = props => {
	const { selectedDiscountIds } = props;

	const numSelected = selectedDiscountIds.length;

	const [selectedProductsMenu, setSelectedProductsMenu] = useState(null);

	const dispatch = useDispatch();

	const createSortHandler = property => event => {
		props.onRequestSort(event, property);
	};

	function openSelectedProductsMenu(event) {
		setSelectedProductsMenu(event.currentTarget);
	}

	function closeSelectedProductsMenu() {
		setSelectedProductsMenu(null);
	}

	return (
		<TableHead>
			<TableRow className="h-48 sm:h-64">
				<TableCell
					whitespace-nowrap
					style={{ whiteSpace: 'nowrap' }}
					padding="none"
					className="w-40 md:w-64 text-center z-99"
				>
					<Checkbox
						indeterminate={numSelected > 0 && numSelected < props.rowCount}
						checked={props.rowCount !== 0 && numSelected === props.rowCount}
						onChange={props.onSelectAllClick}
					/>

					<div
						className={clsx(
							'flex items-center justify-center absolute w-64 top-0 ltr:left-0 rtl:right-0 mx-56 h-64 z-10 border-b-1'
						)}
					>
						{/* <IconButton
                            aria-owns={selectedProductsMenu ? 'selectedProductsMenu' : null}
                            aria-haspopup="true"
                            onClick={openSelectedProductsMenu}
                        >
                            <Icon>more_horiz</Icon>
                        </IconButton> */}
						{/* <Menu
                            id="selectedProductsMenu"
                            anchorEl={selectedProductsMenu}
                            open={Boolean(selectedProductsMenu)}
                            onClose={closeSelectedProductsMenu}
                        >
                            <MenuList>
                                <MenuItem
                                onClick={() => {
                                	dispatch(removeOrders(selectedOrderIds));
                                	props.onMenuItemClick();
                                	closeSelectedProductsMenu();
                                }}
                                >
                                    <ListItemIcon className="min-w-40">
                                        <Icon>delete</Icon>
                                    </ListItemIcon>
                                    <ListItemText primary="Remove" />
                                </MenuItem>
                            </MenuList>
                        </Menu> */}
					</div>
				</TableCell>
				{rows.map(row => {
					return (
						<TableCell
							style={{ whiteSpace: 'nowrap' }}
							className="p-0 md:p-0"
							key={row.id}
							align={row.align}
							padding={row.disablePadding ? 'none' : 'default'}
							//sortDirection={props.order.id === row.id ? props.order.direction : false}
						>
							{row.sort && (
								<Tooltip
									title={row?.label}
									placement={row.align === 'right' ? 'bottom-end' : 'bottom-start'}
									enterDelay={300}
								>
									<TableSortLabel
										active={props.order.id === row.id}
										direction={props.order.direction}
										onClick={createSortHandler(row.id)}
										className="font-semibold"
									>
										{row.label}
									</TableSortLabel>
								</Tooltip>
							)}
						</TableCell>
					);
				}, this)}
			</TableRow>
		</TableHead>
	);
};

export default DiscountsTableHead;
