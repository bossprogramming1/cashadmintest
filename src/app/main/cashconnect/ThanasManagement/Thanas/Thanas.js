import FusePageCarded from '@fuse/core/FusePageCarded';
import { THANA_LIST } from 'app/constant/permission/permission';
import { getUserPermissions } from 'app/store/dataSlice';
import withReducer from 'app/store/withReducer';
import React, { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import PagenotFound from '../../Pagenotfound/PagenotFound';
import reducer from '../store/index';
import ThanasHeader from './ThanasHeader';
import ThanasTable from './ThanasTable';

const Thanas = () => {
	const UserPermissions = useSelector(state => state.data.UserPermissions);
	useEffect(() => {
		dispatch(getUserPermissions());
	}, []);
	const dispatch = useDispatch();
	return (
		<FusePageCarded
			classes={{
				content: 'flex',
				contentCard: 'overflow-hidden',
				header: 'min-h-74 h-64'
			}}
			header={UserPermissions.includes(THANA_LIST) && <ThanasHeader />}
			content={UserPermissions.includes(THANA_LIST) ? <ThanasTable /> : <PagenotFound />}
			innerScroll
		/>
	);
};
export default withReducer('thanasManagement', reducer)(Thanas);
