import FuseScrollbars from '@fuse/core/FuseScrollbars';
import FuseUtils from '@fuse/utils';
import AppBar from '@material-ui/core/AppBar';
import Avatar from '@material-ui/core/Avatar';
import Icon from '@material-ui/core/Icon';
import IconButton from '@material-ui/core/IconButton';
import Input from '@material-ui/core/Input';
import List from '@material-ui/core/List';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import Menu from '@material-ui/core/Menu';
import MenuItem from '@material-ui/core/MenuItem';
import Paper from '@material-ui/core/Paper';
import { useTheme } from '@material-ui/core/styles';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import useMediaQuery from '@material-ui/core/useMediaQuery';
import { motion } from 'framer-motion';
import { useEffect, useMemo, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import axios from 'axios';
import { GET_All_ASSIGNED_CONTACT, WS_URL } from 'app/constant/constants';
import ContactListItem from './ContactListItem';
import StatusIcon from './StatusIcon';
import { getChat } from './store/chatSlice';
import { selectContacts } from './store/contactsSlice';
import { openUserSidebar } from './store/sidebarsSlice';
import { updateUserData } from './store/userSlice';

const statusArr = [
	{
		title: 'Online',
		value: 'online'
	},
	{
		title: 'Away',
		value: 'away'
	},
	{
		title: 'Do not disturb',
		value: 'do-not-disturb'
	},
	{
		title: 'Offline',
		value: 'offline'
	}
];

const authTOKEN = localStorage.getItem('jwt_access_token');
const token = authTOKEN?.slice(7);

function ChatsSidebar(props) {
	const dispatch = useDispatch();
	const contacts = useSelector(selectContacts);
	const chat = useSelector(({ chatApp }) => chatApp?.chat);

	const user = useSelector(({ chatApp }) => chatApp.user);
	const theme = useTheme();
	const isMobile = useMediaQuery(theme.breakpoints.down('md'));
	const selectedChatId = useSelector(({ chatApp }) => chatApp.contacts.selectedChatId);

	const [searchText, setSearchText] = useState('');
	const [statusMenuEl, setStatusMenuEl] = useState(null);
	const [moreMenuEl, setMoreMenuEl] = useState(null);
	const [assignContact, setAssignContact] = useState([]);

	function handleMoreMenuClick(event) {
		setMoreMenuEl(event.currentTarget);
	}

	function handleMoreMenuClose(event) {
		setMoreMenuEl(null);
	}

	function handleStatusMenuClick(event) {
		event.preventDefault();
		event.stopPropagation();
		setStatusMenuEl(event.currentTarget);
	}

	function handleStatusSelect(event, status) {
		event.preventDefault();
		event.stopPropagation();
		dispatch(
			updateUserData({
				...user,
				status
			})
		);
		setStatusMenuEl(null);
	}

	function handleStatusClose(event) {
		event.preventDefault();
		event.stopPropagation();
		setStatusMenuEl(null);
	}

	function handleSearchText(event) {
		setSearchText(event.target.value);
	}

	console.log('1478965', chat);
	const fetchData = async () => {
		try {
			// const response = await axios.get(GET_All_ASSIGNED_CONTACT, {
			// 	headers: {
			// 		'Content-type': 'application/json',
			// 		Authorization: localStorage.getItem('jwt_access_token')
			// 	}
			// });

			// const data = response?.data.chats; // Assuming 'chats' is the data you want to access
			// console.log('dataForContactss', data);
			const websocket = new WebSocket(`${WS_URL}/ws/chat_list/?token=${token}`);

			websocket.onopen = () => {
				// WebSocket connection is established
				console.log('WebSocket connection opened.');

				// Send the message once the connection is open
				websocket.send(JSON.stringify({ action: 'chat_list' }));
			};

			websocket.onmessage = event => {
				// Handle incoming WebSocket messages
				const assignClient = JSON.parse(event.data);
				console.log('for_caht_lsit', assignClient);
				if (assignClient.action == 'chat_list') {
					setAssignContact(assignClient?.chat_list);
				}
				// dispatch(updateUserChatList(message.text));

				console.log('message14778', event);
			};

			websocket.onclose = event => {
				console.log('WebSocket connection closed.', event);
			};
		} catch (error) {
			// Handle any errors that may occur during the request
			console.error('Error fetching data:', error);
			setAssignContact([]);
		}
	};
	useEffect(() => {
		fetchData();
	}, []);
	// useEffect(() => {
	// 	fetchData();
	// }, [selectedChatId]);

	const senderData = contactId => {
		let websocket = new WebSocket(`${WS_URL}/ws/chat/?token=${token}&conn_to=${contactId}`);

		websocket.onopen = () => {
			// WebSocket connection is established
			console.log('WebSocket connection opened.');
		};

		websocket.onmessage = event => {
			// Handle incoming WebSocket messages
			const message = JSON.parse(event.data);
			console.log('message', message);
			// You can dispatch a Redux action here based on the incoming message.
		};

		websocket.onclose = event => {
			// WebSocket connection is closed
			console.log('WebSocket connection closed.', event);
		};
	};

	return (
		<div className="flex flex-col flex-auto h-full">
			<AppBar position="static" color="default" elevation={0}>
				<Toolbar className="flex justify-between items-center px-4">
					{user && (
						<div
							className="relative w-40 h-40 p-0 mx-12 cursor-pointer"
							onClick={() => dispatch(openUserSidebar())}
							onKeyDown={() => dispatch(openUserSidebar())}
							role="button"
							tabIndex={0}
						>
							<Avatar src={user.avatar} alt={user.name} className="w-40 h-40">
								{!user.avatar || user.avatar === '' ? user.name[0] : ''}
							</Avatar>
							<div
								className="absolute right-0 bottom-0 -m-4 z-10 cursor-pointer"
								aria-owns={statusMenuEl ? 'switch-menu' : null}
								aria-haspopup="true"
								onClick={handleStatusMenuClick}
								onKeyDown={handleStatusMenuClick}
								role="button"
								tabIndex={0}
							>
								<StatusIcon status={user.status} />
							</div>

							<Menu
								id="status-switch"
								anchorEl={statusMenuEl}
								open={Boolean(statusMenuEl)}
								onClose={handleStatusClose}
							>
								{statusArr.map(status => (
									<MenuItem onClick={ev => handleStatusSelect(ev, status.value)} key={status.value}>
										<ListItemIcon className="min-w-40">
											<StatusIcon status={status.value} />
										</ListItemIcon>
										<ListItemText primary={status.title} />
									</MenuItem>
								))}
							</Menu>
						</div>
					)}

					<div>
						<IconButton
							aria-owns={moreMenuEl ? 'chats-more-menu' : null}
							aria-haspopup="true"
							onClick={handleMoreMenuClick}
						>
							<Icon>more_vert</Icon>
						</IconButton>
						<Menu
							id="chats-more-menu"
							anchorEl={moreMenuEl}
							open={Boolean(moreMenuEl)}
							onClose={handleMoreMenuClose}
						>
							<MenuItem onClick={handleMoreMenuClose}>Profile</MenuItem>
							<MenuItem onClick={handleMoreMenuClose}>Logout</MenuItem>
						</Menu>
					</div>
				</Toolbar>
				{useMemo(
					() => (
						<Toolbar className="px-16">
							<Paper className="flex p-4 items-center w-full px-8 py-4 shadow">
								<Icon color="action">search</Icon>

								<Input
									placeholder="Search or start new chat"
									className="flex flex-1 px-8"
									disableUnderline
									fullWidth
									value={searchText}
									inputProps={{
										'aria-label': 'Search'
									}}
									onChange={handleSearchText}
								/>
							</Paper>
						</Toolbar>
					),
					[searchText]
				)}
			</AppBar>

			<FuseScrollbars className="overflow-y-auto flex-1">
				<List className="w-full">
					{useMemo(() => {
						function getFilteredArray(arr, _searchText) {
							if (_searchText.length === 0) {
								return arr;
							}
							return FuseUtils.filterArrayByString(arr, _searchText);
						}

						const chatListContacts =
							contacts.length > 0 && user && user.chatList
								? user.chatList.map(_chat => ({
										..._chat,
										...contacts.find(_contact => _contact.id === _chat.contactId)
								  }))
								: [];

						const filteredContacts = getFilteredArray([...assignContact], searchText);
						const filteredChatList = getFilteredArray([...chatListContacts], searchText);
						console.log('filteredContacts', filteredContacts);

						const container = {
							show: {
								transition: {
									staggerChildren: 0.1
								}
							}
						};

						const item = {
							hidden: { opacity: 0, y: 20 },
							show: { opacity: 1, y: 0 }
						};

						return (
							<motion.div
								className="flex flex-col flex-shrink-0"
								variants={container}
								initial="hidden"
								animate="show"
							>
								{/* {filteredChatList.length > 0 && (
									<motion.div variants={item}>
										<Typography className="font-medium text-20 px-16 py-24" color="secondary">
											Chats
										</Typography>
									</motion.div>
								)}

								{filteredChatList.map(contact => (
									<motion.div variants={item} key={contact.id}>
										<ContactListItem
											contact={contact}
											onContactClick={contactId => dispatch(getChat({ contactId, isMobile }))}
										/>
									</motion.div>
								))} */}

								{filteredContacts.length > 0 && (
									<motion.div variants={item}>
										<Typography className="font-medium text-20 px-16 py-24" color="secondary">
											Contacts
										</Typography>
									</motion.div>
								)}

								{filteredContacts.map(contact => (
									<motion.div variants={item} key={contact.id}>
										<ContactListItem
											contact={contact}
											onContactClick={contactId =>
												dispatch(
													getChat({
														contactId: contactId?.contact?.id,
														chatId: contactId?.contact?.chat_id,
														isMobile
													})
												)
											}
										/>
									</motion.div>
								))}
							</motion.div>
						);
					}, [contacts, user, searchText, dispatch, isMobile, assignContact])}
				</List>
			</FuseScrollbars>
		</div>
	);
}

export default ChatsSidebar;
