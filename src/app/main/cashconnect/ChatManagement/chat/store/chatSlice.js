import { createSlice, createAsyncThunk } from '@reduxjs/toolkit';
import axios from 'axios';
import { WS_URL } from 'app/constant/constants';
import { setSelectedChatId, setSelectedContactId } from './contactsSlice';
import { closeMobileChatsSidebar } from './sidebarsSlice';
import { updateUserChatList } from './userSlice';

const authTOKEN = localStorage.getItem('jwt_access_token');
const token = authTOKEN?.slice(7);
let websocket;

// export const getChat = createAsyncThunk(
// 	'chatApp/chat/getChat',
// 	async ({ contactId, isMobile }, { dispatch, getState }) => {
// 		// const { id: userId } = getState().chatApp.user;

// 		// const response = await axios.get('/api/chat/get-chat', {
// 		// 	params: {
// 		// 		contactId,
// 		// 		userId
// 		// 	}
// 		// });
// 		// const { chat, userChatList } = await response.data;

// 		// console.log('userChatList', userChatList);

// 		// dispatch(setSelectedContactId(contactId));
// 		// dispatch(updateUserChatList(userChatList));

// 		// if (isMobile) {
// 		// 	dispatch(closeMobileChatsSidebar());
// 		// }

// 		// return chat;
// 		let websocket = new WebSocket(`${WS_URL}/ws/chat/?token=${token}&conn_to=${contactId}`);

// 		websocket.onopen = () => {
// 			// WebSocket connection is established
// 			console.log('WebSocket connection opened.');
// 		};

// 		websocket.onmessage = event => {
// 			// Handle incoming WebSocket messages
// 			const message = JSON.parse(event.data);

// 			console.log('message', message);
// 			dispatch(setSelectedContactId(contactId));
// 			dispatch(updateUserChatList(message.messages));

// 			// You can dispatch a Redux action here based on the incoming message.

// 			return message.messages;
// 		};

// 		websocket.onclose = event => {
// 			// WebSocket connection is closed
// 			console.log('WebSocket connection closed.', event);
// 		};
// 	}
// );

export const getChat = createAsyncThunk(
	'chatApp/chat/getChat',
	async ({ contactId, isMobile, chatId }, { dispatch }) => {
		const websocket = new WebSocket(`${WS_URL}/ws/chat/?token=${token}&conn_to=${contactId}`);

		// Define a promise to resolve when you receive the data you want
		let resolvePromise;
		console.log('chatId', chatId);
		websocket.onopen = () => {
			// WebSocket connection is established
			console.log('WebSocket connection opened.');
			if (chatId) {
				websocket.send(
					JSON.stringify({
						type: 'view_message',
						chat_id: chatId,
						action: 'read_message'
					})
				);
			}
		};

		websocket.onmessage = event => {
			// Handle incoming WebSocket messages
			const message = JSON.parse(event.data);

			console.log('message', message);

			// Dispatch actions to update your Redux state
			dispatch(setSelectedContactId(contactId));
			dispatch(setSelectedChatId(chatId ? chatId : null));
			dispatch(updateUserChatList(message.text));

			// You can dispatch a Redux action here based on the incoming message.

			// Resolve the promise when you've received the data you want
			if (resolvePromise) {
				resolvePromise(message.text);
			}
		};

		websocket.onclose = event => {
			// WebSocket connection is closed
			console.log('WebSocket connection closed.', event);

			// Handle WebSocket connection closure, if needed
		};

		// Return a promise that resolves with the data you want
		return new Promise(resolve => {
			resolvePromise = resolve;
		});
	}
);
// export const getChat = createAsyncThunk('chatApp/chat/getChat', async ({ contactId, isMobile }, { dispatch }) => {
// 	if (!websocket || websocket.readyState !== WebSocket.OPEN) {
// 		// Create a new WebSocket connection if it doesn't exist or is not open
// 		websocket = new WebSocket(`${WS_URL}/ws/chat/?token=${token}&conn_to=${contactId}`);

// 		websocket.onopen = () => {
// 			// WebSocket connection is established
// 			console.log('WebSocket connection opened.');
// 		};

// 		websocket.onclose = event => {
// 			// WebSocket connection is closed
// 			console.log('WebSocket connection closed.', event);
// 			// Handle WebSocket connection closure, if needed
// 		};
// 	}

// 	// Define a promise to resolve when you receive the data you want
// 	let resolvePromise;

// 	websocket.onmessage = event => {
// 		// Handle incoming WebSocket messages
// 		const message = JSON.parse(event.data);

// 		console.log('message', message);

// 		// Dispatch actions to update your Redux state
// 		dispatch(setSelectedContactId(contactId));
// 		dispatch(updateUserChatList(message.text));

// 		// You can dispatch a Redux action here based on the incoming message.

// 		// Resolve the promise when you've received the data you want
// 		if (resolvePromise) {
// 			resolvePromise(message.text);
// 		}
// 	};

// 	// Return a promise that resolves with the data you want
// 	return new Promise(resolve => {
// 		resolvePromise = resolve;
// 	});
// });
export const sendMessage = createAsyncThunk(
	'chatApp/chat/sendMessage',
	async ({ messageText, chatId, contactId }, { dispatch, getState }) => {
		// const response = await axios.post('/api/chat/send-message', { chatId, messageText, contactId });

		// const { message, userChatList } = await response.data;

		// dispatch(updateUserChatList(userChatList));

		// return message;
		const websocket = new WebSocket(`${WS_URL}/ws/chat/?token=${token}&conn_to=${contactId}`);

		// Define a promise to resolve when you receive the data you want
		let resolvePromise;

		websocket.onopen = () => {
			// WebSocket connection is established
			console.log('WebSocket connection opened.');
		};
		debugger;
		websocket.send(JSON.stringify({ message: messageText }));

		websocket.onmessage = event => {
			debugger;
			// Handle incoming WebSocket messages
			const message = JSON.parse(event.data);

			console.log('message14778', event);

			// Dispatch actions to update your Redux state
			dispatch(updateUserChatList(message.messages));

			// You can dispatch a Redux action here based on the incoming message.
		};

		websocket.onclose = event => {
			// WebSocket connection is closed
			console.log('WebSocket connection closed.', event);

			// Handle WebSocket connection closure, if needed
		};
		// Resolve the promise when you've received the data you want
		if (resolvePromise) {
			resolvePromise(messageText);
		}

		// Return a promise that resolves with the data you want
		return new Promise(resolve => {
			resolvePromise = resolve;
		});
	}
);

const chatSlice = createSlice({
	name: 'chatApp/chat',
	initialState: null,
	reducers: {
		removeChat: (state, action) => action.payload
	},
	extraReducers: {
		[getChat.fulfilled]: (state, action) => action.payload,
		[sendMessage.fulfilled]: (state, action) => {
			state.dialog = [...state.dialog, action.payload];
		}
	}
});

export default chatSlice.reducer;
