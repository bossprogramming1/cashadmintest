import { createEntityAdapter, createSlice, createAsyncThunk } from '@reduxjs/toolkit';
import { GET_All_ASSIGNED_CONTACT } from 'app/constant/constants';

import axios from 'axios';

export const getContacts = createAsyncThunk('chatApp/contacts/getContacts', async params => {
	const response = await axios.get(GET_All_ASSIGNED_CONTACT, {
		params,
		headers: {
			'Content-type': 'application/json',
			Authorization: localStorage.getItem('jwt_access_token')
		}
	});

	const data = await response.data.chats;
	console.log('dataForContact', data);

	return data;
});

const contactsAdapter = createEntityAdapter({});

export const { selectAll: selectContacts, selectById: selectContactById } = contactsAdapter.getSelectors(
	state => state.chatApp.contacts
);

const contactsSlice = createSlice({
	name: 'chatApp/contacts',
	initialState: contactsAdapter.getInitialState({
		selectedContactId: null,
		selectedChatId: null
	}),
	reducers: {
		setSelectedContactId: (state, action) => {
			state.selectedContactId = action.payload;
		},
		setSelectedChatId: (state, action) => {
			state.selectedChatId = action.payload;
		},
		removeSelectedContactId: (state, action) => {
			state.selectedContactId = null;
		}
	},
	extraReducers: {
		[getContacts.fulfilled]: contactsAdapter.setAll
	}
});

export const { setSelectedContactId, setSelectedChatId, removeSelectedContactId } = contactsSlice.actions;

export default contactsSlice.reducer;
