import FuseScrollbars from '@fuse/core/FuseScrollbars';
import Avatar from '@material-ui/core/Avatar';
import Icon from '@material-ui/core/Icon';
import IconButton from '@material-ui/core/IconButton';
import Paper from '@material-ui/core/Paper';
import { makeStyles } from '@material-ui/core/styles';
import TextField from '@material-ui/core/TextField';
import Typography from '@material-ui/core/Typography';
import clsx from 'clsx';
import formatDistanceToNow from 'date-fns/formatDistanceToNow';
import { useEffect, useRef, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { BASE_URL, WS_URL } from 'app/constant/constants';
import useUserInfo from 'app/@customHooks/useUserInfo';
import { selectContacts } from './store/contactsSlice';
import { getChat, sendMessage } from './store/chatSlice';
import { updateUserChatList } from './store/userSlice';

const useStyles = makeStyles(theme => ({
	messageRow: {
		'&.contact': {
			'& .bubble': {
				backgroundColor: '#ffffff',
				color: theme.palette.getContrastText(theme.palette.background.paper),
				borderTopLeftRadius: 5,
				borderBottomLeftRadius: 5,
				borderTopRightRadius: 6,
				borderBottomRightRadius: 6,
				'& .time': {
					marginLeft: 12
				}
			},
			'&.first-of-group': {
				'& .bubble': {
					borderTopLeftRadius: 0
				}
			},
			'&.last-of-group': {
				'& .bubble': {
					borderBottomLeftRadius: 6
				}
			}
		},
		'&.me': {
			paddingLeft: 40,

			'& .avatar': {
				order: 2,
				margin: '0 0 0 16px'
			},
			'& .bubble': {
				marginLeft: 'auto',
				// backgroundColor: theme.palette.primary.main,
				backgroundColor: '#d9fdd3',
				color: theme.palette.getContrastText(theme.palette.background.paper),
				borderTopLeftRadius: 6,
				borderBottomLeftRadius: 6,
				borderTopRightRadius: 5,
				borderBottomRightRadius: 5,
				'& .time': {
					justifyContent: 'flex-end',
					right: 0,
					marginRight: 12
				}
			},
			'&.first-of-group': {
				'& .bubble': {
					borderTopRightRadius: 0
				}
			},

			'&.last-of-group': {
				'& .bubble': {
					borderBottomRightRadius: 6
				}
			}
		},
		'&.contact + .me, &.me + .contact': {
			paddingTop: 20,
			marginTop: 20
		},
		'&.first-of-group': {
			'& .bubble': {
				borderTopLeftRadius: 0,
				paddingTop: 13,
				'& .svg': {
					display: 'flex'
				}
			}
		},
		'&.last-of-group': {
			'& .bubble': {
				borderBottomLeftRadius: 6,
				paddingBottom: 13,
				'& .time': {
					display: 'flex'
				}
			}
		}
	}
}));

const authTOKEN = localStorage.getItem('jwt_access_token');
const token = authTOKEN?.slice(7);

function Chat(props) {
	const dispatch = useDispatch();
	const contacts = useSelector(selectContacts);
	const selectedContactId = useSelector(({ chatApp }) => chatApp.contacts.selectedContactId);
	const selectedChatId = useSelector(({ chatApp }) => chatApp.contacts.selectedChatId);
	const chat = useSelector(({ chatApp }) => chatApp.chat);
	const { userId } = useUserInfo();
	const user = parseInt(userId);
	const classes = useStyles(props);
	const chatRef = useRef(null);
	const [messageText, setMessageText] = useState('');

	// Define a WebSocket ref
	const websocketRef = useRef(null);
	const messageQueueRef = useRef([]);
	const [isWebSocketOpen, setIsWebSocketOpen] = useState(false);

	useEffect(() => {
		if (chat) {
			scrollToBottom();
		}
	}, [chat]);

	console.log('sdfasdfsdfsdfs', contacts, chat);

	function scrollToBottom() {
		chatRef.current.scrollTop = chatRef.current.scrollHeight;
	}

	function shouldShowContactAvatar(item, i) {
		return (
			item.user.id === selectedContactId &&
			((chat[i - 1] && chat[i - 1].user.id !== selectedContactId) || !chat[i - 1])
		);
	}

	function isFirstMessageOfGroup(item, i) {
		return i === 0 || (chat[i - 1] && chat[i - 1].user.id !== item.user.id);
	}

	function isLastMessageOfGroup(item, i) {
		return i === chat.length - 1 || (chat[i + 1] && chat[i + 1].user.id !== item.user.id);
	}

	function onInputChange(ev) {
		setMessageText(ev.target.value);
	}

	function onMessageSubmit(ev) {
		ev.preventDefault();
		if (messageText === '') {
			return;
		}
		const websocket = new WebSocket(`${WS_URL}/ws/chat/?token=${token}&conn_to=${selectedContactId}`);

		websocket.onopen = () => {
			// WebSocket connection is established
			console.log('WebSocket connection opened.');

			// Send the message once the connection is open
			websocket.send(JSON.stringify({ text: messageText, action: 'messages' }));
			setMessageText('');
		};

		websocket.onmessage = event => {
			// Handle incoming WebSocket messages
			const message = JSON.parse(event.data);
			console.log('messageWhen send', message.text);
			// chatId: selectedChatId
			dispatch(getChat({ contactId: selectedContactId }));
			// dispatch(updateUserChatList(message.text));

			console.log('message14778', event);
		};

		websocket.onclose = event => {
			console.log('WebSocket connection closed.', event);
		};

		// dispatch(
		// 	sendMessage({
		// 		messageText,
		// 		chatId: props.chatId,
		// 		contactId: selectedContactId
		// 	})
		// ).then(() => {
		// 	setMessageText('');
		// });
	}
	// function onMessageSubmit(ev) {
	// 	ev.preventDefault();
	// 	if (messageText === '') {
	// 		return;
	// 	}

	// 	// If the WebSocket is not open, add the message to the queue
	// 	if (!isWebSocketOpen) {
	// 		messageQueueRef.current.push(messageText);
	// 	} else {
	// 		// Send the message if the WebSocket is open
	// 		websocketRef.current.send(JSON.stringify({ text: messageText, action: 'messages' }));
	// 		setMessageText('');
	// 	}
	// }

	// // Initialize the WebSocket connection
	// useEffect(() => {
	// 	if (!websocketRef.current) {
	// 		websocketRef.current = new WebSocket(`${WS_URL}/ws/chat/?token=${token}&conn_to=${selectedContactId}`);

	// 		websocketRef.current.onopen = () => {
	// 			setIsWebSocketOpen(true);
	// 			console.log('WebSocket connection opened.');

	// 			// Send any messages in the queue
	// 			while (messageQueueRef.current.length > 0) {
	// 				const message = messageQueueRef.current.shift();
	// 				websocketRef.current.send(JSON.stringify({ text: message, action: 'messages' }));
	// 			}
	// 		};

	// 		websocketRef.current.onmessage = event => {
	// 			// Handle incoming WebSocket messages
	// 			const message = JSON.parse(event.data);
	// 			console.log('messageWhen send', message.text);
	// 			// chatId: selectedChatId
	// 			dispatch(getChat({ contactId: selectedContactId }));
	// 			// dispatch(updateUserChatList(message.text));
	// 			console.log('message14778', event);
	// 		};

	// 		websocketRef.current.onclose = event => {
	// 			setIsWebSocketOpen(false);
	// 			console.log('WebSocket connection closed.', event);
	// 		};
	// 	}
	// }, [selectedContactId]);

	return (
		<div className={clsx('flex flex-col relative', props.className)}>
			<FuseScrollbars ref={chatRef} className="flex flex-1 flex-col overflow-y-auto">
				{chat && chat.length > 0 ? (
					<div className="flex flex-col pt-16 px-16 ltr:pl-56 rtl:pr-56 pb-40">
						{chat.map((item, i) => {
							const contact =
								item.user.id === user ? user : contacts.find(_contact => _contact.id === item.user.id);
							console.log('meeeee', user);
							return (
								<div
									key={item.time}
									className={clsx(
										classes.messageRow,
										'flex flex-col flex-grow-0 flex-shrink-0 items-start justify-end relative px-16 pb-4',
										{ me: item.user.id === user },
										{ contact: item.user.id !== user },
										{ 'first-of-group': isFirstMessageOfGroup(item, i) },
										{ 'last-of-group': isLastMessageOfGroup(item, i) },
										i + 1 === chat.length && 'pb-96'
									)}
								>
									{shouldShowContactAvatar(item, i) && (
										<Avatar
											className="avatar absolute ltr:left-0 rtl:right-0 m-0 -mx-32"
											src={`${BASE_URL}${contact.image}`}
										/>
									)}

									<div className="bubble flex relative items-center justify-center p-12 max-w-full shadow">
										{isFirstMessageOfGroup(item, i) && (
											<svg
												style={{
													marginLeft: item.user.id !== user && '-8px',
													marginRight: item.user.id === user && '-8px',
													color: item.user.id !== user ? '#ffffff' : '#d9fdd3'
												}}
												viewBox="0 0 8 13"
												height="13"
												width="8"
												preserveAspectRatio="xMidYMid meet"
												class=""
												version="1.1"
												x="0px"
												y="0px"
												enable-background="new 0 0 8 13"
												className={clsx(
													'absolute',
													{ 'ltr:right-0 rtl:left-0': item.user.id === user },
													{ 'ltr:left-0 rtl:right-0': item.user.id !== user },
													'top-0 m-0 '
												)}
											>
												<path
													opacity="0.13"
													fill="#0000000"
													d={
														item.user.id !== user
															? `M1.533,3.568L8,12.193V1H2.812 C1.042,1,0.474,2.156,1.533,3.568z`
															: `M5.188,1H0v11.193l6.467-8.625 C7.526,2.156,6.958,1,5.188,1z`
													}
												></path>
												<path
													fill="currentColor"
													d={
														item.user.id !== user
															? 'M1.533,2.568L8,11.193V0L2.812,0C1.042,0,0.474,1.156,1.533,2.568z'
															: 'M5.188,0H0v11.193l6.467-8.625C7.526,1.156,6.958,0,5.188,0z'
													}
												></path>
											</svg>
										)}
										<div className="leading-tight whitespace-pre-wrap">{item.text}</div>

										<Typography
											className="time absolute hidden w-full text-11 mt-8 -mb-24 ltr:left-0 rtl:right-0 bottom-0 whitespace-nowrap"
											color="textSecondary"
										>
											{formatDistanceToNow(new Date(item?.created_at), { addSuffix: true })}
										</Typography>
									</div>
								</div>
							);
						})}
					</div>
				) : (
					<div className="flex flex-col flex-1">
						<div className="flex flex-col flex-1 items-center justify-center">
							<Icon className="text-128" color="disabled">
								chat
							</Icon>
						</div>
						<Typography className="px-16 pb-24 text-center" color="textSecondary">
							Start a conversation by typing your message below.
						</Typography>
					</div>
				)}
			</FuseScrollbars>
			{chat && (
				<form onSubmit={onMessageSubmit} className="absolute bottom-0 right-0 left-0 py-16 px-8">
					<Paper className="flex items-center relative rounded-24 shadow">
						<TextField
							autoFocus={false}
							id="message-input"
							className="flex-1"
							InputProps={{
								disableUnderline: true,
								classes: {
									root: 'flex flex-grow flex-shrink-0 mx-16 ltr:mr-48 rtl:ml-48 my-8',
									input: ''
								},
								placeholder: 'Type your message'
							}}
							InputLabelProps={{
								shrink: false,
								className: classes.bootstrapFormLabel
							}}
							onChange={onInputChange}
							value={messageText}
						/>
						<IconButton className="absolute ltr:right-0 rtl:left-0 top-0" type="submit">
							<Icon className="text-24" color="action">
								send
							</Icon>
						</IconButton>
					</Paper>
				</form>
			)}
		</div>
	);
}

export default Chat;
