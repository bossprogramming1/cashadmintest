import { lazy } from 'react';

const cashconnectRouteConfig = {
	settings: {
		layout: {}
	},
	routes: [
		//employee
		{
			path: '/apps/employee-management/employees',
			component: lazy(() => import('./EmployeeManagement/Employees/Employees'))
		},
		{
			path: '/apps/employee-management/:employeeId',
			component: lazy(() => import('./EmployeeManagement/Employee/NewEmployee'))
		},
		{
			path: '/apps/employee-otp-management/employee/:employeeId/:employeeName?',
			component: lazy(() => import('./EmployeeManagement/Employees/SendOTP'))
		},
		{
			path: '/apps/employee-management/:employeeId/:employeeName?',
			component: lazy(() => import('./EmployeeManagement/Employee/NewEmployee'))
		},
		{
			path: '/apps/users-management/userslist',
			component: lazy(() => import('./EmployeeManagement/UsersList/UsersList'))
		},
		{
			path: '/apps/users-management/forgot-password/:userId',
			component: lazy(() => import('./EmployeeManagement/UsersList/ForgotPassword'))
		},

		//timetable
		{
			path: '/apps/timetables-management/:timetableId/:timetableName?',
			component: lazy(() => import('./PayRollManagement/ShiftTimetableManagement/ShiftTimetable/NewTimetable'))
		},
		{
			path: '/apps/timetables-management',
			component: lazy(() => import('./PayRollManagement/ShiftTimetableManagement/ShiftTimetables/Timetables'))
		},
		{
			path: '/apps/timetable-management/:timetableId',
			component: lazy(() => import('./PayRollManagement/ShiftTimetableManagement/ShiftTimetable/NewTimetable'))
		},
		//shift
		{
			path: '/apps/shifts-management/:shiftId/:shiftName?',
			component: lazy(() => import('./PayRollManagement/ShiftManagement/Shift/NewShift'))
		},
		{
			path: '/apps/shifts-management',
			component: lazy(() => import('./PayRollManagement/ShiftManagement/Shifts/Shifts'))
		},
		{
			path: '/apps/shift-management/:shiftId',
			component: lazy(() => import('./PayRollManagement/ShiftManagement/Shift/NewShift'))
		},
		//downloadSchedule
		{
			path: '/apps/import-schedule-management/',
			component: lazy(() =>
				import('./PayRollManagement/DownloadScheduleManagement/DownloadSchedules/DownloadSchedules')
			)
		},

		//UploadFileSchedule
		{
			path: '/apps/upload-employee-schedule-management/',
			component: lazy(() => import('./PayRollManagement/UploadFileManagement/UploadFiles/UploadFiles'))
		},

		//EmployeeScheduleManagement
		{
			path: '/apps/schedules-management/:scheduleId/:scheduleName?',
			component: lazy(() => import('./PayRollManagement/EmployeeScheduleManagement/Schedule/NewSchedule'))
		},
		{
			path: '/apps/schedules-management',
			component: lazy(() => import('./PayRollManagement/EmployeeScheduleManagement/Schedules/Schedules'))
		},
		{
			path: '/apps/schedule-management/:scheduleId',
			component: lazy(() => import('./PayRollManagement/EmployeeScheduleManagement/Schedule/NewSchedule'))
		},

		//device
		{
			path: '/apps/device-management/devices/:deviceId/:deviceName?',
			component: lazy(() => import('./PayRollManagement/DeviceManagement/Device/NewDevice'))
		},
		{
			path: '/apps/device-management/devices',
			component: lazy(() => import('./PayRollManagement/DeviceManagement/Devices/Devices'))
		},
		{
			path: '/apps/device-management/:deviceId',
			component: lazy(() => import('./PayRollManagement/DeviceManagement/Device/NewDevice'))
		},

		//unit
		{
			path: '/apps/unit-management/units/:unitId/:unitName?',
			component: lazy(() => import('./PayRollManagement/UnitManagement/Unit/NewUnit'))
		},
		{
			path: '/apps/unit-management/units',
			component: lazy(() => import('./PayRollManagement/UnitManagement/Units/Units'))
		},
		{
			path: '/apps/unit-management/:unitId',
			component: lazy(() => import('./PayRollManagement/UnitManagement/Unit/NewUnit'))
		},
		//userdefinevalue
		{
			path: '/apps/userdefinevalue-management/userdefinevalues/:userdefinevalueId/:userdefinevalueName?',
			component: lazy(() =>
				import('./PayRollManagement/UserDefineValueManagement/Userdefinevalue/NewUserdefinevalue')
			)
		},
		{
			path: '/apps/userdefinevalue-management/userdefinevalues',
			component: lazy(() =>
				import('./PayRollManagement/UserDefineValueManagement/Userdefinevalues/Userdefinevalues')
			)
		},
		{
			path: '/apps/userdefinevalue-management/:userdefinevalueId',
			component: lazy(() =>
				import('./PayRollManagement/UserDefineValueManagement/Userdefinevalue/NewUserdefinevalue')
			)
		},
		//ComputeInformationManagement
		{
			path: '/apps/information-management/informations/:informationId/:informationName?',
			component: lazy(() => import('./PayRollManagement/ComputeInformationManagement/Information/NewInformation'))
		},
		{
			path: '/apps/information-management/informations',
			component: lazy(() => import('./PayRollManagement/ComputeInformationManagement/Informations/Informations'))
		},
		{
			path: '/apps/information-management/:informationId',
			component: lazy(() => import('./PayRollManagement/ComputeInformationManagement/Information/NewInformation'))
		},

		//compute
		{
			path: '/apps/compute-management/computes/:computeId/:computeName?',
			component: lazy(() => import('./PayRollManagement/ComputeManagement/Compute/NewCompute'))
		},
		{
			path: '/apps/compute-management/computes',
			component: lazy(() => import('./PayRollManagement/ComputeManagement/Computes/Computes'))
		},
		{
			path: '/apps/compute-management/:computeId',
			component: lazy(() => import('./PayRollManagement/ComputeManagement/Compute/NewCompute'))
		},

		//pyahead
		{
			path: '/apps/pyahead-management/pyaheads/:pyaheadId/:pyaheadName?',
			component: lazy(() => import('./PayRollManagement/PyaheadManagement/Pyahead/NewPyahead'))
		},

		{
			path: '/apps/pyahead-management/pyaheads',
			component: lazy(() => import('./PayRollManagement/PyaheadManagement/Pyaheads/Pyaheads'))
		},
		{
			path: '/apps/pyahead-management/:pyaheadId',
			component: lazy(() => import('./PayRollManagement/PyaheadManagement/Pyahead/NewPyahead'))
		},
		//salary
		{
			path: '/apps/salary-management/salarys/:salaryId/:salaryName?',
			component: lazy(() => import('./PayRollManagement/SalaryManagement/Salary/NewSalary'))
		},

		{
			path: '/apps/salary-management/salarys',
			component: lazy(() => import('./PayRollManagement/SalaryManagement/Salarys/Salarys'))
		},
		{
			path: '/apps/salary-management/:salaryId',
			component: lazy(() => import('./PayRollManagement/SalaryManagement/Salary/NewSalary'))
		},
		//payment-salary
		{
			path: '/apps/paymentSalary-management/paymentSalarys/:paymentSalaryId/:paymentSalaryName?',
			component: lazy(() => import('./PayRollManagement/SalaryPaymentManagement/PaymentSalary/NewPaymentSalary'))
		},
		{
			path: '/apps/paymentSalary-management/paymentSalarys',
			component: lazy(() => import('./PayRollManagement/SalaryPaymentManagement/PaymentSalarys/PaymentSalarys'))
		},
		{
			path: '/apps/paymentSalary-management/:paymentSalaryId',
			component: lazy(() => import('./PayRollManagement/SalaryPaymentManagement/PaymentSalary/NewPaymentSalary'))
		},

		//payment-PF
		// {
		// 	path: '/apps/paymentSalary-management/paymentSalarys/:paymentSalaryId/:paymentSalaryName?',
		// 	component: lazy(() => import('./PayRollManagement/SalaryPaymentManagement/PaymentSalary/NewPaymentSalary'))
		// },
		{
			path: '/apps/paymentPF-management/paymenPFs',
			component: lazy(() => import('./PayRollManagement/PFPaymentManagement/PaymentPFs/PaymentPFs'))
		},
		// {
		// 	path: '/apps/paymentSalary-management/:paymentSalaryId',
		// 	component: lazy(() => import('./PayRollManagement/SalaryPaymentManagement/PaymentSalary/NewPaymentSalary'))
		// },

		//attendanceleave application
		{
			path: '/apps/application-management/applications/:applicationId/:applicationName?',
			component: lazy(() => import('./PayRollManagement/AttendanceVoucherManagement/Application/NewApplication'))
		},

		{
			path: '/apps/application-management/applications',
			component: lazy(() => import('./PayRollManagement/AttendanceVoucherManagement/Applications/Applications'))
		},
		{
			path: '/apps/application-management/:applicationId',
			component: lazy(() => import('./PayRollManagement/AttendanceVoucherManagement/Application/NewApplication'))
		},

		//attendancetype
		{
			path: '/apps/attendancetype-management/attendancetypes/:attendancetypeId/:attendancetypeName?',
			component: lazy(() =>
				import('./PayRollManagement/AttendanceTypeManagement/Attendancetype/NewAttendancetype')
			)
		},
		{
			path: '/apps/attendancetype-management/attendancetypes',
			component: lazy(() =>
				import('./PayRollManagement/AttendanceTypeManagement/Attendancetypes/Attendancetypes')
			)
		},
		{
			path: '/apps/attendancetype-management/:attendancetypeId',
			component: lazy(() =>
				import('./PayRollManagement/AttendanceTypeManagement/Attendancetype/NewAttendancetype')
			)
		},

		//calculationtype
		{
			path: '/apps/calculationtype-management/calculationtypes/:calculationtypeId/:calculationtypeName?',
			component: lazy(() =>
				import('./PayRollManagement/CalculationTypeManagement/Calculationtype/NewCalculationtype')
			)
		},
		{
			path: '/apps/calculationtype-management/calculationtypes',
			component: lazy(() =>
				import('./PayRollManagement/CalculationTypeManagement/Calculationtypes/Calculationtypes')
			)
		},
		{
			path: '/apps/calculationtype-management/:calculationtypeId',
			component: lazy(() =>
				import('./PayRollManagement/CalculationTypeManagement/Calculationtype/NewCalculationtype')
			)
		},

		//vouchertype
		{
			path: '/apps/vouchertype-management/vouchertypes/:vouchertypeId/:vouchertypeName?',
			component: lazy(() => import('./PayRollManagement/VoucherTypeManagement/Vouchertype/NewVouchertype'))
		},
		{
			path: '/apps/vouchertype-management/vouchertypes',
			component: lazy(() => import('./PayRollManagement/VoucherTypeManagement/Vouchertypes/Vouchertypes'))
		},
		{
			path: '/apps/vouchertype-management/:vouchertypeId',
			component: lazy(() => import('./PayRollManagement/VoucherTypeManagement/Vouchertype/NewVouchertype'))
		},

		//voucher
		{
			path: '/apps/voucher-managements/vouchers/:voucherId/:voucherName?',
			component: lazy(() => import('./PayRollManagement/PayrollVouchersManagement/Voucher/NewVoucher'))
		},
		{
			path: '/apps/voucher-managements/vouchers',
			component: lazy(() => import('./PayRollManagement/PayrollVouchersManagement/Vouchers/Vouchers'))
		},
		{
			path: '/apps/voucher-management/:voucherId',
			component: lazy(() => import('./PayRollManagement/PayrollVouchersManagement/Voucher/NewVoucher'))
		},

		//vouchertypeclass
		{
			path: '/apps/vouchertypeclass-management/vouchertypeclasss/:vouchertypeclassId/:vouchertypeclassName?',
			component: lazy(() =>
				import('./PayRollManagement/VoucherTypeClassManagement/Vouchertypeclass/NewVouchertypeclass')
			)
		},
		{
			path: '/apps/vouchertypeclass-management/vouchertypeclasss',
			component: lazy(() =>
				import('./PayRollManagement/VoucherTypeClassManagement/Vouchertypeclasss/Vouchertypeclasss')
			)
		},
		{
			path: '/apps/vouchertypeclass-management/:vouchertypeclassId',
			component: lazy(() =>
				import('./PayRollManagement/VoucherTypeClassManagement/Vouchertypeclass/NewVouchertypeclass')
			)
		},

		//payheadtype
		{
			path: '/apps/payheadtype-management/payheadtypes/:payheadtypeId/:payheadtypeName?',
			component: lazy(() => import('./PayRollManagement/PayheadTypeManagement/Payheadtype/NewPayheadtype'))
		},
		{
			path: '/apps/payheadtype-management/payheadtypes',
			component: lazy(() => import('./PayRollManagement/PayheadTypeManagement/Payheadtypes/Payheadtypes'))
		},
		{
			path: '/apps/payheadtype-management/:payheadtypeId',
			component: lazy(() => import('./PayRollManagement/PayheadTypeManagement/Payheadtype/NewPayheadtype'))
		},

		// attendance Report
		{
			path: '/apps/attendance-management/attendance-reports',
			component: lazy(() =>
				import(
					'./ReportManagement/payrollsReportManagement/AttendanceReportManagement/AttendanceReport/AttendanceReport'
				)
			)
		},
		// attendancesummary Report
		{
			path: '/apps/attendancesummary-management/attendancesummary-reports',
			component: lazy(() =>
				import(
					'./ReportManagement/payrollsReportManagement/AttendanceSummaryReportManagement/AttendancesummaryReport/AttendancesummaryReport'
				)
			)
		},

		//department
		{
			path: '/apps/department-management/departments/:departmentId/:departmentName?',
			component: lazy(() => import('./DepartmentsManagement/Department/NewDepartment'))
		},
		{
			path: '/apps/department-management/departments',
			component: lazy(() => import('./DepartmentsManagement/Departments/Departments'))
		},
		{
			path: '/apps/department-management/:departmentId',
			component: lazy(() => import('./DepartmentsManagement/Department/NewDepartment'))
		},

		//purchasereturn
		{
			path: '/apps/purchasereturn-management/purchasereturn/:purchasereturnId/:purchasereturnName?',
			component: lazy(() => import('./PurchaseReturnManagement/PurchaseReturn/NewPurchaseReturn'))
		},
		{
			path: '/apps/purchasereturn-management/purchasereturn',
			component: lazy(() => import('./PurchaseReturnManagement/PurchaseReturns/PurchaseReturns'))
		},
		{
			path: '/apps/purchasereturn-management/:purchasereturnId',
			component: lazy(() => import('./PurchaseReturnManagement/PurchaseReturn/NewPurchaseReturn'))
		},
		//sellreturn
		{
			path: '/apps/sellreturn-management/sellreturn/:sellreturnId/:sellreturnName?',
			component: lazy(() => import('./SellReturnManagement/SellReturn/NewSellReturn'))
		},
		{
			path: '/apps/sellreturn-management/sellreturn',
			component: lazy(() => import('./SellReturnManagement/SellReturns/SellReturns'))
		},
		{
			path: '/apps/sellreturn-management/:sellreturnId',
			component: lazy(() => import('./SellReturnManagement/SellReturn/NewSellReturn'))
		},

		//qualification
		{
			path: '/apps/qualification-management/qualifications/:qualificationId/:qualificationName?',
			component: lazy(() => import('./QualificationsManagement/Qualification/NewQualification'))
		},
		{
			path: '/apps/qualification-management/qualifications',
			component: lazy(() => import('./QualificationsManagement/Qualifications/Qualifications'))
		},
		{
			path: '/apps/qualification-management/:qualificationId',
			component: lazy(() => import('./QualificationsManagement/Qualification/NewQualification'))
		},

		//city
		{
			path: '/apps/city-management/cities/:cityId/:cityName?',
			component: lazy(() => import('./CitysManagement/City/NewCity'))
		},
		{
			path: '/apps/city-management/cities',
			component: lazy(() => import('./CitysManagement/Citys/Citys'))
		},
		{
			path: '/apps/city-management/:cityId',
			component: lazy(() => import('./CitysManagement/City/NewCity'))
		},

		//thana
		{
			path: '/apps/thana-management/thanas/:thanaId/:thanaName?',
			component: lazy(() => import('./ThanasManagement/Thana/NewThana'))
		},
		{
			path: '/apps/thana-management/thanas',
			component: lazy(() => import('./ThanasManagement/Thanas/Thanas'))
		},
		{
			path: '/apps/thana-management/:thanaId',
			component: lazy(() => import('./ThanasManagement/Thana/NewThana'))
		},

		//branch
		{
			path: '/apps/branch-management/branchs/:branchId/:branchName?',
			component: lazy(() => import('./BranchsManagement/Branch/NewBranch'))
		},
		{
			path: '/apps/branch-management/branchs',
			component: lazy(() => import('./BranchsManagement/Branchs/Branchs'))
		},
		{
			path: '/apps/branch-management/:branchId',
			component: lazy(() => import('./BranchsManagement/Branch/NewBranch'))
		},
		//color
		{
			path: '/apps/color-management/colors/:colorId/:colorName?',
			component: lazy(() => import('./ColorsManagement/Color/NewColor'))
		},
		{
			path: '/apps/color-management/colors',
			component: lazy(() => import('./ColorsManagement/Colors/Colors'))
		},
		{
			path: '/apps/color-management/:colorId',
			component: lazy(() => import('./ColorsManagement/Color/NewColor'))
		},
		//size
		{
			path: '/apps/size-management/sizes/:sizeId/:sizeName?',
			component: lazy(() => import('./SizesManagement/Size/NewSize'))
		},
		{
			path: '/apps/size-management/sizes',
			component: lazy(() => import('./SizesManagement/Sizes/Sizes'))
		},
		{
			path: '/apps/size-management/:sizeId',
			component: lazy(() => import('./SizesManagement/Size/NewSize'))
		},
		//chat

		{
			path: '/apps/chat-management/chats',
			component: lazy(() => import('./ChatsManagement/Chats/Chats'))
		},

		//sitesettings
		{
			path: '/apps/sitesettings-management/sitesetting/:sitesettingId/:sitesettingName?',
			component: lazy(() => import('./SitesettingsManagement/Sitesetting/NewSitesetting'))
		},
		{
			path: '/apps/sitesettings-management/sitesettings/',
			component: lazy(() => import('./SitesettingsManagement/Sitesettings/Sitesettings'))
		},
		{
			path: '/apps/sitesettings-management/sitesetting/:sitesettingId',
			component: lazy(() => import('./SitesettingsManagement/Sitesetting/NewSitesetting'))
		},

		//permission
		{
			path: '/apps/permission-management/permissions/:permissionId/:permissionName?',
			component: lazy(() => import('./PermissionsManagement/Permission/NewPermission'))
		},
		{
			path: '/apps/permission-management/permissions',
			component: lazy(() => import('./PermissionsManagement/Permissions/Permissions'))
		},
		{
			path: '/apps/permission-management/:permissionId',
			component: lazy(() => import('./PermissionsManagement/Permission/NewPermission'))
		},

		//roles
		{
			path: '/apps/roles-management/roles',
			component: lazy(() => import('./RoleManagement/Roles/Roles'))
		},
		{
			path: '/apps/roles-management/:roleId',
			component: lazy(() => import('./RoleManagement/Role/NewRole'))
		},
		{
			path: '/apps/roles-management/:roleId/:roleName?',
			component: lazy(() => import('./RoleManagement/Role/NewRole'))
		},
		//menu
		{
			path: '/apps/menu-management/menus/:menuId/:menuName?',
			component: lazy(() => import('./MenusManagement/Menu/NewMenu'))
		},
		{
			path: '/apps/menu-management/menus',
			component: lazy(() => import('./MenusManagement/Menus/Menus'))
		},
		{
			path: '/apps/menu-management/:menuId',
			component: lazy(() => import('./MenusManagement/Menu/NewMenu'))
		},

		//roleMenu
		{
			path: '/apps/roleMenu-management/roleMenus/:roleMenuId/:roleMenuName?',
			component: lazy(() => import('./RoleMenusManagement/RoleMenu/NewRoleMenu'))
		},
		{
			path: '/apps/roleMenu-management/roleMenus',
			component: lazy(() => import('./RoleMenusManagement/RoleMenus/RoleMenus'))
		},
		{
			path: '/apps/roleMenu-management/:roleMenuId',
			component: lazy(() => import('./RoleMenusManagement/RoleMenu/NewRoleMenu'))
		},

		//ledger
		{
			path: '/apps/ledger-management/ledgers/:ledgerId/:ledgerName?',
			component: lazy(() => import('./AllAccountManagement/LedgersManagement/Ledger/NewLedger'))
		},
		{
			path: '/apps/ledger-management/ledgers',
			component: lazy(() => import('./AllAccountManagement/LedgersManagement/Ledgers/Ledgers'))
		},
		{
			path: '/apps/ledger-management/:ledgerId',
			component: lazy(() => import('./AllAccountManagement/LedgersManagement/Ledger/NewLedger'))
		},

		//subledger
		{
			path: '/apps/subledger-management/subledgers/:subledgerId/:subledgerName?',
			component: lazy(() => import('./AllAccountManagement/SubLedgersManagement/SubLedger/NewSubLedger'))
		},
		{
			path: '/apps/subledger-management/subledgers',
			component: lazy(() => import('./AllAccountManagement/SubLedgersManagement/SubLedgers/SubLedgers'))
		},
		{
			path: '/apps/subledger-management/:subledgerId',
			component: lazy(() => import('./AllAccountManagement/SubLedgersManagement/SubLedger/NewSubLedger'))
		},

		//group
		{
			path: '/apps/group-management/groups/:groupId/:groupName?',
			component: lazy(() => import('./AllAccountManagement/GroupsManagement/Group/NewGroup'))
		},
		{
			path: '/apps/group-management/groups',
			component: lazy(() => import('./AllAccountManagement/GroupsManagement/Groups/Groups'))
		},
		{
			path: '/apps/group-management/:groupId',
			component: lazy(() => import('./AllAccountManagement/GroupsManagement/Group/NewGroup'))
		},

		//payment-voucher
		{
			path: '/apps/accounts/paymentvouchers/:paymentvoucherId/:paymentvoucherName?',
			component: lazy(() =>
				import('./AllAccountManagement/PaymentVoucherManagement/PaymentVoucher/NewPaymentvoucher')
			)
		},
		{
			path: '/apps/accounts/paymentvouchers',
			component: lazy(() =>
				import('./AllAccountManagement/PaymentVoucherManagement/PaymentVouchers/Paymentvouchers')
			)
		},
		{
			path: '/apps/accounts/paymentvoucher/:paymentvoucherId',
			component: lazy(() =>
				import('./AllAccountManagement/PaymentVoucherManagement/PaymentVoucher/NewPaymentvoucher')
			)
		},
		//receipt-voucher
		{
			path: '/apps/accounts/receiptvouchers/:receiptvoucherId/:receiptvoucherName?',
			component: lazy(() =>
				import('./AllAccountManagement/ReceiptVouchersManagement/ReceiptVoucher/NewReceiptVoucher')
			)
		},
		{
			path: '/apps/accounts/receiptvouchers',
			component: lazy(() =>
				import('./AllAccountManagement/ReceiptVouchersManagement/ReceiptVouchers/ReceiptVouchers')
			)
		},
		{
			path: '/apps/accounts/receiptvoucher/:receiptvoucherId',
			component: lazy(() =>
				import('./AllAccountManagement/ReceiptVouchersManagement/ReceiptVoucher/NewReceiptVoucher')
			)
		},
		//sales
		{
			path: '/apps/accounts/receivable_bills/:salesId/:salesName?',
			component: lazy(() => import('./AllAccountManagement/SalesManagement/Sales/NewSales'))
		},
		{
			path: '/apps/accounts/receivable_bills',
			component: lazy(() => import('./AllAccountManagement/SalesManagement/Saless/Saless'))
		},
		{
			path: '/apps/account/receivable_bills/:salesId',
			component: lazy(() => import('./AllAccountManagement/SalesManagement/Sales/NewSales'))
		},
		//purchase
		{
			path: '/apps/accounts/payable_bill/:purchaseId/:purchaseName?',
			component: lazy(() => import('./AllAccountManagement/PurchaseManagement/Purchase/NewPurchase'))
		},
		{
			path: '/apps/accounts/payable_bills',
			component: lazy(() => import('./AllAccountManagement/PurchaseManagement/Purchases/Purchases'))
		},
		{
			path: '/apps/account/payable_bill/:purchaseId',
			component: lazy(() => import('./AllAccountManagement/PurchaseManagement/Purchase/NewPurchase'))
		},
		//customer
		{
			path: '/apps/customer-management/customers/:customerId/:customerName?',
			component: lazy(() => import('./CustomersManagement/Customer/NewCustomer'))
		},
		{
			path: '/apps/customer-otp-management/customers/:customerId/:customerName?',
			component: lazy(() => import('./CustomersManagement/Customers/SendOTP'))
		},
		{
			path: '/apps/customer-management/customers',
			component: lazy(() => import('./CustomersManagement/Customers/Customers'))
		},
		{
			path: '/apps/latest-customer-management/customers/:latestCustomer?',
			component: lazy(() => import('./CustomersManagement/Customers/Customers'))
		},
		{
			path: '/apps/customer-management/:customerId',
			component: lazy(() => import('./CustomersManagement/Customer/NewCustomer'))
		},
		//vendor
		{
			path: '/apps/vendor-management/vendors/:vendorId/:vendorName?',
			component: lazy(() => import('./VendorsManagement/Vendor/NewVendor'))
		},
		{
			path: '/apps/vendor-otp-management/vendors/:vendorId/:vendorName?',
			component: lazy(() => import('./VendorsManagement/Vendors/SendOTP'))
		},
		{
			path: '/apps/vendor-management/vendors',
			component: lazy(() => import('./VendorsManagement/Vendors/Vendors'))
		},
		{
			path: '/apps/vendor-management/:vendorId',
			component: lazy(() => import('./VendorsManagement/Vendor/NewVendor'))
		},
		//shippingAddress
		{
			path: '/apps/shippingAddress-management/shippingAddresses/:shippingAddressId/:shippingAddressName?',
			component: lazy(() => import('./ShippingAddressesManagement/ShippingAddress/NewShippingAddress'))
		},
		{
			path: '/apps/shippingAddress-management/shippingAddresses',
			component: lazy(() => import('./ShippingAddressesManagement/ShippingAddresses/ShippingAddresses'))
		},
		{
			path: '/apps/shippingAddress-management/:shippingAddressId',
			component: lazy(() => import('./ShippingAddressesManagement/ShippingAddress/NewShippingAddress'))
		},
		//paymentVoucher
		{
			path: '/apps/paymentVoucher-management/paymentVouchers/:paymentVoucherId/:paymentVoucherName?',
			component: lazy(() =>
				import('./AllAccountManagement/PaymentVoucherManagement/PaymentVoucher/NewPaymentvoucher')
			)
		},
		{
			path: '/apps/paymentVoucher-management/paymentVouchers',
			component: lazy(() =>
				import('./AllAccountManagement/PaymentVoucherManagement/PaymentVouchers/Paymentvouchers')
			)
		},
		{
			path: '/apps/paymentVoucher-management/:paymentVoucherId',
			component: lazy(() =>
				import('./AllAccountManagement/PaymentVoucherManagement/PaymentVoucher/NewPaymentvoucher')
			)
		},
		//receiptVoucher
		{
			path: '/apps/receiptVoucher-management/receiptVouchers/:receiptVoucherId/:receiptVoucherName?',
			component: lazy(() =>
				import('./AllAccountManagement/ReceiptVouchersManagement/ReceiptVoucher/NewReceiptVoucher')
			)
		},
		{
			path: '/apps/receiptVoucher-management/receiptVouchers',
			component: lazy(() =>
				import('./AllAccountManagement/ReceiptVouchersManagement/ReceiptVouchers/ReceiptVouchers')
			)
		},
		{
			path: '/apps/receiptVoucher-management/:receiptVoucherId',
			component: lazy(() =>
				import('./AllAccountManagement/ReceiptVouchersManagement/ReceiptVoucher/NewReceiptVoucher')
			)
		},
		//sales
		{
			path: '/apps/accounts/receivable_bills/:salesId/:salesName?',
			component: lazy(() => import('./AllAccountManagement/SalesManagement/Sales/NewSales'))
		},
		{
			path: '/apps/accounts/receivable_bills',
			component: lazy(() => import('./AllAccountManagement/SalesManagement/Saless/Saless'))
		},
		{
			path: '/apps/account/receivable_bills/:salesId',
			component: lazy(() => import('./AllAccountManagement/SalesManagement/Sales/NewSales'))
		},
		//purchase
		{
			path: '/apps/accounts/payable_bill/:purchaseId/:purchaseName?',
			component: lazy(() => import('./AllAccountManagement/PurchaseManagement/Purchase/NewPurchase'))
		},
		{
			path: '/apps/accounts/payable_bills',
			component: lazy(() => import('./AllAccountManagement/PurchaseManagement/Purchases/Purchases'))
		},
		{
			path: '/apps/account/payable_bill/:purchaseId',
			component: lazy(() => import('./AllAccountManagement/PurchaseManagement/Purchase/NewPurchase'))
		},
		//contra
		{
			path: '/apps/contra-management/contras/:contraId/:contraName?',
			component: lazy(() => import('./AllAccountManagement/ContrasManagement/Contra/NewContra'))
		},
		{
			path: '/apps/contra-management/contras',
			component: lazy(() => import('./AllAccountManagement/ContrasManagement/Contras/Contras'))
		},
		{
			path: '/apps/contra-management/:contraId',
			component: lazy(() => import('./AllAccountManagement/ContrasManagement/Contra/NewContra'))
		},
		//journal
		{
			path: '/apps/journal-management/journals/:journalId/:journalName?',
			component: lazy(() => import('./AllAccountManagement/JournalsManagement/Journal/NewJournal'))
		},
		{
			path: '/apps/journal-management/journals',
			component: lazy(() => import('./AllAccountManagement/JournalsManagement/Journals/Journals'))
		},
		{
			path: '/apps/journal-management/:journalId',
			component: lazy(() => import('./AllAccountManagement/JournalsManagement/Journal/NewJournal'))
		},
		// order
		{
			path: '/apps/order-management/orders/:orderId/:orderName?',
			component: lazy(() => import('./OrdersManagement/Order/NewOrder'))
		},
		{
			path: '/apps/order-managements/orders/:paymentStatus?',
			component: lazy(() => import('./OrdersManagement/Orders/Orders'))
		},
		{
			path: '/apps/order-management/:orderId',
			component: lazy(() => import('./OrdersManagement/Order/NewOrder'))
		},
		{
			path: '/apps/order-managements/order/:orderId',
			component: lazy(() => import('./OrdersManagement/Orders/orderDetails/OrderDetails'))
		},
		{
			path: '/apps/order-managements/order-invoice/:orderId',
			component: lazy(() => import('./OrdersManagement/Orders/OrderInvoice'))
		},
		{
			path: '/apps/order-managements/send-email/:orderNo/:email/:orderId/:number?',
			component: lazy(() => import('./OrdersManagement/Orders/SendEmail'))
		},

		// categories
		{
			path: '/apps/categories-management/categories',
			component: lazy(() => import('./CategoryManagement/Categories/Categories'))
		},
		{
			path: '/apps/categories-management/:categoryId',
			component: lazy(() => import('./CategoryManagement/Category/NewCategory'))
		},
		{
			path: '/apps/categories-management/:categoryId/:categoryName?',
			component: lazy(() => import('./CategoryManagement/Category/NewCategory'))
		},

		// brand
		{
			path: '/apps/brand-management/brands/:brandId/:brandName?',
			component: lazy(() => import('./BrandsManagement/Brand/NewBrand'))
		},
		{
			path: '/apps/brand-management/brands',
			component: lazy(() => import('./BrandsManagement/Brands/Brands'))
		},
		{
			path: '/apps/brand-management/:brandId',
			component: lazy(() => import('./BrandsManagement/Brand/NewBrand'))
		},

		// manufacturer
		{
			path: '/apps/manufacturer-management/manufacturers/:manufacturerId/:manufacturerName?',
			component: lazy(() => import('./ManufacturersManagement/Manufacturer/NewManufacturer'))
		},
		{
			path: '/apps/manufacturer-management/manufacturers',
			component: lazy(() => import('./ManufacturersManagement/Manufactures/Manufacturers'))
		},
		{
			path: '/apps/manufacturer-management/:manufacturerId',
			component: lazy(() => import('./ManufacturersManagement/Manufacturer/NewManufacturer'))
		},
		//slidersetting
		{
			path: '/apps/slidersetting-management/slidersettings/:slidersettingId/:slidersettingName?',
			component: lazy(() => import('./SlidersettingsManagement/Slidersetting/NewSlidersetting'))
		},
		{
			path: '/apps/slidersetting-management/slidersettings',
			component: lazy(() => import('./SlidersettingsManagement/Slidersettings/Slidersettings'))
		},
		{
			path: '/apps/slidersetting-management/:slidersettingId',
			component: lazy(() => import('./SlidersettingsManagement/Slidersetting/NewSlidersetting'))
		},
		//customertype
		{
			path: '/apps/customertype-management/customertypes/:customertypeId/:customertypeName?',
			component: lazy(() => import('./CustomertypesManagement/Customertype/NewCustomertype'))
		},
		{
			path: '/apps/customertype-management/customertypes',
			component: lazy(() => import('./CustomertypesManagement/Customertypes/Customertypes'))
		},
		{
			path: '/apps/customertype-management/:customertypeId',
			component: lazy(() => import('./CustomertypesManagement/Customertype/NewCustomertype'))
		},

		// support
		{
			path: '/apps/support-management/supports/:supportId/:supportName?',
			component: lazy(() => import('./SupportManagement/Supportss/Support/NewSupport'))
		},
		{
			path: '/apps/support-management/supports',
			component: lazy(() => import('./SupportManagement/Supportss/Supports/Supports'))
		},
		{
			path: '/apps/support-management/:supportId',
			component: lazy(() => import('./SupportManagement/Supportss/Support/NewSupport'))
		},
		{
			path: '/apps/support-managements/ticket-details/:supportId',
			component: lazy(() => import('./SupportManagement/Supportss/Support/NewSupport'))
		},

		//sales
		{
			path: '/apps/sale-management/sales/:saleId/:saleName?',
			component: lazy(() => import('./SupportManagement/Sales/Sale/NewSale'))
		},
		{
			path: '/apps/sale-management/sales',
			component: lazy(() => import('./SupportManagement/Sales/Sales/Sales'))
		},
		{
			path: '/apps/sale-management/:saleId',
			component: lazy(() => import('./SupportManagement/Sales/Sale/NewSale'))
		},
		{
			path: '/apps/sale-managements/ticket-details/:saleId',
			component: lazy(() => import('./SupportManagement/Sales/Sale/NewSale'))
		},

		//accounts
		{
			path: '/apps/account-management/accounts/:accountId/:accountName?',
			component: lazy(() => import('./SupportManagement/Accounts/Account/AccountForm'))
		},
		{
			path: '/apps/acccount-management/accounts',
			component: lazy(() => import('./SupportManagement/Accounts/Accounts/Accounts'))
		},
		{
			path: '/apps/account-management/:accountId',
			component: lazy(() => import('./SupportManagement/Accounts/Account/NewAccount'))
		},
		{
			path: '/apps/account-managements/ticket-details/:accountId',
			component: lazy(() => import('./SupportManagement/Accounts/Account/NewAccount'))
		},
		//discount
		{
			path: '/apps/discount-management/discounts/:discountId/:discountName?',
			component: lazy(() => import('./DiscountManagement/Discount/NewDiscount'))
		},
		{
			path: '/apps/discount-management/discounts',
			component: lazy(() => import('./DiscountManagement/Discounts/Discounts'))
		},
		{
			path: '/apps/discount-management/:discountId',
			component: lazy(() => import('./DiscountManagement/Discount/NewDiscount'))
		},

		// purchase-requests
		{
			path: '/apps/purchase-management/purchase-requests/:purchaseId/:purchaseName?',
			component: lazy(() => import('./PurchasesManagement/Purchase/NewPurchase'))
		},
		{
			path: '/apps/purchase-management/purchase-request/items/:purchaseId/:purchaseName?',
			component: lazy(() => import('./PurchasesManagement/Purcahses/PurchasesItems/PurchasesItems'))
		},
		{
			path: '/apps/purchase-management/purchase-request/purchase-invoice/:purchaseId/:purchaseName?',
			component: lazy(() => import('./PurchasesManagement/Purcahses/PurchaseInvoice'))
		},
		{
			path: '/apps/purchase-management/purchase-requests',
			component: lazy(() => import('./PurchasesManagement/Purcahses/Purchases'))
		},
		{
			path: '/apps/purchase-management/:purchaseId',
			component: lazy(() => import('./PurchasesManagement/Purchase/NewPurchase'))
		},

		// purchase-final
		{
			path: '/apps/purchasefinal-management/purchase-final/:purchaseFinalId/:purchaseFinalName?',
			component: lazy(() => import('./PurchasesFinalManagement/PurchaseFinal/NewPurchaseFinal'))
		},
		{
			path: '/apps/purchasefinal-management/purchase-final',
			component: lazy(() => import('./PurchasesFinalManagement/PurchaseFinals/PurchaseFinals'))
		},
		{
			path: '/apps/purchasefinal-management/:purchaseFinalId',
			component: lazy(() => import('./PurchasesFinalManagement/PurchaseFinal/NewPurchaseFinal'))
		},

		// receiptReport
		{
			path: '/apps/report-management/receipt-reports',
			component: lazy(() =>
				import(
					'./ReportManagement/accountsReportManagement/ReceiptReportManagement/ReceiptReport/ReceiptReport'
				)
			)
		},
		// receipt Summary Report
		{
			path: '/apps/report-management/receipt-summary-reports',
			component: lazy(() =>
				import(
					'./ReportManagement/accountsReportManagement/ReceiptSummaryReportManagement/ReceiptSummaryReport/ReceiptSummaryReport'
				)
			)
		},
		// paymentReport
		{
			path: '/apps/report-management/payment-reports',
			component: lazy(() =>
				import(
					'./ReportManagement/accountsReportManagement/PaymentReportManagement/PaymentReport/PaymentReport'
				)
			)
		},
		// payment Summary Report
		{
			path: '/apps/report-management/payment-summary-reports',
			component: lazy(() =>
				import(
					'./ReportManagement/accountsReportManagement/PaymentSummaryReportManagement/PaymentSummaryReport/PaymentSummaryReport'
				)
			)
		},
		// ledgerReport
		{
			path: '/apps/report-management/ledger-reports',
			component: lazy(() =>
				import('./ReportManagement/accountsReportManagement/LedgerReportManagement/LedgerReport/LedgerReport')
			)
		},
		// account statement Report
		{
			path: '/apps/report-management/account-statement-reports',
			component: lazy(() =>
				import(
					'./ReportManagement/accountsReportManagement/AccountStatementReportManagement/AccountStatementReport/AccountStatementReport'
				)
			)
		},
		// ledgerReport
		{
			path: '/apps/report-management/account-statement-summary-reports',
			component: lazy(() =>
				import(
					'./ReportManagement/accountsReportManagement/AccountSummaryReportManagement/AccountSummaryReport/AccountSummaryReport'
				)
			)
		},
		// Order Report
		{
			path: '/apps/report-management/order-reports',
			component: lazy(() =>
				import('./ReportManagement/eCommerceReportManagement/OrderReportManagement/OrderReport/OrderReport')
			)
		},
		// Order Summary Report
		{
			path: '/apps/report-management/order-summary-reports',
			component: lazy(() =>
				import(
					'./ReportManagement/eCommerceReportManagement/OrderSummaryReportManagement/OrderSummaryReport/OrderSummaryReport'
				)
			)
		},

		// Stock Summary Report
		{
			path: '/apps/report-management/stocks-reports',
			component: lazy(() =>
				import('./ReportManagement/eCommerceReportManagement/StockReportManagement/StockReport/StockReport')
			)
		},
		// Low Stock Summary Report
		{
			path: '/apps/report-management/low-stocks-reports',
			component: lazy(() =>
				import('./ReportManagement/eCommerceReportManagement/LowStockReportManagement/StockReport/StockReport')
			)
		},
		// Out of Stock Summary Report
		{
			path: '/apps/report-management/out_of-stocks-reports',
			component: lazy(() =>
				import(
					'./ReportManagement/eCommerceReportManagement/OutOfStockReportManagement/OutOfStockReport/StockReport'
				)
			)
		},

		// Purchage Final Report
		{
			path: '/apps/report-management/purchage-reports',
			component: lazy(() =>
				import(
					'./ReportManagement/eCommerceReportManagement/PurchageFinalReportManagement/PurchageFinalReport/PurchageFinalReport'
				)
			)
		},
		{
			path: '/apps/report-management/purchage-final-reports/purchase-invoice/:purchaseId',
			component: lazy(() =>
				import(
					'./ReportManagement/eCommerceReportManagement/PurchageFinalReportManagement/PurchageFinalReport/PurchaseInvoice'
				)
			)
		},

		// Purchage Return Report
		{
			path: '/apps/report-management/purchage-return-reports',
			component: lazy(() =>
				import(
					'./ReportManagement/eCommerceReportManagement/PurchageReturnReportManagement/PurchageReturnReport/PurchageReturnReport'
				)
			)
		},
		{
			path: '/apps/report-management/purchage-return-reports/purchase-invoice/:purchaseId',
			component: lazy(() =>
				import(
					'./ReportManagement/eCommerceReportManagement/PurchageReturnReportManagement/PurchageReturnReport/PurchaseInvoice'
				)
			)
		},

		// Sell Return Report
		{
			path: '/apps/report-management/sell-return-reports',
			component: lazy(() =>
				import(
					'./ReportManagement/eCommerceReportManagement/SellReturnReportManagement/SellReturnReport/SellReturnReport'
				)
			)
		},

		// Purchage Req Summary Report
		{
			path: '/apps/report-management/purchage-req-summary-reports',
			component: lazy(() =>
				import(
					'./ReportManagement/eCommerceReportManagement/PurchageReqSummaryReportManagement/PurchageReqSummaryReport/PurchageReqSummaryReport'
				)
			)
		},
		//GrossProfitReportManagement
		{
			path: '/apps/report-management/gross-profit-reports',
			component: lazy(() =>
				import(
					'./ReportManagement/eCommerceReportManagement/GrossProfitReportManagement/ProfitReport/ProfitReport'
				)
			)
		},
		//ProfitLossIncomeReportManagement
		{
			path: '/apps/report-management/profit-loss-reports',
			component: lazy(() =>
				import(
					'./ReportManagement/eCommerceReportManagement/ProfitLossIncomeReportManagement/ProfitLossIncomeReport/ProfitLossIncomeReport'
				)
			)
		},
		// employee salary ledger reports
		{
			path: '/apps/report-management/employee-salary-ledger-reports',
			component: lazy(() =>
				import(
					'./ReportManagement/payrollsReportManagement/SalaryLedgerReportManagement/SalaryReport/SalaryReport'
				)
			)
		},
		// employee salary  reports
		{
			path: '/apps/report-management/employee-salary-reports',
			component: lazy(() =>
				import('./ReportManagement/payrollsReportManagement/SalaryReportManagement/SalaryReport/SalaryReport')
			)
		},
		// employee salary Payment reports
		{
			path: '/apps/report-management/employee-salary-payment-reports',
			component: lazy(() =>
				import(
					'./ReportManagement/payrollsReportManagement/SalaryPaymentReportManagement/SalaryReport/SalaryReport'
				)
			)
		},
		// employee salary SLIP  reports
		{
			path: '/apps/report-management/employee-salary-slip-reports',
			component: lazy(() =>
				import(
					'./ReportManagement/payrollsReportManagement/SalarySlipReportManagement/SalaryReport/SalaryReport'
				)
			)
		},
		{
			path: '/apps/chat',
			component: lazy(() => import('./ChatManagement/chat/ChatApp'))
		}
	]
};

export default cashconnectRouteConfig;
