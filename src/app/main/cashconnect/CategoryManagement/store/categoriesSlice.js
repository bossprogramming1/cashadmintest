import { createAsyncThunk, createEntityAdapter, createSlice } from '@reduxjs/toolkit';
import axios from 'axios';
import { GET_CATEGORIES, GET_CATEGORIES_WITHOUT_PAGINATION } from '../../../../constant/constants';

//categories without pagination
export const getCategoriesWithoutPagination = createAsyncThunk(
	'categoriesManagement/categories/getCategories',
	async pageAndSize => {
		const authTOKEN = {
			headers: {
				'Content-type': 'application/json',
				Authorization: localStorage.getItem('jwt_access_token')
			}
		};

		const response = axios.get(GET_CATEGORIES_WITHOUT_PAGINATION, authTOKEN);
		const data = await response;

		return data.data.categories;
	}
);

export const getCategories = createAsyncThunk('categoriesManagement/categories/getCategories', async pageAndSize => {
	axios.defaults.headers.common['Content-type'] = 'application/json';
	axios.defaults.headers.common.Authorization = localStorage.getItem('jwt_access_token');

	const response = axios.get(GET_CATEGORIES, { params: pageAndSize });
	const data = await response;
	sessionStorage.setItem('total_categories_elements', data.data.total_elements);
	sessionStorage.setItem('total_categories_pages', data.data.total_pages);
	delete axios.defaults.headers.common['Content-type'];
	delete axios.defaults.headers.common.Authorization;

	return data.data.categories;
});

const categoriesAdapter = createEntityAdapter({});
export const { selectAll: selectCategories, selectById: selectCategoryById } = categoriesAdapter.getSelectors(
	state => state.categoriesManagement.categories
);

const categoriesSlice = createSlice({
	name: 'categoriesManagement',
	initialState: categoriesAdapter.getInitialState({
		searchText: ''
	}),
	reducers: {
		setCategoriesSearchText: {
			reducer: (state, action) => {
				state.searchText = action.payload;
			},
			prepare: event => ({ payload: event.target.value || '' })
		}
	},
	extraReducers: {
		[getCategories.fulfilled]: categoriesAdapter.setAll
	}
});

export const { setCategoriesSearchText } = categoriesSlice.actions;

export default categoriesSlice.reducer;
