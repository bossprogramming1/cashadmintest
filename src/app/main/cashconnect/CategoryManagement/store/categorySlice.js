import { createAsyncThunk, createSlice } from '@reduxjs/toolkit';
import { CREATE_CATEGORY, DELETE_CATEGORY, GET_CATEGORY, UPDATE_CATEGORY } from 'app/constant/constants';
import axios from 'axios';

//buildformdata
const buildFormData = (formData, data, parentKey) => {
	if (data && typeof data === 'object' && !(data instanceof Date) && !(data instanceof File)) {
		Object.keys(data).forEach(key => {
			buildFormData(formData, data[key], parentKey ? `${parentKey}[${key}]` : key);
		});
	} else {
		const value = data === null ? '' : data;

		formData.append(parentKey, value);
	}
};

//convertJsonToFormData
const jsonToFormData = data => {
	const formData = new FormData();

	buildFormData(formData, data);
	return formData;
};

export const saveCategory = createAsyncThunk(
	'categoryManagement/category/saveCategory',
	async (categoryData, { dispatch, getState }) => {
		const categoryDataToFormData = jsonToFormData(categoryData);
		const authTOKEN = {
			headers: {
				'Content-type': 'multipart/form-data',
				Authorization: localStorage.getItem('jwt_access_token')
			}
		};
		const response = axios.post(CREATE_CATEGORY, categoryDataToFormData, authTOKEN);
	}
);

export const getCategory = createAsyncThunk('categoryManagement/category/getCategory', async params => {
	const authTOKEN = {
		headers: {
			'Content-type': 'application/json',
			Authorization: localStorage.getItem('jwt_access_token')
		}
	};
	const response = await axios.get(`${GET_CATEGORY}${params.categoryId}`, authTOKEN);
	const data = await response.data;

	return data === undefined ? null : data;
});

export const removeCategory = createAsyncThunk(
	'categoryManagement/category/removeCategory',
	async (val, { dispatch, getState }) => {
		const authTOKEN = {
			headers: {
				'Content-type': 'application/json',
				Authorization: localStorage.getItem('jwt_access_token')
			}
		};
		const categoryId = val.id;
		const response = await axios.delete(`${DELETE_CATEGORY}${categoryId}`, authTOKEN);
	}
);

export const updateCategory = createAsyncThunk(
	'categoriesManagement/category/updateCategory',
	async (categoryData, { dispatch, getState }) => {
		const convertCategoryDataToFormData = jsonToFormData(categoryData);
		const authTOKEN = {
			headers: {
				'Content-type': 'application/json',
				Authorization: localStorage.getItem('jwt_access_token')
			}
		};
		const { category } = getState().categoriesManagement;
		const response = await axios.put(`${UPDATE_CATEGORY}${category.id}`, convertCategoryDataToFormData, authTOKEN);
	}
);

const categorySlice = createSlice({
	name: 'categoriesManagement/category',
	initialState: null,
	reducers: {
		resetCategory: () => null,
		newCategory: {
			reducer: (state, action) => action.payload,
			prepare: event => ({
				payload: {
					name: '',
					description: '',
					is_active: false,
					is_top_category: false,
					show_on_homepage: false,
					image: '',
					icon: '',
					parent: ''
				}
			})
		}
	},
	extraReducers: {
		[saveCategory.fulfilled]: (state, action) => {
			localStorage.setItem('categoryAlert', 'saveCategory');
			return action.payload;
		},
		[getCategory.fulfilled]: (state, action) => action.payload,
		[removeCategory.fulfilled]: (state, action) => {
			localStorage.setItem('categoryAlert', 'deleteCategory');
			return null;
		},
		[updateCategory.fulfilled]: () => {
			localStorage.setItem('categoryAlert', 'updateCategory');
		}
	}
});

export const { newCategory, resetCategory } = categorySlice.actions;

export default categorySlice.reducer;
