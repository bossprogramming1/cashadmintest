import FusePageCarded from '@fuse/core/FusePageCarded';
import { useDeepCompareEffect } from '@fuse/hooks';
import { yupResolver } from '@hookform/resolvers/yup';
import {
	CATEGORY_CREATE,
	CATEGORY_DELETE,
	CATEGORY_DETAILS,
	CATEGORY_UPDATE
} from 'app/constant/permission/permission';
import { getUserPermissions } from 'app/store/dataSlice';
import withReducer from 'app/store/withReducer';
import React, { useEffect, useState } from 'react';
import { FormProvider, useForm } from 'react-hook-form';
import { useDispatch, useSelector } from 'react-redux';
import { useParams } from 'react-router-dom';
import * as yup from 'yup';
import PagenotFound from '../../Pagenotfound/PagenotFound';
import { getCategory, newCategory, resetCategory } from '../store/categorySlice';
import reducer from '../store/index';
import CategoryForm from './CategoryForm';
import NewCategoryHeader from './NewCategoryHeader';

/**
 * Form Validation Schema
 */
const schema = yup.object().shape({
	name: yup.string().required('Name is required')
});

const NewCategory = () => {
	const methods = useForm({
		mode: 'onChange',
		defaultValues: {},
		resolver: yupResolver(schema)
	});
	const { reset, watch, control, onChange, formState } = methods;
	const dispatch = useDispatch();
	const routeParams = useParams();
	const UserPermissions = useSelector(state => state.data.UserPermissions);
	useEffect(() => {
		dispatch(getUserPermissions());
	}, []);
	const category = useSelector(({ categoriesManagement }) => categoriesManagement.category);
	const [noCategory, setNoCategory] = useState(false);
	useDeepCompareEffect(() => {
		function updateCategoryState() {
			const { categoryId } = routeParams;
			if (categoryId === 'new') {
				localStorage.removeItem('deleteCategory');
				localStorage.removeItem('updateCategory');
				/**
				 * Create New Category data
				 */
				dispatch(newCategory());
			} else {
				/**
				 * Get Category data
				 */
				dispatch(getCategory(routeParams)).then(action => {
					/**
					 * If the requested category is not exist show message
					 */
					if (!action.payload) {
						setNoCategory(true);
					}
				});
			}
		}

		updateCategoryState();
	}, [dispatch, routeParams]);

	useEffect(() => {
		if (!category) {
			return;
		}
		/**
		 * Reset the form on employee state changes
		 */
		reset(category);
	}, [category, reset]);

	useEffect(() => {
		return () => {
			/**
			 * Reset Employee on component unload
			 */
			dispatch(resetCategory());
			setNoCategory(false);
		};
	}, [dispatch]);

	return (
		<FormProvider {...methods}>
			{UserPermissions.includes(CATEGORY_CREATE) ||
			UserPermissions.includes(CATEGORY_UPDATE) ||
			UserPermissions.includes(CATEGORY_DELETE) ||
			UserPermissions.includes(CATEGORY_DETAILS) ? (
				<FusePageCarded
					classes={{
						toolbar: 'p-0',
						header: 'min-h-74 h-64'
					}}
					header={<NewCategoryHeader />}
					content={
						<div className="p-16 sm:p-24">
							<CategoryForm />
						</div>
					}
					innerScroll
				/>
			) : (
				<PagenotFound />
			)}
		</FormProvider>
	);
};

export default withReducer('categoriesManagement', reducer)(NewCategory);
