import { Typography } from '@material-ui/core';
import Checkbox from '@material-ui/core/Checkbox';
import { orange } from '@material-ui/core/colors';
import FormControl from '@material-ui/core/FormControl';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Icon from '@material-ui/core/Icon';
import { makeStyles } from '@material-ui/core/styles';
import TextField from '@material-ui/core/TextField';
import { Autocomplete } from '@material-ui/lab';
import clsx from 'clsx';
import { React, useEffect, useRef, useState } from 'react';
import { Controller, useFormContext } from 'react-hook-form';
import { useDispatch, useSelector } from 'react-redux';
import { BASE_URL } from '../../../../constant/constants';
import { getCategoriesWithoutPagination } from '../store/categoriesSlice';

const useStyles = makeStyles(theme => ({
	productImageFeaturedStar: {
		position: 'absolute',
		top: 0,
		right: 0,
		color: orange[400],
		opacity: 0
	},
	productImageUpload: {
		transitionProperty: 'box-shadow',
		transitionDuration: theme.transitions.duration.short,
		transitionTimingFunction: theme.transitions.easing.easeInOut
	},
	productImageItem: {
		transitionProperty: 'box-shadow',
		transitionDuration: theme.transitions.duration.short,
		transitionTimingFunction: theme.transitions.easing.easeInOut,
		'&:hover': {
			'& $productImageFeaturedStar': {
				opacity: 0.8
			}
		},
		'&.featured': {
			pointerEvents: 'none',
			boxShadow: theme.shadows[3],
			'& $productImageFeaturedStar': {
				opacity: 1
			},
			'&:hover $productImageFeaturedStar': {
				opacity: 1
			}
		}
	}
}));

const CategoryForm = () => {
	const methods = useFormContext();
	const { control, watch, setValue, formState } = methods;
	const { errors } = formState;
	const classes = useStyles();
	const dispatch = useDispatch();
	const [parentCategories, setParentCategories] = useState([]);
	const parentIdRef = useRef(null);
	const userId = useSelector(state => state.auth.user.id);
	const image = watch('image');
	const iconImage = watch('icon');
	const [previewImage, setPreviewImage] = useState();
	const [previewIconImage, setPreviewIconImage] = useState();

	useEffect(() => {
		dispatch(getCategoriesWithoutPagination()).then(action => setParentCategories(action.payload));
	}, []);

	return (
		<div>
			<Controller
				name="name"
				control={control}
				render={({ field }) => (
					<TextField
						{...field}
						className="mt-8 mb-16"
						error={!!errors?.name}
						required
						helperText={errors?.name?.message}
						label="Name"
						id="name"
						variant="outlined"
						fullWidth
						InputLabelProps={{ shrink: true }}
					/>
				)}
			/>
			<Controller
				name="description"
				control={control}
				render={({ field }) => (
					<TextField
						{...field}
						className="mt-8 mb-16"
						label="Description"
						id="description"
						variant="outlined"
						rows={6}
						multiline
						required
						fullWidth
						InputLabelProps={{ shrink: true }}
					/>
				)}
			/>
			<Controller
				name="parent"
				control={control}
				render={({ field: { onChange, value } }) => (
					<Autocomplete
						className="mt-8 mb-16"
						freeSolo
						value={value ? parentCategories.find(data => data.id === value) : null}
						options={parentCategories}
						getOptionLabel={option => {
							parentIdRef.current = option.parent;
							const parent = [];
							let parentstr = '';
							for (let i = 0; i < parentCategories.length; i++) {
								parent.push(
									`${
										option.parent
											? parentCategories.find(data => data.id === parentIdRef.current)?.name
											: null
									}`
								);
								parentIdRef.current = option.parent
									? parentCategories.find(data => data.id === parentIdRef.current)?.parent
									: null;
								parentIdRef.current ? null : (i = parentCategories.length);
							}
							parent.reverse();

							for (let i = 0; i < parent.length; i++) {
								parentstr += `${i !== 0 ? '>>' : ''}${parent[i]} `;
							}

							return option.parent ? `${parentstr}>> ${option?.name} ` : `${option?.name} `;
						}}
						onChange={(event, newValue) => {
							onChange(newValue?.id);
						}}
						renderInput={params => (
							<TextField
								{...params}
								//placeholder="Select a Parent Category"
								label="Parent category"
								variant="outlined"
								InputLabelProps={{
									shrink: true
								}}
							/>
						)}
					/>
				)}
			/>
			<Controller
				name="is_active"
				control={control}
				render={({ field }) => (
					<FormControl>
						<FormControlLabel
							required
							label="Active"
							control={<Checkbox {...field} checked={field.value ? field.value : false} />}
						/>
					</FormControl>
				)}
			/>
			<br />
			<Controller
				name="is_top_category"
				control={control}
				render={({ field }) => (
					<FormControl>
						<FormControlLabel
							required
							label="Is Top Category"
							control={<Checkbox {...field} checked={field.value ? field.value : false} />}
						/>
					</FormControl>
				)}
			/>
			<br />
			<Controller
				name="show_on_homepage"
				control={control}
				render={({ field }) => (
					<FormControl>
						<FormControlLabel
							required
							label="Show on homepage"
							control={<Checkbox {...field} checked={field.value ? field.value : false} />}
						/>
					</FormControl>
				)}
			/>

			<div className="flex justify-center sm:justify-start flex-wrap -mx-16">
				<Controller
					name="image"
					control={control}
					render={({ field: { onChange, value } }) => (
						<label
							htmlFor="button-file"
							className={clsx(
								'flex items-center justify-center relative w-128 h-128 rounded-16 mx-12 mb-24 overflow-hidden cursor-pointer shadow hover:shadow-lg'
							)}
						>
							<input
								accept="image/*"
								className="hidden"
								id="button-file"
								type="file"
								onChange={async e => {
									const reader = new FileReader();
									reader.onload = () => {
										if (reader.readyState === 2) {
											setPreviewImage(reader.result);
										}
									};
									reader.readAsDataURL(e.target.files[0]);

									const file = e.target.files[0];
									onChange(file);
								}}
							/>
							<Icon fontSize="large" color="action">
								cloud_upload
							</Icon>
						</label>
					)}
				/>
				{image && !previewImage && (
					<img src={`${BASE_URL}${image} `} style={{ width: '100px', height: '100px' }} alt="Not found" />
				)}

				<div>
					<img style={{ width: '100px', height: '100px' }} src={previewImage} alt="Not found" />
				</div>
			</div>
			<Typography>Icon</Typography>
			<div className="flex justify-center sm:justify-start flex-wrap -mx-16">
				<Controller
					name="icon"
					control={control}
					render={({ field: { onChange, value } }) => (
						<label
							htmlFor="button-file"
							className={clsx(
								//classes.productImageUpload,
								'flex items-center justify-center relative w-128 h-128 rounded-16 mx-12 mb-24 overflow-hidden cursor-pointer shadow hover:shadow-lg'
							)}
						>
							<input
								accept="image/*"
								className="hidden"
								id="button-file"
								type="file"
								onChange={async e => {
									const reader = new FileReader();
									reader.onload = () => {
										if (reader.readyState === 2) {
											setPreviewIconImage(reader.result);
										}
									};
									reader.readAsDataURL(e.target.files[0]);

									const file = e.target.files[0];
									onChange(file);
								}}
							/>
							<Icon fontSize="large" color="action">
								cloud_upload
							</Icon>
						</label>
					)}
				/>
				{iconImage && !previewIconImage && (
					<img src={`${BASE_URL}${iconImage} `} style={{ width: '100px', height: '100px' }} alt="Not found" />
				)}

				<div style={{ width: '100px', height: '100px' }}>
					<img src={previewIconImage} alt="Not found" />
				</div>
			</div>
		</div>
	);
};

export default CategoryForm;
