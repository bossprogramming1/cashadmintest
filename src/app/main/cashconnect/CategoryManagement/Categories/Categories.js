import FusePageCarded from '@fuse/core/FusePageCarded';
import { CATEGORY_LIST } from 'app/constant/permission/permission';
import { getUserPermissions } from 'app/store/dataSlice';
import withReducer from 'app/store/withReducer';
import React, { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import PagenotFound from '../../Pagenotfound/PagenotFound';
import reducer from '../store/index';
import CategoriesHeader from './CategoriesHeader';
import CategoriesTable from './CategoriesTable';

const Categories = () => {
	const UserPermissions = useSelector(state => state.data.UserPermissions);
	useEffect(() => {
		dispatch(getUserPermissions());
	}, []);
	const dispatch = useDispatch();
	return (
		<FusePageCarded
			classes={{
				content: 'flex',
				contentCard: 'overflow-hidden',
				header: 'min-h-74 h-64'
			}}
			header={UserPermissions.includes(CATEGORY_LIST) && <CategoriesHeader />}
			content={UserPermissions.includes(CATEGORY_LIST) ? <CategoriesTable /> : <PagenotFound />}
			innerScroll
		/>
	);
};

export default withReducer('categoriesManagement', reducer)(Categories);
