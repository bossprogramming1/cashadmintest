import FuseScrollbars from '@fuse/core/FuseScrollbars';
import _ from '@lodash';
import Checkbox from '@material-ui/core/Checkbox';
import { makeStyles } from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TablePagination from '@material-ui/core/TablePagination';
import TableRow from '@material-ui/core/TableRow';
import DeleteIcon from '@material-ui/icons/Delete';
import DoneOutlineIcon from '@material-ui/icons/DoneOutline';
import EditIcon from '@material-ui/icons/Edit';
import Pagination from '@material-ui/lab/Pagination';
import React, { useEffect, useRef, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { withRouter } from 'react-router-dom';
import { Tooltip } from '@material-ui/core';
import { BASE_URL, SEARCH_CATEGORY } from '../../../../constant/constants';
import { getCategories, selectCategories } from '../store/categoriesSlice';
import CategoriesTableHead from './CategoriesTableHead';

const useStyles = makeStyles(theme => ({
	root: {
		display: 'flex',
		justifyContent: 'space-between',
		flexWrap: 'wrap',
		'& > *': {
			marginTop: theme.spacing(2),
			marginBottom: theme.spacing(3)
		}
	}
}));

const CategoriesTable = props => {
	const classes = useStyles();
	const dispatch = useDispatch();
	const [selected, setSelected] = useState([]);
	const [order, setOrder] = useState({ direction: 'asc', id: null });
	const categories = useSelector(selectCategories);
	const [data, setData] = useState(categories);
	const [loading, setLoading] = useState(true);
	const [page, setPage] = useState(0);
	const [rowsPerPage, setRowsPerPage] = useState(30);
	const [pageAndSize, setPageAndSize] = useState({ page: 1, size: 30 });
	const user_role = localStorage.getItem('user_role');

	const totalPages = sessionStorage.getItem('total_categories_pages');
	const totalElements = sessionStorage.getItem('total_categories_elements');

	const searchText = useSelector(({ categoriesManagement }) => categoriesManagement.categories.searchText);
	const [searchCategory, setSearchCategory] = useState([]);
	let serialNumber = 1;
	const parentIdRef = useRef(null);
	useEffect(() => {
		dispatch(getCategories(pageAndSize)).then(() => setLoading(false));
	}, [dispatch]);

	//search category
	useEffect(() => {
		searchText !== '' && getSearchCategory();
	}, [searchText]);

	const getSearchCategory = () => {
		fetch(`${SEARCH_CATEGORY}?name=${searchText}`)
			.then(response => response.json())
			.then(searchedCategoriesData => {
				setSearchCategory(searchedCategoriesData.categories);
			});
	};

	function handleSelectAllClick(event) {
		if (event.target.checked) {
			setSelected(categories.map(n => n.id));
			return;
		}
		setSelected([]);
	}

	function handleRequestSort(event, property) {
		const id = property;
		let direction = 'desc';

		if (order.id === property && order.direction === 'desc') {
			direction = 'asc';
		}

		setOrder({
			direction,
			id
		});
	}
	function handleDeselect() {
		setSelected([]);
	}

	function handleCheck(event, id) {
		const selectedIndex = selected.indexOf(id);
		let newSelected = [];

		if (selectedIndex === -1) {
			newSelected = newSelected.concat(selected, id);
		} else if (selectedIndex === 0) {
			newSelected = newSelected.concat(selected.slice(1));
		} else if (selectedIndex === selected.length - 1) {
			newSelected = newSelected.concat(selected.slice(0, -1));
		} else if (selectedIndex > 0) {
			newSelected = newSelected.concat(selected.slice(0, selectedIndex), selected.slice(selectedIndex + 1));
		}

		setSelected(newSelected);
	}

	//pagination
	const handlePagination = (e, handlePage) => {
		setPageAndSize({ ...pageAndSize, page: handlePage });
		setPage(handlePage - 1);
		dispatch(getCategories({ ...pageAndSize, page: handlePage }));
	};

	function handleChangePage(event, value) {
		setPage(value);
		setPageAndSize({ ...pageAndSize, page: value + 1 });
		dispatch(getCategories({ ...pageAndSize, page: value + 1 }));
	}

	function handleChangeRowsPerPage(event) {
		setRowsPerPage(event.target.value);
		setPageAndSize({ ...pageAndSize, size: event.target.value });
		dispatch(getCategories({ ...pageAndSize, size: event.target.value }));
	}

	//handleUpdateCategory
	function handleUpdateCategory(item, event) {
		localStorage.removeItem('deleteCategory');
		localStorage.setItem('updateCategory', event);
		props.history.push(`/apps/categories-management/${item.id}/${item?.name}`);
	}

	//handleDeletedCategory
	function handleDeleteCategory(item, event) {
		localStorage.removeItem('updateCategory');
		localStorage.setItem('deleteCategory', event);
		props.history.push(`/apps/categories-management/${item.id}/${item?.name}`);
	}

	const parentCat = n => {
		parentIdRef.current = n.parent;
		const parent = [];
		let parentstr = '';
		for (let i = 0; i < categories.length; i++) {
			parent.push(`${n.parent ? categories.find(category => category.id === parentIdRef.current)?.name : null}`);
			parentIdRef.current = n.parent ? categories.find(cate => cate.id === parentIdRef.current)?.parentId : null;
			parentIdRef.current ? null : (i = categories.length);
		}
		parent.reverse();

		for (let i = 0; i < parent.length; i++) {
			parentstr += `${i !== 0 ? '>>' : ''}${parent[i]} `;
		}

		return n.parent ? `${parentstr}>> ${n?.name} ` : `${n?.name} `;
	};

	return (
		<div className="w-full flex flex-col">
			<FuseScrollbars className="flex-grow overflow-x-auto">
				<Table stickyHeader className="min-w-xl" aria-labelledby="tableTitle">
					<CategoriesTableHead
						selectedCategoryIds={selected}
						order={order}
						onSelectAllClick={handleSelectAllClick}
						onRequestSort={handleRequestSort}
						rowCount={categories.length}
						onMenuItemClick={handleDeselect}
					/>

					<TableBody>
						{_.orderBy(
							searchText !== '' && searchCategory ? searchCategory : categories,
							[
								o => {
									switch (order.id) {
										case 'categories': {
											return o.categories[0];
										}
										default: {
											return o[order.id];
										}
									}
								}
							],
							[order.direction]
						).map(n => {
							const isSelected = selected.indexOf(n.id) !== -1;
							return (
								<TableRow
									//className="h-52 cursor-pointer"
									hover
									role="checkbox"
									aria-checked={isSelected}
									tabIndex={-1}
									key={n.id}
									selected={isSelected}
								>
									<TableCell className="whitespace-nowrap w-40 md:w-64 text-center" padding="none">
										<Checkbox
											checked={isSelected}
											onClick={event => event.stopPropagation()}
											onChange={event => handleCheck(event, n.id)}
										/>
									</TableCell>

									<TableCell className="whitespace-nowrap p-4 md:p-16" component="th" scope="row">
										{pageAndSize.page * pageAndSize.size - pageAndSize.size + serialNumber++}
									</TableCell>

									<TableCell
										whitespace-nowrap
										className="w-52 px-4 md:px-0"
										component="th"
										scope="row"
										padding="none"
									>
										{n.length > 0 && n.featuredImageId ? (
											<img
												className="w-full block rounded"
												src={_.find(n?.image, { id: n.featuredImageId }).url}
												alt={n?.name}
											/>
										) : (
											<img
												style={{ borderRadius: '50%' }}
												className="w-full block rounded"
												src={`${BASE_URL}${n?.image} `}
												alt={n?.name}
											/>
										)}
									</TableCell>

									<TableCell className="whitespace-nowrap p-4 md:p-16" component="th" scope="row">
										{n?.name}
									</TableCell>

									<TableCell className="whitespace-nowrap p-4 md:p-16" component="th" scope="row">
										{n.description}
									</TableCell>

									<TableCell className="whitespace-nowrap p-4 md:p-16" component="th" scope="row">
										{n.is_active ? <DoneOutlineIcon style={{ color: 'green' }} /> : null}
									</TableCell>

									<TableCell className="whitespace-nowrap p-4 md:p-16" component="th" scope="row">
										{n.parent && parentCat(n)}
									</TableCell>

									<TableCell className="whitespace-nowrap p-4 md:p-16" component="th" scope="row">
										<div>
											<Tooltip title="Edit" placement="top">
												<EditIcon
													onClick={event => handleUpdateCategory(n, 'updateCategory')}
													className="h-52 cursor-pointer"
													style={{ color: 'green' }}
												/>
											</Tooltip>
											<Tooltip title="Delete" placement="top">
												<DeleteIcon
													onClick={event => handleDeleteCategory(n, 'deleteCategory')}
													className="h-52 cursor-pointer"
													style={{
														color: 'red',
														visibility:
															user_role === 'ADMIN' || user_role === 'admin'
																? 'visible'
																: 'hidden'
													}}
												/>
											</Tooltip>
										</div>
									</TableCell>
								</TableRow>
							);
						})}
					</TableBody>
				</Table>
			</FuseScrollbars>

			<div className={classes.root} id="pagiContainer">
				<Pagination
					count={totalPages}
					page={page + 1}
					defaultPage={1}
					color="primary"
					showFirstButton
					showLastButton
					variant="outlined"
					shape="rounded"
					onChange={handlePagination}
				/>

				<TablePagination
					className="flex-shrink-0 border-t-1"
					rowsPerPageOptions={[30, 50, 100]}
					component="div"
					count={totalElements}
					rowsPerPage={rowsPerPage}
					page={page}
					backIconButtonProps={{
						'aria-label': 'Previous Page'
					}}
					nextIconButtonProps={{
						'aria-label': 'Next Page'
					}}
					onChangePage={handleChangePage}
					onChangeRowsPerPage={handleChangeRowsPerPage}
				/>
			</div>
		</div>
	);
};

export default withRouter(CategoriesTable);
