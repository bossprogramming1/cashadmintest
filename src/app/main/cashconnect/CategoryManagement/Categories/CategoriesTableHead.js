import Checkbox from '@material-ui/core/Checkbox';
import Icon from '@material-ui/core/Icon';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import Menu from '@material-ui/core/Menu';
import MenuItem from '@material-ui/core/MenuItem';
import MenuList from '@material-ui/core/MenuList';
import { makeStyles } from '@material-ui/core/styles';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import TableSortLabel from '@material-ui/core/TableSortLabel';
import Tooltip from '@material-ui/core/Tooltip';
import clsx from 'clsx';
import React, { useState } from 'react';
import { useDispatch } from 'react-redux';

const rows = [
	{
		id: 'sl_no',
		align: 'left',
		disablePadding: true,
		label: 'SL_NO',
		sort: true
	},
	{
		id: 'sl_no',
		align: 'left',
		disablePadding: true,
		label: 'Image',
		sort: true
	},
	{
		id: 'name',
		align: 'left',
		disablePadding: true,
		label: 'Name',
		sort: true
	},
	{
		id: 'description',
		align: 'left',
		disablePadding: true,
		label: 'Description',
		sort: true
	},
	{
		id: 'active',
		align: 'left',
		disablePadding: true,
		label: 'Active',
		sort: true
	},
	{
		id: 'parent_category',
		align: 'left',
		disablePadding: true,
		label: 'Parent Category',
		sort: true
	},
	{
		id: 'action',
		align: 'left',
		disablePadding: true,
		label: 'Action',
		sort: true
	}
];

const useStyles = makeStyles(theme => ({
	actionsButtonWrapper: {
		background: theme.palette.background.paper
	}
}));

const CategoriesTableHead = props => {
	const classes = useStyles();
	const dispatch = useDispatch();

	const { selectedCategoryIds } = props;
	const numSelected = selectedCategoryIds.length;

	const [selectedCategoriesMenu, setSelectedCategoriesMenu] = useState(null);

	//handler
	const createSortHandler = property => event => {
		props.onRequestSort(event, property);
	};
	function openSelectedCategoriesMenu(event) {
		setSelectedCategoriesMenu(event.currentTarget);
	}
	function closeSelectedCategoriesMenu() {
		setSelectedCategoriesMenu(null);
	}

	return (
		<TableHead>
			<TableRow className="h-50">
				<TableCell padding="none" className="w-40 md:w-64 text-center z-99">
					<Checkbox
						indeterminate={numSelected > 0 && numSelected < props.rowCount}
						checked={props.rowCount !== 0 && numSelected === props.rowCount}
						onChange={props.onSelectAllClick}
					/>

					<div
						className={clsx(
							'flex items-center justify-center absolute w-64 top-0 ltr:left-0 rtl:right-0 mx-56 h-64 z-10 border-b-1'
						)}
					>
						<Menu
							id="selectedProductsMenu"
							anchorEl={selectedCategoriesMenu}
							open={Boolean(selectedCategoriesMenu)}
							onClose={closeSelectedCategoriesMenu}
						>
							<MenuList>
								<MenuItem
									onClick={() => {
										//dispatch(removeEmployees(selectedCategoryIds));
										props.onMenuItemClick();
										closeSelectedCategoriesMenu();
									}}
								>
									<ListItemIcon className="min-w-40">
										<Icon>delete</Icon>
									</ListItemIcon>
									<ListItemText primary="Remove" />
								</MenuItem>
							</MenuList>
						</Menu>
					</div>
				</TableCell>
				{rows.map(row => {
					return (
						<TableCell
							className="p-4 md:p-16"
							key={row.id}
							align={row.align}
							padding={row.disablePadding ? 'none' : 'default'}
							//sortDirection={props.order.id === row.id ? props.order.direction : false}
						>
							{row.sort && (
								<Tooltip
									title={row?.label}
									placement={row.align === 'right' ? 'bottom-end' : 'bottom-start'}
									enterDelay={300}
								>
									<TableSortLabel
										active={props.order.id === row.id}
										direction={props.order.direction}
										onClick={createSortHandler(row.id)}
										className="font-semibold"
									>
										{row.label}
									</TableSortLabel>
								</Tooltip>
							)}
						</TableCell>
					);
				}, this)}
			</TableRow>
		</TableHead>
	);
};

export default CategoriesTableHead;
