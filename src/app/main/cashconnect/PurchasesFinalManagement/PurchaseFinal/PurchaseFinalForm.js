/* eslint-disable react/jsx-no-duplicate-props */
import Backdrop from '@material-ui/core/Backdrop';
import Button from '@material-ui/core/Button';
import Fade from '@material-ui/core/Fade';
import Grid from '@material-ui/core/Grid';
import Modal from '@material-ui/core/Modal';
import Paper from '@material-ui/core/Paper';
import { makeStyles, styled, useTheme } from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import TextField from '@material-ui/core/TextField';
import Typography from '@material-ui/core/Typography';
import DeleteIcon from '@material-ui/icons/Delete';
import DoneOutlineIcon from '@material-ui/icons/DoneOutline';
import EditIcon from '@material-ui/icons/Edit';
import ShoppingCartIcon from '@material-ui/icons/ShoppingCart';
import { Autocomplete } from '@material-ui/lab';
import axios from 'axios';
import React, { useEffect, useState } from 'react';
import { Controller, useFormContext } from 'react-hook-form';
import { useDispatch, useSelector } from 'react-redux';
import CircularProgress from '@mui/material/CircularProgress';
import { useHistory, useParams } from 'react-router-dom';
import {
	getBranches,
	getPaymentMathods,
	getProducts,
	getUsers,
	getVendors,
	getAllColor,
	getAllSize
} from 'app/store/dataSlice';
import moment from 'moment';
import {
	BASE_URL,
	DELETE_PURCHASE_FINAL_ITEM,
	GET_INVENTORY_BY_PRODUCT_ID,
	GET_PURCHASE_FINAL_WITH_ITEMS,
	GET_STOCK_INVENTORY_BY_PRODUCT_ID,
	PAY_AND_CONFIRM_PURCHASE_FINAL,
	UPDATE_PURCHASE_FINAL_ITEM
} from '../../../../constant/constants';
import { savePurchaseFinal, updatePurchaseFinal } from '../store/purchaseFinalSlice';

const useStyles = makeStyles(theme => ({
	hidden: {
		display: 'none'
	},
	tblContainer: {
		borderRadius: '0px'
	},
	table: {
		minWidth: 600
	},
	tableHead: {
		backgroundColor: theme.palette.primary[500]
	},
	dateOfSaleContainer: {
		display: 'flex',
		flexWrap: 'nowrap',
		justifyContent: 'space-between',
		minWidth: '250px'
	},
	dateOfSale: {
		width: '115px',
		background: theme.palette.background.paper,
		'& > div': {
			'&:before': {
				borderBottom: '0px solid black !important'
			}
		},
		'& input': {
			boxSizing: 'inherit',
			marginLeft: '5px',
			marginTop: '4px'
		}
	},
	paytypeLabel: {
		'&& > label': {
			fontSize: '16px'
		}
	},
	paidAmount: {
		'&& > p': {
			marginTop: '-27px'
		}
	},
	textField: {
		textAlign: 'center',
		paddingLeft: '22px'
	},
	input: {
		'&::-webkit-outer-spin-button, &::-webkit-inner-spin-button': {
			'-webkit-appearance': 'none',
			margin: 0
		},
		'-moz-appearance': 'textfield' // Firefox
	},
	modal: {
		display: 'flex',
		alignItems: 'center',
		justifyContent: 'center'
	},
	paper: {
		backgroundColor: theme.palette.background.paper,
		border: `2px solid ${theme.palette.primary[800]}`,
		boxShadow: theme.shadows[5],
		padding: theme.spacing(2, 4, 3)
	}
}));

function PurchaseFinalForm(props) {
	const [purchaseFinallId, setPurchaseFinalid] = useState(null);
	const purchaseFinlId = useSelector(({ purchaseFinalsManagement }) => purchaseFinalsManagement?.purchaseFinal?.id);
	const [loading, setLoading] = useState(false);
	const [total, setTotal] = useState('0.00');
	const [grossTotal, setGrossTotal] = useState('0.00');
	const [returnPlusDue, setReturnPlusDue] = useState('0.0');
	const [bonusPlusDiscount, setBonusPlusDiscount] = useState('0.00');
	const [productListRow, setProductListRow] = useState([]);
	const [editableRowIds, setEditableRowIds] = useState({});
	const [getReturnDue, setGetReturnDue] = useState(false);
	const [doUpdate, setdoUpdate] = useState(false);
	const [openCardOption, setOpenCardOption] = useState(false);
	const [getOrder, setGetOrder] = useState(false);
	const [priceError, setPriceError] = useState('');
	const [paymentSuccss, setPaymentSuccess] = useState('');
	const [alert, setAlert] = useState(false);
	const [purchaseFinalIdFromResponse, setPurchaseFinalIdFromResponse] = useState(0);
	const user_role = localStorage.getItem('user_role');
	const [duplicatedProductMessage, setDuplicatedProductMessage] = useState('');
	const userID = localStorage.getItem('UserID');
	const user = useSelector(({ auth }) => auth.user);
	const purchaseFinal = useSelector(({ purchaseFinalsManagement }) => purchaseFinalsManagement.purchaseFinal);
	const dispatch = useDispatch();
	const branchs = useSelector(state => state.data.branches);
	const vendors = useSelector(state => state.data.vendors);
	const products = useSelector(state => state.data.products);
	const payTypes = useSelector(state => state.data.paymentMathods);
	const sizes = useSelector(state => state.data.sizes);
	const colors = useSelector(state => state.data.colors);

	let serialNumber = 1;
	const dateObj = new Date();
	const currentDate = moment().format('YYYY-MM-DD');
	const classes = useStyles(props);
	const methods = useFormContext();
	const history = useHistory();
	const { control, formState, reset, getValues, watch, setError, setValue } = methods;
	const formValues = getValues();
	const dateOfSale = formValues.purchaseFinal_date;
	const { errors } = formState;
	const branchId = watch('branch');
	const vendorId = watch('vendor');
	const purchaseFinalDate = watch('purchaseFinal_date');
	const purchaseDate = watch('purchase_date');
	const productId = watch('product');
	const quantity = Number(watch('quantity'));
	const unitPrice = watch('unit_price');
	const purchasePrice = watch('purchase_price');
	const availableQuantity = watch('availableQuantity');
	const vendorInvoiceNo = watch('vendor_invoice_no');
	const descriptionText = watch('description');
	const routeParams = useParams();
	const { purchaseFinalId } = routeParams;
	useEffect(() => {
		if (purchaseFinalId && purchaseFinalId !== 'new') {
			setPurchaseFinalIdFromResponse(purchaseFinalId);
		}
	}, [purchaseFinalId]);

	const Theme = useTheme();
	const PrimaryBg = Theme.palette.primary[500];
	const PrimaryDark = Theme.palette.primary[700];
	const PrimaryLight = Theme.palette.primary[400];
	const PaperColor = Theme.palette.background.paper;
	const primaryBlack = Theme.palette.primary.dark;
	const handleDelete = localStorage.getItem('orderEvent');
	const authTOKEN = {
		headers: {
			'Content-type': 'application/json',
			Authorization: localStorage.getItem('jwt_access_token')
		}
	};

	useEffect(() => {
		setValue('purchaseFinal_date', purchaseDate?.slice(0, 10));
		dispatch(getBranches());
		dispatch(getUsers());
		dispatch(getVendors());
		dispatch(getProducts());
		dispatch(getAllSize());
		dispatch(getAllColor());

		dispatch(getPaymentMathods());
		reset({ ...formValues, purchaseFinal_date: currentDate, quantity: 1, availableQuantity: 1 });
	}, []);

	useEffect(() => {
		setTimeout(() => {
			setAlert(false);
		}, 6000);
	});
	//set updated item for update view
	useEffect(() => {
		purchaseFinlId && setUpdatedItem();
	}, [purchaseFinlId]);

	//Update Purchase Final
	useEffect(() => {
		if (doUpdate && routeParams.purchaseFinalId !== 'new' && handleDelete !== 'Delete') {
			dispatch(updatePurchaseFinal(getValues()));
		}
		setdoUpdate(false);
	}, [doUpdate]);

	//setReturnDue
	useEffect(() => {
		if (getReturnDue) {
			const newPaidAmount = watch('paidAmount') || 0;
			const returnDue = grossTotal - Number(newPaidAmount).toFixed(2);

			setReturnPlusDue(Number(returnDue).toFixed(2));
		}

		setGetReturnDue(false);
	}, [getReturnDue]);

	const handleAddPurchase = () => {
		setDuplicatedProductMessage('');
		const purchasePriceToFloat = parseFloat(purchasePrice);
		const unitPriceToFloat = parseFloat(unitPrice);
		if (purchasePriceToFloat > unitPriceToFloat) {
			setPriceError("Purchase price can't be greater than sales price");
		} else {
			if (branchId === null) {
				setError('branch', {
					type: 'manual',
					message: 'Branch is required'
				});
			}
			if (vendorId === null) {
				setError('vendor', {
					type: 'manual',
					message: 'Vendor is required'
				});
			}
			if (vendorInvoiceNo === '') {
				setError('vendor_invoice_no', {
					type: 'manual',
					message: 'Vendor invoice is required'
				});
			}
			if (handleDelete !== 'Delete' && branchId !== null && vendorId !== null && vendorInvoiceNo !== null) {
				setPriceError('');
				const findProductName = products.find(data => data.id === productId);
				const duplicateProduct = productListRow.find(data => data?.productdatails === findProductName?.name);
				// if (duplicateProduct) {
				// 	setDuplicatedProductMessage('Product is already added');
				// } else {
				dispatch(
					savePurchaseFinal({
						purchase_final: purchaseFinalIdFromResponse || '',
						is_new_purchase: localStorage.getItem('is_new_purchase') ? 'true' : 'false',
						branch: branchId,
						vendor: vendorId,
						purchase_date: purchaseFinalDate?.slice(0, 10),
						product: productId,
						quantity,
						purchase_price: watch('purchase_price'),
						size: watch('size') || null,
						color: watch('color') || null,
						unit_price: unitPrice,
						vendor_invoice_no: vendorInvoiceNo,
						description: descriptionText
					})
				).then(action => {
					localStorage.removeItem('is_new_purchase');
					if (action?.payload?.purchase_final?.id) {
						reset({
							...formValues,
							availableQuantity: 0,
							product: 0,
							quantity: 0,
							unit_price: 0,
							purchase_price: 0,
							price: 0
						});

						setPurchaseFinalIdFromResponse(action.payload.purchase_final.id);
						setTotal(action.payload?.purchase_final?.net_amount);
						const totalGross =
							Number(action.payload?.purchase_final?.net_amount).toFixed(2) -
							Number(bonusPlusDiscount).toFixed(2);
						setGrossTotal(totalGross.toFixed(2));
						setReturnPlusDue((totalGross.toFixed(2) - (watch('paidAmount') || 0)).toFixed(2));

						axios
							.get(`${GET_PURCHASE_FINAL_WITH_ITEMS}${action.payload?.purchase_final?.id}`, authTOKEN)
							.then(response => {
								const rowList = [];
								response?.data?.purchase_final?.purchase_final_items?.map(data => {
									rowList.push({
										id: data.id,
										image: data?.product?.thumbnail,
										productdatails: data.product?.name,
										quantity: data.quantity,
										size: data?.size || null,
										color: data?.color || null,
										unit_price: data.unit_price,
										totalPrice: data.subtotal
									});
									return null;
								});
								setProductListRow(rowList);
							});
						setPurchaseFinalid(action.payload?.purchase_final?.id);
					}
				});
				// }
			}
		}
	};

	// //set product item list
	// useEffect(() => {
	// 	let newEdiableRowIds = {};
	// 	productListRow.map(data => {
	// 		newEdiableRowIds = { ...newEdiableRowIds, [data.id]: false };
	// 		return null;
	// 	});
	// 	setEditableRowIds(newEdiableRowIds);
	// }, [productListRow]);

	//set Updated Item
	function setUpdatedItem() {
		axios.get(`${GET_PURCHASE_FINAL_WITH_ITEMS}${purchaseFinallId || purchaseFinlId}`, authTOKEN).then(res => {
			const rowList = [];
			let totalPrice = 0;
			res.data.purchase_final.purchase_final_items.map(data => {
				rowList.push({
					id: data?.id,
					image: data?.product?.thumbnail,
					productdatails: data?.product?.name,
					quantity: data?.quantity,
					size: data?.size || null,
					color: data?.color || null,
					unit_price: data?.unit_price,
					totalPrice: data?.subtotal
				});
				totalPrice += Number(data?.subtotal);
				return null;
			});

			setTotal(totalPrice.toFixed(2));
			const totalGrossAmount = totalPrice.toFixed(2) - Number(bonusPlusDiscount).toFixed(2);
			setGrossTotal(totalGrossAmount.toFixed(2));
			setReturnPlusDue(Number(totalGrossAmount.toFixed(2) - (watch('paidAmount') || 0)).toFixed(2));
			setProductListRow(rowList);
		});
	}

	//update product item
	function updateProductItem(item) {
		const productitemPrice = watch(`purchase_price${item.id}`);
		const productitemQuantity = watch(`unit_quantity${item.id}`);
		const colorId = watch(`color${item.id}`);
		const sizeId = watch(`size${item.id}`);

		const data = {
			quantity: productitemQuantity,
			unit_price: productitemPrice,
			size: sizeId || null,
			color: colorId || null
		};

		axios
			.put(`${UPDATE_PURCHASE_FINAL_ITEM}${purchaseFinallId || purchaseFinlId}/${item.id}`, data, authTOKEN)
			.then(res => {
				setUpdatedItem();
			});
	}

	//remove product item
	function removeProductItem(itemId) {
		axios
			.delete(`${DELETE_PURCHASE_FINAL_ITEM}${purchaseFinallId || purchaseFinlId}/${itemId}`, authTOKEN)
			.then(() => {
				setUpdatedItem();
			});
	}

	//confirmOrder
	const confirmPurchaseFinal = () => {
		setLoading(true);

		const confirmData = {
			pay_amount: watch('paidAmount'),
			payment_method: watch('payment_method'),
			branch: watch('branch'),
			vendor: watch('vendor'),
			vendor_invoice_no: watch('vendor_invoice_no'),
			description: watch('description')
		};

		if (confirmData.pay_amount && confirmData.payment_method) {
			axios
				.post(`${PAY_AND_CONFIRM_PURCHASE_FINAL}${purchaseFinallId || purchaseFinlId}`, confirmData, authTOKEN)
				.then(res => {
					setPaymentSuccess('Payment Successfull');
					setAlert(true);
					localStorage.setItem('paymentSuccessfullAlert', 'paymentSuccessfullAlert');
					history.push('/apps/purchasefinal-management/purchase-final');
				})
				.finally(() => {
					setLoading(false);
				});
		} else {
			if (!confirmData.payment_method) {
				setLoading(false);

				setError('payment_method', {
					type: 'manual',
					message: 'Select A payment Mathod'
				});
			}
			if (!confirmData.pay_amount) {
				setLoading(false);

				setError('paidAmount', {
					type: 'manual',
					message: 'Enter A Valid Amount'
				});
			}
		}
	};

	//card option close
	const handleCloseCardModal = () => {
		setOpenCardOption(false);
	};

	const cellStyle = { border: `1px solid ${Theme.palette.grey[600]}`, fontWeight: 'bold', color: primaryBlack };

	return (
		<>
			<Grid container spacing={1} className="mb-16">
				<Grid item xs={12}>
					<Div pd="2" h="40px">
						<Text fs="20px" color="dark">
							Purchase Final
						</Text>
						{/* <Btn>New Sale</Btn> */}
					</Div>
				</Grid>
				<Grid item xs={12}>
					<Div bg={PrimaryBg} h="2" justCont="center">
						<Text fs="17px">Customer Payment</Text>
					</Div>
				</Grid>

				{/* left... */}
				<Grid item xs={12} md={5} style={{ paddingLeft: '30px', marginTop: '20px' }}>
					<div>
						<Controller
							name={purchaseFinalId === 'new' ? 'created_by' : 'updated_by'}
							control={control}
							defaultValue={userID}
							render={({ field }) => {
								return (
									<TextField
										{...field}
										size="small"
										className={classes.hidden}
										label="created by"
										id="created_by"
										error={false}
										required
										variant="outlined"
										fullWidth
									/>
								);
							}}
						/>

						<Controller
							name="branch"
							control={control}
							render={({ field: { onChange, value, name } }) => (
								<Autocomplete
									className="mt-8 mb-16"
									freeSolo
									value={value ? branchs.find(data => data.id === value) : null}
									options={branchs}
									getOptionLabel={option => `${option?.name}`}
									onChange={(event, newValue) => {
										onChange(newValue?.id);
										setdoUpdate(true);
									}}
									renderInput={params => (
										<TextField
											{...params}
											size="small"
											placeholder="Select Branch"
											label="Branch"
											error={!!errors.branch}
											required
											helperText={errors?.branch?.message}
											variant="outlined"
											autoFocus
											InputLabelProps={{
												shrink: true
											}}
										/>
									)}
								/>
							)}
						/>

						<Controller
							name="vendor"
							control={control}
							render={({ field: { onChange, value, name } }) => (
								<Autocomplete
									className="mt-8 mb-16"
									freeSolo
									value={value ? vendors.find(data => data.id === value) : null}
									options={vendors}
									getOptionLabel={option =>
										`${option.first_name} ${option.last_name} [${option.email}]`
									}
									onChange={(event, newValue) => {
										onChange(newValue?.id);
										setdoUpdate(true);
									}}
									renderInput={params => (
										<TextField
											{...params}
											size="small"
											placeholder="Select Vendor"
											label="Vendor"
											error={!!errors.vendor}
											required
											helperText={errors?.vendor?.message}
											variant="outlined"
											InputLabelProps={{
												shrink: true
											}}
										/>
									)}
								/>
							)}
						/>
						<Controller
							name="vendor_invoice_no"
							control={control}
							render={({ field }) => (
								<TextField
									{...field}
									className="mt-8 mb-16"
									error={!!errors.vendor_invoice_no}
									required
									onBlur={event => {
										reset({
											...formValues,
											vendor_invoice_no: event.target.value
										});
										setdoUpdate(true);
									}}
									//value={item.item_name}
									InputLabelProps={{ shrink: true }}
									helperText={errors?.vendor_invoice_no?.message}
									id="vendor_invoice_no"
									placeholder="Vendor Invoice"
									label="Vendor Invoice No"
									type="text"
									variant="outlined"
									fullWidth
								/>
							)}
						/>

						<Controller
							name="description"
							control={control}
							render={({ field }) => (
								<TextField
									{...field}
									className="mt-8 mb-16"
									error={!!errors.description}
									required
									onBlur={event => {
										reset({
											...formValues,
											description: event.target.value
										});
										setdoUpdate(true);
									}}
									//value={item.item_name}
									InputLabelProps={{ shrink: true }}
									helperText={errors?.description?.message}
									id="description"
									placeholder="Description"
									label="Description"
									type="text"
									variant="outlined"
									fullWidth
								/>
							)}
						/>
					</div>
				</Grid>

				<Grid item xs={12} md={2} />

				{/* right... */}
				<Grid item xs={12} md={5}>
					<Grid container spacing={1}>
						<Grid item xs={6}>
							<Div h="1" top={PrimaryDark} justCont="flex-start" pd="2" border>
								<Text color="dark" bold>
									Total:
								</Text>
							</Div>
						</Grid>
						<Grid item xs={6}>
							<Div h="1" top={PrimaryDark} justCont="flex-start" pd="2" border>
								<Text color="dark" bold>
									{total}
								</Text>
							</Div>
						</Grid>
					</Grid>

					{/* <Grid container spacing={1}>
						<Grid item xs={6}>
							<Div h="1" top={PrimaryDark} justCont="flex-start" pd="2" border>
								<Text color="dark" bold>
									Bonus Point + Discount :
								</Text>
							</Div>
						</Grid>
						<Grid item xs={6}>
							<Div h="1" top={PrimaryDark} justCont="flex-start" pd="2" border>
								<Text color="dark" bold>
									{bonusPlusDiscount}
								</Text>
							</Div>
						</Grid>
					</Grid> */}

					<Grid container spacing={1}>
						<Grid item xs={6}>
							<Div h="1" top={PrimaryDark} justCont="flex-start" pd="2" border>
								<Text color="dark" bold>
									Gross Total :
								</Text>
							</Div>
						</Grid>
						<Grid item xs={6}>
							<Div h="1" top={PrimaryDark} justCont="flex-start" pd="2" border>
								<Text color="dark" bold>
									{grossTotal}
								</Text>
							</Div>
						</Grid>
					</Grid>

					<Grid container spacing={1}>
						<Grid item xs={12}>
							<Controller
								name="payment_method"
								control={control}
								render={({ field: { onChange, value, name } }) => (
									<Autocomplete
										className="mt-8 mb-16"
										freeSolo
										value={value ? payTypes.find(data => data.id === value) : null}
										options={payTypes}
										getOptionLabel={option => `${option?.name}`}
										onChange={(event, newValue) => {
											onChange(newValue?.id);
											newValue?.name === 'Card' && setOpenCardOption(true);
											purchaseFinal?.payment_method && setdoUpdate(true);
										}}
										renderInput={params => (
											<TextField
												{...params}
												className={classes.paytypeLabel}
												size="small"
												placeholder="Select Payment Pathod"
												label="Pay Type"
												error={!!errors.payment_method}
												helperText={errors?.payment_method?.message}
												variant="outlined"
												required
												InputLabelProps={{
													shrink: true
												}}
											/>
										)}
									/>
								)}
							/>
						</Grid>
						{alert && paymentSuccss ? <p style={{ color: 'green' }}>{`${paymentSuccss}`}</p> : null}
					</Grid>

					<Modal
						aria-labelledby="transition-modal-title"
						aria-describedby="transition-modal-description"
						className={classes.modal}
						open={openCardOption}
						onClose={handleCloseCardModal}
						closeAfterTransition
						BackdropComponent={Backdrop}
						BackdropProps={{
							timeout: 500
						}}
					>
						<Fade in={openCardOption}>
							<div className={classes.paper}>
								<h2 id="transition-modal-title">Card Payment Option</h2>
								<p id="transition-modal-description" />
							</div>
						</Fade>
					</Modal>

					<Grid container spacing={1}>
						<Grid item xs={6}>
							<Div h="1" top={PrimaryDark} justCont="flex-start" pd="2" border>
								<Text color="dark" bold>
									Paid Amount :
								</Text>
							</Div>
						</Grid>
						<Grid item xs={6}>
							<Div h="1" top={PrimaryDark} justCont="flex-start" pd="2" border>
								<Input>
									<Controller
										name="paidAmount"
										control={control}
										render={({ field }) => {
											return (
												<TextField
													{...field}
													className={classes.paidAmount}
													onChange={(event, newValue) => {
														field.onChange(event);
														setGetReturnDue(true);
													}}
													type="number"
													size="small"
													error={!!errors.paidAmount}
													helperText={errors?.paidAmount?.message}
													id="paidAmount"
													required
													variant="outlined"
													InputLabelProps={field.value && { shrink: true }}
													fullWidth
												/>
											);
										}}
									/>
								</Input>
							</Div>
						</Grid>
					</Grid>

					<Grid container spacing={1}>
						<Grid item xs={6}>
							<Div h="1" top={PrimaryDark} justCont="flex-start" pd="2" border>
								<Text color="dark" bold>
									Return(+)/Due() :
								</Text>
							</Div>
						</Grid>
						<Grid item xs={6}>
							<Div h="1" top={PrimaryDark} justCont="flex-start" pd="2" border>
								<Text color="dark" bold>
									{returnPlusDue}
								</Text>
							</Div>
						</Grid>
					</Grid>

					<Grid container spacing={1}>
						<Grid item xs={12}>
							<Div
								h="1"
								top={PrimaryDark}
								style={{ flexDirection: 'column', justifyContent: 'center', alignItems: 'center' }}
								pd="2"
								border
							>
								{loading ? (
									<CircularProgress size={24} />
								) : (
									<Btn
										style={{ backgroundColor: 'green', color: 'white' }}
										h="30px"
										onClick={confirmPurchaseFinal}
										disabled={loading}
									>
										Bill Payment
									</Btn>
								)}
							</Div>
						</Grid>
					</Grid>

					{/* <Grid container spacing={1}>
						<Grid item xs={6}>
							<Div h="1" top={PrimaryDark} justCont="flex-start" pd="2" border>
								<Btn h="30px">Summery Report</Btn>
							</Div>
						</Grid>
						<Grid item xs={6}>
							<Div h="1" top={PrimaryDark} justCont="flex-start" pd="2" border>
								<Btn h="30px">New Purchase</Btn>
							</Div>
						</Grid>
					</Grid> */}
				</Grid>

				{/* buttom... */}
				<Grid item xs={12}>
					<Div bg={PrimaryBg} h="2" pd="2">
						<Text>Purchase Inventory</Text>
						<Text>{user.role || user.displayName}</Text>
						{priceError && <p style={{ color: 'red' }}>{`${priceError}`}</p>}
						<div className={classes.dateOfSaleContainer}>
							<Controller
								name="purchaseFinal_date"
								control={control}
								render={({ field }) => {
									return (
										<TextField
											{...field}
											onChange={(event, newValue) => {
												field.onChange(event);
												setdoUpdate(true);
											}}
											value={field.value?.slice(0, 10)}
											color="primary"
											className={classes.dateOfSale}
											id="purchaseFinal_date"
											type="date"
											defaultValue={currentDate}
											fullWidth
											InputLabelProps={{
												shrink: true
											}}
										/>
									);
								}}
							/>
							<Text style={{ marginLeft: '10px' }}>
								Date Of Purchase Final : {dateOfSale ? dateOfSale.slice(0, 10) : currentDate}
							</Text>
						</div>
					</Div>
				</Grid>

				<Grid item xs={1}>
					<Div bg={PrimaryBg} h="1" justCont="center">
						<Text>Bar Code</Text>
					</Div>
				</Grid>

				<Grid item xs={2}>
					<Div bg={PrimaryBg} h="1" justCont="center">
						<Text>Product Name</Text>
					</Div>
				</Grid>

				<Grid item xs={1}>
					<Div bg={PrimaryBg} h="1" justCont="center">
						<Text style={{ fontSize: '10px', fontWeight: 600 }}>Purchase Qty</Text>
					</Div>
				</Grid>

				<Grid item xs={1}>
					<Div bg={PrimaryBg} h="1" justCont="center">
						<Text style={{ fontSize: '10px', fontWeight: 600 }}>Available Qty</Text>
					</Div>
				</Grid>
				<Grid item xs={3}>
					<Div bg={PrimaryBg} h="1" justCont="center">
						<Text>Color & Size</Text>
					</Div>
				</Grid>

				<Grid item xs={2}>
					<Div bg={PrimaryBg} h="1" justCont="center">
						<Text>Sales price</Text>
					</Div>
				</Grid>

				<Grid item xs={2}>
					<Div bg={PrimaryBg} h="1" justCont="left">
						<Text>Purchase Price</Text>
					</Div>
				</Grid>

				<Grid item xs={1}>
					<Div bg={PrimaryBg} h="3" justCont="center">
						<Input bg={PaperColor} size="large" h="35px">
							<Controller
								name="barcode"
								control={control}
								render={({ field }) => {
									return (
										<TextField
											{...field}
											size="small"
											onBlur={event => {
												const productItem = products.find(
													data => data.barcode === event.target.value
												);

												fetch(`${GET_STOCK_INVENTORY_BY_PRODUCT_ID}${productItem?.id}`)
													.then(res => res.json())
													.then(data => {
														const quantityy = watch('quantity');
														reset({
															...formValues,
															unit_price: productItem?.unit_price,
															product: productItem?.id,
															quantity: quantityy || 1,
															availableQuantity: data.in_stock < 0 ? 1 : data.in_stock,
															vendor_invoice_no: vendorInvoiceNo || ''
														});
													});

												setGetOrder(true);
											}}
											id="barcode"
											required
											variant="outlined"
											InputLabelProps={field.value && { shrink: true }}
											fullWidth
										/>
									);
								}}
							/>
						</Input>
					</Div>
				</Grid>

				<Grid item xs={2}>
					<Div bg={PrimaryBg} h="3" justCont="center">
						<Input bg={PaperColor} size="large">
							<Controller
								name="product"
								control={control}
								render={({ field: { onChange, value, name } }) => (
									<Autocomplete
										freeSolo
										value={value ? products.find(data => data.id === value) : null}
										options={products}
										getOptionLabel={option => `${option?.name}`}
										onChange={(event, newValue) => {
											onChange(newValue?.id);

											fetch(`${GET_INVENTORY_BY_PRODUCT_ID}${newValue?.id}`)
												.then(res => res.json())
												.then(data => {
													const quantitty = watch('quantity');
													reset({
														...formValues,
														unit_price: newValue?.unit_price,
														product: newValue?.id,
														quantity: quantitty || 1,
														availableQuantity: data.in_stock < 0 ? 1 : data.in_stock,
														vendor_invoice_no: vendorInvoiceNo || ''
													});
												});
										}}
										renderInput={params => (
											<TextField
												{...params}
												size="small"
												placeholder="Select Products"
												error={!!errors.product}
												required
												helperText={errors?.product?.message}
												variant="outlined"
												InputLabelProps={{
													shrink: true
												}}
											/>
										)}
									/>
								)}
							/>
						</Input>
					</Div>
				</Grid>

				<Grid item xs={1}>
					<Div bg={PrimaryBg} h="3" justCont="center">
						<Input bg={PaperColor} size="small">
							<Controller
								name="quantity"
								control={control}
								render={({ field }) => {
									return (
										<TextField
											{...field}
											inputProps={{ min: 0, style: { textAlign: 'center' } }} // the change is here
											InputProps={{ classes: { input: classes.input } }} // Apply custom styles to hide the spinner
											type="number"
											size="small"
											error={!!errors.quantity}
											helperText={errors?.quantity?.message}
											id="quantity"
											required
											variant="outlined"
											InputLabelProps={field.value && { shrink: true }}
											fullWidth
										/>
									);
								}}
							/>
						</Input>
					</Div>
				</Grid>

				<Grid item xs={1}>
					<Div bg={PrimaryBg} h="3" justCont="center">
						<Typography
							style={{
								width: '60px',
								height: '40px',

								backgroundColor: 'white',
								textAlign: 'center',
								margin: '7px',
								color: 'black',
								paddingTop: '10px'
							}}
						>
							{availableQuantity}
						</Typography>
					</Div>
				</Grid>
				<Grid item xs={2}>
					<Div bg={PrimaryBg} h="3" justCont="center">
						<Input bg={PaperColor} size="large">
							<Controller
								name="color"
								control={control}
								render={({ field: { onChange, value, name } }) => (
									<Autocomplete
										freeSolo
										value={value ? colors.find(data => data.id === value) : null}
										options={colors}
										getOptionLabel={option => `${option?.name}`}
										onChange={(event, newValue) => {
											onChange(newValue?.id);
										}}
										renderInput={params => (
											<TextField
												{...params}
												size="small"
												placeholder="Select Color"
												error={!!errors.color}
												required
												helperText={errors?.color?.message}
												variant="outlined"
												InputLabelProps={{
													shrink: true
												}}
											/>
										)}
									/>
								)}
							/>
						</Input>
					</Div>
				</Grid>
				<Grid item xs={1}>
					<Div bg={PrimaryBg} h="3" justCont="center">
						<Input bg={PaperColor} size="large">
							<Controller
								name="size"
								control={control}
								render={({ field: { onChange, value, name } }) => (
									<Autocomplete
										freeSolo
										value={value ? sizes.find(data => data.id === value) : null}
										options={sizes}
										getOptionLabel={option => `${option?.name}`}
										onChange={(event, newValue) => {
											onChange(newValue?.id);
										}}
										renderInput={params => (
											<TextField
												{...params}
												size="small"
												placeholder="Size"
												error={!!errors.size}
												required
												helperText={errors?.size?.message}
												variant="outlined"
												InputLabelProps={{
													shrink: true
												}}
											/>
										)}
									/>
								)}
							/>
						</Input>
					</Div>
				</Grid>

				<Grid item xs={2}>
					<Div bg={PrimaryBg} h="3" justCont="center">
						<Typography
							style={{
								width: '144px',
								height: '40px',
								textAlign: 'center',
								backgroundColor: 'white',
								margin: '7px',
								color: 'black',
								paddingTop: '10px'
							}}
						>
							{unitPrice}
						</Typography>
					</Div>
				</Grid>
				<Grid item xs={2}>
					<Div bg={PrimaryBg} h="3" justCont="center">
						<Input bg={PaperColor} size="large">
							<Controller
								name="purchase_price"
								control={control}
								render={({ field }) => {
									return (
										<TextField
											{...field}
											size="small"
											error={!!errors?.purchase_price}
											helperText={errors?.purchase_price?.message}
											id="purchase_price"
											required
											variant="outlined"
											InputLabelProps={field.value && { shrink: true }}
											fullWidth
										/>
									);
								}}
							/>
						</Input>
						<div style={{ backgroundColor: PaperColor }}>
							<Btn h="30px" bg="white" size="small" onClick={handleAddPurchase}>
								<ShoppingCartIcon />+
							</Btn>
						</div>
						{/* onClick={handleAddPurchase} */}
					</Div>
				</Grid>

				<Grid item xs={12}>
					<Div
						top={PrimaryDark}
						left={PrimaryDark}
						justCont="center"
						pd="2"
						h="fit-content"
						alignItems="space-between"
						border
					>
						<Grid container spacing={1}>
							{duplicatedProductMessage !== '' && (
								<Typography style={{ color: 'red', textAlign: 'center' }}>
									{duplicatedProductMessage}
								</Typography>
							)}
							<Grid item xs={12}>
								<Div bg={PrimaryBg} h="2" justCont="center">
									<Text>Use of Produts of this Purchase</Text>
								</Div>
							</Grid>
							{/* product details */}
							<Grid item xs={12}>
								<Div h="fit-content" pd="2" minHeight="272px" alignItems="flex-start" border>
									<TableContainer component={Paper} className={classes.tblContainer}>
										<Table className={classes.table} aria-label="simple table">
											<TableHead className={classes.tableHead}>
												<TableRow>
													<TableCell style={{ color: PaperColor }}>No.</TableCell>
													<TableCell
														whitespace-nowrap
														style={{ color: PaperColor }}
														align="center"
													>
														IMG
													</TableCell>
													<TableCell
														whitespace-nowrap
														style={{ color: PaperColor }}
														align="center"
													>
														Product Details
													</TableCell>
													<TableCell
														whitespace-nowrap
														style={{ color: PaperColor }}
														align="center"
													>
														Color{' '}
													</TableCell>
													<TableCell
														whitespace-nowrap
														style={{ color: PaperColor }}
														align="center"
													>
														Size{' '}
													</TableCell>
													<TableCell
														whitespace-nowrap
														style={{ color: PaperColor }}
														align="center"
													>
														Quantity
													</TableCell>
													<TableCell
														whitespace-nowrap
														style={{ color: PaperColor }}
														align="center"
													>
														Price
													</TableCell>
													<TableCell
														whitespace-nowrap
														style={{ color: PaperColor }}
														align="center"
													>
														Total
													</TableCell>
													<TableCell
														whitespace-nowrap
														style={{ color: PaperColor }}
														align="center"
													>
														Action
													</TableCell>
												</TableRow>
											</TableHead>

											<TableBody>
												{productListRow?.map((row, idx) => {
													console.log('productListRow', productListRow);
													return (
														<TableRow key={row?.name}>
															<TableCell
																whitespace-nowrap
																style={cellStyle}
																component="th"
																scope="row"
															>
																{serialNumber++}
															</TableCell>
															<TableCell
																style={{
																	...cellStyle,
																	paddingTop: '0px',
																	paddingBottom: '0px'
																}}
																align="center"
															>
																<img
																	className="w-full block rounded"
																	style={{
																		borderRadius: '50%',
																		height: '50px',
																		width: '50px',
																		margin: 'auto'
																	}}
																	src={`${BASE_URL}${row.image}`}
																	alt="Not Found"
																/>
															</TableCell>
															<TableCell
																whitespace-nowrap
																style={cellStyle}
																align="center"
															>
																{row.productdatails}
															</TableCell>
															<TableCell
																whitespace-nowrap
																style={cellStyle}
																align="center"
															>
																{editableRowIds[row.id] ? (
																	<Controller
																		name={`color${row.id}`}
																		control={control}
																		render={({
																			field: { onChange, value, name }
																		}) => (
																			<Autocomplete
																				freeSolo
																				value={
																					value
																						? colors.find(
																								data =>
																									data.id === value
																						  )
																						: null
																				}
																				options={colors}
																				getOptionLabel={option =>
																					`${option?.name}`
																				}
																				onChange={(event, newValue) => {
																					onChange(newValue?.id);
																				}}
																				renderInput={params => (
																					<TextField
																						{...params}
																						size="small"
																						placeholder="Select Color"
																						id={`color${row.id}`}
																						error={!!errors.color}
																						required
																						helperText={
																							errors?.color?.message
																						}
																						variant="outlined"
																						InputLabelProps={{
																							shrink: true
																						}}
																					/>
																				)}
																			/>
																		)}
																	/>
																) : (
																	row.color?.name || null
																)}
															</TableCell>
															<TableCell
																whitespace-nowrap
																style={cellStyle}
																align="center"
															>
																{editableRowIds[row.id] ? (
																	<Controller
																		name={`size${row.id}`}
																		control={control}
																		render={({
																			field: { onChange, value, name }
																		}) => (
																			<Autocomplete
																				freeSolo
																				value={
																					value
																						? sizes.find(
																								data =>
																									data.id === value
																						  )
																						: null
																				}
																				options={sizes}
																				getOptionLabel={option =>
																					`${option?.name}`
																				}
																				onChange={(event, newValue) => {
																					onChange(newValue?.id);
																				}}
																				renderInput={params => (
																					<TextField
																						{...params}
																						size="small"
																						placeholder="Select Size"
																						id={`size${row.id}`}
																						error={!!errors.size}
																						required
																						helperText={
																							errors?.size?.message
																						}
																						variant="outlined"
																						InputLabelProps={{
																							shrink: true
																						}}
																					/>
																				)}
																			/>
																		)}
																	/>
																) : (
																	row.size?.name || null
																)}
															</TableCell>
															<TableCell
																style={{ ...cellStyle, width: '15%' }}
																align="center"
															>
																{editableRowIds[row.id] ? (
																	<Controller
																		name={`unit_quantity${row.id}`}
																		control={control}
																		render={({ field }) => {
																			return (
																				<TextField
																					{...field}
																					size="small"
																					error={!!errors.unit_quantity}
																					helperText={
																						errors?.product_guantity
																							?.message
																					}
																					id={`unit_quantity${row.id}`}
																					required
																					variant="outlined"
																					InputLabelProps={
																						field.value && { shrink: true }
																					}
																					fullWidth
																				/>
																			);
																		}}
																	/>
																) : (
																	row.quantity
																)}
															</TableCell>
															<TableCell
																style={{ ...cellStyle, width: '15%' }}
																align="center"
															>
																{editableRowIds[row.id] ? (
																	<Controller
																		name={`purchase_price${row.id}`}
																		control={control}
																		render={({ field }) => {
																			return (
																				<TextField
																					{...field}
																					size="small"
																					error={!!errors.purchase_price}
																					helperText={
																						errors?.purchase_price?.message
																					}
																					id={`purchase_price${row.id}`}
																					required
																					variant="outlined"
																					InputLabelProps={
																						field.value && { shrink: true }
																					}
																					fullWidth
																				/>
																			);
																		}}
																	/>
																) : (
																	row.unit_price
																)}
															</TableCell>
															<TableCell
																whitespace-nowrap
																style={cellStyle}
																align="center"
															>
																{row.totalPrice}
															</TableCell>
															<TableCell
																whitespace-nowrap
																style={cellStyle}
																align="center"
															>
																<div style={{ display: 'flex' }}>
																	{editableRowIds[row.id] ? (
																		<DoneOutlineIcon
																			style={{ color: 'green' }}
																			className="cursor-pointer"
																			onClick={e => {
																				setEditableRowIds({
																					...editableRowIds,
																					[row.id]: false
																				});
																				updateProductItem(row);
																			}}
																		/>
																	) : (
																		<EditIcon
																			style={{ color: 'green' }}
																			className="cursor-pointer"
																			onClick={e => {
																				setEditableRowIds({
																					...editableRowIds,
																					[row.id]: true
																				});

																				reset({
																					...formValues,
																					[`unit_quantity${row.id}`]:
																						row.quantity,
																					[`color${row.id}`]:
																						row?.color?.id || null,
																					[`size${row.id}`]:
																						row?.size?.id || null,
																					[`purchase_price${row.id}`]:
																						row.unit_price
																				});
																			}}
																		/>
																	)}{' '}
																	<DeleteIcon
																		style={{
																			color: 'red',
																			visibility:
																				user_role === 'ADMIN' ||
																				user_role === 'admin'
																					? 'visible'
																					: 'hidden'
																		}}
																		className="cursor-pointer"
																		onClick={() => removeProductItem(row.id)}
																	/>
																</div>
															</TableCell>
														</TableRow>
													);
												})}
											</TableBody>
										</Table>
									</TableContainer>
								</Div>
							</Grid>
						</Grid>
					</Div>

					<Div bg={PrimaryBg} h="1" />
				</Grid>

				{/* <Grid item xs={3}>
					<Div bg={PrimaryDark} h="2" justCont="center">
						<Text>{`Point & Discount Panel`}</Text>
					</Div>
					<Div bg={PrimaryLight} h="350px" justCont="center" pd="2" wrap>
						<Div>
							<Text>* % Discount </Text>
							<Input size="medium" bg={PaperColor}>
								<Controller
									name="discount"
									control={control}
									render={({ field }) => {
										return (
											<TextField
												{...field}
												size="small"
												// error={!!errors.discount}
												// helperText={errors?.discount?.message}
												id="discount"
												required
												variant="outlined"
												InputLabelProps={field.value && { shrink: true }}
												fullWidth
											/>
										);
									}}
								/>
							</Input>
						</Div>
						<Div>
							<Text>* Custom Dis </Text>
						</Div>
						<Div>
							<Text>Amount </Text>
							<Input size="medium" bg={PaperColor}>
								<Controller
									name="Amount"
									control={control}
									render={({ field }) => {
										return (
											<TextField
												{...field}
												size="small"
												// error={!!errors.Amount}
												// helperText={errors?.Amount?.message}
												id="Amount"
												required
												variant="outlined"
												InputLabelProps={field.value && { shrink: true }}
												fullWidth
											/>
										);
									}}
								/>
							</Input>
						</Div>
						<Div>
							<Text>Bonus Point : </Text>
							<Input size="medium" bg={PaperColor}>
								<Controller
									name="bonusPoint"
									control={control}
									render={({ field }) => {
										return (
											<TextField
												{...field}
												size="small"
												// error={!!errors.bonusPoint}
												// helperText={errors?.bonusPoint?.message}
												id="bonusPoint"
												required
												variant="outlined"
												InputLabelProps={field.value && { shrink: true }}
												fullWidth
											/>
										);
									}}
								/>
							</Input>
						</Div>
						<Div>
							<Text>Red Point : </Text>
							<Input size="medium" bg={PaperColor}>
								<Controller
									name="redPoint"
									control={control}
									render={({ field }) => {
										return (
											<TextField
												{...field}
												size="small"
												// error={!!errors.redPoint}
												// helperText={errors?.redPoint?.message}
												id="redPoint"
												required
												variant="outlined"
												InputLabelProps={field.value && { shrink: true }}
												fullWidth
											/>
										);
									}}
								/>
							</Input>
						</Div>
						<Div justCont="center">
							<Btn h="20" bg={PaperColor}>
								Re Calculate
							</Btn>
						</Div>
					</Div>
				</Grid> */}
			</Grid>
		</>
	);
}

export default PurchaseFinalForm;

const btn = styled(({ color, ...other }) => <Button variant="outlined" {...other} />)({});
const Btn = styled(btn)(props => ({
	borderRadius: '3px',
	fontWeight: 'bold',
	borderWidth: '2px',
	height: props.h ? props.h : '40px',
	background: props.bg
}));

const Text = styled('b')(props => ({
	color: props.color === 'dark' ? 'black' : 'white',
	fontSize: props.fs ? props.fs : '15px',
	fontWeight: props.bold ? 'bold' : '500'
}));

const Input = styled('div')(props => ({
	background: props.bg,
	height: props.h ? props.h : '35px',
	width: props.size === 'large' ? '100%' : props.size === 'medium' ? '110px' : props.size === 'small' && '70px',
	margin: '10px',
	borderRadius: '2px',
	fontWeight: 'bold',
	color: props.color,
	hover: 'white'
}));

const Div = styled('div')(props => ({
	background: props.bg,
	height: props.h === '1' ? '25px' : props.h === '2' ? '35px' : props.h === 3 ? '50px' : props.h && props.h,
	width: props.w ? props.w : '100%',
	display: 'flex',
	justifyContent: props.justCont ? props.justCont : 'space-between',
	alignItems: props.alignItems ? props.alignItems : 'center',
	padding: props.pd === '1' ? '10px' : props.pd === '2' ? '20px' : props.pd && props.pd,
	flexWrap: props.wrap && 'wrap',
	border: props.border && '2px solid gray',
	borderTop: `4px solid ${props.top}`,
	borderLeft: `4px solid ${props.left}`,
	minHeight: props.minHeight
}));

// const PaidAmountTextField = styled(TextField)(props => ({

// }));
