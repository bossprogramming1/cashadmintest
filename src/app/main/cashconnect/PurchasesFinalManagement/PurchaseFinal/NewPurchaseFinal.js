import FusePageCarded from '@fuse/core/FusePageCarded';
import { useDeepCompareEffect } from '@fuse/hooks';
import { yupResolver } from '@hookform/resolvers/yup';
import Button from '@material-ui/core/Button';
import Link from '@material-ui/core/Link';
import Typography from '@material-ui/core/Typography';
import {
	PURCHASE_FINAL_CREATE,
	PURCHASE_FINAL_DELETE,
	PURCHASE_FINAL_DETAIL,
	PURCHASE_FINAL_UPDATE
} from 'app/constant/permission/permission';
import { getUserPermissions } from 'app/store/dataSlice';
import withReducer from 'app/store/withReducer';
import { motion } from 'framer-motion';
import React, { useEffect, useState } from 'react';
import { FormProvider, useForm } from 'react-hook-form';
import { useDispatch, useSelector } from 'react-redux';
import { useParams } from 'react-router-dom';
import * as yup from 'yup';
import PagenotFound from '../../Pagenotfound/PagenotFound';
import reducer from '../store/index';
import { getPurchaseFinal, newOrder, resetOrder } from '../store/purchaseFinalSlice';
import NewPurchaseFinalHeader from './NewPurchaseFinalHeader';
import PurchaseFinalForm from './PurchaseFinalForm';

/**
 * Form Validation Schema
 */
const schema = yup.object().shape({
	branch: yup.string().required('Branch is required'),
	vendor: yup.string().required('Vendor is required'),
	vendor_invoice_no: yup.string().required('Vendor invoice is required'),
	product: yup.string().required('Product is required'),
	payType: yup.string().required('Select A Payment Mathod'),
	paidAmount: yup.string().required('Enter A Valid Amount')
});

const NewPurchaseFinal = () => {
	const dispatch = useDispatch();
	const purchaseFinal = useSelector(({ purchaseFinalsManagement }) => purchaseFinalsManagement.purchaseFinal);
	const [noOrder, setNoOrder] = useState(false);
	const methods = useForm({
		mode: 'onChange',
		defaultValues: {},
		resolver: yupResolver(schema)
	});
	const routeParams = useParams();
	const UserPermissions = useSelector(state => state.data.UserPermissions);
	useEffect(() => {
		dispatch(getUserPermissions());
	}, []);
	const { reset, watch, control, onChange, formState } = methods;
	const form = watch();
	const purchaseFinalDate = watch('purchaseFinal_date');
	const quantity = watch('quantity');

	useDeepCompareEffect(() => {
		function updateOrderState() {
			const { purchaseFinalId } = routeParams;

			if (purchaseFinalId === 'new') {
				localStorage.removeItem('event');
				/**
				 * Create New User data
				 */
				dispatch(newOrder());
			} else {
				/**
				 * Get User data
				 */

				dispatch(getPurchaseFinal(purchaseFinalId)).then(action => {
					/**
					 * If the requested product is not exist show message
					 */
					if (!action.payload) {
						setNoOrder(true);
					}
				});
			}
		}

		updateOrderState();
	}, [dispatch, routeParams]);

	// useEffect(() => { }, []);

	useEffect(() => {
		if (!purchaseFinal) {
			return;
		}
		/**
		 * Reset the form on purchase final state changes
		 */
		reset({
			...purchaseFinal,
			purchaseFinal_date: purchaseFinal?.purchase_date || purchaseFinalDate,
			quantity,
			vendor: purchaseFinal?.vendor?.id || null,
			branch: purchaseFinal?.branch?.id || null,
			paidAmount: purchaseFinal?.pay_amount || null,
			payment_method: purchaseFinal?.payment_method?.id || null
		});
	}, [purchaseFinal, reset]);

	useEffect(() => {
		return () => {
			/**
			 * Reset Order on component unload
			 */
			dispatch(resetOrder());
			setNoOrder(false);
		};
	}, [dispatch]);

	/**
	 * Show Message if the requested products is not exists
	 */
	if (noOrder) {
		return (
			<motion.div
				initial={{ opacity: 0 }}
				animate={{ opacity: 1, transition: { delay: 0.1 } }}
				className="flex flex-col flex-1 items-center justify-center h-full"
			>
				<Typography color="textSecondary" variant="h5">
					There is no such Purchase Final!
				</Typography>
				<Button
					className="mt-24"
					component={Link}
					variant="outlined"
					to="/apps/e-commerce/products"
					color="inherit"
				>
					Go to Purchase Final Page
				</Button>
			</motion.div>
		);
	}

	return (
		<FormProvider {...methods}>
			{UserPermissions.includes(PURCHASE_FINAL_CREATE) ||
			UserPermissions.includes(PURCHASE_FINAL_UPDATE) ||
			UserPermissions.includes(PURCHASE_FINAL_DELETE) ||
			UserPermissions.includes(PURCHASE_FINAL_DETAIL) ? (
				<FusePageCarded
					classes={{
						toolbar: 'p-0',
						header: 'min-h-74 h-64'
					}}
					header={<NewPurchaseFinalHeader />}
					content={
						<div className="pt-16 sm:pt-24 max-w-3xl">
							<PurchaseFinalForm />
						</div>
					}
					innerScroll
				/>
			) : (
				<PagenotFound />
			)}
		</FormProvider>
	);
};
export default withReducer('purchaseFinalsManagement', reducer)(NewPurchaseFinal);
