import { createAsyncThunk, createSlice } from '@reduxjs/toolkit';
import {
	CREATE_PURCHASE_FINAL_MANUALLY,
	DELETE_PURCHASE_FINAL,
	GET_PURCHASE_FINAL,
	UPDATE_PURCHASE_FINAL
} from 'app/constant/constants';
import axios from 'axios';

export const getPurchaseFinal = createAsyncThunk(
	'orderManagement/order/getOrder',
	async (params, { rejectWithValue }) => {
		const authTOKEN = {
			headers: {
				'Content-type': 'application/json',
				Authorization: localStorage.getItem('jwt_access_token')
			}
		};

		try {
			const response = await axios.get(`${GET_PURCHASE_FINAL}${params}`, authTOKEN);
			const data = await response.data;
			return data === undefined ? null : data;
		} catch (err) {
			return rejectWithValue(params);
		}
	}
);

export const removePurchaseFinal = createAsyncThunk(
	'purchaseFinalManagement/purchaseFinal/removePurchaseFinal',
	async (val, { dispatch, getState }) => {
		const authTOKEN = {
			headers: {
				'Content-type': 'application/json',
				Authorization: localStorage.getItem('jwt_access_token')
			}
		};
		const response = await axios.delete(`${DELETE_PURCHASE_FINAL}${val}`, authTOKEN);
	}
);

export const updatePurchaseFinal = createAsyncThunk(
	'purchaseFinalManagement/purchaseFinal/updatePurchaseFinal',
	async (purchaseFinalData, { dispatch, getState }) => {
		const { purchaseFinal } = getState().purchaseFinalsManagement;
		const data = {
			seller_id: purchaseFinalData?.seller,
			branch_id: purchaseFinalData?.branch_id,
			seller_invoice_no: purchaseFinalData?.seller_invoice_no,
			purchase_date: purchaseFinalData?.purchase_date.slice(0, 10)
		
		};

		const authTOKEN = {
			headers: {
				'Content-type': 'application/json',
				Authorization: localStorage.getItem('jwt_access_token')
			}
		};
		if (data.seller_id && data.branch_id && data.seller_invoice_no && data.purchase_date) {
			const response = await axios.put(`${UPDATE_PURCHASE_FINAL}${purchaseFinal.id}`, data, authTOKEN);
		}
	}
);



export const savePurchaseFinal = createAsyncThunk(
	'purchaseFinalManagement/purchaseFinal/savePurchaseFinal',
	async (purchaseFinalData, { dispatch, getState }) => {

		const authTOKEN = {
			headers: {
				'Content-type': 'application/json',
				Authorization: localStorage.getItem('jwt_access_token')
			}
		};
		try {
			const response = axios.post(`${CREATE_PURCHASE_FINAL_MANUALLY}`, purchaseFinalData, authTOKEN);

			const data = await response;

			return data.data;
		} catch (error) {

			return error.response.data;
		}
	}
);

const purchaseFinalSlice = createSlice({
	name: 'purchaseFinalManagement/purchaseFinal',
	initialState: null,
	reducers: {
		resetOrder: () => null,
		newOrder: {
			reducer: (state, action) => action.payload,
			prepare: event => ({
				payload: {}
			})
		}
	},
	extraReducers: {
		[getPurchaseFinal.fulfilled]: (state, action) => action.payload,
		[removePurchaseFinal.fulfilled]: () => {
			localStorage.setItem('orderAlert', 'deleteOrder');
		},
		[updatePurchaseFinal.fulfilled]: () => {
			localStorage.setItem('orderAlert', 'updateOrder');
		},
		[savePurchaseFinal.fulfilled]: (state, action) => {
			localStorage.setItem('orderAlert', 'saveOrder');
		}
	}
});

export const { newOrder, resetOrder } = purchaseFinalSlice.actions;

export default purchaseFinalSlice.reducer;
