import { combineReducers } from '@reduxjs/toolkit';
import purchaseFinal from './purchaseFinalSlice';
import purchaseFinals from './purchaseFinalsSlice';

const reducer = combineReducers({
	purchaseFinal,
	purchaseFinals,
});

export default reducer;