import { createAsyncThunk, createEntityAdapter, createSlice } from '@reduxjs/toolkit';
import axios from 'axios';
import { DELETE_ORDER, GET_PURCHASE_FINALS } from '../../../../constant/constants';

export const getPurchaseFinals = createAsyncThunk(
	'purchaseFinalsManagement/purchaseFinals/getpurchaseFinals',
	async parameter => {
		const { page, size } = parameter;
		axios.defaults.headers.common['Content-type'] = 'application/json';
		axios.defaults.headers.common.Authorization = localStorage.getItem('jwt_access_token');

		const response = axios.get(GET_PURCHASE_FINALS, { params: { page, size } });
		const data = await response;
		sessionStorage.setItem('orders_total_elements', data.data.total_elements);
		sessionStorage.setItem('orders_total_pages', data.data.total_pages);
		delete axios.defaults.headers.common['Content-type'];
		delete axios.defaults.headers.common.Authorization;

		return data.data.purchase_finals;
	}
);

export const removeOrders = createAsyncThunk(
	'purchaseFinalsManagement/purchaseFinals/removepurchaseFinals',
	async (orderIds, { dispatch, getState }) => {
		const authTOKEN = {
			headers: {
				'Content-type': 'application/json',
				Authorization: localStorage.getItem('jwt_access_token')
			}
		};
		await axios.delete(`${DELETE_ORDER}`, { orderIds }, authTOKEN);

		return orderIds;
	}
);

const ordersAdapter = createEntityAdapter({});

export const { selectAll: selectPurchaseFinals, selectById: selectOrderById } = ordersAdapter.getSelectors(
	state => state.purchaseFinalsManagement.purchaseFinals
);

const purchaseFinalsSlice = createSlice({
	name: 'purchaseFinalsManagement/purchaseFinals',
	initialState: ordersAdapter.getInitialState({
		searchText: ''
	}),
	reducers: {
		setOrdersSearchText: {
			reducer: (state, action) => {
				state.searchText = action.payload;
			},
			prepare: event => ({ payload: event?.target?.value || '' })
		}
	},
	extraReducers: {
		[getPurchaseFinals.fulfilled]: ordersAdapter.setAll
	}
});

export const { setData, setOrdersSearchText } = purchaseFinalsSlice.actions;
export default purchaseFinalsSlice.reducer;
