import Button from '@material-ui/core/Button';
import Icon from '@material-ui/core/Icon';
import Input from '@material-ui/core/Input';
import Paper from '@material-ui/core/Paper';
import { makeStyles, ThemeProvider } from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';
import CancelIcon from '@material-ui/icons/Cancel';
import Alert from '@material-ui/lab/Alert';
import { PURCHASE_FINAL_CREATE } from 'app/constant/permission/permission';
import { getUserPermissions } from 'app/store/dataSlice';
import { selectMainTheme } from 'app/store/fuse/settingsSlice';
import { motion } from 'framer-motion';
import React, { useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { useHistory } from 'react-router';
import { Link } from 'react-router-dom';
import { setOrdersSearchText } from '../store/purchaseFinalsSlice';

const useStyles = makeStyles(theme => ({
	alert: props => ({
		width: '20%',
		minWidth: '250px',
		height: '35px',
		position: 'fixed',
		right: '30px',
		paddingTop: '0px',
		fontSize: '15px',
		borderRadius: '15px',
		transitionTimingFunction: 'ease-out',
		zIndex: props ? '1' : '-1',
		transition: props ? '0s' : '1s',
		opacity: props ? 1 : 0
	})
}));

const PurchaseFinalsHeader = () => {
	const [alerOpen, setAlertOpen] = useState(false);
	const [alertMessage, setAlertMessage] = useState('');
	const mainTheme = useSelector(selectMainTheme);
	const dispatch = useDispatch();
	const searchText = useSelector(
		({ purchaseFinalsManagement }) => purchaseFinalsManagement.purchaseFinals.searchText
	);

	const classes = useStyles(alerOpen);
	const UserPermissions = useSelector(state => state.data.UserPermissions);
	useEffect(() => {
		dispatch(getUserPermissions());
	}, []);
	useEffect(() => {
		const alert = localStorage.getItem('paymentSuccessfullAlert');
		const alertTwo = localStorage.getItem('orderAlert');
		if (alert === 'paymentSuccessfullAlert') {
			setAlertOpen(true);
			setAlertMessage('Payment Success...');
			localStorage.removeItem('paymentSuccessfullAlert');
		}
		if (alertTwo === 'updateOrder') {
			setAlertOpen(true);
			setAlertMessage('Update Success...');
			localStorage.removeItem('orderAlert');
		}
		if (alertTwo === 'deleteOrder') {
			setAlertOpen(true);
			setAlertMessage('Remove Success...');
			localStorage.removeItem('orderAlert');
		}
		if (alertTwo === 'saveOrder') {
			setAlertOpen(true);
			setAlertMessage('Save Success...');
			localStorage.removeItem('orderAlert');
		}

		setTimeout(() => {
			setAlertOpen(false);
		}, 3000);
	}, []);

	const history = useHistory();
	

	const enterClick = event => {
		if (event.key === 'Enter') {
			dispatch(setOrdersSearchText(event));
		}
	};

	return (
		<div className="flex flex-1 w-full items-center justify-between">
			<div className="flex items-center">
				<Icon
					component={motion.span}
					initial={{ scale: 0 }}
					animate={{ scale: 1, transition: { delay: 0.2 } }}
					className="text-24 md:text-32"
				>
					person
				</Icon>
				<Typography
					component={motion.span}
					initial={{ x: -10 }}
					animate={{ x: 0, transition: { delay: 0.2 } }}
					delay={300}
					className="hidden sm:flex text-16 md:text-24 mx-12 font-semibold"
				>
					Purchase Final
				</Typography>
			</div>

			<div className="flex flex-1 items-center justify-center px-12">
				<ThemeProvider theme={mainTheme}>
					<Paper
						component={motion.div}
						initial={{ y: -20, opacity: 0 }}
						animate={{ y: 0, opacity: 1, transition: { delay: 0.2 } }}
						className="flex items-center w-full max-w-512 px-8 py-4 rounded-16 shadow"
					>
						<Icon color="action">search</Icon>

						<Input
							placeholder="Search Customer"
							className="flex flex-1 mx-8"
							disableUnderline
							fullWidth
							//value={searchText}
							inputProps={{
								'aria-label': 'Search'
							}}
							onKeyDown={enterClick}
							// onChange={ev => dispatch(setOrdersSearchText(ev))}
						/>
					</Paper>
				</ThemeProvider>
			</div>
			<motion.div initial={{ opacity: 0, x: 20 }} animate={{ opacity: 1, x: 0, transition: { delay: 0.2 } }}>
				{UserPermissions.includes(PURCHASE_FINAL_CREATE) && (
					<Button
						component={Link}
						to="/apps/purchasefinal-management/new"
						className="whitespace-nowrap"
						variant="contained"
						color="secondary"
						onClick={() => localStorage.setItem('is_new_purchase', true)}
					>
						<span className="hidden sm:flex">Add New PurchaseFinal</span>
						<span className="flex sm:hidden">New</span>
					</Button>
				)}
			</motion.div>

			<Alert
				variant="filled"
				severity="success"
				className={classes.alert}
				action={
					<CancelIcon
						onClick={() => {
							setAlertOpen(false);
						}}
						style={{ marginTop: '8px' }}
					/>
				}
			>
				{alertMessage}
			</Alert>
		</div>
	);
};

export default PurchaseFinalsHeader;
