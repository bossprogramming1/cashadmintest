import FuseLoading from '@fuse/core/FuseLoading';
import FuseScrollbars from '@fuse/core/FuseScrollbars';
import _ from '@lodash';
import Checkbox from '@material-ui/core/Checkbox';
import { makeStyles } from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TablePagination from '@material-ui/core/TablePagination';
import TableRow from '@material-ui/core/TableRow';
import DeleteIcon from '@material-ui/icons/Delete';
import EditIcon from '@material-ui/icons/Edit';
import ReceiptIcon from '@material-ui/icons/Receipt';
import Pagination from '@material-ui/lab/Pagination';
import React, { useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { withRouter } from 'react-router-dom';
import history from '@history';
import { getBranches, getOrdersStatus, getUsers, getVendors } from 'app/store/dataSlice';
import { motion } from 'framer-motion';
import { SEARCH_PURCHASE_FINAL } from 'app/constant/constants';
import { Tooltip, Typography } from '@material-ui/core';
import clsx from 'clsx';
import { getPurchaseFinals, selectPurchaseFinals, setOrdersSearchText } from '../store/purchaseFinalsSlice';
import PurchaseFinalsTableHead from './PurchaseFinalsTableHead';

const useStyles = makeStyles(theme => ({
	root: {
		display: 'flex',
		justifyContent: 'space-between',
		flexWrap: 'wrap',
		'& > *': {
			marginTop: theme.spacing(2),
			marginBottom: theme.spacing(3)
		}
	}
}));

const PurchaseFinalsTable = props => {
	const branchs = useSelector(state => state.data.branches);
	const _ordersStatus = useSelector(state => state.data.ordersStatus);
	const users = useSelector(state => state.data.users);
	const vendors = useSelector(state => state.data.vendors);
	const user_role = localStorage.getItem('user_role');
	const dispatch = useDispatch();
	const purchaseFinals = useSelector(selectPurchaseFinals);
	const searchText = useSelector(
		({ purchaseFinalsManagement }) => purchaseFinalsManagement.purchaseFinals.searchText
	);
	const [searchPurchaseFinal, setSearchPurchaseFinal] = useState([]);
	const [loading, setLoading] = useState(true);
	const [selected, setSelected] = useState([]);
	const [data, setData] = useState(purchaseFinals);
	const [page, setPage] = useState(0);
	const [rowsPerPage, setRowsPerPage] = useState(30);
	const [order, setOrder] = useState({
		direction: 'asc',
		id: null
	});
	let serialNumber = 1;
	const [parameter, setParameter] = useState({ page: 1, size: 30 });
	const totalPages = sessionStorage.getItem('orders_total_pages');
	const totalElements = sessionStorage.getItem('orders_total_elements');
	const classes = useStyles();

	useEffect(() => {
		const event = '';
		dispatch(getBranches());
		dispatch(getOrdersStatus());
		dispatch(getUsers());
		dispatch(getVendors());
		dispatch(setOrdersSearchText(event));
	}, []);

	useEffect(() => {
		dispatch(getPurchaseFinals(parameter)).then(() => setLoading(false));
	}, [dispatch]);

	//search order
	useEffect(() => {
		searchText !== '' && getSearchPurchaseFinal();
	}, [searchText]);

	const getSearchPurchaseFinal = () => {
		fetch(`${SEARCH_PURCHASE_FINAL}?keyword=${searchText}`)
			.then(response => response.json())
			.then(searchedPurchaseFinalData => {
				setSearchPurchaseFinal(searchedPurchaseFinalData.purchase_finals);
			});
	};

	function handleRequestSort(orderEvent, property) {
		const id = property;
		let direction = 'desc';

		if (order.id === property && order.direction === 'desc') {
			direction = 'asc';
		}

		setOrder({
			direction,
			id
		});
	}

	function handleSelectAllClick(orderEvent) {
		if (orderEvent.target.checked) {
			setSelected(purchaseFinals.map(n => n.id));
			return;
		}
		setSelected([]);
	}

	function handleDeselect() {
		setSelected([]);
	}

	function handleUpdatePurchaseFinal(item) {
		localStorage.removeItem('orderEvent');

		props.history.push(`/apps/purchasefinal-management/${item.id}`);
	}
	function handleDeletePurchaseFinal(item, orderEvent) {
		localStorage.removeItem('orderEvent');
		localStorage.setItem('orderEvent', orderEvent);
		props.history.push(`/apps/purchasefinal-management/${item.id}`);
	}

	function handleCheck(orderEvent, id) {
		const selectedIndex = selected.indexOf(id);
		let newSelected = [];

		if (selectedIndex === -1) {
			newSelected = newSelected.concat(selected, id);
		} else if (selectedIndex === 0) {
			newSelected = newSelected.concat(selected.slice(1));
		} else if (selectedIndex === selected.length - 1) {
			newSelected = newSelected.concat(selected.slice(0, -1));
		} else if (selectedIndex > 0) {
			newSelected = newSelected.concat(selected.slice(0, selectedIndex), selected.slice(selectedIndex + 1));
		}

		setSelected(newSelected);
	}

	const handlePagination = (e, handlePage) => {
		setParameter({ ...parameter, page: handlePage });
		setPage(handlePage - 1);
		dispatch(getPurchaseFinals({ ...parameter, page: handlePage }));
	};

	function handleChangePage(orderEvent, value) {
		setPage(value);
		setParameter({ ...parameter, page: value + 1 });
		dispatch(getPurchaseFinals({ ...parameter, page: value + 1 }));
	}

	function handleChangeRowsPerPage(orderEvent) {
		setRowsPerPage(orderEvent.target.value);
		setParameter({ ...parameter, size: orderEvent.target.value });
		dispatch(getPurchaseFinals({ ...parameter, size: orderEvent.target.value }));
	}

	if (loading) {
		return <FuseLoading />;
	}

	if (purchaseFinals?.length === 0) {
		return (
			<motion.div
				initial={{ opasize: 0 }}
				animate={{ opasize: 1, transition: { delay: 0.1 } }}
				className="flex flex-1 items-center justify-center h-full"
			>
				<Typography size="textSecondary" variant="h5">
					There are no Purchage Final!
				</Typography>
			</motion.div>
		);
	}
	return (
		<div className="w-full flex flex-col">
			<FuseScrollbars className="flex-grow overflow-x-auto">
				<Table stickyHeader className="min-w-xl" aria-labelledby="tableTitle">
					<PurchaseFinalsTableHead
						selectedOrderIds={selected}
						order={order}
						onSelectAllClick={handleSelectAllClick}
						onRequestSort={handleRequestSort}
						rowCount={purchaseFinals.length}
						onMenuItemClick={handleDeselect}
					/>

					<TableBody>
						{_.orderBy(
							searchText !== '' && searchPurchaseFinal ? searchPurchaseFinal : purchaseFinals,
							[
								o => {
									switch (order.id) {
										case 'categories': {
											return o.categories[0];
										}
										default: {
											return o[order.id];
										}
									}
								}
							],
							[order.direction]
						).map(n => {
							const isSelected = selected.indexOf(n.id) !== -1;
							return (
								<TableRow
									hover
									role="checkbox"
									aria-checked={isSelected}
									tabIndex={-1}
									key={n.id}
									selected={isSelected}
								>
									<TableCell className="whitespace-nowrap w-40 md:w-64 text-center" padding="none">
										<Checkbox
											checked={isSelected}
											onClick={orderEvent => orderEvent.stopPropagation()}
											onChange={orderEvent => handleCheck(orderEvent, n.id)}
										/>
									</TableCell>

									<TableCell className="whitespace-nowrap w-40 md:w-64" component="th" scope="row">
										{parameter.page * parameter.size - parameter.size + serialNumber++}
									</TableCell>

									<TableCell className="whitespace-nowrap p-4 md:p-16" component="th" scope="row">
										{n.purchase_no}
									</TableCell>
									<TableCell className="whitespace-nowrap p-4 md:p-16" component="th" scope="row">
										{n?.vendor_invoice_no}
									</TableCell>

									<TableCell className="whitespace-nowrap p-4 md:p-16" component="th" scope="row">
										<div
											className={clsx(
												'inline text-12 font-semibold py-4 px-12 rounded-full truncate text-white',
												n?.is_paid === true ? 'text-green' : 'text-orange'
											)}
										>
											{n?.is_paid === true
												? 'Payment Success'
												: n.is_paid === false
												? 'Pending'
												: 'Unknown'}
										</div>
									</TableCell>

									<TableCell className="whitespace-nowrap p-4 md:p-16" component="th" scope="row">
										{n?.vendor?.email}
									</TableCell>

									<TableCell className="whitespace-nowrap p-4 md:p-16" component="th" scope="row">
										{n?.branch?.name}
									</TableCell>

									<TableCell
										style={{}}
										className="p-4 md:p-16 whitespace-nowrap"
										component="th"
										scope="row"
									>
										{n?.purchase_date?.slice(0, 10) /*moment(n.created_at).format("dd/mm/yyyy")*/}
									</TableCell>

									<TableCell className="whitespace-nowrap p-4 md:p-16" component="th" scope="row">
										{n.net_amount}
									</TableCell>

									{/* <TableCell 	className="w-52 px-4 md:px-4" component="th" scope="row">
											{n.length > 0 && n.featuredImageId ? (
												<img
													className="block rounded"
													style={{ borderRadius: '50%', height: '50px', width: '50px' }}
													src={_.find(n.image, { id: n.featuredImageId }).url}
													alt={n?.name}
												/>
											) : (
												<img
													className="w-full block rounded"
													style={{ borderRadius: '50%', height: '50px', width: '50px' }}
													src={`${BASE_URL}${n.image}`}
													alt={n?.name}
												/>
											)}
									</TableCell> */}

									<TableCell
										className="p-0 md:p-0"
										align="center"
										component="th"
										scope="row"
										style={{ minWidth: '80px' }}
									>
										<div>
											<Tooltip title="Edit" placement="top">
												<EditIcon
													onClick={orderEvent => handleUpdatePurchaseFinal(n)}
													className="h-52 cursor-pointer"
													style={{
														color: 'green',
														visibility: n?.is_paid === false ? 'visible' : 'hidden'
													}}
												/>
											</Tooltip>

											<Tooltip title="Invoice" placement="top">
												<ReceiptIcon
													onClick={() => {
														history.push(
															`/apps/report-management/purchage-final-reports/purchase-invoice/${n.id}`
														);
													}}
													className="h-52 cursor-pointer"
													style={{ color: 'orange' }}
												/>
											</Tooltip>
											<Tooltip title="Delete" placement="top">
												<DeleteIcon
													onClick={event => handleDeletePurchaseFinal(n, 'Delete')}
													className="h-52 cursor-pointer"
													style={{
														color: 'red',
														// display: n?.is_paid === false ? 'inline-block' : 'none',
														visibility:
															(user_role === 'ADMIN' || user_role === 'admin') &&
															n?.is_paid === false
																? 'visible'
																: 'hidden'
													}}
												/>
											</Tooltip>
										</div>
									</TableCell>
								</TableRow>
							);
						})}
					</TableBody>
				</Table>
			</FuseScrollbars>

			<div className={classes.root}>
				<Pagination
					count={totalPages}
					page={page + 1}
					defaultPage={1}
					color="primary"
					showFirstButton
					showLastButton
					variant="outlined"
					shape="rounded"
					onChange={handlePagination}
				/>

				<TablePagination
					className="flex-shrink-0 border-t-1"
					rowsPerPageOptions={[30, 50, 100]}
					component="div"
					count={totalElements}
					rowsPerPage={rowsPerPage}
					page={page}
					backIconButtonProps={{
						'aria-label': 'Previous Page'
					}}
					nextIconButtonProps={{
						'aria-label': 'Next Page'
					}}
					onChangePage={handleChangePage}
					onChangeRowsPerPage={handleChangeRowsPerPage}
				/>
			</div>
		</div>
	);
};

export default withRouter(PurchaseFinalsTable);
