import _ from '@lodash';
import TextField from '@material-ui/core/TextField';
import { Autocomplete } from '@material-ui/lab';
import React, { useEffect } from 'react';
import { Controller, useFormContext } from 'react-hook-form';
import { useDispatch, useSelector } from 'react-redux';
import { useHistory, useParams } from 'react-router-dom';
import { getCountries } from '../../../../store/dataSlice';
import { saveSize, updateSize } from '../store/sizeSlice';

function SizeForm(props) {
	const dispatch = useDispatch();
	const countries = useSelector(state => state.data.countries);
	const methods = useFormContext();
	const { control, formState, getValues } = methods;
	const { errors, isValid, dirtyFields } = formState;
	const routeParams = useParams();
	const history = useHistory();
	const handleDelete = localStorage.getItem('sizeEvent');

	useEffect(() => {
		dispatch(getCountries());
	}, []);

	function handleSaveSize() {
		dispatch(saveSize(getValues())).then(res => {
			if (res.payload) {
				localStorage.setItem('sizeAlert', 'saveSize');
				history.push('/apps/size-management/sizes');
			}
		});
	}

	function handleUpdateSize() {
		dispatch(updateSize(getValues())).then(res => {
			if (res.payload) {
				localStorage.setItem('sizeAlert', 'updateSize');
				history.push('/apps/size-management/sizes');
			}
		});
	}

	const handleSubmitOnKeyDownEnter = ev => {
		if (ev.key === 'Enter') {
			if (routeParams.sizeId === 'new' && !(_.isEmpty(dirtyFields) || !isValid)) {
				handleSaveSize();
			} else if (handleDelete !== 'Delete' && routeParams?.sizeName) {
				handleUpdateSize();
			}
		}
	};

	return (
		<div>
			<Controller
				name="name"
				control={control}
				render={({ field }) => {
					return (
						<TextField
							{...field}
							className="mt-8 mb-16"
							error={!!errors?.name}
							helperText={errors?.name?.message}
							label="Name"
							id="name"
							required
							variant="outlined"
							InputLabelProps={field.value && { shrink: true }}
							fullWidth
							onKeyDown={handleSubmitOnKeyDownEnter}
						/>
					);
				}}
			/>
		</div>
	);
}

export default SizeForm;
