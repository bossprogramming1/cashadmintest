import _ from '@lodash';
import Button from '@material-ui/core/Button';
import Icon from '@material-ui/core/Icon';
import { useTheme } from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';
import { motion } from 'framer-motion';
import React from 'react';
import { useFormContext } from 'react-hook-form';
import { useDispatch } from 'react-redux';
import { Link, useHistory, useParams } from 'react-router-dom';
import { removeSize, saveSize, updateSize } from '../store/sizeSlice';

const NewSizeHeader = () => {
	const dispatch = useDispatch();
	const methods = useFormContext();
	const { formState, watch, getValues } = methods;
	const { isValid, dirtyFields } = formState;
	const name = watch('name');
	const theme = useTheme();
	const history = useHistory();
	const routeParams = useParams();
	const handleDelete = localStorage.getItem('sizeEvent');

	function handleSaveSize() {
		dispatch(saveSize(getValues())).then(res => {
			if (res.payload) {
				localStorage.setItem('sizeAlert', 'saveSize');
				history.push('/apps/size-management/sizes');
			}
		});
	}

	function handleUpdateSize() {
		dispatch(updateSize(getValues())).then(res => {
			if (res.payload) {
				localStorage.setItem('sizeAlert', 'updateSize');
				history.push('/apps/size-management/sizes');
			}
		});
	}

	function handleRemoveSize() {
		dispatch(removeSize(getValues())).then(res => {
			if (res.payload) {
				localStorage.setItem('sizeAlert', 'deleteSize');
				localStorage.removeItem('sizeEvent');
				history.push('/apps/size-management/sizes');
			}
		});
	}

	function handleCancel() {
		history.push('/apps/size-management/sizes');
	}

	return (
		<div className="flex flex-1 w-full items-center justify-between">
			<div className="flex flex-col items-start max-w-full min-w-0">
				<motion.div initial={{ x: 20, opasize: 0 }} animate={{ x: 0, opasize: 1, transition: { delay: 0.3 } }}>
					<Typography
						className="flex items-center sm:mb-2"
						component={Link}
						role="button"
						to="/apps/size-management/sizes"
						size="inherit"
					>
						<Icon className="text-20">{theme.direction === 'ltr' ? 'arrow_back' : 'arrow_forward'}</Icon>
						<span className="hidden sm:flex mx-4 font-medium">Sizes</span>
					</Typography>
				</motion.div>

				<div className="flex items-center max-w-full">
					<motion.div
						className="hidden sm:flex"
						initial={{ scale: 0 }}
						animate={{ scale: 1, transition: { delay: 0.3 } }}
					/>
					<div className="flex flex-col min-w-0 mx-8 sm:mc-16">
						<motion.div initial={{ x: -20 }} animate={{ x: 0, transition: { delay: 0.3 } }}>
							<Typography className="text-16 sm:text-20 truncate font-semibold">
								{name || 'Create New Size'}
							</Typography>
							<Typography variant="caption" className="font-medium">
								Sizess Detail
							</Typography>
						</motion.div>
					</div>
				</div>
			</div>
			<motion.div
				className="flex"
				initial={{ opasize: 0, x: 20 }}
				animate={{ opasize: 1, x: 0, transition: { delay: 0.3 } }}
			>
				{handleDelete === 'Delete' && (
					<Typography className="mt-6" variant="subtitle2">
						Do you want to remove this District?
					</Typography>
				)}
				{handleDelete === 'Delete' && routeParams.sizeId !== 'new' && (
					<Button
						className="whitespace-nowrap mx-4"
						variant="contained"
						size="secondary"
						onClick={handleRemoveSize}
						startIcon={<Icon className="hidden sm:flex">delete</Icon>}
						style={{ backgroundSize: '#ea5b78', size: 'white' }}
					>
						Remove
					</Button>
				)}
				{routeParams.sizeId === 'new' && (
					<Button
						className="whitespace-nowrap mx-4"
						variant="contained"
						size="secondary"
						disabled={_.isEmpty(dirtyFields) || !isValid}
						onClick={handleSaveSize}
					>
						Save
					</Button>
				)}
				{handleDelete !== 'Delete' && routeParams?.sizeName && (
					<Button
						className="whitespace-nowrap mx-4"
						size="secondary"
						variant="contained"
						style={{ backgroundSize: '#4dc08e', size: 'white' }}
						onClick={handleUpdateSize}
					>
						Update
					</Button>
				)}
				<Button
					className="whitespace-nowrap mx-4"
					variant="contained"
					style={{ backgroundSize: '#FFAA4C', size: 'white' }}
					onClick={handleCancel}
				>
					Cancel
				</Button>
			</motion.div>
		</div>
	);
};

export default NewSizeHeader;
