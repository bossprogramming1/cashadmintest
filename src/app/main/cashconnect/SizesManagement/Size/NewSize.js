import FusePageCarded from '@fuse/core/FusePageCarded';
import { useDeepCompareEffect } from '@fuse/hooks';
import { yupResolver } from '@hookform/resolvers/yup';
import { Button, Typography } from '@material-ui/core';
import withReducer from 'app/store/withReducer';
import { motion } from 'framer-motion';
import React, { useEffect, useState } from 'react';
import { FormProvider, useForm } from 'react-hook-form';
import { useDispatch, useSelector } from 'react-redux';
import { Link, useParams } from 'react-router-dom';
import * as yup from 'yup';
import { getUserPermissions } from 'app/store/dataSlice';
import { SIZE_CREATE, SIZE_DELETE, SIZE_DETAILS, SIZE_UPDATE } from 'app/constant/permission/permission';
import { getSize, newSize, resetSize } from '../store/sizeSlice';
import reducer from '../store/index';
import SizeForm from './SizeForm';
import NewSizeHeader from './NewSizeHeader';
import PagenotFound from '../../Pagenotfound/PagenotFound';

/**
 * Form Validation Schema
 */
const schema = yup.object().shape({
	name: yup.string().required('Name is required')
});

const Size = () => {
	const dispatch = useDispatch();
	const size = useSelector(({ sizesManagement }) => sizesManagement.size);

	const [noSize, setNoSize] = useState(false);
	const methods = useForm({
		mode: 'onChange',
		defaultValues: {},
		resolver: yupResolver(schema)
	});
	const routeParams = useParams();
	const UserPermissions = useSelector(state => state.data.UserPermissions);
	useEffect(() => {
		dispatch(getUserPermissions());
	}, []);
	const { reset, watch } = methods;
	const form = watch();

	useDeepCompareEffect(() => {
		function updateSizeState() {
			const { sizeId } = routeParams;

			if (sizeId === 'new') {
				localStorage.removeItem('event');
				/**
				 * Create New User data
				 */
				dispatch(newSize());
			} else {
				/**
				 * Get User data
				 */

				dispatch(getSize(sizeId)).then(action => {
					/**
					 * If the requested product is not exist show message
					 */
					if (!action.payload) {
						setNoSize(true);
					}
				});
			}
		}

		updateSizeState();
	}, [dispatch, routeParams]);

	useEffect(() => {}, []);

	useEffect(() => {
		if (!size) {
			return;
		}
		/**
		 * Reset the form on size state changes
		 */
		reset(size);
	}, [size, reset]);

	useEffect(() => {
		return () => {
			/**
			 * Reset Size on component unload
			 */
			dispatch(resetSize());
			setNoSize(false);
		};
	}, [dispatch]);

	/**
	 * Show Message if the requested products is not exists
	 */
	if (noSize) {
		return (
			<motion.div
				initial={{ opasize: 0 }}
				animate={{ opasize: 1, transition: { delay: 0.1 } }}
				className="flex flex-col flex-1 items-center justify-center h-full"
			>
				<Typography size="textSecondary" variant="h5">
					There is no such size!
				</Typography>
				<Button
					className="mt-24"
					component={Link}
					variant="outlined"
					to="/apps/e-commerce/products"
					size="inherit"
				>
					Go to Size Page
				</Button>
			</motion.div>
		);
	}

	return (
		<FormProvider {...methods}>
			{UserPermissions.includes(SIZE_CREATE) ||
			UserPermissions.includes(SIZE_UPDATE) ||
			UserPermissions.includes(SIZE_DELETE) ||
			UserPermissions.includes(SIZE_DETAILS) ? (
				<FusePageCarded
					classes={{
						toolbar: 'p-0',
						header: 'min-h-80 h-80'
					}}
					header={<NewSizeHeader />}
					content={
						<div className="p-16 sm:p-24">
							<SizeForm />
						</div>
					}
					innerScroll
				/>
			) : (
				<PagenotFound />
			)}
		</FormProvider>
	);
};
export default withReducer('sizesManagement', reducer)(Size);
