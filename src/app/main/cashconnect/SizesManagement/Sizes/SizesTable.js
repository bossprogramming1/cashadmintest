import FuseLoading from '@fuse/core/FuseLoading';
import FuseScrollbars from '@fuse/core/FuseScrollbars';
import _ from '@lodash';
import { Tooltip, Typography } from '@material-ui/core';
import Checkbox from '@material-ui/core/Checkbox';
import { makeStyles } from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TablePagination from '@material-ui/core/TablePagination';
import TableRow from '@material-ui/core/TableRow';
import DeleteIcon from '@material-ui/icons/Delete';
import EditIcon from '@material-ui/icons/Edit';
import Pagination from '@material-ui/lab/Pagination';
import { rowsPerPageOptions } from 'app/@data/data';
import { motion } from 'framer-motion';
import React, { useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { withRouter } from 'react-router-dom';
import { SEARCH_SIZE } from '../../../../constant/constants';
import { getSizes, selectSizes } from '../store/sizesSlice';
import SizesTableHead from './SizesTableHead';

const useStyles = makeStyles(() => ({
	root: {
		display: 'flex',
		justifyContent: 'space-between',
		flexWrap: 'nowrap',
		overflow: 'auto',
		minHeight: '35px'
	},
	toolbar: {
		'& > div': {
			minHeight: 'fit-content'
		}
	}
}));

const SizesTable = props => {
	const dispatch = useDispatch();
	const sizes = useSelector(selectSizes);
	const searchText = useSelector(({ sizesManagement }) => sizesManagement.sizes.searchText);
	const [searchSize, setSearchSize] = useState([]);
	const [loading, setLoading] = useState(true);
	const [selected, setSelected] = useState([]);
	const [page, setPage] = useState(0);
	const [rowsPerPage, setRowsPerPage] = useState(30);
	const [order, setOrder] = useState({
		direction: 'asc',
		id: null
	});
	let serialNumber = 1;

	const [parameter, setParameter] = useState({ page: 1, size: 30 });
	const totalPages = sessionStorage.getItem('sizes_total_pages');
	const totalElements = sessionStorage.getItem('sizes_total_elements');
	const classes = useStyles();
	const user_role = localStorage.getItem('user_role');

	useEffect(() => {
		dispatch(getSizes(parameter)).then(() => setLoading(false));
	}, [dispatch]);

	//search size
	useEffect(() => {
		searchText ? getSearchSize() : setSearchSize([]);
	}, [searchText]);

	const getSearchSize = () => {
		fetch(`${SEARCH_SIZE}?name=${searchText}`)
			.then(response => response.json())
			.then(searchedSizesData => {
				setSearchSize(searchedSizesData.sizes);
			})
			.catch(() => setSearchSize([]));
	};

	function handleRequestSort(sizeEvent, property) {
		const id = property;
		let direction = 'desc';

		if (order.id === property && order.direction === 'desc') {
			direction = 'asc';
		}

		setOrder({
			direction,
			id
		});
	}

	function handleSelectAllClick(sizeEvent) {
		if (sizeEvent.target.checked) {
			setSelected(sizes.map(n => n.id));
			return;
		}
		setSelected([]);
	}

	function handleDeselect() {
		setSelected([]);
	}

	function handleUpdateSize(item) {
		localStorage.removeItem('sizeEvent');
		props.history.push(`/apps/size-management/sizes/${item.id}/${item?.name}`);
	}
	function handleDeleteSize(item, sizeEvent) {
		localStorage.removeItem('sizeEvent');
		localStorage.setItem('sizeEvent', sizeEvent);
		props.history.push(`/apps/size-management/sizes/${item.id}/${item?.name}`);
	}

	function handleCheck(sizeEvent, id) {
		const selectedIndex = selected.indexOf(id);
		let newSelected = [];

		if (selectedIndex === -1) {
			newSelected = newSelected.concat(selected, id);
		} else if (selectedIndex === 0) {
			newSelected = newSelected.concat(selected.slice(1));
		} else if (selectedIndex === selected.length - 1) {
			newSelected = newSelected.concat(selected.slice(0, -1));
		} else if (selectedIndex > 0) {
			newSelected = newSelected.concat(selected.slice(0, selectedIndex), selected.slice(selectedIndex + 1));
		}

		setSelected(newSelected);
	}

	const handlePagination = (e, handlePage) => {
		setParameter({ ...parameter, page: handlePage });
		setPage(handlePage - 1);
		dispatch(getSizes({ ...parameter, page: handlePage }));
	};

	function handleChangePage(sizeEvent, value) {
		setPage(value);
		setParameter({ ...parameter, page: value + 1 });
		dispatch(getSizes({ ...parameter, page: value - 1 }));
	}

	function handleChangeRowsPerPage(sizeEvent) {
		setRowsPerPage(sizeEvent.target.value);
		setParameter({ ...parameter, size: sizeEvent.target.value });
		dispatch(getSizes({ ...parameter, size: sizeEvent.target.value }));
	}

	if (loading) {
		return <FuseLoading />;
	}

	if (sizes?.length === 0) {
		return (
			<motion.div
				initial={{ opasize: 0 }}
				animate={{ opasize: 1, transition: { delay: 0.1 } }}
				className="flex flex-1 items-center justify-center h-full"
			>
				<Typography size="textSecondary" variant="h5">
					There are no size!
				</Typography>
			</motion.div>
		);
	}

	return (
		<div className="w-full flex flex-col">
			<FuseScrollbars className="flex-grow overflow-x-auto">
				<Table stickyHeader className="min-w-xl" aria-labelledby="tableTitle">
					<SizesTableHead
						selectedSizeIds={selected}
						order={order}
						onSelectAllClick={handleSelectAllClick}
						onRequestSort={handleRequestSort}
						rowCount={sizes.length}
						onMenuItemClick={handleDeselect}
					/>

					<TableBody>
						{_.orderBy(
							searchText !== '' && !_.isEmpty(searchSize) ? searchSize : sizes,
							[
								o => {
									switch (order.id) {
										case 'categories': {
											return o.categories[0];
										}
										default: {
											return o[order.id];
										}
									}
								}
							],
							[order.direction]
						).map(n => {
							const isSelected = selected.indexOf(n.id) !== -1;
							return (
								<TableRow
									className="h-52 cursor-pointer"
									hover
									role="checkbox"
									aria-checked={isSelected}
									tabIndex={-1}
									key={n.id}
									selected={isSelected}
								>
									<TableCell className="whitespace-nowrap w-40 md:w-64 text-center" padding="none">
										<Checkbox
											checked={isSelected}
											onClick={sizeEvent => sizeEvent.stopPropagation()}
											onChange={sizeEvent => handleCheck(sizeEvent, n.id)}
										/>
									</TableCell>

									<TableCell className="whitespace-nowrap w-40 md:w-64" component="th" scope="row">
										{parameter.page * parameter.size - parameter.size + serialNumber++}
									</TableCell>

									<TableCell className="whitespace-nowrap p-4 md:p-16" component="th" scope="row">
										{n?.name}
									</TableCell>

									<TableCell
										whitespace-nowrap
										className="p-4 md:p-16"
										align="center"
										component="th"
										scope="row"
									>
										<div>
											<Tooltip title="Edit" placement="top" enterDelay={300}>
												<EditIcon
													onClick={sizeEvent => handleUpdateSize(n)}
													className="cursor-pointer"
													style={{ size: 'green' }}
												/>
											</Tooltip>
											<Tooltip title="Delete" placement="top" enterDelay={300}>
												<DeleteIcon
													onClick={event => handleDeleteSize(n, 'Delete')}
													className="cursor-pointer"
													style={{
														size: 'red',
														visibility:
															user_role === 'ADMIN' || user_role === 'admin'
																? 'visible'
																: 'hidden'
													}}
												/>
											</Tooltip>
										</div>
									</TableCell>
								</TableRow>
							);
						})}
					</TableBody>
				</Table>
			</FuseScrollbars>

			<div className={classes.root}>
				<Pagination
					classes={{ ul: 'flex-nowrap' }}
					count={totalPages}
					defaultPage={1}
					size="primary"
					showFirstButton
					showLastButton
					variant="outlined"
					shape="rounded"
					onChange={handlePagination}
				/>

				<TablePagination
					classes={{ root: 'overflow-visible' }}
					rowsPerPageOptions={rowsPerPageOptions}
					component="div"
					count={totalElements}
					rowsPerPage={rowsPerPage}
					page={page}
					className={classes.toolbar}
					backIconButtonProps={{
						'aria-label': 'Previous Page',
						className: 'py-0'
					}}
					nextIconButtonProps={{
						'aria-label': 'Next Page',
						className: 'py-0'
					}}
					onChangePage={handleChangePage}
					onChangeRowsPerPage={handleChangeRowsPerPage}
				/>
			</div>
		</div>
	);
};

export default withRouter(SizesTable);
