import FusePageCarded from '@fuse/core/FusePageCarded';
import { SIZE_LIST } from 'app/constant/permission/permission';
import { getUserPermissions } from 'app/store/dataSlice';
import withReducer from 'app/store/withReducer';
import React, { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import PagenotFound from '../../Pagenotfound/PagenotFound';
import reducer from '../store/index';
import SizesHeader from './SizesHeader';
import SizesTable from './SizesTable';

const Sizes = () => {
	const UserPermissions = useSelector(state => state.data.UserPermissions);
	useEffect(() => {
		dispatch(getUserPermissions());
	}, []);
	const dispatch = useDispatch();
	return (
		<FusePageCarded
			classes={{
				content: 'flex',
				contentCard: 'overflow-hidden',
				header: 'min-h-74 h-64'
			}}
			header={UserPermissions.includes(SIZE_LIST) && <SizesHeader />}
			content={UserPermissions.includes(SIZE_LIST) ? <SizesTable /> : <PagenotFound />}
			innerScroll
		/>
	);
};
export default withReducer('sizesManagement', reducer)(Sizes);
