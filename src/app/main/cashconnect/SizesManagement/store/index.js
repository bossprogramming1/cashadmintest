import { combineReducers } from '@reduxjs/toolkit';
import size from './sizeSlice';
import sizes from './sizesSlice';

const reducer = combineReducers({
	size,
	sizes
});

export default reducer;
