import { createAsyncThunk, createEntityAdapter, createSlice } from '@reduxjs/toolkit';
import axios from 'axios';
import { DELETE_SIZE, GET_SIZES } from '../../../../constant/constants';

export const getSizes = createAsyncThunk('sizeManagement/sizes/getSizes', async parameter => {
	const { page, size } = parameter;

	axios.defaults.headers.common['Content-type'] = 'application/json';
	axios.defaults.headers.common.Authorization = localStorage.getItem('jwt_access_token');

	const response = axios.get(GET_SIZES, { params: { page, size } });
	const data = await response;

	sessionStorage.setItem('sizes_total_elements', data.data.total_elements);
	sessionStorage.setItem('sizes_total_pages', data.data.total_pages);
	delete axios.defaults.headers.common['Content-type'];
	delete axios.defaults.headers.common.Authorization;

	return data.data.sizes;
});

export const removeSizes = createAsyncThunk(
	'sizeManagement/sizes/removeSizes',
	async (sizeIds, { dispatch, getState }) => {
		const authTOKEN = {
			headers: {
				'Content-type': 'application/json',
				Authorization: localStorage.getItem('jwt_access_token')
			}
		};
		await axios.delete(`${DELETE_SIZE}`, { sizeIds }, authTOKEN);

		return sizeIds;
	}
);

const sizesAdapter = createEntityAdapter({});

export const { selectAll: selectSizes, selectById: selectSizeById } = sizesAdapter.getSelectors(
	state => state.sizesManagement.sizes
);

const sizesSlice = createSlice({
	name: 'sizeManagement/sizes',
	initialState: sizesAdapter.getInitialState({
		searchText: ''
	}),
	reducers: {
		setSizesSearchText: {
			reducer: (state, action) => {
				state.searchText = action.payload;
			},
			prepare: event => ({ payload: event.target.value || '' })
		}
	},
	extraReducers: {
		[getSizes.fulfilled]: sizesAdapter.setAll
	}
});

export const { setData, setSizesSearchText } = sizesSlice.actions;
export default sizesSlice.reducer;
