import { createAsyncThunk, createSlice } from '@reduxjs/toolkit';
import axios from 'axios';
import { CREATE_SIZE, DELETE_SIZE, GET_SIZEID, UPDATE_SIZE } from '../../../../constant/constants';

export const getSize = createAsyncThunk('sizeManagement/size/getSize', async (params, { rejectWithValue }) => {
	const authTOKEN = {
		headers: {
			'Content-type': 'application/json',
			Authorization: localStorage.getItem('jwt_access_token')
		}
	};

	try {
		const response = await axios.get(`${GET_SIZEID}${params}`, authTOKEN);
		const data = await response.data;
		return data === undefined ? null : data;
	} catch (err) {
		return rejectWithValue(params);
	}
});

export const removeSize = createAsyncThunk('sizeManagement/size/removeSize', async val => {
	const authTOKEN = {
		headers: {
			'Content-type': 'application/json',
			Authorization: localStorage.getItem('jwt_access_token')
		}
	};

	const sizeId = val.id;
	const response = await axios.delete(`${DELETE_SIZE}${sizeId}`, authTOKEN);
	return response;
});

export const updateSize = createAsyncThunk(
	'sizeManagement/size/updateSize',
	async (sizeData, { dispatch, getState }) => {
		const { size } = getState().sizesManagement;

		const authTOKEN = {
			headers: {
				'Content-type': 'application/json',
				Authorization: localStorage.getItem('jwt_access_token')
			}
		};
		const response = await axios.put(`${UPDATE_SIZE}${size.id}`, sizeData, authTOKEN);
		return response;
	}
);

export const saveSize = createAsyncThunk('sizeManagement/size/saveSize', async sizeData => {
	const authTOKEN = {
		headers: {
			'Content-type': 'application/json',
			Authorization: localStorage.getItem('jwt_access_token')
		}
	};
	const response = await axios.post(`${CREATE_SIZE}`, sizeData, authTOKEN);
	return response;
});

const sizeSlice = createSlice({
	name: 'sizeManagement/size',
	initialState: null,
	reducers: {
		resetSize: () => null,
		newSize: {
			reducer: (state, action) => action.payload,
			prepare: event => ({
				payload: {}
			})
		}
	},
	extraReducers: {
		[getSize.fulfilled]: (state, action) => action.payload,
		[saveSize.fulfilled]: (state, action) => action.payload,
		[removeSize.fulfilled]: (state, action) => action.payload,
		[updateSize.fulfilled]: (state, action) => action.payload
	}
});

export const { newSize, resetSize } = sizeSlice.actions;

export default sizeSlice.reducer;
