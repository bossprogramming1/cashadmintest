import FusePageCarded from '@fuse/core/FusePageCarded';
import { CITY_LIST } from 'app/constant/permission/permission';
import { getUserPermissions } from 'app/store/dataSlice';
import withReducer from 'app/store/withReducer';
import React, { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import PagenotFound from '../../Pagenotfound/PagenotFound';
import reducer from '../store/index';
import CitysHeader from './CitysHeader';
import CitysTable from './CitysTable';

const Citys = () => {
	const UserPermissions = useSelector(state => state.data.UserPermissions);
	useEffect(() => {
		dispatch(getUserPermissions());
	}, []);
	const dispatch = useDispatch();
	return (
		<FusePageCarded
			classes={{
				content: 'flex',
				contentCard: 'overflow-hidden',
				header: 'min-h-74 h-64'
			}}
			header={UserPermissions.includes(CITY_LIST) && <CitysHeader />}
			content={UserPermissions.includes(CITY_LIST) ? <CitysTable /> : <PagenotFound />}
			innerScroll
		/>
	);
};
export default withReducer('citysManagement', reducer)(Citys);
