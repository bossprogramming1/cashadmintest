import FusePageCarded from '@fuse/core/FusePageCarded';
import { useDeepCompareEffect } from '@fuse/hooks';
import { yupResolver } from '@hookform/resolvers/yup';
import { Button, Typography } from '@material-ui/core';
import {
	HOMEPAGE_SLIDER_CREATE,
	HOMEPAGE_SLIDER_DELETE,
	HOMEPAGE_SLIDER_DETAILS,
	HOMEPAGE_SLIDER_UPDATE
} from 'app/constant/permission/permission';
import { getUserPermissions } from 'app/store/dataSlice';
import withReducer from 'app/store/withReducer';
import { motion } from 'framer-motion';
import React, { useEffect, useState } from 'react';
import { FormProvider, useForm } from 'react-hook-form';
import { useDispatch, useSelector } from 'react-redux';
import { Link, useParams } from 'react-router-dom';
import * as yup from 'yup';
import PagenotFound from '../../Pagenotfound/PagenotFound';
import reducer from '../store/index';
import { getSlidersetting, newSlidersetting, resetSlidersetting } from '../store/slidersettingSlice';
import NewSlidersettingHeader from './NewSlidersettingHeader';
import SlidersettingForm from './SlidersettingForm';

/**
 * Form Validation Schema
 */
const schema = yup.object().shape({
	title: yup.string().required('Title is required'),
	link: yup.string().required('Link is required'),
	serial_number: yup.number().required('Serial number is required')
});

const Slidersetting = () => {
	const dispatch = useDispatch();
	const slidersetting = useSelector(({ slidersettingsManagement }) => slidersettingsManagement.slidersetting);

	const [noSlidersetting, setNoSlidersetting] = useState(false);
	const methods = useForm({
		mode: 'onChange',
		defaultValues: {},
		resolver: yupResolver(schema)
	});
	const routeParams = useParams();
	const UserPermissions = useSelector(state => state.data.UserPermissions);
	useEffect(() => {
		dispatch(getUserPermissions());
	}, []);
	const { reset, watch, control, onChange, formState } = methods;
	const form = watch();

	useDeepCompareEffect(() => {
		function updateSlidersettingState() {
			const { slidersettingId } = routeParams;

			if (slidersettingId === 'new') {
				localStorage.removeItem('deleteSlidersettingEvent');
				localStorage.removeItem('updateSlidersettingEvent');
				// localStorage.removeItem('event')
				/**
				 * Create New User data
				 */
				dispatch(newSlidersetting());
			} else {
				/**
				 * Get User data
				 */

				dispatch(getSlidersetting(slidersettingId)).then(action => {
					/**
					 * If the requested product is not exist show message
					 */
					if (!action.payload) {
						setNoSlidersetting(true);
					}
				});
			}
		}
		updateSlidersettingState();
	}, [dispatch, routeParams]);

	useEffect(() => {
		if (!slidersetting) {
			return;
		}
		/**
		 * Reset the form on slidersetting state changes
		 */
		reset(slidersetting);
	}, [slidersetting, reset]);

	useEffect(() => {
		return () => {
			/**
			 * Reset Slidersetting on component unload
			 */
			dispatch(resetSlidersetting());
			setNoSlidersetting(false);
		};
	}, [dispatch]);

	/**
	 * Show Message if the requested products is not exists
	 */
	if (noSlidersetting) {
		return (
			<motion.div
				initial={{ opacity: 0 }}
				animate={{ opacity: 1, transition: { delay: 0.1 } }}
				className="flex flex-col flex-1 items-center justify-center h-full"
			>
				<Typography color="textSecondary" variant="h5">
					There is no such slidersetting!
				</Typography>
				<Button
					className="mt-24"
					component={Link}
					variant="outlined"
					to="/apps/e-commerce/products"
					color="inherit"
				>
					Go to Slidersetting Page
				</Button>
			</motion.div>
		);
	}

	return (
		<FormProvider {...methods}>
			{UserPermissions.includes(HOMEPAGE_SLIDER_CREATE) ||
			UserPermissions.includes(HOMEPAGE_SLIDER_UPDATE) ||
			UserPermissions.includes(HOMEPAGE_SLIDER_DELETE) ||
			UserPermissions.includes(HOMEPAGE_SLIDER_DETAILS) ? (
				<FusePageCarded
					classes={{
						toolbar: 'p-0',
						header: 'min-h-52 h-52 sm:h-136 sm:min-h-136'
					}}
					header={<NewSlidersettingHeader />}
					content={
						<div className="p-16 sm:p-24">
							<SlidersettingForm />
						</div>
					}
					innerScroll
				/>
			) : (
				<PagenotFound />
			)}
		</FormProvider>
	);
};
export default withReducer('slidersettingsManagement', reducer)(Slidersetting);
