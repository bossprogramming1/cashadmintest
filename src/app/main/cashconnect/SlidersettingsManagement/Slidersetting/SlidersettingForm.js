import { Icon } from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';
import TextField from '@material-ui/core/TextField';
import clsx from 'clsx';
import React, { useState } from 'react';
import { Controller, useFormContext } from 'react-hook-form';
import { useParams } from 'react-router-dom';
import { BASE_URL } from '../../../../constant/constants';

const useStyles = makeStyles(theme => ({
	hidden: {
		display: 'none'
	}
}));

function SlidersettingForm(props) {
	const userID = localStorage.getItem('UserID');
	const classes = useStyles(props);
	const methods = useFormContext();
	const { control, formState, watch } = methods;
	const { errors } = formState;
	const routeParams = useParams();
	const { slidersettingId } = routeParams;
	const image = watch('image');
	const [previewImage, setPreviewImage] = useState();

	return (
		<div>
			<Controller
				name="title"
				control={control}
				render={({ field }) => {
					return (
						<TextField
							{...field}
							className="mt-8 mb-16"
							error={!!errors.title}
							helperText={errors?.title?.message}
							label="Title"
							id="title"
							required
							variant="outlined"
							InputLabelProps={field.value && { shrink: true }}
							fullWidth
						/>
					);
				}}
			/>
			<Controller
				name="link"
				control={control}
				render={({ field }) => {
					return (
						<TextField
							{...field}
							className="mt-8 mb-16"
							error={!!errors.link}
							helperText={errors?.link?.message}
							label="Link"
							id="link"
							required
							variant="outlined"
							InputLabelProps={field.value && { shrink: true }}
							fullWidth
						/>
					);
				}}
			/>
			<Controller
				name="serial_number"
				control={control}
				render={({ field }) => {
					return (
						<TextField
							{...field}
							className="mt-8 mb-16"
							error={!!errors.serial_number}
							helperText={errors?.serial_number?.message}
							label="Serial Number"
							id="serial_number"
							required
							variant="outlined"
							InputLabelProps={field.value && { shrink: true }}
							fullWidth
						/>
					);
				}}
			/>
			<Controller
				name="details"
				control={control}
				render={({ field }) => {
					return (
						<TextField
							{...field}
							className="mt-8 mb-16"
							error={!!errors.details}
							helperText={errors?.details?.message}
							label="Details"
							id="details"
							multiline
							rows={4}
							required
							variant="outlined"
							InputLabelProps={field.value && { shrink: true }}
							fullWidth
						/>
					);
				}}
			/>
			{/* image upload*/}
			<div className="flex justify-center sm:justify-start flex-wrap -mx-16">
				<Controller
					name="image"
					control={control}
					render={({ field: { onChange, value } }) => (
						<label
							htmlFor="button-file"
							className={clsx(
								classes.productImageUpload,
								'flex items-center justify-center relative w-128 h-128 rounded-16 mx-12 mb-24 overflow-hidden cursor-pointer shadow hover:shadow-lg'
							)}
						>
							<input
								accept="image/*"
								className="hidden"
								id="button-file"
								type="file"
								onChange={async e => {
									const reader = new FileReader();
									reader.onload = () => {
										if (reader.readyState === 2) {
											setPreviewImage(reader.result);
										}
									};
									reader.readAsDataURL(e.target.files[0]);

									const file = e.target.files[0];
									onChange(file);
								}}
							/>
							<Icon fontSize="large" color="action">
								cloud_upload
							</Icon>
						</label>
					)}
				/>
				{image && !previewImage && (
					<img src={`${BASE_URL}${image}`} style={{ width: '100px', height: '100px' }} alt="Not found" />
				)}

				<div style={{ width: '100px', height: '100px' }}>
					<img src={previewImage} alt="Not found" />
				</div>
			</div>
		</div>
	);
}

export default SlidersettingForm;
