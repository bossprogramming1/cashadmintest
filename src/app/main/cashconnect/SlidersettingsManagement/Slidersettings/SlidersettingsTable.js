import FuseLoading from '@fuse/core/FuseLoading';
import FuseScrollbars from '@fuse/core/FuseScrollbars';
import _ from '@lodash';
import Checkbox from '@material-ui/core/Checkbox';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TablePagination from '@material-ui/core/TablePagination';
import TableRow from '@material-ui/core/TableRow';
import DeleteIcon from '@material-ui/icons/Delete';
import EditIcon from '@material-ui/icons/Edit';
import React, { useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { withRouter } from 'react-router-dom';
import { Tooltip } from '@material-ui/core';
import { BASE_URL } from '../../../../constant/constants';
import { getSlidersettings, selectSlidersettings } from '../store/slidersettingsSlice';
import SlidersettingsTableHead from './SlidersettingsTableHead';

const SlidersettingsTable = props => {
	const dispatch = useDispatch();
	const slidersettings = useSelector(selectSlidersettings);
	const searchText = useSelector(
		({ slidersettingsManagement }) => slidersettingsManagement.slidersettings.searchText
	);
	const [loading, setLoading] = useState(true);
	const [selected, setSelected] = useState([]);
	const [data, setData] = useState(slidersettings);
	const [page, setPage] = useState(0);
	const [rowsPerPage, setRowsPerPage] = useState(10);
	const [order, setOrder] = useState({
		direction: 'asc',
		id: null
	});
	const user_role = localStorage.getItem('user_role');

	let serialNumber = 1;
	const [deleteItem, setDeleteItem] = useState('');
	useEffect(() => {
		dispatch(getSlidersettings()).then(() => setLoading(false));
	}, [dispatch]);

	useEffect(() => {
		if (searchText.length !== 0) {
			setData(_.filter(slidersettings, item => item?.name.toLowerCase().includes(searchText.toLowerCase())));
			setPage(0);
		} else {
			setData(slidersettings);
		}
	}, [slidersettings, searchText]);

	function handleRequestSort(slidersettingEvent, property) {
		const id = property;
		let direction = 'desc';

		if (order.id === property && order.direction === 'desc') {
			direction = 'asc';
		}

		setOrder({
			direction,
			id
		});
	}

	function handleSelectAllClick(slidersettingEvent) {
		if (slidersettingEvent.target.checked) {
			setSelected(data.map(n => n.id));
			return;
		}
		setSelected([]);
	}

	function handleDeselect() {
		setSelected([]);
	}

	function handleUpdateSlidersetting(item, event) {
		localStorage.removeItem('deleteSlidersettingEvent');
		localStorage.setItem('updateSlidersettingEvent', event);
		props.history.push(`/apps/slidersetting-management/slidersettings/${item.id}/${item?.name}`);
	}
	function handleDeleteSlidersetting(item, event) {
		localStorage.removeItem('updateSlidersettingEvent');
		localStorage.setItem('deleteSlidersettingEvent', event);
		props.history.push(`/apps/slidersetting-management/slidersettings/${item.id}/${item?.name}`);
	}

	function handleCheck(slidersettingEvent, id) {
		const selectedIndex = selected.indexOf(id);
		let newSelected = [];

		if (selectedIndex === -1) {
			newSelected = newSelected.concat(selected, id);
		} else if (selectedIndex === 0) {
			newSelected = newSelected.concat(selected.slice(1));
		} else if (selectedIndex === selected.length - 1) {
			newSelected = newSelected.concat(selected.slice(0, -1));
		} else if (selectedIndex > 0) {
			newSelected = newSelected.concat(selected.slice(0, selectedIndex), selected.slice(selectedIndex + 1));
		}

		setSelected(newSelected);
	}

	function handleChangePage(slidersettingEvent, value) {
		setPage(value);
	}

	function handleChangeRowsPerPage(slidersettingEvent) {
		setRowsPerPage(slidersettingEvent.target.value);
	}

	if (loading) {
		return <FuseLoading />;
	}

	return (
		<div className="w-full flex flex-col">
			<FuseScrollbars className="flex-grow overflow-x-auto">
				<Table stickyHeader className="min-w-xl" aria-labelledby="tableTitle">
					<SlidersettingsTableHead
						selectedSlidersettingIds={selected}
						order={order}
						onSelectAllClick={handleSelectAllClick}
						onRequestSort={handleRequestSort}
						rowCount={data.length}
						onMenuItemClick={handleDeselect}
					/>

					<TableBody>
						{_.orderBy(
							data,
							[
								o => {
									switch (order.id) {
										case 'categories': {
											return o.categories[0];
										}
										default: {
											return o[order.id];
										}
									}
								}
							],
							[order.direction]
						)
							.slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
							.map(n => {
								const isSelected = selected.indexOf(n.id) !== -1;
								return (
									<TableRow
										hover
										role="checkbox"
										aria-checked={isSelected}
										tabIndex={-1}
										key={n.id}
										selected={isSelected}
									>
										<TableCell
											whitespace-nowrap
											className="w-40 md:w-64 text-center"
											padding="none"
										>
											<Checkbox
												checked={isSelected}
												onClick={slidersettingEvent => slidersettingEvent.stopPropagation()}
												onChange={slidersettingEvent => handleCheck(slidersettingEvent, n.id)}
											/>
										</TableCell>
										<TableCell
											whitespace-nowrap
											className="w-40 md:w-64"
											component="th"
											scope="row"
										>
											{serialNumber++}
										</TableCell>
										<TableCell
											className="w-52 px-4 md:px-0"
											component="th"
											scope="row"
											padding="none"
										>
											{n.image && n.featuredImageId ? (
												<img
													className="w-full block rounded"
													src={_.find(n.image, { id: n.featuredImageId }).url}
													alt={n.title}
												/>
											) : (
												<img
													className="w-full block rounded"
													style={{ borderRadius: '50%' }}
													src={`${BASE_URL}${n.image}`}
													alt={n.title}
												/>
											)}
										</TableCell>
										<TableCell className="whitespace-nowrap p-4 md:p-16" component="th" scope="row">
											{n.title}
										</TableCell>
										<TableCell className="whitespace-nowrap p-4 md:p-16" component="th" scope="row">
											{n.link}
										</TableCell>
										<TableCell className="whitespace-nowrap p-4 md:p-16" component="th" scope="row">
											{n.details}
										</TableCell>

										<TableCell
											whitespace-nowrap
											className="p-4 md:p-16"
											align="center"
											component="th"
											scope="row"
										>
											<div>
												<Tooltip title="Edit" placement="top" enterDelay={300}>
													<EditIcon
														onClick={slidersettingEvent =>
															handleUpdateSlidersetting(n, 'updateSlidersettingEvent')
														}
														className="h-52 cursor-pointer"
														style={{ color: 'green' }}
													/>
												</Tooltip>
												<Tooltip title="Delete" placement="top" enterDelay={300}>
													<DeleteIcon
														onClick={event =>
															handleDeleteSlidersetting(n, 'deleteSlidersettingEvent')
														}
														className="h-52 cursor-pointer"
														style={{
															color: 'red',
															visibility:
																user_role === 'ADMIN' || user_role === 'admin'
																	? 'visible'
																	: 'hidden'
														}}
													/>
												</Tooltip>
											</div>
										</TableCell>
									</TableRow>
								);
							})}
					</TableBody>
				</Table>
			</FuseScrollbars>

			<TablePagination
				className="flex-shrink-0 border-t-1"
				rowsPerPageOptions={[5, 10, 25]}
				component="div"
				count={data.length}
				rowsPerPage={rowsPerPage}
				page={page}
				backIconButtonProps={{
					'aria-label': 'Previous Page'
				}}
				nextIconButtonProps={{
					'aria-label': 'Next Page'
				}}
				onChangePage={handleChangePage}
				onChangeRowsPerPage={handleChangeRowsPerPage}
			/>
		</div>
	);
};
export default withRouter(SlidersettingsTable);
