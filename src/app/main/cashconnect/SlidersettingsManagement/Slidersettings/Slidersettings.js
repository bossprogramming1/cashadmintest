import FusePageCarded from '@fuse/core/FusePageCarded';
import { HOMEPAGE_SLIDER_LIST } from 'app/constant/permission/permission';
import { getUserPermissions } from 'app/store/dataSlice';
import withReducer from 'app/store/withReducer';
import React, { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import PagenotFound from '../../Pagenotfound/PagenotFound';
import reducer from '../store/index';
import SlidersettingsHeader from './SlidersettingsHeader';
import SlidersettingsTable from './SlidersettingsTable';

const Slidersettings = () => {
	const UserPermissions = useSelector(state => state.data.UserPermissions);
	useEffect(() => {
		dispatch(getUserPermissions());
	}, []);
	const dispatch = useDispatch();
	return (
		<FusePageCarded
			classes={{
				content: 'flex',
				contentCard: 'overflow-hidden',
				header: 'min-h-74 h-64'
			}}
			header={UserPermissions.includes(HOMEPAGE_SLIDER_LIST) && <SlidersettingsHeader />}
			content={UserPermissions.includes(HOMEPAGE_SLIDER_LIST) ? <SlidersettingsTable /> : <PagenotFound />}
			innerScroll
		/>
	);
};
export default withReducer('slidersettingsManagement', reducer)(Slidersettings);
