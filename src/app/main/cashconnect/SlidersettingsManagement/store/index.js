import { combineReducers } from '@reduxjs/toolkit';
import slidersetting from './slidersettingSlice';
import slidersettings from './slidersettingsSlice';

const reducer = combineReducers({
    slidersetting,
    slidersettings,
});

export default reducer;