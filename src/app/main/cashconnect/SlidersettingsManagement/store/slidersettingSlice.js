import { createAsyncThunk, createSlice } from '@reduxjs/toolkit';
import axios from 'axios';
import {
	CREATE_SLIDERSETTING,
	DELETE_SLIDERSETTING,
	GET_SLIDERSETTINGID,
	UPDATE_SLIDERSETTING
} from '../../../../constant/constants';

export const getSlidersetting = createAsyncThunk(
	'slidersettingManagement/slidersetting/getSlidersetting',
	async (params, { rejectWithValue }) => {
		const authTOKEN = {
			headers: {
				'Content-type': 'application/json',
				Authorization: localStorage.getItem('jwt_access_token')
			}
		};

		try {
			const response = await axios.get(`${GET_SLIDERSETTINGID}${params}`, authTOKEN);
			const data = await response.data;
			return data === undefined ? null : data;
		} catch (err) {
			return rejectWithValue(params);
		}
	}
);

export const removeSlidersetting = createAsyncThunk(
	'slidersettingManagement/slidersetting/removeSlidersetting',
	async (val, { dispatch, getState }) => {
		const authTOKEN = {
			headers: {
				'Content-type': 'application/json',
				Authorization: localStorage.getItem('jwt_access_token')
			}
		};

		const slidersettingId = val.id;
		await axios.delete(`${DELETE_SLIDERSETTING}${slidersettingId}`, authTOKEN);
	}
);

export const updateSlidersetting = createAsyncThunk(
	'slidersettingManagement/slidersetting/updateSlidersetting',
	async (slidersettingData, { dispatch, getState }) => {
		const { slidersetting } = getState().slidersettingsManagement;
		const sliderSettingDataToFormData = jsonToFormData(slidersettingData);

		const authTOKEN = {
			headers: {
				'Content-type': 'application/json',
				Authorization: localStorage.getItem('jwt_access_token')
			}
		};
		const response = await axios.put(
			`${UPDATE_SLIDERSETTING}${slidersetting.id}`,
			sliderSettingDataToFormData,
			authTOKEN
		);
	}
);

//buildformdata
const buildFormData = (formData, data, parentKey) => {
	if (data && typeof data === 'object' && !(data instanceof Date) && !(data instanceof File)) {
		Object.keys(data).forEach(key => {
			buildFormData(formData, data[key], parentKey ? `${parentKey}[${key}]` : key);
		});
	} else {
		const value = data === null ? '' : data;

		formData.append(parentKey, value);
	}
};

//convertJsonToFormData
const jsonToFormData = data => {
	const formData = new FormData();

	buildFormData(formData, data);
	return formData;
};

export const saveSlidersetting = createAsyncThunk(
	'slidersettingManagement/slidersetting/saveSlidersetting',
	async (slidersettingData, { dispatch, getState }) => {
		const sliderSettingDataToFormData = jsonToFormData(slidersettingData);
		const authTOKEN = {
			headers: {
				'Content-type': 'application/json',
				Authorization: localStorage.getItem('jwt_access_token')
			}
		};
		const response = await axios.post(`${CREATE_SLIDERSETTING}`, sliderSettingDataToFormData, authTOKEN);
	}
);

const slidersettingSlice = createSlice({
	name: 'slidersettingManagement/slidersetting',
	initialState: null,
	reducers: {
		resetSlidersetting: () => null,
		newSlidersetting: {
			reducer: (state, action) => action.payload,
			prepare: event => ({
				payload: {
					title: '',
					link: '',
					serial_number: 0,
					details: '',
					image: ''
				}
			})
		}
	},
	extraReducers: {
		[getSlidersetting.fulfilled]: (state, action) => action.payload,
		[saveSlidersetting.fulfilled]: (state, action) => {
			localStorage.setItem('slidersettingAlert', 'saveSlidersetting');
			return action.payload;
		},
		[removeSlidersetting.fulfilled]: () => {
			localStorage.setItem('slidersettingAlert', 'deleteSlidersetting');
		},
		[updateSlidersetting.fulfilled]: () => {
			localStorage.setItem('slidersettingAlert', 'updateSlidersetting');
		}
	}
});

export const { newSlidersetting, resetSlidersetting } = slidersettingSlice.actions;
export default slidersettingSlice.reducer;
