import { createAsyncThunk, createEntityAdapter, createSlice } from '@reduxjs/toolkit';
import axios from 'axios';
import { DELETE_SLIDERSETTING, GET_SLIDERSETTINGS } from '../../../../constant/constants';

export const getSlidersettings = createAsyncThunk('slidersettingManagement/slidersettings/geSlidersettings', async () => {
    const authTOKEN = {
        headers: {
            'Content-type': 'application/json',
            Authorization: localStorage.getItem('jwt_access_token'),
        }
    }

    const response = axios.get(GET_SLIDERSETTINGS, authTOKEN);
    const data = await response;
    return data.data.homepage_sliders;
});


export const removeSlidersettings = createAsyncThunk(
    'slidersettingManagement/slidersettings/removeSlidersettings',
    async (slidersettingIds, { dispatch, getState }) => {
        const authTOKEN = {
            headers: {
                'Content-type': 'application/json',
                Authorization: localStorage.getItem('jwt_access_token'),
            }
        }
        await axios.delete(`${DELETE_SLIDERSETTING}`, { slidersettingIds }, authTOKEN);

        return slidersettingIds;
    }
);



const slidersettingsAdapter = createEntityAdapter({});

export const { selectAll: selectSlidersettings, selectById: selectSlidersettingById } = slidersettingsAdapter.getSelectors(
    state => state.slidersettingsManagement.slidersettings
);

const slidersettingsSlice = createSlice({
    name: 'slidersettingManagement/slidersettings',
    initialState: slidersettingsAdapter.getInitialState({
        searchText: ''
    }),
    reducers: {
        setSlidersettingsSearchText: {
            reducer: (state, action) => {
                state.searchText = action.payload;
            },
            prepare: event => ({ payload: event.target.value || '' })
        }
    },
    extraReducers: {
        [getSlidersettings.fulfilled]: slidersettingsAdapter.setAll
    }
});

export const { setData, setSlidersettingsSearchText } = slidersettingsSlice.actions;
export default slidersettingsSlice.reducer;