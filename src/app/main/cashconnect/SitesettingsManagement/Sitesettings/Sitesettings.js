import FusePageCarded from '@fuse/core/FusePageCarded';
import { GENERAL_SETTING_LIST } from 'app/constant/permission/permission';
import { getUserPermissions } from 'app/store/dataSlice';
import withReducer from 'app/store/withReducer';
import React, { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import PagenotFound from '../../Pagenotfound/PagenotFound';
import reducer from '../store/index';
import SitesettingsHeader from './SitesettingsHeader';
import SitesettingsTable from './SitesettingsTable';

const Sitesettings = () => {
	const UserPermissions = useSelector(state => state.data.UserPermissions);
	useEffect(() => {
		dispatch(getUserPermissions());
	}, []);
	const dispatch = useDispatch();
	return (
		<FusePageCarded
			classes={{
				content: 'flex',
				contentCard: 'overflow-hidden',
				header: 'min-h-74 h-64'
			}}
			header={UserPermissions.includes(GENERAL_SETTING_LIST) && <SitesettingsHeader />}
			content={UserPermissions.includes(GENERAL_SETTING_LIST) ? <SitesettingsTable /> : <PagenotFound />}
			innerScroll
		/>
	);
};
export default withReducer('sitesettingsManagement', reducer)(Sitesettings);
