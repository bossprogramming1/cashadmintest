import FusePageCarded from '@fuse/core/FusePageCarded';
import { useDeepCompareEffect } from '@fuse/hooks';
import { yupResolver } from '@hookform/resolvers/yup';
import { Tab, Tabs } from '@material-ui/core';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';
import {
	EMPLOYEE_CREATE,
	EMPLOYEE_DELETE,
	EMPLOYEE_DETAILS,
	EMPLOYEE_UPDATE
} from 'app/constant/permission/permission';
import { getUserPermissions } from 'app/store/dataSlice';
import withReducer from 'app/store/withReducer';
import { motion } from 'framer-motion';
import React, { useEffect, useState } from 'react';
import { FormProvider, useForm } from 'react-hook-form';
import { useDispatch, useSelector } from 'react-redux';
import { Link, useParams } from 'react-router-dom';
import * as yup from 'yup';
import PagenotFound from '../../Pagenotfound/PagenotFound';
import { getEmployee, newEmployee, resetEmployee } from '../store/employeeSlice';
import reducer from '../store/index';
import PersonalInfoTab from '../tabs/PersonalInfoTab';
import EmployeeForm from './EmployeeForm';
import NewEmployeeHeader from './NewEmployeeHeader';

/**
 * Form Validation Schema
 */
const schema = yup.object().shape({
	branch: yup.number().required('Branch is required'),
	emp_id_no: yup.string().required('Id is required'),
	first_name: yup.string().required('First name is required').min(5, 'First name must be at least 5 characters'),
	last_name: yup.string().required('Last name is required'),
	username: yup.string().required('User name is required'),
	email: yup.string().email('You must enter a valid email address').required('You must enter a email address'),
	password: yup.string().min(6, 'Password must be at least 6 characters').required('Password is required'),
	confirmPassword: yup
		.string()
		.required('Confirm password is required')
		.oneOf([yup.ref('password'), null], 'Passwords must match'),
	primary_phone: yup.string().required('Primary phone is required'),
	street_address_one: yup.string().required('Primary address is required'),
	gender: yup.string().required('Gender is required'),
	thana: yup.number().required('Police station is required'),
	city: yup.number().required('District is required'),
	country: yup.number().required('Country is required'),
	role: yup.number().required('Role is required'),
	department: yup.number().required('Department is required')
});

const NewEmployee = () => {
	const dispatch = useDispatch();
	const employee = useSelector(({ employeesManagement }) => employeesManagement.employee);
	const routeParams = useParams();
	const [tabValue, setTabValue] = useState(0);
	const [noEmployee, setNoEmployee] = useState(false);
	const methods = useForm({
		mode: 'onChange',
		defaultValues: {},
		resolver: yupResolver(schema)
	});
	const { reset, watch } = methods;
	const UserPermissions = useSelector(state => state.data.UserPermissions);
	useEffect(() => {
		dispatch(getUserPermissions());
	}, []);
	useDeepCompareEffect(() => {
		function updateEmployeeState() {
			const { employeeId } = routeParams;
			if (employeeId === 'new') {
				localStorage.removeItem('deleteEmployee');
				localStorage.removeItem('updateEmployee');
				/**
				 * Create New User data
				 */
				dispatch(newEmployee());
			} else {
				/**
				 * Get User data
				 */
				dispatch(getEmployee(routeParams)).then(action => {
					/**
					 * If the requested product is not exist show message
					 */
					if (!action.payload) {
						setNoEmployee(true);
					}
				});
			}
		}

		updateEmployeeState();
	}, [dispatch, routeParams]);

	useEffect(() => {
		if (!employee) {
			return;
		}
		/**
		 * Reset the form on employee state changes
		 */
		reset({
			...employee,
			branch: employee?.branch?.id,
			thana: employee?.thana?.id,
			city: employee?.city?.id,
			country: employee?.country?.id,
			department: employee?.department?.id,
			role: employee?.role?.id,
			country_code1: '+880',
			country_code2: '+880',
			show_country_code1: '+880',
			show_country_code2: '+880'
		});
	}, [employee, reset]);

	useEffect(() => {
		return () => {
			/**
			 * Reset Employee on component unload
			 */
			dispatch(resetEmployee());
			setNoEmployee(false);
		};
	}, [dispatch]);

	function handleTabChange(event, value) {
		setTabValue(value);
	}

	/**
	 * Show Message if the requested products is not exists
	 */
	if (noEmployee) {
		return (
			<motion.div
				initial={{ opacity: 0 }}
				animate={{ opacity: 1, transition: { delay: 0.1 } }}
				className="flex flex-col flex-1 items-center justify-center h-full"
			>
				<Typography color="textSecondary" variant="h5">
					There is no such employee!
				</Typography>
				<Button
					className="mt-24"
					component={Link}
					variant="outlined"
					to="/apps/e-commerce/products"
					color="inherit"
				>
					Go to Employee Page
				</Button>
			</motion.div>
		);
	}

	return (
		<FormProvider {...methods}>
			{UserPermissions.includes(EMPLOYEE_CREATE) ||
			UserPermissions.includes(EMPLOYEE_UPDATE) ||
			UserPermissions.includes(EMPLOYEE_DELETE) ||
			UserPermissions.includes(EMPLOYEE_DETAILS) ? (
				<FusePageCarded
					classes={{
						root: {},
						toolbar: 'p-0',
						header: 'min-h-80 h-80'
					}}
					contentToolbar={
						<Tabs
							value={tabValue}
							onChange={handleTabChange}
							indicatorColor="primary"
							textColor="primary"
							variant="scrollable"
							scrollButtons="auto"
							classes={{ root: 'w-full h-64' }}
						>
							<Tab className="h-64" label="Basic Info" />
							<Tab className="h-64" label="Personal Info" />
						</Tabs>
					}
					header={<NewEmployeeHeader />}
					content={
						<div className="p-16 sm:p-24">
							<div className={tabValue !== 0 ? 'hidden' : ''}>
								<EmployeeForm />
							</div>
							<div className={tabValue !== 1 ? 'hidden' : ''}>
								<PersonalInfoTab />
							</div>
						</div>
					}
					innerScroll
				/>
			) : (
				<PagenotFound />
			)}
		</FormProvider>
	);
};
export default withReducer('employeesManagement', reducer)(NewEmployee);
