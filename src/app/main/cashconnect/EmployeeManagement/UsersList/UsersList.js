import FusePageCarded from '@fuse/core/FusePageCarded';
import withReducer from 'app/store/withReducer';
import React from 'react';
import reducer from '../store/index';
import UsersListHeader from './UsersListHeader';
import UsersListTable from './UsersListTable';


const UsersList = () => {
    return (
        <FusePageCarded
            classes={{
                content: 'flex',
                contentCard: 'overflow-hidden',
                header:  'min-h-74 h-64'
            }}
            header={<UsersListHeader />}
            content={<UsersListTable />}
            innerScroll
        />
    );
};
export default withReducer('employeesManagement', reducer)(UsersList);