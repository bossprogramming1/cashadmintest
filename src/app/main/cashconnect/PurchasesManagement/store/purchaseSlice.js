import { createAsyncThunk, createSlice } from '@reduxjs/toolkit';
import axios from 'axios';
import {
	CREATE_PRODUCT_IMAGE,
	CREATE_PURCHASE,
	DELETE_PRODUCT_IMAGE,
	DELETE_PURCHASE,
	GET_PURCHASEID,
	GET_PURCHASE_ITEMS,
	UPDATE_PURCHASE
} from '../../../../constant/constants';

export const getPurchase = createAsyncThunk(
	'purchaseManagement/purchase/getPurchase',
	async (params, { rejectWithValue }) => {
		const authTOKEN = {
			headers: {
				'Content-type': 'application/json',
				Authorization: localStorage.getItem('jwt_access_token')
			}
		};

		try {
			const response = await axios.get(`${GET_PURCHASEID}${params}`, authTOKEN);
			const data = await response.data.purchase_request;
			return data === undefined ? null : data;
		} catch (err) {
			return rejectWithValue(params);
		}
	}
);
export const getPurchaseItems = createAsyncThunk(
	'purchaseManagement/purchase/getPurchaseItems',
	async (params, { rejectWithValue }) => {
		const authTOKEN = {
			headers: {
				'Content-type': 'application/json',
				Authorization: localStorage.getItem('jwt_access_token')
			}
		};

		try {
			const response = await axios.get(`${GET_PURCHASE_ITEMS}${params}`, authTOKEN);
			const data = await response.data;
			return data === undefined ? null : data;
		} catch (err) {
			return rejectWithValue(params);
		}
	}
);

export const removePurchase = createAsyncThunk(
	'purchaseManagement/purchase/removePurchase',
	async (purchaseId, { dispatch, getState }) => {
		const authTOKEN = {
			headers: {
				'Content-type': 'application/json',
				Authorization: localStorage.getItem('jwt_access_token')
			}
		};

		await axios.delete(`${DELETE_PURCHASE}${purchaseId}`, authTOKEN);
	}
);

export const updatePurchase = createAsyncThunk(
	'purchaseManagement/purchase/updatePurchase',
	async (updatePurchaseData, { dispatch, getState }) => {
		const updatePurchaseDataToFormData = jsonToFormData(updatePurchaseData);

		const { purchase } = getState().purchasesManagement;
		const authTOKEN = {
			headers: {
				'Content-type': 'application/json',
				Authorization: localStorage.getItem('jwt_access_token')
			}
		};
		const response = await axios.put(`${UPDATE_PURCHASE}${purchase.id}`, updatePurchaseDataToFormData, authTOKEN);
	}
);

export const savePurchase = createAsyncThunk(
	'purchaseManagement/purchase/savePurchase',
	async (purchaseData, { dispatch, getState }) => {
		const purchaseDataToFormData = jsonToFormData(purchaseData);

		const authTOKEN = {
			headers: {
				'Content-type': 'application/json',
				Authorization: localStorage.getItem('jwt_access_token')
			}
		};
		const response = await axios.post(`${CREATE_PURCHASE}`, purchaseDataToFormData, authTOKEN);
	}
);
//buildformdata
const buildFormData = (formData, data, parentKey) => {
	if (data && typeof data === 'object' && !(data instanceof Date) && !(data instanceof File)) {
		Object.keys(data).forEach(key => {
			buildFormData(formData, data[key], parentKey ? `${parentKey}[${key}]` : key);
		});
	} else {
		const value = data === null ? '' : data;

		formData.append(parentKey, value);
	}
};

//convertJsonToFormData
const jsonToFormData = data => {
	const formData = new FormData();

	buildFormData(formData, data);
	return formData;
};

//saveProductImage
export const savePurchaseProductImage = createAsyncThunk(
	'eCommerceApp/product/saveProduct',
	async (productImageData, { dispatch, getState }) => {
		const productImageDataToFormData = jsonToFormData(productImageData);
		const authTOKEN = {
			headers: {
				'content-type': 'multipart/form-data',
				Authorization: localStorage.getItem('jwt_access_token')
			}
		};
		const response = await axios.post(CREATE_PRODUCT_IMAGE, productImageDataToFormData, authTOKEN);
	}
);

//removeProductImage

export const removePurchaseProductImage = createAsyncThunk('eCommerceApp/product/removeProduct', async val => {
	const authTOKEN = {
		headers: {
			'Content-type': 'application/json',
			Authorization: localStorage.getItem('jwt_access_token')
		}
	};
	axios.delete(`${DELETE_PRODUCT_IMAGE}${val}`, authTOKEN);
});

const purchaseSlice = createSlice({
	name: 'purchaseManagement/purchase',
	initialState: null,
	reducers: {
		resetPurchase: () => null,
		newPurchase: {
			reducer: (state, action) => action.payload,
			prepare: event => ({
				payload: {
					first_name: '',
					last_name: '',
					contact_no: '',
					email: '',
					contact_type: '',
					is_subscribed: false,
					vendor: 0,
					branch: 0,
					purchase_status: 0,
					assigned_to: 0,
					verified_by: 0,
					cancelled_by: 0
				}
			})
		}
	},
	extraReducers: {
		[getPurchase.fulfilled]: (state, action) => action.payload,
		[getPurchaseItems.fulfilled]: (state, action) => action.payload,
		[savePurchase.fulfilled]: (state, action) => {
			localStorage.setItem('purchaseAlert', 'savePurchase');
			return action.payload;
		},
		[removePurchase.fulfilled]: () => {
			localStorage.setItem('purchaseAlert', 'deletePurchase');
		},
		[updatePurchase.fulfilled]: () => {
			localStorage.setItem('purchaseAlert', 'updatePurchase');
		}
	}
});

export const { newPurchase, resetPurchase } = purchaseSlice.actions;

export default purchaseSlice.reducer;
