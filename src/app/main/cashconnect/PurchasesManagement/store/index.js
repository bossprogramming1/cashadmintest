import { combineReducers } from '@reduxjs/toolkit';
import purchase from './purchaseSlice';
import purchases from './purchasesSlice';

const reducer = combineReducers({
    purchase,
    purchases,
});

export default reducer;