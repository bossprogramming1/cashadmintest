import { createAsyncThunk, createEntityAdapter, createSlice } from '@reduxjs/toolkit';
import { GET_PURCHASES } from 'app/constant/constants';
import axios from 'axios';

export const getPurchases = createAsyncThunk('purchaseManagement/purchases/gePurchases', async (pageAndSize) => {

    axios.defaults.headers.common['Content-type'] = 'application/json';
    axios.defaults.headers.common.Authorization = localStorage.getItem('jwt_access_token');

    const response = axios.get(GET_PURCHASES, { params: pageAndSize });
    const data = await response;

    sessionStorage.setItem('total_purchases_elements', data.data.total_elements);
    sessionStorage.setItem('total_purchases_pages', data.data.total_pages);
    delete axios.defaults.headers.common['Content-type'];
    delete axios.defaults.headers.common.Authorization;

    return data.data.purchaserequests
});

const purchasesAdapter = createEntityAdapter({});

export const { selectAll: selectPurchases, selectById: selectPurchaseById } = purchasesAdapter.getSelectors(
    state => state.purchasesManagement.purchases
);

const purchasesSlice = createSlice({
    name: 'purchaseManagement/purchases',
    initialState: purchasesAdapter.getInitialState({
        searchText: ''
    }),
    reducers: {
        setPurchasesSearchText: {
            reducer: (state, action) => {
                state.searchText = action.payload;
            },
            prepare: event => ({ payload: event?.target?.value || '' })
        }
    },
    extraReducers: {
        [getPurchases.fulfilled]: purchasesAdapter.setAll
    }
});

export const { setData, setPurchasesSearchText } = purchasesSlice.actions;
export default purchasesSlice.reducer;