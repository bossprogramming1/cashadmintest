import FusePageCarded from '@fuse/core/FusePageCarded';
import { useDeepCompareEffect } from '@fuse/hooks';
import { yupResolver } from '@hookform/resolvers/yup';
import Button from '@material-ui/core/Button';
import Link from '@material-ui/core/Link';
import Tab from '@material-ui/core/Tab';
import Tabs from '@material-ui/core/Tabs';
import Typography from '@material-ui/core/Typography';
import {
	PURCHASE_REQUEST_CREATE,
	PURCHASE_REQUEST_DELETE,
	PURCHASE_REQUEST_DETAIL,
	PURCHASE_REQUEST_UPDATE
} from 'app/constant/permission/permission';
import { getUserPermissions } from 'app/store/dataSlice';
import withReducer from 'app/store/withReducer';
import { motion } from 'framer-motion';
import React, { useEffect, useState } from 'react';
import { FormProvider, useForm } from 'react-hook-form';
import { useDispatch, useSelector } from 'react-redux';
import { useParams } from 'react-router-dom';
import * as yup from 'yup';
import { BASE_URL } from '../../../../constant/constants';
import PagenotFound from '../../Pagenotfound/PagenotFound';
import reducer from '../store/index';
import { getPurchase, newPurchase, resetPurchase } from '../store/purchaseSlice';
import ItemContext from './ItemContext';
import NewPurchaseHeader from './NewPurchaseHeader';
import PurchaseBasicInfoTab from './tabs/PurchaseBasicInfoTab';
import PurchaseItemTab from './tabs/PurchaseItemTab';
import PurchaseSettingTab from './tabs/PurchaseSettingTab';

/**
 * Form Validation Schema
 */
const schema = yup.object().shape({
	first_name: yup.string().required('Name is required'),
	last_name: yup.string().required('Name is required')
});

//export const ItemContext = createContext();

const NewPurchase = () => {
	const [items, setItems] = useState([
		{
			id: 0,
			item_name: '',
			item_description: '',
			item_price: 0,
			item_quantity: 0,
			item_brand_id: 0,
			item_category_id: 0,
			images: []
		}
	]);
	const dispatch = useDispatch();
	const purchase = useSelector(({ purchasesManagement }) => purchasesManagement.purchase);
	const [noPurchase, setNoPurchase] = useState(false);
	const [tabValue, setTabValue] = useState(0);
	const methods = useForm({
		mode: 'onChange',
		defaultValues: {},
		resolver: yupResolver(schema)
	});
	const routeParams = useParams();
	const { purchaseId } = routeParams;
	const { reset, watch, control, onChange, formState } = methods;
	const form = watch();
	const [previewImage, setPreviewImage] = useState([]);
	const UserPermissions = useSelector(state => state.data.UserPermissions);
	useEffect(() => {
		dispatch(getUserPermissions());
	}, []);
	useDeepCompareEffect(() => {
		function updatePurchaseState() {
			if (purchaseId === 'new') {
				localStorage.removeItem('updatePurchase');
				localStorage.removeItem('deletePurchase');
				/**
				 * Create New User data
				 */
				dispatch(newPurchase());
			} else {
				/**
				 * Get User data
				 */

				dispatch(getPurchase(purchaseId)).then(action => {
					/**
					 * If the requested product is not exist show message
					 */
					if (!action.payload) {
						setNoPurchase(true);
					}
				});
			}
		}

		updatePurchaseState();
	}, [dispatch, routeParams]);

	const setFormState = () => {
		const updatePurchaseData = {
			first_name: purchase?.first_name,
			last_name: purchase?.last_name,
			contact_no: purchase?.contact_no,
			email: purchase?.email,
			contact_type: purchase?.contact_type,
			request_date: purchase?.request_date,
			street_address: purchase?.street_address,
			is_subscribed: purchase?.is_subscribed,
			branch: purchase?.branch?.id,
			vendor: purchase?.vendor?.id,
			assigned_to: purchase?.assigned_to?.id,
			verified_by: purchase?.verified_by?.id,
			cancelled_by: purchase?.cancelled_by?.id,
			purchase_status: purchase?.purchase_status?.id
		};
		purchase?.purchase_request_items?.map((item, idx) => {
			if (idx === 0) {
				items[idx].item_name = item?.name;
				items[idx].item_description = item?.description;
				items[idx].item_price = item.unit_price;
				items[idx].item_quantity = item.quantity;
				items[idx].id = item.id;
				items[idx].item_brand_id = item.brand;
				items[idx].item_category_id = item.category;
				updatePurchaseData[`item_name${idx}`] = item?.name;
				updatePurchaseData[`item_description${idx}`] = item?.description;
				updatePurchaseData[`item_price${idx}`] = item.unit_price;
				updatePurchaseData[`item_quantity${idx}`] = item.quantity;
				updatePurchaseData[`item_brand_id${idx}`] = item.brand;
				updatePurchaseData[`item_category_id${idx}`] = item.category;

				item.purchase_request_item_images.map((image, id) => {
					updatePurchaseData[`image${id}`] = `${BASE_URL}${image.image}`;

					items[idx].images.push(image.image);
					return null;
				});
			} else {
				const prevItems = items;

				prevItems.push({
					id: 0,
					item_name: '',
					item_description: '',
					item_price: 0,
					item_quantity: 0,
					item_brand_id: 0,
					item_category_id: 0,
					images: []
				});
				setItems(prevItems);
				items[idx].item_name = item?.name;
				items[idx].item_description = item?.description;
				items[idx].item_price = item.unit_price;
				items[idx].item_quantity = item.quantity;
				items[idx].id = item.id;
				items[idx].item_brand_id = item.brand;
				items[idx].item_category_id = item.category;
				updatePurchaseData[`item_name${idx}`] = item?.name;
				updatePurchaseData[`item_description${idx}`] = item?.description;
				updatePurchaseData[`item_price${idx}`] = item.unit_price;
				updatePurchaseData[`item_quantity${idx}`] = item.quantity;
				updatePurchaseData[`item_brand_id${idx}`] = item.brand;
				updatePurchaseData[`item_category_id${idx}`] = item.category;

				item.purchase_request_item_images.map((image, id) => {
					updatePurchaseData[`image${id}`] = image.image;
					items[idx].images.push(image.image);
					return null;
				});
			}
			return null;
		});
		return updatePurchaseData;
	};

	useEffect(() => {
		if (!purchase) {
			return;
		}
		/**
		 * Reset the form on purchase state changes
		 */
		const purchaseData = setFormState();
		reset(purchaseData);
	}, [purchase, reset]);

	useEffect(() => {
		return () => {
			/**
			 * Reset Purchase on component unload
			 */
			dispatch(resetPurchase());
			setNoPurchase(false);
		};
	}, [dispatch]);

	/**
	 * Tab Change
	 */
	function handleTabChange(event, value) {
		setTabValue(value);
	}

	/**
	 * Show Message if the requested products is not exists
	 */
	if (noPurchase) {
		return (
			<motion.div
				initial={{ opacity: 0 }}
				animate={{ opacity: 1, transition: { delay: 0.1 } }}
				className="flex flex-col flex-1 items-center justify-center h-full"
			>
				<Typography color="textSecondary" variant="h5">
					There is no such purchase!
				</Typography>
				<Button
					className="mt-24"
					component={Link}
					variant="outlined"
					to="/apps/e-commerce/products"
					color="inherit"
				>
					Go to Purchase Page
				</Button>
			</motion.div>
		);
	}

	return (
		<ItemContext.Provider value={[items, setItems]}>
			<FormProvider {...methods}>
				{UserPermissions.includes(PURCHASE_REQUEST_CREATE) ||
				UserPermissions.includes(PURCHASE_REQUEST_UPDATE) ||
				UserPermissions.includes(PURCHASE_REQUEST_DELETE) ||
				UserPermissions.includes(PURCHASE_REQUEST_DETAIL) ? (
					<FusePageCarded
						classes={{
							toolbar: 'p-0',
							header: 'min-h-74 h-64'
						}}
						header={<NewPurchaseHeader />}
						contentToolbar={
							<Tabs
								value={tabValue}
								onChange={handleTabChange}
								indicatorColor="primary"
								textColor="primary"
								variant="scrollable"
								scrollButtons="auto"
								classes={{ root: 'w-full h-64' }}
							>
								<Tab className="h-64" label="Basic Info" />
								<Tab className="h-64" label="Item" />
								{localStorage.getItem('updatePurchase') && <Tab className="h-64" label="Setting" />}
							</Tabs>
						}
						content={
							<div className="p-16 sm:p-24">
								<div className={tabValue !== 0 ? 'hidden' : ''}>
									<PurchaseBasicInfoTab />
								</div>

								<div className={tabValue !== 1 ? 'hidden' : ''}>
									<PurchaseItemTab purchaseId={purchaseId} />
								</div>

								<div className={tabValue !== 2 ? 'hidden' : ''}>
									<PurchaseSettingTab purchaseId={purchaseId} />
								</div>
							</div>
						}
						innerScroll
					/>
				) : (
					<PagenotFound />
				)}
			</FormProvider>
		</ItemContext.Provider>
	);
};
export default withReducer('purchasesManagement', reducer)(NewPurchase);
