import { makeStyles } from '@material-ui/core/styles';
import AddIcon from '@material-ui/icons/Add';
import CancelIcon from '@material-ui/icons/Cancel';
import { Button, Card, CardContent, Modal, TextField, Typography, Icon } from '@material-ui/core';
import CloseIcon from '@material-ui/icons/Close';
//import { DownloadIcon } from '@material-ui/icons';
import { Autocomplete } from '@material-ui/lab';
import { getBrand, getCategory } from 'app/store/dataSlice';
import React, { useContext, useEffect, useRef, useState } from 'react';
import { Controller, useFormContext } from 'react-hook-form';
import { useDispatch, useSelector } from 'react-redux';
import { Link, useParams } from 'react-router-dom';
import UrlImageDownloader from 'react-url-image-downloader';
import { BASE_URL, DELETE_PURCHASE_REQ_ITEM } from '../../../../../constant/constants';
import { getPurchase, getPurchaseItems } from '../../store/purchaseSlice';
import { getPurchases } from '../../store/purchasesSlice';
import ItemContext from '../ItemContext';

const useStyles = makeStyles(theme => ({
	imgContainer: {
		'& img': {
			width: '100px',
			height: '100px'
		}
	},
	modal: {
		margin: 'auto',
		backgroundColor: 'white',
		width: '900px',
		height: 'fit-content',
		maxWidth: '540px',
		maxHeight: 'fit-content',
		borderRadius: '20px',
		overflow: 'hidden'
	}
}));

function PurchaseImageTab(props) {
	const updatePurchase = localStorage.getItem('updatePurchase');
	const [items, setItems] = useContext(ItemContext);
	// const [items, setItems] = useState([]);
	const [openModal, setOpenModal] = useState(false);
	const [itemName, setItemName] = useState('');
	const methods = useFormContext();
	const { control, formState, reset, watch } = methods;
	const { errors } = formState;
	const userID = localStorage.getItem('UserID');
	const classes = useStyles(props);
	const routeParams = useParams();
	const { purchaseId } = routeParams;
	const [previewImage, setPreviewImage] = useState([]);
	const dispatch = useDispatch();
	const purchase = useSelector(({ purchasesManagement }) => purchasesManagement.purchase);

	const [newPrevImage, setNewPrevImage] = useState([{ images: [] }]);
	const [imgLimitMessage, setImgLimitMessage] = useState({});
	const [purchaseImages, setPurchaseImages] = useState([]);
	const [itm, setItm] = useState(true);
	const parentIdRef = useRef(null);

	useEffect(() => {
		dispatch(getBrand());
		dispatch(getCategory());
		dispatch(getPurchaseItems(purchaseId));
	}, []);
	// useEffect(() => {
	// 	dispatch(getPurchaseItems(purchaseId)).then(item => {
	// 		setItems(item.payload);
	// 	});
	// }, [purchaseId]);

	const brands = useSelector(({ data }) => data.brands);
	const categories = useSelector(({ data }) => data.parent_categories);

	useEffect(() => {
		purchase?.purchase_request_items?.map((item, idx) => {
			const pImage = newPrevImage;

			if (idx === 0) {
				item.purchase_request_item_images.map((image, id) => {
					pImage[idx].images.push(`${BASE_URL}${image.image}`);
					setNewPrevImage(pImage);
					return null;
				});
			} else {
				pImage.push({ images: [] });
				item.purchase_request_item_images.map((image, id) => {
					pImage[idx].images.push(`${BASE_URL}${image.image}`);
					setNewPrevImage(pImage);
					return null;
				});
			}
			return null;
		});
	}, [purchase]);

	//handleAddItem
	const handleAddItem = () => {
		const prevItems = items;
		const pImage = newPrevImage;

		prevItems.push({
			id: 0,
			item_name: '',
			item_description: '',
			item_price: 0,
			item_quantity: 0,
			item_brand_id: 0,
			item_category_id: 0,
			images: []
		});
		setItems(prevItems);

		pImage.push({ images: [] });
		setNewPrevImage(pImage);

		setItm(!itm);
	};
	//handleDeleteItem
	//old
	// const handleDeleteItem = idx => {
	// 	const CardItems = [...items];
	// 	reset({
	// 		...formState,

	// 		[`item_name${idx}`]: '',
	// 		[`item_description${idx}`]: '',
	// 		[`item_price${idx}`]: '',
	// 		[`item_quantity${idx}`]: '',
	// 		[`image${idx}`]: ''
	// 	});
	// 	CardItems.splice(idx, 1);
	// 	setItems(CardItems);

	// 	const selectedItem = [...newPrevImage];
	// 	selectedItem.splice(idx, 1);
	// 	setNewPrevImage(selectedItem);
	// 	setItm(!itm);
	// };

	const confirmItemDeletion = (idx, itemName) => {
		setItemName(itemName);
		setOpenModal(true);
	};

	const handleDeleteItem = idx => {
		const cardItems = [...items];
		cardItems.splice(idx, 1);
		setItems(cardItems);

		const selectedItem = [...newPrevImage];
		selectedItem.splice(idx, 1);
		setNewPrevImage(selectedItem);

		// Create a copy of the form state and remove the fields associated with the deleted item
		const updatedFormState = { ...formState };
		delete updatedFormState[`item_name${idx}`];
		delete updatedFormState[`item_brand_id${idx}`];
		delete updatedFormState[`item_category_id${idx}`];
		delete updatedFormState[`item_description${idx}`];
		delete updatedFormState[`item_price${idx}`];
		delete updatedFormState[`item_quantity${idx}`];
		delete updatedFormState[`image${idx}`];

		// Reset the form with the updated state
		reset(updatedFormState);

		setItm(!itm); // I'm not sure what this is for, you can remove it if unnecessary
	};

	//handleOnChange
	const handleOnChange = (idx, event, bciEventName, newValueId) => {
		const newItems = items.map((item, sidx) => {
			if (idx !== sidx) return items;

			const newItem = { ...item };
			if (event.target?.name === `item_name${idx}`) {
				newItem.item_name = event.target.value;
				items[sidx] = newItem;
				setItems(items);
			}
			if (event.target?.name === `item_description${idx}`) {
				newItem.item_description = event.target.value;
				items[sidx] = newItem;
				setItems(items);
			}
			if (event.target?.name === `item_price${idx}`) {
				newItem.item_price = event.target.value;
				items[sidx] = newItem;
				setItems(items);
			}
			if (event.target?.name === `item_quantity${idx}`) {
				newItem.item_quantity = event.target.value;
				items[sidx] = newItem;
				setItems(items);
			}
			if (bciEventName === `item_brand_id${idx}`) {
				newItem.item_brand_id = newValueId;
				items[sidx] = newItem;
				setItems(items);
			}
			if (bciEventName === `item_category_id${idx}`) {
				newItem.item_category_id = newValueId;
				items[sidx] = newItem;
				setItems(items);
			}

			if (bciEventName === `image${sidx}`) {
				newPrevImage.map((imageItem, id) => {
					if (sidx === id && imageItem.images.length <= 3) {
						setImgLimitMessage('');
						const reader = new FileReader();
						reader.onload = () => {
							if (reader.readyState === 2) {
								const prevImage = imageItem;
								prevImage.images.push(reader.result);
								setItm(!itm);
							}
						};
						reader.readAsDataURL(event.target.files[0]);
						const file = event.target.files[0];
						newItem.images.push(file);
						items[sidx] = newItem;
					} else {
						setImgLimitMessage({ id: idx, message: 'You can add maximum 4 images' });
					}
					return null;
				});
			}

			return null;
		});
		return null;
	};

	//handleImageDelete
	const handleImageDelete = (idx, imgIdx) => {
		items.map((item, sidx) => {
			if (sidx === idx) {
				newPrevImage.map((imageItem, id) => {
					if (idx === id) {
						const img = imageItem;
						img.images.splice(imgIdx, 1);
						setItm(!itm);
					}
					return null;
				});

				const newItem = { ...item };
				newItem.images.splice(imgIdx, 1);
				items[sidx] = newItem;
				setItm(!itm);
			}
			return null;
		});
	};
	function handleDeletePurchaseItem(item, idx) {
		fetch(`${DELETE_PURCHASE_REQ_ITEM}${purchaseId}/${item.id}`, {
			method: 'DELETE',
			headers: {
				'content-type': 'application/json',
				Authorization: localStorage.getItem('jwt_access_token')
			}
		})
			.then(response => response.json())
			.then(resData => {
				setItems(resData?.data);

				setOpenModal(false);

				// const CardItems = [...items];
				// reset({
				// 	...formState,

				// 	[`item_name${idx}`]: '',
				// 	[`item_description${idx}`]: '',
				// 	[`item_price${idx}`]: '',
				// 	[`item_quantity${idx}`]: '',
				// 	[`image${idx}`]: ''
				// });
				// CardItems.splice(idx, 1);
				// setItems(CardItems);

				// const selectedItem = [...newPrevImage];
				// selectedItem.splice(idx, 1);
				// setNewPrevImage(selectedItem);
				// setItm(!itm);
			});
	}
	return (
		<div>
			{items.map((item, idx) => {
				return (
					<div
						style={{ border: '2px solid gray', borderRadius: '5px', padding: '20px', marginBottom: '10px' }}
					>
						{updatePurchase && (
							<>
								<Controller
									name={`item_brand_id${idx}`}
									control={control}
									render={({ field: { onChange, value } }) => (
										<Autocomplete
											freeSolo
											required
											value={value ? brands.find(brand => brand?.id === value) : null}
											options={brands}
											getOptionLabel={option => `${option?.name}`}
											onChange={(event, newValue) =>
												handleOnChange(idx, event, `item_brand_id${idx}`, newValue?.id)
											}
											renderInput={params => (
												<TextField
													{...params}
													className="mt-8 mb-16"
													error={!!errors.item_brand_id}
													InputLabelProps={{ shrink: true }}
													helperText={errors?.item_brand_id?.message}
													label="Brand"
													id="brand"
													variant="outlined"
												/>
											)}
										/>
									)}
								/>
								<Controller
									name={`item_category_id${idx}`}
									control={control}
									render={({ field: { onChange, value } }) => (
										<Autocomplete
											freeSolo
											value={value ? categories.find(data => data.id === value) : null}
											options={categories}
											getOptionLabel={option => {
												parentIdRef.current = option.parent;
												const parent = [];
												let parentstr = '';
												for (let i = 0; i < categories.length; i++) {
													parent.push(
														`${
															option.parent
																? categories.find(
																		data => data.id === parentIdRef.current
																  )?.name
																: null
														}`
													);
													parentIdRef.current = option.parent
														? categories.find(data => data.id === parentIdRef.current)
																?.parent
														: null;
													parentIdRef.current ? null : (i = categories.length);
												}
												parent.reverse();

												for (let i = 0; i < parent.length; i++) {
													parentstr += `${i !== 0 ? '>>' : ''}${parent[i]} `;
												}

												return option.parent
													? `${parentstr}>> ${option?.name} `
													: `${option?.name} `;
											}}
											onChange={(event, newValue) =>
												handleOnChange(idx, event, `item_category_id${idx}`, newValue?.id)
											}
											renderInput={params => (
												<TextField
													{...params}
													className="mt-8 mb-16"
													error={!!errors.item_category_id}
													InputLabelProps={{ shrink: true }}
													helperText={errors?.item_category_id?.message}
													label="Category"
													variant="outlined"
												/>
											)}
										/>
									)}
								/>
							</>
						)}
						<Controller
							name={`item_name${idx}`}
							control={control}
							render={({ field }) => (
								<TextField
									{...field}
									disabled={updatePurchase}
									className="mt-8 mb-16"
									error={!!errors.item_name}
									required
									autoFocus
									onBlur={event => handleOnChange(idx, event)}
									InputLabelProps={{ shrink: true }}
									helperText={errors?.item_name?.message}
									id="item_name"
									label="Item Name"
									type="text"
									multiline
									rows={3}
									variant="outlined"
									fullWidth
								/>
							)}
						/>
						<Controller
							name={`item_description${idx}`}
							control={control}
							render={({ field }) => (
								<TextField
									{...field}
									disabled={updatePurchase}
									className="mt-8 mb-16"
									error={!!errors.item_description}
									required
									autoFocus
									onBlur={event => handleOnChange(idx, event)}
									InputLabelProps={{ shrink: true }}
									helperText={errors?.item_description?.message}
									id="item_description"
									label="Item Description"
									type="text"
									multiline
									rows={3}
									variant="outlined"
									fullWidth
								/>
							)}
						/>
						<Controller
							name={`item_price${idx}`}
							control={control}
							render={({ field }) => (
								<TextField
									{...field}
									updatePurchase={updatePurchase}
									className="mt-8 mb-16"
									error={!!errors.item_price}
									disabled={updatePurchase}
									required
									onBlur={event => handleOnChange(idx, event)}
									//value={item.item_price}
									InputLabelProps={{ shrink: true }}
									helperText={errors?.item_price?.message}
									id="item_price"
									label="Item Price"
									type="text"
									variant="outlined"
									fullWidth
								/>
							)}
						/>
						<Controller
							name={`item_quantity${idx}`}
							control={control}
							render={({ field }) => (
								<TextField
									{...field}
									disabled={updatePurchase}
									className="mt-8 mb-16"
									error={!!errors.item_quantity}
									required
									onBlur={event => handleOnChange(idx, event)}
									//value={item.item_price}
									InputLabelProps={{ shrink: true }}
									helperText={errors?.item_quantity?.message}
									id="item_quantity"
									label="Item Quantity"
									type="text"
									variant="outlined"
									fullWidth
								/>
							)}
						/>
						<div className="flex justify-center sm:justify-start flex-wrap -mx-16">
							<input
								type="file"
								id="file"
								style={{ marginBottom: '100px' }}
								name={`image${idx} `}
								onChange={event => handleOnChange(idx, event, `image${idx}`)}
								multiple
							/>

							{newPrevImage.length !== 0 &&
								newPrevImage.map((imgItem, imgIndex) =>
									idx === imgIndex
										? imgItem.images.map((image, imgidx) => (
												<div>
													<CancelIcon
														style={{ color: 'red', marginLeft: '100px', cursor: 'pointer' }}
														onClick={() => handleImageDelete(idx, imgidx)}
													/>
													{/* <img
                                                    style={{ width: '100px', height: '100px' }}
                                                    src={image}
                                                    alt="Not found"
                                                /> */}
													{/* <Link to={image} target="_blank" download={image.slice(53)}>Download</Link> */}
													{/* <a href={image} download={image.slice(53)}>
                                                    <img
                                                        style={{ width: '100px', height: '100px' }}
                                                        src={image}
                                                    />
                                                    Download
                                                </a> */}
													<div
														style={{ width: '100px', height: '100px' }}
														className={classes.imgContainer}
													>
														<div>
															<UrlImageDownloader imageUrl={image} />
														</div>
													</div>
												</div>
										  ))
										: null
								)}
						</div>

						{imgLimitMessage.id === idx &&
							newPrevImage.map((imageItem, id) =>
								id === idx && imageItem.images.length >= 4 ? (
									<div>
										<Typography
											style={{ color: 'red' }}
										>{`${imgLimitMessage.message} `}</Typography>
									</div>
								) : null
							)}

						<Button
							style={{
								color: 'red',
								marginTop: '10px',
								display: items.length == 1 ? 'none' : 'inline-block'
							}}
							variant="contained"
							size="small"
							//color="secondary"
							startIcon={<CancelIcon />}
							onClick={event => {
								// handleDeletePurchaseItem(item, idx)
								confirmItemDeletion(idx, item);
							}}
						>
							Delete Item
						</Button>
					</div>
				);
			})}
			{!updatePurchase && (
				<Button
					variant="contained"
					size="small"
					color="primary"
					startIcon={<AddIcon />}
					onClick={handleAddItem}
				>
					Add Another Item
				</Button>
			)}
			<Modal
				open={openModal}
				className={classes.modal}
				onClose={() => {
					setOpenModal(false);
				}}
				keepMounted
			>
				<>
					<Card style={{ marginBottom: '10px' }}>
						<CardContent>
							<div style={{ display: 'flex', justifyContent: 'space-between' }}>
								<Typography
									className="text-center m-10"
									style={{ visibility: 'hidden' }}
									variant="h5"
									component="div"
								>
									Application Status
								</Typography>
								<CloseIcon
									onClick={event => setOpenModal(false)}
									className="cursor-pointer custom-delete-icon-style"
									// style={{ color: 'red' }}
								/>
							</div>

							<Typography className="text-center m-10" variant="h5" component="div">
								Are you sure you want to remove the item <b>"{itemName.item_name}"</b> ?
							</Typography>

							<div className="flex justify-center items-center  mr-32">
								<Button
									className="whitespace-nowrap mx-4"
									variant="contained"
									color="secondary"
									style={{ backgroundColor: 'red', color: 'white', height: '35px' }}
									// disabled={!name || _.isEmpty(name)}
									onClick={event => setOpenModal(false)}
								>
									Cancel
								</Button>
								<Button
									className="whitespace-nowrap mx-4"
									variant="contained"
									color="secondary"
									style={{ backgroundColor: '#22d3ee', height: '35px' }}
									// disabled={!name || _.isEmpty(name)}
									onClick={event => handleDeletePurchaseItem(itemName)}
								>
									Delete
								</Button>
							</div>
						</CardContent>
					</Card>
				</>
			</Modal>
		</div>
	);
}

export default PurchaseImageTab;
