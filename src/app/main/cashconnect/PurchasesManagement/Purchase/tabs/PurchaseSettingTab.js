import TextField from '@material-ui/core/TextField';
import { Autocomplete } from '@material-ui/lab';
import { getBranches, getUsers } from 'app/store/dataSlice';
import React, { useEffect, useState } from 'react';
import { Controller, useFormContext } from 'react-hook-form';
import { useDispatch, useSelector } from 'react-redux';
import {
	GET_EMPLOYEES_WITHOUT_PAGINATION,
	GET_CUSTOMER_WITHOUT_PAGINATION,
	GET_EMPLOYEES,
	GET_PURCHASESTATUS,
	GET_VENDORS
} from '../../../../../constant/constants';

const PurchaseSettingTab = () => {
	const methods = useFormContext();
	const dispatch = useDispatch();
	const {
		control,
		formState: { errors },
		setError,
		clearErrors
	} = methods;
	const purchaseData = useSelector(({ purchasesManagement }) => purchasesManagement.purchase);
	const branches = useSelector(state => state.data.branches);
	const users = useSelector(state => state.data.users);
	const [employees, setEmployees] = useState([]);
	const [purchaseStatus, setPurchaseStatus] = useState([]);
	const [vendors, setVendors] = useState([]);

	useEffect(() => {
		dispatch(getBranches());
		dispatch(getUsers());
		getEmployees();
		getPurchaseStatus();
		getVendors();
	}, []);

	const getEmployees = () => {
		fetch(GET_EMPLOYEES_WITHOUT_PAGINATION)
			.then(response => response.json())
			.then(data => setEmployees(data.employees));
	};
	const getPurchaseStatus = () => {
		fetch(GET_PURCHASESTATUS)
			.then(response => response.json())
			.then(data => setPurchaseStatus(data.purchase_statuses));
	};
	const getVendors = () => {
		fetch(GET_CUSTOMER_WITHOUT_PAGINATION)
			.then(response => response.json())
			.then(data => setVendors(data.customers));
	};

	return (
		<div>
			<Controller
				name="branch"
				control={control}
				render={({ field: { onChange, value } }) => (
					<Autocomplete
						className="mt-8 mb-16"
						freeSolo
						disabled={
							purchaseData?.transfer_status === ('submitted' || 'Submitted') &&
							purchaseData?.purchase_status?.name === ('submitted' || 'Submitted')
						}
						value={value ? branches.find(branch => branch.id === value) : null}
						options={branches}
						getOptionLabel={option => `${option?.name}`}
						InputLabelProps={{ shrink: true }}
						onChange={(event, newValue) => {
							onChange(newValue?.id);
						}}
						//value={employee && employee.branch}
						//defaultValue={{ id: null, name: "Select a branch" }}
						renderInput={params => (
							<TextField
								{...params}
								placeholder="Select a branch"
								label="Branch"
								error={!!errors.branch}
								helperText={errors?.branch?.message}
								variant="outlined"
								autoFocus
								InputLabelProps={{
									shrink: true
								}}
							/>
						)}
					/>
				)}
			/>
			<Controller
				name="vendor"
				control={control}
				render={({ field: { onChange, value } }) => (
					<Autocomplete
						className="mt-8 mb-16"
						freeSolo
						disabled={
							purchaseData?.transfer_status === ('submitted' || 'Submitted') &&
							purchaseData?.purchase_status?.name === ('submitted' || 'Submitted')
						}
						value={value ? vendors.find(vendor => vendor.id === value) : null}
						options={vendors}
						getOptionLabel={option => `${option.first_name} ${option.last_name}`}
						InputLabelProps={{ shrink: true }}
						onChange={(event, newValue) => {
							onChange(newValue?.id);
						}}
						//value={employee && employee.branch}
						//defaultValue={{ id: null, name: "Select a branch" }}
						renderInput={params => (
							<TextField
								{...params}
								placeholder="Select a vendor"
								label="Vendor"
								error={!!errors.vendor}
								helperText={errors?.vendor?.message}
								variant="outlined"
								autoFocus
								InputLabelProps={{
									shrink: true
								}}
							/>
						)}
					/>
				)}
			/>
			<Controller
				name="assigned_to"
				control={control}
				render={({ field: { onChange, value } }) => (
					<Autocomplete
						className="mt-8 mb-16"
						freeSolo
						disabled={
							purchaseData?.transfer_status === ('submitted' || 'Submitted') &&
							purchaseData?.purchase_status?.name === ('submitted' || 'Submitted')
						}
						value={value ? employees.find(employee => employee.id === value) : null}
						options={employees}
						getOptionLabel={option => `${option.username}`}
						InputLabelProps={{ shrink: true }}
						onChange={(event, newValue) => {
							onChange(newValue?.id);
						}}
						renderInput={params => (
							<TextField
								{...params}
								placeholder="Assign a employee"
								label="Assign To"
								error={!!errors.assigned_to}
								helperText={errors?.assigned_to?.message}
								variant="outlined"
								autoFocus
								InputLabelProps={{
									shrink: true
								}}
							/>
						)}
					/>
				)}
			/>

			<Controller
				name="purchase_status"
				control={control}
				render={({ field: { onChange, value } }) => (
					<Autocomplete
						className="mt-8 mb-16"
						freeSolo
						disabled={
							purchaseData?.transfer_status === ('submitted' || 'Submitted') &&
							purchaseData?.purchase_status?.name === ('submitted' || 'Submitted')
						}
						value={value ? purchaseStatus.find(Status => Status.id === value) : null}
						options={purchaseStatus}
						getOptionLabel={option => `${option?.name}`}
						InputLabelProps={{ shrink: true }}
						onChange={(event, newValue) => {
							onChange(newValue?.id);
							const getPurchaseStatusFromOnChange = purchaseStatus?.find(
								purchaseStas => purchaseStas.id === newValue?.id
							)?.name;
							if (
								getPurchaseStatusFromOnChange === 'verified' ||
								getPurchaseStatusFromOnChange === 'Verified'
							) {
								clearErrors('cancelled_by');
								setError('verified_by', {
									type: 'manual',
									message: 'Verified by is required'
								});
							} else if (
								getPurchaseStatusFromOnChange === 'cancelled' ||
								getPurchaseStatusFromOnChange === 'Cancelled'
							) {
								clearErrors('verified_by');
								setError('cancelled_by', {
									type: 'manual',
									message: 'Cancelled by is required'
								});
							} else {
								clearErrors('verified_by');
								clearErrors('cancelled_by');
							}
						}}
						renderInput={params => (
							<TextField
								{...params}
								//placeholder="Select a branch"
								label="Purchase Status"
								error={!!errors.purchase_status}
								helperText={errors?.purchase_status?.message}
								variant="outlined"
								autoFocus
								InputLabelProps={{
									shrink: true
								}}
							/>
						)}
					/>
				)}
			/>
			<Controller
				name="verified_by"
				control={control}
				render={({ field: { onChange, value } }) => (
					<Autocomplete
						className="mt-8 mb-16"
						freeSolo
						disabled={
							purchaseData?.transfer_status === ('submitted' || 'Submitted') &&
							purchaseData?.purchase_status?.name === ('submitted' || 'Submitted')
						}
						value={value ? employees.find(employee => employee.id === value) : null}
						options={employees}
						getOptionLabel={option => `${option.email}`}
						InputLabelProps={{ shrink: true }}
						onChange={(event, newValue) => {
							onChange(newValue?.id);
						}}
						renderInput={params => (
							<TextField
								{...params}
								label="Verified by"
								error={!!errors.verified_by}
								helperText={errors?.verified_by?.message}
								variant="outlined"
								autoFocus
								InputLabelProps={{
									shrink: true
								}}
							/>
						)}
					/>
				)}
			/>
			<Controller
				name="cancelled_by"
				control={control}
				render={({ field: { onChange, value } }) => (
					<Autocomplete
						className="mt-8 mb-16"
						freeSolo
						disabled={
							purchaseData?.transfer_status === ('submitted' || 'Submitted') &&
							purchaseData?.purchase_status?.name === ('submitted' || 'Submitted')
						}
						value={value ? employees.find(employee => employee.id === value) : null}
						options={employees}
						getOptionLabel={option => `${option.email}`}
						InputLabelProps={{ shrink: true }}
						onChange={(event, newValue) => {
							onChange(newValue?.id);
						}}
						renderInput={params => (
							<TextField
								{...params}
								label="Cancelled by"
								error={!!errors.cancelled_by}
								helperText={errors?.cancelled_by?.message}
								variant="outlined"
								autoFocus
								InputLabelProps={{
									shrink: true
								}}
							/>
						)}
					/>
				)}
			/>
		</div>
	);
};

export default PurchaseSettingTab;
