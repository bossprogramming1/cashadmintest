import FuseScrollbars from '@fuse/core/FuseScrollbars';
import _ from '@lodash';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TablePagination from '@material-ui/core/TablePagination';
import TableRow from '@material-ui/core/TableRow';
import DeleteIcon from '@material-ui/icons/Delete';
import { useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { BASE_URL } from '../../../../../constant/constants';
import { removePurchaseProductImage } from '../../store/purchaseSlice';
import { selectPurchases } from '../../store/purchasesSlice';
import PurchaseImageTableHead from './PurchaseImageTableHead';

function PurchaseImageTable(props) {
	const getPurchaseImages = props.getPurchaseImages;
	const purchaseImages = props.purchaseImages;
	const dispatch = useDispatch();
	const purchase = useSelector(selectPurchases);
	const user_role = localStorage.getItem('user_role');
	const [selected, setSelected] = useState([]);
	let serialNumber = 1;
	const [page, setPage] = useState(0);

	const [data, setData] = useState(purchase);

	const [rowsPerPage, setRowsPerPage] = useState(10);
	const [order, setOrder] = useState({
		direction: 'asc',
		id: null
	});

	function handleRequestSort(event, property) {
		const id = property;
		let direction = 'desc';

		if (order.id === property && order.direction === 'desc') {
			direction = 'asc';
		}

		setOrder({
			direction,
			id
		});
	}

	function handleSelectAllClick(event) {
		if (event.target.checked) {
			setSelected(data.map(n => n.id));
			return;
		}
		setSelected([]);
	}

	function handleDeselect() {
		setSelected([]);
	}

	function handleClick(item) {
		props.history.push(`/apps/e-commerce/products/${item.id}/${item.handle}`);
	}

	function handleCheck(event, id) {
		const selectedIndex = selected.indexOf(id);
		let newSelected = [];

		if (selectedIndex === -1) {
			newSelected = newSelected.concat(selected, id);
		} else if (selectedIndex === 0) {
			newSelected = newSelected.concat(selected.slice(1));
		} else if (selectedIndex === selected.length - 1) {
			newSelected = newSelected.concat(selected.slice(0, -1));
		} else if (selectedIndex > 0) {
			newSelected = newSelected.concat(selected.slice(0, selectedIndex), selected.slice(selectedIndex + 1));
		}

		setSelected(newSelected);
	}

	function handleChangePage(event, value) {
		setPage(value);
	}

	function handleChangeRowsPerPage(event) {
		setRowsPerPage(event.target.value);
	}

	function handleRemoveProduct(item) {
		dispatch(removePurchaseProductImage(item.id)).then(() => getPurchaseImages());
	}

	return (
		<div className="w-full flex flex-col">
			<FuseScrollbars className="flex-grow overflow-x-auto">
				<Table stickyHeader className="min-w-xl" aria-labelledby="tableTitle">
					<PurchaseImageTableHead
						//selectedProductIds={selected}
						order={order}
						onSelectAllClick={handleSelectAllClick}
						onRequestSort={handleRequestSort}
						//rowCount={data.length}
						onMenuItemClick={handleDeselect}
					/>

					<TableBody>
						{_.orderBy(
							purchaseImages,
							[
								o => {
									switch (order.id) {
										case 'categories': {
											return o.categories[0];
										}
										default: {
											return o[order.id];
										}
									}
								}
							],
							[order.direction]
						)
							.slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
							.map(n => {
								const isSelected = selected.indexOf(n.id) !== -1;
								return (
									<TableRow
										//className="h-52 cursor-pointer"
										hover
										role="checkbox"
										aria-checked={isSelected}
										tabIndex={-1}
										key={n.id}
										selected={isSelected}
									>
										<TableCell className="whitespace-nowrap p-4 md:p-16" component="th" scope="row">
											{serialNumber++}
										</TableCell>

										<TableCell
											className="w-52 px-4 md:px-0"
											component="th"
											scope="row"
											padding="none"
										>
											{n.length > 0 && n.featuredImageId ? (
												<img
													className="w-full block rounded"
													src={_.find(n.thumbnail, { id: n.featuredImageId }).url}
													alt={n.id}
												/>
											) : n.length > 0 && n.featuredImageId ? (
												<img
													style={{ borderRadius: '50%' }}
													className="w-full block rounded"
													src={`${BASE_URL}${n.image}`}
													alt={n?.name}
												/>
											) : null}
										</TableCell>

										<TableCell className="whitespace-nowrap p-4 md:p-16" component="th" scope="row">
											{n.description}
										</TableCell>

										<TableCell className="whitespace-nowrap p-4 md:p-16" component="th" scope="row">
											<div>
												<DeleteIcon
													className="h-52 cursor-pointer"
													style={{
														color: 'red',
														visibility:
															user_role === 'ADMIN' || user_role === 'admin'
																? 'visible'
																: 'hidden'
													}}
													onClick={event => handleRemoveProduct(n, 'deleteProduct')}
												/>
											</div>
										</TableCell>
									</TableRow>
								);
							})}
					</TableBody>

					{/* <TableBody>
                        {props.productImages.map(n => {
                            <TableRow>

                                <TableCell 	className="p-4 md:p-16" component="th" scope="row">
                                    {n.id}
                                </TableCell>

                                <TableCell 	className="p-4 md:p-16" component="th" scope="row">
                                    <Typography>Hello</Typography>
                                </TableCell>

                                <TableCell
                                    className="w-52 px-4 md:px-0"
                                    component="th"
                                    scope="row"
                                    padding="none"
                                >
                                    <img
                                        style={{ borderRadius: '50%' }}
                                        className="w-full block rounded"
                                        src={`${BASE_URL}${n.image}`}
                                        alt={n.id}
                                    />
                                </TableCell>

                                <TableCell>
                                    <div>
                                        <EditIcon className="h-52 cursor-pointer" style={{ color: 'green' }}  /> <DeleteIcon className="h-52 cursor-pointer" style={{ color: 'red' }} onClick={event => handleDeleteProductImage(n, "deleteProduct")} />
                                    </div>
                                </TableCell>

                            </TableRow>
                        })}
                    </TableBody> */}
				</Table>
			</FuseScrollbars>

			<TablePagination
				className="flex-shrink-0 border-t-1"
				component="div"
				//count={data.length}
				rowsPerPage={rowsPerPage}
				page={page}
				backIconButtonProps={{
					'aria-label': 'Previous Page'
				}}
				nextIconButtonProps={{
					'aria-label': 'Next Page'
				}}
				onChangePage={handleChangePage}
				onChangeRowsPerPage={handleChangeRowsPerPage}
			/>
		</div>
	);
}

export default PurchaseImageTable;
