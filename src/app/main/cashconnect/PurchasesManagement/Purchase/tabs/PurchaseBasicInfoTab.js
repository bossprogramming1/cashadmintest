import Checkbox from '@material-ui/core/Checkbox';
import FormControl from '@material-ui/core/FormControl';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import FormLabel from '@material-ui/core/FormLabel';
import Icon from '@material-ui/core/Icon';
import InputAdornment from '@material-ui/core/InputAdornment';
import Radio from '@material-ui/core/Radio';
import RadioGroup from '@material-ui/core/RadioGroup';
import { makeStyles } from '@material-ui/core/styles';
import TextField from '@material-ui/core/TextField';
import React from 'react';
import { Controller, useFormContext } from "react-hook-form";
import { useParams } from 'react-router-dom';

const useStyles = makeStyles(theme => ({
    hidden: {
        display: "none"
    }
}));

function PurchaseBasicInfoTab(props) {

    const userID = localStorage.getItem('UserID')
    const classes = useStyles(props);

    const methods = useFormContext();
    const { control, formState } = methods;
    const { errors } = formState;

    const routeParams = useParams();
    const { purchaseId } = routeParams;

    return (
        <div>

            <Controller
                name="first_name"
                control={control}
                render={({ field }) => {
                    return (<TextField
                        {...field}
                        disabled
                        className="mt-8 mb-16"
                        error={!!errors.first_name}
                        helperText={errors?.first_name?.message}
                        label="First Name"
                        id="first_name"
                        required
                        variant="outlined"
                        InputLabelProps={field.value && { shrink: true }}
                        fullWidth
                    />)
                }}
            />

            <Controller
                name="last_name"
                control={control}
                render={({ field }) => {
                    return (<TextField
                        {...field}
                        disabled
                        className="mt-8 mb-16"
                        error={!!errors.last_name}
                        helperText={errors?.last_name?.message}
                        label="Last Name"
                        id="last_name"
                        required
                        variant="outlined"
                        InputLabelProps={field.value && { shrink: true }}
                        fullWidth
                    />)
                }}
            />

            <Controller
                name="contact_no"
                control={control}
                render={({ field }) => (
                    <TextField
                        {...field}
                        disabled
                        className="mt-8 mb-16"
                        error={!!errors.primary_phone}
                        helperText={errors?.primary_phone?.message}
                        label="Contact"
                        id="contact"
                        variant="outlined"
                        fullWidth
                        InputLabelProps={{ shrink: true }}
                    />
                )}
            />

            <Controller
                name="email"
                control={control}
                render={({ field }) => (
                    <TextField
                        {...field}
                        disabled
                        className="mt-8 mb-16"
                        type="text"
                        error={!!errors.email}
                        required
                        helperText={errors?.email?.message}
                        label="Email"
                        InputProps={{
                            endAdornment: (
                                <InputAdornment position="end">
                                    <Icon className="text-20" color="action">
                                        user
                                    </Icon>
                                </InputAdornment>
                            )
                        }}
                        variant="outlined"
                        fullWidth
                        InputLabelProps={{ shrink: true }}
                    />
                )}
            />
            {/* <Controller
                name="request_date"
                control={control}
                render={({ field }) => (
                    <TextField
                        {...field}
                        className="mt-8 mb-16"
                        id="date"
                        label="Purchase Request Date"
                        error={!!errors.request_date}
                        required
                        helperText={errors?.request_date?.message}
                        type="date"
                        //defaultValue="2017-05-24"
                        fullWidth
                        InputLabelProps={{
                            shrink: true,
                        }}
                    />
                )}
            /> */}
            <Controller
                name="street_address"
                control={control}
                render={({ field }) => (
                    <TextField
                        {...field}
                        disabled
                        className="mt-8 mb-16"
                        error={!!errors.street_address}
                        helperText={errors?.street_address?.message}
                        required
                        label="Street address"
                        id="street_address"
                        variant="outlined"
                        fullWidth
                        InputLabelProps={{ shrink: true }}
                    />
                )}
            />
            <Controller
                name="is_subscribed"
                control={control}
                render={({ field }) => (
                    <FormControl>
                        <FormControlLabel
                            className="mt-8 mb-16"
                            required
                            disabled
                            label="Subscribe to our newsletter"
                            id="is_subscribe"
                            control={<Checkbox
                                {...field}
                                checked={field.value ? field.value : false}
                            />}
                        />
                    </FormControl>
                )}
            /><br />
            <Controller
                name="contact_type"
                control={control}
                render={({ field }) => (
                    <FormControl component="fieldset">
                        <FormLabel component="legend">How would you like us to contact you?</FormLabel>
                        <RadioGroup
                            // {...field}
                            // checked={field.value? value: false}
                            row aria-label="position"
                            name="position"
                            defaultValue="top"
                        >
                            <FormControlLabel
                                {...field}
                                disabled
                                value="email"
                                control={<Radio
                                    checked={field.value === "email" ? field.value : false}
                                    color="primary"
                                />
                                }
                                label="Email"
                            />
                            <FormControlLabel
                                {...field}
                                disabled
                                value="sms"
                                control={<Radio
                                    checked={field.value === "sms" ? field.value : false}
                                    color="primary"
                                />}
                                label="SMS"
                            />
                            <FormControlLabel
                                {...field}
                                disabled
                                value="cell"
                                control={<Radio
                                    checked={field.value === "cell" ? field.value : false}
                                    color="primary"
                                />}
                                label="Cell"
                            />
                        </RadioGroup>
                    </FormControl>
                )}
            />
        </div>
    );
}

export default PurchaseBasicInfoTab