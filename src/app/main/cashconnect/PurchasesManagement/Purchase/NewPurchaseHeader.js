import _ from '@lodash';
import Button from '@material-ui/core/Button';
import Icon from '@material-ui/core/Icon';
import { useTheme } from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';
import { motion } from 'framer-motion';
import React, { useContext } from 'react';
import { useFormContext } from 'react-hook-form';
import { useDispatch } from 'react-redux';
import { Link, useHistory, useParams } from 'react-router-dom';
import { removePurchase, savePurchase, updatePurchase } from '../store/purchaseSlice';
import ItemContext from './ItemContext';

const NewPurchaseHeader = () => {
	const [items, setItems] = useContext(ItemContext);
	const dispatch = useDispatch();
	const methods = useFormContext();
	const { formState, watch, getValues } = methods;
	const { isValid, dirtyFields } = formState;
	const name = watch('name');
	const theme = useTheme();
	const history = useHistory();
	const routeParams = useParams();
	const { purchaseId } = routeParams;
	const handleDelete = localStorage.getItem('deletePurchase');

	function handleSavePurchase() {
		const data = getValues();

		const purchaseData = {
			first_name: data.first_name,
			last_name: data.last_name,
			contact_no: data.contact_no,
			email: data.email,
			contact_type: data.contact_type,
			request_date: data.request_date,
			street_address: data.street_address,
			is_subscribed: data.is_subscribed,
			items
		};
		dispatch(savePurchase(purchaseData)).then(() => {
			history.push('/apps/purchase-management/purchase-requests');
		});
	}

	function handleUpdatePurchase() {
		const data = getValues();
		const updatePurchaseData = {
			first_name: data.first_name,
			last_name: data.last_name,
			contact_no: data.contact_no,
			email: data.email,
			contact_type: data.contact_type,
			request_date: data.request_date,
			street_address: data.street_address,
			is_subscribed: data.is_subscribed,
			items,
			branch: data.branch,
			vendor: data.vendor,
			verified_by: data.verified_by,
			cancelled_by: data.cancelled_by,
			purchase_status: data.purchase_status,
			assigned_to: data.assigned_to
		};
		dispatch(updatePurchase(updatePurchaseData)).then(() => {
			history.push('/apps/purchase-management/purchase-requests');
		});
	}

	function handleRemovePurchase() {
		dispatch(removePurchase(purchaseId)).then(() => {
			localStorage.removeItem('purchaseEvent');
			history.push('/apps/purchase-management/purchase-requests');
		});
	}

	function handleCancel() {
		localStorage.removeItem('updatePurchase');
		localStorage.removeItem('deletePurchase');
		history.push('/apps/purchase-management/purchase-requests');
	}

	return (
		<div className="flex flex-1 w-full items-center justify-between">
			<div className="flex flex-col items-start max-w-full min-w-0">
				<motion.div initial={{ x: 20, opacity: 0 }} animate={{ x: 0, opacity: 1, transition: { delay: 0.3 } }}>
					<Typography
						className="flex items-center sm:mb-12"
						component={Link}
						role="button"
						to="/apps/purchase-management/purchase-requests"
						color="inherit"
					>
						<Icon className="text-20">{theme.direction === 'ltr' ? 'arrow_back' : 'arrow_forward'}</Icon>
						<span className="hidden sm:flex mx-4 font-medium">Purchases</span>
					</Typography>
				</motion.div>

				<div className="flex items-center max-w-full">
					<motion.div
						className="hidden sm:flex"
						initial={{ scale: 0 }}
						animate={{ scale: 1, transition: { delay: 0.3 } }}
					/>

					<div className="flex flex-col min-w-0 mx-8 sm:mc-16">
						<motion.div initial={{ x: -20 }} animate={{ x: 0, transition: { delay: 0.3 } }}>
							<Typography className="text-16 sm:text-20 truncate font-semibold">
								{name || 'Create New Purchase'}
							</Typography>
							<Typography variant="caption" className="font-medium">
								Purchases Detail
							</Typography>
						</motion.div>
					</div>
				</div>
			</div>
			<motion.div
				className="flex"
				initial={{ opacity: 0, x: 20 }}
				animate={{ opacity: 1, x: 0, transition: { delay: 0.3 } }}
			>
				{handleDelete === 'deletePurchase' && (
					<Typography className="mt-6" variant="subtitle2">
						Do you want to remove this Purchase?
					</Typography>
				)}
				{handleDelete === 'deletePurchase' && routeParams.purchaseId !== 'new' && (
					<Button
						className="whitespace-nowrap mx-4"
						variant="contained"
						color="secondary"
						onClick={handleRemovePurchase}
						startIcon={<Icon className="hidden sm:flex">delete</Icon>}
						style={{ backgroundColor: '#ea5b78', color: 'white' }}
					>
						Remove
					</Button>
				)}
				{routeParams.purchaseId === 'new' && (
					<Button
						className="whitespace-nowrap mx-4"
						variant="contained"
						color="secondary"
						disabled={_.isEmpty(dirtyFields) || !isValid}
						onClick={handleSavePurchase}
					>
						Save
					</Button>
				)}
				{handleDelete !== 'deletePurchase' && routeParams?.purchaseName && (
					<Button
						className="whitespace-nowrap mx-4"
						color="secondary"
						variant="contained"
						style={{ backgroundColor: '#4dc08e', color: 'white' }}
						onClick={handleUpdatePurchase}
					>
						Update
					</Button>
				)}
				<Button
					className="whitespace-nowrap mx-4"
					variant="contained"
					style={{ backgroundColor: '#FFAA4C', color: 'white' }}
					onClick={handleCancel}
				>
					Cancel
				</Button>
			</motion.div>
		</div>
	);
};

export default NewPurchaseHeader;
