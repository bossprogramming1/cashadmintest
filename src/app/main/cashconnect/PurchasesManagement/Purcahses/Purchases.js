import FusePageCarded from '@fuse/core/FusePageCarded';
import { PURCHASE_REQUEST_LIST } from 'app/constant/permission/permission';
import { getUserPermissions } from 'app/store/dataSlice';
import withReducer from 'app/store/withReducer';
import React, { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import PagenotFound from '../../Pagenotfound/PagenotFound';
import reducer from '../store/index';
import PurchasesHeader from './PurchasesHeader';
import PurchasesTable from './PurchasesTable';

const Purchases = () => {
	const UserPermissions = useSelector(state => state.data.UserPermissions);
	useEffect(() => {
		dispatch(getUserPermissions());
	}, []);
	const dispatch = useDispatch();
	return (
		<FusePageCarded
			classes={{
				content: 'flex',
				contentCard: 'overflow-hidden',
				header: 'min-h-74 h-64'
			}}
			header={UserPermissions.includes(PURCHASE_REQUEST_LIST) && <PurchasesHeader />}
			content={UserPermissions.includes(PURCHASE_REQUEST_LIST) ? <PurchasesTable /> : <PagenotFound />}
			innerScroll
		/>
	);
};
export default withReducer('purchasesManagement', reducer)(Purchases);
