import FusePageCarded from '@fuse/core/FusePageCarded';
import withReducer from 'app/store/withReducer';
import React, { useEffect } from 'react';
import { useDispatch } from 'react-redux';
import { useParams } from 'react-router-dom';
import reducer from '../../store/index';
import { getPurchase } from '../../store/purchaseSlice';
import PurchaseItemsHeader from './PurchaseItemsHeader';
import PurchaseItemsTable from './PurchaseItemsTable';

const PurchasesItems = () => {
	const routeParams = useParams();
	const { purchaseId } = routeParams;
	const dispatch = useDispatch();

	useEffect(() => {
		dispatch(getPurchase(purchaseId));
	});
	return (
		<FusePageCarded
			classes={{
				content: 'flex',
				contentCard: 'overflow-hidden',
				header: 'min-h-74 h-64'
			}}
			header={<PurchaseItemsHeader />}
			content={<PurchaseItemsTable />}
			innerScroll
		/>
	);
};
export default withReducer('purchasesManagement', reducer)(PurchasesItems);
