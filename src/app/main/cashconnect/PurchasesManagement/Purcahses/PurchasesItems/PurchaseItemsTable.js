import FuseScrollbars from '@fuse/core/FuseScrollbars';
import _ from '@lodash';
import Checkbox from '@material-ui/core/Checkbox';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TablePagination from '@material-ui/core/TablePagination';
import TableRow from '@material-ui/core/TableRow';
import TextField from '@material-ui/core/TextField';
import DeleteIcon from '@material-ui/icons/Delete';
import DoneOutlineIcon from '@material-ui/icons/DoneOutline';
import EditIcon from '@material-ui/icons/Edit';
import axios from 'axios';
import clsx from 'clsx';
import React, { useEffect, useState } from 'react';
import { Controller, useForm } from 'react-hook-form';
import { useDispatch, useSelector } from 'react-redux';
import { useParams, withRouter } from 'react-router-dom';
import { addNotification } from 'app/fuse-layouts/shared-components/notificationPanel/store/dataSlice';
import NotificationModel from 'app/fuse-layouts/shared-components/notificationPanel/model/NotificationModel';
import { BASE_URL, CREATE_PURCHASE_FINAL, DELETE_PURCHASE_REQ_ITEM } from '../../../../../constant/constants';
import { getPurchase } from '../../store/purchaseSlice';
import { getPurchases, selectPurchases } from '../../store/purchasesSlice';
import PurchaseItemsTableHead from './PurchaseItemsTableHead';

const PurchaseItemsTable = props => {
	const routeParams = useParams();
	const { purchaseId } = routeParams;
	const dispatch = useDispatch();
	const purchases = useSelector(selectPurchases);
	const purchase = purchases.find(item => item.id === purchaseId);
	const purchaseItem = useSelector(state => state.purchasesManagement.purchase?.purchase_request_items);
	const searchText = useSelector(({ purchasesManagement }) => purchasesManagement.purchases.searchText);
	const [loading, setLoading] = useState(true);
	const [selected, setSelected] = useState([]);
	const [data, setData] = useState(purchaseItem);
	const [editableRowIds, setEditableRowIds] = useState({});
	const [purchaseItemStatus, setPurchaseItemStatus] = useState('');
	const [randomState, setRandomState] = useState(false);
	const [page, setPage] = useState(0);
	const [rowsPerPage, setRowsPerPage] = useState(10);
	const [order, setOrder] = useState({
		direction: 'asc',
		id: null
	});
	const { control, register, watch, formState, reset, getValues, setError, handleSubmit } = useForm();
	const { errors } = formState;
	const formValues = getValues();
	let serialNumber = 1;
	const [deleteItem, setDeleteItem] = useState(true);

	useEffect(() => {
		dispatch(getPurchases()).then(() => setLoading(false));
	}, [dispatch]);

	useEffect(() => {
		if (searchText.length !== 0) {
			setData(_.filter(purchases, item => item?.name.toLowerCase().includes(searchText.toLowerCase())));
			setPage(0);
		} else {
			setData(purchases);
		}
	}, [purchases, searchText]);

	function handleRequestSort(purchaseEvent, property) {
		const id = property;
		let direction = 'desc';

		if (order.id === property && order.direction === 'desc') {
			direction = 'asc';
		}

		setOrder({
			direction,
			id
		});
	}

	function handleSelectAllClick(purchaseEvent) {
		if (purchaseEvent.target.checked) {
			setSelected(data.map(n => n.id));
			return;
		}
		setSelected([]);
	}

	function handleDeselect() {
		setSelected([]);
	}

	function handleUpdatePurchase(item, purchaseEvent) {
		localStorage.removeItem('deletePurchase');
		localStorage.setItem('updatePurchase', purchaseEvent);
		props.history.push(`/apps/purchase-management/purchase-requests/${item.id}/${item.first_name}`);
	}
	function handleDeletePurchaseItem(item, purchaseEvent) {
		fetch(`${DELETE_PURCHASE_REQ_ITEM}${purchaseId}/${item.id}`, {
			method: 'DELETE',
			headers: {
				'content-type': 'application/json',
				Authorization: localStorage.getItem('jwt_access_token')
			}
		})
			.then(response => response.json())
			.then(resData => dispatch(getPurchase(purchaseId)));
	}

	//update product item
	function updatePurchaseRequestItem(itmId) {
		const purchaseItemPrice = watch(`purchase_price${itmId}`);
		const purchaseItemSellPrice = watch(`sell_price${itmId}`);
		if (purchaseItemSellPrice == 0) {
			dispatch(
				addNotification(
					NotificationModel({
						message: `Please add sell price`,
						options: { variant: 'error' },
						item_id: purchaseItemPrice
					})
				)
			);
		} else {
			const updatePurchaseRequestItemData = {
				purchase_req_id: purchaseId,
				purchase_req_item_id: itmId,
				purchase_price: purchaseItemPrice,
				sell_price: purchaseItemSellPrice
			};

			const authTOKEN = {
				headers: {
					'Content-type': 'application/json',
					Authorization: localStorage.getItem('jwt_access_token')
				}
			};
			if (
				updatePurchaseRequestItemData.purchase_req_id &&
				updatePurchaseRequestItemData.purchase_req_item_id &&
				updatePurchaseRequestItemData.purchase_price &&
				updatePurchaseRequestItemData.sell_price
			) {
				axios.post(`${CREATE_PURCHASE_FINAL}`, updatePurchaseRequestItemData, authTOKEN).then(res => {
					dispatch(getPurchase(purchaseId));
				});
			}
		}
	}

	function handleCheck(purchaseEvent, id) {
		const selectedIndex = selected.indexOf(id);
		let newSelected = [];

		if (selectedIndex === -1) {
			newSelected = newSelected.concat(selected, id);
		} else if (selectedIndex === 0) {
			newSelected = newSelected.concat(selected.slice(1));
		} else if (selectedIndex === selected.length - 1) {
			newSelected = newSelected.concat(selected.slice(0, -1));
		} else if (selectedIndex > 0) {
			newSelected = newSelected.concat(selected.slice(0, selectedIndex), selected.slice(selectedIndex + 1));
		}

		setSelected(newSelected);
	}

	function handleChangePage(purchaseEvent, value) {
		setPage(value);
	}

	function handleChangeRowsPerPage(purchaseEvent) {
		setRowsPerPage(purchaseEvent.target.value);
	}

	return (
		<div className="w-full flex flex-col">
			<FuseScrollbars className="flex-grow overflow-x-auto">
				<Table stickyHeader className="min-w-xl" aria-labelledby="tableTitle">
					<PurchaseItemsTableHead
						selectedPurchaseIds={selected}
						order={order}
						onSelectAllClick={handleSelectAllClick}
						onRequestSort={handleRequestSort}
						onMenuItemClick={handleDeselect}
					/>

					<TableBody>
						{_.orderBy(
							purchaseItem,
							[
								o => {
									switch (order.id) {
										case 'categories': {
											return o.categories[0];
										}
										default: {
											return o[order.id];
										}
									}
								}
							],
							[order.direction]
						)
							.slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
							.map(n => {
								const isSelected = selected.indexOf(n.id) !== -1;
								return (
									<TableRow
										hover
										role="checkbox"
										aria-checked={isSelected}
										tabIndex={-1}
										key={n.id}
										selected={isSelected}
									>
										<TableCell
											whitespace-nowrap
											className="w-40 md:w-64 text-center"
											padding="none"
										>
											<Checkbox
												checked={isSelected}
												onClick={purchaseEvent => purchaseEvent.stopPropagation()}
												onChange={purchaseEvent => handleCheck(purchaseEvent, n.id)}
											/>
										</TableCell>

										<TableCell
											whitespace-nowrap
											className="w-40 md:w-64"
											component="th"
											scope="row"
										>
											{serialNumber++}
										</TableCell>

										<TableCell className="whitespace-nowrap p-4 md:p-16" component="th" scope="row">
											<img
												style={{ height: '100px', width: '100px' }}
												src={`${BASE_URL}${n?.purchase_request_item_images[0]?.image}`}
												alt="Not found"
											/>
										</TableCell>

										<TableCell className="whitespace-nowrap p-4 md:p-16" component="th" scope="row">
											{n?.name}
										</TableCell>

										<TableCell className="whitespace-nowrap p-4 md:p-16" component="th" scope="row">
											{n.quantity}
										</TableCell>

										{/* <TableCell 	className="p-4 md:p-16" component="th" scope="row">
                                            {n.unit_price}
                                        </TableCell> */}
										<TableCell>
											{editableRowIds[n.id] ? (
												<Controller
													name={`purchase_price${n.id}`}
													control={control}
													render={({ field }) => {
														return (
															<TextField
																style={{ width: '75px', marginTop: '25px' }}
																{...field}
																autoFocus
																size="small"
																error={!!errors.purchase_price}
																helperText={errors?.purchase_price?.message}
																id={`purchase_price${n.id}`}
																required
																variant="outlined"
																InputLabelProps={field.value && { shrink: true }}
																fullWidth
															/>
														);
													}}
												/>
											) : (
												<TableCell
													whitespace-nowrap
													className="p-4 md:p-16"
													component="th"
													scope="row"
												>
													{n.unit_price}
												</TableCell>
											)}
										</TableCell>
										{/* {editableRowIds[n.id] ? (
                                            <Controller
                                                name={`sell_price${n.id}`}
                                                control={control}
                                                render={({ field }) => {
                                                    return (
                                                        <TextField
                                                            style={{ width: '75px', marginTop: '25px' }}
                                                            {...field}
                                                            autoFocus
                                                            size="small"
                                                            error={!!errors.sell_price}
                                                            helperText={
                                                                errors?.sell_price?.message
                                                            }
                                                            id={`sell_price${n.id}`}
                                                            required
                                                            variant="outlined"
                                                            InputLabelProps={
                                                                field.value && { shrink: true }
                                                            }
                                                            fullWidth
                                                        />
                                                    );
                                                }}
                                            />
                                        )
                                         : (
                                            <TableCell
                                                className="p-4 md:p-16" component="th" scope="row"
                                            >
                                                {n?.sell_price || 0}
                                            </TableCell>
                                        )
                                        
                                        } */}
										{/* <TableCell 	className="p-4 md:p-16" component="th" scope="row">
                                            {n.is_added_to_purchasefinalitem_product_inventory}
                                        </TableCell> */}

										<TableCell>
											{editableRowIds[n.id] ? (
												<Controller
													name={`sell_price${n.id}`}
													control={control}
													render={({ field }) => {
														return (
															<TextField
																style={{ width: '75px', marginTop: '25px' }}
																{...field}
																size="small"
																error={!!errors.sell_price}
																helperText={errors?.sell_price?.message}
																id={`sell_price${n.id}`}
																required
																variant="outlined"
																InputLabelProps={field.value && { shrink: true }}
																fullWidth
															/>
														);
													}}
												/>
											) : (
												<TableCell
													whitespace-nowrap
													className="p-4 md:p-16"
													component="th"
													scope="row"
												>
													{n?.sell_price || 0}
												</TableCell>
											)}
										</TableCell>
										<TableCell className="whitespace-nowrap p-4 md:p-16" component="th" scope="row">
											{/* <div style={{ backgroundColor: '#4FC3F7', borderRadius: '5px' }}> {purchase.purchase_status}</div> */}
											<div
												className={clsx(
													'inline text-12 font-semibold py-4 px-12 rounded-full truncate text-white',
													_.isEmpty(n) ||
														n.is_added_to_purchasefinalitem_product_inventory === true
														? 'bg-green-800'
														: _.isEmpty(n) ||
														  n.is_added_to_purchasefinalitem_product_inventory === false
														? 'bg-orange'
														: ''
													// _.isEmpty(n) || n.is_added_to_purchasefinalitem_product_inventory === ('complete' || 'Complete') ? 'bg-green-800' :
													//     _.isEmpty(n) || n.is_added_to_purchasefinalitem_product_inventory ? 'bg-black' : ''
												)}
											>
												{_.isEmpty(n) ||
												n.is_added_to_purchasefinalitem_product_inventory === true
													? 'Yes'
													: 'No'}
											</div>
										</TableCell>

										<TableCell
											whitespace-nowrap
											className="p-4 md:p-16"
											align="center"
											component="th"
											scope="row"
										>
											<div>
												{editableRowIds[n.id] ? (
													<DoneOutlineIcon
														style={{ color: 'green' }}
														className="cursor-pointer"
														onClick={e => {
															setEditableRowIds({
																...editableRowIds,
																[n.id]: false
															});
															updatePurchaseRequestItem(n.id);
														}}
													/>
												) : (
													n.is_added_to_purchasefinalitem_product_inventory === !true && (
														<EditIcon
															style={{ color: 'green' }}
															className="cursor-pointer"
															onClick={e => {
																setEditableRowIds({
																	...editableRowIds,
																	[n.id]: true
																});
																reset({
																	...formValues,
																	[`purchase_price${n.id}`]: n.unit_price,
																	[`sell_price${n.id}`]: n?.sell_price || 0
																});
															}}
														/>
													)
												)}{' '}
												{/* <DeleteIcon onClick={event => handleDeletePurchaseItem(n, event)} className="h-52 cursor-pointer" style={{ color: 'red' }} /> */}
											</div>
										</TableCell>
									</TableRow>
								);
							})}
					</TableBody>
				</Table>
			</FuseScrollbars>

			<TablePagination
				className="flex-shrink-0 border-t-1"
				rowsPerPageOptions={[5, 10, 25]}
				component="div"
				//count={data.length}
				rowsPerPage={rowsPerPage}
				page={page}
				backIconButtonProps={{
					'aria-label': 'Previous Page'
				}}
				nextIconButtonProps={{
					'aria-label': 'Next Page'
				}}
				onChangePage={handleChangePage}
				onChangeRowsPerPage={handleChangeRowsPerPage}
			/>
		</div>
	);
};

export default withRouter(PurchaseItemsTable);
