/* eslint-disable react-hooks/exhaustive-deps */
import FuseScrollbars from '@fuse/core/FuseScrollbars';
import _ from '@lodash';
import Checkbox from '@material-ui/core/Checkbox';
import { makeStyles } from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import clsx from 'clsx';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TablePagination from '@material-ui/core/TablePagination';
import TableRow from '@material-ui/core/TableRow';
import ReceiptIcon from '@material-ui/icons/Receipt';
import PrintIcon from '@material-ui/icons/Print';
import moment from 'moment';
import DeleteIcon from '@material-ui/icons/Delete';
import EditIcon from '@material-ui/icons/Edit';
import VisibilityIcon from '@material-ui/icons/Visibility';
import Pagination from '@material-ui/lab/Pagination';
import React, { useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { withRouter } from 'react-router-dom';
import { motion } from 'framer-motion';
import { format } from 'date-fns';
import { SEARCH_PURCHASE_REQ } from 'app/constant/constants';
import { getPurchaseRequestStatus } from 'app/store/dataSlice';
import { Tooltip, Typography } from '@material-ui/core';
import FuseLoading from '@fuse/core/FuseLoading';
import { getPurchases, selectPurchases, setPurchasesSearchText } from '../store/purchasesSlice';
import PurchasesTableHead from './PurchasesTableHead';

const useStyles = makeStyles(theme => ({
	root: {
		display: 'flex',
		justifyContent: 'space-between',
		flexWrap: 'wrap',
		'& > *': {
			marginTop: theme.spacing(2),
			marginBottom: theme.spacing(3)
		}
	}
}));

const PurchasesTable = props => {
	const classes = useStyles();
	const dispatch = useDispatch();
	const purchases = useSelector(selectPurchases);
	const purchaseStatus = useSelector(state => state.data.purchaseStatus);
	const searchText = useSelector(({ purchasesManagement }) => purchasesManagement.purchases.searchText);
	const [searchPurchaseReq, setSearchPurchaseReq] = useState([]);
	const [loading, setLoading] = useState(true);
	const [selected, setSelected] = useState([]);
	const [data, setData] = useState(purchases);
	const [page, setPage] = useState(0);
	const [rowsPerPage, setRowsPerPage] = useState(10);
	const [pageAndSize, setPageAndSize] = useState({ page: 1, size: 10 });
	const user_role = localStorage.getItem('user_role');
	const totalPages = sessionStorage.getItem('total_purchases_pages');
	const totalElements = sessionStorage.getItem('total_purchases_elements');
	const [order, setOrder] = useState({
		direction: 'asc',
		id: null
	});
	let serialNumber = 1;
	const [deleteItem, setDeleteItem] = useState('');
	useEffect(() => {
		const event = '';
		dispatch(getPurchaseRequestStatus());
		dispatch(setPurchasesSearchText(event));
	}, []);
	useEffect(() => {
		dispatch(getPurchases(pageAndSize)).then(() => setLoading(false));
	}, [dispatch]);

	// search purchase_req
	useEffect(() => {
		searchText !== '' && getSearchPurchaseReq();
	}, [searchText]);

	const getSearchPurchaseReq = () => {
		fetch(`${SEARCH_PURCHASE_REQ}?keyword=${searchText}`)
			.then(response => response.json())
			.then(searchedPurchaseReqData => {
				setSearchPurchaseReq(searchedPurchaseReqData.purchase_requests);
			});
	};

	function handleRequestSort(purchaseEvent, property) {
		const id = property;
		let direction = 'desc';

		if (order.id === property && order.direction === 'desc') {
			direction = 'asc';
		}

		setOrder({
			direction,
			id
		});
	}

	function handleSelectAllClick(purchaseEvent) {
		if (purchaseEvent.target.checked) {
			setSelected(purchases.map(n => n.id));
			return;
		}
		setSelected([]);
	}

	function handleDeselect() {
		setSelected([]);
	}

	// pagination
	const handlePagination = (e, handlePage) => {
		setPageAndSize({ ...pageAndSize, page: handlePage });
		setPage(handlePage - 1);
		dispatch(getPurchases({ ...pageAndSize, page: handlePage }));
	};

	function handleChangePage(event, value) {
		setPage(value);
		setPageAndSize({ ...pageAndSize, page: value + 1 });
		dispatch(getPurchases({ ...pageAndSize, page: value + 1 }));
	}

	function handleChangeRowsPerPage(event) {
		setRowsPerPage(event.target.value);
		setPageAndSize({ ...pageAndSize, size: event.target.value });
		dispatch(getPurchases({ ...pageAndSize, size: event.target.value }));
	}

	// updatePurchase
	function handleUpdatePurchase(item, purchaseEvent) {
		localStorage.removeItem('deletePurchase');
		localStorage.setItem('updatePurchase', purchaseEvent);
		props.history.push(`/apps/purchase-management/purchase-requests/${item.id}/${item.first_name}`);
	}
	function handleDeletePurchase(item, purchaseEvent) {
		localStorage.removeItem('updatePurchase');
		localStorage.setItem('deletePurchase', purchaseEvent);
		props.history.push(`/apps/purchase-management/purchase-requests/${item.id}/${item.first_name}`);
	}

	function handleViewItem(item, purchaseEvent) {
		localStorage.removeItem('deletePurchase');
		localStorage.removeItem('updatePurchase');
		localStorage.setItem('viewPurchaseItem', purchaseEvent);
		props.history.push(`/apps/purchase-management/purchase-request/items/${item.id}/${item.first_name}`);
	}

	// handlePrintInvoice
	function handlePrintInvoice(item, purchaseEvent) {
		props.history.push(`/apps/purchase-management/purchase-request/purchase-invoice/${item.id}/${item.first_name}`);
	}

	function handleCheck(purchaseEvent, id) {
		const selectedIndex = selected.indexOf(id);
		let newSelected = [];

		if (selectedIndex === -1) {
			newSelected = newSelected.concat(selected, id);
		} else if (selectedIndex === 0) {
			newSelected = newSelected.concat(selected.slice(1));
		} else if (selectedIndex === selected.length - 1) {
			newSelected = newSelected.concat(selected.slice(0, -1));
		} else if (selectedIndex > 0) {
			newSelected = newSelected.concat(selected.slice(0, selectedIndex), selected.slice(selectedIndex + 1));
		}

		setSelected(newSelected);
	}

	if (loading) {
		return <FuseLoading />;
	}

	if (purchases?.length === 0) {
		return (
			<motion.div
				initial={{ opasize: 0 }}
				animate={{ opasize: 1, transition: { delay: 0.1 } }}
				className="flex flex-1 items-center justify-center h-full"
			>
				<Typography size="textSecondary" variant="h5">
					There are no Purchage Request!
				</Typography>
			</motion.div>
		);
	}

	return (
		<div className="w-full flex flex-col">
			<FuseScrollbars className="flex-grow overflow-x-auto">
				<Table stickyHeader className="min-w-xl" aria-labelledby="tableTitle">
					<PurchasesTableHead
						selectedPurchaseIds={selected}
						order={order}
						onSelectAllClick={handleSelectAllClick}
						onRequestSort={handleRequestSort}
						rowCount={purchases.length}
						onMenuItemClick={handleDeselect}
					/>

					<TableBody>
						{_.orderBy(
							searchText !== '' && searchPurchaseReq ? searchPurchaseReq : purchases,
							[
								o => {
									switch (order.id) {
										case 'categories': {
											return o.categories[0];
										}
										default: {
											return o[order.id];
										}
									}
								}
							],
							[order.direction]
						).map((n, id) => {
							let iconState = 0;
							n?.purchase_request_items?.map((itm, i) => {
								if (!itm.brand || !itm.category || !n.branch) {
									iconState = 1;
								}
								return null;
							});
							const isSelected = selected.indexOf(n.id) !== -1;
							return (
								<TableRow
									hover
									role="checkbox"
									aria-checked={isSelected}
									tabIndex={-1}
									key={n.id}
									selected={isSelected}
								>
									<TableCell className="whitespace-nowrap w-40 md:w-64 text-center" padding="none">
										<Checkbox
											checked={isSelected}
											onClick={purchaseEvent => purchaseEvent.stopPropagation()}
											onChange={purchaseEvent => handleCheck(purchaseEvent, n.id)}
										/>
									</TableCell>

									<TableCell className="whitespace-nowrap w-40 md:w-64" component="th" scope="row">
										{pageAndSize.page * pageAndSize.size - pageAndSize.size + serialNumber++}
									</TableCell>

									<TableCell
										style={{ whiteSpace: 'nowrap' }}
										className="p-4 md:p-16"
										component="th"
										scope="row"
									>
										{`${n?.first_name} ${n?.last_name}`}
									</TableCell>

									<TableCell className="whitespace-nowrap p-4 md:p-16" component="th" scope="row">
										{n?.email}
									</TableCell>

									<TableCell className="whitespace-nowrap p-4 md:p-16" component="th" scope="row">
										{n?.contact_no}
									</TableCell>

									<TableCell
										className="p-4 md:p-16"
										style={{ whiteSpace: 'nowrap' }}
										component="th"
										scope="row"
									>
										{moment(new Date(n?.request_date)).format('DD-MM-YYYY')}{' '}
									</TableCell>

									<TableCell className="whitespace-nowrap p-4 md:p-16" component="th" scope="row">
										{n?.area}
									</TableCell>
									<TableCell className="whitespace-nowrap p-4 md:p-16" component="th" scope="row">
										{n?.invoice_no}
									</TableCell>
									<TableCell className="whitespace-nowrap p-4 md:p-16" component="th" scope="row">
										<div
											className={clsx(
												'inline text-12 font-semibold py-4 px-12 rounded-full truncate text-white',
												n?.purchase_status?.name === ('pending' || 'Pending')
													? 'bg-orange'
													: n?.purchase_status?.name === ('submitted' || 'submitted')
													? 'bg-green-800'
													: n?.purchase_status?.name
													? 'bg-black'
													: ''
											)}
										>
											{n?.purchase_status?.name}
										</div>
									</TableCell>
									<TableCell className="whitespace-nowrap p-4 md:p-16" component="th" scope="row">
										<div
											className={clsx(
												'inline text-12 font-semibold py-4 px-12 rounded-full truncate text-white',
												n?.transfer_status === ('pending' || 'Pending')
													? 'bg-orange'
													: n?.transfer_status === ('submitted' || 'Submitted')
													? 'bg-green-800'
													: ''
											)}
										>
											{n?.transfer_status}
										</div>
									</TableCell>

									<TableCell
										whitespace-nowrap
										className="p-4 md:p-16"
										align="center"
										component="th"
										scope="row"
									>
										<div className="flex">
											<Tooltip title="Edit" placement="top">
												<EditIcon
													onClick={purchaseEvent => handleUpdatePurchase(n, 'updatePurchase')}
													className="cursor-pointer"
													style={{ color: 'green' }}
												/>
											</Tooltip>
											{n?.transfer_status === 'submitted' ? (
												''
											) : (
												<Tooltip title="Delete" placement="top">
													<DeleteIcon
														onClick={event => handleDeletePurchase(n, 'deletePurchase')}
														className="cursor-pointer"
														style={{
															color: 'red',
															visibility:
																user_role === 'ADMIN' || user_role === 'admin'
																	? 'visible'
																	: 'hidden'
														}}
													/>
												</Tooltip>
											)}
											{iconState === 0 && (
												<Tooltip title="Item Details" placement="top-">
													<VisibilityIcon
														onClick={() => handleViewItem(n, 'viewPurchaseItem')}
														className=" cursor-pointer"
														style={{ color: 'orange' }}
													/>
												</Tooltip>
											)}
											<Tooltip title="Invoice" placement="top-">
												<ReceiptIcon
													onClick={invoiceEvent => handlePrintInvoice(n, 'invoicePurchase')}
													className=" cursor-pointer"
													style={{ color: '#6495ED' }}
												/>
											</Tooltip>
										</div>
									</TableCell>
								</TableRow>
							);
						})}
					</TableBody>
				</Table>
			</FuseScrollbars>

			<div className={classes.root} id="pagiContainer">
				<Pagination
					count={totalPages}
					page={page + 1}
					defaultPage={1}
					color="primary"
					showFirstButton
					showLastButton
					variant="outlined"
					shape="rounded"
					onChange={handlePagination}
				/>

				<TablePagination
					className="flex-shrink-0 border-t-1"
					rowsPerPageOptions={[10, 30, 50, 100]}
					component="div"
					count={totalElements}
					rowsPerPage={rowsPerPage}
					page={page}
					backIconButtonProps={{
						'aria-label': 'Previous Page'
					}}
					nextIconButtonProps={{
						'aria-label': 'Next Page'
					}}
					onChangePage={handleChangePage}
					onChangeRowsPerPage={handleChangeRowsPerPage}
				/>
			</div>
		</div>
	);
};

export default withRouter(PurchasesTable);
