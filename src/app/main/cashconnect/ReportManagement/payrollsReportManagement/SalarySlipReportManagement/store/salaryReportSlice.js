import { createAsyncThunk, createSlice } from '@reduxjs/toolkit';
import axios from 'axios';
import moment from 'moment';
import {
	FILTER_EMPLOYEE_SALARY_SLIP_REPORT,
	FILTER_EMPLOYEE_SALARY_SLIP_REPORT_WITHOUT_PG
} from '../../../../../../constant/constants';

export const getSalarys = createAsyncThunk(
	'salarysReportManagement/getSalarys',
	async ({ values, pageAndSize }, { rejectWithValue }) => {
		try {
			axios.defaults.headers.common['Content-type'] = 'application/json';
			axios.defaults.headers.common.Authorization = localStorage.getItem('jwt_access_token');
			const year = values?.date_after ? values?.date_after?.getFullYear() : '';
			console.log('values', values);

			const res = await axios.get(
				`${FILTER_EMPLOYEE_SALARY_SLIP_REPORT}?employee=${values.employee || ''}&department=${
					values.department || ''
				}&date_from=${values.date_after ? moment(values.date_after).format('YYYY-MM-DD') : ''}&date_to=${
					values.date_before ? moment(values.date_before).format('YYYY-MM-DD') : ''
				}&year=${year || ''}`,
				{ params: pageAndSize }
			);

			delete axios.defaults.headers.common['Content-type'];
			delete axios.defaults.headers.common.Authorization;

			return res.data || {};
		} catch (err) {
			return rejectWithValue({});
		}
	}
);

export const getAllSalarys = createAsyncThunk(
	'salarysReportManagement/getAllSalarys',
	async (values, { rejectWithValue }) => {
		try {
			axios.defaults.headers.common['Content-type'] = 'application/json';
			axios.defaults.headers.common.Authorization = localStorage.getItem('jwt_access_token');
			const year = values?.date_after ? values?.date_after?.getFullYear() : '';

			const res = await axios.get(
				`${FILTER_EMPLOYEE_SALARY_SLIP_REPORT_WITHOUT_PG}?employee=${values.employee || ''}&department=${
					values.department || ''
				}&date_from=${values.date_after ? moment(values.date_after).format('YYYY-MM-DD') : ''}&date_to=${
					values.date_before ? moment(values.date_before).format('YYYY-MM-DD') : ''
				}&year=${year || ''}`
			);

			delete axios.defaults.headers.common['Content-type'];
			delete axios.defaults.headers.common.Authorization;

			return res.data || {};
		} catch (err) {
			return rejectWithValue({});
		}
	}
);

const salaryReportsSlice = createSlice({
	name: 'salarysReportManagement/salarys',
	initialState: {
		salarys: []
	},
	extraReducers: {
		[getSalarys.fulfilled]: (state, action) => {
			state.salarys = action.payload?.salarys || [];
		},
		[getSalarys.rejected]: state => {
			state.salarys = [];
		},
		[getAllSalarys.fulfilled]: (state, action) => {
			state.salarys = action.payload?.salarys || [];
		},
		[getAllSalarys.rejected]: state => {
			state.salarys = [];
		}
	}
});

export default salaryReportsSlice.reducer;
