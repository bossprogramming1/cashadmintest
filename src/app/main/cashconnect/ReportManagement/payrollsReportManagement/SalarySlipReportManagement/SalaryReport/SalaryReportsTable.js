import { faBookOpen, faDownload, faFileExcel, faFilePdf, faScroll } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { Email, Language, LocationOn, PhoneEnabled, GetApp, ViewWeek } from '@material-ui/icons';

import { yupResolver } from '@hookform/resolvers/yup';
import PrintIcon from '@material-ui/icons/Print';
import useReportData2 from 'app/@customHooks/useReportData2';
import useUserInfo from 'app/@customHooks/useUserInfo';
import getPaginationData from 'app/@helpers/getPaginationData';
import {
	Box,
	Button,
	Grid,
	Paper,
	TableContainer,
	TableHead,
	TextField,
	Typography,
	makeStyles,
	Table,
	TableBody,
	TableCell,
	TableRow
} from '@material-ui/core';

import getTotalAmount from 'app/@helpers/getTotalAmount';
import html2PDF from 'jspdf-html2canvas';
import React, { useEffect, useLayoutEffect, useReducer, useRef, useState } from 'react';
import { unstable_batchedUpdates } from 'react-dom';
import { FormProvider, useForm } from 'react-hook-form';
import ReactHtmlTableToExcel from 'react-html-table-to-excel';
import { useDispatch } from 'react-redux';
import { useReactToPrint } from 'react-to-print';
import * as yup from 'yup';
import { months } from 'app/@data/data';
import { BASE_URL, GET_SITESETTINGS } from '../../../../../../constant/constants';
import ColumnLabel from '../../../reportComponents/ColumnLabel';
import Pagination from '../../../reportComponents/Pagination';
import SinglePage from '../../../reportComponents/SiglePage';
import { getReportMakeStyles } from '../../../reportUtils/reportMakeStyls';
import { getPayrollMakeStyles } from '../../../reportUtils/payrollMakeStyles';

import tableColumnsReducer from '../../../reportUtils/tableColumnsReducer';
import { getAllSalarys, getSalarys } from '../store/salaryReportSlice';
import SalaryFilterMenu from './SalaryFilterMenu';
import '../../../Print.css';

const useStyles = makeStyles(theme => ({
	...getReportMakeStyles(theme),
	inputBg: {
		background: theme.palette.primary.main,
		color: theme.palette.primary[50]
	},
	...getPayrollMakeStyles(theme),
	root: {
		display: 'flex',
		justifyContent: 'space-between',
		flexWrap: 'nowrap',
		overflow: 'auto',
		minHeight: '35px'
	},
	toolbar: {
		'& > div': {
			minHeight: 'fit-content'
		}
	},
	box: {
		background: '#fff',
		border: '1px solid',
		borderColor: 'grey',
		borderRadius: 2,
		fontSize: '0.875rem',
		fontWeight: '700',
		width: '50%',
		padding: '20px',
		height: 'fit-content'
	},
	tableBox: {
		background: '#fff',
		border: '1px solid',
		borderColor: 'grey',
		borderRadius: 2,
		fontSize: '0.875rem',
		fontWeight: '700',
		padding: '20px',
		height: 'fit-content',
		margin: '20px'
	},

	itemHead: {
		display: 'flex',
		flexDirection: 'column',
		justifyContent: 'center',
		alignItems: 'center'
	}
}));

const schema = yup.object().shape({});

const initialTableColumnsState = [
	{ id: 1, label: 'Sl_No', sortAction: false, isSerialNo: true, show: true },
	{ id: 2, label: 'Employee', name: 'employee', subName: 'first_name', show: true, type: 'date' },
	{ id: 3, label: 'Department', name: 'department', show: true },
	{ id: 5, label: 'Amount', name: 'amount', show: true }
	// {
	// 	id: 6,
	// 	label: 'Debit',

	// 	name: 'debit_amount',
	// 	show: true,
	// 	style: { justifyContent: 'flex-end', marginRight: '5px' },
	// 	headStyle: { textAlign: 'right' }
	// },
	// {
	// 	id: 7,
	// 	label: 'Credit',
	// 	name: 'credit_amount',
	// 	show: true,
	// 	style: { justifyContent: 'flex-end', marginRight: '5px' },
	// 	headStyle: { textAlign: 'right' }
	// },
	// {
	// 	id: 8,
	// 	label: 'Balance',
	// 	name: 'balance',
	// 	show: true,
	// 	style: { justifyContent: 'flex-end', marginRight: '5px' },
	// 	headStyle: { textAlign: 'right' }
	// }
];

const SalaryReportsTable = () => {
	const classes = useStyles();

	const methods = useForm({
		mode: 'onChange',
		defaultValues: {},
		resolver: yupResolver(schema)
	});

	const { getValues } = methods;

	const dispatch = useDispatch();

	const { authTOKEN } = useUserInfo();

	const [generalData, setGeneralData] = useState({});

	const [modifiedSalaryData, setModifiedSalaryData, setSortBy] = useReportData2({ type: 'salary', extraRowCount: 1 });

	const [employeeData, setEmployeeData] = useState([]);
	const [tableColumns, dispatchTableColumns] = useReducer(tableColumnsReducer, initialTableColumnsState);

	const [totalCdAmount, settotalCdAmount] = useState(0);
	const [totalDbAmount, settotalDbAmount] = useState(0);
	const [totalBAlance, setTotalBAlance] = useState(0);
	const [actionPrint, setActionPrint] = useState(false);

	//tools state
	const [inPrint, setInPrint] = useState(false);
	const [inSiglePageMode, setInSiglePageMode] = useState(false);
	const [inShowAllMode, setInShowAllMode] = useState(false);
	const [inDowloadPdf, setInDowloadPdf] = useState(false);
	const [inDowloadExcel, setInDowloadExcel] = useState(false);
	const [showClmSelectOption, setShowClmSelectOption] = useState(false);

	//pagination state
	const [page, setPage] = useState(1);
	const [size, setSize] = useState(24);
	const [totalPages, setTotalPages] = useState(0);
	const [totalElements, setTotalElements] = useState(0);

	//get general setting data
	useEffect(() => {
		fetch(`${GET_SITESETTINGS}`, authTOKEN)
			.then(response => response.json())
			.then(data => setGeneralData(data.general_settings[0] || {}))
			.catch(() => setGeneralData({}));
	}, []);

	//print dom ref
	const componentRef = useRef();

	//printer action
	const printAction = useReactToPrint({
		content: () => componentRef.current
	});

	//print handler
	const handlePrint = () => {
		setInPrint(true);
		if (!inPrint) {
			if (!inShowAllMode && totalPages > 1) {
				handleGetAllSalarys(null, () => {
					printAction();
					setInPrint(false);
					handleGetSalarys();
				});
			} else {
				printAction();
				setInPrint(false);
			}
		}
	};
	const margins = {
		top: 10, // Top margin in mm
		bottom: 10, // Bottom margin in mm
		left: 10, // Left margin in mm
		right: 10 // Right margin in mm
	};
	//pdf downloader action
	const pdfDownloadAction = () => {
		html2PDF(downloadPage, {
			margin: margins,
			filename: 'pdfhtml2.pdf',
			html2canvas: {
				dpi: 300,
				letterRendering: true
			},
			setTestIsImage: false,
			useCORS: true,
			jsPDF: { unit: 'mm', format: 'a4', orientation: 'portrait' }
		});
		setInDowloadPdf(false);
	};

	//pdf download handler
	const handlePdfDownload = () => {
		setInDowloadPdf(true);
		if (!inDowloadPdf) {
			if (!inShowAllMode && totalPages > 1) {
				handleGetAllSalarys(null, () => {
					pdfDownloadAction();
					setInDowloadPdf(false);
					handleGetSalarys();
				});
			} else {
				pdfDownloadAction();
				setInDowloadPdf(false);
			}
		}
	};

	//exel download page dom
	let downloadPage = document.getElementById('downloadPage');

	//exel download handler
	const handleExelDownload = () => {
		setInDowloadExcel(true);
		if (!inDowloadExcel) {
			if (!inShowAllMode && totalPages > 1) {
				handleGetAllSalarys(null, () => {
					document.getElementById('test-table-xls-button').click();
					setInDowloadExcel(false);
					handleGetSalarys();
				});
			} else {
				document.getElementById('test-table-xls-button').click();
				setInDowloadExcel(false);
			}
		}
	};

	//column select close handler
	useLayoutEffect(() => {
		window.addEventListener('click', e => {
			if (e.target.id !== 'insideClmSelect') setShowClmSelectOption(false);
		});
	}, []);

	//pagination handler
	const firstPageHandler = event => {
		handleGetSalarys(event.page);
	};
	const previousPageHandler = event => {
		handleGetSalarys(event.page);
	};
	const nextPageHandler = event => {
		handleGetSalarys(event.page);
	};
	const lastPageHandler = event => {
		handleGetSalarys(event.page);
	};

	//get salarys
	const handleGetSalarys = (pagePram, callBack) => {
		dispatch(getSalarys({ values: getValues(), pageAndSize: { page: pagePram || page, size } })).then(res => {
			unstable_batchedUpdates(() => {
				callBack && callBack(res.payload);
				setModifiedSalaryData(res.payload || [], 25, res.payload?.remaining_balance || 0);
				setEmployeeData([res.payload]);
				const totalAmount = getTotalAmount(res.payload || [], 'amount');
				setTotalBAlance(totalAmount.toFixed(2) || '0.00');
				setPage(res.payload?.page || 1);
				setSize(res.payload?.size || 25);
				setTotalPages(res.payload?.total_pages || 0);
				setTotalElements(res.payload?.total_elements || 0);
				setInSiglePageMode(true);
				setInShowAllMode(false);
			});
		});
	};

	//get all salary without pagination
	const handleGetAllSalarys = (callBack, callBackAfterStateUpdated) => {
		dispatch(getAllSalarys(getValues())).then(res => {
			unstable_batchedUpdates(() => {
				callBack && callBack(res.payload);
				setModifiedSalaryData(res.payload || [], 25, res.payload?.remaining_balance || 0);
				const totalAmount = getTotalAmount(res.payload || [], 'amount');
				setTotalBAlance(totalAmount.toFixed(2) || '0.00');

				setInSiglePageMode(false);
				setInShowAllMode(true);
				//get pagination data
				const { totalPages, totalElements } = getPaginationData(res.payload, size, page);
				setPage(page || 1);
				setSize(size || 25);
				setTotalPages(totalPages);
				setTotalElements(totalElements);
			});
			callBackAfterStateUpdated && callBackAfterStateUpdated(res.payload);
		});
	};

	return (
		<>
			<div className={classes.headContainer}>
				{/* filter */}
				<FormProvider {...methods}>
					<SalaryFilterMenu
						inShowAllMode={inShowAllMode}
						handleGetSalarys={handleGetSalarys}
						handleGetAllSalarys={handleGetAllSalarys}
					/>
				</FormProvider>
			</div>
			<div className={`${classes.menubar} justify-start md:justify-center`}>
				{/* pagination */}
				<Pagination
					page={page}
					size={size}
					totalPages={totalPages || 0}
					totalElements={totalElements || 0}
					onClickFirstPage={firstPageHandler}
					onClickPreviousPage={previousPageHandler}
					onClickNextPage={nextPageHandler}
					onClickLastPage={lastPageHandler}
				/>

				<div className="downloadIcon">
					{/* download options*/}
					<div className="downloadOptionContainer">
						<div className="indicator"></div>
						<div className="downloadOptions shadow-4">
							{/* download as Pdf */}
							{/*<div
								className="cursor-pointer downloadContainer shadow-4"
								style={{ width: '150px', margin: '10px' }}
								onClick={() => handlePdfDownload()}
							>
								<FontAwesomeIcon icon={faFilePdf} />
								<b>Download PDF</b>
								<FontAwesomeIcon icon={faDownload} />
							</div>*/}

							{/* download as Excel */}
							<div
								className="cursor-pointer downloadContainer shadow-4"
								style={{ width: '160px', margin: '0px 10px 10px 10px' }}
								onClick={() => handleExelDownload()}
							>
								<FontAwesomeIcon icon={faFileExcel} />
								<b>Download Excel</b>
								<FontAwesomeIcon icon={faDownload} />
							</div>
						</div>
					</div>
					{/* download icon*/}
					<GetApp
						className="cursor-pointer inside icon"
						style={{ margin: '0px', border: (inDowloadPdf || inDowloadExcel) && '1px solid' }}
					/>
				</div>

				{/* print icon*/}
				<PrintIcon
					className="cursor-pointer inside icon"
					style={{ padding: '6px', border: inPrint && '1px solid' }}
					onClick={() => handlePrint()}
				/>

				{/* show single page icon*/}
				<FontAwesomeIcon
					className="cursor-pointer inside icon"
					style={{ padding: '8px', border: inSiglePageMode && '1px solid' }}
					onClick={() => handleGetSalarys()}
					icon={faBookOpen}
				/>

				{/* show all page icon*/}
				<FontAwesomeIcon
					className="cursor-pointer inside icon"
					style={{ padding: '8px', border: inShowAllMode && '1px solid' }}
					onClick={() => handleGetAllSalarys()}
					icon={faScroll}
				/>

				{/* column select option icon*/}
				<div className="columnSelectContainer">
					<ViewWeek
						id="insideClmSelect"
						className="cursor-pointer inside icon"
						style={{ margin: '0px', padding: '6px', border: showClmSelectOption && '1px solid' }}
						onClick={() => setTimeout(() => setShowClmSelectOption(true), 0)}
					/>

					{/* columns */}
					<div
						id="insideClmSelect"
						className={`allColumnContainer shadow-5 ${showClmSelectOption ? 'block' : 'hidden'}`}
					>
						{tableColumns.map(column => (
							<ColumnLabel key={column.id} column={column} dispatchTableColumns={dispatchTableColumns} />
						))}
					</div>
				</div>
			</div>

			{/* excel converter */}
			<div className="hidden">
				<ReactHtmlTableToExcel
					id="test-table-xls-button"
					className="download-table-xls-button"
					table="table-to-xls"
					filename="tablexls"
					sheet="tablexls"
					buttonText="Download as XLS"
				/>
			</div>

			<table id="table-to-xls" className="w-full" style={{ minHeight: '270px' }}>
				<div ref={componentRef} id="downloadPage">
					{/* each single page (table) */}
					{employeeData.map((salary, idx) => {
						const maxRowCount = Math.max(
							salary && salary.earnings && salary.earnings.length,
							salary && salary.deductions && salary.deductions.length
						);
						const totalEarnings =
							salary &&
							salary.earnings &&
							salary.earnings.reduce((total, item) => total + item.amount, 0);
						const totalDeductions =
							salary &&
							salary.deductions &&
							salary.deductions.reduce((total, item) => total + item.amount, 0);

						const emptyCellClass = 'text-12 font-medium p-5';
						const netAmount = totalEarnings - totalDeductions;
						const monthNumber = parseInt(salary.month, 10); // Convert the month string to a number
						const monthName = months[monthNumber - 1]; // Adjust for 0-based array indexing

						return (
							<div
								style={{ backgroundColor: 'white' }}
								className={`${classes.pageContainer} printPageContainer`}
							>
								{salary && salary.earnings && (
									<>
										{' '}
										<div>
											<div>
												<div>
													<div className="logoContainer pr-0 md:-pr-20">
														<img
															style={{
																visibility: generalData.header_image
																	? 'visible'
																	: 'hidden',
																textAlign: 'center'
															}}
															src={
																generalData.header_image
																	? `${BASE_URL}${generalData.header_image}`
																	: null
															}
															alt="Not found"
														/>
													</div>
												</div>
											</div>
											<div
												style={{
													textAlign: 'center',
													// borderBottom: '1px solid gray',
													// marginBottom: '20px',
													margin: '-30px 20px 0 20px',

													fontSize: '8px'
												}}
											>
												<LocationOn fontSize="small" />
												{` ${generalData?.address}` || ''} &nbsp; &nbsp; &nbsp;{' '}
												<PhoneEnabled fontSize="small" />
												{` ${generalData?.phone || ''}`}&nbsp; &nbsp; <Email fontSize="small" />
												{` ${generalData?.email || ''}`} &nbsp; &nbsp;{' '}
												<Language fontSize="small" />
												{` ${generalData?.site_address || ''}`}
											</div>
											<div
												className="p-20"
												style={{
													display: 'flex',
													flexDirection: 'column',
													alignItems: 'center'
												}}
											>
												<h1 className="font-600 mb-20">
													Pay Slip for {monthName}, {salary?.year}
												</h1>

												<div
													style={{ display: 'grid', gridTemplateColumns: 'max-content auto' }}
												>
													<h4 style={{ gridRow: '1', gridColumn: '1' }}>
														Employee Name {`\xa0\xa0\xa0\xa0\xa0\xa0`}
													</h4>
													<h4 style={{ gridRow: '1', gridColumn: '2' }}>
														: {`\xa0\xa0\xa0\xa0`}
														{salary?.employee?.first_name}
													</h4>
													<h4 style={{ gridRow: '2', gridColumn: '1' }}>
														Designation {`\xa0\xa0\xa0\xa0\xa0\xa0`}
													</h4>
													<h4 style={{ gridRow: '2', gridColumn: '2' }}>
														: {`\xa0\xa0\xa0\xa0`}
														{salary?.employee?.designation || ''}
													</h4>

													<h4 style={{ gridRow: '3', gridColumn: '1' }}>
														Department {`\xa0\xa0\xa0\xa0\xa0\xa0`}
													</h4>
													<h4 style={{ gridRow: '3', gridColumn: '2' }}>
														: {`\xa0\xa0\xa0\xa0`}
														{salary?.employee?.department?.name}
													</h4>

													<h4 style={{ gridRow: '4', gridColumn: '1' }}>
														Location {`\xa0\xa0\xa0\xa0\xa0\xa0`}
													</h4>
													<h4 style={{ gridRow: '4', gridColumn: '2' }}>
														: {`\xa0\xa0\xa0\xa0`}
														{salary?.employee?.street_address_one}
													</h4>

													<h4 style={{ gridRow: '5', gridColumn: '1' }}>
														Date of Joining {`\xa0\xa0\xa0\xa0\xa0\xa0`}
													</h4>
													<h4 style={{ gridRow: '5', gridColumn: '2' }}>
														: {`\xa0\xa0\xa0\xa0`}
														{salary?.employee?.emp_join_date}
													</h4>
												</div>
											</div>
											<div style={{ width: '50%', margin: '50px' }}>
												<div
													style={{ display: 'grid', gridTemplateColumns: 'max-content auto' }}
												>
													<h4
														className="font-600"
														style={{
															gridRow: '1',
															gridColumn: '1',
															border: '1px solid #000',
															padding: '5px',
															borderBottom: 'none',
															borderRight: 'none'
														}}
													>
														Attendance Details {`\xa0\xa0\xa0\xa0\xa0\xa0`}
													</h4>
													<h4
														className="font-600"
														style={{
															gridRow: '1',
															gridColumn: '2',
															border: '1px solid #000',
															padding: '5px',
															textAlign: 'end',
															borderBottom: 'none'
														}}
													>
														{`\xa0\xa0\xa0\xa0`}
														Value
													</h4>
													<h4
														style={{
															gridRow: '2',
															gridColumn: '1',
															border: '1px solid #000',
															padding: '5px',
															borderBottom: 'none',
															borderRight: 'none'
														}}
													>
														Absent {`\xa0\xa0\xa0\xa0\xa0\xa0`}
													</h4>
													<h4
														style={{
															gridRow: '2',
															gridColumn: '2',
															border: '1px solid #000',
															padding: '5px',
															textAlign: 'end',
															borderBottom: 'none'
														}}
													>
														{`\xa0\xa0\xa0\xa0`}
														{`${salary?.attendace_details?.absent_days || 0} Day`}
													</h4>

													<h4
														style={{
															gridRow: '3',
															gridColumn: '1',
															border: '1px solid #000',
															padding: '5px',
															borderBottom: 'none',
															borderRight: 'none'
														}}
													>
														Present {`\xa0\xa0\xa0\xa0\xa0\xa0`}
													</h4>
													<h4
														style={{
															gridRow: '3',
															gridColumn: '2',
															border: '1px solid #000',
															padding: '5px',
															textAlign: 'end',
															borderBottom: 'none'
														}}
													>
														{`\xa0\xa0\xa0\xa0`}
														{`${salary?.attendace_details?.present_days || 0} Day`}
													</h4>

													<h4
														style={{
															gridRow: '4',
															gridColumn: '1',
															border: '1px solid #000',
															padding: '5px',
															borderRight: 'none'
														}}
													>
														Overtime {`\xa0\xa0\xa0\xa0\xa0\xa0`}
													</h4>
													<h4
														style={{
															gridRow: '4',
															gridColumn: '2',
															border: '1px solid #000',
															padding: '5px',
															textAlign: 'end'
														}}
													>
														{`\xa0\xa0\xa0\xa0`}
														{`${salary?.attendace_details?.amount_of_overtime || 0} Hrs`}
													</h4>
												</div>
											</div>

											<Box
												style={{
													margin: '0 50px 50px 50px',
													// border: '1px solid #1b2330',
													height: 'fit-content',
													display: 'flex',
													// className={classes.mainContainer}
													// padding: '10px',
													alignItems: 'flex-start',
													borderRadius: '5px',

													justifyContent: 'space-between'
												}}
											>
												<TableContainer
													component={Paper}
													className={classes.tblContainer}
													style={{
														borderTopRightRadius: '0px',
														borderBottomRightRadius: '0px'
													}}
												>
													<Table aria-label="simple table">
														<TableHead className={classes.tableHead}>
															<TableRow hover style={{ fontSize: '14px' }}>
																<TableCell
																	style={{ fontSize: '14px' }}
																	className={classes.tableCell}
																>
																	<Typography className="text-14 font-medium">
																		Earnings
																	</Typography>
																</TableCell>
																<TableCell
																	style={{ fontSize: '14px' }}
																	className={classes.tableCell}
																>
																	<Typography className="text-14 font-medium">
																		Amount
																	</Typography>
																</TableCell>
															</TableRow>
														</TableHead>
														<TableBody>
															{Array.from({ length: maxRowCount }).map((_, index) => {
																const earningsItem = salary.earnings[index];
																return (
																	<TableRow hover key={index}>
																		<TableCell
																			className={
																				earningsItem
																					? 'text-12 font-medium p-5'
																					: emptyCellClass
																			}
																		>
																			{earningsItem ? (
																				earningsItem.payhead_name
																			) : (
																				<span style={{ visibility: 'hidden' }}>
																					deductionsItem.payhead_name
																				</span>
																			)}
																		</TableCell>
																		<TableCell
																			className={
																				earningsItem
																					? 'text-12 font-medium p-5'
																					: emptyCellClass
																			}
																		>
																			{earningsItem ? (
																				`${earningsItem.amount} `
																			) : (
																				<span style={{ visibility: 'hidden' }}>
																					deductionsItem.payhead_name
																				</span>
																			)}
																		</TableCell>
																	</TableRow>
																);
															})}
															<TableRow>
																<TableCell className={classes.tableCellInBody}>
																	<Typography className={classes.tableCellInBody}>
																		Total Earnings
																	</Typography>
																</TableCell>
																<TableCell
																	className={classes.tableCellInBody}
																	align="center"
																>
																	<Typography className={classes.tableCellInBody}>
																		{totalEarnings}
																	</Typography>
																</TableCell>
															</TableRow>
															<TableRow style={{ visibility: 'hidden' }}>
																<TableCell className={classes.tableCellInBody}>
																	<Typography className={classes.tableCellInBody}>
																		Total Earnings
																	</Typography>
																</TableCell>
																<TableCell
																	className={classes.tableCellInBody}
																	align="center"
																>
																	<Typography className={classes.tableCellInBody}>
																		{totalEarnings}
																	</Typography>
																</TableCell>
															</TableRow>
														</TableBody>
													</Table>
												</TableContainer>

												<TableContainer
													component={Paper}
													className={classes.tblContainer}
													style={{
														borderTopLeftRadius: '0px',
														borderBottomLeftRadius: '0px'
													}}
												>
													<Table aria-label="simple table">
														<TableHead className={classes.tableHead}>
															<TableRow hover style={{ fontSize: '14px' }}>
																<TableCell
																	style={{ fontSize: '14px' }}
																	className={classes.tableCell}
																>
																	<Typography className="text-14 font-medium">
																		Deductions
																	</Typography>
																</TableCell>
																<TableCell
																	style={{ fontSize: '14px' }}
																	className={classes.tableCell}
																>
																	<Typography className="text-14 font-medium">
																		Amount
																	</Typography>
																</TableCell>
															</TableRow>
														</TableHead>
														<TableBody>
															{Array.from({ length: maxRowCount }).map((_, index) => {
																const deductionsItem = salary.deductions[index];
																return (
																	<TableRow hover key={index}>
																		<TableCell
																			className={
																				deductionsItem
																					? 'text-12 font-medium p-5'
																					: emptyCellClass
																			}
																		>
																			{deductionsItem ? (
																				deductionsItem.payhead_name
																			) : (
																				<span style={{ visibility: 'hidden' }}>
																					deductionsItem.payhead_name
																				</span>
																			)}
																		</TableCell>
																		<TableCell
																			className={
																				deductionsItem
																					? 'text-12 font-medium p-5'
																					: emptyCellClass
																			}
																		>
																			{deductionsItem ? (
																				`${deductionsItem.amount} `
																			) : (
																				<span style={{ visibility: 'hidden' }}>
																					deductionsItem.payhead_name
																				</span>
																			)}
																		</TableCell>
																	</TableRow>
																);
															})}
															<TableRow>
																<TableCell className={classes.tableCellInBody}>
																	<Typography className={classes.tableCellInBody}>
																		Total Deductions
																	</Typography>
																</TableCell>
																<TableCell
																	className={classes.tableCellInBody}
																	align="center"
																>
																	<Typography className={classes.tableCellInBody}>
																		{totalDeductions}
																	</Typography>
																</TableCell>
															</TableRow>
															<TableRow>
																<TableCell className={classes.tableCellInBody}>
																	<Typography className={classes.tableCellInBody}>
																		Net Amount
																	</Typography>
																</TableCell>

																<TableCell
																	className={classes.tableCellInBody}
																	align="center"
																>
																	<Typography className={classes.tableCellInBody}>
																		{netAmount}
																	</Typography>
																</TableCell>
															</TableRow>
														</TableBody>
													</Table>
												</TableContainer>
											</Box>
										</div>
										<div className={classes.pageFooterContainer}>
											<div style={{ height: 'fit-content' }}>
												<div
													style={{
														display: !inSiglePageMode ? 'flex' : 'block',
														justifyContent: 'space-between',
														marginLeft: '10px',
														marginRight: '10px'
													}}
												>
													<h6 style={{ textAlign: 'left' }}>
														Developed by RAMS(Bluebay IT Limited)
													</h6>
													{/* {!inSiglePageMode && (
						<h6 style={{ textAlign: 'right' }}>Page : {inSiglePageMode ? page : data?.page}</h6>
					)} */}
												</div>
												<img
													style={{ width: '100%' }}
													src="assets/images/logos/footer_report.png"
													alt="footer_report"
												/>
											</div>
										</div>
									</>
								)}
							</div>
						);
					})}
				</div>
			</table>
		</>
	);
};
export default SalaryReportsTable;
