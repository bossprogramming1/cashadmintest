import FusePageCarded from '@fuse/core/FusePageCarded';
import withReducer from 'app/store/withReducer';
import { motion } from 'framer-motion';
import { Icon, Typography } from '@material-ui/core';
import React from 'react';
import reducer from '../store';
import SalaryReportsTable from './SalaryReportsTable';

const SalaryReport = () => {
	return (
		<FusePageCarded
			header={
				<div className="flex flex-1 w-full items-center justify-between">
					<div className="flex items-center">
						<Icon
							component={motion.span}
							initial={{ scale: 0 }}
							animate={{ scale: 1, transition: { delay: 0.2 } }}
							className="text-24 md:text-32"
						>
							person
						</Icon>
						<Typography
							component={motion.span}
							initial={{ x: -10 }}
							animate={{ x: 0, transition: { delay: 0.2 } }}
							delay={300}
							className="hidden sm:flex text-16 md:text-24 mx-12 font-semibold"
						>
							Salary Slip
						</Typography>
					</div>
				</div>
			}
			headerBgHeight="102px"
			className="bg-grey-200"
			classes={{
				content: 'bg-grey-200',
				contentCard: 'overflow-hidden',
				header: 'min-h-52 h-52'
			}}
			content={<SalaryReportsTable />}
			innerScroll
		/>
	);
};

export default withReducer('salarysReportManagement', reducer)(SalaryReport);
