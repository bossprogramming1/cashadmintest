import { createAsyncThunk, createSlice } from '@reduxjs/toolkit';
import { FILTER_ATTENDANCE_REPORT } from 'app/constant/constants';
import axios from 'axios';

export const getAttendances = createAsyncThunk(
	'attendancesReportManagement/getAttendances',
	async ({ values, pageAndSize }, { rejectWithValue }) => {
		try {
			axios.defaults.headers.common['Content-type'] = 'application/json';
			axios.defaults.headers.common.Authorization = localStorage.getItem('jwt_access_token');

			const res = await axios.get(
				`${FILTER_ATTENDANCE_REPORT}?employee=${values.employee || ''}&department=${
					values.department || ''
				}&date_from=${values.date_from || ''}&date_to=${values.date_to || ''}`,
				{ params: pageAndSize }
			);

			delete axios.defaults.headers.common['Content-type'];
			delete axios.defaults.headers.common.Authorization;

			return res.data || {};
		} catch (err) {
			return rejectWithValue({});
		}
	}
);

// export const getAllAttendances = createAsyncThunk(
// 	'attendancesReportManagement/getAllAttendances',
// 	async (values, { rejectWithValue }) => {
// 		try {
// 			axios.defaults.headers.common['Content-type'] = 'application/json';
// 			axios.defaults.headers.common.Authorization = localStorage.getItem('jwt_access_token');

// 			const res = await axios.get(
// 				`${FILTER_ATTENDANCE_REPORT}?employee=${values.employee || ''}&department=${
// 					values.department || ''
// 				}&date_form=${values.date_from || ''}&date_to=${values.date_to || ''}`
// 			);

// 			delete axios.defaults.headers.common['Content-type'];
// 			delete axios.defaults.headers.common.Authorization;

// 			return res.data || {};
// 		} catch (err) {
// 			return rejectWithValue({});
// 		}
// 	}
// );

const attendanceReportsSlice = createSlice({
	name: 'attendancesReportManagement/attendances',
	initialState: {
		attendance_report: []
	},
	extraReducers: {
		[getAttendances.fulfilled]: (state, action) => {
			state.attendance_report = action.payload?.attendance_report || [];
		},
		[getAttendances.rejected]: state => {
			state.attendance_report = [];
		}
		// [getAllAttendances.fulfilled]: (state, action) => {
		// 	state.attendance_report = action.payload?.attendance_report || [];
		// },
		// [getAllAttendances.rejected]: state => {
		// 	state.attendance_report = [];
		// }
	}
});

export default attendanceReportsSlice.reducer;
