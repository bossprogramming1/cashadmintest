import { combineReducers } from '@reduxjs/toolkit';
import attendanceReports from './attendanceReportSlice';

const reducer = combineReducers({
	attendanceReports
});

export default reducer;
