import { faBookOpen, faCalendarAlt, faChevronDown, faTimesCircle } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { makeStyles, TextField } from '@material-ui/core';
import { Autocomplete } from '@material-ui/lab';
import { DatePicker } from '@material-ui/pickers';
import { getReportFilterMakeStyles } from 'app/main/cashconnect/ReportManagement/reportUtils/reportMakeStyls';
import { getDepartments, getEmployees } from 'app/store/dataSlice';
import moment from 'moment';
import { useEffect, useState, useRef } from 'react';
import { Controller, useFormContext } from 'react-hook-form';
import { useDispatch, useSelector } from 'react-redux';

const useStyles = makeStyles(theme => ({
	...getReportFilterMakeStyles(theme)
}));

function AttendanceFilterMenu({ inShowAllMode, handleGetAttendances, handleGetAllAttendances }) {
	const classes = useStyles();

	const dispatch = useDispatch();

	const [_reRender, setReRender] = useState(0);

	//select field data
	const employees = useSelector(state => state.data.employees);
	const departments = useSelector(state => state.data?.departments);
	const methods = useFormContext();
	const { control, getValues, setValue } = methods;
	const values = getValues();

	//element refs

	const purchaseNoEl = useRef(null);

	useEffect(() => {
		dispatch(getEmployees());
		dispatch(getDepartments());
	}, []);

	return (
		<>
			<div className={classes.filterMenuContainer}>
				<div className="allFieldContainer borderTop mt-4">
					{/* Date from */}
					<div className="fieldContainer">
						<FontAwesomeIcon
							className="icon cursor-pointer"
							icon={faCalendarAlt}
							onClick={() => document.getElementById('dateAfterEl').click()}
						/>

						<div className="dateLabel" onClick={() => document.getElementById('dateAfterEl').click()}>
							Date From
						</div>

						<Controller
							name="date_from"
							control={control}
							render={({ field }) => {
								return (
									<DatePicker
										id="dateAfterEl"
										className="hidden"
										autoOk
										clearable
										format={'dd/MM/yyyy'}
										maxDate={values.date_to || new Date()}
										value={field.value || ''}
										onChange={value => {
											value
												? field.onChange(moment(new Date(value)).format('YYYY-MM-DD'))
												: field.onChange('');
											setReRender(Math.random());
											inShowAllMode ? handleGetAllAttendances() : handleGetAttendances();
										}}
									/>
								);
							}}
						/>
					</div>

					{/* Date to */}
					<div className="fieldContainer">
						<FontAwesomeIcon
							className="icon cursor-pointer"
							icon={faCalendarAlt}
							onClick={() => document.getElementById('dateBeforeEl').click()}
						/>

						<div className="dateLabel" onClick={() => document.getElementById('dateBeforeEl').click()}>
							Date To
						</div>

						<Controller
							name="date_to"
							control={control}
							render={({ field }) => {
								return (
									<DatePicker
										id="dateBeforeEl"
										className="hidden"
										autoOk
										clearable
										format={'dd/MM/yyyy'}
										value={field.value || ''}
										minDate={values.date_from}
										maxDate={new Date()}
										onChange={value => {
											value
												? field.onChange(moment(new Date(value)).format('YYYY-MM-DD'))
												: field.onChange('');
											setReRender(Math.random());
											inShowAllMode ? handleGetAllAttendances() : handleGetAttendances();
										}}
									/>
								);
							}}
						/>
					</div>

					{/* employee */}
					<div className="fieldContainer">
						<FontAwesomeIcon className="icon" icon={faBookOpen} />

						<div
							className="selectLabel"
							style={{
								width: values.employeeFocused ? '0px' : 'fit-content',
								margin: values.employeeFocused ? '0px' : '2px 5px 0px 10px'
							}}
							onClick={() => {
								setValue('employeeFocused', true);
								setReRender(Math.random());
								setTimeout(() => document.getElementById('employeeEl').focus(), 300);
							}}
						>
							Employee
						</div>
						<FontAwesomeIcon
							className="selectOpenIcon cursor-pointer"
							style={{
								width: values.employeeFocused ? '0px' : '15px',
								margin: values.employeeFocused ? '0px' : '2px 10px 0px 0px'
							}}
							onClick={() => {
								setValue('employeeFocused', true);
								setReRender(Math.random());
								setTimeout(() => document.getElementById('employeeEl').focus(), 300);
							}}
							icon={faChevronDown}
						/>

						<Controller
							name="employee"
							control={control}
							render={({ field: { onChange, value } }) => (
								<Autocomplete
									id="employeeEl"
									className="mb-3 selectField"
									style={{
										width: values.employeeFocused ? '150px' : '0px',
										margin: values.employeeFocused ? '0px 10px' : '0px',
										display: values.employeeFocused ? 'block' : 'none'
									}}
									classes={{ endAdornment: 'endAdornment' }}
									openOnFocus={true}
									onClose={() => {
										setValue('employeeFocused', false);
										setReRender(Math.random());
									}}
									freeSolo
									options={employees}
									value={value ? employees.find(data => data.id == value) : null}
									getOptionLabel={option => `${option?.first_name} [${option.id}]`}
									onChange={(event, newValue) => {
										onChange(newValue?.id);
										setValue('employeeName', newValue?.first_name || '');
										inShowAllMode ? handleGetAllAttendances() : handleGetAttendances();
									}}
									renderInput={params => (
										<TextField
											{...params}
											fullWidth
											className="textFieldUnderSelect"
											placeholder="Select Employee"
										/>
									)}
								/>
							)}
						/>
					</div>

					{/* DepartMent */}
					<div className="fieldContainer">
						<FontAwesomeIcon className="icon" icon={faBookOpen} />

						<div
							className="selectLabel"
							style={{
								width: values.departmentFocused ? '0px' : '70px',
								margin: values.departmentFocused ? '0px' : '3px 5px 0px 5px'
							}}
							onClick={() => {
								setValue('departmentFocused', true);
								setReRender(Math.random());
								setTimeout(() => document.getElementById('departmentEl').focus(), 300);
							}}
						>
							Departments
						</div>
						<FontAwesomeIcon
							className="selectOpenIcon cursor-pointer"
							style={{
								width: values.departmentFocused ? '0px' : '15px',
								margin: values.departmentFocused ? '0px' : '2px 10px 0px 0px'
							}}
							onClick={() => {
								setValue('departmentFocused', true);
								setReRender(Math.random());
								setTimeout(() => document.getElementById('departmentEl').focus(), 300);
							}}
							icon={faChevronDown}
						/>

						<Controller
							name="department"
							control={control}
							render={({ field: { onChange, value } }) => (
								<Autocomplete
									id="departmentEl"
									className="mb-3 selectField"
									style={{
										width: values.departmentFocused ? '130px' : '0px',
										margin: values.departmentFocused ? '0px 10px' : '0px',
										display: values.departmentFocused ? 'block' : 'none'
									}}
									classes={{ endAdornment: 'endAdornment' }}
									openOnFocus={true}
									onClose={() => {
										setValue('departmentFocused', false);
										setReRender(Math.random());
									}}
									freeSolo
									options={departments}
									value={value ? departments.find(data => data.id == value) : null}
									getOptionLabel={option => `${option?.name}`}
									onChange={(event, newValue) => {
										onChange(newValue?.id);
										setValue('departmentName', newValue?.name || '');
										inShowAllMode ? handleGetAllAttendances() : handleGetAttendances();
									}}
									renderInput={params => (
										<TextField
											{...params}
											className="textFieldUnderSelect"
											placeholder="Select Category"
										/>
									)}
								/>
							)}
						/>
					</div>
				</div>

				{/* keywords */}

				<div className="allKeyWrdContainer">
					{values.date_from && (
						<div className="keywordContainer">
							<b>Date From</b>
							<div>
								<FontAwesomeIcon className="iconWithKeyWord" icon={faCalendarAlt} />
								<p>{moment(new Date(values.date_from)).format('DD-MM-YYYY')}</p>
								<FontAwesomeIcon
									className="closeIconWithKeyWord"
									icon={faTimesCircle}
									onClick={() => {
										setValue('date_from', '');
										inShowAllMode ? handleGetAllAttendances() : handleGetAttendances();
										setReRender(Math.random());
									}}
								/>
							</div>
						</div>
					)}

					{values.date_to && (
						<div className="keywordContainer">
							<b>Date To</b>
							<div>
								<FontAwesomeIcon className="iconWithKeyWord" icon={faCalendarAlt} />
								<p>{moment(new Date(values.date_to)).format('DD-MM-YYYY')}</p>
								<FontAwesomeIcon
									className="closeIconWithKeyWord"
									icon={faTimesCircle}
									onClick={() => {
										setValue('date_to', '');
										inShowAllMode ? handleGetAllAttendances() : handleGetAttendances();
										setReRender(Math.random());
									}}
								/>
							</div>
						</div>
					)}

					{values.employeeName && (
						<div className="keywordContainer">
							<b>employee</b>
							<div>
								<FontAwesomeIcon className="iconWithKeyWord" icon={faBookOpen} />
								<p>{values.employeeName}</p>
								<FontAwesomeIcon
									className="closeIconWithKeyWord"
									icon={faTimesCircle}
									onClick={() => {
										setValue('employeeName', '');
										setValue('employee', '');
										inShowAllMode ? handleGetAllAttendances() : handleGetAttendances();
										setReRender(Math.random());
									}}
								/>
							</div>
						</div>
					)}

					{values.departmentName && (
						<div className="keywordContainer">
							<b>Department</b>
							<div>
								<FontAwesomeIcon className="iconWithKeyWord" icon={faBookOpen} />
								<p>{values.departmentName}</p>
								<FontAwesomeIcon
									className="closeIconWithKeyWord"
									icon={faTimesCircle}
									onClick={() => {
										setValue('departmentName', '');
										setValue('department', '');
										inShowAllMode ? handleGetAllAttendances() : handleGetAttendances();
										setReRender(Math.random());
									}}
								/>
							</div>
						</div>
					)}
				</div>
			</div>
		</>
	);
}

export default AttendanceFilterMenu;
