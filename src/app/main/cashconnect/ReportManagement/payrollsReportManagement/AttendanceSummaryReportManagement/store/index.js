import { combineReducers } from '@reduxjs/toolkit';
import attendancesummaryReports from './attendancesummaryReportSlice';

const reducer = combineReducers({
	attendancesummaryReports
});

export default reducer;
