import { createAsyncThunk, createSlice } from '@reduxjs/toolkit';
import { FILTER_ATTENDANCESUMMARY_REPORT } from 'app/constant/constants';
import axios from 'axios';

export const getAttendanceSummarys = createAsyncThunk(
	'attendancesummarysReportManagement/getAttendanceSummarys',
	async ({ values, pageAndSize }, { rejectWithValue }) => {
		try {
			axios.defaults.headers.common['Content-type'] = 'application/json';
			axios.defaults.headers.common.Authorization = localStorage.getItem('jwt_access_token');

			const res = await axios.get(
				`${FILTER_ATTENDANCESUMMARY_REPORT}?employee=${values.employee || ''}&department=${
					values.department || ''
				}&date_from=${values.date_from || ''}&date_to=${values.date_to || ''}`,
				{ params: pageAndSize }
			);

			delete axios.defaults.headers.common['Content-type'];
			delete axios.defaults.headers.common.Authorization;

			return res.data || {};
		} catch (err) {
			return rejectWithValue({});
		}
	}
);

// export const getAllAttendanceSummarys = createAsyncThunk(
// 	'attendancesummarysReportManagement/getAllAttendanceSummarys',
// 	async (values, { rejectWithValue }) => {
// 		try {
// 			axios.defaults.headers.common['Content-type'] = 'application/json';
// 			axios.defaults.headers.common.Authorization = localStorage.getItem('jwt_access_token');

// 			const res = await axios.get(
// 				`${FILTER_ATTENDANCESUMMARY_REPORT}?employee=${values.employee || ''}&department=${
// 					values.department || ''
// 				}&date_form=${values.date_from || ''}&date_to=${values.date_to || ''}`
// 			);

// 			delete axios.defaults.headers.common['Content-type'];
// 			delete axios.defaults.headers.common.Authorization;

// 			return res.data || {};
// 		} catch (err) {
// 			return rejectWithValue({});
// 		}
// 	}
// );

const attendancesummaryReportsSlice = createSlice({
	name: 'attendancesummarysReportManagement/attendancesummarys',
	initialState: {
		attendancesummary_report: []
	},
	extraReducers: {
		[getAttendanceSummarys.fulfilled]: (state, action) => {
			state.attendancesummary_report = action.payload?.attendancesummary_report || [];
		},
		[getAttendanceSummarys.rejected]: state => {
			state.attendancesummary_report = [];
		}
		// [getAllAttendanceSummarys.fulfilled]: (state, action) => {
		// 	state.attendancesummary_report = action.payload?.attendancesummary_report || [];
		// },
		// [getAllAttendanceSummarys.rejected]: state => {
		// 	state.attendancesummary_report = [];
		// }
	}
});

export default attendancesummaryReportsSlice.reducer;
