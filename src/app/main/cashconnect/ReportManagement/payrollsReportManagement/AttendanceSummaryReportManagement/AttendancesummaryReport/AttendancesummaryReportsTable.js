/* eslint-disable import/named */
import { faBookOpen, faDownload, faFileExcel, faFilePdf, faScroll } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { yupResolver } from '@hookform/resolvers/yup';
import { makeStyles } from '@material-ui/core';
import { GetApp, ViewWeek } from '@material-ui/icons';
import Tooltip from '@mui/material/Tooltip';
import Button from '@mui/material/Button';

import PrintIcon from '@material-ui/icons/Print';
import useReportData from 'app/@customHooks/useReportData';
import useUserInfo from 'app/@customHooks/useUserInfo';
import getPaginationData from 'app/@helpers/getPaginationData';
import getTotalAmount from 'app/@helpers/getTotalAmount';
import html2PDF from 'jspdf-html2canvas';
import React, { useEffect, useLayoutEffect, useReducer, useRef, useState } from 'react';
import { unstable_batchedUpdates } from 'react-dom';
import { FormProvider, useForm } from 'react-hook-form';
import ReactHtmlTableToExcel from 'react-html-table-to-excel';
import { useDispatch } from 'react-redux';
import { useReactToPrint } from 'react-to-print';
import * as yup from 'yup';
import ReceiptIcon from '@material-ui/icons/Receipt';
import history from '@history';
import { getReportMakeStyles } from 'app/main/cashconnect/ReportManagement/reportUtils/reportMakeStyls';
import tableColumnsReducer from 'app/main/cashconnect/ReportManagement/reportUtils/tableColumnsReducer';
import { GET_SITESETTINGS } from 'app/constant/constants';
import SinglePage from 'app/main/cashconnect/ReportManagement/reportComponents//SiglePage';
import Pagination from 'app/main/cashconnect/ReportManagement/reportComponents/Pagination';
import ColumnLabel from 'app/main/cashconnect/ReportManagement/reportComponents/ColumnLabel';
import moment from 'moment';
import AttendanceSummaryFilterMenu from './AttendancesummaryFilterMenu';
import { getAllAttendanceSummarys, getAttendanceSummarys } from '../store/attendancesummaryReportSlice';

const useStyles = makeStyles(theme => ({
	...getReportMakeStyles(theme),
	inputBg: {
		background: theme.palette.primary.main,
		color: theme.palette.primary[50]
	}
}));

const schema = yup.object().shape({});

const initialTableColumnsState = [
	{ id: 1, label: 'Sl_No', sortAction: false, isSerialNo: true, show: true },
	{ id: 2, label: 'Employee Name', name: 'employee', show: true },
	{
		id: 3,
		label: 'Check Date',
		name: 'check_date',
		show: true,
		type: 'date',
		style: { justifyContent: 'center', whiteSpace: 'nowrap' }
	},
	{
		id: 4,
		label: 'Check In',
		name: 'check_in',
		show: true,
		style: { justifyContent: 'center', whiteSpace: 'nowrap' }
	},
	{
		id: 5,
		label: 'Check Out',
		name: 'check_out',
		show: true,
		style: { justifyContent: 'center', whiteSpace: 'nowrap' }
	},
	{
		id: 6,
		label: 'Late time',
		name: 'late_time',
		show: true,
		style: { justifyContent: 'center', whiteSpace: 'nowrap' }
	},
	{
		id: 6,
		label: 'Over time',
		name: 'overtime',
		show: true,
		style: { justifyContent: 'center', whiteSpace: 'nowrap' }
	}
	// {
	// 	id: 4,
	// 	label: 'Check In',
	// 	getterMethod: data => `${moment(new Date(data.voucher_month)).format('MMMM, yyyy')}`,
	// 	show: true
	// },
	// {
	// 	id: 5,
	// 	label: 'Check Out',
	// 	getterMethod: data => `${data?.value} ${data.unit?.formal_name}`,
	// 	show: true
	// },
	// {
	// 	id: 6,
	// 	label: 'Absent Days',
	// 	getterMethod: data => `${data?.absent} ${data.unit?.formal_name}`,
	// 	show: true
	// }
	// {
	// 	id: 9,
	// 	label: 'Action',
	// 	getterMethod: item => {
	// 		return (
	// 			<ReceiptIcon
	// 				onClick={() => {
	// 					history.push(`/apps/report-management/attendancesummary-final-reports/purchase-invoice/${item.id}`);
	// 				}}
	// 				className="h-52 cursor-pointer"
	// 				style={{ color: 'orange' }}
	// 			/>
	// 		);
	// 	},
	// 	show: true
	// }
];

const AttendanceSummaryReportsTable = () => {
	const classes = useStyles();

	const methods = useForm({
		mode: 'onChange',
		defaultValues: {},
		resolver: yupResolver(schema)
	});

	const { getValues } = methods;

	const dispatch = useDispatch();

	const { authTOKEN } = useUserInfo();

	const [generalData, setGeneralData] = useState({});

	const [modifiedAttendanceSummaryData, setModifiedAttendanceSummaryData, setSortBy] = useReportData([], 25, 1);

	const [tableColumns, dispatchTableColumns] = useReducer(tableColumnsReducer, initialTableColumnsState);

	const [totalAmount, setTotalAmount] = useState(0);

	//tools state
	const [inPrint, setInPrint] = useState(false);
	const [inSiglePageMode, setInSiglePageMode] = useState(false);
	const [inShowAllMode, setInShowAllMode] = useState(false);
	const [inDowloadPdf, setInDowloadPdf] = useState(false);
	const [inDowloadExcel, setInDowloadExcel] = useState(false);
	const [showClmSelectOption, setShowClmSelectOption] = useState(false);

	//pagination state
	const [page, setPage] = useState(1);
	const [size, setSize] = useState(25);
	const [totalPages, setTotalPages] = useState(0);
	const [totalElements, setTotalElements] = useState(0);

	//get general setting data
	useEffect(() => {
		fetch(`${GET_SITESETTINGS}`, authTOKEN)
			.then(response => response.json())
			.then(data => setGeneralData(data.general_settings[0] || {}))
			.catch(() => setGeneralData({}));
	}, []);

	//print dom ref
	const componentRef = useRef();

	//printer action
	const printAction = useReactToPrint({
		content: () => componentRef.current
	});

	//print handler
	const handlePrint = () => {
		setInPrint(true);
		if (!inPrint) {
			if (!inShowAllMode && totalPages > 1) {
				// handleGetAllAttendanceSummarys(null, () => {
				printAction();
				setInPrint(false);
				handleGetAttendanceSummarys();
				// });
			} else {
				printAction();
				setInPrint(false);
			}
		}
	};

	//pdf downloader action
	const pdfDownloadAction = () => {
		html2PDF(downloadPage, {
			margin: [0, 0, 0, 0],
			filename: 'pdfhtml2.pdf',
			html2canvas: {
				dpi: 300,
				letterRendering: true
			},
			setTestIsImage: false,
			useCORS: true,
			jsPDF: { unit: 'mm', format: 'a4', orientation: 'portrait' }
		});
		setInDowloadPdf(false);
	};

	//pdf download handler
	const handlePdfDownload = () => {
		setInDowloadPdf(true);
		if (!inDowloadPdf) {
			if (!inShowAllMode && totalPages > 1) {
				// handleGetAllAttendanceSummarys(null, () => {
				pdfDownloadAction();
				setInDowloadPdf(false);
				handleGetAttendanceSummarys();
				// });
			} else {
				pdfDownloadAction();
				setInDowloadPdf(false);
			}
		}
	};

	//exel download page dom
	let downloadPage = document.getElementById('downloadPage');

	//exel download handler
	const handleExelDownload = () => {
		setInDowloadExcel(true);
		if (!inDowloadExcel) {
			if (!inShowAllMode && totalPages > 1) {
				// handleGetAllAttendanceSummarys(null, () => {
				document.getElementById('test-table-xls-button').click();
				setInDowloadExcel(false);
				handleGetAttendanceSummarys();
				// });
			} else {
				document.getElementById('test-table-xls-button').click();
				setInDowloadExcel(false);
			}
		}
	};

	//column select close handler
	useLayoutEffect(() => {
		window.addEventListener('click', e => {
			if (e.target.id !== 'insideClmSelect') setShowClmSelectOption(false);
		});
	}, []);

	//pagination handler
	const firstPageHandler = event => {
		handleGetAttendanceSummarys(event.page);
	};
	const previousPageHandler = event => {
		handleGetAttendanceSummarys(event.page);
	};
	const nextPageHandler = event => {
		handleGetAttendanceSummarys(event.page);
	};
	const lastPageHandler = event => {
		handleGetAttendanceSummarys(event.page);
	};

	//get purchases
	const handleGetAttendanceSummarys = (pagePram, callBack) => {
		dispatch(getAttendanceSummarys({ values: getValues(), pageAndSize: { page: pagePram || page, size } })).then(
			res => {
				unstable_batchedUpdates(() => {
					callBack && callBack(res.payload);
					const mainArr = res.payload?.second || [];
					const modifiedArr = [];
					mainArr.map(purchase => {
						modifiedArr.push({
							...purchase
						});
					});

					setModifiedAttendanceSummaryData(modifiedArr || []);
					setTotalAmount(getTotalAmount(modifiedArr, 'net_amount'));
					setPage(res.payload?.page || 1);
					setSize(res.payload?.size || 25);
					setTotalPages(res.payload?.total_pages || 0);
					setTotalElements(res.payload?.total_elements || 0);
					setInSiglePageMode(true);
					setInShowAllMode(false);
				});
			}
		);
	};

	return (
		<>
			<div className={classes.headContainer}>
				{/* filter */}
				<FormProvider {...methods}>
					<AttendanceSummaryFilterMenu
						inShowAllMode={inShowAllMode}
						handleGetAttendanceSummarys={handleGetAttendanceSummarys}
					/>
				</FormProvider>
			</div>
			<div className={`${classes.menubar} justify-start md:justify-center`}>
				{/* pagination */}
				<Pagination
					page={page}
					size={size}
					totalPages={totalPages || 0}
					totalElements={totalElements || 0}
					onClickFirstPage={firstPageHandler}
					onClickPreviousPage={previousPageHandler}
					onClickNextPage={nextPageHandler}
					onClickLastPage={lastPageHandler}
				/>

				<div className="downloadIcon">
					{/* download options*/}
					<div className="downloadOptionContainer">
						<div className="indicator"></div>
						<div className="downloadOptions shadow-4">
							{/* download as Pdf */}
							{/*<div
								className="cursor-pointer downloadContainer shadow-4"
								style={{ width: '150px', margin: '10px' }}
								onClick={() => handlePdfDownload()}
							>
								<FontAwesomeIcon icon={faFilePdf} />
								<b>Download PDF</b>
								<FontAwesomeIcon icon={faDownload} />
							</div>*/}

							{/* download as Excel */}
							<div
								className="cursor-pointer downloadContainer shadow-4"
								style={{ width: '160px', margin: '0px 10px 10px 10px' }}
								onClick={() => handleExelDownload()}
							>
								<FontAwesomeIcon icon={faFileExcel} />
								<b>Download Excel</b>
								<FontAwesomeIcon icon={faDownload} />
							</div>
						</div>
					</div>
					{/* download icon*/}
					<GetApp
						className="cursor-pointer inside icon"
						style={{ margin: '0px', border: (inDowloadPdf || inDowloadExcel) && '1px solid' }}
					/>
				</div>

				{/* print icon*/}
				<Tooltip title={<p style={{ fontSize: '130%' }}>Print</p>} placement="top">
					<PrintIcon
						className="cursor-pointer inside icon"
						style={{ padding: '6px', border: inPrint && '1px solid' }}
						onClick={() => handlePrint()}
					/>
				</Tooltip>
				{/* show single page icon*/}
				<Tooltip title={<p style={{ fontSize: '130%' }}>AttendanceSummary</p>} placement="top">
					<span>
						<FontAwesomeIcon
							className="cursor-pointer inside icon"
							style={{ padding: '8px', border: inSiglePageMode && '1px solid' }}
							onClick={() => handleGetAttendanceSummarys()}
							icon={faBookOpen}
						/>
					</span>
				</Tooltip>

				{/* column select option icon*/}
				<div className="columnSelectContainer">
					<ViewWeek
						id="insideClmSelect"
						className="cursor-pointer inside icon"
						style={{ margin: '0px', padding: '6px', border: showClmSelectOption && '1px solid' }}
						onClick={() => setTimeout(() => setShowClmSelectOption(true), 0)}
					/>

					{/* columns */}
					<div
						id="insideClmSelect"
						className={`allColumnContainer shadow-5 ${showClmSelectOption ? 'block' : 'hidden'}`}
					>
						{tableColumns.map(column => (
							<ColumnLabel key={column.id} column={column} dispatchTableColumns={dispatchTableColumns} />
						))}
					</div>
				</div>
			</div>

			{/* excel converter */}
			<div className="hidden">
				<ReactHtmlTableToExcel
					id="test-table-xls-button"
					className="download-table-xls-button"
					table="table-to-xls"
					filename="tablexls"
					sheet="tablexls"
					buttonText="Download as XLS"
				/>
			</div>

			<table id="table-to-xls" className="w-full" style={{ minHeight: '270px' }}>
				<div ref={componentRef} id="downloadPage">
					{/* each single page (table) */}
					{modifiedAttendanceSummaryData.map((order, idx) => {
						return (
							<SinglePage
								classes={classes}
								generalData={generalData}
								reporTitle="Attendance Summary Report"
								tableColumns={tableColumns}
								dispatchTableColumns={dispatchTableColumns}
								data={
									order.isLastPage
										? {
												...order,
												data: order.data.concat({
													net_amount: totalAmount,
													vendor_invoice_no: 'Grand Total',
													getterMethod: () => '',
													hideSerialNo: true,
													rowStyle: { fontWeight: 600 }
												})
										  }
										: order
								}
								serialNumber={order.page * order.size - order.size + 1}
								setPage={setPage}
								page={page}
								inSiglePageMode={inSiglePageMode}
								setSortBy={setSortBy}
							/>
						);
					})}
				</div>
			</table>
		</>
	);
};
export default AttendanceSummaryReportsTable;
