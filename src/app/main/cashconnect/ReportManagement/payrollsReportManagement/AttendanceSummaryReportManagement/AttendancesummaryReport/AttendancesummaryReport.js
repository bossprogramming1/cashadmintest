import FusePageCarded from '@fuse/core/FusePageCarded';
import withReducer from 'app/store/withReducer';
import React from 'react';
import reducer from '../store';
import AttendanceSummaryReportsTable from './AttendancesummaryReportsTable';

const AttendanceSummaryReport = () => {
	return (
		<FusePageCarded
			headerBgHeight="102px"
			className="bg-grey-300"
			classes={{
				content: 'bg-grey-300',
				contentCard: 'overflow-hidden',
				header: 'min-h-52 h-52'
			}}
			content={<AttendanceSummaryReportsTable />}
			innerScroll
		/>
	);
};

export default withReducer('attendancesummarysReportManagement', reducer)(AttendanceSummaryReport);
