import { createAsyncThunk, createSlice } from '@reduxjs/toolkit';
import axios from 'axios';
import moment from 'moment';
import {
	FILTER_EMPLOYEE_SALARY_LEDGER_REPORT,
	FILTER_EMPLOYEE_SALARY_LEDGER_REPORT_WITHOUT_PG
} from '../../../../../../constant/constants';

export const getSalaryLedgers = createAsyncThunk(
	'salaryLedgersReportManagement/getSalaryLedgers',
	async ({ values, pageAndSize }, { rejectWithValue }) => {
		try {
			axios.defaults.headers.common['Content-type'] = 'application/json';
			axios.defaults.headers.common.Authorization = localStorage.getItem('jwt_access_token');
			// const monthNumber = values.date_after.getMonth() + 1; // Adding 1 to adjust for zero-indexed months

			const res = await axios.get(
				`${FILTER_EMPLOYEE_SALARY_LEDGER_REPORT}?employee=${values.employee || ''}&department=${
					values.department || ''
				}&date_from=${values.date_after ? moment(values.date_after).format('YYYY-MM-DD') : ''}&date_to=${
					values.date_before ? moment(values.date_before).format('YYYY-MM-DD') : ''
				}&year=${values?.date_after?.getFullYear() || ''}`,
				{ params: pageAndSize }
			);

			delete axios.defaults.headers.common['Content-type'];
			delete axios.defaults.headers.common.Authorization;

			return res.data || {};
		} catch (err) {
			return rejectWithValue({});
		}
	}
);

export const getAllSalaryLedgers = createAsyncThunk(
	'salaryLedgersReportManagement/getAllSalaryLedgers',
	async (values, { rejectWithValue }) => {
		try {
			axios.defaults.headers.common['Content-type'] = 'application/json';
			axios.defaults.headers.common.Authorization = localStorage.getItem('jwt_access_token');
			const year = values?.date_after?.getFullYear();

			const res = await axios.get(
				`${FILTER_EMPLOYEE_SALARY_LEDGER_REPORT_WITHOUT_PG}?employee=${values.employee || ''}&department=${
					values.department || ''
				}&date_from=${values.date_after ? moment(values.date_after).format('YYYY-MM-DD') : ''}&date_to=${
					values.date_before ? moment(values.date_before).format('YYYY-MM-DD') : ''
				}&year=${year || ''}`
			);

			delete axios.defaults.headers.common['Content-type'];
			delete axios.defaults.headers.common.Authorization;

			return res.data || {};
		} catch (err) {
			return rejectWithValue({});
		}
	}
);

const salaryReportsSlice = createSlice({
	name: 'salaryLedgersReportManagement/salaryLedgers',
	initialState: {
		salarys: []
	},
	extraReducers: {
		[getSalaryLedgers.fulfilled]: (state, action) => {
			state.salarys = action.payload?.salarys || [];
		},
		[getSalaryLedgers.rejected]: state => {
			state.salarys = [];
		},
		[getAllSalaryLedgers.fulfilled]: (state, action) => {
			state.salarys = action.payload?.salarys || [];
		},
		[getAllSalaryLedgers.rejected]: state => {
			state.salarys = [];
		}
	}
});

export default salaryReportsSlice.reducer;
