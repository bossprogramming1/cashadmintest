import { combineReducers } from '@reduxjs/toolkit';
import salaryReports from './salaryReportSlice';

const reducer = combineReducers({
	salaryReports
});

export default reducer;
