import {
	faBookOpen,
	faCalendarAlt,
	faChevronDown,
	faTextHeight,
	faTimesCircle
} from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { makeStyles, TextField } from '@material-ui/core';
import { Autocomplete } from '@material-ui/lab';
import { DatePicker } from '@material-ui/pickers';
import { bankAndCash } from 'app/@data/data';
import moment from 'moment';
import { useEffect, useState } from 'react';
import { Controller, useFormContext } from 'react-hook-form';
import { useDispatch, useSelector } from 'react-redux';
import { getDepartments, getEmployees } from 'app/store/dataSlice';
import { getReportFilterMakeStyles } from '../../../reportUtils/reportMakeStyls';

const useStyles = makeStyles(theme => ({
	...getReportFilterMakeStyles(theme)
}));

function SalaryFilterMenu({ inShowAllMode, handleGetSalarys, handleGetAllSalarys }) {
	const classes = useStyles();

	const dispatch = useDispatch();

	const [_reRender, setReRender] = useState(0);

	//select field data
	const employees = useSelector(state => state.data.employees);
	const departments = useSelector(state => state.data.departments);

	const methods = useFormContext();
	const { control, getValues, setValue } = methods;
	const values = getValues();

	useEffect(() => {
		dispatch(getEmployees());
		dispatch(getDepartments());
	}, []);

	return (
		<div className={classes.filterMenuContainer}>
			<div className="allFieldContainer borderTop mt-4">
				{/* Date from */}
				<div className="fieldContainer">
					<FontAwesomeIcon
						className="icon cursor-pointer"
						icon={faCalendarAlt}
						onClick={() => document.getElementById('dateAfterEl').click()}
					/>

					<div className="dateLabel" onClick={() => document.getElementById('dateAfterEl').click()}>
						Date From
					</div>

					<Controller
						name="date_after"
						control={control}
						render={({ field }) => {
							return (
								<DatePicker
									id="dateAfterEl"
									className="hidden"
									autoOk
									clearable
									format={'dd/MM/yyyy'}
									maxDate={values.date_before || new Date()}
									views={['month']}
									value={field.value || null}
									onChange={value => {
										if (value) {
											// Extract the selected year and month from the DatePicker value
											const selectedDate = new Date(value);
											const year = selectedDate.getFullYear();
											const month = selectedDate.getMonth() + 1;
											// Set the field value to the month number
											field.onChange(month);
											const newDate = new Date(year, month - 1, 1);
											field.onChange(newDate);
										} else {
											field.onChange('');
										}

										setReRender(Math.random());
										inShowAllMode ? handleGetAllSalarys() : handleGetSalarys();
									}}
								/>
							);
						}}
					/>
				</div>
				{/* Date to */}
				<div className="fieldContainer">
					<FontAwesomeIcon
						className="icon cursor-pointer"
						icon={faCalendarAlt}
						onClick={() => document.getElementById('dateBeforeEl').click()}
					/>

					<div className="dateLabel" onClick={() => document.getElementById('dateBeforeEl').click()}>
						Date To
					</div>

					<Controller
						name="date_before"
						control={control}
						render={({ field }) => {
							return (
								<DatePicker
									id="dateBeforeEl"
									className="hidden"
									autoOk
									clearable
									format={'dd/MM/yyyy'}
									value={field.value || null}
									minDate={values.date_after}
									maxDate={new Date()}
									views={['month']}
									onChange={value => {
										if (value) {
											// Extract the selected year and month from the DatePicker value
											const selectedDate = new Date(value);
											const year = selectedDate.getFullYear();
											const month = selectedDate.getMonth() + 1;
											// Set the field value to the month number
											field.onChange(month);
											const newDate = new Date(year, month, 0);
											field.onChange(newDate);
										} else {
											field.onChange('');
										}

										setReRender(Math.random());
										inShowAllMode ? handleGetAllSalarys() : handleGetSalarys();
									}}
								/>
							);
						}}
					/>
				</div>

				{/* employee */}
				<div className="fieldContainer">
					<FontAwesomeIcon className="icon" icon={faBookOpen} />

					<div
						className="selectLabel"
						style={{
							width: values.employeeFocused ? '0px' : 'fit-content',
							margin: values.employeeFocused ? '0px' : '2px 5px 0px 10px'
						}}
						onClick={() => {
							setValue('employeeFocused', true);
							setReRender(Math.random());
							setTimeout(() => document.getElementById('employeeEl').focus(), 300);
						}}
					>
						Employee
					</div>
					<FontAwesomeIcon
						className="selectOpenIcon cursor-pointer"
						style={{
							width: values.employeeFocused ? '0px' : '15px',
							margin: values.employeeFocused ? '0px' : '2px 10px 0px 0px'
						}}
						onClick={() => {
							setValue('employeeFocused', true);
							setReRender(Math.random());
							setTimeout(() => document.getElementById('employeeEl').focus(), 300);
						}}
						icon={faChevronDown}
					/>

					<Controller
						name="employee"
						control={control}
						render={({ field: { onChange, value } }) => (
							<Autocomplete
								id="employeeEl"
								className="mb-3 selectField"
								style={{
									width: values.employeeFocused ? '130px' : '0px',
									margin: values.employeeFocused ? '0px 10px' : '0px',
									display: values.employeeFocused ? 'block' : 'none'
								}}
								classes={{ endAdornment: 'endAdornment' }}
								openOnFocus={true}
								onClose={() => {
									setValue('employeeFocused', false);
									setReRender(Math.random());
								}}
								freeSolo
								options={employees}
								value={value ? employees.find(data => data.id == value) : null}
								getOptionLabel={option => option?.first_name}
								onChange={(event, newValue) => {
									onChange(newValue?.id);
									setValue('employeeName', newValue?.first_name || '');
									inShowAllMode ? handleGetAllSalarys() : handleGetSalarys();
								}}
								renderInput={params => (
									<TextField
										{...params}
										className="textFieldUnderSelect"
										placeholder="Select Employee"
									/>
								)}
							/>
						)}
					/>
				</div>

				{/* department */}
				<div className="fieldContainer">
					<FontAwesomeIcon className="icon" icon={faBookOpen} />

					<div
						className="selectLabel"
						style={{
							width: values.departmentFocused ? '0px' : '70px',
							margin: values.departmentFocused ? '0px' : '3px 5px 0px 5px'
						}}
						onClick={() => {
							setValue('departmentFocused', true);
							setReRender(Math.random());
							setTimeout(() => document.getElementById('departmentEl').focus(), 300);
						}}
					>
						Department
					</div>
					<FontAwesomeIcon
						className="selectOpenIcon cursor-pointer"
						style={{
							width: values.departmentFocused ? '0px' : '15px',
							margin: values.departmentFocused ? '0px' : '2px 10px 0px 0px'
						}}
						onClick={() => {
							setValue('departmentFocused', true);
							setReRender(Math.random());
							setTimeout(() => document.getElementById('departmentEl').focus(), 300);
						}}
						icon={faChevronDown}
					/>

					<Controller
						name="department"
						control={control}
						render={({ field: { onChange, value } }) => (
							<Autocomplete
								id="departmentEl"
								className="mb-3 selectField"
								style={{
									width: values.departmentFocused ? '130px' : '0px',
									margin: values.departmentFocused ? '0px 10px' : '0px',
									display: values.departmentFocused ? 'block' : 'none'
								}}
								classes={{ endAdornment: 'endAdornment' }}
								openOnFocus={true}
								onClose={() => {
									setValue('departmentFocused', false);
									setReRender(Math.random());
								}}
								freeSolo
								options={departments}
								value={value ? departments.find(data => data.id == value) : null}
								getOptionLabel={option => `${option?.name}`}
								onChange={(event, newValue) => {
									onChange(newValue?.id);
									setValue('departmentName', newValue?.name || '');
									inShowAllMode ? handleGetAllSalarys() : handleGetSalarys();
								}}
								renderInput={params => (
									<TextField
										{...params}
										className="textFieldUnderSelect"
										placeholder="Select Department"
									/>
								)}
							/>
						)}
					/>
				</div>
			</div>

			{/* keywords */}
			<div className="allKeyWrdContainer">
				{values.date_after && (
					<div className="keywordContainer">
						<b>Date From</b>
						<div>
							<FontAwesomeIcon className="iconWithKeyWord" icon={faCalendarAlt} />
							<p>{moment(values.date_after).format('DD-MM-YYYY')}</p>
							<FontAwesomeIcon
								className="closeIconWithKeyWord"
								icon={faTimesCircle}
								onClick={() => {
									setValue('date_after', '');
									inShowAllMode ? handleGetAllSalarys() : handleGetSalarys();
									setReRender(Math.random());
								}}
							/>
						</div>
					</div>
				)}

				{values.date_before && (
					<div className="keywordContainer">
						<b>Date To</b>
						<div>
							<FontAwesomeIcon className="iconWithKeyWord" icon={faCalendarAlt} />
							<p>{moment(new Date(values.date_before)).format('DD-MM-YYYY')}</p>
							<FontAwesomeIcon
								className="closeIconWithKeyWord"
								icon={faTimesCircle}
								onClick={() => {
									setValue('date_before', '');
									inShowAllMode ? handleGetAllSalarys() : handleGetSalarys();
									setReRender(Math.random());
								}}
							/>
						</div>
					</div>
				)}

				{values.employeeName && (
					<div className="keywordContainer">
						<b>employee</b>
						<div>
							<FontAwesomeIcon className="iconWithKeyWord" icon={faBookOpen} />
							<p>{values.employeeName}</p>
							<FontAwesomeIcon
								className="closeIconWithKeyWord"
								icon={faTimesCircle}
								onClick={() => {
									setValue('employeeName', '');
									setValue('employee', '');
									inShowAllMode ? handleGetAllSalarys() : handleGetSalarys();
									setReRender(Math.random());
								}}
							/>
						</div>
					</div>
				)}

				{values.departmentName && (
					<div className="keywordContainer">
						<b>Department</b>
						<div>
							<FontAwesomeIcon className="iconWithKeyWord" icon={faBookOpen} />
							<p>{values.departmentName}</p>
							<FontAwesomeIcon
								className="closeIconWithKeyWord"
								icon={faTimesCircle}
								onClick={() => {
									setValue('departmentName', '');
									setValue('department', '');
									inShowAllMode ? handleGetAllSalarys() : handleGetSalarys();
									setReRender(Math.random());
								}}
							/>
						</div>
					</div>
				)}
			</div>
		</div>
	);
}

export default SalaryFilterMenu;
