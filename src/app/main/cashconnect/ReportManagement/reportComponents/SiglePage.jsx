import { faArrowUp } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import { Email, Language, LocationOn, PhoneEnabled } from '@material-ui/icons';
import moment from 'moment';
import React from 'react';
import { BASE_URL } from '../../../../constant/constants';
import '../Print.css';

function SiglePage({
	classes,
	generalData,
	reporTitle = 'Report',
	tableColumns = [],
	dispatchTableColumns,
	data,
	dateTo,
	dateForm,
	serialNumber,
	setPage,
	page,
	inSiglePageMode,
	setSortBy,
	setSortBySubKey
}) {
	let pageBasedSerialNo = serialNumber;

	return (
		<div
			className={`${classes.pageContainer} printPageContainer`}
			onMouseOver={() => {
				inSiglePageMode || setPage(data.page);
			}}
		>
			<div>
				<div>
					<div className="logoContainer pr-0 md:-pr-20">
						<img
							style={{
								visibility: generalData.header_image ? 'visible' : 'hidden',
								textAlign: 'center'
							}}
							src={generalData.header_image ? `${BASE_URL}${generalData.header_image}` : null}
							alt="Not found"
						/>
					</div>
				</div>
				<div
					style={{
						textAlign: 'center',
						// borderBottom: '1px solid gray',
						// marginBottom: '20px',
						margin: '-30px 20px 0 20px',

						fontSize: '8px'
					}}
				>
					<LocationOn fontSize="small" />
					{` ${generalData?.address}` || ''} &nbsp; &nbsp; &nbsp; <PhoneEnabled fontSize="small" />
					{` ${generalData?.phone || ''}`}&nbsp; &nbsp; <Email fontSize="small" />
					{` ${generalData?.email || ''}`} &nbsp; &nbsp; <Language fontSize="small" />
					{` ${generalData?.site_address || ''}`}
				</div>

				<div className={classes.pageHead}>
					<h2 className="title  pl-0 md:-pl-20">
						<u>{reporTitle}</u>
					</h2>
				</div>
				{dateTo && (
					<div className={classes.pageHead}>
						<h2 className="title  pl-0 md:-pl-20">{`${dateForm} to ${dateTo}`}</h2>
					</div>
				)}

				<Table aria-label="simple table" className={`${classes.table} w-11/12`}>
					<TableHead style={{ backgroundColor: '#D7DBDD' }}>
						<TableRow>
							{tableColumns.map((column, indx) => {
								return column.show ? (
									<TableCell
										key={column.id}
										align="center"
										className="tableCellHead"
										onDrop={e =>
											dispatchTableColumns({
												type: 'dragAndDrop',
												dragger: e.dataTransfer.getData('draggerLebel'),
												dropper: column.id
											})
										}
										onDragOver={e => e.preventDefault()}
									>
										<div
											draggable={true}
											onDragStart={e => e.dataTransfer.setData('draggerLebel', column.id)}
											onClick={() => {
												if (column.sortAction !== false) {
													setSortBy(data.sortBy === column?.name ? '' : column?.name);
													setSortBySubKey &&
														column.subName &&
														setSortBySubKey(column.subName);
												}
											}}
											style={{
												margin: indx === 0 && '0px -5px 0px 5px'
											}}
										>
											{column.label}
											<FontAwesomeIcon
												className={`sortIcon ${column.sortAction === false && 'invisible'}`}
												style={{
													transform:
														data.sortBy === column?.name ? 'rotate(180deg)' : 'rotate(0deg)'
												}}
												icon={faArrowUp}
											/>
										</div>
									</TableCell>
								) : null;
							})}
						</TableRow>
					</TableHead>
					<TableBody>
						{data?.data?.map((dataArr, idx) => (
							<TableRow key={dataArr.id} className="tableRow cursor-pointer" hover>
								{tableColumns.map(column => {
									console.log('column', column, 'dataArr', dataArr);
									return column.show ? (
										<TableCell align="center" className="tableCell">
											<div
												style={{
													whiteSpace: column.type === 'date' && 'nowrap',
													// color:
													// 	column?.name == 'balance' && dataArr?.[column?.name] > 0
													// 		? 'red'
													// 		: 'black',

													...column?.style,
													...dataArr?.rowStyle
												}}
											>
												{column?.subName
													? dataArr?.[column?.name]?.[column?.subName]
													: column.type === 'date'
													? dataArr?.[column?.name]
														? moment(new Date(dataArr?.[column?.name])).format('DD-MM-YYYY')
														: ''
													: column?.name
													? dataArr?.[column?.name]
													: column?.isSerialNo
													? dataArr.hideSerialNo || pageBasedSerialNo++
													: dataArr.getterMethod
													? dataArr.getterMethod(dataArr)
													: column.getterMethod
													? column.getterMethod(dataArr)
													: ''}
											</div>
										</TableCell>
									) : null;
								})}
							</TableRow>
						))}
					</TableBody>
				</Table>
			</div>

			<div className={classes.pageFooterContainer}>
				<div>
					<div style={{ height: 'fit-content' }}>
						<div
							style={{
								display: !inSiglePageMode ? 'flex' : 'block',
								justifyContent: 'space-between',
								marginLeft: '10px',
								marginRight: '10px'
							}}
						>
							<h6 style={{ textAlign: 'left' }}>Developed by RAMS(Bluebay IT Limited)</h6>
							{!inSiglePageMode && (
								<h6 style={{ textAlign: 'right' }}>Page : {inSiglePageMode ? page : data?.page}</h6>
							)}
						</div>
						<img
							style={{ width: '100%' }}
							src="assets/images/logos/footer_report.png"
							alt="footer_report"
						/>
					</div>
				</div>
			</div>
		</div>
	);
}

export default SiglePage;
