import { faBookOpen, faChevronDown, faTimesCircle } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { makeStyles, TextField } from '@material-ui/core';
import { Autocomplete } from '@material-ui/lab';
import { getCategory, getSubCategory, getSubSubCategory } from 'app/store/dataSlice';
import { selectMainTheme } from 'app/store/fuse/settingsSlice';
import { useEffect, useState } from 'react';
import { Controller, useFormContext } from 'react-hook-form';
import { useDispatch, useSelector } from 'react-redux';
import { getReportFilterMakeStyles } from '../../../reportUtils/reportMakeStyls';

const useStyles = makeStyles(theme => ({
	...getReportFilterMakeStyles(theme)
}));

function StockFilterMenu({ inShowAllMode, handleGetStocks, handleGetAllStocks }) {
	const classes = useStyles();

	const dispatch = useDispatch();

	const [_reRender, setReRender] = useState(0);

	//select field data
	const categories = useSelector(state => state.data?.parent_categories);
	const sub_categories = useSelector(state => state.data?.sub_categories);
	const sub_sub_categories = useSelector(state => state.data?.sub_sub_categories);

	const mainTheme = useSelector(selectMainTheme);

	const searchText = useSelector(({ stocksManagement }) => stocksManagement?.stocks?.searchText);

	const methods = useFormContext();
	const { control, getValues, setValue } = methods;
	const values = getValues();

	useEffect(() => {
		dispatch(getCategory());
	}, []);

	return (
		<div className={classes.filterMenuContainer}>
			<div className="flex flex-1 items-center justify-center px-12">
				{/* <ThemeProvider theme={mainTheme}>
					<Paper
						component={motion.div}
						initial={{ y: -20, opacity: 0 }}
						animate={{ y: 0, opacity: 1, transition: { delay: 0.2 } }}
						className="flex items-center w-full max-w-512 px-8 py-4 rounded-16 shadow"
					>
						<Icon color="action">search</Icon>

						<Input
							placeholder="Search"
							className="flex flex-1 mx-8"
							disableUnderline
							fullWidth
							//value={searchText}
							inputProps={{
								'aria-label': 'Search'
							}}
							//onBlur={ev => dispatch(setPassengersSearchText(ev))}
							// onKeyDown={ev => {
							// 	if (ev.key === 'Enter') {
							// 		dispatch(setStocksSearchText(ev));
							// 	}
							// }}
						/>
					</Paper>
				</ThemeProvider> */}
			</div>
			<div className="allFieldContainer borderTop mt-4">
				{/* Category */}
				<div className="fieldContainer">
					<FontAwesomeIcon className="icon" icon={faBookOpen} />

					<div
						className="selectLabel"
						style={{
							width: values.categoryFocused ? '0px' : '70px',
							margin: values.categoryFocused ? '0px' : '3px 5px 0px 5px'
						}}
						onClick={() => {
							setValue('categoryFocused', true);
							setReRender(Math.random());
							setTimeout(() => document.getElementById('categoryEl').focus(), 300);
						}}
					>
						Category
					</div>
					<FontAwesomeIcon
						className="selectOpenIcon cursor-pointer"
						style={{
							width: values.categoryFocused ? '0px' : '15px',
							margin: values.categoryFocused ? '0px' : '2px 10px 0px 0px'
						}}
						onClick={() => {
							setValue('categoryFocused', true);
							setReRender(Math.random());
							setTimeout(() => document.getElementById('categoryEl').focus(), 300);
						}}
						icon={faChevronDown}
					/>

					<Controller
						name="category"
						control={control}
						render={({ field: { onChange, value } }) => (
							<Autocomplete
								id="categoryEl"
								className="mb-3 selectField"
								style={{
									width: values.categoryFocused ? '130px' : '0px',
									margin: values.categoryFocused ? '0px 10px' : '0px',
									display: values.categoryFocused ? 'block' : 'none'
								}}
								classes={{ endAdornment: 'endAdornment' }}
								openOnFocus={true}
								onClose={() => {
									setValue('categoryFocused', false);
									setReRender(Math.random());
								}}
								freeSolo
								options={categories}
								value={value ? categories.find(data => data.id == value) : null}
								getOptionLabel={option => `${option?.name}`}
								onChange={(event, newValue) => {
									onChange(newValue?.id);
									dispatch(getSubCategory(newValue?.id));
									setValue('categoryName', newValue?.name || '');
									inShowAllMode ? handleGetAllStocks() : handleGetStocks();
								}}
								renderInput={params => (
									<TextField
										{...params}
										className="textFieldUnderSelect"
										placeholder="Select Category"
									/>
								)}
							/>
						)}
					/>
				</div>
				{/* Category */}
				<div className="fieldContainer">
					<FontAwesomeIcon className="icon" icon={faBookOpen} />

					<div
						className="selectLabel"
						style={{
							width: values.categorySubFocused ? '0px' : '70px',
							margin: values.categorySubFocused ? '0px' : '3px 5px 0px 5px'
						}}
						onClick={() => {
							setValue('categorySubFocused', true);
							setReRender(Math.random());
							setTimeout(() => document.getElementById('subcategoryEl').focus(), 300);
						}}
					>
						Sub Category
					</div>
					<FontAwesomeIcon
						className="selectOpenIcon cursor-pointer"
						style={{
							width: values.categorySubFocused ? '0px' : '15px',
							margin: values.categorySubFocused ? '0px' : '2px 10px 0px 0px'
						}}
						onClick={() => {
							setValue('categorySubFocused', true);
							setReRender(Math.random());
							setTimeout(() => document.getElementById('subcategoryEl').focus(), 300);
						}}
						icon={faChevronDown}
					/>

					<Controller
						name="category"
						control={control}
						render={({ field: { onChange, value } }) => (
							<Autocomplete
								id="subcategoryEl"
								className="mb-3 selectField"
								style={{
									width: values.categorySubFocused ? '130px' : '0px',
									margin: values.categorySubFocused ? '0px 10px' : '0px',
									display: values.categorySubFocused ? 'block' : 'none'
								}}
								classes={{ endAdornment: 'endAdornment' }}
								openOnFocus={true}
								onClose={() => {
									setValue('categorySubFocused', false);
									setReRender(Math.random());
								}}
								freeSolo
								options={sub_categories}
								value={value ? sub_categories.find(data => data.id == value) : null}
								getOptionLabel={option => `${option?.name}`}
								onChange={(event, newValue) => {
									onChange(newValue?.id);
									dispatch(getSubSubCategory(newValue?.id));
									setValue('categorySubName', newValue?.name || '');
									inShowAllMode ? handleGetAllStocks() : handleGetStocks();
								}}
								renderInput={params => (
									<TextField
										{...params}
										className="textFieldUnderSelect"
										placeholder="Select Category"
									/>
								)}
							/>
						)}
					/>
				</div>
				{/* Category */}
				<div className="fieldContainer">
					<FontAwesomeIcon className="icon" icon={faBookOpen} />

					<div
						className="selectLabel"
						style={{
							width: values.categorySubSubFocused ? '0px' : '70px',
							margin: values.categorySubSubFocused ? '0px' : '3px 5px 0px 5px'
						}}
						onClick={() => {
							setValue('categorySubSubFocused', true);
							setReRender(Math.random());
							setTimeout(() => document.getElementById('subsubcategoryEl').focus(), 300);
						}}
					>
						Child Category
					</div>
					<FontAwesomeIcon
						className="selectOpenIcon cursor-pointer"
						style={{
							width: values.categorySubSubFocused ? '0px' : '15px',
							margin: values.categorySubSubFocused ? '0px' : '2px 10px 0px 0px'
						}}
						onClick={() => {
							setValue('categorySubSubFocused', true);
							setReRender(Math.random());
							setTimeout(() => document.getElementById('subsubcategoryEl').focus(), 300);
						}}
						icon={faChevronDown}
					/>

					<Controller
						name="category"
						control={control}
						render={({ field: { onChange, value } }) => (
							<Autocomplete
								id="subsubcategoryEl"
								className="mb-3 selectField"
								style={{
									width: values.categorySubSubFocused ? '130px' : '0px',
									margin: values.categorySubSubFocused ? '0px 10px' : '0px',
									display: values.categorySubSubFocused ? 'block' : 'none'
								}}
								classes={{ endAdornment: 'endAdornment' }}
								openOnFocus={true}
								onClose={() => {
									setValue('categorySubSubFocused', false);
									setReRender(Math.random());
								}}
								freeSolo
								options={sub_sub_categories}
								value={value ? sub_sub_categories.find(data => data.id == value) : null}
								getOptionLabel={option => `${option?.name}`}
								onChange={(event, newValue) => {
									onChange(newValue?.id);
									// dispatch(getSubSubCategory(newValue?.id));
									setValue('categorySubSubName', newValue?.name || '');
									inShowAllMode ? handleGetAllStocks() : handleGetStocks();
								}}
								renderInput={params => (
									<TextField
										{...params}
										className="textFieldUnderSelect"
										placeholder="Select Category"
									/>
								)}
							/>
						)}
					/>
				</div>
			</div>

			{/* keywords */}
			<div className="allKeyWrdContainer">
				{values.categoryName && (
					<div className="keywordContainer">
						<b>Category</b>
						<div>
							<FontAwesomeIcon className="iconWithKeyWord" icon={faBookOpen} />
							<p>{values.categoryName}</p>
							<FontAwesomeIcon
								className="closeIconWithKeyWord"
								icon={faTimesCircle}
								onClick={() => {
									setValue('categoryName', '');
									setValue('category', '');
									inShowAllMode ? handleGetAllStocks() : handleGetStocks();
									setReRender(Math.random());
								}}
							/>
						</div>
					</div>
				)}
				{values.categorySubName && (
					<div className="keywordContainer">
						<b>Sub Category</b>
						<div>
							<FontAwesomeIcon className="iconWithKeyWord" icon={faBookOpen} />
							<p>{values.categorySubName}</p>
							<FontAwesomeIcon
								className="closeIconWithKeyWord"
								icon={faTimesCircle}
								onClick={() => {
									setValue('categorySubName', '');
									setValue('category', '');
									inShowAllMode ? handleGetAllStocks() : handleGetStocks();
									setReRender(Math.random());
								}}
							/>
						</div>
					</div>
				)}
				{values.categorySubSubName && (
					<div className="keywordContainer">
						<b>Child Category</b>
						<div>
							<FontAwesomeIcon className="iconWithKeyWord" icon={faBookOpen} />
							<p>{values.categorySubSubName}</p>
							<FontAwesomeIcon
								className="closeIconWithKeyWord"
								icon={faTimesCircle}
								onClick={() => {
									setValue('categorySubSubName', '');
									setValue('category', '');
									inShowAllMode ? handleGetAllStocks() : handleGetStocks();
									setReRender(Math.random());
								}}
							/>
						</div>
					</div>
				)}
			</div>
		</div>
	);
}

export default StockFilterMenu;
