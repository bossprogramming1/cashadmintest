import { combineReducers } from '@reduxjs/toolkit';
import receiptReports from './stockReportSlice';

const reducer = combineReducers({
	receiptReports
});

export default reducer;
