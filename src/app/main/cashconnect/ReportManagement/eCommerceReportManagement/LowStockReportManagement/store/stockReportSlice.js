import { createAsyncThunk, createSlice } from '@reduxjs/toolkit';
import axios from 'axios';
import {
	FILTER_SUMMARY_LOW_INVENTORY_REPORT,
	FILTER_SUMMARY_LOW_INVENTORY_REPORT_WITHOUT_PG,
	GET_INVENTORY_SEARCH
} from '../../../../../../constant/constants';

export const getStocks = createAsyncThunk(
	'stocksReportManagement/getStocks',
	async ({ values, pageAndSize }, { rejectWithValue }) => {
		try {
			axios.defaults.headers.common['Content-type'] = 'application/json';
			axios.defaults.headers.common.Authorization = localStorage.getItem('jwt_access_token');

			const res = await axios.get(`${FILTER_SUMMARY_LOW_INVENTORY_REPORT}?category=${values.category || ''}`, {
				params: pageAndSize
			});

			delete axios.defaults.headers.common['Content-type'];
			delete axios.defaults.headers.common.Authorization;

			return res.data || {};
		} catch (err) {
			return rejectWithValue({});
		}
	}
);

export const getAllStocks = createAsyncThunk(
	'stocksReportManagement/getAllStocks',
	async (values, { rejectWithValue }) => {
		try {
			axios.defaults.headers.common['Content-type'] = 'application/json';
			axios.defaults.headers.common.Authorization = localStorage.getItem('jwt_access_token');

			const res = await axios.get(
				`${FILTER_SUMMARY_LOW_INVENTORY_REPORT_WITHOUT_PG}?category=${values.category || ''}`
			);

			delete axios.defaults.headers.common['Content-type'];
			delete axios.defaults.headers.common.Authorization;

			return res.data || {};
		} catch (err) {
			return rejectWithValue({});
		}
	}
);

export const getSearchText = createAsyncThunk(
	'stocksReportManagement/getSearchText',
	async ({ searchText, pageAndSize }, { rejectWithValue }) => {
		// try {
		axios.defaults.headers.common['Content-type'] = 'application/json';
		axios.defaults.headers.common.Authorization = localStorage.getItem('jwt_access_token');
		const res = await axios.get(`${GET_INVENTORY_SEARCH}?keyword=${searchText}`, { params: pageAndSize });

		return res.data;
	}
);

const stockReportsSlice = createSlice({
	name: 'stocksReportManagement/stocks',
	initialState: {
		inventories: [],
		searchText: ''
	},
	reducers: {
		setStockSearchText: {
			reducer: (state, action) => {
				state.searchText = action.payload;
			},
			prepare: event => ({ payload: event.target.value || '' })
		}
	},

	extraReducers: {
		[getStocks.fulfilled]: (state, action) => {
			state.inventories = action.payload?.inventories || [];
		},
		[getStocks.rejected]: state => {
			state.inventories = [];
		},
		[getAllStocks.fulfilled]: (state, action) => {
			state.inventories = action.payload?.inventories || [];
		},
		[getAllStocks.rejected]: state => {
			state.inventories = [];
		},
		[getSearchText.fulfilled]: (state, action) => {
			state.inventories = action.payload?.inventories || [];
		},
		[getSearchText.rejected]: state => {
			state.inventories = [];
		}
	}
});

export const { setStockSearchText } = stockReportsSlice.actions;

export default stockReportsSlice.reducer;
