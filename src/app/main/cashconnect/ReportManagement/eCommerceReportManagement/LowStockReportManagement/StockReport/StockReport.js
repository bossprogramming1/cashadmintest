import FusePageCarded from '@fuse/core/FusePageCarded';
import withReducer from 'app/store/withReducer';
import React from 'react';
import reducer from '../store';
import StockReportHeader from './StockReportHeader';
import StockReportsTable from './StockReportsTable';

const StockReport = () => {
	return (
		<FusePageCarded
			header={<StockReportHeader />}
			headerBgHeight="102px"
			className="bg-grey-300"
			classes={{
				content: 'bg-grey-300',
				contentCard: 'overflow-hidden',
				header: 'min-h-52 h-52'
			}}
			content={<StockReportsTable />}
			innerScroll
		/>
	);
};

export default withReducer('stocksReportManagement', reducer)(StockReport);
