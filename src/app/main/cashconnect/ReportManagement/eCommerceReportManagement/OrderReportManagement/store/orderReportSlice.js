import { createAsyncThunk, createSlice } from '@reduxjs/toolkit';
import axios from 'axios';
import { ORDER_FILTER_BY, ORDER_FILTER_WITHOUT_PG } from '../../../../../../constant/constants';

export const getOrders = createAsyncThunk(
	'ordersReportManagement/getOrders',
	async ({ values, pageAndSize }, { rejectWithValue }) => {
		try {
			axios.defaults.headers.common['Content-type'] = 'application/json';
			axios.defaults.headers.common.Authorization = localStorage.getItem('jwt_access_token');

			const res = await axios.get(
				`${ORDER_FILTER_BY}?brand=${values.brand || ''}&category=${values.category || ''}&date_after=${
					values.date_after || ''
				}&date_before=${values.date_before || ''}`,
				{ params: pageAndSize },
				{
					headers: {
						'Content-type': 'application/json',
						Authorization: localStorage.getItem('jwt_access_token')
					}
				}
			);

			delete axios.defaults.headers.common['Content-type'];
			delete axios.defaults.headers.common.Authorization;

			return res.data || {};
		} catch (err) {
			return rejectWithValue({});
		}
	}
);

export const getAllOrders = createAsyncThunk(
	'ordersReportManagement/getAllOrders',
	async (values, { rejectWithValue }) => {
		try {
			axios.defaults.headers.common['Content-type'] = 'application/json';
			axios.defaults.headers.common.Authorization = localStorage.getItem('jwt_access_token');

			const res = await axios.get(
				`${ORDER_FILTER_WITHOUT_PG}?brand=${values.brand || ''}&category=${values.category || ''}&date_after=${
					values.date_after || ''
				}&date_before=${values.date_before || ''}`,
				{
					headers: {
						'Content-type': 'application/json',
						Authorization: localStorage.getItem('jwt_access_token')
					}
				}
			);

			delete axios.defaults.headers.common['Content-type'];
			delete axios.defaults.headers.common.Authorization;

			return res.data || {};
		} catch (err) {
			return rejectWithValue({});
		}
	}
);

const orderReportsSlice = createSlice({
	name: 'ordersReportManagement/orders',
	initialState: {
		order_items: []
	},
	extraReducers: {
		[getOrders.fulfilled]: (state, action) => {
			state.order_items = action.payload?.order_items || [];
		},
		[getOrders.rejected]: state => {
			state.order_items = [];
		},
		[getAllOrders.fulfilled]: (state, action) => {
			state.order_items = action.payload?.order_items || [];
		},
		[getAllOrders.rejected]: state => {
			state.order_items = [];
		}
	}
});

export default orderReportsSlice.reducer;
