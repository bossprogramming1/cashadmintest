import { combineReducers } from '@reduxjs/toolkit';
import receiptReports from './orderReportSlice';

const reducer = combineReducers({
	receiptReports
});

export default reducer;
