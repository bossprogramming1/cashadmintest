import { createAsyncThunk, createSlice } from '@reduxjs/toolkit';
import { FILTER_PURCHASE_REQ_REPORT, FILTER_PURCHASE_REQ_REPORT_WITHOUT_PG } from 'app/constant/constants';
import axios from 'axios';

export const getPurchaseReq = createAsyncThunk(
	'purchagesReqSummaryReportManagement/getPurchagesReqSummary',
	async ({ values, pageAndSize }, { rejectWithValue }) => {
		try {
			axios.defaults.headers.common['Content-type'] = 'application/json';
			axios.defaults.headers.common.Authorization = localStorage.getItem('jwt_access_token');

			const res = await axios.get(
				`${FILTER_PURCHASE_REQ_REPORT}?invoice_no=${values.invoice_no || ''}&email=${values.email || ''}&contact_no=${values.contact_no 
					||''}&purchase_status=${values.purchase_status || ''}&date_after=${
					values.date_after || ''
				}&date_before=${values.date_before || ''}`,
				{ params: pageAndSize }
			);

			delete axios.defaults.headers.common['Content-type'];
			delete axios.defaults.headers.common.Authorization;

			return res.data || {};
		} catch (err) {
			return rejectWithValue({});
		}
	}
);

export const getAllPurchagesReq = createAsyncThunk(
	'purchagesReqSummaryReportManagement/getAllPurchagesReqSummary',
	async (values, { rejectWithValue }) => {
		try {
			axios.defaults.headers.common['Content-type'] = 'application/json';
			axios.defaults.headers.common.Authorization = localStorage.getItem('jwt_access_token');

			const res = await axios.get(
				`${FILTER_PURCHASE_REQ_REPORT_WITHOUT_PG}?invoice_no=${values.invoice_no || ''}&email=${
					values.email ||''}&purchase_status=${values.purchase_status || ''
				}&date_after=${values.date_after || ''}&date_before=${values.date_before || ''}`
			);

			delete axios.defaults.headers.common['Content-type'];
			delete axios.defaults.headers.common.Authorization;

			return res.data || {};
		} catch (err) {
			return rejectWithValue({});
		}
	}
);

const purchageReqSummaryReportsSlice = createSlice({
	name: 'purchagesReqSummaryReportManagement/purchagesReqSummary',
	initialState: {
		purchase_requests: []
	},
	extraReducers: {
		[getPurchaseReq.fulfilled]: (state, action) => {
			state.purchase_requests = action.payload?.purchase_requests || [];
		},
		[getPurchaseReq.rejected]: state => {
			state.purchase_requests = [];
		},
		[getAllPurchagesReq.fulfilled]: (state, action) => {
			state.purchase_requests = action.payload?.purchase_requests || [];
		},
		[getAllPurchagesReq.rejected]: state => {
			state.purchase_requests = [];
		}
	}
});

export default purchageReqSummaryReportsSlice.reducer;
