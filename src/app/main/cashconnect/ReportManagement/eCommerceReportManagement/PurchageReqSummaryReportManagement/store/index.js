import { combineReducers } from '@reduxjs/toolkit';
import receiptReports from './purchageReqSummaryReportSlice';

const reducer = combineReducers({
	receiptReports
});

export default reducer;
