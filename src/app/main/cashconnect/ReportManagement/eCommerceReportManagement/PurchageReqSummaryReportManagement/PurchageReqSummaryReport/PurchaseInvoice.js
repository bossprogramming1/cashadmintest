/* eslint-disable import/extensions */
import React, { useEffect, useState, useRef } from 'react';
import { makeStyles, styled, useTheme } from '@material-ui/core/styles';
import { withRouter, useParams } from 'react-router-dom';
import Paper from '@material-ui/core/Paper';
import { Box, Typography, Container } from '@material-ui/core';
import TableContainer from '@material-ui/core/TableContainer';
import { useReactToPrint } from 'react-to-print';
import PrintIcon from '@material-ui/icons/Print';
import Table from '@material-ui/core/Table';
import TableHead from '@material-ui/core/TableHead';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableRow from '@material-ui/core/TableRow';
import { format } from 'date-fns';
import { BASE_URL, GET_SITESETTINGS } from 'app/constant/constants';
import { GET_PURCHASE_BY_INVOICE } from '../../../../constant/constants';

const useStyles = makeStyles(theme => ({
	hidden: {
		display: 'none'
	},
	tblContainer: {
		borderRadius: '0px',
		width: '700px',
		marginLeft: '150px',
		marginBottom: '50px'
	},
	table: {
		minWidth: 700
	},
	tableHead: {
		backgroundColor: theme.palette.primary[500]
	},
	dateOfSaleContainer: {
		display: 'flex',
		flexWrap: 'nowrap',
		justifyContent: 'space-between',
		minWidth: '250px'
	},
	paper: {
		marginTop: '10%',
		backgroundColor: theme.palette.background.paper,
		boxShadow: theme.shadows[5],
		padding: theme.spacing(2, 4, 3)
	},
	underline: {
		borderBottom: '1px solid grey',
		marginTop: '4%',
		marginBottom: '1%'
	},
	textMarginBottom: {
		marginBottom: '2%'
	}
}));

const PurchaseInvoice = props => {
	const [purchaseRequestInvoice, setPurchaseRequestInvoice] = useState({});
	const routeParams = useParams();
	const { purchaseId } = routeParams;
	const classes = useStyles(props);
	const Theme = useTheme();
	const PaperColor = Theme.palette.background.paper;
	const [showPrintBtn, setShowPrintBtn] = useState(true);
	let serialNumber = 1;
	const [generalData, setGeneralData] = useState({});

	//get general setting data
	useEffect(() => {
		const authTOKEN = {
			headers: {
				'Content-type': 'application/json',
				Authorization: localStorage.getItem('jwt_access_token')
			}
		};

		fetch(`${GET_SITESETTINGS}`, authTOKEN)
			.then(response => response.json())
			.then(data => setGeneralData(data.general_settings[0] || {}))
			.catch(() => setGeneralData({}));
	}, []);

	const authTOKEN = {
		headers: {
			'Content-type': 'application/json',
			Authorization: localStorage.getItem('jwt_access_token')
		}
	};

	useEffect(() => {
		fetch(`${GET_PURCHASE_BY_INVOICE}${purchaseId}`, authTOKEN)
			.then(response => response.json())
			.then(data => setPurchaseRequestInvoice(data));
	}, []);

	//print
	const componentRef = useRef();
	const handlePrint = useReactToPrint({
		content: () => componentRef.current
	});

	return (
		<Box style={{ backgroundColor: 'white' }}>
			<Box
				style={{
					display: 'flex',
					justifyContent: 'flex-end',
					flexDirection: 'row',
					marginRight: '150px'
				}}
			>
				<PrintIcon
					onClick={handlePrint}
					className="h-52 cursor-pointer"
					style={{
						color: '#6495ED',
						textAlign: 'right',
						height: '30px',
						width: '30px'
					}}
				/>
			</Box>
			<div
				ref={componentRef}
				style={{
					width: '100%',
					marginLeft: 'auto',
					marginRight: 'auto',
					padding: '5vh'
				}}
			>
				<div
					style={{
						display: 'flex',
						justifyContent: 'space-between',
						marginTop: '50px',
						bgcolor: 'background.paper'
					}}
				>
					<Box>
						<Typography variant="h6" gutterBottom component="div">
							BILL FROM:
						</Typography>
						<Typography variant="P" gutterBottom component="div">
							{purchaseRequestInvoice?.bill_from?.site_name}
						</Typography>
						<Typography variant="P" gutterBottom component="div">
							{purchaseRequestInvoice?.bill_from?.address}
						</Typography>
						<Typography variant="P" gutterBottom component="div">
							{purchaseRequestInvoice?.bill_from?.site_address}
						</Typography>
					</Box>
					<Box style={{ textAlign: 'right' }}>
						<img
							style={{ height: '80px', width: '256.33px' }}
							src={`${BASE_URL}${generalData?.logo}`}
							alt="Not found"
						/>
					</Box>
				</div>

				<div
					style={{
						display: 'flex',
						justifyContent: 'space-between',
						marginTop: '50px',
						marginBottom: '50px',
						bgcolor: 'background.paper'
					}}
				>
					<Box>
						<Typography variant="h6" gutterBottom component="div">
							BILL TO:
						</Typography>
						<Typography variant="P" gutterBottom component="div">
							{purchaseRequestInvoice?.purchase_request?.first_name}
						</Typography>
						<Typography variant="P" gutterBottom component="div">
							{purchaseRequestInvoice?.purchase_request?.street_address}
						</Typography>
						<Typography variant="P" gutterBottom component="div">
							{purchaseRequestInvoice?.purchase_request?.email}
						</Typography>
					</Box>
					<Box
						style={{
							display: 'flex',
							justifyContent: 'space-between',
							alignItems: 'center'
						}}
					>
						<Box style={{ textAlign: 'left' }}>
							<Typography variant="P" gutterBottom component="div">
								INVOICE
							</Typography>
							<Typography variant="P" gutterBottom component="div">
								INVOICE DATE
							</Typography>
							<Typography
								variant="P"
								gutterBottom
								component="div"
								style={{
									backgroundColor: '#D7DBDD',
									padding: '5px',
									fontWeight: '600'
								}}
							>
								AMOUNT DUE
							</Typography>
						</Box>
						<Box style={{ textAlign: 'right' }}>
							<Typography variant="P" gutterBottom component="div">
								{purchaseRequestInvoice?.purchase_final?.purchase_request?.invoice_no}
							</Typography>
							<Typography variant="P" gutterBottom component="div">
								{/* {format(new Date(purchaseRequestInvoice?.purchase_request?.request_date), "MMM dd, yyyy")} */}
								{/* {purchaseRequestInvoice?.purchase_request?.request_date} */}
								{/* {purchaseRequestInvoice?.purchase_request?.request_date.slice(0, 10)} */}
								{purchaseRequestInvoice?.purchase_final?.purchase_request?.request_date &&
									format(
										new Date(purchaseRequestInvoice?.purchase_request?.request_date),
										'dd-MM-yyyy'
									)}
							</Typography>
							<Typography
								variant="P"
								gutterBottom
								component="div"
								style={{
									backgroundColor: '#D7DBDD',
									padding: '5px',
									fontWeight: '600'
								}}
							>
								{purchaseRequestInvoice?.purchase_final?.purchase_request?.total_price}
							</Typography>
						</Box>
					</Box>
				</div>

				<Table
					aria-label="simple table"
					style={{
						width: '100%',
						marginLeft: 'auto',
						marginRight: 'auto',
						marginBottom: '50px'
					}}
				>
					<TableHead style={{ backgroundColor: '#D7DBDD' }}>
						<TableRow>
							<TableCell align="center">Sl_No</TableCell>
							<TableCell align="center">Item</TableCell>
							<TableCell align="center">Description</TableCell>
							<TableCell align="center">Quantity</TableCell>
							<TableCell align="center">Unit Cost</TableCell>
							<TableCell align="center">Line Total</TableCell>
						</TableRow>
					</TableHead>
					<TableBody>
						{purchaseRequestInvoice?.items?.map((item, idx) => (
							<TableRow id={idx}>
								<TableCell align="center">{serialNumber++}</TableCell>
								<TableCell align="center">{item?.name}</TableCell>
								<TableCell align="center">{item?.description}</TableCell>
								<TableCell align="center">{item?.quantity}</TableCell>
								<TableCell align="center">{item?.unit_price}</TableCell>
								<TableCell align="center">{item?.subtotal}</TableCell>
							</TableRow>
						))}
					</TableBody>
				</Table>
				<div
					style={{
						display: 'flex',
						justifyContent: 'space-between',
						marginTop: '50px',
						marginBottom: '50px',
						// marginBottom: '50px',
						bgcolor: 'background.paper'
					}}
				>
					<Box>
						<Typography variant="h6" gutterBottom component="div" style={{ fontWeight: '700' }}>
							NOTES/MEMO
						</Typography>
						<Typography variant="P" gutterBottom component="div">
							Free Shipping with 30-day money-back guarantee.
						</Typography>
					</Box>
					<Box
						style={{
							display: 'flex',
							justifyContent: 'space-between',
							alignItems: 'center'
						}}
					>
						<Box style={{ textAlign: 'left' }}>
							<Typography variant="P" gutterBottom component="div">
								SUBTOTAL
							</Typography>
							<Typography variant="P" gutterBottom component="div">
								TAX
							</Typography>
							<Typography
								variant="P"
								gutterBottom
								component="div"
								style={{
									backgroundColor: '#D7DBDD',
									padding: '5px',
									fontWeight: '600'
								}}
							>
								TOTAL
							</Typography>
						</Box>
						<Box style={{ textAlign: 'right' }}>
							<Typography variant="P" gutterBottom component="div">
								{purchaseRequestInvoice?.purchase_request?.total_price}
							</Typography>
							<Typography variant="P" gutterBottom component="div">
								00.00
							</Typography>
							<Typography
								variant="P"
								gutterBottom
								component="div"
								style={{
									backgroundColor: '#D7DBDD',
									padding: '5px',
									fontWeight: '600'
								}}
							>
								{purchaseRequestInvoice?.purchase_request?.total_price}
							</Typography>
						</Box>
					</Box>
				</div>
			</div>
		</Box>
	);
};
export default withRouter(PurchaseInvoice);
