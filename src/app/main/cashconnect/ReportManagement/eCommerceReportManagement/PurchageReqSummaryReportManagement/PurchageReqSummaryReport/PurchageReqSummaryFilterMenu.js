import {
	faBookOpen,
	faCalendarAlt,
	faChevronDown,
	faTextHeight,
	faTimesCircle
} from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { makeStyles, TextField } from '@material-ui/core';
import { Autocomplete } from '@material-ui/lab';
import { DatePicker } from '@material-ui/pickers';
import { getBrand, getPurchaseRequestStatus } from 'app/store/dataSlice';
import moment from 'moment';
import { useEffect, useState, useRef } from 'react';
import { Controller, useFormContext } from 'react-hook-form';
import { useDispatch, useSelector } from 'react-redux';
import { getReportFilterMakeStyles } from '../../../reportUtils/reportMakeStyls';

const useStyles = makeStyles(theme => ({
	...getReportFilterMakeStyles(theme)
}));

function PurhcageReqSummaryFilterMenu({ inShowAllMode, handleGetPurchasesReq, handleGetAllPurchasesReq }) {
	const classes = useStyles();
	const dispatch = useDispatch();
	const [_reRender, setReRender] = useState(0);

	//select field data
	const purchase_statusess = useSelector(state => state.data.purchaseStatus);
	const methods = useFormContext();
	const { control, getValues, setValue } = methods;
	const values = getValues();
	//element refs
	const purchaseNoEl = useRef(null);
	const emailIdE1 = useRef(null);
	const contactNoE1 = useRef(null);

	useEffect(() => {
		dispatch(getBrand());
		dispatch(getPurchaseRequestStatus());
	}, []);

	return (
		<div className={classes.filterMenuContainer}>
			<div className="allFieldContainer borderTop mt-4">
				{/* Date from */}
				<div className="fieldContainer">
					<FontAwesomeIcon
						className="icon cursor-pointer"
						icon={faCalendarAlt}
						onClick={() => document.getElementById('dateAfterEl').click()}
					/>

					<div className="dateLabel" onClick={() => document.getElementById('dateAfterEl').click()}>
						Date From
					</div>

					<Controller
						name="date_after"
						control={control}
						render={({ field }) => {
							return (
								<DatePicker
									id="dateAfterEl"
									className="hidden"
									autoOk
									clearable
									format={'dd/MM/yyyy'}
									maxDate={values.date_before || new Date()}
									value={field.value || ''}
									onChange={value => {
										value
											? field.onChange(moment(new Date(value)).format('YYYY-MM-DD'))
											: field.onChange('');
										setReRender(Math.random());
										inShowAllMode ? handleGetAllPurchasesReq() : handleGetPurchasesReq();
									}}
								/>
							);
						}}
					/>
				</div>

				{/* Date to */}
				<div className="fieldContainer">
					<FontAwesomeIcon
						className="icon cursor-pointer"
						icon={faCalendarAlt}
						onClick={() => document.getElementById('dateBeforeEl').click()}
					/>

					<div className="dateLabel" onClick={() => document.getElementById('dateBeforeEl').click()}>
						Date To
					</div>

					<Controller
						name="date_before"
						control={control}
						render={({ field }) => {
							return (
								<DatePicker
									id="dateBeforeEl"
									className="hidden"
									autoOk
									clearable
									format={'dd/MM/yyyy'}
									value={field.value || ''}
									minDate={values.date_after}
									maxDate={new Date()}
									onChange={value => {
										value
											? field.onChange(moment(new Date(value)).format('YYYY-MM-DD'))
											: field.onChange('');
										setReRender(Math.random());
										inShowAllMode ? handleGetAllPurchasesReq() : handleGetPurchasesReq();
									}}
								/>
							);
						}}
					/>
				</div>

				{/* Invoice No */}
				<div className="fieldContainer">
					<FontAwesomeIcon className="icon" icon={faBookOpen} />

					<input
						ref={purchaseNoEl}
						onKeyDown={e => {
							if (e.key === 'Enter') {
								setValue('invoice_no', e.target.value);
								inShowAllMode ? handleGetAllPurchasesReq() : handleGetPurchasesReq();
								purchaseNoEl.current.blur();
								purchaseNoEl.current.value = '';
								setReRender(Math.random());
							}
						}}
						onFocus={() =>
							(purchaseNoEl.current.value = purchaseNoEl.current.value || values.username || '')
						}
						className="textField"
						style={{ width: '75px' }}
						placeholder="Invoice No"
					/>
				</div>
				{/* Email No */}
				<div className="fieldContainer">
					<FontAwesomeIcon className="icon" icon={faBookOpen} />

					<input
						ref={emailIdE1}
						onKeyDown={e => {
							if (e.key === 'Enter') {
								setValue('email', e.target.value);
								inShowAllMode ? handleGetAllPurchasesReq() : handleGetPurchasesReq();
								emailIdE1.current.blur();
								emailIdE1.current.value = '';
								setReRender(Math.random());
							}
						}}
						onFocus={() => (emailIdE1.current.value = emailIdE1.current.value || values.email || '')}
						className="textField"
						style={{ width: '75px' }}
						placeholder="Email"
					/>
				</div>
				{/* Contact No */}
				<div className="fieldContainer">
					<FontAwesomeIcon className="icon" icon={faBookOpen} />

					<input
						ref={contactNoE1}
						onKeyDown={e => {
							if (e.key === 'Enter') {
								setValue('contact_no', e.target.value);
								inShowAllMode ? handleGetAllPurchasesReq() : handleGetPurchasesReq();
								contactNoE1.current.blur();
								contactNoE1.current.value = '';
								setReRender(Math.random());
							}
						}}
						onFocus={() =>
							(contactNoE1.current.value = contactNoE1.current.value || values.contact_no || '')
						}
						className="textField"
						style={{ width: '75px' }}
						placeholder="Contact No"
					/>
				</div>

				{/* Purchase status */}
				<div className="fieldContainer">
					<FontAwesomeIcon className="icon" icon={faBookOpen} />

					<div
						className="selectLabel"
						style={{
							width: values.statusFocused ? '0px' : '110px',
							margin: values.statusFocused ? '0px' : '2px 5px 0px 10px'
						}}
						onClick={() => {
							setValue('statusFocused', true);
							setReRender(Math.random());
							setTimeout(() => document.getElementById('statusEl').focus(), 300);
						}}
					>
						Purchase Status
					</div>
					<FontAwesomeIcon
						className="selectOpenIcon cursor-pointer"
						style={{
							width: values.statusFocused ? '0px' : '15px',
							margin: values.statusFocused ? '0px' : '2px 10px 0px 0px'
						}}
						onClick={() => {
							setValue('statusFocused', true);
							setReRender(Math.random());
							setTimeout(() => document.getElementById('statusEl').focus(), 300);
						}}
						icon={faChevronDown}
					/>

					<Controller
						name="purchase_status"
						control={control}
						render={({ field: { onChange, value } }) => (
							<Autocomplete
								id="statusEl"
								className="mb-3 selectField"
								style={{
									width: values.statusFocused ? '130px' : '0px',
									margin: values.statusFocused ? '0px 10px' : '0px',
									display: values.statusFocused ? 'block' : 'none'
								}}
								classes={{ endAdornment: 'endAdornment' }}
								openOnFocus={true}
								onClose={() => {
									setValue('statusFocused', false);
									setReRender(Math.random());
								}}
								freeSolo
								options={purchase_statusess}
								value={value ? purchase_statusess.find(data => data?.id == value) : null}
								getOptionLabel={option => option?.name}
								onChange={(event, newValue) => {
									onChange(newValue?.id);
									setValue('statusName', newValue?.name || '');
									inShowAllMode ? handleGetAllPurchasesReq() : handleGetPurchasesReq();
								}}
								renderInput={params => (
									<TextField
										{...params}
										className="textFieldUnderSelect"
										placeholder="Select Status"
									/>
								)}
							/>
						)}
					/>
				</div>
			</div>

			{/* keywords */}
			<div className="allKeyWrdContainer">
				{values.date_after && (
					<div className="keywordContainer">
						<b>Date From</b>
						<div>
							<FontAwesomeIcon className="iconWithKeyWord" icon={faCalendarAlt} />
							<p>{moment(new Date(values.date_after)).format('DD-MM-YYYY')}</p>
							<FontAwesomeIcon
								className="closeIconWithKeyWord"
								icon={faTimesCircle}
								onClick={() => {
									setValue('date_after', '');
									inShowAllMode ? handleGetAllPurchasesReq() : handleGetPurchasesReq();
									setReRender(Math.random());
								}}
							/>
						</div>
					</div>
				)}

				{values.date_before && (
					<div className="keywordContainer">
						<b>Date To</b>
						<div>
							<FontAwesomeIcon className="iconWithKeyWord" icon={faCalendarAlt} />
							<p>{moment(new Date(values.date_before)).format('DD-MM-YYYY')}</p>
							<FontAwesomeIcon
								className="closeIconWithKeyWord"
								icon={faTimesCircle}
								onClick={() => {
									setValue('date_before', '');
									inShowAllMode ? handleGetAllPurchasesReq() : handleGetPurchasesReq();
									setReRender(Math.random());
								}}
							/>
						</div>
					</div>
				)}

				{values.invoice_no && (
					<div className="keywordContainer">
						<b>Invoice No</b>
						<div>
							<FontAwesomeIcon className="iconWithKeyWord" icon={faBookOpen} />
							<p>{values.invoice_no}</p>
							<FontAwesomeIcon
								className="closeIconWithKeyWord"
								icon={faTimesCircle}
								onClick={() => {
									setValue('invoice_no', '');
									purchaseNoEl.current.value = '';
									inShowAllMode ? handleGetAllPurchasesReq() : handleGetPurchasesReq();
									setReRender(Math.random());
								}}
							/>
						</div>
					</div>
				)}
				{values.email && (
					<div className="keywordContainer">
						<b>Email</b>
						<div>
							<FontAwesomeIcon className="iconWithKeyWord" icon={faBookOpen} />
							<p>{values.email}</p>
							<FontAwesomeIcon
								className="closeIconWithKeyWord"
								icon={faTimesCircle}
								onClick={() => {
									setValue('email', '');
									emailIdE1.current.value = '';
									inShowAllMode ? handleGetAllPurchasesReq() : handleGetPurchasesReq();
									setReRender(Math.random());
								}}
							/>
						</div>
					</div>
				)}
				{values.contact_no && (
					<div className="keywordContainer">
						<b>Contact No</b>
						<div>
							<FontAwesomeIcon className="iconWithKeyWord" icon={faBookOpen} />
							<p>{values.contact_no}</p>
							<FontAwesomeIcon
								className="closeIconWithKeyWord"
								icon={faTimesCircle}
								onClick={() => {
									setValue('contact_no', '');
									contactNoE1.current.value = '';
									inShowAllMode ? handleGetAllPurchasesReq() : handleGetPurchasesReq();
									setReRender(Math.random());
								}}
							/>
						</div>
					</div>
				)}
				{values.statusName && (
					<div className="keywordContainer">
						<b>Status</b>
						<div>
							<FontAwesomeIcon className="iconWithKeyWord" icon={faBookOpen} />
							<p>{values.statusName}</p>
							<FontAwesomeIcon
								className="closeIconWithKeyWord"
								icon={faTimesCircle}
								onClick={() => {
									setValue('statusName', '');
									setValue('purchase_status', '');
									inShowAllMode ? handleGetAllPurchasesReq() : handleGetPurchasesReq();
									setReRender(Math.random());
								}}
							/>
						</div>
					</div>
				)}
			</div>
		</div>
	);
}

export default PurhcageReqSummaryFilterMenu;
