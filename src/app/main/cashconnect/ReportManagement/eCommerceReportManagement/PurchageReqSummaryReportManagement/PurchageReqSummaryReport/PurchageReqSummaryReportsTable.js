/* eslint-disable no-sparse-arrays */
import { faBookOpen, faDownload, faFileExcel, faFilePdf, faScroll } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { yupResolver } from '@hookform/resolvers/yup';
import { makeStyles } from '@material-ui/core';
import { GetApp, ViewWeek } from '@material-ui/icons';
import Tooltip from '@mui/material/Tooltip';
import Button from '@mui/material/Button';

import PrintIcon from '@material-ui/icons/Print';
import useReportData2WithRowSize from 'app/@customHooks/useReportData2WithRowSize';
import FuseLoading from '@fuse/core/FuseLoading';
import useUserInfo from 'app/@customHooks/useUserInfo';
import getPaginationData from 'app/@helpers/getPaginationData';
import getTotalAmount from 'app/@helpers/getTotalAmount';
import html2PDF from 'jspdf-html2canvas';
import React, { useEffect, useLayoutEffect, useReducer, useRef, useState } from 'react';
import { unstable_batchedUpdates } from 'react-dom';
import { FormProvider, useForm } from 'react-hook-form';
import ReactHtmlTableToExcel from 'react-html-table-to-excel';
import { useDispatch } from 'react-redux';
import { useReactToPrint } from 'react-to-print';
import * as yup from 'yup';
import ReceiptIcon from '@material-ui/icons/Receipt';
import history from '@history';
import { GET_SITESETTINGS } from '../../../../../../constant/constants';
import ColumnLabel from '../../../reportComponents/ColumnLabel';
import Pagination from '../../../reportComponents/Pagination';
import SinglePage from '../../../reportComponents/SiglePage';
import { getReportMakeStyles } from '../../../reportUtils/reportMakeStyls';
import tableColumnsReducer from '../../../reportUtils/tableColumnsReducer';
import { getAllPurchagesReq, getPurchaseReq } from '../store/purchageReqSummaryReportSlice';
import PurhcageReqSummaryFilterMenu from './PurchageReqSummaryFilterMenu';

const useStyles = makeStyles(theme => ({
	...getReportMakeStyles(theme),
	inputBg: {
		background: theme.palette.primary.main,
		color: theme.palette.primary[50]
	}
}));

const schema = yup.object().shape({});

const initialTableColumnsState = [
	{ id: 1, label: 'Sl_No', sortAction: false, isSerialNo: true, show: true },
	{ id: 2, label: 'Purchase Req Date', name: 'request_date', show: true, type: 'date' },
	{ id: 3, label: 'Invoice No', name: 'invoice_no', show: true },
	{
		id: 4,
		label: 'User Name',
		getterMethod: data => `${data.first_name} ${data.last_name}`,
		style: { whiteSpace: 'nowrap' },
		show: true
	},
	{ id: 5, label: 'Email', name: 'email', show: true },
	{ id: 6, label: 'Contact No', name: 'contact_no', show: true },
	{ id: 7, label: 'Purchase Status', name: 'purchase_status', show: true },

	{ id: 8, label: 'Transfer Status', name: 'transfer_status', show: true },
	,
	{
		id: 9,
		label: 'Action',
		getterMethod: item => {
			return (
				<ReceiptIcon
					onClick={() => {
						history.push(
							`/apps/purchase-management/purchase-request/purchase-invoice/${item.id}/${item.first_name}`
						);
					}}
					className="h-52 cursor-pointer"
					style={{ color: 'orange' }}
				/>
			);
		},
		show: true
	}
];

const PurchageReqSumaryReportsTable = () => {
	const classes = useStyles();

	const methods = useForm({
		mode: 'onChange',
		defaultValues: {},
		resolver: yupResolver(schema)
	});

	const { getValues } = methods;

	const dispatch = useDispatch();

	const { authTOKEN } = useUserInfo();

	const [generalData, setGeneralData] = useState({});

	const [rowSize, setRowSize] = useState(20);

	const [modifiedPurchageFinalData, setModifiedPurchageFinalData, setSortBy] = useReportData2WithRowSize({
		type: 'purchageFinalSummaryData',
		extraRowCount: 1,
		row: rowSize
	});
	const handleRowChange = event => {
		const newRowSize = parseInt(event.target.value); // Get the new row size from the input field and parse it as an integer
		if (!isNaN(newRowSize) && newRowSize > 0) {
			setRowSize(newRowSize);
		} else if (isNaN(newRowSize)) {
			setRowSize(20);
		} else {
			setRowSize(1);
		}
	};

	const [pagination, setPagination] = useState(false);

	const [loading, setLoading] = useState(false);
	const [tableColumns, dispatchTableColumns] = useReducer(tableColumnsReducer, initialTableColumnsState);

	const [totalAmount, setTotalAmount] = useState(0);

	//tools state
	const [inPrint, setInPrint] = useState(false);
	const [inSiglePageMode, setInSiglePageMode] = useState(false);
	const [inShowAllMode, setInShowAllMode] = useState(false);
	const [inDowloadPdf, setInDowloadPdf] = useState(false);
	const [inDowloadExcel, setInDowloadExcel] = useState(false);
	const [showClmSelectOption, setShowClmSelectOption] = useState(false);

	//pagination state
	const [page, setPage] = useState(1);
	const [size, setSize] = useState(25);
	const [totalPages, setTotalPages] = useState(0);
	const [totalElements, setTotalElements] = useState(0);

	//get general setting data
	useEffect(() => {
		fetch(`${GET_SITESETTINGS}`, authTOKEN)
			.then(response => response.json())
			.then(data => setGeneralData(data.general_settings[0] || {}))
			.catch(() => setGeneralData({}));
	}, []);

	//print dom ref
	const componentRef = useRef();

	//printer action
	const printAction = useReactToPrint({
		content: () => componentRef.current
	});

	//print handler
	const handlePrint = () => {
		setInPrint(true);
		printAction();
		setInPrint(false);
	};

	//pdf downloader action
	const pdfDownloadAction = () => {
		html2PDF(downloadPage, {
			margin: [0, 0, 0, 0],
			filename: 'pdfhtml2.pdf',
			html2canvas: {
				dpi: 300,
				letterRendering: true
			},
			setTestIsImage: false,
			useCORS: true,
			jsPDF: { unit: 'mm', format: 'a4', orientation: 'portrait' }
		});
		setInDowloadPdf(false);
	};

	//pdf download handler
	const handlePdfDownload = () => {
		setInDowloadPdf(true);
		if (!inDowloadPdf) {
			if (!inShowAllMode && totalPages > 1) {
				handleGetAllPurchasesReq(null, () => {
					pdfDownloadAction();
					setInDowloadPdf(false);
					handleGetPurchasesReq();
				});
			} else {
				pdfDownloadAction();
				setInDowloadPdf(false);
			}
		}
	};

	//exel download page dom
	let downloadPage = document.getElementById('downloadPage');

	//exel download handler
	const handleExelDownload = () => {
		setInDowloadExcel(true);
		if (!inDowloadExcel) {
			if (!inShowAllMode && totalPages > 1) {
				handleGetAllPurchasesReq(null, () => {
					document.getElementById('test-table-xls-button').click();
					setInDowloadExcel(false);
					handleGetPurchasesReq();
				});
			} else {
				document.getElementById('test-table-xls-button').click();
				setInDowloadExcel(false);
			}
		}
	};

	//column select close handler
	useLayoutEffect(() => {
		window.addEventListener('click', e => {
			if (e.target.id !== 'insideClmSelect') setShowClmSelectOption(false);
		});
	}, []);

	//pagination handler
	const firstPageHandler = event => {
		handleGetPurchasesReq(event.page);
	};
	const previousPageHandler = event => {
		handleGetPurchasesReq(event.page);
	};
	const nextPageHandler = event => {
		handleGetPurchasesReq(event.page);
	};
	const lastPageHandler = event => {
		handleGetPurchasesReq(event.page);
	};

	//get Purchase
	const handleGetPurchasesReq = (pagePram, callBack) => {
		setLoading(true); // this

		dispatch(getPurchaseReq({ values: getValues(), pageAndSize: { page: pagePram || page, size } })).then(res => {
			setLoading(false); // this
			unstable_batchedUpdates(() => {
				callBack && callBack(res.payload);
				const mainArr = res.payload?.purchase_requests || [];
				const modifiedArr = [];
				mainArr.map(purchase => {
					modifiedArr.push({
						...purchase,
						purchase_status: purchase?.purchase_status?.name,
						transfer_status: purchase?.transfer_status,

						rowColumnStyle: {
							columnKey: ['purchase_status', 'transfer_status'],
							style: {
								color:
									purchase?.purchase_status?.name === 'pending'
										? 'orange'
										: purchase?.purchase_status?.name === 'verified'
										? 'green'
										: purchase?.purchase_status?.name === 'cancelled'
										? 'red'
										: purchase?.purchase_status?.name === 'submitted'
										? '#7803dd'
										: 'black',
								fontWeight: 'bold'
							}
							// {
							// 	color:
							// 		purchase?.transfer_status === 'pending'
							// 			? 'orange'
							// 			: purchase?.transfer_status === 'verified'
							// 			? 'green'
							// 			: purchase?.transfer_status === 'cancelled'
							// 			? 'red'
							// 			: purchase?.transfer_status === 'submitted'
							// 			? '#7803dd'
							// 			: 'black',
							// 	fontWeight: 'bold'
							// }
						}
					});
				});
				setModifiedPurchageFinalData(modifiedArr || []);

				setTotalAmount(getTotalAmount(modifiedArr, 'price'));
				setPage(res.payload?.page || 1);
				setSize(res.payload?.size || 25);
				setTotalPages(res.payload?.total_pages || 0);
				setTotalElements(res.payload?.total_elements || 0);
				setInSiglePageMode(true);
				setInShowAllMode(false);
				setPagination(true); // this
			});
		});
	};

	//get all Purchase without pagination
	const handleGetAllPurchasesReq = (callBack, callBackAfterStateUpdated) => {
		setLoading(true); // this

		dispatch(getAllPurchagesReq(getValues())).then(res => {
			setLoading(false); // this
			unstable_batchedUpdates(() => {
				callBack && callBack(res.payload);
				const mainArr = res.payload?.purchase_requests || [];
				const modifiedArr = [];
				mainArr.map(purchase => {
					modifiedArr.push({
						...purchase,
						purchase_status: purchase?.purchase_status?.name,
						transfer_status: purchase?.transfer_status,

						rowColumnStyle: {
							columnKey: ['purchase_status'],
							style: {
								color:
									purchase?.purchase_status?.name === 'pending'
										? 'orange'
										: purchase?.purchase_status?.name === 'verified'
										? 'green'
										: purchase?.purchase_status?.name === 'cancelled'
										? 'red'
										: purchase?.purchase_status?.name === 'submitted'
										? '#7803dd'
										: 'black',
								fontWeight: 'bold'
							}
							// {
							// 	color:
							// 		purchase?.transfer_status === 'pending'
							// 			? 'orange'
							// 			: purchase?.transfer_status === 'verified'
							// 			? 'green'
							// 			: purchase?.transfer_status === 'cancelled'
							// 			? 'red'
							// 			: purchase?.transfer_status === 'submitted'
							// 			? '#7803dd'
							// 			: 'black',
							// 	fontWeight: 'bold'
							// }
						}
					});
				});
				setModifiedPurchageFinalData(modifiedArr || []);

				setTotalAmount(getTotalAmount(modifiedArr, 'price'));
				setPage(res.payload?.page || 1);
				setSize(res.payload?.size || 25);
				setTotalPages(res.payload?.total_pages || 0);
				setTotalElements(res.payload?.total_elements || 0);
				setInSiglePageMode(true);
				setInShowAllMode(false);
				setPagination(false); // this
			});
			callBackAfterStateUpdated && callBackAfterStateUpdated(res.payload);
		});
	};

	return (
		<>
			<div className={classes.headContainer}>
				{/* filter */}
				<FormProvider {...methods}>
					<PurhcageReqSummaryFilterMenu
						inShowAllMode={inShowAllMode}
						handleGetPurchasesReq={handleGetPurchasesReq}
						handleGetAllPurchasesReq={handleGetAllPurchasesReq}
					/>
				</FormProvider>
			</div>
			<div className={`${classes.menubar} justify-start md:justify-center`}>
				{/* pagination */}
				<div style={{ visibility: pagination ? 'visible' : 'hidden' }}>
					<Pagination
						page={page}
						size={size}
						totalPages={totalPages || 0}
						totalElements={totalElements || 0}
						onClickFirstPage={firstPageHandler}
						onClickPreviousPage={previousPageHandler}
						onClickNextPage={nextPageHandler}
						onClickLastPage={lastPageHandler}
					/>
				</div>

				<div className="downloadIcon">
					{/* download options*/}
					<div className="downloadOptionContainer">
						<div className="indicator"></div>
						<div className="downloadOptions shadow-4">
							{/* download as Pdf */}
							{/*<div
								className="cursor-pointer downloadContainer shadow-4"
								style={{ width: '150px', margin: '10px' }}
								onClick={() => handlePdfDownload()}
							>
								<FontAwesomeIcon icon={faFilePdf} />
								<b>Download PDF</b>
								<FontAwesomeIcon icon={faDownload} />
							</div>*/}

							{/* download as Excel */}
							<div
								className="cursor-pointer downloadContainer shadow-4"
								style={{ width: '160px', margin: '0px 10px 10px 10px' }}
								onClick={() => handleExelDownload()}
							>
								<FontAwesomeIcon icon={faFileExcel} />
								<b>Download Excel</b>
								<FontAwesomeIcon icon={faDownload} />
							</div>
						</div>
					</div>
					{/* download icon*/}
					<GetApp
						className="cursor-pointer inside icon"
						style={{ margin: '0px', border: (inDowloadPdf || inDowloadExcel) && '1px solid' }}
					/>
				</div>

				{/* show single page icon*/}
				<Tooltip title={<p style={{ fontSize: '130%' }}>Purchase</p>} placement="top">
					<span>
						<FontAwesomeIcon
							className="cursor-pointer inside icon"
							style={{ padding: '8px', border: inSiglePageMode && '1px solid' }}
							onClick={() => handleGetPurchasesReq()}
							icon={faBookOpen}
						/>
					</span>
				</Tooltip>
				{/* show all page icon*/}
				<Tooltip title={<p style={{ fontSize: '130%' }}>Purchases</p>} placement="top">
					<span>
						<FontAwesomeIcon
							className="cursor-pointer inside icon"
							style={{ padding: '8px', border: inShowAllMode && '1px solid' }}
							onClick={() => handleGetAllPurchasesReq()}
							icon={faScroll}
						/>
					</span>
				</Tooltip>

				{/* column select option icon*/}
				<div className="columnSelectContainer">
					<ViewWeek
						id="insideClmSelect"
						className="cursor-pointer inside icon"
						style={{ margin: '0px', padding: '6px', border: showClmSelectOption && '1px solid' }}
						onClick={() => setTimeout(() => setShowClmSelectOption(true), 0)}
					/>

					{/* columns */}
					<div
						id="insideClmSelect"
						className={`allColumnContainer shadow-5 ${showClmSelectOption ? 'block' : 'hidden'}`}
					>
						{tableColumns.map(column => (
							<ColumnLabel key={column.id} column={column} dispatchTableColumns={dispatchTableColumns} />
						))}
					</div>
				</div>
				{/* print icon*/}
				<Tooltip title={<p style={{ fontSize: '130%' }}>Print</p>} placement="top">
					<PrintIcon
						className="cursor-pointer inside icon"
						style={{ padding: '6px', border: inPrint && '1px solid' }}
						onClick={() => handlePrint()}
					/>
				</Tooltip>
				<div style={{ display: 'flex' }}>
					<p htmlFor="rowSize">Row Size:</p>
					<input
						type="text"
						id="rowSize"
						style={{
							borderRadius: '10px',
							marginLeft: '10px',
							padding: '2px',
							width: '20%',
							textAlign: 'center'
						}}
						className={`rounded-input ${classes.inputBg}`}
						value={modifiedPurchageFinalData.row}
						onChange={handleRowChange}
					/>
				</div>
			</div>

			{/* excel converter */}
			<div className="hidden">
				<ReactHtmlTableToExcel
					id="test-table-xls-button"
					className="download-table-xls-button"
					table="table-to-xls"
					filename="tablexls"
					sheet="tablexls"
					buttonText="Download as XLS"
				/>
			</div>
			{loading ? (
				<FuseLoading />
			) : (
				<table id="table-to-xls" className="w-full" style={{ minHeight: '270px' }}>
					<div ref={componentRef} id="downloadPage">
						{/* each single page (table) */}
						{modifiedPurchageFinalData.map((order, idx) => {
							return (
								<SinglePage
									classes={classes}
									generalData={generalData}
									reporTitle="Purchase Report"
									tableColumns={tableColumns}
									dispatchTableColumns={dispatchTableColumns}
									data={order}
									serialNumber={
										pagination ? page * size - size + 1 : order.page * order.size - order.size + 1
									}
									setPage={setPage}
									page={page}
									inSiglePageMode={inSiglePageMode}
									setSortBy={setSortBy}
								/>
							);
						})}
					</div>
				</table>
			)}
		</>
	);
};
export default PurchageReqSumaryReportsTable;
