import { createAsyncThunk, createSlice } from '@reduxjs/toolkit';
import axios from 'axios';
import { ORDER_SUMMARY_FILTER_BY, ORDER_SUMMARY_FILTER_WITHOUT_PG } from '../../../../../../constant/constants';

export const getOrders = createAsyncThunk(
	'ordersSummaryReportManagement/getOrders',
	async ({ values, pageAndSize }, { rejectWithValue }) => {
		try {
			axios.defaults.headers.common['Content-type'] = 'application/json';
			axios.defaults.headers.common.Authorization = localStorage.getItem('jwt_access_token');

			const res = await axios.get(
				`${ORDER_SUMMARY_FILTER_BY}?order_status=${values.order_status || ''}&is_paid=${
					values.is_paid || ''
				}&is_online=${values.is_online || ''}&order_no=${values.order_no || ''}&branch=${
					values?.name || ''
				}&customer=${values.email || ''}&date_after=${values.date_after || ''}&date_before=${
					values.date_before || ''
				}`,
				{ params: pageAndSize },
				{
					headers: {
						'Content-type': 'application/json',
						Authorization: localStorage.getItem('jwt_access_token')
					}
				}
			);

			delete axios.defaults.headers.common['Content-type'];
			delete axios.defaults.headers.common.Authorization;

			return res.data || {};
		} catch (err) {
			return rejectWithValue({});
		}
	}
);

export const getAllOrders = createAsyncThunk(
	'ordersSummaryReportManagement/getAllOrders',
	async (values, { rejectWithValue }) => {
		try {
			axios.defaults.headers.common['Content-type'] = 'application/json';
			axios.defaults.headers.common.Authorization = localStorage.getItem('jwt_access_token');

			const res = await axios.get(
				`${ORDER_SUMMARY_FILTER_WITHOUT_PG}?order_status=${values.order_status || ''}&is_paid=${
					values.is_paid || ''
				}&is_online=${values.is_online || ''}&order_no=${values.order_no || ''}&customer=${
					values.email || ''
				}&branch=${values?.name || ''}&date_after=${values.date_after || ''}&date_before=${
					values.date_before || ''
				}`,
				{
					headers: {
						'Content-type': 'application/json',
						Authorization: localStorage.getItem('jwt_access_token')
					}
				}
			);

			delete axios.defaults.headers.common['Content-type'];
			delete axios.defaults.headers.common.Authorization;

			return res.data || {};
		} catch (err) {
			return rejectWithValue({});
		}
	}
);

const orderReportsSlice = createSlice({
	name: 'ordersSummaryReportManagement/orders',
	initialState: {
		orders: []
	},
	extraReducers: {
		[getOrders.fulfilled]: (state, action) => {
			state.orders = action.payload?.orders || [];
		},
		[getOrders.rejected]: state => {
			state.orders = [];
		},
		[getAllOrders.fulfilled]: (state, action) => {
			state.orders = action.payload?.orders || [];
		},
		[getAllOrders.rejected]: state => {
			state.orders = [];
		}
	}
});

export default orderReportsSlice.reducer;
