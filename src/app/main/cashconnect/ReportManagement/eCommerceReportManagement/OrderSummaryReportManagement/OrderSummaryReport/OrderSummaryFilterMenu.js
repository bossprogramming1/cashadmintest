import {
	faBookOpen,
	faCalendarAlt,
	faChevronDown,
	faTextHeight,
	faTimesCircle
} from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { makeStyles, TextField } from '@material-ui/core';
import { Autocomplete } from '@material-ui/lab';
import { DatePicker } from '@material-ui/pickers';
import { booleanTypes } from 'app/@data/data';
import { getBranches, getCusotmerDetails, getOrdersStatus } from 'app/store/dataSlice';
import moment from 'moment';
import { useEffect, useRef, useState } from 'react';
import { Controller, useFormContext } from 'react-hook-form';
import { useDispatch, useSelector } from 'react-redux';
import { getReportFilterMakeStyles } from '../../../reportUtils/reportMakeStyls';

const useStyles = makeStyles(theme => ({
	...getReportFilterMakeStyles(theme)
}));

function OrderSummaryFilterMenu({ inShowAllMode, handleGetOrders, handleGetAllOrders }) {
	const classes = useStyles();

	const dispatch = useDispatch();

	const [_reRender, setReRender] = useState(0);

	//select field data
	const ordersStatuses = useSelector(state => state.data.ordersStatus);
	const customerDetails = useSelector(state => state.data.cusotmerDetails);
	const branches = useSelector(state => state.data.branches);
	const paymentStatus = booleanTypes;
	const onlineStatus = booleanTypes;
	const methods = useFormContext();
	const { control, getValues, setValue } = methods;
	const values = getValues();
	const invoiceNoEl = useRef(null);

	useEffect(() => {
		dispatch(getOrdersStatus());
		dispatch(getCusotmerDetails());
		dispatch(getBranches());
	}, []);
	console.log('customerDetails', customerDetails);
	return (
		<div className={classes.filterMenuContainer}>
			<div className="allFieldContainer borderTop mt-4">
				{/* Date from */}
				<div className="fieldContainer">
					<FontAwesomeIcon
						className="icon cursor-pointer"
						icon={faCalendarAlt}
						onClick={() => document.getElementById('dateAfterEl').click()}
					/>

					<div className="dateLabel" onClick={() => document.getElementById('dateAfterEl').click()}>
						Date From
					</div>

					<Controller
						name="date_after"
						control={control}
						render={({ field }) => {
							return (
								<DatePicker
									id="dateAfterEl"
									className="hidden"
									autoOk
									clearable
									format={'dd/MM/yyyy'}
									maxDate={values.date_before || new Date()}
									value={field.value || ''}
									onChange={value => {
										value
											? field.onChange(moment(new Date(value)).format('YYYY-MM-DD'))
											: field.onChange('');
										setReRender(Math.random());
										inShowAllMode ? handleGetAllOrders() : handleGetOrders();
									}}
								/>
							);
						}}
					/>
				</div>
				{/* Date to */}
				<div className="fieldContainer">
					<FontAwesomeIcon
						className="icon cursor-pointer"
						icon={faCalendarAlt}
						onClick={() => document.getElementById('dateBeforeEl').click()}
					/>

					<div className="dateLabel" onClick={() => document.getElementById('dateBeforeEl').click()}>
						Date To
					</div>

					<Controller
						name="date_before"
						control={control}
						render={({ field }) => {
							return (
								<DatePicker
									id="dateBeforeEl"
									className="hidden"
									autoOk
									clearable
									format={'dd/MM/yyyy'}
									value={field.value || ''}
									minDate={values.date_after}
									maxDate={new Date()}
									onChange={value => {
										value
											? field.onChange(moment(new Date(value)).format('YYYY-MM-DD'))
											: field.onChange('');
										setReRender(Math.random());
										inShowAllMode ? handleGetAllOrders() : handleGetOrders();
									}}
								/>
							);
						}}
					/>
				</div>
				{/* Invoice No */}
				<div className="fieldContainer">
					<FontAwesomeIcon className="icon" icon={faBookOpen} />

					<input
						ref={invoiceNoEl}
						onKeyDown={e => {
							if (e.key === 'Enter') {
								setValue('order_no', e.target.value);
								inShowAllMode ? handleGetAllOrders() : handleGetOrders();
								invoiceNoEl.current.blur();
								invoiceNoEl.current.value = '';
								setReRender(Math.random());
							}
						}}
						onFocus={() => (invoiceNoEl.current.value = invoiceNoEl.current.value || values.username || '')}
						className="textField"
						style={{ width: '75px' }}
						placeholder="Order No"
					/>
				</div>
				{/* order status */}
				<div className="fieldContainer">
					<FontAwesomeIcon className="icon" icon={faBookOpen} />

					<div
						className="selectLabel"
						style={{
							width: values.statusFocused ? '0px' : '82px',
							margin: values.statusFocused ? '0px' : '2px 5px 0px 10px'
						}}
						onClick={() => {
							setValue('statusFocused', true);
							setReRender(Math.random());
							setTimeout(() => document.getElementById('statusEl').focus(), 300);
						}}
					>
						Order Status
					</div>
					<FontAwesomeIcon
						className="selectOpenIcon cursor-pointer"
						style={{
							width: values.statusFocused ? '0px' : '15px',
							margin: values.statusFocused ? '0px' : '2px 10px 0px 0px'
						}}
						onClick={() => {
							setValue('statusFocused', true);
							setReRender(Math.random());
							setTimeout(() => document.getElementById('statusEl').focus(), 300);
						}}
						icon={faChevronDown}
					/>

					<Controller
						name="order_status"
						control={control}
						render={({ field: { onChange, value } }) => (
							<Autocomplete
								id="statusEl"
								className="mb-3 selectField"
								style={{
									width: values.statusFocused ? '130px' : '0px',
									margin: values.statusFocused ? '0px 10px' : '0px',
									display: values.statusFocused ? 'block' : 'none'
								}}
								classes={{ endAdornment: 'endAdornment' }}
								openOnFocus={true}
								onClose={() => {
									setValue('statusFocused', false);
									setReRender(Math.random());
								}}
								freeSolo
								options={ordersStatuses}
								value={value ? ordersStatuses.find(data => data?.id == value) : null}
								getOptionLabel={option => option?.name}
								onChange={(event, newValue) => {
									onChange(newValue?.id);
									setValue('statusName', newValue?.name || '');
									inShowAllMode ? handleGetAllOrders() : handleGetOrders();
								}}
								renderInput={params => (
									<TextField
										{...params}
										className="textFieldUnderSelect"
										placeholder="Select Status"
									/>
								)}
							/>
						)}
					/>
				</div>
				{/* By Payment */}
				<div className="fieldContainer">
					<FontAwesomeIcon className="icon" icon={faBookOpen} />

					<div
						className="selectLabel"
						style={{
							width: values.paymentFocused ? '0px' : '77px',
							margin: values.paymentFocused ? '0px' : '3px 5px 0px 5px'
						}}
						onClick={() => {
							setValue('paymentFocused', true);
							setReRender(Math.random());
							setTimeout(() => document.getElementById('paymentEl').focus(), 300);
						}}
					>
						By Payment
					</div>
					<FontAwesomeIcon
						className="selectOpenIcon cursor-pointer"
						style={{
							width: values.paymentFocused ? '0px' : '15px',
							margin: values.paymentFocused ? '0px' : '2px 10px 0px 0px'
						}}
						onClick={() => {
							setValue('paymentFocused', true);
							setReRender(Math.random());
							setTimeout(() => document.getElementById('paymentEl').focus(), 300);
						}}
						icon={faChevronDown}
					/>

					<Controller
						name="is_paid"
						control={control}
						render={({ field: { onChange, value } }) => (
							<Autocomplete
								id="paymentEl"
								className="mb-3 selectField"
								style={{
									width: values.paymentFocused ? '130px' : '0px',
									margin: values.paymentFocused ? '0px 10px' : '0px',
									display: values.paymentFocused ? 'block' : 'none'
								}}
								classes={{ endAdornment: 'endAdornment' }}
								openOnFocus={true}
								onClose={() => {
									setValue('paymentFocused', false);
									setReRender(Math.random());
								}}
								freeSolo
								options={paymentStatus}
								value={value ? paymentStatus.find(data => data.id == value) : null}
								getOptionLabel={option => `${option?.name}`}
								onChange={(event, newValue) => {
									onChange(newValue?.id);
									setValue('paymentName', newValue?.name || '');
									inShowAllMode ? handleGetAllOrders() : handleGetOrders();
								}}
								renderInput={params => (
									<TextField
										{...params}
										className="textFieldUnderSelect"
										placeholder="Select Payment"
									/>
								)}
							/>
						)}
					/>
				</div>
				{/* By Online */}
				<div className="fieldContainer">
					<FontAwesomeIcon className="icon" icon={faBookOpen} />

					<div
						className="selectLabel"
						style={{
							width: values.onlineFocused ? '0px' : '60px',
							margin: values.onlineFocused ? '0px' : '3px 5px 0px 5px'
						}}
						onClick={() => {
							setValue('onlineFocused', true);
							setReRender(Math.random());
							setTimeout(() => document.getElementById('onlineEl').focus(), 300);
						}}
					>
						By Online
					</div>
					<FontAwesomeIcon
						className="selectOpenIcon cursor-pointer"
						style={{
							width: values.onlineFocused ? '0px' : '15px',
							margin: values.onlineFocused ? '0px' : '2px 10px 0px 0px'
						}}
						onClick={() => {
							setValue('onlineFocused', true);
							setReRender(Math.random());
							setTimeout(() => document.getElementById('onlineEl').focus(), 300);
						}}
						icon={faChevronDown}
					/>

					<Controller
						name="is_online"
						control={control}
						render={({ field: { onChange, value } }) => (
							<Autocomplete
								id="onlineEl"
								className="mb-3 selectField"
								style={{
									width: values.onlineFocused ? '130px' : '0px',
									margin: values.onlineFocused ? '0px 10px' : '0px',
									display: values.onlineFocused ? 'block' : 'none'
								}}
								classes={{ endAdornment: 'endAdornment' }}
								openOnFocus={true}
								onClose={() => {
									setValue('onlineFocused', false);
									setReRender(Math.random());
								}}
								freeSolo
								options={onlineStatus}
								value={value ? onlineStatus.find(data => data.id == value) : null}
								getOptionLabel={option => `${option?.name}`}
								onChange={(event, newValue) => {
									onChange(newValue?.id);
									setValue('onlineName', newValue?.name || '');
									inShowAllMode ? handleGetAllOrders() : handleGetOrders();
								}}
								renderInput={params => (
									<TextField {...params} className="textFieldUnderSelect" placeholder="Is Online" />
								)}
							/>
						)}
					/>
				</div>
				{/* By Customer Email */}`{' '}
				<div className="fieldContainer">
					<FontAwesomeIcon className="icon" icon={faBookOpen} />

					<div
						className="selectLabel"
						style={{
							width: values.statusEmailFocused ? '0px' : '82px',
							margin: values.statusEmailFocused ? '0px' : '2px 5px 0px 10px'
						}}
						onClick={() => {
							setValue('statusEmailFocused', true);
							setReRender(Math.random());
							setTimeout(() => document.getElementById('statusEmailEl').focus(), 300);
						}}
					>
						By Email
					</div>
					<FontAwesomeIcon
						className="selectOpenIcon cursor-pointer"
						style={{
							width: values.statusEmailFocused ? '0px' : '15px',
							margin: values.statusEmailFocused ? '0px' : '2px 10px 0px 0px'
						}}
						onClick={() => {
							setValue('statusEmailFocused', true);
							setReRender(Math.random());
							setTimeout(() => document.getElementById('statusEmailEl').focus(), 300);
						}}
						icon={faChevronDown}
					/>

					<Controller
						name="email"
						control={control}
						render={({ field: { onChange, value } }) => (
							<Autocomplete
								id="statusEmailEl"
								className="mb-3 selectField"
								style={{
									width: values.statusEmailFocused ? '130px' : '0px',
									margin: values.statusEmailFocused ? '0px 10px' : '0px',
									display: values.statusEmailFocused ? 'block' : 'none'
								}}
								classes={{ endAdornment: 'endAdornment' }}
								openOnFocus={true}
								onClose={() => {
									setValue('statusEmailFocused', false);
									setReRender(Math.random());
								}}
								freeSolo
								options={customerDetails}
								value={value ? customerDetails.find(data => data?.id == value) : null}
								getOptionLabel={option =>
									option?.email ? option?.email : `${option?.first_name} (No Email)`
								}
								onChange={(event, newValue) => {
									onChange(newValue?.id);
									setValue(
										'statusEmailName',
										newValue?.email ? newValue?.email : `${newValue?.first_name} (No Email)`
									);
									inShowAllMode ? handleGetAllOrders() : handleGetOrders();
								}}
								renderInput={params => (
									<TextField {...params} className="textFieldUnderSelect" placeholder="By Email" />
								)}
							/>
						)}
					/>
				</div>
				{/* By branch */}
				<div className="fieldContainer">
					<FontAwesomeIcon className="icon" icon={faTextHeight} />

					<div
						className="selectLabel"
						style={{
							width: values.branchFocused ? '0px' : '89px',
							margin: values.branchFocused ? '0px' : '3px 5px 0px 5px'
						}}
						onClick={() => {
							setValue('branchFocused', true);
							setReRender(Math.random());
							setTimeout(() => document.getElementById('branchEl').focus(), 300);
						}}
					>
						By Branch
					</div>
					<FontAwesomeIcon
						className="selectOpenIcon cursor-pointer"
						style={{
							width: values.branchFocused ? '0px' : '15px',
							margin: values.branchFocused ? '0px' : '2px 10px 0px 0px'
						}}
						onClick={() => {
							setValue('branchFocused', true);
							setReRender(Math.random());
							setTimeout(() => document.getElementById('branchEl').focus(), 300);
						}}
						icon={faChevronDown}
					/>

					<Controller
						name="name"
						control={control}
						render={({ field: { onChange, value } }) => (
							<Autocomplete
								id="branchEl"
								className="mb-3 selectField"
								style={{
									width: values.branchFocused ? '130px' : '0px',
									margin: values.branchFocused ? '0px 10px' : '0px',
									display: values.branchFocused ? 'block' : 'none'
								}}
								classes={{ endAdornment: 'endAdornment' }}
								openOnFocus={true}
								onClose={() => {
									setValue('branchFocused', false);
									setReRender(Math.random());
								}}
								freeSolo
								options={branches}
								value={value ? branches.find(data => data.id == value) : null}
								getOptionLabel={option => `${option?.name}`}
								onChange={(event, newValue) => {
									onChange(newValue?.id);
									setValue('branchName', newValue?.name || '');
									inShowAllMode ? handleGetAllOrders() : handleGetOrders();
								}}
								renderInput={params => (
									<TextField
										{...params}
										className="textFieldUnderSelect"
										placeholder="Select Branch"
									/>
								)}
							/>
						)}
					/>
				</div>
			</div>

			{/* keywords */}
			<div className="allKeyWrdContainer">
				{values.date_after && (
					<div className="keywordContainer">
						<b>Date From</b>
						<div>
							<FontAwesomeIcon className="iconWithKeyWord" icon={faCalendarAlt} />
							<p>{moment(new Date(values.date_after)).format('DD-MM-YYYY')}</p>
							<FontAwesomeIcon
								className="closeIconWithKeyWord"
								icon={faTimesCircle}
								onClick={() => {
									setValue('date_after', '');
									inShowAllMode ? handleGetAllOrders() : handleGetOrders();
									setReRender(Math.random());
								}}
							/>
						</div>
					</div>
				)}

				{values.date_before && (
					<div className="keywordContainer">
						<b>Date To</b>
						<div>
							<FontAwesomeIcon className="iconWithKeyWord" icon={faCalendarAlt} />
							<p>{moment(new Date(values.date_before)).format('DD-MM-YYYY')}</p>
							<FontAwesomeIcon
								className="closeIconWithKeyWord"
								icon={faTimesCircle}
								onClick={() => {
									setValue('date_before', '');
									inShowAllMode ? handleGetAllOrders() : handleGetOrders();
									setReRender(Math.random());
								}}
							/>
						</div>
					</div>
				)}

				{values.order_no && (
					<div className="keywordContainer">
						<b>Order No</b>
						<div>
							<FontAwesomeIcon className="iconWithKeyWord" icon={faBookOpen} />
							<p>{values.order_no}</p>
							<FontAwesomeIcon
								className="closeIconWithKeyWord"
								icon={faTimesCircle}
								onClick={() => {
									setValue('order_no', '');
									invoiceNoEl.current.value = '';
									inShowAllMode ? handleGetAllOrders() : handleGetOrders();
									setReRender(Math.random());
								}}
							/>
						</div>
					</div>
				)}

				{values.statusName && (
					<div className="keywordContainer">
						<b>Status</b>
						<div>
							<FontAwesomeIcon className="iconWithKeyWord" icon={faBookOpen} />
							<p>{values.statusName}</p>
							<FontAwesomeIcon
								className="closeIconWithKeyWord"
								icon={faTimesCircle}
								onClick={() => {
									setValue('statusName', '');
									setValue('order_status', '');
									inShowAllMode ? handleGetAllOrders() : handleGetOrders();
									setReRender(Math.random());
								}}
							/>
						</div>
					</div>
				)}

				{values.paymentName && (
					<div className="keywordContainer">
						<b>Payment</b>
						<div>
							<FontAwesomeIcon className="iconWithKeyWord" icon={faBookOpen} />
							<p>{values.paymentName}</p>
							<FontAwesomeIcon
								className="closeIconWithKeyWord"
								icon={faTimesCircle}
								onClick={() => {
									setValue('paymentName', '');
									setValue('is_paid', '');
									inShowAllMode ? handleGetAllOrders() : handleGetOrders();
									setReRender(Math.random());
								}}
							/>
						</div>
					</div>
				)}

				{values.statusEmailName && (
					<div className="keywordContainer">
						<b>By Email</b>
						<div>
							<FontAwesomeIcon className="iconWithKeyWord" icon={faBookOpen} />
							<p>{values.statusEmailName}</p>
							<FontAwesomeIcon
								className="closeIconWithKeyWord"
								icon={faTimesCircle}
								onClick={() => {
									setValue('statusEmailName', '');
									setValue('email', '');
									inShowAllMode ? handleGetAllOrders() : handleGetOrders();
									setReRender(Math.random());
								}}
							/>
						</div>
					</div>
				)}

				{values.branchName && (
					<div className="keywordContainer">
						<b>Branch</b>
						<div>
							<FontAwesomeIcon className="iconWithKeyWord" icon={faTextHeight} />
							<p>{values.branchName}</p>
							<FontAwesomeIcon
								className="closeIconWithKeyWord"
								icon={faTimesCircle}
								onClick={() => {
									setValue('branchName', '');
									setValue('name', '');
									inShowAllMode ? handleGetAllOrders() : handleGetOrders();
									setReRender(Math.random());
								}}
							/>
						</div>
					</div>
				)}
			</div>
		</div>
	);
}

export default OrderSummaryFilterMenu;
