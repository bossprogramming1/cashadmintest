import { faBookOpen, faDownload, faFileExcel, faFilePdf, faScroll } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { yupResolver } from '@hookform/resolvers/yup';
import { makeStyles } from '@material-ui/core';
import { GetApp, ViewWeek } from '@material-ui/icons';
import Tooltip from '@mui/material/Tooltip';
import PrintIcon from '@material-ui/icons/Print';
import useReportData2WithRowSize from 'app/@customHooks/useReportData2WithRowSize';
import FuseLoading from '@fuse/core/FuseLoading';
import useUserInfo from 'app/@customHooks/useUserInfo';
import ReceiptIcon from '@material-ui/icons/Receipt';
import getPaginationData from 'app/@helpers/getPaginationData';
import getTotalAmount from 'app/@helpers/getTotalAmount';
import html2PDF from 'jspdf-html2canvas';
import React, { useEffect, useLayoutEffect, useReducer, useRef, useState } from 'react';
import { unstable_batchedUpdates } from 'react-dom';
import { FormProvider, useForm } from 'react-hook-form';
import ReactHtmlTableToExcel from 'react-html-table-to-excel';
import { useDispatch } from 'react-redux';
import { useReactToPrint } from 'react-to-print';
import * as yup from 'yup';
import history from '@history';
import { GET_SITESETTINGS } from '../../../../../../constant/constants';
import ColumnLabel from '../../../reportComponents/ColumnLabel';
import Pagination from '../../../reportComponents/Pagination';
import SinglePage from '../../../reportComponents/SiglePage';
import { getReportMakeStyles } from '../../../reportUtils/reportMakeStyls';
import tableColumnsReducer from '../../../reportUtils/tableColumnsReducer';
import { getAllOrders, getOrders } from '../store/orderReportSlice';
import OrderSummaryFilterMenu from './OrderSummaryFilterMenu';

const useStyles = makeStyles(theme => ({
	...getReportMakeStyles(theme),
	inputBg: {
		background: theme.palette.primary.main,
		color: theme.palette.primary[50]
	}
}));

const schema = yup.object().shape({});

const initialTableColumnsState = [
	{ id: 1, label: 'Sl_No', sortAction: false, isSerialNo: true, show: true },
	{ id: 2, label: 'Date', name: 'order_date', show: true, type: 'date' },
	{ id: 3, label: 'Invoice No', name: 'order_no', show: true },
	{ id: 4, label: 'Order Status', name: 'order_status', show: true },
	{ id: 5, label: 'Payment Status', name: 'is_paid', show: true },

	{ id: 6, label: 'Is Online', name: 'is_online', show: true },

	{ id: 7, label: 'Customer', name: 'email', show: true },

	{ id: 8, label: 'Branch', name: 'branch', show: true },

	{
		id: 9,
		label: 'Pay amount',
		name: 'price',
		show: true,
		style: { justifyContent: 'flex-end', marginRight: '5px' },
		headStyle: { textAlign: 'right' }
	},
	{
		id: 10,
		label: 'Action',
		getterMethod: item => {
			return (
				<ReceiptIcon
					onClick={() => {
						history.push(`/apps/order-managements/order-invoice/${item.id}`);
					}}
					className="h-52 cursor-pointer"
					style={{ color: 'orange' }}
				/>
			);
		},
		show: true
	}
];

const OrderSummaryReportsTable = () => {
	const classes = useStyles();

	const methods = useForm({
		mode: 'onChange',
		defaultValues: {},
		resolver: yupResolver(schema)
	});

	const { getValues } = methods;

	const dispatch = useDispatch();

	const { authTOKEN } = useUserInfo();

	const [generalData, setGeneralData] = useState({});

	const [rowSize, setRowSize] = useState(20);

	const [modifiedOrderData, setModifiedOrderData, setSortBy] = useReportData2WithRowSize({
		type: 'orderSummaryData',
		extraRowCount: 1,
		row: rowSize
	});
	const handleRowChange = event => {
		const newRowSize = parseInt(event.target.value); // Get the new row size from the input field and parse it as an integer
		if (!isNaN(newRowSize) && newRowSize > 0) {
			setRowSize(newRowSize);
		} else if (isNaN(newRowSize)) {
			setRowSize(20);
		} else {
			setRowSize(1);
		}
	};

	const [pagination, setPagination] = useState(false);

	const [loading, setLoading] = useState(false);

	const [tableColumns, dispatchTableColumns] = useReducer(tableColumnsReducer, initialTableColumnsState);

	const [totalAmount, setTotalAmount] = useState(0);

	//tools state
	const [inPrint, setInPrint] = useState(false);
	const [inSiglePageMode, setInSiglePageMode] = useState(false);
	const [inShowAllMode, setInShowAllMode] = useState(false);
	const [inDowloadPdf, setInDowloadPdf] = useState(false);
	const [inDowloadExcel, setInDowloadExcel] = useState(false);
	const [showClmSelectOption, setShowClmSelectOption] = useState(false);

	//pagination state
	const [page, setPage] = useState(1);
	const [size, setSize] = useState(25);
	const [totalPages, setTotalPages] = useState(0);
	const [totalElements, setTotalElements] = useState(0);

	//get general setting data
	useEffect(() => {
		fetch(`${GET_SITESETTINGS}`, authTOKEN)
			.then(response => response.json())
			.then(data => setGeneralData(data.general_settings[0] || {}))
			.catch(() => setGeneralData({}));
	}, []);

	//print dom ref
	const componentRef = useRef();

	//printer action
	const printAction = useReactToPrint({
		content: () => componentRef.current
	});

	//print handler
	const handlePrint = () => {
		setInPrint(true);
		printAction();
		setInPrint(false);
	};

	//pdf downloader action
	const pdfDownloadAction = () => {
		html2PDF(downloadPage, {
			margin: [0, 0, 0, 0],
			filename: 'pdfhtml2.pdf',
			html2canvas: {
				dpi: 300,
				letterRendering: true
			},
			setTestIsImage: false,
			useCORS: true,
			jsPDF: { unit: 'mm', format: 'a4', orientation: 'portrait' }
		});
		setInDowloadPdf(false);
	};

	//pdf download handler
	const handlePdfDownload = () => {
		setInDowloadPdf(true);
		if (!inDowloadPdf) {
			if (!inShowAllMode && totalPages > 1) {
				handleGetAllOrders(null, () => {
					pdfDownloadAction();
					setInDowloadPdf(false);
					handleGetOrders();
				});
			} else {
				pdfDownloadAction();
				setInDowloadPdf(false);
			}
		}
	};

	//exel download page dom
	let downloadPage = document.getElementById('downloadPage');

	//exel download handler
	const handleExelDownload = () => {
		setInDowloadExcel(true);
		if (!inDowloadExcel) {
			if (!inShowAllMode && totalPages > 1) {
				handleGetAllOrders(null, () => {
					document.getElementById('test-table-xls-button').click();
					setInDowloadExcel(false);
					handleGetOrders();
				});
			} else {
				document.getElementById('test-table-xls-button').click();
				setInDowloadExcel(false);
			}
		}
	};

	//column select close handler
	useLayoutEffect(() => {
		window.addEventListener('click', e => {
			if (e.target.id !== 'insideClmSelect') setShowClmSelectOption(false);
		});
	}, []);

	//pagination handler
	const firstPageHandler = event => {
		handleGetOrders(event.page);
	};
	const previousPageHandler = event => {
		handleGetOrders(event.page);
	};
	const nextPageHandler = event => {
		handleGetOrders(event.page);
	};
	const lastPageHandler = event => {
		handleGetOrders(event.page);
	};

	//get orders
	const handleGetOrders = (pagePram, callBack) => {
		setLoading(true); // this
		dispatch(getOrders({ values: getValues(), pageAndSize: { page: pagePram || page, size } })).then(res => {
			setLoading(false); // this
			unstable_batchedUpdates(() => {
				callBack && callBack(res.payload);
				const mainArr = res.payload?.orders || [];
				const modifiedArr = [];
				mainArr.map(orders => {
					modifiedArr.push({
						order_date: orders?.order_date,
						order_no: orders?.order_no,
						order_status: orders.order_status?.name,
						is_paid: orders?.is_paid ? 'Yes' : 'No',
						is_online: orders?.is_online ? 'Yes' : 'No',
						branch: orders?.branch?.name,
						email: orders.user?.email,
						price: orders?.pay_amount,
						id: orders.id
					});
				});
				setModifiedOrderData(modifiedArr || []);
				setTotalAmount(getTotalAmount(modifiedArr, 'price'));
				setPage(res.payload?.page || 1);
				setSize(res.payload?.size || 25);
				setTotalPages(res.payload?.total_pages || 0);
				setTotalElements(res.payload?.total_elements || 0);
				setInSiglePageMode(true);
				setInShowAllMode(false);
				setPagination(true); // this
			});
		});
	};

	//get all order without pagination
	const handleGetAllOrders = (callBack, callBackAfterStateUpdated) => {
		setLoading(true); // this

		dispatch(getAllOrders(getValues())).then(res => {
			setLoading(false); // this

			unstable_batchedUpdates(() => {
				callBack && callBack(res.payload);
				const mainArr = res.payload?.orders || [];
				const modifiedArr = [];
				mainArr.map(orders => {
					modifiedArr.push({
						order_date: orders?.order_date,
						order_no: orders?.order_no,
						order_status: orders.order_status?.name,
						is_paid: orders?.is_paid ? 'Yes' : 'No',
						is_online: orders?.is_online ? 'Yes' : 'No',
						branch: orders?.branch?.name,
						email: orders.user?.email,
						price: orders?.pay_amount,
						id: orders.id
					});
				});
				setModifiedOrderData(modifiedArr || []);
				setTotalAmount(getTotalAmount(modifiedArr, 'price'));
				setInSiglePageMode(false);
				setInShowAllMode(true);
				//get pagination data
				const { totalPages, totalElements } = getPaginationData(res.payload?.orders, size, page);
				setPage(page || 1);
				setSize(size || 25);
				setTotalPages(totalPages);
				setTotalElements(totalElements);
				setPagination(false); // this
			});
			callBackAfterStateUpdated && callBackAfterStateUpdated(res.payload);
		});
	};

	return (
		<>
			<div className={classes.headContainer}>
				{/* filter */}
				<FormProvider {...methods}>
					<OrderSummaryFilterMenu
						inShowAllMode={inShowAllMode}
						handleGetOrders={handleGetOrders}
						handleGetAllOrders={handleGetAllOrders}
					/>
				</FormProvider>
			</div>
			<div className={`${classes.menubar} justify-start md:justify-center`}>
				{/* pagination */}
				<div style={{ visibility: pagination ? 'visible' : 'hidden' }}>
					<Pagination
						page={page}
						size={size}
						totalPages={totalPages || 0}
						totalElements={totalElements || 0}
						onClickFirstPage={firstPageHandler}
						onClickPreviousPage={previousPageHandler}
						onClickNextPage={nextPageHandler}
						onClickLastPage={lastPageHandler}
					/>
				</div>

				<div className="downloadIcon">
					{/* download options*/}
					<div className="downloadOptionContainer">
						<div className="indicator"></div>
						<div className="downloadOptions shadow-4">
							{/* download as Pdf */}
							{/*<div
								className="cursor-pointer downloadContainer shadow-4"
								style={{ width: '150px', margin: '10px' }}
								onClick={() => handlePdfDownload()}
							>
								<FontAwesomeIcon icon={faFilePdf} />
								<b>Download PDF</b>
								<FontAwesomeIcon icon={faDownload} />
							</div>*/}

							{/* download as Excel */}
							<div
								className="cursor-pointer downloadContainer shadow-4"
								style={{ width: '160px', margin: '0px 10px 10px 10px' }}
								onClick={() => handleExelDownload()}
							>
								<FontAwesomeIcon icon={faFileExcel} />
								<b>Download Excel</b>
								<FontAwesomeIcon icon={faDownload} />
							</div>
						</div>
					</div>
					{/* download icon*/}
					<GetApp
						className="cursor-pointer inside icon"
						style={{ margin: '0px', border: (inDowloadPdf || inDowloadExcel) && '1px solid' }}
					/>
				</div>

				{/* print icon
				<Tooltip title={<p style={{ fontSize: '130%' }}>Print</p>} placement="top">
					<PrintIcon
						className="cursor-pointer inside icon"
						style={{ padding: '6px', border: inPrint && '1px solid' }}
						onClick={() => handlePrint()}
					/>
				</Tooltip> */}
				{/* show single page icon*/}
				<Tooltip title={<p style={{ fontSize: '130%' }}>Order</p>} placement="top">
					<span>
						<FontAwesomeIcon
							className="cursor-pointer inside icon"
							style={{ padding: '8px', border: inSiglePageMode && '1px solid' }}
							onClick={() => handleGetOrders()}
							icon={faBookOpen}
						/>
					</span>
				</Tooltip>

				{/* show all page icon*/}
				<Tooltip title={<p style={{ fontSize: '130%' }}>Orders</p>} placement="top">
					<span>
						<FontAwesomeIcon
							className="cursor-pointer inside icon"
							style={{ padding: '8px', border: inShowAllMode && '1px solid' }}
							onClick={() => handleGetAllOrders()}
							icon={faScroll}
						/>
					</span>
				</Tooltip>

				{/* column select option icon*/}
				<div className="columnSelectContainer">
					<ViewWeek
						id="insideClmSelect"
						className="cursor-pointer inside icon"
						style={{ margin: '0px', padding: '6px', border: showClmSelectOption && '1px solid' }}
						onClick={() => setTimeout(() => setShowClmSelectOption(true), 0)}
					/>

					{/* columns */}
					<div
						id="insideClmSelect"
						className={`allColumnContainer shadow-5 ${showClmSelectOption ? 'block' : 'hidden'}`}
					>
						{tableColumns.map(column => (
							<ColumnLabel key={column.id} column={column} dispatchTableColumns={dispatchTableColumns} />
						))}
					</div>
				</div>
				{/* print icon*/}
				<PrintIcon
					className="cursor-pointer inside icon"
					style={{ padding: '6px', border: inPrint && '1px solid' }}
					onClick={() => handlePrint()}
				/>
				<div style={{ display: 'flex' }}>
					<p htmlFor="rowSize">Row Size:</p>
					<input
						type="text"
						id="rowSize"
						style={{
							borderRadius: '10px',
							marginLeft: '10px',
							padding: '2px',
							width: '20%',
							textAlign: 'center'
						}}
						className={`rounded-input ${classes.inputBg}`}
						value={modifiedOrderData.row}
						onChange={handleRowChange}
					/>
				</div>
			</div>

			{/* excel converter */}
			<div className="hidden">
				<ReactHtmlTableToExcel
					id="test-table-xls-button"
					className="download-table-xls-button"
					table="table-to-xls"
					filename="tablexls"
					sheet="tablexls"
					buttonText="Download as XLS"
				/>
			</div>

			{loading ? (
				<FuseLoading />
			) : (
				<table id="table-to-xls" className="w-full" style={{ minHeight: '270px' }}>
					<div ref={componentRef} id="downloadPage">
						{/* each single page (table) */}
						{modifiedOrderData.map((order, idx) => {
							return (
								<SinglePage
									classes={classes}
									generalData={generalData}
									reporTitle="Order Summary Report"
									tableColumns={tableColumns}
									dispatchTableColumns={dispatchTableColumns}
									data={
										order.isLastPage
											? {
													...order,
													data: order.data.concat({
														price: totalAmount,
														branch: 'Grand Total',
														hideSerialNo: true,
														getterMethod: () => '',
														rowStyle: { fontWeight: 600 }
													})
											  }
											: order
									}
									serialNumber={
										pagination ? page * size - size + 1 : order.page * order.size - order.size + 1
									}
									setPage={setPage}
									page={page}
									inSiglePageMode={inSiglePageMode}
									setSortBy={setSortBy}
								/>
							);
						})}
					</div>
				</table>
			)}
		</>
	);
};
export default OrderSummaryReportsTable;
