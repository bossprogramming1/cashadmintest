import { Box, Button, Divider, Typography } from '@material-ui/core';
import { makeStyles, useTheme } from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import PrintIcon from '@material-ui/icons/Print';
import { format } from 'date-fns';
import React, { useEffect, useRef, useState } from 'react';
import { useSelector } from 'react-redux';
import { useHistory, useParams, withRouter } from 'react-router-dom';
import { useReactToPrint } from 'react-to-print';
import ArrowBackIcon from '@mui/icons-material/ArrowBack';
import Barcode from 'react-barcode';
import { BASE_URL, GET_PURCHASE_FINAL_BY_INVOICE, GET_SITESETTINGS } from '../../../../../../constant/constants';

const useStyles = makeStyles(theme => ({
	hidden: {
		display: 'none'
	},
	tblContainer: {
		borderRadius: '0px',
		width: '700px',
		marginLeft: '150px',
		marginBottom: '50px'
	},
	table: {
		minWidth: 700
	},
	tableHead: {
		backgroundColor: theme.palette.primary[500]
	},
	dateOfSaleContainer: {
		display: 'flex',
		flexWrap: 'nowrap',
		justifyContent: 'space-between',
		minWidth: '250px'
	},
	paper: {
		marginTop: '10%',
		backgroundColor: theme.palette.background.paper,
		//border: `2px solid ${theme.palette.grey[800]}`,
		boxShadow: theme.shadows[5],
		padding: theme.spacing(2, 4, 3)
	},
	underline: {
		borderBottom: '1px solid grey',
		marginTop: '4%',
		marginBottom: '1%'
	},
	textMarginBottom: {
		marginBottom: '2%'
	}
}));

const PurchaseInvoice = props => {
	const [purchaseRequestInvoice, setPurchaseRequestInvoice] = useState({});
	const routeParams = useParams();
	const { purchaseId } = routeParams;
	const classes = useStyles(props);
	const Theme = useTheme();
	const PaperColor = Theme.palette.background.paper;
	const [showPrintBtn, setShowPrintBtn] = useState(true);
	const [generalData, setGeneralData] = useState({});
	const history = useHistory();
	let serialNumber = 1;
	const authTOKEN = {
		headers: {
			'Content-type': 'application/json',
			Authorization: localStorage.getItem('jwt_access_token')
		}
	};

	useEffect(() => {
		fetch(`${GET_PURCHASE_FINAL_BY_INVOICE}${purchaseId}`, authTOKEN)
			.then(response => response.json())
			.then(data => setPurchaseRequestInvoice(data));
	}, []);

	//get general setting data
	useEffect(() => {
		const authTOKEN = {
			headers: {
				'Content-type': 'application/json',
				Authorization: localStorage.getItem('jwt_access_token')
			}
		};

		fetch(`${GET_SITESETTINGS}`, authTOKEN)
			.then(response => response.json())
			.then(data => setGeneralData(data.general_settings[0] || {}))
			.catch(() => setGeneralData({}));
	}, []);
	//print
	const componentRef = useRef();
	const handlePrint = useReactToPrint({
		content: () => componentRef.current
	});

	const barcodeConfig2 = {
		width: 1,
		height: 40,
		margin: 0,
		format: 'CODE128',
		displayValue: false
	};

	return (
		<>
			<Box
				style={{
					display: 'flex',
					justifyContent: 'space-between',
					flexDirection: 'row',
					marginRight: '150px',
					marginLeft: '150px'
				}}
			>
				<Button onClick={() => history.goBack()} style={{ fontSize: '20px' }}>
					<ArrowBackIcon
						className="h-52 cursor-pointer"
						style={{
							color: 'black',
							textAlign: 'left',

							height: '30px',
							width: '30px'
						}}
					/>
					Go Back
				</Button>
				<PrintIcon
					onClick={handlePrint}
					className="h-52 cursor-pointer"
					style={{
						color: '#6495ED',
						textAlign: 'right',
						height: '30px',
						width: '30px'
					}}
				/>
			</Box>
			<div
				ref={componentRef}
				style={{
					width: '100%',
					// marginLeft: 'auto',
					// marginRight: 'auto',
					padding: '6vh',
					fontSize: '10px'
				}}
			>
				<div
					style={{
						display: 'flex',
						justifyContent: 'space-between',
						bgcolor: 'background.paper'
					}}
				>
					<Box style={{ textAlign: 'left' }}>
						<Box style={{ textAlign: 'right' }}>
							<img
								style={{ height: '30px', width: '100px' }}
								src={`${BASE_URL}${generalData?.logo}`}
								alt="Not found"
							/>
						</Box>
						<Box>
							<Typography style={{ fontSize: '10px' }} variant="h6" component="div">
								{purchaseRequestInvoice?.bill_from?.site_name}
							</Typography>
							<Typography
								style={{ fontSize: '10px', overflowWrap: ' break-word' }}
								variant="P"
								component="div"
							>
								{purchaseRequestInvoice?.bill_from?.address}
							</Typography>
							<Typography style={{ fontSize: '10px' }} variant="P" component="div">
								Email: {purchaseRequestInvoice?.bill_from?.email}
							</Typography>
							<Typography style={{ fontSize: '10px' }} variant="P" component="div">
								Phone: {purchaseRequestInvoice?.bill_from?.phone}
							</Typography>
							<Typography style={{ fontSize: '10px' }} variant="P" component="div">
								Website: {purchaseRequestInvoice?.bill_from?.site_address}
							</Typography>
						</Box>
					</Box>
					<Box>
						<Typography
							style={{ textAlign: 'right', marginTop: '-16px', marginRight: '5px', fontSize: '12px' }}
							variant="h5"
							component="div"
						>
							INVOICE
						</Typography>
						<Barcode value={purchaseRequestInvoice?.purchase_final?.purchase_no} {...barcodeConfig2} />

						<div
							style={{
								display: 'flex',
								justifyContent: 'space-between',
								alignItems: 'center'
							}}
						>
							<Box style={{ textAlign: 'left' }}>
								<Typography style={{ fontSize: '10px' }} variant="P" component="div">
									INVOICE
								</Typography>
								<Typography style={{ fontSize: '10px' }} variant="P" component="div">
									INVOICE DATE
								</Typography>
								<Typography style={{ fontSize: '10px' }} variant="P" component="div">
									PAYMENT TYPE
								</Typography>
								{/* <Typography
									variant="P"
									component="div"
									style={{
										backgroundColor: '#D7DBDD',
										padding: '5px',
										fontWeight: '600',
										fontSize: '10px'
									}}
								>
									AMOUNT DUE
								</Typography> */}
							</Box>
							<Box style={{ textAlign: 'right' }}>
								<Typography style={{ fontSize: '10px' }} variant="P" component="div">
									{purchaseRequestInvoice?.purchase_final?.purchase_no || 'No Order Found'}
								</Typography>
								<Typography style={{ fontSize: '10px' }} variant="P" component="div">
									{/* {format(new Date(purchaseRequestInvoice?.purchase_request?.request_date), "MMM dd, yyyy")} */}
									{/* {purchaseRequestInvoice?.purchase_request?.request_date} */}
									{/* {purchaseRequestInvoice?.purchase_final?.order_date?.slice(0, 10)} */}
									{(purchaseRequestInvoice?.purchase_final?.purchase_date &&
										format(
											new Date(purchaseRequestInvoice?.purchase_final?.purchase_date),
											'dd-MM-yyyy'
										)) ||
										'No Date Found'}
								</Typography>
								<Typography style={{ fontSize: '10px' }} variant="P" component="div">
									{purchaseRequestInvoice?.purchase_final?.payment_method?.name.toUpperCase() ||
										'Un-paid'}
								</Typography>
								{/* <Typography
									variant="P"
									component="div"
									style={{
										backgroundColor: '#D7DBDD',
										padding: '5px',
										fontWeight: '600',
										fontSize: '10px'
									}}
								>
									{purchaseRequestInvoice?.purchase_final?.net_amount - purchaseRequestInvoice?.purchase_final?.pay_amount || 0.0}
								</Typography> */}
							</Box>
						</div>
					</Box>
				</div>
				<Divider style={{ marginTop: '10px', marginBottom: '10px' }} />

				<Box>
					<Typography style={{ fontSize: '10px' }} variant="h6" component="div">
						Purchase From
					</Typography>
					<Typography style={{ fontSize: '10px' }} variant="P" component="div">
						Name: {purchaseRequestInvoice?.purchase_final?.vendor?.first_name || ''}{' '}
						{purchaseRequestInvoice?.purchase_final?.vendor?.last_name || ''}
					</Typography>
					<Typography style={{ fontSize: '10px', overflowWrap: ' break-word' }} variant="P" component="div">
						Address:{' '}
						{purchaseRequestInvoice?.purchase_final?.is_online == true
							? purchaseRequestInvoice?.purchase_final?.street_address
							: purchaseRequestInvoice?.purchase_final?.vendor?.street_address_one}
						,{' '}
						{purchaseRequestInvoice?.purchase_final?.is_online == false
							? purchaseRequestInvoice?.purchase_final?.vendor?.thana?.name
							: ''}
						,{' '}
						{purchaseRequestInvoice?.purchase_final?.is_online == false
							? purchaseRequestInvoice?.purchase_final?.vendor?.city?.name
							: ''}
					</Typography>
					<Typography style={{ fontSize: '10px' }} variant="P" component="div">
						Phone: {purchaseRequestInvoice?.purchase_final?.vendor?.primary_phone || 'N/A'}
					</Typography>
					<Typography style={{ fontSize: '10px' }} variant="P" component="div">
						Email: {purchaseRequestInvoice?.purchase_final?.vendor?.email}
					</Typography>
				</Box>
				{/* <div
					style={{
						display: 'flex',
						marginTop: '10px',
						marginBottom: '10px',
						bgcolor: 'background.paper'
					}}
				>
					<Box>
						<Typography style={{ fontSize: '10px' }} variant="h6" component="div">
							Shipping Address
						</Typography>
						<Typography style={{ fontSize: '10px' }} variant="P" component="div">
							Name: {purchaseRequestInvoice?.shipping?.name || 'No Name Found'}
						</Typography>
						<Typography
							style={{ fontSize: '10px', overflowWrap: ' break-word' }}
							variant="P"
							component="div"
						>
							Address: {purchaseRequestInvoice?.shipping?.street_address},{' '}
							{purchaseRequestInvoice?.shipping?.thana?.name},{' '}
							{purchaseRequestInvoice?.shipping?.city?.name}
						</Typography>
						<Typography style={{ fontSize: '10px' }} variant="P" component="div">
							Phone: {purchaseRequestInvoice?.shipping?.phone}
						</Typography>
						<Typography style={{ fontSize: '10px' }} variant="P" component="div">
							Email: {purchaseRequestInvoice?.shipping?.email}
						</Typography>
					</Box>
					<Box style={{ marginLeft: '150px' }}>
						<Typography style={{ fontSize: '10px' }} variant="h6" component="div">
							Billing Address
						</Typography>
						<Typography style={{ fontSize: '10px' }} variant="P" component="div">
							Name: {purchaseRequestInvoice?.billing?.name || 'No Name Found'}
						</Typography>
						<Typography
							style={{ fontSize: '10px', overflowWrap: ' break-word' }}
							variant="P"
							component="div"
						>
							Address: {purchaseRequestInvoice?.billing?.street_address},{' '}
							{purchaseRequestInvoice?.billing?.thana?.name},{' '}
							{purchaseRequestInvoice?.billing?.city?.name}
						</Typography>
						<Typography style={{ fontSize: '10px' }} variant="P" component="div">
							Phone: {purchaseRequestInvoice?.billing?.phone}
						</Typography>
						<Typography style={{ fontSize: '10px' }} variant="P" component="div">
							Email: {purchaseRequestInvoice?.billing?.email}
						</Typography>
					</Box>
				</div> */}

				<Table
					aria-label="simple table"
					style={{
						width: '100%',
						marginLeft: 'auto',
						marginRight: 'auto',
						marginBottom: '50px',
						marginTop: '20px'
					}}
				>
					<TableHead style={{ backgroundColor: '#D7DBDD' }}>
						<TableRow>
							<TableCell
								style={{
									fontSize: '10px',
									lineHeight: '0.228571rem;',
									padding: 0,
									borderRight: '1px solid white'
								}}
								align="center"
							>
								Sl_No
							</TableCell>
							<TableCell
								style={{
									fontSize: '10px',
									lineHeight: '0.228571rem;',
									padding: 0,
									borderRight: '1px solid white'
								}}
								align="center"
							>
								Item
							</TableCell>

							<TableCell
								style={{
									fontSize: '10px',
									lineHeight: '0.228571rem;',
									padding: 0,
									borderRight: '1px solid white'
								}}
								align="center"
							>
								Description
							</TableCell>

							<TableCell
								style={{
									fontSize: '10px',
									lineHeight: '0.228571rem;',
									padding: 0,
									borderRight: '1px solid white'
								}}
								align="center"
							>
								Quantity
							</TableCell>
							<TableCell
								style={{
									fontSize: '10px',
									lineHeight: '0.228571rem;',
									padding: 0,
									borderRight: '1px solid white'
								}}
								align="center"
							>
								Unit Cost
							</TableCell>
							<TableCell
								style={{
									fontSize: '10px',
									lineHeight: '0.228571rem;',
									padding: 0,
									borderRight: '1px solid white'
								}}
								align="center"
							>
								Line Total
							</TableCell>
						</TableRow>
					</TableHead>
					<TableBody>
						{purchaseRequestInvoice?.items?.map((item, idx) => (
							<TableRow id={idx}>
								<TableCell style={{ fontSize: '10px', width: '35px' }} align="center">
									{serialNumber++}
								</TableCell>
								<TableCell style={{ fontSize: '10px', width: '70px' }} align="center">
									<img
										style={{ height: '40px', width: '40px' }}
										src={`${BASE_URL}${item?.product?.thumbnail}`}
										alt={item?.product?.name}
									/>
								</TableCell>
								<TableCell style={{ fontSize: '10px' }} align="center">
									{item?.product?.name}
								</TableCell>

								<TableCell style={{ fontSize: '10px' }} align="center">
									{item?.quantity}
								</TableCell>
								<TableCell style={{ fontSize: '10px' }} align="center">
									{item?.unit_price}
								</TableCell>
								<TableCell style={{ fontSize: '10px' }} align="center">
									{item?.subtotal}
								</TableCell>
							</TableRow>
						))}
					</TableBody>
				</Table>
				<div
					style={{
						display: 'flex',
						justifyContent: 'space-between',
						marginTop: '50px',
						marginBottom: '50px',
						// marginBottom: '50px',
						bgcolor: 'background.paper'
					}}
				>
					<Box>
						<Typography variant="h6" component="div" style={{ fontWeight: '700', fontSize: '10px' }}>
							NOTES/MEMO
						</Typography>
						<Typography variant="P" component="div" style={{ fontSize: '10px' }}>
							Free Shipping with 30-day money-back guarantee.
						</Typography>
					</Box>
					<Box
						style={{
							display: 'flex',
							justifyContent: 'space-between',
							alignItems: 'center'
						}}
					>
						<Box style={{ textAlign: 'left', fontSize: '10px' }}>
							<Typography variant="P" component="div">
								SUBTOTAL
							</Typography>
							<Typography style={{ fontSize: '10px' }} variant="P" component="div">
								TAX
							</Typography>
							<Typography style={{ fontSize: '10px' }} variant="P" component="div">
								SHIPPING FEE
							</Typography>
							<Typography
								variant="P"
								component="div"
								style={{
									backgroundColor: '#D7DBDD',
									padding: '5px',
									fontWeight: '600',
									fontSize: '10px'
								}}
							>
								TOTAL
							</Typography>
						</Box>
						<Box style={{ textAlign: 'right', fontSize: '10px' }}>
							<Typography variant="P" component="div">
								{purchaseRequestInvoice?.purchase_final?.net_amount}
							</Typography>
							<Typography style={{ fontSize: '10px' }} variant="P" component="div">
								{purchaseRequestInvoice?.purchase_final?.tax_price}
							</Typography>
							<Typography style={{ fontSize: '10px' }} variant="P" component="div">
								{purchaseRequestInvoice?.purchase_final?.tax_price}
							</Typography>
							<Typography
								variant="P"
								component="div"
								style={{
									backgroundColor: '#D7DBDD',
									padding: '5px',
									fontWeight: '600',
									fontSize: '10px'
								}}
							>
								{purchaseRequestInvoice?.purchase_final?.net_amount}
							</Typography>
						</Box>
					</Box>
				</div>
			</div>
		</>
	);
};
export default withRouter(PurchaseInvoice);
