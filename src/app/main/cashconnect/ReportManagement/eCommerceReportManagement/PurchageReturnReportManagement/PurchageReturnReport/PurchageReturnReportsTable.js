import { faBookOpen, faDownload, faFileExcel, faFilePdf, faScroll } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { yupResolver } from '@hookform/resolvers/yup';
import { makeStyles } from '@material-ui/core';
import { GetApp, ViewWeek } from '@material-ui/icons';
import Tooltip from '@mui/material/Tooltip';
import Button from '@mui/material/Button';

import PrintIcon from '@material-ui/icons/Print';
import useReportData2WithRowSize from 'app/@customHooks/useReportData2WithRowSize';
import FuseLoading from '@fuse/core/FuseLoading';
import useUserInfo from 'app/@customHooks/useUserInfo';
import getPaginationData from 'app/@helpers/getPaginationData';
import getTotalAmount from 'app/@helpers/getTotalAmount';
import html2PDF from 'jspdf-html2canvas';
import React, { useEffect, useLayoutEffect, useReducer, useRef, useState } from 'react';
import { unstable_batchedUpdates } from 'react-dom';
import { FormProvider, useForm } from 'react-hook-form';
import ReactHtmlTableToExcel from 'react-html-table-to-excel';
import { useDispatch } from 'react-redux';
import { useReactToPrint } from 'react-to-print';
import * as yup from 'yup';
import ReceiptIcon from '@material-ui/icons/Receipt';
import history from '@history';
import { GET_SITESETTINGS } from '../../../../../../constant/constants';
import ColumnLabel from '../../../reportComponents/ColumnLabel';
import Pagination from '../../../reportComponents/Pagination';
import SinglePage from '../../../reportComponents/SiglePage';
import { getReportMakeStyles } from '../../../reportUtils/reportMakeStyls';
import tableColumnsReducer from '../../../reportUtils/tableColumnsReducer';
import { getAllPurchagesReturn, getPurchagesReturn } from '../store/purchageReturnReportSlice';
import PurhcageReturnFilterMenu from './PurchageReturnFilterMenu';

const useStyles = makeStyles(theme => ({
	...getReportMakeStyles(theme),
	inputBg: {
		background: theme.palette.primary.main,
		color: theme.palette.primary[50]
	}
}));

const schema = yup.object().shape({});

const initialTableColumnsState = [
	{ id: 1, label: 'Sl_No', sortAction: false, isSerialNo: true, show: true },
	{ id: 2, label: 'Product Name', name: 'product', show: true },
	{ id: 3, label: 'Color & Size', name: 'color', show: true },
	{ id: 4, label: 'Quantity', name: 'quantity', show: true },
	{ id: 5, label: 'Unit Price', name: 'unit_price', show: true },
	{ id: 6, label: 'Sub Total', name: 'subtotal', show: true }
];

const PurchageReturnReportsTable = () => {
	const classes = useStyles();

	const methods = useForm({
		mode: 'onChange',
		defaultValues: {},
		resolver: yupResolver(schema)
	});

	const { getValues } = methods;

	const dispatch = useDispatch();

	const { authTOKEN } = useUserInfo();

	const [generalData, setGeneralData] = useState({});
	const [rowSize, setRowSize] = useState(20);

	const [modifiedPurchageReturnData, setModifiedPurchageReturnData, setSortBy] = useReportData2WithRowSize({
		type: 'purchageReturnData',
		extraRowCount: 1,
		row: rowSize
	});
	const handleRowChange = event => {
		const newRowSize = parseInt(event.target.value); // Get the new row size from the input field and parse it as an integer
		if (!isNaN(newRowSize) && newRowSize > 0) {
			setRowSize(newRowSize);
		} else if (isNaN(newRowSize)) {
			setRowSize(20);
		} else {
			setRowSize(1);
		}
	};

	const [pagination, setPagination] = useState(false);

	const [loading, setLoading] = useState(false);

	const [tableColumns, dispatchTableColumns] = useReducer(tableColumnsReducer, initialTableColumnsState);

	const [totalAmount, setTotalAmount] = useState(0);
	const [totalQuantity, setTotalQuantity] = useState(0);
	const [totalSubtotal, setTotalSubtotal] = useState(0);

	//tools state
	const [inPrint, setInPrint] = useState(false);
	const [inSiglePageMode, setInSiglePageMode] = useState(false);
	const [inShowAllMode, setInShowAllMode] = useState(false);
	const [inDowloadPdf, setInDowloadPdf] = useState(false);
	const [inDowloadExcel, setInDowloadExcel] = useState(false);
	const [showClmSelectOption, setShowClmSelectOption] = useState(false);

	//pagination state
	const [page, setPage] = useState(1);
	const [size, setSize] = useState(25);
	const [totalPages, setTotalPages] = useState(0);
	const [totalElements, setTotalElements] = useState(0);

	//get general setting data
	useEffect(() => {
		fetch(`${GET_SITESETTINGS}`, authTOKEN)
			.then(response => response.json())
			.then(data => setGeneralData(data.general_settings[0] || {}))
			.catch(() => setGeneralData({}));
	}, []);

	//print dom ref
	const componentRef = useRef();

	//printer action
	const printAction = useReactToPrint({
		content: () => componentRef.current
	});

	//print handler
	const handlePrint = () => {
		setInPrint(true);
		printAction();
		setInPrint(false);
	};

	//pdf downloader action
	const pdfDownloadAction = () => {
		html2PDF(downloadPage, {
			margin: [0, 0, 0, 0],
			filename: 'pdfhtml2.pdf',
			html2canvas: {
				dpi: 300,
				letterRendering: true
			},
			setTestIsImage: false,
			useCORS: true,
			jsPDF: { unit: 'mm', format: 'a4', orientation: 'portrait' }
		});
		setInDowloadPdf(false);
	};

	//pdf download handler
	const handlePdfDownload = () => {
		setInDowloadPdf(true);
		if (!inDowloadPdf) {
			if (!inShowAllMode && totalPages > 1) {
				handleGetAllPurchagesReturn(null, () => {
					pdfDownloadAction();
					setInDowloadPdf(false);
					handleGetPurchagesReturn();
				});
			} else {
				pdfDownloadAction();
				setInDowloadPdf(false);
			}
		}
	};

	//exel download page dom
	let downloadPage = document.getElementById('downloadPage');

	//exel download handler
	const handleExelDownload = () => {
		setInDowloadExcel(true);
		if (!inDowloadExcel) {
			if (!inShowAllMode && totalPages > 1) {
				handleGetAllPurchagesReturn(null, () => {
					document.getElementById('test-table-xls-button').click();
					setInDowloadExcel(false);
					handleGetPurchagesReturn();
				});
			} else {
				document.getElementById('test-table-xls-button').click();
				setInDowloadExcel(false);
			}
		}
	};

	//column select close handler
	useLayoutEffect(() => {
		window.addEventListener('click', e => {
			if (e.target.id !== 'insideClmSelect') setShowClmSelectOption(false);
		});
	}, []);

	//pagination handler
	const firstPageHandler = event => {
		handleGetPurchagesReturn(event.page);
	};
	const previousPageHandler = event => {
		handleGetPurchagesReturn(event.page);
	};
	const nextPageHandler = event => {
		handleGetPurchagesReturn(event.page);
	};
	const lastPageHandler = event => {
		handleGetPurchagesReturn(event.page);
	};

	//get purchases
	const handleGetPurchagesReturn = (pagePram, callBack) => {
		setLoading(true); // this
		dispatch(getPurchagesReturn({ values: getValues(), pageAndSize: { page: pagePram || page, size } })).then(
			res => {
				setLoading(false); // this
				unstable_batchedUpdates(() => {
					callBack && callBack(res.payload);
					const mainArr = res.payload?.purchase_return_items || [];
					const modifiedArr = [];
					mainArr.map(purchase => {
						modifiedArr.push({
							...purchase,
							color: `${purchase?.color?.name} ${
								purchase?.size?.name ? `[${purchase?.size?.name}]` : ''
							}`,
							product: purchase.product.name,
							quantity: purchase.quantity,
							unit_price: purchase.unit_price,
							subtotal: purchase.subtotal
						});
					});

					setModifiedPurchageReturnData(modifiedArr || []);
					setTotalAmount(getTotalAmount(modifiedArr, 'unit_price'));
					setTotalSubtotal(getTotalAmount(modifiedArr, 'subtotal'));
					setTotalQuantity(getTotalAmount(modifiedArr, 'quantity'));
					console.log('setTotalAmount', totalAmount);
					setPage(res.payload?.page || 1);
					setSize(res.payload?.size || 25);
					setTotalPages(res.payload?.total_pages || 0);
					setTotalElements(res.payload?.total_elements || 0);
					setInSiglePageMode(true);
					setInShowAllMode(false);
					setPagination(true); // this
				});
			}
		);
	};

	//get all purchase without pagination
	const handleGetAllPurchagesReturn = (callBack, callBackAfterStateUpdated) => {
		setLoading(true); // this

		dispatch(getAllPurchagesReturn(getValues())).then(res => {
			setLoading(false); // this
			unstable_batchedUpdates(() => {
				callBack && callBack(res.payload);
				const mainArr = res.payload?.purchase_return_items || [];
				const modifiedArr = [];
				mainArr.map(purchase => {
					modifiedArr.push({
						...purchase,
						color: `${purchase?.color?.name} ${purchase?.size?.name ? `[${purchase?.size?.name}]` : ''}`,
						product: purchase.product.name,
						quantity: purchase.quantity,
						unit_price: purchase.unit_price,
						subtotal: purchase.subtotal
					});
				});

				setModifiedPurchageReturnData(modifiedArr || []);
				setTotalAmount(getTotalAmount(modifiedArr, 'unit_price'));
				setTotalSubtotal(getTotalAmount(modifiedArr, 'subtotal'));
				setTotalQuantity(getTotalAmount(modifiedArr, 'quantity'));
				setPage(res.payload?.page || 1);
				setSize(res.payload?.size || 25);
				setTotalPages(res.payload?.total_pages || 0);
				setTotalElements(res.payload?.total_elements || 0);
				setInSiglePageMode(true);
				setInShowAllMode(false);
				setPagination(false); // this
			});
			callBackAfterStateUpdated && callBackAfterStateUpdated(res.payload);
		});
	};

	return (
		<>
			<div className={classes.headContainer}>
				{/* filter */}
				<FormProvider {...methods}>
					<PurhcageReturnFilterMenu
						inShowAllMode={inShowAllMode}
						handleGetPurchagesReturn={handleGetPurchagesReturn}
						handleGetAllPurchagesReturn={handleGetAllPurchagesReturn}
					/>
				</FormProvider>
			</div>
			<div className={`${classes.menubar} justify-start md:justify-center`}>
				{/* pagination */}
				<div style={{ visibility: pagination ? 'visible' : 'hidden' }}>
					<Pagination
						page={page}
						size={size}
						totalPages={totalPages || 0}
						totalElements={totalElements || 0}
						onClickFirstPage={firstPageHandler}
						onClickPreviousPage={previousPageHandler}
						onClickNextPage={nextPageHandler}
						onClickLastPage={lastPageHandler}
					/>
				</div>

				<div className="downloadIcon">
					{/* download options*/}
					<div className="downloadOptionContainer">
						<div className="indicator"></div>
						<div className="downloadOptions shadow-4">
							{/* download as Pdf */}
							{/*<div
								className="cursor-pointer downloadContainer shadow-4"
								style={{ width: '150px', margin: '10px' }}
								onClick={() => handlePdfDownload()}
							>
								<FontAwesomeIcon icon={faFilePdf} />
								<b>Download PDF</b>
								<FontAwesomeIcon icon={faDownload} />
							</div>*/}

							{/* download as Excel */}
							<div
								className="cursor-pointer downloadContainer shadow-4"
								style={{ width: '160px', margin: '0px 10px 10px 10px' }}
								onClick={() => handleExelDownload()}
							>
								<FontAwesomeIcon icon={faFileExcel} />
								<b>Download Excel</b>
								<FontAwesomeIcon icon={faDownload} />
							</div>
						</div>
					</div>
					{/* download icon*/}
					<GetApp
						className="cursor-pointer inside icon"
						style={{ margin: '0px', border: (inDowloadPdf || inDowloadExcel) && '1px solid' }}
					/>
				</div>

				{/* show single page icon*/}
				<Tooltip title={<p style={{ fontSize: '130%' }}>Purchage</p>} placement="top">
					<span>
						<FontAwesomeIcon
							className="cursor-pointer inside icon"
							style={{ padding: '8px', border: inSiglePageMode && '1px solid' }}
							onClick={() => handleGetPurchagesReturn()}
							icon={faBookOpen}
						/>
					</span>
				</Tooltip>

				{/* show all page icon*/}
				<Tooltip title={<p style={{ fontSize: '130%' }}>Purchages</p>} placement="top">
					<span>
						<FontAwesomeIcon
							className="cursor-pointer inside icon"
							style={{ padding: '8px', border: inShowAllMode && '1px solid' }}
							onClick={() => handleGetAllPurchagesReturn()}
							icon={faScroll}
						/>
					</span>
				</Tooltip>

				{/* column select option icon*/}
				<div className="columnSelectContainer">
					<ViewWeek
						id="insideClmSelect"
						className="cursor-pointer inside icon"
						style={{ margin: '0px', padding: '6px', border: showClmSelectOption && '1px solid' }}
						onClick={() => setTimeout(() => setShowClmSelectOption(true), 0)}
					/>

					{/* columns */}
					<div
						id="insideClmSelect"
						className={`allColumnContainer shadow-5 ${showClmSelectOption ? 'block' : 'hidden'}`}
					>
						{tableColumns.map(column => (
							<ColumnLabel key={column.id} column={column} dispatchTableColumns={dispatchTableColumns} />
						))}
					</div>
				</div>
				{/* print icon*/}
				<Tooltip title={<p style={{ fontSize: '130%' }}>Print</p>} placement="top">
					<PrintIcon
						className="cursor-pointer inside icon"
						style={{ padding: '6px', border: inPrint && '1px solid' }}
						onClick={() => handlePrint()}
					/>
				</Tooltip>
				<div style={{ display: 'flex' }}>
					<p htmlFor="rowSize">Row Size:</p>
					<input
						type="text"
						id="rowSize"
						style={{
							borderRadius: '10px',
							marginLeft: '10px',
							padding: '2px',
							width: '20%',
							textAlign: 'center'
						}}
						className={`rounded-input ${classes.inputBg}`}
						value={modifiedPurchageReturnData.row}
						onChange={handleRowChange}
					/>
				</div>
			</div>

			{/* excel converter */}
			<div className="hidden">
				<ReactHtmlTableToExcel
					id="test-table-xls-button"
					className="download-table-xls-button"
					table="table-to-xls"
					filename="tablexls"
					sheet="tablexls"
					buttonText="Download as XLS"
				/>
			</div>
			{loading ? (
				<FuseLoading />
			) : (
				<table id="table-to-xls" className="w-full" style={{ minHeight: '270px' }}>
					<div ref={componentRef} id="downloadPage">
						{/* each single page (table) */}
						{modifiedPurchageReturnData.map((order, idx) => {
							return (
								<SinglePage
									classes={classes}
									generalData={generalData}
									reporTitle="Puchase Return Report"
									tableColumns={tableColumns}
									dispatchTableColumns={dispatchTableColumns}
									data={
										order.isLastPage
											? {
													...order,
													data: order.data.concat({
														quantity: totalQuantity,
														unit_price: totalAmount,
														subtotal: totalSubtotal,
														color: 'Grand Total',
														getterMethod: () => '',
														hideSerialNo: true,
														rowStyle: { fontWeight: 600 }
													})
											  }
											: order
									}
									serialNumber={
										pagination ? page * size - size + 1 : order.page * order.size - order.size + 1
									}
									setPage={setPage}
									page={page}
									inSiglePageMode={inSiglePageMode}
									setSortBy={setSortBy}
								/>
							);
						})}
					</div>
				</table>
			)}
		</>
	);
};
export default PurchageReturnReportsTable;
