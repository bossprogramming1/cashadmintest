import { faBookOpen, faCalendarAlt, faChevronDown, faTimesCircle } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { makeStyles, TextField } from '@material-ui/core';
import { Autocomplete } from '@material-ui/lab';
import { DatePicker } from '@material-ui/pickers';
import { getBrand, getCategory } from 'app/store/dataSlice';
import moment from 'moment';
import { useEffect, useState, useRef } from 'react';
import { Controller, useFormContext } from 'react-hook-form';
import { useDispatch, useSelector } from 'react-redux';
import { getReportFilterMakeStyles } from '../../../reportUtils/reportMakeStyls';

const useStyles = makeStyles(theme => ({
	...getReportFilterMakeStyles(theme)
}));

function PurhcageReturnFilterMenu({ inShowAllMode, handleGetPurchagesReturn, handleGetAllPurchagesReturn }) {
	const classes = useStyles();

	const dispatch = useDispatch();

	const [_reRender, setReRender] = useState(0);

	//select field data
	const brands = useSelector(state => state.data.brands);
	const categories = useSelector(state => state.data?.parent_categories);

	const methods = useFormContext();
	const { control, getValues, setValue } = methods;
	const values = getValues();

	//element refs

	const purchaseNoEl = useRef(null);

	useEffect(() => {
		dispatch(getBrand());
		dispatch(getCategory());
	}, []);

	return (
		<div className={classes.filterMenuContainer}>
			<div className="allFieldContainer borderTop mt-4">
				{/* Date from */}
				<div className="fieldContainer">
					<FontAwesomeIcon
						className="icon cursor-pointer"
						icon={faCalendarAlt}
						onClick={() => document.getElementById('dateAfterEl').click()}
					/>

					<div className="dateLabel" onClick={() => document.getElementById('dateAfterEl').click()}>
						Date From
					</div>

					<Controller
						name="date_after"
						control={control}
						render={({ field }) => {
							return (
								<DatePicker
									id="dateAfterEl"
									className="hidden"
									autoOk
									clearable
									format={'dd/MM/yyyy'}
									maxDate={values.date_before || new Date()}
									value={field.value || ''}
									onChange={value => {
										value
											? field.onChange(moment(new Date(value)).format('YYYY-MM-DD'))
											: field.onChange('');
										setReRender(Math.random());
										inShowAllMode ? handleGetAllPurchagesReturn() : handleGetPurchagesReturn();
									}}
								/>
							);
						}}
					/>
				</div>

				{/* Date to */}
				<div className="fieldContainer">
					<FontAwesomeIcon
						className="icon cursor-pointer"
						icon={faCalendarAlt}
						onClick={() => document.getElementById('dateBeforeEl').click()}
					/>

					<div className="dateLabel" onClick={() => document.getElementById('dateBeforeEl').click()}>
						Date To
					</div>

					<Controller
						name="date_before"
						control={control}
						render={({ field }) => {
							return (
								<DatePicker
									id="dateBeforeEl"
									className="hidden"
									autoOk
									clearable
									format={'dd/MM/yyyy'}
									value={field.value || ''}
									minDate={values.date_after}
									maxDate={new Date()}
									onChange={value => {
										value
											? field.onChange(moment(new Date(value)).format('YYYY-MM-DD'))
											: field.onChange('');
										setReRender(Math.random());
										inShowAllMode ? handleGetAllPurchagesReturn() : handleGetPurchagesReturn();
									}}
								/>
							);
						}}
					/>
				</div>

				{/* Invoice No */}
				<div className="fieldContainer">
					<FontAwesomeIcon className="icon" icon={faBookOpen} />

					<input
						ref={purchaseNoEl}
						onKeyDown={e => {
							if (e.key === 'Enter') {
								setValue('purchase_no', e.target.value);
								inShowAllMode ? handleGetAllPurchagesReturn() : handleGetPurchagesReturn();
								purchaseNoEl.current.blur();
								purchaseNoEl.current.value = '';
								setReRender(Math.random());
							}
						}}
						onFocus={() =>
							(purchaseNoEl.current.value = purchaseNoEl.current.value || values.username || '')
						}
						className="textField"
						style={{ width: '75px' }}
						placeholder="Purchase No"
					/>
				</div>

				{/* brand */}
				<div className="fieldContainer">
					<FontAwesomeIcon className="icon" icon={faBookOpen} />

					<div
						className="selectLabel"
						style={{
							width: values.brandFocused ? '0px' : '45px',
							margin: values.brandFocused ? '0px' : '2px 5px 0px 10px'
						}}
						onClick={() => {
							setValue('brandFocused', true);
							setReRender(Math.random());
							setTimeout(() => document.getElementById('brandEl').focus(), 300);
						}}
					>
						Brand
					</div>
					<FontAwesomeIcon
						className="selectOpenIcon cursor-pointer"
						style={{
							width: values.brandFocused ? '0px' : '15px',
							margin: values.brandFocused ? '0px' : '2px 10px 0px 0px'
						}}
						onClick={() => {
							setValue('brandFocused', true);
							setReRender(Math.random());
							setTimeout(() => document.getElementById('brandEl').focus(), 300);
						}}
						icon={faChevronDown}
					/>

					<Controller
						name="brand"
						control={control}
						render={({ field: { onChange, value } }) => (
							<Autocomplete
								id="brandEl"
								className="mb-3 selectField"
								style={{
									width: values.brandFocused ? '130px' : '0px',
									margin: values.brandFocused ? '0px 10px' : '0px',
									display: values.brandFocused ? 'block' : 'none'
								}}
								classes={{ endAdornment: 'endAdornment' }}
								openOnFocus={true}
								onClose={() => {
									setValue('brandFocused', false);
									setReRender(Math.random());
								}}
								freeSolo
								options={brands}
								value={value ? brands.find(data => data.id == value) : null}
								getOptionLabel={option => option?.name}
								onChange={(event, newValue) => {
									onChange(newValue?.id);
									setValue('brandName', newValue?.name || '');
									inShowAllMode ? handleGetAllPurchagesReturn() : handleGetPurchagesReturn();
								}}
								renderInput={params => (
									<TextField
										{...params}
										className="textFieldUnderSelect"
										placeholder="Select Brand"
									/>
								)}
							/>
						)}
					/>
				</div>

				{/* subLedger */}
				<div className="fieldContainer">
					<FontAwesomeIcon className="icon" icon={faBookOpen} />

					<div
						className="selectLabel"
						style={{
							width: values.categoryFocused ? '0px' : '70px',
							margin: values.categoryFocused ? '0px' : '3px 5px 0px 5px'
						}}
						onClick={() => {
							setValue('categoryFocused', true);
							setReRender(Math.random());
							setTimeout(() => document.getElementById('categoryEl').focus(), 300);
						}}
					>
						Category
					</div>
					<FontAwesomeIcon
						className="selectOpenIcon cursor-pointer"
						style={{
							width: values.categoryFocused ? '0px' : '15px',
							margin: values.categoryFocused ? '0px' : '2px 10px 0px 0px'
						}}
						onClick={() => {
							setValue('categoryFocused', true);
							setReRender(Math.random());
							setTimeout(() => document.getElementById('categoryEl').focus(), 300);
						}}
						icon={faChevronDown}
					/>

					<Controller
						name="category"
						control={control}
						render={({ field: { onChange, value } }) => (
							<Autocomplete
								id="categoryEl"
								className="mb-3 selectField"
								style={{
									width: values.categoryFocused ? '130px' : '0px',
									margin: values.categoryFocused ? '0px 10px' : '0px',
									display: values.categoryFocused ? 'block' : 'none'
								}}
								classes={{ endAdornment: 'endAdornment' }}
								openOnFocus={true}
								onClose={() => {
									setValue('categoryFocused', false);
									setReRender(Math.random());
								}}
								freeSolo
								options={categories}
								value={value ? categories.find(data => data.id == value) : null}
								getOptionLabel={option => `${option?.name}`}
								onChange={(event, newValue) => {
									onChange(newValue?.id);
									setValue('categoryName', newValue?.name || '');
									inShowAllMode ? handleGetAllPurchagesReturn() : handleGetPurchagesReturn();
								}}
								renderInput={params => (
									<TextField
										{...params}
										className="textFieldUnderSelect"
										placeholder="Select Category"
									/>
								)}
							/>
						)}
					/>
				</div>
			</div>

			{/* keywords */}
			<div className="allKeyWrdContainer">
				{values.date_after && (
					<div className="keywordContainer">
						<b>Date From</b>
						<div>
							<FontAwesomeIcon className="iconWithKeyWord" icon={faCalendarAlt} />
							<p>{moment(new Date(values.date_after)).format('DD-MM-YYYY')}</p>
							<FontAwesomeIcon
								className="closeIconWithKeyWord"
								icon={faTimesCircle}
								onClick={() => {
									setValue('date_after', '');
									inShowAllMode ? handleGetAllPurchagesReturn() : handleGetPurchagesReturn();
									setReRender(Math.random());
								}}
							/>
						</div>
					</div>
				)}

				{values.date_before && (
					<div className="keywordContainer">
						<b>Date To</b>
						<div>
							<FontAwesomeIcon className="iconWithKeyWord" icon={faCalendarAlt} />
							<p>{moment(new Date(values.date_before)).format('DD-MM-YYYY')}</p>
							<FontAwesomeIcon
								className="closeIconWithKeyWord"
								icon={faTimesCircle}
								onClick={() => {
									setValue('date_before', '');
									inShowAllMode ? handleGetAllPurchagesReturn() : handleGetPurchagesReturn();
									setReRender(Math.random());
								}}
							/>
						</div>
					</div>
				)}

				{values.purchase_no && (
					<div className="keywordContainer">
						<b>Purchage No</b>
						<div>
							<FontAwesomeIcon className="iconWithKeyWord" icon={faBookOpen} />
							<p>{values.purchase_no}</p>
							<FontAwesomeIcon
								className="closeIconWithKeyWord"
								icon={faTimesCircle}
								onClick={() => {
									setValue('purchase_no', '');
									purchaseNoEl.current.value = '';
									inShowAllMode ? handleGetAllPurchagesReturn() : handleGetPurchagesReturn();
									setReRender(Math.random());
								}}
							/>
						</div>
					</div>
				)}

				{values.brandName && (
					<div className="keywordContainer">
						<b>brand</b>
						<div>
							<FontAwesomeIcon className="iconWithKeyWord" icon={faBookOpen} />
							<p>{values.brandName}</p>
							<FontAwesomeIcon
								className="closeIconWithKeyWord"
								icon={faTimesCircle}
								onClick={() => {
									setValue('brandName', '');
									setValue('brand', '');
									inShowAllMode ? handleGetAllPurchagesReturn() : handleGetPurchagesReturn();
									setReRender(Math.random());
								}}
							/>
						</div>
					</div>
				)}

				{values.categoryName && (
					<div className="keywordContainer">
						<b>Category</b>
						<div>
							<FontAwesomeIcon className="iconWithKeyWord" icon={faBookOpen} />
							<p>{values.categoryName}</p>
							<FontAwesomeIcon
								className="closeIconWithKeyWord"
								icon={faTimesCircle}
								onClick={() => {
									setValue('categoryName', '');
									setValue('category', '');
									inShowAllMode ? handleGetAllPurchagesReturn() : handleGetPurchagesReturn();
									setReRender(Math.random());
								}}
							/>
						</div>
					</div>
				)}
			</div>
		</div>
	);
}

export default PurhcageReturnFilterMenu;
