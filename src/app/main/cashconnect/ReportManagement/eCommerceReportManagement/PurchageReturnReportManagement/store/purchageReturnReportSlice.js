import { createAsyncThunk, createSlice } from '@reduxjs/toolkit';
import { FILTER_PURCHASE_RETURN_REPORT, FILTER_PURCHASE_RETURN_REPOR_WITHOUT_PG } from 'app/constant/constants';
import axios from 'axios';

export const getPurchagesReturn = createAsyncThunk(
	'purchagesReturnReportManagement/getPurchagesReturn',
	async ({ values, pageAndSize }, { rejectWithValue }) => {
		try {
			axios.defaults.headers.common['Content-type'] = 'application/json';
			axios.defaults.headers.common.Authorization = localStorage.getItem('jwt_access_token');

			const res = await axios.get(
				`${FILTER_PURCHASE_RETURN_REPORT}?purchase_no=${values.purchase_no || ''}&category=${
					values.category || ''
				}&date_after=${values.date_after || ''}&date_before=${values.date_before || ''}`,
				{ params: pageAndSize }
			);

			delete axios.defaults.headers.common['Content-type'];
			delete axios.defaults.headers.common.Authorization;

			return res.data || {};
		} catch (err) {
			return rejectWithValue({});
		}
	}
);

export const getAllPurchagesReturn = createAsyncThunk(
	'purchagesReturnReportManagement/getAllPurchagesReturn',
	async (values, { rejectWithValue }) => {
		try {
			axios.defaults.headers.common['Content-type'] = 'application/json';
			axios.defaults.headers.common.Authorization = localStorage.getItem('jwt_access_token');

			const res = await axios.get(
				`${FILTER_PURCHASE_RETURN_REPOR_WITHOUT_PG}?purchase_no=${values.purchase_no || ''}&category=${
					values.category || ''
				}&date_after=${values.date_after || ''}&date_before=${values.date_before || ''}`
			);

			delete axios.defaults.headers.common['Content-type'];
			delete axios.defaults.headers.common.Authorization;

			return res.data || {};
		} catch (err) {
			return rejectWithValue({});
		}
	}
);

const purchageReportsSlice = createSlice({
	name: 'purchagesReturnReportManagement/purchagesReturn',
	initialState: {
		purchase_returns: []
	},
	extraReducers: {
		[getPurchagesReturn.fulfilled]: (state, action) => {
			state.purchase_returns = action.payload?.purchase_returns || [];
		},
		[getPurchagesReturn.rejected]: state => {
			state.purchase_returns = [];
		},
		[getAllPurchagesReturn.fulfilled]: (state, action) => {
			state.purchase_returns = action.payload?.purchase_returns || [];
		},
		[getAllPurchagesReturn.rejected]: state => {
			state.purchase_returns = [];
		}
	}
});

export default purchageReportsSlice.reducer;
