import { combineReducers } from '@reduxjs/toolkit';
import receiptReports from './purchageReturnReportSlice';

const reducer = combineReducers({
	receiptReports
});

export default reducer;
