import FusePageCarded from '@fuse/core/FusePageCarded';
import { getUserPermissions } from 'app/store/dataSlice';
import { useParams } from 'react-router-dom';
import { motion } from 'framer-motion';
import { Icon, Typography } from '@material-ui/core';
import withReducer from 'app/store/withReducer';
import React, { useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import reducer from '../store';
import ProfitLossIncomeReportsTable from './ProfitLossIncomeReportsTable';

const ProfitLossIncomeReport = () => {
	const UserPermissions = useSelector(state => state.data.UserPermissions);
	useEffect(() => {
		dispatch(getUserPermissions());
	}, []);
	const dispatch = useDispatch();
	const [profitLossIncomeReport, setProfitLossIncomeReport] = useState(false);
	const routeParams = useParams();

	useEffect(() => {
		if (routeParams.profitLossIncomeReport) {
			setProfitLossIncomeReport(true);
		}
	}, [routeParams.profitLossIncomeReport]);

	return (
		<FusePageCarded
			header={
				<div className="flex flex-1 w-full items-center justify-between">
					<div className="flex items-center">
						<Icon
							component={motion.span}
							initial={{ scale: 0 }}
							animate={{ scale: 1, transition: { delay: 0.2 } }}
							className="text-24 md:text-32"
						>
							person
						</Icon>
						<Typography
							component={motion.span}
							initial={{ x: -10 }}
							animate={{ x: 0, transition: { delay: 0.2 } }}
							delay={300}
							className="hidden sm:flex text-16 md:text-24 mx-12 font-semibold"
						>
							Statement of Profit or Loss
						</Typography>
					</div>
				</div>
			}
			headerBgHeight="102px"
			className="bg-grey-300"
			classes={{
				content: 'bg-white',
				contentCard: 'overflow-hidden',
				header: 'min-h-52 h-52'
			}}
			content={
				// UserPermissions.includes(PAYMENT_VOIUCHER_SUMMARY_LIST) ? (
				<ProfitLossIncomeReportsTable />
				// ) : (
				// 	<PagenotFound />
				// )
			}
			innerScroll
		/>
	);
};

export default withReducer('profitLossIncomesReportManagement', reducer)(ProfitLossIncomeReport);
