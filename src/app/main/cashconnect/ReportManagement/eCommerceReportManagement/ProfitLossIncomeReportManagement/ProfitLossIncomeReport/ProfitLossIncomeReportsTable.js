import { faBookOpen, faDownload, faFileExcel, faFilePdf, faScroll } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { yupResolver } from '@hookform/resolvers/yup';
import {
	Grid,
	Tooltip,
	makeStyles,
	Paper,
	Table,
	TableBody,
	TableCell,
	TableContainer,
	TableFooter,
	TableHead,
	TableRow
} from '@material-ui/core';
import PrintIcon from '@material-ui/icons/Print';
import useReportData from 'app/@customHooks/useReportData';
import useUserInfo from 'app/@customHooks/useUserInfo';
import getPaginationData from 'app/@helpers/getPaginationData';
import getTotalAmount from 'app/@helpers/getTotalAmount';
import html2PDF from 'jspdf-html2canvas';
import FuseLoading from '@fuse/core/FuseLoading';
import { useHistory } from 'react-router';
import { useParams } from 'react-router-dom';
import React, { useEffect, useLayoutEffect, useReducer, useRef, useState } from 'react';
import { Email, Language, LocationOn, PhoneEnabled } from '@material-ui/icons';
import { unstable_batchedUpdates } from 'react-dom';
import { FormProvider, useForm } from 'react-hook-form';
import ReactHtmlTableToExcel from 'react-html-table-to-excel';
import { useDispatch } from 'react-redux';
import { useReactToPrint } from 'react-to-print';
import * as yup from 'yup';
import moment from 'moment';
import { BASE_URL, GET_SITESETTINGS } from 'app/constant/constants';
import ColumnLabel from '../../../reportComponents/ColumnLabel';
import Pagination from '../../../reportComponents/Pagination';
import SiglePage from '../../../reportComponents/SiglePage';
import { getReportMakeStyles } from '../../../reportUtils/reportMakeStyls';
import tableColumnsReducer from '../../../reportUtils/tableColumnsReducer';
import { getAllProfitLossIncomes, getProfitLossIncomes } from '../store/profitLossIncomeReportSlice';
import ProfitLossIncomeFilterMenu from './ProfitLossIncomeFilterMenu';

const useStyles = makeStyles(theme => ({
	...getReportMakeStyles(theme)
}));

const schema = yup.object().shape({});

const ProfitLossIncomeReportsTable = () => {
	const classes = useStyles();

	const methods = useForm({
		mode: 'onChange',
		defaultValues: {},
		resolver: yupResolver(schema)
	});

	const router = useHistory();

	const { getValues } = methods;

	const dispatch = useDispatch();

	const { authTOKEN } = useUserInfo();
	const routeParams = useParams();
	const [totalData, setTotalData] = React.useState();
	const [inDowloadPdf, setInDowloadPdf] = useState(false);
	const [inPrint, setInPrint] = useState(false);
	const [payments, setPayments] = useState('');
	const [inShowAllMode, setInShowAllMode] = useState(false);
	const [inSinglePageMode, setInSiglePageMode] = useState(false);
	const [generalData, setGeneralData] = useState({});
	const [inDowloadExcel, setInDowloadExcel] = useState(false);
	const [profitLossIncomeReport, setProfitLossIncomeReport] = useState(false);
	const [title, setTitle] = useState('');
	//print dom ref
	const componentRef = useRef();

	useEffect(() => {
		if (routeParams.profitLossIncomeReport) {
			setProfitLossIncomeReport(true);
		}
	}, [routeParams.profitLossIncomeReport]);
	//get general setting data
	useEffect(() => {
		const authTOKEN = {
			headers: {
				'Content-type': 'application/json',
				Authorization: localStorage.getItem('jwt_access_token')
			}
		};

		fetch(`${GET_SITESETTINGS}`, authTOKEN)
			.then(response => response.json())
			.then(data => setGeneralData(data.general_settings[0] || {}))
			.catch(() => setGeneralData({}));
	}, []);
	const styles = {
		allSignatureContainer: {
			height: '25px',
			display: 'flex',
			justifyContent: 'space-around',
			alignItems: 'flex-end'
		},
		signatureContainer: {
			height: '20px',
			display: 'flex',
			flexDirection: 'column',
			alignItems: 'center',
			justifyContent: 'space-between'
		},
		divBorder: {
			borderTop: '1px dashed',
			width: '100%'
		},
		h5: {
			width: 'fit-content',
			whiteSpace: 'nowrap'
		}
	};
	//printer action
	const printAction = useReactToPrint({
		content: () => componentRef.current
	});

	const handleGetAllProfitLossIncomes = (pagePram, callBack) => {
		dispatch(getProfitLossIncomes({ values: getValues() })).then(res => {
			const allData = res.payload;
			console.log('allData', allData);
			setPayments(allData);
		});
	};
	console.log('payments', payments);
	//print handler
	const handlePrint = () => {
		setInPrint(true);
		printAction();
		setInPrint(false);
	};

	//pdf downloader action
	const pdfDownloadAction = () => {
		html2PDF(downloadPage, {
			margin: [0, 0, 0, 0],
			filename: 'pdfhtml2.pdf',
			html2canvas: {
				dpi: 300,
				letterRendering: true
			},
			setTestIsImage: false,
			useCORS: true,
			jsPDF: { unit: 'mm', format: 'a4', orientation: 'portrait' }
		});
		setInDowloadPdf(false);
	};

	//pdf download handler
	const handlePdfDownload = () => {
		setInDowloadPdf(true);
		pdfDownloadAction();
		setInDowloadPdf(false);
	};

	//exel download page dom
	let downloadPage = document.getElementById('downloadPage');

	//exel download handler
	const handleExelDownload = () => {
		setInDowloadExcel(true);
		document.getElementById('test-table-xls-button').click();
		setInDowloadExcel(false);
	};

	return (
		<>
			<div className={classes.headContainer}>
				<FormProvider {...methods}>
					<ProfitLossIncomeFilterMenu handleGetAllProfitLossIncomes={handleGetAllProfitLossIncomes} />
				</FormProvider>
			</div>
			<div className={`${classes.menubar} justify-start md:justify-center`}>
				<div className="downloadIcon"></div>

				{/* print icon*/}
				<PrintIcon
					className="cursor-pointer inside icon"
					style={{ padding: '6px', border: inPrint && '1px solid' }}
					onClick={() => printAction()}
				/>

				{/* <div style={{ display: 'flex' }}>
					<p htmlFor="size">Title</p>
					<input
						type="text"
						id="size"
						style={{
							borderRadius: '10px',
							marginLeft: '10px',
							padding: '2px',
							width: '',
							textAlign: 'center'
						}}
						className="rounded-input"
						onChange={e => {
							setTitle(e.target.value);
						}}
					/>
				</div> */}
			</div>

			<div
				ref={componentRef}
				id="downloadPage"
				className="bg-white"
				style={{ marginLeft: '70px', marginRight: '70px', paddingTop: '30px', fontSize: '12px' }}
			>
				<div>
					<div className="logoContainer pr-0 md:-pr-20">
						<img
							style={{
								visibility: generalData.header_image ? 'visible' : 'hidden',
								textAlign: 'center'
							}}
							src={generalData.header_image ? `${BASE_URL}${generalData.header_image}` : null}
							alt="Not found"
						/>
					</div>
				</div>
				<div
					style={{
						textAlign: 'center',
						// borderBottom: '1px solid gray',
						// marginBottom: '20px',
						margin: '-30px 20px 0 20px',

						fontSize: '8px'
					}}
				>
					<LocationOn fontSize="small" />
					{` ${generalData?.address}` || ''} &nbsp; &nbsp; &nbsp; <PhoneEnabled fontSize="small" />
					{` ${generalData?.phone || ''}`}&nbsp; &nbsp; <Email fontSize="small" />
					{` ${generalData?.email || ''}`} &nbsp; &nbsp; <Language fontSize="small" />
					{` ${generalData?.site_address || ''}`}
				</div>
				<div
					className="mb-7"
					style={{
						marginTop: '30px'
					}}
				>
					<h4 className="text-center font-bold">Statement of Profit or Loss</h4>
				</div>
				<div className=" mt-4 mb-7">
					<h5 className="text-center font-bold">
						{/* Date : {`${payments?.date_after} to ${payments?.date_before}`} */}
						{getValues()?.date_after
							? `Date : ${
									getValues()?.date_after
										? moment(new Date(getValues()?.date_after)).format('DD-MM-YYYY')
										: ''
							  } to ${
									getValues()?.date_before
										? moment(new Date(getValues()?.date_before)).format('DD-MM-YYYY')
										: ''
							  }`
							: ''}
						&nbsp;&nbsp; &nbsp;Branch: {getValues()?.branchsName ? getValues()?.branchsName : 'All'}
					</h5>
				</div>
				<Grid container>
					<Grid item xs={12} style={{ border: '1px solid black' }}>
						<TableContainer>
							<Table>
								{/* <TableHead>
									<TableRow className="cursor-pointer" hover>
										<TableCell className="p-4 px-10 border-none text-center">
											<b>Income</b>
										</TableCell>
									</TableRow>
								</TableHead> */}
								<TableHead style={{ borderBottom: '1px solid black' }}>
									<TableRow className="cursor-pointer" hover>
										<TableCell className="p-4 px-10 text-center border-r">
											<b>Particulars</b>
										</TableCell>
										<TableCell className="p-4 px-10  border-gray-300  text-right bold">
											<b>Total</b>
										</TableCell>
									</TableRow>
								</TableHead>
								<TableBody>
									<TableRow className="cursor-pointer" hover>
										<TableCell className="p-4 px-10   border-r">Total Sales Amount</TableCell>
										<TableCell className="p-4 px-10  text-right">
											{payments?.total_sales_amount}
										</TableCell>
									</TableRow>
									<TableRow className="cursor-pointer" hover>
										<TableCell className="p-4 px-10   border-r">Total Purchase Amount</TableCell>
										<TableCell className="p-4 px-10  text-right">
											{payments?.total_purchase_amount}
										</TableCell>
									</TableRow>

									<TableRow className="cursor-pointer" hover>
										<TableCell className="p-4 px-10  border-r">
											<b>Total Grand Profit</b>
										</TableCell>
										<TableCell className="p-4 px-10  border-gray-300  text-right bold">
											<b>{payments?.total_grand_profit}</b>
										</TableCell>
									</TableRow>
									<TableRow className="cursor-pointer" hover>
										<TableCell className="p-4 px-10  border-r">
											<b>Total Sales Return Profit</b>
										</TableCell>
										<TableCell className="p-4 px-10  border-gray-300  text-right bold">
											<b>{payments?.total_sales_return_profit}</b>
										</TableCell>
									</TableRow>
									<TableRow className="cursor-pointer" hover>
										<TableCell className="p-4 px-10  border-r">
											<b>Net Profit</b>
										</TableCell>
										<TableCell className="p-4 px-10  border-gray-300  text-right bold">
											<b>{payments?.net_profit}</b>
										</TableCell>
									</TableRow>
								</TableBody>
							</Table>
						</TableContainer>
					</Grid>
				</Grid>{' '}
				<div className={classes.pageFooterContainer}>
					<div>
						<div style={{ height: 'fit-content' }}>
							<div
								style={{
									// display: !inSiglePageMode ? 'flex' : 'block',
									justifyContent: 'space-between',
									marginLeft: '10px',
									marginRight: '10px'
								}}
							>
								<h6 style={{ textAlign: 'left' }}>Developed by RAMS(Bluebay IT Limited)</h6>
								{/* {!inSiglePageMode && (
								<h6 style={{ textAlign: 'right' }}>Page : {inSiglePageMode ? page : data?.page}</h6>
							)} */}
							</div>
							<img
								style={{ width: '100%' }}
								src="assets/images/logos/footer_report.png"
								alt="footer_report"
							/>
						</div>
					</div>
				</div>
			</div>

			<div style={{ marginLeft: '10%' }} ref={componentRef}></div>
		</>
	);
};
export default ProfitLossIncomeReportsTable;
