import { combineReducers } from '@reduxjs/toolkit';
import profitLossIncomeReports from './profitLossIncomeReportSlice';

const reducer = combineReducers({
	profitLossIncomeReports
});

export default reducer;
