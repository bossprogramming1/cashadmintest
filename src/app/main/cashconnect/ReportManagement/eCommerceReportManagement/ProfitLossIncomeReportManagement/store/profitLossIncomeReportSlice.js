import { createAsyncThunk, createSlice } from '@reduxjs/toolkit';
import axios from 'axios';
import { PROFITLOSSINCOME_FILTER_BY, PROFITLOSSINCOME_FILTER_WITHOUT_PG } from 'app/constant/constants';

export const getProfitLossIncomes = createAsyncThunk(
	'profitLossIncomesReportManagement/getProfitLossIncomes',
	async ({ values, pageAndSize }, { rejectWithValue }) => {
		try {
			axios.defaults.headers.common['Content-type'] = 'application/json';
			axios.defaults.headers.common.Authorization = localStorage.getItem('jwt_access_token');

			const res = await axios.get(
				`${PROFITLOSSINCOME_FILTER_BY}?ledger=${values.ledger || ''}&sub_ledger=${
					values.sub_ledger || ''
				}&date_after=${values.date_after || ''}&date_before=${values.date_before || ''}&branch=${
					values.branch || ''
				}&account_type=${values.account_type || ''}`,
				{ params: pageAndSize }
			);

			delete axios.defaults.headers.common['Content-type'];
			delete axios.defaults.headers.common.Authorization;

			return res.data || {};
		} catch (err) {
			return rejectWithValue({});
		}
	}
);

export const getAllProfitLossIncomes = createAsyncThunk(
	'profitLossIncomesReportManagement/getAllProfitLossIncomes',
	async (values, { rejectWithValue }) => {
		try {
			axios.defaults.headers.common['Content-type'] = 'application/json';
			axios.defaults.headers.common.Authorization = localStorage.getItem('jwt_access_token');

			const res = await axios.get(
				`${PROFITLOSSINCOME_FILTER_WITHOUT_PG}?ledger=${values.ledger || ''}&sub_ledger=${
					values.sub_ledger || ''
				}&date_after=${values.date_after || ''}&branch=${values.branch || ''}&branch=${
					values.branch || ''
				}&date_before=${values.date_before || ''}${
					values.account_type ? `&account_type=${values.account_type}` : ''
				}`
			);

			delete axios.defaults.headers.common['Content-type'];
			delete axios.defaults.headers.common.Authorization;

			return res.data || {};
		} catch (err) {
			return rejectWithValue({});
		}
	}
);

const paymentreceiptReportsSlice = createSlice({
	name: 'profitLossIncomesReportManagement/paymentreceipts',
	initialState: {
		paymentreceipts: []
	},
	extraReducers: {
		[getProfitLossIncomes.fulfilled]: (state, action) => {
			state.paymentreceipts = action.payload?.paymentreceipts || [];
		},
		[getProfitLossIncomes.rejected]: state => {
			state.paymentreceipts = [];
		},
		[getAllProfitLossIncomes.fulfilled]: (state, action) => {
			state.paymentreceipts = action.payload?.paymentreceipts || [];
		},
		[getAllProfitLossIncomes.rejected]: state => {
			state.paymentreceipts = [];
		}
	}
});

export default paymentreceiptReportsSlice.reducer;
