import { faBookOpen, faChevronDown, faTimesCircle, faCalendarAlt } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { makeStyles, TextField } from '@material-ui/core';
import { Autocomplete } from '@material-ui/lab';
import { LocalActivity } from '@material-ui/icons';
import { getBranches, getCategory, getProducts, getSubCategory, getSubSubCategory } from 'app/store/dataSlice';
import { selectMainTheme } from 'app/store/fuse/settingsSlice';
import { useEffect, useState } from 'react';
import { Controller, useFormContext } from 'react-hook-form';
import { useDispatch, useSelector } from 'react-redux';
import moment from 'moment';
import { DatePicker } from '@material-ui/pickers';
import { getReportFilterMakeStyles } from '../../../reportUtils/reportMakeStyls';

const useStyles = makeStyles(theme => ({
	...getReportFilterMakeStyles(theme)
}));

function ProfitFilterMenu({ inShowAllMode, handleGetProfits, handleGetAllProfits }) {
	const classes = useStyles();

	const dispatch = useDispatch();

	const [_reRender, setReRender] = useState(0);

	//select field data
	const categories = useSelector(state => state.data?.parent_categories);
	const products = useSelector(state => state.data.products);

	const branchs = useSelector(state => state.data.branches);
	const mainTheme = useSelector(selectMainTheme);
	const searchText = useSelector(({ profitsManagement }) => profitsManagement?.profits?.searchText);
	const methods = useFormContext();
	const { control, getValues, setValue } = methods;
	const values = getValues();

	useEffect(() => {
		dispatch(getCategory());
		dispatch(getProducts());

		dispatch(getBranches());
	}, []);

	return (
		<div className={classes.filterMenuContainer}>
			<div className="flex flex-1 items-center justify-center px-12"></div>
			<div className="allFieldContainer borderTop mt-4">
				{/* Date from */}
				<div className="fieldContainer">
					<FontAwesomeIcon
						className="icon cursor-pointer"
						icon={faCalendarAlt}
						onClick={() => document.getElementById('dateAfterEl').click()}
					/>

					<div className="dateLabel" onClick={() => document.getElementById('dateAfterEl').click()}>
						Date From
					</div>

					<Controller
						name="date_after"
						control={control}
						render={({ field }) => {
							return (
								<DatePicker
									id="dateAfterEl"
									className="hidden"
									autoOk
									clearable
									format={'dd/MM/yyyy'}
									maxDate={values.date_before || new Date()}
									value={field.value || ''}
									onChange={value => {
										value
											? field.onChange(moment(new Date(value)).format('YYYY-MM-DD'))
											: field.onChange('');
										setReRender(Math.random());
										inShowAllMode ? handleGetAllProfits() : handleGetProfits();
									}}
								/>
							);
						}}
					/>
				</div>

				{/* Date to */}
				<div className="fieldContainer">
					<FontAwesomeIcon
						className="icon cursor-pointer"
						icon={faCalendarAlt}
						onClick={() => document.getElementById('dateBeforeEl').click()}
					/>

					<div className="dateLabel" onClick={() => document.getElementById('dateBeforeEl').click()}>
						Date To
					</div>

					<Controller
						name="date_before"
						control={control}
						render={({ field }) => {
							return (
								<DatePicker
									id="dateBeforeEl"
									className="hidden"
									autoOk
									clearable
									format={'dd/MM/yyyy'}
									value={field.value || ''}
									minDate={values.date_after}
									maxDate={new Date()}
									onChange={value => {
										value
											? field.onChange(moment(new Date(value)).format('YYYY-MM-DD'))
											: field.onChange('');
										setReRender(Math.random());
										inShowAllMode ? handleGetAllProfits() : handleGetProfits();
									}}
								/>
							);
						}}
					/>
				</div>
				<div className="allFieldContainer borderTop mt-4">
					{/* Product */}
					<div className="fieldContainer">
						<FontAwesomeIcon className="icon" icon={faBookOpen} />

						<div
							className="selectLabel"
							style={{
								width: values.productFocused ? '0px' : '70px',
								margin: values.productFocused ? '0px' : '3px 5px 0px 5px'
							}}
							onClick={() => {
								setValue('productFocused', true);
								setReRender(Math.random());
								setTimeout(() => document.getElementById('productEl').focus(), 300);
							}}
						>
							Product
						</div>
						<FontAwesomeIcon
							className="selectOpenIcon cursor-pointer"
							style={{
								width: values.productFocused ? '0px' : '15px',
								margin: values.productFocused ? '0px' : '2px 10px 0px 0px'
							}}
							onClick={() => {
								setValue('productFocused', true);
								setReRender(Math.random());
								setTimeout(() => document.getElementById('productEl').focus(), 300);
							}}
							icon={faChevronDown}
						/>

						<Controller
							name="product"
							control={control}
							render={({ field: { onChange, value } }) => (
								<Autocomplete
									id="productEl"
									className="mb-3 selectField"
									style={{
										width: values.productFocused ? '130px' : '0px',
										margin: values.productFocused ? '0px 10px' : '0px',
										display: values.productFocused ? 'block' : 'none'
									}}
									classes={{ endAdornment: 'endAdornment' }}
									openOnFocus={true}
									onClose={() => {
										setValue('productFocused', false);
										setReRender(Math.random());
									}}
									freeSolo
									options={products}
									value={value ? products.find(data => data.id == value) : null}
									getOptionLabel={option => `${option?.name}`}
									onChange={(event, newValue) => {
										onChange(newValue?.id);

										setValue('productName', newValue?.name || '');
										inShowAllMode ? handleGetAllProfits() : handleGetProfits();
									}}
									renderInput={params => (
										<TextField
											{...params}
											className="textFieldUnderSelect"
											placeholder="Select Product"
										/>
									)}
								/>
							)}
						/>
					</div>
				</div>
				{/* Category */}
				<div className="fieldContainer">
					<FontAwesomeIcon className="icon" icon={faBookOpen} />

					<div
						className="selectLabel"
						style={{
							width: values.categoryFocused ? '0px' : '70px',
							margin: values.categoryFocused ? '0px' : '3px 5px 0px 5px'
						}}
						onClick={() => {
							setValue('categoryFocused', true);
							setReRender(Math.random());
							setTimeout(() => document.getElementById('categoryEl').focus(), 300);
						}}
					>
						Category
					</div>
					<FontAwesomeIcon
						className="selectOpenIcon cursor-pointer"
						style={{
							width: values.categoryFocused ? '0px' : '15px',
							margin: values.categoryFocused ? '0px' : '2px 10px 0px 0px'
						}}
						onClick={() => {
							setValue('categoryFocused', true);
							setReRender(Math.random());
							setTimeout(() => document.getElementById('categoryEl').focus(), 300);
						}}
						icon={faChevronDown}
					/>

					<Controller
						name="category"
						control={control}
						render={({ field: { onChange, value } }) => (
							<Autocomplete
								id="categoryEl"
								className="mb-3 selectField"
								style={{
									width: values.categoryFocused ? '130px' : '0px',
									margin: values.categoryFocused ? '0px 10px' : '0px',
									display: values.categoryFocused ? 'block' : 'none'
								}}
								classes={{ endAdornment: 'endAdornment' }}
								openOnFocus={true}
								onClose={() => {
									setValue('categoryFocused', false);
									setReRender(Math.random());
								}}
								freeSolo
								options={categories}
								value={value ? categories.find(data => data.id == value) : null}
								getOptionLabel={option => `${option?.name}`}
								onChange={(event, newValue) => {
									onChange(newValue?.id);
									dispatch(getSubCategory(newValue?.id));
									setValue('categoryName', newValue?.name || '');
									inShowAllMode ? handleGetAllProfits() : handleGetProfits();
								}}
								renderInput={params => (
									<TextField
										{...params}
										className="textFieldUnderSelect"
										placeholder="Select Category"
									/>
								)}
							/>
						)}
					/>
				</div>

				{/* branchs  */}
				<div className="fieldContainer">
					<LocalActivity style={{ fontSize: '25px' }} />

					<div
						className="selectLabel"
						style={{
							width: values.branchsFocused ? '0px' : '54px',
							margin: values.branchsFocused ? '0px' : '2px 5px 0px 5px'
						}}
						onClick={() => {
							setValue('branchsFocused', true);
							setReRender(Math.random());
							setTimeout(() => document.getElementById('branchsEl').focus(), 300);
						}}
					>
						Branch
					</div>
					<FontAwesomeIcon
						className="selectOpenIcon cursor-pointer"
						style={{
							width: values.branchsFocused ? '0px' : '15px',
							margin: values.branchsFocused ? '0px' : '2px 10px 0px 0px'
						}}
						onClick={() => {
							setValue('branchsFocused', true);
							setReRender(Math.random());
							setTimeout(() => document.getElementById('branchsEl').focus(), 300);
						}}
						icon={faChevronDown}
					/>

					<Controller
						name="branch"
						control={control}
						render={({ field: { onChange, value } }) => (
							<Autocomplete
								id="branchsEl"
								className="mb-3 selectField"
								style={{
									width: values.branchsFocused ? '130px' : '0px',
									margin: values.branchsFocused ? '0px 10px' : '0px',
									display: values.branchsFocused ? 'block' : 'none'
								}}
								classes={{ endAdornment: 'endAdornment' }}
								openOnFocus={true}
								onClose={() => {
									setValue('branchsFocused', false);
									setReRender(Math.random());
								}}
								freeSolo
								options={branchs}
								value={value ? branchs.find(data => data.id == value) : null}
								getOptionLabel={option => `${option.name}`}
								onChange={(event, newValue) => {
									onChange(newValue?.id);
									setValue('branchsName', newValue?.name || '');
									inShowAllMode ? handleGetAllProfits() : handleGetProfits();
								}}
								renderInput={params => (
									<TextField
										{...params}
										className="textFieldUnderSelect"
										placeholder="Select Branch"
									/>
								)}
							/>
						)}
					/>
				</div>
			</div>

			{/* keywords */}
			<div className="allKeyWrdContainer">
				{values.date_after && (
					<div className="keywordContainer">
						<b>Date From</b>
						<div>
							<FontAwesomeIcon className="iconWithKeyWord" icon={faCalendarAlt} />
							<p>{moment(new Date(values.date_after)).format('DD-MM-YYYY')}</p>
							<FontAwesomeIcon
								className="closeIconWithKeyWord"
								icon={faTimesCircle}
								onClick={() => {
									setValue('date_after', '');
									inShowAllMode ? handleGetAllProfits() : handleGetProfits();
									setReRender(Math.random());
								}}
							/>
						</div>
					</div>
				)}

				{values.date_before && (
					<div className="keywordContainer">
						<b>Date To</b>
						<div>
							<FontAwesomeIcon className="iconWithKeyWord" icon={faCalendarAlt} />
							<p>{moment(new Date(values.date_before)).format('DD-MM-YYYY')}</p>
							<FontAwesomeIcon
								className="closeIconWithKeyWord"
								icon={faTimesCircle}
								onClick={() => {
									setValue('date_before', '');
									inShowAllMode ? handleGetAllProfits() : handleGetProfits();
									setReRender(Math.random());
								}}
							/>
						</div>
					</div>
				)}
				{values.productName && (
					<div className="keywordContainer">
						<b>Product</b>
						<div>
							<FontAwesomeIcon className="iconWithKeyWord" icon={faBookOpen} />
							<p>{values.productName}</p>
							<FontAwesomeIcon
								className="closeIconWithKeyWord"
								icon={faTimesCircle}
								onClick={() => {
									setValue('productName', '');
									setValue('product', '');
									inShowAllMode ? handleGetAllProfits() : handleGetProfits();
									setReRender(Math.random());
								}}
							/>
						</div>
					</div>
				)}
				{values.categoryName && (
					<div className="keywordContainer">
						<b>Category</b>
						<div>
							<FontAwesomeIcon className="iconWithKeyWord" icon={faBookOpen} />
							<p>{values.categoryName}</p>
							<FontAwesomeIcon
								className="closeIconWithKeyWord"
								icon={faTimesCircle}
								onClick={() => {
									setValue('categoryName', '');
									setValue('category', '');
									inShowAllMode ? handleGetAllProfits() : handleGetProfits();
									setReRender(Math.random());
								}}
							/>
						</div>
					</div>
				)}

				{values.branchsName && (
					<div className="keywordContainer">
						<b>Branch </b>
						<div>
							<LocalActivity className="iconWithKeyWord" style={{ fontSize: '18px' }} />
							<p>{values.branchsName}</p>
							<FontAwesomeIcon
								className="closeIconWithKeyWord"
								icon={faTimesCircle}
								onClick={() => {
									setValue('branchsName', '');
									setValue('branch', '');
									inShowAllMode ? handleGetAllProfits() : handleGetProfits();
									setReRender(Math.random());
								}}
							/>
						</div>
					</div>
				)}
				{values.categoryName && (
					<div className="keywordContainer">
						<b>Category</b>
						<div>
							<FontAwesomeIcon className="iconWithKeyWord" icon={faBookOpen} />
							<p>{values.categoryName}</p>
							<FontAwesomeIcon
								className="closeIconWithKeyWord"
								icon={faTimesCircle}
								onClick={() => {
									setValue('categoryName', '');
									setValue('category', '');
									inShowAllMode ? handleGetAllProfits() : handleGetProfits();
									setReRender(Math.random());
								}}
							/>
						</div>
					</div>
				)}
			</div>
		</div>
	);
}

export default ProfitFilterMenu;
