import FusePageCarded from '@fuse/core/FusePageCarded';
import withReducer from 'app/store/withReducer';
import React from 'react';
import reducer from '../store';
import ProfitReportHeader from './ProfitReportHeader';
import ProfitReportsTable from './ProfitReportsTable';

const ProfitReport = () => {
	return (
		<FusePageCarded
			header={<ProfitReportHeader />}
			headerBgHeight="102px"
			className="bg-grey-300"
			classes={{
				content: 'bg-grey-300',
				contentCard: 'overflow-hidden',
				header: 'min-h-52 h-52'
			}}
			content={<ProfitReportsTable />}
			innerScroll
		/>
	);
};

export default withReducer('profitsReportManagement', reducer)(ProfitReport);
