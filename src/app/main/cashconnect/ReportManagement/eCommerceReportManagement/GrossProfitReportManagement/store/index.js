import { combineReducers } from '@reduxjs/toolkit';
import receiptReports from './profitReportSlice';

const reducer = combineReducers({
	receiptReports
});

export default reducer;
