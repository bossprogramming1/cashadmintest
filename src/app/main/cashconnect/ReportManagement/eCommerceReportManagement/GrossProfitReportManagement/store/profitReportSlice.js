import { createAsyncThunk, createSlice } from '@reduxjs/toolkit';
import axios from 'axios';
import {
	FILTER_SUMMARY_GROSS_PROFIT_REPORT,
	FILTER_SUMMARY_GROSS_PROFIT_REPORT_WITHOUT_PG,
	GET_INVENTORY_SEARCH
} from '../../../../../../constant/constants';

export const getProfits = createAsyncThunk(
	'profitsReportManagement/getProfits',
	async ({ values, pageAndSize }, { rejectWithValue }) => {
		try {
			axios.defaults.headers.common['Content-type'] = 'application/json';
			axios.defaults.headers.common.Authorization = localStorage.getItem('jwt_access_token');

			const res = await axios.get(
				`${FILTER_SUMMARY_GROSS_PROFIT_REPORT}?category=${values.category || ''}&date_after=${
					values.date_after || ''
				}&date_before=${values.date_before || ''}&branch=${values.branch || ''}&product=${
					values.product || ''
				}`,
				{
					params: pageAndSize
				}
			);

			delete axios.defaults.headers.common['Content-type'];
			delete axios.defaults.headers.common.Authorization;

			return res.data || {};
		} catch (err) {
			return rejectWithValue({});
		}
	}
);

export const getAllProfits = createAsyncThunk(
	'profitsReportManagement/getAllProfits',
	async (values, { rejectWithValue }) => {
		try {
			axios.defaults.headers.common['Content-type'] = 'application/json';
			axios.defaults.headers.common.Authorization = localStorage.getItem('jwt_access_token');

			const res = await axios.get(
				`${FILTER_SUMMARY_GROSS_PROFIT_REPORT_WITHOUT_PG}?category=${values.category || ''}&date_after=${
					values.date_after || ''
				}&date_before=${values.date_before || ''}&branch=${values.branch || ''}&product=${values.product || ''}`
			);

			delete axios.defaults.headers.common['Content-type'];
			delete axios.defaults.headers.common.Authorization;

			return res.data || {};
		} catch (err) {
			return rejectWithValue({});
		}
	}
);

export const getSearchText = createAsyncThunk(
	'profitsReportManagement/getSearchText',
	async ({ searchText, pageAndSize }, { rejectWithValue }) => {
		// try {
		axios.defaults.headers.common['Content-type'] = 'application/json';
		axios.defaults.headers.common.Authorization = localStorage.getItem('jwt_access_token');
		const res = await axios.get(`${GET_INVENTORY_SEARCH}?keyword=${searchText}`, { params: pageAndSize });

		return res.data;
	}
);

const profitReportsSlice = createSlice({
	name: 'profitsReportManagement/profits',
	initialState: {
		inventories: [],
		searchText: ''
	},
	reducers: {
		setProfitSearchText: {
			reducer: (state, action) => {
				state.searchText = action.payload;
			},
			prepare: event => ({ payload: event.target.value || '' })
		}
	},

	extraReducers: {
		[getProfits.fulfilled]: (state, action) => {
			state.inventories = action.payload?.inventories || [];
		},
		[getProfits.rejected]: state => {
			state.inventories = [];
		},
		[getAllProfits.fulfilled]: (state, action) => {
			state.inventories = action.payload?.inventories || [];
		},
		[getAllProfits.rejected]: state => {
			state.inventories = [];
		},
		[getSearchText.fulfilled]: (state, action) => {
			state.inventories = action.payload?.inventories || [];
		},
		[getSearchText.rejected]: state => {
			state.inventories = [];
		}
	}
});

export const { setProfitSearchText } = profitReportsSlice.actions;

export default profitReportsSlice.reducer;
