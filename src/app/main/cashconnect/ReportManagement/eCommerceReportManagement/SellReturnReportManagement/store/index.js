import { combineReducers } from '@reduxjs/toolkit';
import receiptReports from './sellReturnReportSlice';

const reducer = combineReducers({
	receiptReports
});

export default reducer;
