import { createAsyncThunk, createSlice } from '@reduxjs/toolkit';
import { FILTER_SELL_RETURN_REPORT, FILTER_SELL_RETURN_REPOR_WITHOUT_PG } from 'app/constant/constants';
import axios from 'axios';

export const getSellsReturn = createAsyncThunk(
	'sellsReturnReportManagement/getSellsReturn',
	async ({ values, pageAndSize }, { rejectWithValue }) => {
		try {
			axios.defaults.headers.common['Content-type'] = 'application/json';
			axios.defaults.headers.common.Authorization = localStorage.getItem('jwt_access_token');

			const res = await axios.get(
				`${FILTER_SELL_RETURN_REPORT}?sell_no=${values.sell_no || ''}&category=${
					values.category || ''
				}&date_after=${values.date_after || ''}&date_before=${values.date_before || ''}`,
				{ params: pageAndSize }
			);

			delete axios.defaults.headers.common['Content-type'];
			delete axios.defaults.headers.common.Authorization;

			return res.data || {};
		} catch (err) {
			return rejectWithValue({});
		}
	}
);

export const getAllSellsReturn = createAsyncThunk(
	'sellsReturnReportManagement/getAllSellsReturn',
	async (values, { rejectWithValue }) => {
		try {
			axios.defaults.headers.common['Content-type'] = 'application/json';
			axios.defaults.headers.common.Authorization = localStorage.getItem('jwt_access_token');

			const res = await axios.get(
				`${FILTER_SELL_RETURN_REPOR_WITHOUT_PG}?sell_no=${values.sell_no || ''}&category=${
					values.category || ''
				}&date_after=${values.date_after || ''}&date_before=${values.date_before || ''}`
			);

			delete axios.defaults.headers.common['Content-type'];
			delete axios.defaults.headers.common.Authorization;

			return res.data || {};
		} catch (err) {
			return rejectWithValue({});
		}
	}
);

const sellReportsSlice = createSlice({
	name: 'sellsReturnReportManagement/sellsReturn',
	initialState: {
		sell_returns: []
	},
	extraReducers: {
		[getSellsReturn.fulfilled]: (state, action) => {
			state.sell_returns = action.payload?.sell_returns || [];
		},
		[getSellsReturn.rejected]: state => {
			state.sell_returns = [];
		},
		[getAllSellsReturn.fulfilled]: (state, action) => {
			state.sell_returns = action.payload?.sell_returns || [];
		},
		[getAllSellsReturn.rejected]: state => {
			state.sell_returns = [];
		}
	}
});

export default sellReportsSlice.reducer;
