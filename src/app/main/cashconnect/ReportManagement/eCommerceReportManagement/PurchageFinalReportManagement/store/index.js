import { combineReducers } from '@reduxjs/toolkit';
import receiptReports from './purchageFinalReportSlice';

const reducer = combineReducers({
	receiptReports
});

export default reducer;
