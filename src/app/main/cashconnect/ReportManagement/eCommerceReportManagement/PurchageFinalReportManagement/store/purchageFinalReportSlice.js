import { createAsyncThunk, createSlice } from '@reduxjs/toolkit';
import { FILTER_PURCHASE_FINAL_REPORT, FILTER_PURCHASE_FINAL_REPOR_WITHOUT_PG } from 'app/constant/constants';
import axios from 'axios';

export const getPurchagesFinal = createAsyncThunk(
	'purchagesFinalReportManagement/getPurchagesFinal',
	async ({ values, pageAndSize }, { rejectWithValue }) => {
		try {
			axios.defaults.headers.common['Content-type'] = 'application/json';
			axios.defaults.headers.common.Authorization = localStorage.getItem('jwt_access_token');

			const res = await axios.get(
				`${FILTER_PURCHASE_FINAL_REPORT}?purchase_no=${values.purchase_no || ''}&category=${values.category || ''}&date_after=${
					values.date_after || ''
				}&date_before=${values.date_before || ''}`,
				{ params: pageAndSize }
			);

			delete axios.defaults.headers.common['Content-type'];
			delete axios.defaults.headers.common.Authorization;

			return res.data || {};
		} catch (err) {
			return rejectWithValue({});
		}
	}
);

export const getAllPurchagesFinal = createAsyncThunk(
	'purchagesFinalReportManagement/getAllPurchagesFinal',
	async (values, { rejectWithValue }) => {
		try {
			axios.defaults.headers.common['Content-type'] = 'application/json';
			axios.defaults.headers.common.Authorization = localStorage.getItem('jwt_access_token');

			const res = await axios.get(
				`${FILTER_PURCHASE_FINAL_REPOR_WITHOUT_PG}?purchase_no=${values.purchase_no || ''}&category=${
					values.category || ''
				}&date_after=${values.date_after || ''}&date_before=${values.date_before || ''}`
			);

			delete axios.defaults.headers.common['Content-type'];
			delete axios.defaults.headers.common.Authorization;

			return res.data || {};
		} catch (err) {
			return rejectWithValue({});
		}
	}
);

const purchageReportsSlice = createSlice({
	name: 'purchagesFinalReportManagement/purchagesFinal',
	initialState: {
		purchase_finals: []
	},
	extraReducers: {
		[getPurchagesFinal.fulfilled]: (state, action) => {
			state.purchase_finals = action.payload?.purchase_finals || [];
		},
		[getPurchagesFinal.rejected]: state => {
			state.purchase_finals = [];
		},
		[getAllPurchagesFinal.fulfilled]: (state, action) => {
			state.purchase_finals = action.payload?.purchase_finals || [];
		},
		[getAllPurchagesFinal.rejected]: state => {
			state.purchase_finals = [];
		}
	}
});

export default purchageReportsSlice.reducer;
