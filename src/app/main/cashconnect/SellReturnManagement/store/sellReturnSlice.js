import { createAsyncThunk, createSlice } from '@reduxjs/toolkit';
import axios from 'axios';
import { addNotification } from 'app/fuse-layouts/shared-components/notificationPanel/store/dataSlice';
import NotificationModel from 'app/fuse-layouts/shared-components/notificationPanel/model/NotificationModel';
import {
	CREATE_SELLRETURN,
	DELETE_SELLRETURN,
	GET_SELLRETURNID,
	GET_SELL_FINALS_ITMES_BY_INVOICE,
	UPDATE_SELLRETURN
} from '../../../../constant/constants';

export const getSellReturn = createAsyncThunk(
	'sellreturnManagement/sellreturn/getSellReturn',
	async (params, { rejectWithValue }) => {
		const authTOKEN = {
			headers: {
				'Content-type': 'application/json',
				Authorization: localStorage.getItem('jwt_access_token')
			}
		};

		try {
			const response = await axios.get(`${GET_SELLRETURNID}${params}`, authTOKEN);
			const data = await response.data;
			return data === undefined ? null : data;
		} catch (err) {
			return rejectWithValue(params);
		}
	}
);
export const getSellFinalItem = createAsyncThunk(
	'sellreturnManagement/sellreturn/getSellFinalItem',
	async (params, { dispatch, rejectWithValue }) => {
		const authTOKEN = {
			headers: {
				'Content-type': 'application/json',
				Authorization: localStorage.getItem('jwt_access_token')
			}
		};

		try {
			const response = await axios.get(`${GET_SELL_FINALS_ITMES_BY_INVOICE}${params}`, authTOKEN);
			const data = await response.data;
			return data === undefined ? null : data;
		} catch (err) {
			console.log('error', err.response.data);
			dispatch(
				addNotification(
					NotificationModel({
						message: `${err.response.data}`,
						options: { variant: 'error' },
						item_id: params
					})
				)
			);
			return rejectWithValue(params);
		}
	}
);

export const removeSellReturn = createAsyncThunk('sellreturnManagement/sellreturn/removeSellReturn', async val => {
	const authTOKEN = {
		headers: {
			'Content-type': 'application/json',
			Authorization: localStorage.getItem('jwt_access_token')
		}
	};

	const sellreturnId = val.id;
	const response = await axios.delete(`${DELETE_SELLRETURN}${sellreturnId}`, authTOKEN);
	return response;
});

export const updateSellReturn = createAsyncThunk(
	'sellreturnManagement/sellreturn/updateSellReturn',
	async (sellreturnData, { dispatch, getState }) => {
		const { sellreturn } = getState().sellreturnsManagement;

		const authTOKEN = {
			headers: {
				'Content-type': 'application/json',
				Authorization: localStorage.getItem('jwt_access_token')
			}
		};
		const response = await axios.put(`${UPDATE_SELLRETURN}${sellreturn.id}`, sellreturnData, authTOKEN);
		return response;
	}
);

export const saveSellReturn = createAsyncThunk(
	'sellreturnManagement/sellreturn/saveSellReturn',
	async (sellreturnData, { dispatch, getState }) => {
		const authTOKEN = {
			headers: {
				'Content-type': 'application/json',
				Authorization: localStorage.getItem('jwt_access_token')
			}
		};
		const response = await axios.post(`${CREATE_SELLRETURN}`, sellreturnData, authTOKEN);
		return response;
	}
);

const sellreturnSlice = createSlice({
	name: 'sellreturnManagement/sellreturn',
	initialState: null,
	reducers: {
		resetSellReturn: () => null,
		newSellReturn: {
			reducer: (state, action) => action.payload,
			prepare: event => ({
				payload: {
					invoice_no: ''
				}
			})
		}
	},
	extraReducers: {
		[getSellReturn.fulfilled]: (state, action) => action.payload,
		[getSellFinalItem.fulfilled]: (state, action) => action.payload,
		[saveSellReturn.fulfilled]: (state, action) => action.payload,
		[removeSellReturn.fulfilled]: (state, action) => action.payload,
		[updateSellReturn.fulfilled]: (state, action) => action.payload
	}
});

export const { newSellReturn, resetSellReturn } = sellreturnSlice.actions;

export default sellreturnSlice.reducer;
