import { createAsyncThunk, createEntityAdapter, createSlice } from '@reduxjs/toolkit';
import axios from 'axios';
import { DELETE_SELLRETURN, GET_SELLRETURNS } from '../../../../constant/constants';

export const getSellReturns = createAsyncThunk('sellreturnManagement/sellreturns/getSellReturns', async parameter => {
	const { page, size } = parameter;

	axios.defaults.headers.common['Content-type'] = 'application/json';
	axios.defaults.headers.common.Authorization = localStorage.getItem('jwt_access_token');

	try {
		const response = await axios.get(GET_SELLRETURNS, { params: { page, size } });

		sessionStorage.setItem('sellreturns_total_elements', response.data.total_elements);
		sessionStorage.setItem('sellreturns_total_pages', response.data.total_pages);

		delete axios.defaults.headers.common['Content-type'];
		delete axios.defaults.headers.common.Authorization;
		console.log('sellreturnssf', response.da);

		return response.data?.orderitems || [];
	} catch (error) {
		// Handle the error, log it or throw it if needed

		throw error;
	}
});

export const removeSellReturns = createAsyncThunk(
	'sellreturnManagement/sellreturns/removeSellReturns',
	async (sellreturnIds, { dispatch, getState }) => {
		const authTOKEN = {
			headers: {
				'Content-type': 'application/json',
				Authorization: localStorage.getItem('jwt_access_token')
			}
		};
		await axios.delete(`${DELETE_SELLRETURN}`, { sellreturnIds }, authTOKEN);

		return sellreturnIds;
	}
);

const sellreturnsAdapter = createEntityAdapter({});

export const { selectAll: selectSellReturns, selectById: selectSellReturnById } = sellreturnsAdapter.getSelectors(
	state => state.sellreturnsManagement.sellreturns
);

const sellreturnsSlice = createSlice({
	name: 'sellreturnManagement/sellreturns',
	initialState: sellreturnsAdapter.getInitialState({
		searchText: ''
	}),
	reducers: {
		setSellReturnsSearchText: {
			reducer: (state, action) => {
				state.searchText = action.payload;
			},
			prepare: event => ({ payload: event.target.value || '' })
		}
	},
	extraReducers: {
		[getSellReturns.fulfilled]: sellreturnsAdapter.setAll
	}
});

export const { setData, setSellReturnsSearchText } = sellreturnsSlice.actions;
export default sellreturnsSlice.reducer;
