import { combineReducers } from '@reduxjs/toolkit';
import sellreturn from './sellReturnSlice';
import sellreturns from './sellReturnsSlice';

const reducer = combineReducers({
	sellreturn,
	sellreturns
});

export default reducer;
