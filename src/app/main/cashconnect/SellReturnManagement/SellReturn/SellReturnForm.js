import _ from '@lodash';
import TextField from '@material-ui/core/TextField';
import React, { useEffect, useState } from 'react';
import { Controller, useFormContext } from 'react-hook-form';
import { useDispatch } from 'react-redux';
import { useHistory, useParams } from 'react-router-dom';
import DoneOutlineIcon from '@material-ui/icons/DoneOutline';
import EditIcon from '@material-ui/icons/Edit';
import {
	Button,
	Checkbox,
	Grid,
	Paper,
	Table,
	TableBody,
	TableCell,
	TableContainer,
	TableHead,
	TableRow,
	makeStyles,
	styled,
	useTheme
} from '@material-ui/core';
import { BASE_URL } from 'app/constant/constants';
import { getSellFinalItem, saveSellReturn, updateSellReturn } from '../store/sellReturnSlice';

const useStyles = makeStyles(theme => ({
	hidden: {
		display: 'none'
	},
	tblContainer: {
		borderRadius: '0px'
	},
	table: {
		minWidth: 600
	},
	tableHead: {
		backgroundColor: theme.palette.primary[500]
	},
	dateOfSaleContainer: {
		display: 'flex',
		flexWrap: 'nowrap',
		justifyContent: 'space-between',
		minWidth: '250px'
	},
	dateOfSale: {
		width: '115px',
		background: theme.palette.background.paper,
		'& > div': {
			'&:before': {
				borderBottom: '0px solid black !important'
			}
		},
		'& input': {
			boxSizing: 'inherit',
			marginLeft: '5px',
			marginTop: '4px'
		}
	},
	paytypeLabel: {
		'&& > label': {
			fontSize: '16px'
		}
	},
	paidAmount: {
		'&& > p': {
			marginTop: '-27px'
		}
	},
	textField: {
		textAlign: 'center',
		paddingLeft: '22px'
	},
	input: {
		'&::-webkit-outer-spin-button, &::-webkit-inner-spin-button': {
			'-webkit-appearance': 'none',
			margin: 0
		},
		'-moz-appearance': 'textfield' // Firefox
	},
	modal: {
		display: 'flex',
		alignItems: 'center',
		justifyContent: 'center'
	},
	paper: {
		backgroundColor: theme.palette.background.paper,
		border: `2px solid ${theme.palette.primary[800]}`,
		boxShadow: theme.shadows[5],
		padding: theme.spacing(2, 4, 3)
	}
}));
function SellReturnForm(props) {
	const methods = useFormContext();
	const { control, formState, reset, getValues, watch, setError, setValue } = methods;
	const formValues = getValues();
	const { errors, isValid, dirtyFields } = formState;
	const routeParams = useParams();
	const history = useHistory();
	const handleDelete = localStorage.getItem('sellreturnEvent');
	const dispatch = useDispatch();
	const [sellItems, setSellItems] = useState([]);
	const [selected, setSelected] = useState([]);
	const [editableRowIds, setEditableRowIds] = useState({});
	const [isRendom, setIsRendom] = useState(false);

	//Theme Design
	const Theme = useTheme();
	const PrimaryBg = Theme.palette.primary[500];
	const PrimaryDark = Theme.palette.primary[700];
	const PrimaryLight = Theme.palette.primary[400];
	const PaperColor = Theme.palette.background.paper;
	const primaryBlack = Theme.palette.primary.dark;
	const cellStyle = { border: `1px solid ${Theme.palette.grey[600]}`, fontWeight: 'bold', color: primaryBlack };
	let serialNumber = 1;
	const sellReturnItems = watch(`sell_return_items`);
	console.log('sellReturnItems', sellReturnItems);
	const classes = useStyles(props);

	function handleSaveSellReturn() {
		dispatch(saveSellReturn(getValues())).then(res => {
			if (res.payload) {
				localStorage.setItem('sellreturnAlert', 'saveSellReturn');
				history.push('/apps/sellreturn-management/sellreturn');
			}
		});
	}

	function handleUpdateSellReturn() {
		dispatch(updateSellReturn(getValues())).then(res => {
			if (res.payload) {
				localStorage.setItem('sellreturnAlert', 'updateSellReturn');
				history.push('/apps/sellreturn-management/sellreturn');
			}
		});
	}

	useEffect(() => {
		if (routeParams.sellreturnId !== 'new' && sellReturnItems && !isRendom) {
			setSellItems(sellReturnItems);

			setIsRendom(true);
		}
	}, [routeParams.sellreturnId, sellReturnItems, isRendom]);

	const handleSubmitOnKeyDownEnter = ev => {
		if (ev.key === 'Enter') {
			if (routeParams.sellreturnId === 'new' && !(_.isEmpty(dirtyFields) || !isValid)) {
				handleSaveSellReturn();
			} else if (handleDelete !== 'Delete' && routeParams?.sellreturnName) {
				handleUpdateSellReturn();
			}
		}
	};
	const handleGetFinalItemOnKeyDownEnter = ev => {
		if (ev.key === 'Enter') {
			console.log('skdf', ev?.target?.value);

			dispatch(getSellFinalItem(ev?.target?.value)).then(res => {
				if (res.payload) {
					console.log('sdkfj', res.payload);
					setSellItems(res?.payload?.order_items);
					setValue('invoice_no', ev?.target?.value);
				} else {
					console.log('error', res.payload);
				}
			});
		}
	};

	function handleCheck(row) {
		const newSelected = new Set(selected);

		if (newSelected.has(row?.id)) {
			newSelected.delete(row?.id);
		} else {
			newSelected.add(row?.id);
		}

		console.log('itemSelected', Array.from(newSelected));
		setSelected(Array.from(newSelected));
		setValue('sell_return_items', getSelldItems(newSelected)); // Assuming getSelldItems is a function to get full row data
	}
	function getSelldItems(selectedIds) {
		// Assuming sellItems is the array containing all row data
		return sellItems
			.filter(item => selectedIds.has(item.id))
			.map(item => {
				// Extract only the 'id' property from the 'color' object
				const { color, product, size, ...rest } = item;
				return {
					...rest,
					color: color?.id || null,
					product: product?.id,
					size: size?.id || null
				};
			});
	}
	function handleSelectAllClick(orderEvent) {
		if (orderEvent.target.checked) {
			setSelected(sellItems.map(n => n?.id));
			setValue(
				'sell_return_items',
				sellItems.map(item => {
					// Extract only the 'id' property from the 'color' object
					const { color, product, size, ...rest } = item;
					return {
						...rest,
						color: color?.id || null,
						product: product?.id,
						size: size?.id || null
					};
				})
			);
			return;
		}
		setSelected([]);
		setValue('sell_return_items', []);
	}

	function updateProductItem(row) {
		const productitemQuantity = watch(`unit_quantity${row.id}`);
		const updatedSellItems = sellItems.map(sell => {
			if (sell.id === row.id) {
				return {
					...sell,
					quantity: parseInt(productitemQuantity),
					subtotal: (parseInt(productitemQuantity) * sell?.price).toFixed(2)
				};
			}
			return sell;
		});
		setSellItems(updatedSellItems);

		console.log('updatedSellItems', updatedSellItems);
	}

	return (
		<div>
			<Controller
				name="invoice_no"
				control={control}
				render={({ field }) => {
					return (
						<TextField
							{...field}
							className="mt-8 mb-16"
							error={!!errors?.invoice_no}
							helperText={errors?.invoice_no?.message}
							label="Sell Invoice"
							id="invoice_no"
							required
							variant="outlined"
							InputLabelProps={field.value && { shrink: true }}
							fullWidth
							onKeyDown={handleGetFinalItemOnKeyDownEnter}
						/>
					);
				}}
			/>

			{sellItems?.length !== 0 && (
				<Grid container spacing={1} className="mb-16">
					<Grid item xs={12}>
						<Div
							top={PrimaryDark}
							left={PrimaryDark}
							justCont="center"
							pd="2"
							h="fit-content"
							alignItems="space-between"
							border
						>
							<Grid container spacing={1}>
								<Grid item xs={12}>
									<Div bg={PrimaryBg} h="2" justCont="center">
										<Text>Purcahse Final Items Details</Text>
									</Div>
								</Grid>
								{/* product details */}
								<Grid item xs={12}>
									<Div h="fit-content" pd="2" minHeight="272px" alignItems="flex-start" border>
										<TableContainer component={Paper} className={classes.tblContainer}>
											<Table className={classes.table} aria-label="simple table">
												<TableHead className={classes.tableHead}>
													<TableRow>
														<TableCell
															whitespace-nowrap
															style={{ whiteSpace: 'nowrap' }}
															padding="none"
															className="w-40 md:w-64 text-center z-99"
														>
															<Checkbox
																indeterminate={
																	selected?.length > 0 &&
																	selected?.length < sellItems?.length
																}
																checked={selected?.length === sellItems?.length}
																onChange={handleSelectAllClick}
															/>
														</TableCell>
														<TableCell style={{ color: PaperColor }}>No.</TableCell>
														<TableCell
															whitespace-nowrap
															style={{ color: PaperColor }}
															align="center"
														>
															IMG
														</TableCell>
														<TableCell
															whitespace-nowrap
															style={{ color: PaperColor }}
															align="center"
														>
															Product Details
														</TableCell>
														<TableCell
															whitespace-nowrap
															style={{ color: PaperColor }}
															align="center"
														>
															Color{' '}
														</TableCell>
														<TableCell
															whitespace-nowrap
															style={{ color: PaperColor }}
															align="center"
														>
															Size{' '}
														</TableCell>
														<TableCell
															whitespace-nowrap
															style={{ color: PaperColor }}
															align="center"
														>
															Quantity
														</TableCell>
														<TableCell
															whitespace-nowrap
															style={{ color: PaperColor }}
															align="center"
														>
															Price
														</TableCell>
														<TableCell
															whitespace-nowrap
															style={{ color: PaperColor }}
															align="center"
														>
															Total
														</TableCell>
														<TableCell
															whitespace-nowrap
															style={{ color: PaperColor }}
															align="center"
														>
															Action
														</TableCell>
													</TableRow>
												</TableHead>

												<TableBody>
													{sellItems?.map((row, idx) => {
														console.log('sellItems', sellItems);
														const isSelected = selected.indexOf(row?.id) !== -1;

														return (
															<TableRow
																key={row?.name}
																aria-checked={isSelected}
																selected={isSelected}
															>
																<TableCell
																	className="whitespace-nowrap w-40 md:w-64 text-center"
																	padding="none"
																>
																	<Checkbox
																		checked={isSelected}
																		onClick={event => event.stopPropagation()}
																		onChange={() => handleCheck(row)}
																	/>
																</TableCell>
																<TableCell
																	whitespace-nowrap
																	style={cellStyle}
																	component="th"
																	scope="row"
																>
																	{serialNumber++}
																</TableCell>
																<TableCell
																	style={{
																		...cellStyle,
																		paddingTop: '0px',
																		paddingBottom: '0px'
																	}}
																	align="center"
																>
																	<img
																		className="w-full block rounded"
																		style={{
																			borderRadius: '50%',
																			height: '50px',
																			width: '50px'
																		}}
																		src={`${BASE_URL}${
																			row.image || row?.product?.thumbnail
																		}`}
																		alt="Not Found"
																	/>
																</TableCell>
																<TableCell
																	whitespace-nowrap
																	style={cellStyle}
																	align="center"
																>
																	{row?.product?.name}
																</TableCell>
																<TableCell
																	whitespace-nowrap
																	style={cellStyle}
																	align="center"
																>
																	{row?.color?.name}
																</TableCell>
																<TableCell
																	whitespace-nowrap
																	style={cellStyle}
																	align="center"
																>
																	{row?.size?.name}
																</TableCell>

																<TableCell
																	whitespace-nowrap
																	style={cellStyle}
																	align="center"
																>
																	{editableRowIds[row.id] ? (
																		<Controller
																			name={`unit_quantity${row.id}`}
																			control={control}
																			render={({ field }) => {
																				return (
																					<TextField
																						{...field}
																						size="small"
																						error={!!errors.unit_quantity}
																						helperText={
																							errors?.product_guantity
																								?.message
																						}
																						id={`unit_quantity${row.id}`}
																						required
																						variant="outlined"
																						InputLabelProps={
																							field.value && {
																								shrink: true
																							}
																						}
																						fullWidth
																					/>
																				);
																			}}
																		/>
																	) : (
																		row.quantity
																	)}
																</TableCell>
																<TableCell
																	whitespace-nowrap
																	style={cellStyle}
																	align="center"
																>
																	{row.price}
																</TableCell>
																<TableCell
																	whitespace-nowrap
																	style={cellStyle}
																	align="center"
																>
																	{row.subtotal}
																</TableCell>
																<TableCell
																	whitespace-nowrap
																	style={cellStyle}
																	align="center"
																>
																	<div>
																		{editableRowIds[row.id] ? (
																			<DoneOutlineIcon
																				style={{ color: 'green' }}
																				className="cursor-pointer"
																				onClick={e => {
																					setEditableRowIds({
																						...editableRowIds,
																						[row.id]: false
																					});
																					updateProductItem(row);
																					console.log(
																						'rowData',
																						watch(`unit_quantity${row.id}`)
																					);
																				}}
																			/>
																		) : (
																			<EditIcon
																				style={{ color: 'green' }}
																				className="cursor-pointer"
																				onClick={e => {
																					setEditableRowIds({
																						...editableRowIds,
																						[row.id]: true
																					});

																					reset({
																						...formValues,
																						[`unit_quantity${row.id}`]:
																							row.quantity
																					});
																				}}
																			/>
																		)}{' '}
																	</div>
																</TableCell>
															</TableRow>
														);
													})}
												</TableBody>
											</Table>
										</TableContainer>
									</Div>
								</Grid>
							</Grid>
						</Div>

						<Div bg={PrimaryBg} h="1" />
					</Grid>
				</Grid>
			)}
		</div>
	);
}

export default SellReturnForm;
const btn = styled(({ color, ...other }) => <Button variant="outlined" {...other} />)({});
const Btn = styled(btn)(props => ({
	borderRadius: '3px',
	fontWeight: 'bold',
	borderWidth: '2px',
	height: props.h ? props.h : '40px',
	background: props.bg
}));

const Text = styled('b')(props => ({
	color: props.color === 'dark' ? 'black' : 'white',
	fontSize: props.fs ? props.fs : '15px',
	fontWeight: props.bold ? 'bold' : '500'
}));

const Input = styled('div')(props => ({
	background: props.bg,
	height: props.h ? props.h : '35px',
	width: props.size === 'large' ? '100%' : props.size === 'medium' ? '110px' : props.size === 'small' && '70px',
	margin: '10px',
	borderRadius: '2px',
	fontWeight: 'bold',
	color: props.color,
	hover: 'white'
}));

const Div = styled('div')(props => ({
	background: props.bg,
	height: props.h === '1' ? '25px' : props.h === '2' ? '35px' : props.h === 3 ? '50px' : props.h && props.h,
	width: props.w ? props.w : '100%',
	display: 'flex',
	justifyContent: props.justCont ? props.justCont : 'space-between',
	alignItems: props.alignItems ? props.alignItems : 'center',
	padding: props.pd === '1' ? '10px' : props.pd === '2' ? '20px' : props.pd && props.pd,
	flexWrap: props.wrap && 'wrap',
	border: props.border && '2px solid gray',
	borderTop: `4px solid ${props.top}`,
	borderLeft: `4px solid ${props.left}`,
	minHeight: props.minHeight
}));
