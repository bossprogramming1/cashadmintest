import _ from '@lodash';
import Button from '@material-ui/core/Button';
import Icon from '@material-ui/core/Icon';
import { useTheme } from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';
import { motion } from 'framer-motion';
import React from 'react';
import { useFormContext } from 'react-hook-form';
import { useDispatch } from 'react-redux';
import { Link, useHistory, useParams } from 'react-router-dom';
import { removeSellReturn, saveSellReturn, updateSellReturn } from '../store/sellReturnSlice';

const NewSellReturnHeader = () => {
	const dispatch = useDispatch();
	const methods = useFormContext();
	const { formState, watch, getValues } = methods;
	const { isValid, dirtyFields } = formState;
	const name = watch('name');
	const theme = useTheme();
	const history = useHistory();

	const routeParams = useParams();

	const handleDelete = localStorage.getItem('sellreturnEvent');

	function handleSaveSellReturn() {
		dispatch(saveSellReturn(getValues())).then(res => {
			if (res.payload) {
				localStorage.setItem('sellreturnAlert', 'saveSellReturn');
				history.push('/apps/sellreturn-management/sellreturn');
			}
		});
	}

	function handleUpdateSellReturn() {
		const data = getValues();
		const modifiedItems = data?.sell_return_items?.map(item => {
			// Extract only the 'id' property from the 'color' object
			const { color, product, size, ...rest } = item;
			return {
				...rest,
				color: color?.id || null,
				product: product?.id,
				size: size?.id || null
			};
		});

		// Update the data object with the modified sell_return_items
		const updatedData = {
			...data,
			sell_return_items: modifiedItems
		};
		dispatch(updateSellReturn(updatedData)).then(res => {
			if (res.payload) {
				localStorage.setItem('sellreturnAlert', 'updateSellReturn');
				history.push('/apps/sellreturn-management/sellreturn');
			}
		});
	}

	function handleRemoveSellReturn() {
		dispatch(removeSellReturn(getValues())).then(res => {
			if (res.payload) {
				localStorage.setItem('sellreturnAlert', 'deleteSellReturn');
				localStorage.removeItem('sellreturnEvent');
				history.push('/apps/sellreturn-management/sellreturn');
			}
		});
	}

	function handleCancel() {
		history.push('/apps/sellreturn-management/sellreturn');
	}

	// if (loading) {
	//     return <FuseLoading />;
	// }

	return (
		<div className="flex flex-1 w-full items-center justify-between">
			<div className="flex flex-col items-start max-w-full min-w-0">
				<motion.div initial={{ x: 20, opacity: 0 }} animate={{ x: 0, opacity: 1, transition: { delay: 0.3 } }}>
					<Typography
						className="flex items-center sm:mb-2"
						component={Link}
						role="button"
						to="/apps/sellreturn-management/sellreturn"
						color="inherit"
					>
						<Icon className="text-20">{theme.direction === 'ltr' ? 'arrow_back' : 'arrow_forward'}</Icon>
						<span className="hidden sm:flex mx-4 font-medium">SellReturns</span>
					</Typography>
				</motion.div>

				<div className="flex items-center max-w-full">
					<motion.div
						className="hidden sm:flex"
						initial={{ scale: 0 }}
						animate={{ scale: 1, transition: { delay: 0.3 } }}
					/>
					<div className="flex flex-col min-w-0 mx-8 sm:mc-16">
						<motion.div initial={{ x: -20 }} animate={{ x: 0, transition: { delay: 0.3 } }}>
							<Typography className="text-16 sm:text-20 truncate font-semibold">
								{name || 'Return Sell Item'}
							</Typography>
							<Typography variant="caption" className="font-medium">
								Sell Returns Detail
							</Typography>
						</motion.div>
					</div>
				</div>
			</div>
			<motion.div
				className="flex"
				initial={{ opacity: 0, x: 20 }}
				animate={{ opacity: 1, x: 0, transition: { delay: 0.3 } }}
			>
				{handleDelete === 'Delete' && (
					<Typography className="mt-6" variant="subtitle2">
						Do you want to remove this Sell Return?
					</Typography>
				)}
				{handleDelete === 'Delete' && routeParams.sellreturnId !== 'new' && (
					<Button
						className="whitespace-nowrap mx-4"
						variant="contained"
						color="secondary"
						onClick={handleRemoveSellReturn}
						startIcon={<Icon className="hidden sm:flex">delete</Icon>}
						style={{ backgroundColor: '#ea5b78', color: 'white' }}
					>
						Remove
					</Button>
				)}
				{routeParams.sellreturnId === 'new' && (
					<Button
						className="whitespace-nowrap mx-4"
						variant="contained"
						color="secondary"
						// disabled={_.isEmpty(dirtyFields) || !isValid}
						onClick={handleSaveSellReturn}
					>
						Return Purcahse
					</Button>
				)}
				{handleDelete !== 'Delete' && routeParams?.sellreturnName && (
					<Button
						className="whitespace-nowrap mx-4"
						color="secondary"
						variant="contained"
						//disabled={_.isEmpty(dirtyFields) || !isValid}
						style={{ backgroundColor: '#4dc08e', color: 'white' }}
						onClick={handleUpdateSellReturn}
					>
						Update
					</Button>
				)}
				<Button
					className="whitespace-nowrap mx-4"
					variant="contained"
					style={{ backgroundColor: '#FFAA4C', color: 'white' }}
					onClick={handleCancel}
				>
					Cancel
				</Button>
			</motion.div>
		</div>
	);
};

export default NewSellReturnHeader;
