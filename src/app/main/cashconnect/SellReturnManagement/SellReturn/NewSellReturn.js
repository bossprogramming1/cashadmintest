import FusePageCarded from '@fuse/core/FusePageCarded';
import { useDeepCompareEffect } from '@fuse/hooks';
import { yupResolver } from '@hookform/resolvers/yup';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';
import {
	SELLRETURN_CREATE,
	SELLRETURN_DELETE,
	SELLRETURN_DETAILS,
	SELLRETURN_UPDATE
} from 'app/constant/permission/permission';
import { getUserPermissions } from 'app/store/dataSlice';
import withReducer from 'app/store/withReducer';
import { motion } from 'framer-motion';
import React, { useEffect, useState } from 'react';
import { FormProvider, useForm } from 'react-hook-form';
import { useDispatch, useSelector } from 'react-redux';
import { Link, useParams } from 'react-router-dom';
import * as yup from 'yup';
import PagenotFound from '../../Pagenotfound/PagenotFound';
import { getSellReturn, newSellReturn, resetSellReturn } from '../store/sellReturnSlice';
import reducer from '../store/index';
import NewSellReturnHeader from './NewSellReturnHeader';
import SellReturnForm from './SellReturnForm';

/**
 * Form Validation Schema
 */
const schema = yup.object().shape({
	// sell_return_items: yup.string().required('Return item is required')
});

const SellReturn = () => {
	const dispatch = useDispatch();
	const sellreturn = useSelector(({ sellreturnsManagement }) => sellreturnsManagement.sellreturn);

	const [noSellReturn, setNoSellReturn] = useState(false);
	const methods = useForm({
		mode: 'onChange',
		defaultValues: {},
		resolver: yupResolver(schema)
	});
	const routeParams = useParams();
	const UserPermissions = useSelector(state => state.data.UserPermissions);
	useEffect(() => {
		dispatch(getUserPermissions());
	}, []);
	const { reset, watch } = methods;
	const { sellreturnId } = routeParams;
	useDeepCompareEffect(() => {
		function updateSellReturnState() {
			if (sellreturnId === 'new') {
				localStorage.removeItem('event');
				/**
				 * Create New User data
				 */
				dispatch(newSellReturn());
			} else {
				/**
				 * Get User data
				 */
				dispatch(getSellReturn(sellreturnId)).then(action => {
					/**
					 * If the requested product is not exist show message
					 */
					if (!action.payload) {
						setNoSellReturn(true);
					}
				});
			}
		}

		updateSellReturnState();
	}, [dispatch, routeParams]);

	useEffect(() => {}, []);

	useEffect(() => {
		if (!sellreturn) {
			return;
		}
		/**
		 * Reset the form on sellreturn state changes
		 */
		if (sellreturnId === 'new') {
			reset(sellreturn);
		} else {
			reset({
				invoice_no: sellreturn?.invoice_no,
				id: sellreturn?.id,
				sell_return_items: [sellreturn]
			});
		}
	}, [sellreturn, reset]);

	useEffect(() => {
		return () => {
			/**
			 * Reset SellReturn on component unload
			 */
			dispatch(resetSellReturn());
			setNoSellReturn(false);
		};
	}, [dispatch]);

	/**
	 * Show Message if the requested products is not exists
	 */
	if (noSellReturn) {
		return (
			<motion.div
				initial={{ opacity: 0 }}
				animate={{ opacity: 1, transition: { delay: 0.1 } }}
				className="flex flex-col flex-1 items-center justify-center h-full"
			>
				<Typography color="textSecondary" variant="h5">
					There is no such sellreturn!
				</Typography>
				<Button
					className="mt-24"
					component={Link}
					variant="outlined"
					to="/apps/e-commerce/products"
					color="inherit"
				>
					Go to Sell Return Page
				</Button>
			</motion.div>
		);
	}

	return (
		<FormProvider {...methods}>
			{UserPermissions.includes(SELLRETURN_CREATE) ||
			UserPermissions.includes(SELLRETURN_UPDATE) ||
			UserPermissions.includes(SELLRETURN_DELETE) ||
			UserPermissions.includes(SELLRETURN_DETAILS) ? (
				<FusePageCarded
					classes={{
						toolbar: 'p-0',
						header: 'min-h-80 h-80'
					}}
					header={<NewSellReturnHeader />}
					content={
						<div className="p-16 sm:p-24">
							<SellReturnForm />
						</div>
					}
					innerScroll
				/>
			) : (
				<PagenotFound />
			)}
		</FormProvider>
	);
};
export default withReducer('sellreturnsManagement', reducer)(SellReturn);
