import { combineReducers } from '@reduxjs/toolkit';
import color from './colorSlice';
import colors from './colorsSlice';

const reducer = combineReducers({
	color,
	colors
});

export default reducer;
