import { createAsyncThunk, createEntityAdapter, createSlice } from '@reduxjs/toolkit';
import axios from 'axios';
import { DELETE_COLOR, GET_COLORS } from '../../../../constant/constants';

export const getColors = createAsyncThunk('colorManagement/colors/getColors', async parameter => {
	const { page, size } = parameter;

	axios.defaults.headers.common['Content-type'] = 'application/json';
	axios.defaults.headers.common.Authorization = localStorage.getItem('jwt_access_token');

	const response = axios.get(GET_COLORS, { params: { page, size } });
	const data = await response;

	sessionStorage.setItem('colors_total_elements', data.data.total_elements);
	sessionStorage.setItem('colors_total_pages', data.data.total_pages);
	delete axios.defaults.headers.common['Content-type'];
	delete axios.defaults.headers.common.Authorization;

	return data.data.colors;
});

export const removeColors = createAsyncThunk(
	'colorManagement/colors/removeColors',
	async (colorIds, { dispatch, getState }) => {
		const authTOKEN = {
			headers: {
				'Content-type': 'application/json',
				Authorization: localStorage.getItem('jwt_access_token')
			}
		};
		await axios.delete(`${DELETE_COLOR}`, { colorIds }, authTOKEN);

		return colorIds;
	}
);

const colorsAdapter = createEntityAdapter({});

export const { selectAll: selectColors, selectById: selectColorById } = colorsAdapter.getSelectors(
	state => state.colorsManagement.colors
);

const colorsSlice = createSlice({
	name: 'colorManagement/colors',
	initialState: colorsAdapter.getInitialState({
		searchText: ''
	}),
	reducers: {
		setColorsSearchText: {
			reducer: (state, action) => {
				state.searchText = action.payload;
			},
			prepare: event => ({ payload: event.target.value || '' })
		}
	},
	extraReducers: {
		[getColors.fulfilled]: colorsAdapter.setAll
	}
});

export const { setData, setColorsSearchText } = colorsSlice.actions;
export default colorsSlice.reducer;
