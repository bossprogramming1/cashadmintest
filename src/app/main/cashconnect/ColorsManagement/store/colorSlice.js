import { createAsyncThunk, createSlice } from '@reduxjs/toolkit';
import axios from 'axios';
import { CREATE_COLOR, DELETE_COLOR, GET_COLORID, UPDATE_COLOR } from '../../../../constant/constants';

export const getColor = createAsyncThunk('colorManagement/color/getColor', async (params, { rejectWithValue }) => {
	const authTOKEN = {
		headers: {
			'Content-type': 'application/json',
			Authorization: localStorage.getItem('jwt_access_token')
		}
	};

	try {
		const response = await axios.get(`${GET_COLORID}${params}`, authTOKEN);
		const data = await response.data;
		return data === undefined ? null : data;
	} catch (err) {
		return rejectWithValue(params);
	}
});

export const removeColor = createAsyncThunk('colorManagement/color/removeColor', async val => {
	const authTOKEN = {
		headers: {
			'Content-type': 'application/json',
			Authorization: localStorage.getItem('jwt_access_token')
		}
	};

	const colorId = val.id;
	const response = await axios.delete(`${DELETE_COLOR}${colorId}`, authTOKEN);
	return response;
});

export const updateColor = createAsyncThunk(
	'colorManagement/color/updateColor',
	async (colorData, { dispatch, getState }) => {
		const { color } = getState().colorsManagement;

		const authTOKEN = {
			headers: {
				'Content-type': 'application/json',
				Authorization: localStorage.getItem('jwt_access_token')
			}
		};
		const response = await axios.put(`${UPDATE_COLOR}${color.id}`, colorData, authTOKEN);
		return response;
	}
);

export const saveColor = createAsyncThunk('colorManagement/color/saveColor', async colorData => {
	const authTOKEN = {
		headers: {
			'Content-type': 'application/json',
			Authorization: localStorage.getItem('jwt_access_token')
		}
	};
	const response = await axios.post(`${CREATE_COLOR}`, colorData, authTOKEN);
	return response;
});

const colorSlice = createSlice({
	name: 'colorManagement/color',
	initialState: null,
	reducers: {
		resetColor: () => null,
		newColor: {
			reducer: (state, action) => action.payload,
			prepare: event => ({
				payload: {}
			})
		}
	},
	extraReducers: {
		[getColor.fulfilled]: (state, action) => action.payload,
		[saveColor.fulfilled]: (state, action) => action.payload,
		[removeColor.fulfilled]: (state, action) => action.payload,
		[updateColor.fulfilled]: (state, action) => action.payload
	}
});

export const { newColor, resetColor } = colorSlice.actions;

export default colorSlice.reducer;
