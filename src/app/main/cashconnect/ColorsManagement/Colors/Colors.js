import FusePageCarded from '@fuse/core/FusePageCarded';
import { COLOR_LIST } from 'app/constant/permission/permission';
import { getUserPermissions } from 'app/store/dataSlice';
import withReducer from 'app/store/withReducer';
import React, { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import PagenotFound from '../../Pagenotfound/PagenotFound';
import reducer from '../store/index';
import ColorsHeader from './ColorsHeader';
import ColorsTable from './ColorsTable';

const Colors = () => {
	const UserPermissions = useSelector(state => state.data.UserPermissions);
	useEffect(() => {
		dispatch(getUserPermissions());
	}, []);
	const dispatch = useDispatch();
	return (
		<FusePageCarded
			classes={{
				content: 'flex',
				contentCard: 'overflow-hidden',
				header: 'min-h-74 h-64'
			}}
			header={UserPermissions.includes(COLOR_LIST) && <ColorsHeader />}
			content={UserPermissions.includes(COLOR_LIST) ? <ColorsTable /> : <PagenotFound />}
			innerScroll
		/>
	);
};
export default withReducer('colorsManagement', reducer)(Colors);
