import FusePageCarded from '@fuse/core/FusePageCarded';
import { useDeepCompareEffect } from '@fuse/hooks';
import { yupResolver } from '@hookform/resolvers/yup';
import { Button, Typography } from '@material-ui/core';
import { COLOR_CREATE, COLOR_DELETE, COLOR_DETAILS, COLOR_UPDATE } from 'app/constant/permission/permission';
import { getUserPermissions } from 'app/store/dataSlice';
import withReducer from 'app/store/withReducer';
import { motion } from 'framer-motion';
import React, { useEffect, useState } from 'react';
import { FormProvider, useForm } from 'react-hook-form';
import { useDispatch, useSelector } from 'react-redux';
import { Link, useParams } from 'react-router-dom';
import * as yup from 'yup';
import PagenotFound from '../../Pagenotfound/PagenotFound';
import { getColor, newColor, resetColor } from '../store/colorSlice';
import reducer from '../store/index';
import ColorForm from './ColorForm';
import NewColorHeader from './NewColorHeader';

/**
 * Form Validation Schema
 */
const schema = yup.object().shape({
	name: yup.string().required('Name is required')
});

const Color = () => {
	const dispatch = useDispatch();
	const color = useSelector(({ colorsManagement }) => colorsManagement.color);

	const [noColor, setNoColor] = useState(false);
	const methods = useForm({
		mode: 'onChange',
		defaultValues: {},
		resolver: yupResolver(schema)
	});
	const routeParams = useParams();
	const UserPermissions = useSelector(state => state.data.UserPermissions);
	useEffect(() => {
		dispatch(getUserPermissions());
	}, []);
	const { reset, watch } = methods;
	const form = watch();

	useDeepCompareEffect(() => {
		function updateColorState() {
			const { colorId } = routeParams;

			if (colorId === 'new') {
				localStorage.removeItem('event');
				/**
				 * Create New User data
				 */
				dispatch(newColor());
			} else {
				/**
				 * Get User data
				 */

				dispatch(getColor(colorId)).then(action => {
					/**
					 * If the requested product is not exist show message
					 */
					if (!action.payload) {
						setNoColor(true);
					}
				});
			}
		}

		updateColorState();
	}, [dispatch, routeParams]);

	useEffect(() => {}, []);

	useEffect(() => {
		if (!color) {
			return;
		}
		/**
		 * Reset the form on color state changes
		 */
		reset(color);
	}, [color, reset]);

	useEffect(() => {
		return () => {
			/**
			 * Reset Color on component unload
			 */
			dispatch(resetColor());
			setNoColor(false);
		};
	}, [dispatch]);

	/**
	 * Show Message if the requested products is not exists
	 */
	if (noColor) {
		return (
			<motion.div
				initial={{ opacolor: 0 }}
				animate={{ opacolor: 1, transition: { delay: 0.1 } }}
				className="flex flex-col flex-1 items-center justify-center h-full"
			>
				<Typography color="textSecondary" variant="h5">
					There is no such color!
				</Typography>
				<Button
					className="mt-24"
					component={Link}
					variant="outlined"
					to="/apps/e-commerce/products"
					color="inherit"
				>
					Go to Color Page
				</Button>
			</motion.div>
		);
	}

	return (
		<FormProvider {...methods}>
			{UserPermissions.includes(COLOR_CREATE) ||
			UserPermissions.includes(COLOR_UPDATE) ||
			UserPermissions.includes(COLOR_DELETE) ||
			UserPermissions.includes(COLOR_DETAILS) ? (
				<FusePageCarded
					classes={{
						toolbar: 'p-0',
						header: 'min-h-80 h-80'
					}}
					header={<NewColorHeader />}
					content={
						<div className="p-16 sm:p-24">
							<ColorForm />
						</div>
					}
					innerScroll
				/>
			) : (
				<PagenotFound />
			)}
		</FormProvider>
	);
};
export default withReducer('colorsManagement', reducer)(Color);
