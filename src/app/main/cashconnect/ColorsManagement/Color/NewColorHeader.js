import _ from '@lodash';
import Button from '@material-ui/core/Button';
import Icon from '@material-ui/core/Icon';
import { useTheme } from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';
import { motion } from 'framer-motion';
import React from 'react';
import { useFormContext } from 'react-hook-form';
import { useDispatch } from 'react-redux';
import { Link, useHistory, useParams } from 'react-router-dom';
import { removeColor, saveColor, updateColor } from '../store/colorSlice';

const NewColorHeader = () => {
	const dispatch = useDispatch();
	const methods = useFormContext();
	const { formState, watch, getValues } = methods;
	const { isValid, dirtyFields } = formState;
	const name = watch('name');
	const theme = useTheme();
	const history = useHistory();
	const routeParams = useParams();
	const handleDelete = localStorage.getItem('colorEvent');
	function handleSaveColor() {
		dispatch(saveColor(getValues())).then(res => {
			if (res.payload) {
				localStorage.setItem('colorAlert', 'saveColor');
				history.push('/apps/color-management/colors');
			}
		});
	}

	function handleUpdateColor() {
		dispatch(updateColor(getValues())).then(res => {
			if (res.payload) {
				localStorage.setItem('colorAlert', 'updateColor');
				history.push('/apps/color-management/colors');
			}
		});
	}

	function handleRemoveColor() {
		dispatch(removeColor(getValues())).then(res => {
			if (res.payload) {
				localStorage.setItem('colorAlert', 'deleteColor');
				localStorage.removeItem('colorEvent');
				history.push('/apps/color-management/colors');
			}
		});
	}

	function handleCancel() {
		history.push('/apps/color-management/colors');
	}

	return (
		<div className="flex flex-1 w-full items-center justify-between">
			<div className="flex flex-col items-start max-w-full min-w-0">
				<motion.div
					initial={{ x: 20, opacolor: 0 }}
					animate={{ x: 0, opacolor: 1, transition: { delay: 0.3 } }}
				>
					<Typography
						className="flex items-center sm:mb-2"
						component={Link}
						role="button"
						to="/apps/color-management/colors"
						color="inherit"
					>
						<Icon className="text-20">{theme.direction === 'ltr' ? 'arrow_back' : 'arrow_forward'}</Icon>
						<span className="hidden sm:flex mx-4 font-medium">Districts</span>
					</Typography>
				</motion.div>

				<div className="flex items-center max-w-full">
					<motion.div
						className="hidden sm:flex"
						initial={{ scale: 0 }}
						animate={{ scale: 1, transition: { delay: 0.3 } }}
					/>
					<div className="flex flex-col min-w-0 mx-8 sm:mc-16">
						<motion.div initial={{ x: -20 }} animate={{ x: 0, transition: { delay: 0.3 } }}>
							<Typography className="text-16 sm:text-20 truncate font-semibold">
								{name || 'Create New District'}
							</Typography>
							<Typography variant="caption" className="font-medium">
								Districts Detail
							</Typography>
						</motion.div>
					</div>
				</div>
			</div>
			<motion.div
				className="flex"
				initial={{ opacolor: 0, x: 20 }}
				animate={{ opacolor: 1, x: 0, transition: { delay: 0.3 } }}
			>
				{handleDelete === 'Delete' && (
					<Typography className="mt-6" variant="subtitle2">
						Do you want to remove this District?
					</Typography>
				)}
				{handleDelete === 'Delete' && routeParams.colorId !== 'new' && (
					<Button
						className="whitespace-nowrap mx-4"
						variant="contained"
						color="secondary"
						onClick={handleRemoveColor}
						startIcon={<Icon className="hidden sm:flex">delete</Icon>}
						style={{ backgroundColor: '#ea5b78', color: 'white' }}
					>
						Remove
					</Button>
				)}
				{routeParams.colorId === 'new' && (
					<Button
						className="whitespace-nowrap mx-4"
						variant="contained"
						color="secondary"
						disabled={_.isEmpty(dirtyFields) || !isValid}
						onClick={handleSaveColor}
					>
						Save
					</Button>
				)}
				{handleDelete !== 'Delete' && routeParams?.colorName && (
					<Button
						className="whitespace-nowrap mx-4"
						color="secondary"
						variant="contained"
						style={{ backgroundColor: '#4dc08e', color: 'white' }}
						onClick={handleUpdateColor}
					>
						Update
					</Button>
				)}
				<Button
					className="whitespace-nowrap mx-4"
					variant="contained"
					style={{ backgroundColor: '#FFAA4C', color: 'white' }}
					onClick={handleCancel}
				>
					Cancel
				</Button>
			</motion.div>
		</div>
	);
};

export default NewColorHeader;
