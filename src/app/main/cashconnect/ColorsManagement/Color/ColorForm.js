import _ from '@lodash';
import TextField from '@material-ui/core/TextField';
import { Autocomplete } from '@material-ui/lab';
import React, { useEffect } from 'react';
import { Controller, useFormContext } from 'react-hook-form';
import { useDispatch, useSelector } from 'react-redux';
import { useHistory, useParams } from 'react-router-dom';
import { getCountries } from '../../../../store/dataSlice';
import { saveColor, updateColor } from '../store/colorSlice';

function ColorForm(props) {
	const dispatch = useDispatch();
	const countries = useSelector(state => state.data.countries);
	const methods = useFormContext();
	const { control, formState, getValues } = methods;
	const { errors, isValid, dirtyFields } = formState;
	const routeParams = useParams();
	const history = useHistory();
	const handleDelete = localStorage.getItem('colorEvent');

	useEffect(() => {
		dispatch(getCountries());
	}, []);

	function handleSaveColor() {
		dispatch(saveColor(getValues())).then(res => {
			if (res.payload) {
				localStorage.setItem('colorAlert', 'saveColor');
				history.push('/apps/color-management/colors');
			}
		});
	}

	function handleUpdateColor() {
		dispatch(updateColor(getValues())).then(res => {
			if (res.payload) {
				localStorage.setItem('colorAlert', 'updateColor');
				history.push('/apps/color-management/colors');
			}
		});
	}

	const handleSubmitOnKeyDownEnter = ev => {
		if (ev.key === 'Enter') {
			if (routeParams.colorId === 'new' && !(_.isEmpty(dirtyFields) || !isValid)) {
				handleSaveColor();
			} else if (handleDelete !== 'Delete' && routeParams?.colorName) {
				handleUpdateColor();
			}
		}
	};

	return (
		<div>
			<Controller
				name="name"
				control={control}
				render={({ field }) => {
					return (
						<TextField
							{...field}
							className="mt-8 mb-16"
							error={!!errors?.name}
							helperText={errors?.name?.message}
							label="Name"
							id="name"
							required
							variant="outlined"
							InputLabelProps={field.value && { shrink: true }}
							fullWidth
							onKeyDown={handleSubmitOnKeyDownEnter}
						/>
					);
				}}
			/>
		</div>
	);
}

export default ColorForm;
