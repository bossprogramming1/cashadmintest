import FusePageCarded from '@fuse/core/FusePageCarded';
import { getUserPermissions } from 'app/store/dataSlice';
import withReducer from 'app/store/withReducer';
import React, { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import UploadFilesHeader from './UploadFilesHeader';
import UploadFilesTable from './UploadFilesTable';

const UploadFiles = () => {
	return (
		<FusePageCarded
			classes={{
				// content: 'flex',
				contentCard: 'overflow-hidden',
				header: 'min-h-74 h-64'
			}}
			header={<UploadFilesHeader />}
			content={<UploadFilesTable />}
			innerScroll
		/>
	);
};
export default withReducer('uploadfilesManagement')(UploadFiles);
