import _ from '@lodash';
import { makeStyles } from '@material-ui/core/styles';
import TextField from '@material-ui/core/TextField';
import { Autocomplete } from '@material-ui/lab';
import React, { useEffect } from 'react';
import { Controller, useFormContext } from 'react-hook-form';
import { useDispatch, useSelector } from 'react-redux';
import { useHistory, useParams } from 'react-router-dom';
import { getLedgerAsPayhead } from 'app/store/dataSlice';
import { saveVouchertypeclass, updateVouchertypeclass } from '../store/vouchertypeclassSlice';

const useStyles = makeStyles(theme => ({
	hidden: {
		display: 'none'
	}
}));

function VouchertypeclassForm(props) {
	const userID = localStorage.getItem('user_id');
	const payheadTypes = useSelector(state => state.data.payheadTypes);
	const ledgersAsPayhead = useSelector(state => state.data.ledgersAsPayhead);
	const classes = useStyles(props);
	const methods = useFormContext();
	const { control, formState, getValues } = methods;
	const { errors, isValid, dirtyFields } = formState;
	const routeParams = useParams();
	const { vouchertypeclassId } = routeParams;
	const history = useHistory();
	const handleDelete = localStorage.getItem('vouchertypeclassEvent');

	const dispatch = useDispatch();

	useEffect(() => {
		dispatch(getLedgerAsPayhead());
	}, []);

	function handleSaveVouchertypeclass() {
		dispatch(saveVouchertypeclass(getValues())).then(res => {
			if (res.payload) {
				localStorage.setItem('vouchertypeclassAlert', 'saveVouchertypeclass');
				history.push('/apps/vouchertypeclass-management/vouchertypeclasss');
			}
		});
	}

	function handleUpdateVouchertypeclass() {
		dispatch(updateVouchertypeclass(getValues())).then(res => {
			if (res.payload) {
				localStorage.setItem('vouchertypeclassAlert', 'updateVouchertypeclass');
				history.push('/apps/vouchertypeclass-management/vouchertypeclasss');
			}
		});
	}

	const handleSubmitOnKeyDownEnter = ev => {
		if (ev.key === 'Enter') {
			if (routeParams.vouchertypeclassId === 'new' && !(_.isEmpty(dirtyFields) || !isValid)) {
				handleSaveVouchertypeclass();
			} else if (handleDelete !== 'Delete' && routeParams?.vouchertypeclassName) {
				handleUpdateVouchertypeclass();
			}
		}
	};

	return (
		<div>
			<Controller
				name="name"
				control={control}
				render={({ field }) => {
					return (
						<TextField
							{...field}
							className="mt-8 mb-16"
							error={!!errors?.name}
							helperText={errors?.name?.message}
							label="Voucher Type Class"
							id="name"
							required
							variant="outlined"
							InputLabelProps={field.value && { shrink: true }}
							fullWidth
							onKeyDown={handleSubmitOnKeyDownEnter}
						/>
					);
				}}
			/>
			<Controller
				name="ledger_account"
				control={control}
				render={({ field: { onChange, value, name } }) => (
					<Autocomplete
						className="mt-8 mb-16"
						freeSolo
						value={value ? ledgersAsPayhead.find(data => data.id === value) : null}
						options={ledgersAsPayhead}
						getOptionLabel={option => `${option?.name}`}
						onChange={(event, newValue) => {
							onChange(newValue?.id);
						}}
						renderInput={params => (
							<TextField
								{...params}
								placeholder="Select Ledger"
								label="Ledger"
								error={!!errors.ledger_account}
								required
								helperText={errors?.ledger_account?.message}
								variant="outlined"
								InputLabelProps={{
									shrink: true
								}}
								// onKeyDown={handleSubmitOnKeyDownEnter}
							/>
						)}
					/>
				)}
			/>

			{/* <Controller
				name="city"
				control={control}
				render={({ field: { onChange, value, name } }) => (
					<Autocomplete
						className="mt-8 mb-16"
						freeSolo
						value={value ? citys.find(data => data.id === value) : null}
						options={citys}
						getOptionLabel={option => `${option?.name}`}
						onChange={(event, newValue) => {
							onChange(newValue?.id);
						}}
						renderInput={params => (
							<TextField
								{...params}
								placeholder="Select City"
								label="City"
								error={!!errors.city}
								required
								helperText={errors?.city?.message}
								variant="outlined"
								autoFocus
								InputLabelProps={{
									shrink: true
								}}
								// onKeyDown={handleSubmitOnKeyDownEnter}
							/>
						)}
					/>
				)}
			/> */}
		</div>
	);
}

export default VouchertypeclassForm;
