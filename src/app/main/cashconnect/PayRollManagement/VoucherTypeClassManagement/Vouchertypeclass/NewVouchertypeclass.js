import FusePageCarded from '@fuse/core/FusePageCarded';
import { useDeepCompareEffect } from '@fuse/hooks';
import { yupResolver } from '@hookform/resolvers/yup';
import { Button, Typography } from '@material-ui/core';
import withReducer from 'app/store/withReducer';
import { motion } from 'framer-motion';
import React, { useEffect, useState } from 'react';
import { FormProvider, useForm } from 'react-hook-form';
import { useDispatch, useSelector } from 'react-redux';
import { Link, useParams } from 'react-router-dom';
import * as yup from 'yup';
import reducer from '../store/index';
import { getVouchertypeclass, newVouchertypeclass, resetVouchertypeclass } from '../store/vouchertypeclassSlice';
import VouchertypeclassForm from './VouchertypeclassForm';
import NewVouchertypeclassHeader from './NewVouchertypeclassHeader';

/**
 * Form Validation Schema
 */
const schema = yup.object().shape({
	name: yup.string().required('Name is required')
});

const Vouchertypeclass = () => {
	const dispatch = useDispatch();
	const vouchertypeclass = useSelector(
		({ vouchertypeclasssManagement }) => vouchertypeclasssManagement.vouchertypeclass
	);

	const [noVouchertypeclass, setNoVouchertypeclass] = useState(false);
	const methods = useForm({
		mode: 'onChange',
		defaultValues: {},
		resolver: yupResolver(schema)
	});
	const routeParams = useParams();

	const { reset } = methods;

	useDeepCompareEffect(() => {
		function updateVouchertypeclassState() {
			const { vouchertypeclassId } = routeParams;

			if (vouchertypeclassId === 'new') {
				localStorage.removeItem('event');
				/**
				 * Create New User data
				 */
				dispatch(newVouchertypeclass());
			} else {
				/**
				 * Get User data
				 */

				dispatch(getVouchertypeclass(vouchertypeclassId)).then(action => {
					/**
					 * If the requested product is not exist show message
					 */
					if (!action.payload) {
						setNoVouchertypeclass(true);
					}
				});
			}
		}

		updateVouchertypeclassState();
	}, [dispatch, routeParams]);

	useEffect(() => {}, []);

	useEffect(() => {
		if (!vouchertypeclass) {
			return;
		}
		/**
		 * Reset the form on vouchertypeclass state changes
		 */
		reset(vouchertypeclass);
	}, [vouchertypeclass, reset]);

	useEffect(() => {
		return () => {
			/**
			 * Reset Vouchertypeclass on component unload
			 */
			dispatch(resetVouchertypeclass());
			setNoVouchertypeclass(false);
		};
	}, [dispatch]);

	/**
	 * Show Message if the requested products is not exists
	 */
	if (noVouchertypeclass) {
		return (
			<motion.div
				initial={{ opacity: 0 }}
				animate={{ opacity: 1, transition: { delay: 0.1 } }}
				className="flex flex-col flex-1 items-center justify-center h-full"
			>
				<Typography color="textSecondary" variant="h5">
					There is no such vouchertypeclass!
				</Typography>
				<Button
					className="mt-24"
					component={Link}
					variant="outlined"
					to="/apps/e-commerce/products"
					color="inherit"
				>
					Go to Voucher type class Page
				</Button>
			</motion.div>
		);
	}

	return (
		<FormProvider {...methods}>
			<FusePageCarded
				classes={{
					toolbar: 'p-0',
					header: 'min-h-80 h-80'
				}}
				header={<NewVouchertypeclassHeader />}
				content={
					<div className="p-16 sm:p-24">
						<VouchertypeclassForm />
					</div>
				}
				innerScroll
			/>
		</FormProvider>
	);
};
export default withReducer('vouchertypeclasssManagement', reducer)(Vouchertypeclass);
