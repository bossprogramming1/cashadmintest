import { createAsyncThunk, createSlice } from '@reduxjs/toolkit';
import {
	CREATE_VOUCHER_TYPE_CLASS,
	DELETE_VOUCHER_TYPE_CLASS,
	GET_VOUCHER_TYPE_CLASSID,
	UPDATE_VOUCHER_TYPE_CLASS
} from 'app/constant/constants';
import axios from 'axios';

export const getVouchertypeclass = createAsyncThunk(
	'vouchertypeclassManagement/vouchertypeclass/getVouchertypeclass',
	async (params, { rejectWithValue }) => {
		const authTOKEN = {
			headers: {
				'Content-type': 'application/json',
				Authorization: localStorage.getItem('jwt_access_token')
			}
		};

		try {
			const response = await axios.get(`${GET_VOUCHER_TYPE_CLASSID}${params}`, authTOKEN);
			const data = await response.data;
			return data === undefined ? null : data;
		} catch (err) {
			return rejectWithValue(params);
		}
	}
);

export const removeVouchertypeclass = createAsyncThunk(
	'vouchertypeclassManagement/vouchertypeclass/removeVouchertypeclass',
	async val => {
		const authTOKEN = {
			headers: {
				'Content-type': 'application/json',
				Authorization: localStorage.getItem('jwt_access_token')
			}
		};

		const vouchertypeclassId = val.id;
		const response = await axios.delete(`${DELETE_VOUCHER_TYPE_CLASS}${vouchertypeclassId}`, authTOKEN);
		return response;
	}
);

export const updateVouchertypeclass = createAsyncThunk(
	'vouchertypeclassManagement/vouchertypeclass/updateVouchertypeclass',
	async (vouchertypeclassData, { dispatch, getState }) => {
		const { vouchertypeclass } = getState().vouchertypeclasssManagement;

		const authTOKEN = {
			headers: {
				'Content-type': 'application/json',
				Authorization: localStorage.getItem('jwt_access_token')
			}
		};
		const response = await axios.put(
			`${UPDATE_VOUCHER_TYPE_CLASS}${vouchertypeclass.id}`,
			vouchertypeclassData,
			authTOKEN
		);
		return response;
	}
);

export const saveVouchertypeclass = createAsyncThunk(
	'vouchertypeclassManagement/vouchertypeclass/saveVouchertypeclass',
	async vouchertypeclassData => {
		const authTOKEN = {
			headers: {
				'Content-type': 'application/json',
				Authorization: localStorage.getItem('jwt_access_token')
			}
		};
		const response = await axios.post(`${CREATE_VOUCHER_TYPE_CLASS}`, vouchertypeclassData, authTOKEN);
		return response;
	}
);

const vouchertypeclassSlice = createSlice({
	name: 'vouchertypeclassManagement/vouchertypeclass',
	initialState: null,
	reducers: {
		resetVouchertypeclass: () => null,
		newVouchertypeclass: {
			reducer: (state, action) => action.payload,
			prepare: event => ({
				payload: {
					name: ''
				}
			})
		}
	},
	extraReducers: {
		[getVouchertypeclass.fulfilled]: (state, action) => action.payload,
		[saveVouchertypeclass.fulfilled]: (state, action) => action.payload,
		[removeVouchertypeclass.fulfilled]: (state, action) => action.payload,
		[updateVouchertypeclass.fulfilled]: (state, action) => action.payload
	}
});

export const { newVouchertypeclass, resetVouchertypeclass } = vouchertypeclassSlice.actions;

export default vouchertypeclassSlice.reducer;
