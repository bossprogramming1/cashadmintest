import { createAsyncThunk, createEntityAdapter, createSlice } from '@reduxjs/toolkit';
import { DELETE_VOUCHER_TYPE_CLASS, GET_VOUCHER_TYPE_CLASSS } from 'app/constant/constants';
import axios from 'axios';

export const getVouchertypeclasss = createAsyncThunk(
	'vouchertypeclassManagement/vouchertypeclasss/getVouchertypeclasss',
	async parameter => {
		const { page, size } = parameter;
		axios.defaults.headers.common['Content-type'] = 'application/json';
		axios.defaults.headers.common.Authorization = localStorage.getItem('jwt_access_token');

		const response = axios.get(GET_VOUCHER_TYPE_CLASSS, { params: { page, size } });
		const data = await response;

		sessionStorage.setItem('vouchertypeclasss_total_elements', data.data.total_elements);
		sessionStorage.setItem('vouchertypeclasss_total_pages', data.data.total_pages);
		delete axios.defaults.headers.common['Content-type'];
		delete axios.defaults.headers.common.Authorization;

		return data.data.voucher_type_classes;
	}
);

export const removeVouchertypeclasss = createAsyncThunk(
	'vouchertypeclassManagement/vouchertypeclasss/removeVouchertypeclasss',
	async (vouchertypeclassIds, { dispatch, getState }) => {
		const authTOKEN = {
			headers: {
				'Content-type': 'application/json',
				Authorization: localStorage.getItem('jwt_access_token')
			}
		};
		await axios.delete(`${DELETE_VOUCHER_TYPE_CLASS}`, { vouchertypeclassIds }, authTOKEN);

		return vouchertypeclassIds;
	}
);

const vouchertypeclasssAdapter = createEntityAdapter({});

export const { selectAll: selectVouchertypeclasss, selectById: selectVouchertypeclassById } =
	vouchertypeclasssAdapter.getSelectors(state => state.vouchertypeclasssManagement.vouchertypeclasss);

const vouchertypeclasssSlice = createSlice({
	name: 'vouchertypeclassManagement/vouchertypeclasss',
	initialState: vouchertypeclasssAdapter.getInitialState({
		searchText: ''
	}),
	reducers: {
		setVouchertypeclasssSearchText: {
			reducer: (state, action) => {
				state.searchText = action.payload;
			},
			prepare: event => ({ payload: event.target.value || '' })
		}
	},
	extraReducers: {
		[getVouchertypeclasss.fulfilled]: vouchertypeclasssAdapter.setAll
	}
});

export const { setData, setVouchertypeclasssSearchText } = vouchertypeclasssSlice.actions;
export default vouchertypeclasssSlice.reducer;
