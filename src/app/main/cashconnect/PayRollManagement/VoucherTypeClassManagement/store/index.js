import { combineReducers } from '@reduxjs/toolkit';
import vouchertypeclass from './vouchertypeclassSlice';
import vouchertypeclasss from './vouchertypeclasssSlice';

const reducer = combineReducers({
	vouchertypeclass,
	vouchertypeclasss
});

export default reducer;
