import { combineReducers } from '@reduxjs/toolkit';
import application from './applicationSlice';
import applications from './applicationsSlice';

const reducer = combineReducers({
	application,
	applications
});

export default reducer;
