import { createAsyncThunk, createSlice } from '@reduxjs/toolkit';
import jsonToFormData from 'app/@helpers/jsonToFormData';
import { CREATE_APPLICATION, DELETE_APPLICATION, GET_APPLICATIONID, UPDATE_APPLICATION } from 'app/constant/constants';
import axios from 'axios';

export const getApplication = createAsyncThunk(
	'applicationManagement/application/getApplication',
	async (params, { rejectWithValue }) => {
		const authTOKEN = {
			headers: {
				'Content-type': 'application/json',
				Authorization: localStorage.getItem('jwt_access_token')
			}
		};

		try {
			const response = await axios.get(`${GET_APPLICATIONID}${params}`, authTOKEN);
			const data = await response.data;
			return data === undefined ? null : data;
		} catch (err) {
			return rejectWithValue(params);
		}
	}
);

export const removeApplication = createAsyncThunk('applicationManagement/application/removeApplication', async val => {
	const authTOKEN = {
		headers: {
			'Content-type': 'application/json',
			Authorization: localStorage.getItem('jwt_access_token')
		}
	};

	const applicationId = val.id;
	const response = await axios.delete(`${DELETE_APPLICATION}${applicationId}`, authTOKEN);
	return response;
});

export const updateApplication = createAsyncThunk(
	'applicationManagement/application/updateApplication',
	async (applicationData, { dispatch, getState }) => {
		const { application } = getState().applicationsManagement;

		const authTOKEN = {
			headers: {
				'Content-type': 'multipart/form-data',
				Authorization: localStorage.getItem('jwt_access_token')
			}
		};
		const convertDataToFormData = jsonToFormData(applicationData);

		const response = await axios.put(`${UPDATE_APPLICATION}${application.id}`, convertDataToFormData, authTOKEN);
		return response;
	}
);

export const saveApplication = createAsyncThunk(
	'applicationManagement/application/saveApplication',
	async applicationData => {
		const authTOKEN = {
			headers: {
				'Content-type': 'multipart/form-data',
				Authorization: localStorage.getItem('jwt_access_token')
			}
		};
		const convertDataToFormData = jsonToFormData(applicationData);

		const response = await axios.post(`${CREATE_APPLICATION}`, convertDataToFormData, authTOKEN);
		return response;
	}
);

const applicationSlice = createSlice({
	name: 'applicationManagement/application',
	initialState: null,
	reducers: {
		resetApplication: () => null,
		newApplication: {
			reducer: (state, action) => action.payload,
			prepare: event => ({
				payload: {
					name: ''
				}
			})
		}
	},
	extraReducers: {
		[getApplication.fulfilled]: (state, action) => action.payload,
		[saveApplication.fulfilled]: (state, action) => action.payload,
		[removeApplication.fulfilled]: (state, action) => action.payload,
		[updateApplication.fulfilled]: (state, action) => action.payload
	}
});

export const { newApplication, resetApplication } = applicationSlice.actions;

export default applicationSlice.reducer;
