import { createAsyncThunk, createEntityAdapter, createSlice } from '@reduxjs/toolkit';
import { DELETE_APPLICATION, GET_APPLICATIONS } from 'app/constant/constants';
import axios from 'axios';

export const getApplications = createAsyncThunk(
	'applicationManagement/applications/getApplications',
	async parameter => {
		const { page, size } = parameter;
		axios.defaults.headers.common['Content-type'] = 'application/json';
		axios.defaults.headers.common.Authorization = localStorage.getItem('jwt_access_token');

		const response = axios.get(GET_APPLICATIONS, { params: { page, size } });
		const data = await response;

		sessionStorage.setItem('applications_total_elements', data.data.total_elements);
		sessionStorage.setItem('applications_total_pages', data.data.total_pages);
		delete axios.defaults.headers.common['Content-type'];
		delete axios.defaults.headers.common.Authorization;

		return data.data?.leave_applications;
	}
);

export const removeApplications = createAsyncThunk(
	'applicationManagement/applications/removeApplications',
	async (applicationIds, { dispatch, getState }) => {
		const authTOKEN = {
			headers: {
				'Content-type': 'application/json',
				Authorization: localStorage.getItem('jwt_access_token')
			}
		};
		await axios.delete(`${DELETE_APPLICATION}`, { applicationIds }, authTOKEN);

		return applicationIds;
	}
);

const applicationsAdapter = createEntityAdapter({});

export const { selectAll: selectApplications, selectById: selectApplicationById } = applicationsAdapter.getSelectors(
	state => state.applicationsManagement.applications
);

const applicationsSlice = createSlice({
	name: 'applicationManagement/applications',
	initialState: applicationsAdapter.getInitialState({
		searchText: ''
	}),
	reducers: {
		setApplicationsSearchText: {
			reducer: (state, action) => {
				state.searchText = action.payload;
			},
			prepare: event => ({ payload: event.target.value || '' })
		}
	},
	extraReducers: {
		[getApplications.fulfilled]: applicationsAdapter.setAll
	}
});

export const { setData, setApplicationsSearchText } = applicationsSlice.actions;
export default applicationsSlice.reducer;
