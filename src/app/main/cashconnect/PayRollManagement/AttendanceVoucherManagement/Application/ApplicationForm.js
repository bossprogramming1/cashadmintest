import _ from '@lodash';
import { makeStyles } from '@material-ui/core/styles';
import TextField from '@material-ui/core/TextField';
import { Autocomplete } from '@material-ui/lab';
import React, { useEffect, useState } from 'react';
import { Controller, useFormContext } from 'react-hook-form';
import { useDispatch, useSelector } from 'react-redux';
import { useHistory, useParams } from 'react-router-dom';
// import { getCities } from '../../../../store/dataSlice';
import { getAttendanceProductionTypes, getEmployees, getPayheads, getUnits } from 'app/store/dataSlice';
import { Box, Icon, Table, TableBody, TableCell, TableHead, TableRow, Typography } from '@material-ui/core';
import moment from 'moment';
import CustomDatePicker from 'app/@components/CustomDatePicker';
import clsx from 'clsx';
import { BASE_URL } from 'app/constant/constants';
import { PictureAsPdf } from '@material-ui/icons';
import { saveApplication, updateApplication } from '../store/applicationSlice';

const useStyles = makeStyles(theme => ({
	hidden: {
		display: 'none'
	}
}));

function ApplicationForm(props) {
	const userID = localStorage.getItem('user_id');

	const classes = useStyles(props);
	const methods = useFormContext();
	const { control, formState, getValues, watch } = methods;
	const { errors, isValid, dirtyFields } = formState;
	const routeParams = useParams();
	const { applicationId } = routeParams;
	const history = useHistory();
	const handleDelete = localStorage.getItem('applicationEvent');
	const employees = useSelector(state => state.data.employees);
	const attendanceProductionTypes = useSelector(state => state.data.attendanceProductionTypes);
	const units = useSelector(state => state.data.units);
	const file = watch('file') || '';

	const [previewFile, setPreviewFile] = useState('');
	const [fileExtName, setFileExtName] = useState('');
	const [previewImage, setPreviewImage] = useState();

	const dispatch = useDispatch();

	useEffect(() => {
		dispatch(getEmployees());
		dispatch(getAttendanceProductionTypes());
		dispatch(getUnits());
	}, []);

	function handleSaveApplication() {
		dispatch(saveApplication(getValues())).then(res => {
			if (res.payload) {
				localStorage.setItem('applicationAlert', 'saveApplication');
				history.push('/apps/application-management/applications');
			}
		});
	}

	function handleUpdateApplication() {
		dispatch(updateApplication(getValues())).then(res => {
			if (res.payload) {
				localStorage.setItem('applicationAlert', 'updateApplication');
				history.push('/apps/application-management/applications');
			}
		});
	}

	const handleSubmitOnKeyDownEnter = ev => {
		if (ev.key === 'Enter') {
			if (routeParams.applicationId === 'new' && !(_.isEmpty(dirtyFields) || !isValid)) {
				handleSaveApplication();
			} else if (handleDelete !== 'Delete' && routeParams?.applicationName) {
				handleUpdateApplication();
			}
		}
	};

	return (
		<div>
			<Controller
				className="p-0"
				name={`date`}
				control={control}
				render={({ field }) => {
					return (
						<CustomDatePicker
							previous_date_disable={false}
							className="p-0"
							fullWidth
							field={field}
							label="Date"
						/>
					);
				}}
			/>
			<Controller
				name="employee"
				control={control}
				render={({ field: { onChange, value } }) => {
					return (
						<Autocomplete
							className="mt-8 mb-16"
							freeSolo
							value={value ? employees.find(data => data.id === value) : null}
							options={employees}
							getOptionLabel={option => `${option?.first_name} ${option?.last_name}`}
							onChange={(event, newValue) => {
								onChange(newValue?.id);
							}}
							renderInput={params => (
								<TextField
									{...params}
									placeholder="Select Employee"
									label="Employee"
									error={!!errors.employee}
									required
									helperText={errors?.employee?.message}
									variant="outlined"
									InputLabelProps={{
										shrink: true
									}}
								/>
							)}
						/>
					);
				}}
			/>

			<Controller
				name="days"
				control={control}
				render={({ field }) => {
					return (
						<TextField
							{...field}
							className="mt-8 mb-16"
							error={!!errors?.days}
							helperText={errors?.days?.message}
							label="Leave Days"
							id="days"
							required
							variant="outlined"
							InputLabelProps={field.value && { shrink: true }}
							fullWidth
							onKeyDown={handleSubmitOnKeyDownEnter}
						/>
					);
				}}
			/>
			<Controller
				name="note"
				control={control}
				render={({ field }) => {
					return (
						<TextField
							{...field}
							className="mt-8 mb-16"
							error={!!errors?.note}
							helperText={errors?.note?.message}
							label="Leave Note"
							id="note"
							multiline
							rows={4}
							required
							variant="outlined"
							InputLabelProps={field.value && { shrink: true }}
							fullWidth
							onKeyDown={handleSubmitOnKeyDownEnter}
						/>
					);
				}}
			/>

			<div className="flex justify-center sm:justify-start flex-wrap -mx-16">
				<Controller
					name="file"
					control={control}
					render={({ field: { onChange, value } }) => (
						<label
							htmlFor="button-file"
							className={clsx(
								classes.productImageUpload,
								'flex items-center justify-center relative w-128 h-128 rounded-16 mx-12 mb-24 overflow-hidden cursor-pointer shadow hover:shadow-lg'
							)}
						>
							<input
								accept="image/x-png,image/gif,image/jpeg,application/pdf"
								className="hidden"
								id="button-file"
								type="file"
								onChange={async e => {
									const reader = new FileReader();
									reader.onload = () => {
										if (reader.readyState === 2) {
											setPreviewFile(reader.result);
										}
									};
									reader.readAsDataURL(e.target.files[0]);

									const file = e.target.files[0];

									setFileExtName(e.target.files[0]?.name?.split('.')?.pop()?.toLowerCase());

									onChange(file);
								}}
							/>
							<Icon fontSize="large" color="action">
								cloud_upload
							</Icon>
						</label>
					)}
				/>
				{!previewFile && file && (
					<div style={{ width: 'auto', height: '150px', overflow: 'hidden', display: 'flex' }}>
						{(file?.name || file)?.split('.')?.pop()?.toLowerCase() === 'pdf' ? (
							<PictureAsPdf
								style={{
									color: 'red',
									cursor: 'pointer',
									display: 'block',
									fontSize: '35px',
									margin: 'auto'
								}}
								onClick={() => window.open(`${BASE_URL}${file}`)}
							/>
						) : (
							<img src={`${BASE_URL}${file}`} style={{ height: '150px' }} />
						)}
					</div>
				)}

				{previewFile && (
					<div style={{ width: 'auto', height: '150px', overflow: 'hidden' }}>
						{fileExtName === 'pdf' ? (
							<iframe src={previewFile} frameBorder="0" scrolling="auto" height="150px" width="150px" />
						) : (
							<img src={previewFile} style={{ height: '150px' }} />
						)}
					</div>
				)}
			</div>
		</div>
	);
}

export default ApplicationForm;
