import FusePageCarded from '@fuse/core/FusePageCarded';
import { useDeepCompareEffect } from '@fuse/hooks';
import { yupResolver } from '@hookform/resolvers/yup';
import { Button, Typography } from '@material-ui/core';
import withReducer from 'app/store/withReducer';
import { motion } from 'framer-motion';
import React, { useEffect, useState } from 'react';
import { FormProvider, useForm } from 'react-hook-form';
import { useDispatch, useSelector } from 'react-redux';
import { Link, useParams } from 'react-router-dom';
import * as yup from 'yup';
import reducer from '../store/index';
import { getApplication, newApplication, resetApplication } from '../store/applicationSlice';
import NewApplicationHeader from './NewApplicationHeader';
import ApplicationForm from './ApplicationForm';

/**
 * Form Validation Schema
 */
const schema = yup.object().shape({
	date: yup.string().required('Date is required'),
	days: yup.string().required('Days  is required'),
	employee: yup.number().required('employee is required')
});

const Application = () => {
	const dispatch = useDispatch();
	const application = useSelector(({ applicationsManagement }) => applicationsManagement.application);

	const [noApplication, setNoApplication] = useState(false);
	const methods = useForm({
		mode: 'onChange',
		defaultValues: {},
		resolver: yupResolver(schema)
	});
	const routeParams = useParams();

	const { reset } = methods;

	useDeepCompareEffect(() => {
		function updateApplicationState() {
			const { applicationId } = routeParams;

			if (applicationId === 'new') {
				localStorage.removeItem('event');
				/**
				 * Create New User data
				 */
				dispatch(newApplication());
			} else {
				/**
				 * Get User data
				 */

				dispatch(getApplication(applicationId)).then(action => {
					/**
					 * If the requested product is not exist show message
					 */
					if (!action.payload) {
						setNoApplication(true);
					}
				});
			}
		}

		updateApplicationState();
	}, [dispatch, routeParams]);

	useEffect(() => {}, []);

	useEffect(() => {
		if (!application) {
			return;
		}
		/**
		 * Reset the form on application state changes
		 */
		reset(application);
	}, [application, reset]);

	useEffect(() => {
		return () => {
			/**
			 * Reset Application on component unload
			 */
			dispatch(resetApplication());
			setNoApplication(false);
		};
	}, [dispatch]);

	/**
	 * Show Message if the requested products is not exists
	 */
	if (noApplication) {
		return (
			<motion.div
				initial={{ opacity: 0 }}
				animate={{ opacity: 1, transition: { delay: 0.1 } }}
				className="flex flex-col flex-1 items-center justify-center h-full"
			>
				<Typography color="textSecondary" variant="h5">
					There is no such application!
				</Typography>
				<Button
					className="mt-24"
					component={Link}
					variant="outlined"
					to="/apps/e-commerce/products"
					color="inherit"
				>
					Go to Application Page
				</Button>
			</motion.div>
		);
	}

	return (
		<FormProvider {...methods}>
			<FusePageCarded
				classes={{
					toolbar: 'p-0',
					header: 'min-h-80 h-80'
				}}
				header={<NewApplicationHeader />}
				content={
					<div className="p-16 sm:p-24">
						<ApplicationForm />
					</div>
				}
				innerScroll
			/>
		</FormProvider>
	);
};
export default withReducer('applicationsManagement', reducer)(Application);
