import FusePageCarded from '@fuse/core/FusePageCarded';
import { useDeepCompareEffect } from '@fuse/hooks';
import { yupResolver } from '@hookform/resolvers/yup';
import { Button, Typography } from '@material-ui/core';
import withReducer from 'app/store/withReducer';
import { motion } from 'framer-motion';
import React, { useEffect, useState } from 'react';
import { FormProvider, useForm } from 'react-hook-form';
import { useDispatch, useSelector } from 'react-redux';
import { Link, useParams } from 'react-router-dom';
import * as yup from 'yup';
import reducer from '../store/index';
import { getVouchertype, newVouchertype, resetVouchertype } from '../store/vouchertypeSlice';
import VouchertypeForm from './VouchertypeForm';
import NewVouchertypeHeader from './NewVouchertypeHeader';

/**
 * Form Validation Schema
 */
const schema = yup.object().shape({
	name: yup.string().required('Name is required')
});

const Vouchertype = () => {
	const dispatch = useDispatch();
	const vouchertype = useSelector(({ vouchertypesManagement }) => vouchertypesManagement.vouchertype);

	const [noVouchertype, setNoVouchertype] = useState(false);
	const methods = useForm({
		mode: 'onChange',
		defaultValues: {},
		resolver: yupResolver(schema)
	});
	const routeParams = useParams();

	const { reset } = methods;

	useDeepCompareEffect(() => {
		function updateVouchertypeState() {
			const { vouchertypeId } = routeParams;

			if (vouchertypeId === 'new') {
				localStorage.removeItem('event');
				/**
				 * Create New User data
				 */
				dispatch(newVouchertype());
			} else {
				/**
				 * Get User data
				 */

				dispatch(getVouchertype(vouchertypeId)).then(action => {
					/**
					 * If the requested product is not exist show message
					 */
					if (!action.payload) {
						setNoVouchertype(true);
					}
				});
			}
		}

		updateVouchertypeState();
	}, [dispatch, routeParams]);

	useEffect(() => {}, []);

	useEffect(() => {
		if (!vouchertype) {
			return;
		}
		/**
		 * Reset the form on vouchertype state changes
		 */
		reset(vouchertype);
	}, [vouchertype, reset]);

	useEffect(() => {
		return () => {
			/**
			 * Reset Vouchertype on component unload
			 */
			dispatch(resetVouchertype());
			setNoVouchertype(false);
		};
	}, [dispatch]);

	/**
	 * Show Message if the requested products is not exists
	 */
	if (noVouchertype) {
		return (
			<motion.div
				initial={{ opacity: 0 }}
				animate={{ opacity: 1, transition: { delay: 0.1 } }}
				className="flex flex-col flex-1 items-center justify-center h-full"
			>
				<Typography color="textSecondary" variant="h5">
					There is no such Payroll Voucher Type!
				</Typography>
				<Button
					className="mt-24"
					component={Link}
					variant="outlined"
					to="/apps/e-commerce/products"
					color="inherit"
				>
					Go to Payroll Voucher Type Page
				</Button>
			</motion.div>
		);
	}

	return (
		<FormProvider {...methods}>
			<FusePageCarded
				classes={{
					toolbar: 'p-0',
					header: 'min-h-80 h-80'
				}}
				header={<NewVouchertypeHeader />}
				content={
					<div className="p-16 sm:p-24">
						<VouchertypeForm />
					</div>
				}
				innerScroll
			/>
		</FormProvider>
	);
};
export default withReducer('vouchertypesManagement', reducer)(Vouchertype);
