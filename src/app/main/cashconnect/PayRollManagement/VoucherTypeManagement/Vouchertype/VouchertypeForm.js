import _ from '@lodash';
import { makeStyles } from '@material-ui/core/styles';
import TextField from '@material-ui/core/TextField';
import { Autocomplete } from '@material-ui/lab';
import React, { useEffect } from 'react';
import { Controller, useFormContext } from 'react-hook-form';
import { useDispatch, useSelector } from 'react-redux';
import { useHistory, useParams } from 'react-router-dom';
import { getPayrollVoucherClass } from 'app/store/dataSlice';
import { saveVouchertype, updateVouchertype } from '../store/vouchertypeSlice';

const useStyles = makeStyles(theme => ({
	hidden: {
		display: 'none'
	}
}));

function VouchertypeForm(props) {
	const userID = localStorage.getItem('user_id');
	const payrollVoucherClass = useSelector(state => state.data.payrollVoucherClass);
	const ledgers = useSelector(state => state.data.ledgers);
	const classes = useStyles(props);
	const methods = useFormContext();
	const { control, formState, getValues } = methods;
	const { errors, isValid, dirtyFields } = formState;
	const routeParams = useParams();
	const { vouchertypeId } = routeParams;
	const history = useHistory();
	const handleDelete = localStorage.getItem('vouchertypeEvent');

	const dispatch = useDispatch();

	useEffect(() => {
		dispatch(getPayrollVoucherClass());
	}, []);

	function handleSaveVouchertype() {
		dispatch(saveVouchertype(getValues())).then(res => {
			if (res.payload) {
				localStorage.setItem('vouchertypeAlert', 'saveVouchertype');
				history.push('/apps/vouchertype-management/vouchertypes');
			}
		});
	}

	function handleUpdateVouchertype() {
		dispatch(updateVouchertype(getValues())).then(res => {
			if (res.payload) {
				localStorage.setItem('vouchertypeAlert', 'updateVouchertype');
				history.push('/apps/vouchertype-management/vouchertypes');
			}
		});
	}

	const handleSubmitOnKeyDownEnter = ev => {
		if (ev.key === 'Enter') {
			if (routeParams.vouchertypeId === 'new' && !(_.isEmpty(dirtyFields) || !isValid)) {
				handleSaveVouchertype();
			} else if (handleDelete !== 'Delete' && routeParams?.vouchertypeName) {
				handleUpdateVouchertype();
			}
		}
	};

	return (
		<div>
			<Controller
				name="name"
				control={control}
				render={({ field }) => {
					return (
						<TextField
							{...field}
							className="mt-8 mb-16"
							error={!!errors?.name}
							helperText={errors?.name?.message}
							label="Vouchertype Name"
							id="name"
							required
							variant="outlined"
							InputLabelProps={field.value && { shrink: true }}
							fullWidth
							onKeyDown={handleSubmitOnKeyDownEnter}
						/>
					);
				}}
			/>
			<Controller
				name="class_name"
				control={control}
				render={({ field: { onChange, value, name } }) => (
					<Autocomplete
						className="mt-8 mb-16"
						freeSolo
						value={value ? payrollVoucherClass.find(data => data.id === value) : null}
						options={payrollVoucherClass}
						getOptionLabel={option => `${option?.name}`}
						onChange={(event, newValue) => {
							onChange(newValue?.id);
						}}
						renderInput={params => (
							<TextField
								{...params}
								placeholder="Select Voucher Class"
								label="Voucher Type Class"
								error={!!errors.class_name}
								required
								helperText={errors?.class_name?.message}
								variant="outlined"
								InputLabelProps={{
									shrink: true
								}}
								// onKeyDown={handleSubmitOnKeyDownEnter}
							/>
						)}
					/>
				)}
			/>

			{/* <Controller
				name="city"
				control={control}
				render={({ field: { onChange, value, name } }) => (
					<Autocomplete
						className="mt-8 mb-16"
						freeSolo
						value={value ? citys.find(data => data.id === value) : null}
						options={citys}
						getOptionLabel={option => `${option?.name}`}
						onChange={(event, newValue) => {
							onChange(newValue?.id);
						}}
						renderInput={params => (
							<TextField
								{...params}
								placeholder="Select City"
								label="City"
								error={!!errors.city}
								required
								helperText={errors?.city?.message}
								variant="outlined"
								autoFocus
								InputLabelProps={{
									shrink: true
								}}
								// onKeyDown={handleSubmitOnKeyDownEnter}
							/>
						)}
					/>
				)}
			/> */}
		</div>
	);
}

export default VouchertypeForm;
