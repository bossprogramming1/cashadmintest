import { createAsyncThunk, createSlice } from '@reduxjs/toolkit';
import {
	CREATE_VOUCHER_TYPE,
	DELETE_VOUCHER_TYPE,
	GET_VOUCHER_TYPEID,
	UPDATE_VOUCHER_TYPE
} from 'app/constant/constants';
import axios from 'axios';

export const getVouchertype = createAsyncThunk(
	'vouchertypeManagement/vouchertype/getVouchertype',
	async (params, { rejectWithValue }) => {
		const authTOKEN = {
			headers: {
				'Content-type': 'application/json',
				Authorization: localStorage.getItem('jwt_access_token')
			}
		};

		try {
			const response = await axios.get(`${GET_VOUCHER_TYPEID}${params}`, authTOKEN);
			const data = await response.data;
			return data === undefined ? null : data;
		} catch (err) {
			return rejectWithValue(params);
		}
	}
);

export const removeVouchertype = createAsyncThunk('vouchertypeManagement/vouchertype/removeVouchertype', async val => {
	const authTOKEN = {
		headers: {
			'Content-type': 'application/json',
			Authorization: localStorage.getItem('jwt_access_token')
		}
	};

	const vouchertypeId = val.id;
	const response = await axios.delete(`${DELETE_VOUCHER_TYPE}${vouchertypeId}`, authTOKEN);
	return response;
});

export const updateVouchertype = createAsyncThunk(
	'vouchertypeManagement/vouchertype/updateVouchertype',
	async (vouchertypeData, { dispatch, getState }) => {
		const { vouchertype } = getState().vouchertypesManagement;

		const authTOKEN = {
			headers: {
				'Content-type': 'application/json',
				Authorization: localStorage.getItem('jwt_access_token')
			}
		};
		const response = await axios.put(`${UPDATE_VOUCHER_TYPE}${vouchertype.id}`, vouchertypeData, authTOKEN);
		return response;
	}
);

export const saveVouchertype = createAsyncThunk(
	'vouchertypeManagement/vouchertype/saveVouchertype',
	async vouchertypeData => {
		const authTOKEN = {
			headers: {
				'Content-type': 'application/json',
				Authorization: localStorage.getItem('jwt_access_token')
			}
		};
		const response = await axios.post(`${CREATE_VOUCHER_TYPE}`, vouchertypeData, authTOKEN);
		return response;
	}
);

const vouchertypeSlice = createSlice({
	name: 'vouchertypeManagement/vouchertype',
	initialState: null,
	reducers: {
		resetVouchertype: () => null,
		newVouchertype: {
			reducer: (state, action) => action.payload,
			prepare: event => ({
				payload: {
					name: ''
				}
			})
		}
	},
	extraReducers: {
		[getVouchertype.fulfilled]: (state, action) => action.payload,
		[saveVouchertype.fulfilled]: (state, action) => action.payload,
		[removeVouchertype.fulfilled]: (state, action) => action.payload,
		[updateVouchertype.fulfilled]: (state, action) => action.payload
	}
});

export const { newVouchertype, resetVouchertype } = vouchertypeSlice.actions;

export default vouchertypeSlice.reducer;
