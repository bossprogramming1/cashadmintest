import { combineReducers } from '@reduxjs/toolkit';
import vouchertype from './vouchertypeSlice';
import vouchertypes from './vouchertypesSlice';

const reducer = combineReducers({
	vouchertype,
	vouchertypes
});

export default reducer;
