import { createAsyncThunk, createEntityAdapter, createSlice } from '@reduxjs/toolkit';
import { DELETE_VOUCHER_TYPE, GET_VOUCHER_TYPES } from 'app/constant/constants';
import axios from 'axios';

export const getVouchertypes = createAsyncThunk(
	'vouchertypeManagement/vouchertypes/getVouchertypes',
	async parameter => {
		const { page, size } = parameter;
		axios.defaults.headers.common['Content-type'] = 'application/json';
		axios.defaults.headers.common.Authorization = localStorage.getItem('jwt_access_token');

		const response = axios.get(GET_VOUCHER_TYPES, { params: { page, size } });
		const data = await response;

		sessionStorage.setItem('vouchertypes_total_elements', data.data.total_elements);
		sessionStorage.setItem('vouchertypes_total_pages', data.data.total_pages);
		delete axios.defaults.headers.common['Content-type'];
		delete axios.defaults.headers.common.Authorization;

		return data.data.voucher_types;
	}
);

export const removeVouchertypes = createAsyncThunk(
	'vouchertypeManagement/vouchertypes/removeVouchertypes',
	async (vouchertypeIds, { dispatch, getState }) => {
		const authTOKEN = {
			headers: {
				'Content-type': 'application/json',
				Authorization: localStorage.getItem('jwt_access_token')
			}
		};
		await axios.delete(`${DELETE_VOUCHER_TYPE}`, { vouchertypeIds }, authTOKEN);

		return vouchertypeIds;
	}
);

const vouchertypesAdapter = createEntityAdapter({});

export const { selectAll: selectVouchertypes, selectById: selectVouchertypeById } = vouchertypesAdapter.getSelectors(
	state => state.vouchertypesManagement.vouchertypes
);

const vouchertypesSlice = createSlice({
	name: 'vouchertypeManagement/vouchertypes',
	initialState: vouchertypesAdapter.getInitialState({
		searchText: ''
	}),
	reducers: {
		setVouchertypesSearchText: {
			reducer: (state, action) => {
				state.searchText = action.payload;
			},
			prepare: event => ({ payload: event.target.value || '' })
		}
	},
	extraReducers: {
		[getVouchertypes.fulfilled]: vouchertypesAdapter.setAll
	}
});

export const { setData, setVouchertypesSearchText } = vouchertypesSlice.actions;
export default vouchertypesSlice.reducer;
