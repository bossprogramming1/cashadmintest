/* eslint-disable jsx-a11y/label-has-associated-control */
import _ from '@lodash';
import { makeStyles } from '@material-ui/core/styles';
import TextField from '@material-ui/core/TextField';
import { Autocomplete } from '@material-ui/lab';
import InputAdornment from '@material-ui/core/InputAdornment';
import {
	getAttendanceTypes,
	getCalculationTypes,
	getCompute,
	getGroups,
	getPayheadTypes,
	getPayheads
} from 'app/store/dataSlice';
import React, { useEffect, useState } from 'react';
import { Controller, useFieldArray, useFormContext } from 'react-hook-form';
import { useDispatch, useSelector } from 'react-redux';
import { useHistory, useParams } from 'react-router-dom';
import { calculationPeriod, incomeType, payheadValue } from 'app/@data/data';
import { Add as AddIcon, Delete as DeleteIcon } from '@material-ui/icons';
import {
	Checkbox,
	FormControl,
	FormControlLabel,
	Grid,
	Paper,
	Table,
	TableBody,
	TableCell,
	TableContainer,
	TableHead,
	TableRow
} from '@material-ui/core';
import CustomDatePicker from 'app/@components/CustomDatePicker';
import axios from 'axios';
import { GET_ATTENDANCE_PRODUCTION_TYPE_CALCULATION_TYPE_ID, GET_GROUP_BY_PAYHEAD_ID } from 'app/constant/constants';
import { savePyahead, updatePyahead } from '../store/pyaheadSlice';
import { getPayrollMakeStyles } from '../../PayRollUtils/payrollMakeStyles';

const useStyles = makeStyles(theme => ({
	...getPayrollMakeStyles(theme)
}));

function PyaheadForm(props) {
	const userID = localStorage.getItem('user_id');
	const [computedValue, setComputedValue] = useState(false);
	const [calculationsPeriod, setCalculationsPeriod] = useState(false);
	const [onAttendance, setOnAttendance] = useState(false);
	const [notApplicable, setNotApplicable] = useState(false);
	const [payheadsFormula, setPayheadsFormula] = useState(false);
	const [valueIcon, setValueIcon] = useState(false);
	const [groups, setGroups] = useState([]);
	const [attendanceTypes, setAttendanceTypes] = useState([]);
	const [uptoAmount, setUptoAmount] = useState('');
	const payheadTypes = useSelector(state => state.data.payheadTypes);
	const calculationTypes = useSelector(state => state.data.calculationTypes);
	const compute = useSelector(state => state.data.computes);
	const payheads = useSelector(state => state.data?.payheads);
	const classes = useStyles(props);
	const methods = useFormContext();
	const { control, formState, getValues, setValue, reset, watch } = methods;
	const { errors, isValid, dirtyFields } = formState;
	const routeParams = useParams();
	const { pyaheadId } = routeParams;
	const history = useHistory();
	const handleDelete = localStorage.getItem('pyaheadEvent');
	const dispatch = useDispatch();
	const { fields, remove } = useFieldArray({
		control,
		name: 'items',
		keyName: 'key'
		// defaultValues: [
		// 	{
		// 		effective_from: '',
		// 		amount_from: 0,
		// 		amount_upto: 0,
		// 		slab_type: 0,
		// 		value: 0
		// 	}
		// ]
	});

	console.log('getValues', getValues().items);

	const authTOKEN = {
		headers: {
			'Content-type': 'application/json',
			Authorization: localStorage.getItem('jwt_access_token')
		}
	};
	const payhead_type = watch('payhead_type');

	const calculationType = watch('calculation_type');
	const computeType = watch('compute');
	const attendance_type = watch('attendance_type');

	useEffect(() => {
		setOnAttendance(true);
	}, [attendance_type]);
	useEffect(() => {
		// dispatch(getAttendanceTypes());
		dispatch(getPayheadTypes());
		dispatch(getCalculationTypes());
		// dispatch(getGroups());
		dispatch(getCompute());
		dispatch(getPayheads());
	}, []);
	useEffect(() => {
		const matchingCalculationType = calculationTypes.find(
			type => type.id === calculationType && type.name === 'As Computed Value'
		);
		const matchingCalculationTypeOnProduction = calculationTypes.find(
			type => type.id === calculationType && (type.name === 'On Attendance' || type.name === 'On Production')
		);
		const matchingComputeType = compute.find(
			type => type.id === computeType && type.name === 'On Specified Formula'
		);
		if (matchingCalculationType) {
			setComputedValue(true);
		} else {
			setComputedValue(false);
		}
		if (matchingComputeType) {
			setPayheadsFormula(true);
		} else {
			setPayheadsFormula(false);
		}

		// if (pyaheadId !== 'new') {
		// 	reset({
		// 		items: [
		// 			{
		// 				effective_from: '',
		// 				amount_from: 0,
		// 				amount_upto: 0,
		// 				slab_type: 0,
		// 				value: 0
		// 			}
		// 		]
		// 	});
		// }

		if (matchingCalculationTypeOnProduction?.id) {
			setOnAttendance(true);
			setCalculationsPeriod(false);
			setComputedValue(false);

			axios
				.get(`${GET_ATTENDANCE_PRODUCTION_TYPE_CALCULATION_TYPE_ID}${calculationType}`, authTOKEN)
				.then(response => {
					setAttendanceTypes(response.data);
				});
		}

		if (payhead_type) {
			axios.get(`${GET_GROUP_BY_PAYHEAD_ID}${payhead_type}`, authTOKEN).then(response => {
				setGroups(response.data.groups);
			});
		}
	}, [calculationType, calculationTypes, compute, computeType, payhead_type, onAttendance, pyaheadId]);

	function handleSavePyahead() {
		dispatch(savePyahead(getValues())).then(res => {
			if (res.payload) {
				localStorage.setItem('pyaheadAlert', 'savePyahead');
				history.push('/apps/pyahead-management/pyaheads');
			}
		});
	}

	function handleUpdatePyahead() {
		dispatch(updatePyahead(getValues())).then(res => {
			if (res.payload) {
				localStorage.setItem('pyaheadAlert', 'updatePyahead');
				history.push('/apps/pyahead-management/pyaheads');
			}
		});
	}

	const handleSubmitOnKeyDownEnter = ev => {
		if (ev.key === 'Enter') {
			if (routeParams.pyaheadId === 'new' && !(_.isEmpty(dirtyFields) || !isValid)) {
				handleSavePyahead();
			} else if (handleDelete !== 'Delete' && routeParams?.pyaheadName) {
				handleUpdatePyahead();
			}
		}
	};

	return (
		<>
			<Grid container spacing={12}>
				{!notApplicable && (
					<Grid item xs={3}>
						<div>
							<Controller
								name="name"
								control={control}
								render={({ field }) => {
									setValue('payslip_display_name', field.value);
									return (
										<TextField
											{...field}
											className="mt-8 mb-16"
											error={!!errors?.name}
											helperText={errors?.name?.message}
											label="Payhead Name"
											id="name"
											required
											autoFocus
											variant="outlined"
											InputLabelProps={field.value && { shrink: true }}
											fullWidth
											onKeyDown={handleSubmitOnKeyDownEnter}
										/>
									);
								}}
							/>
							<Controller
								name="payhead_type"
								control={control}
								render={({ field: { onChange, value, name } }) => {
									return (
										<Autocomplete
											className="mt-8 mb-16"
											freeSolo
											value={value ? payheadTypes.find(data => data.id === value) : null}
											options={payheadTypes}
											getOptionLabel={option => `${option?.name}`}
											onChange={(event, newValue) => {
												onChange(newValue?.id);
												if (newValue?.name == 'Not Applicable') {
													setNotApplicable(true);
												} else {
													setNotApplicable(false);
												}
												axios
													.get(`${GET_GROUP_BY_PAYHEAD_ID}${newValue?.id}`, authTOKEN)
													.then(response => {
														setGroups(response.data.groups);
													});
											}}
											renderInput={params => (
												<TextField
													{...params}
													placeholder="Select Payhead Type"
													label="Payhead Type"
													error={!!errors.payhead_type}
													required
													helperText={errors?.payhead_type?.message}
													variant="outlined"
													InputLabelProps={{
														shrink: true
													}}
												/>
											)}
										/>
									);
								}}
							/>
							<Controller
								name="income_type"
								control={control}
								render={({ field: { onChange, value, name } }) => (
									<Autocomplete
										className="mt-8 mb-16"
										freeSolo
										value={value ? incomeType.find(data => data.name === value) : null}
										options={incomeType}
										getOptionLabel={option => `${option?.name}`}
										onChange={(event, newValue) => {
											onChange(newValue?.name);
										}}
										renderInput={params => (
											<TextField
												{...params}
												placeholder="Select Income Type"
												label="Income Type"
												error={!!errors.income_type}
												required
												helperText={errors?.income_type?.message}
												variant="outlined"
												InputLabelProps={{
													shrink: true
												}}
											/>
										)}
									/>
								)}
							/>
							<Controller
								name="group"
								control={control}
								render={({ field: { onChange, value, name } }) => (
									<Autocomplete
										className="mt-8 mb-16"
										freeSolo
										value={value ? groups.find(data => data.id === value) : null}
										options={groups}
										getOptionLabel={option => `${option?.name}`}
										onChange={(event, newValue) => {
											onChange(newValue?.id);
										}}
										renderInput={params => (
											<TextField
												{...params}
												placeholder="Select Group"
												label="Group"
												error={!!errors.group}
												required
												helperText={errors?.group?.message}
												variant="outlined"
												InputLabelProps={{
													shrink: true
												}}
											/>
										)}
									/>
								)}
							/>
							<Controller
								name="affect_net_salary"
								control={control}
								render={({ field }) => (
									<FormControl>
										<FormControlLabel
											label="Affect net salary"
											control={
												<Checkbox {...field} checked={field.value ? field.value : false} />
											}
										/>
									</FormControl>
								)}
							/>
							<Controller
								name="payslip_display_name"
								control={control}
								render={({ field }) => {
									return (
										<TextField
											{...field}
											className="mt-8 mb-16"
											error={!!errors?.payslip_display_name}
											helperText={errors?.payslip_display_name?.message}
											label="Pay slip display name"
											id="payslip_display_name"
											required
											variant="outlined"
											InputLabelProps={field.value && { shrink: true }}
											fullWidth
											onKeyDown={handleSubmitOnKeyDownEnter}
										/>
									);
								}}
							/>
							<Controller
								name="usefor_gratuity_calculation"
								control={control}
								render={({ field }) => (
									<FormControl>
										<FormControlLabel
											label="Use for calculation of gratuity"
											control={
												<Checkbox {...field} checked={field.value ? field.value : false} />
											}
										/>
									</FormControl>
								)}
							/>

							<Controller
								name="calculation_type"
								control={control}
								render={({ field: { onChange, value, name } }) => (
									<Autocomplete
										className="mt-8 mb-16"
										freeSolo
										value={value ? calculationTypes.find(data => data.id === value) : null}
										options={calculationTypes}
										getOptionLabel={option => `${option?.name}`}
										onChange={(event, newValue) => {
											onChange(newValue?.id);
											if (newValue?.name === 'As Computed Value') {
												setComputedValue(true);
												setOnAttendance(false);
												setCalculationsPeriod(false);

												// setCalculationsPeriod(false);
											} else if (newValue?.name === 'Flat Rate') {
												setCalculationsPeriod(true);
												setComputedValue(false);
												setOnAttendance(false);
											} else if (
												newValue?.name === 'On Attendance' ||
												newValue?.name === 'On Production'
											) {
												setOnAttendance(true);
												setCalculationsPeriod(false);
												setComputedValue(false);

												axios
													.get(
														`${GET_ATTENDANCE_PRODUCTION_TYPE_CALCULATION_TYPE_ID}${newValue?.id}`,
														authTOKEN
													)
													.then(response => {
														setAttendanceTypes(response.data);
													});
											} else {
												setOnAttendance(false);

												setCalculationsPeriod(false);

												setComputedValue(false);
											}
										}}
										renderInput={params => (
											<TextField
												{...params}
												placeholder="Select Calculation Type"
												label="Calculation Type"
												error={!!errors.calculation_type}
												required
												helperText={errors?.calculation_type?.message}
												variant="outlined"
												InputLabelProps={{
													shrink: true
												}}
											/>
										)}
									/>
								)}
							/>

							{onAttendance && (
								<Controller
									name="attendance_type"
									control={control}
									render={({ field: { onChange, value, name } }) => (
										<Autocomplete
											className="mt-8 mb-16"
											freeSolo
											value={value ? attendanceTypes.find(data => data.id === value) : null}
											options={attendanceTypes}
											getOptionLabel={option => `${option?.name}`}
											onChange={(event, newValue) => {
												onChange(newValue?.id);
											}}
											renderInput={params => (
												<TextField
													{...params}
													placeholder="Select Attendance Type"
													label="Attendance Type"
													error={!!errors.attendance_type}
													required
													helperText={errors?.attendance_type?.message}
													variant="outlined"
													InputLabelProps={{
														shrink: true
													}}
												/>
											)}
										/>
									)}
								/>
							)}

							{calculationsPeriod && (
								<Controller
									name="calculation_period"
									control={control}
									render={({ field: { onChange, value, name } }) => (
										<Autocomplete
											className="mt-8 mb-16"
											freeSolo
											value={value ? calculationPeriod.find(data => data.id === value) : null}
											options={calculationPeriod}
											getOptionLabel={option => `${option?.name}`}
											onChange={(event, newValue) => {
												onChange(newValue?.id);
											}}
											renderInput={params => (
												<TextField
													{...params}
													placeholder="Select Calculation Period"
													label="Calculation Period"
													error={!!errors.calculation_period}
													required
													helperText={errors?.calculation_period?.message}
													variant="outlined"
													InputLabelProps={{
														shrink: true
													}}
													// onKeyDown={handleSubmitOnKeyDownEnter}
												/>
											)}
										/>
									)}
								/>
							)}
							{/* <Controller
				name="city"
				control={control}
				render={({ field: { onChange, value, name } }) => (
					<Autocomplete
						className="mt-8 mb-16"
						freeSolo
						value={value ? citys.find(data => data.id === value) : null}
						options={citys}
						getOptionLabel={option => `${option?.name}`}
						onChange={(event, newValue) => {
							onChange(newValue?.id);
						}}
						renderInput={params => (
							<TextField
								{...params}
								placeholder="Select City"
								label="City"
								error={!!errors.city}
								required
								helperText={errors?.city?.message}
								variant="outlined"
								autoFocus
								InputLabelProps={{
									shrink: true
								}}
								// onKeyDown={handleSubmitOnKeyDownEnter}
							/>
						)}
					/>
				)}
			/> */}
						</div>
					</Grid>
				)}
				{notApplicable && (
					<Grid item xs={3}>
						<div>
							<Controller
								name="name"
								control={control}
								render={({ field }) => {
									setValue('payslip_display_name', field.value);
									return (
										<TextField
											{...field}
											className="mt-8 mb-16"
											error={!!errors?.name}
											helperText={errors?.name?.message}
											label="Pyahead Name"
											id="name"
											required
											// onChange={setValue('payslip_display_name', field.value)}
											autoFocus
											variant="outlined"
											InputLabelProps={field.value && { shrink: true }}
											fullWidth
											onKeyDown={handleSubmitOnKeyDownEnter}
										/>
									);
								}}
							/>
							<Controller
								name="payhead_type"
								control={control}
								render={({ field: { onChange, value, name } }) => (
									<Autocomplete
										className="mt-8 mb-16"
										freeSolo
										value={value ? payheadTypes.find(data => data.id === value) : null}
										options={payheadTypes}
										getOptionLabel={option => `${option?.name}`}
										onChange={(event, newValue) => {
											onChange(newValue?.id);
											if (newValue?.name == 'Not Applicable') {
												setNotApplicable(true);
											} else {
												setNotApplicable(false);
											}
											axios
												.get(`${GET_GROUP_BY_PAYHEAD_ID}${newValue?.id}`, authTOKEN)
												.then(response => {
													setGroups(response.data.groups);
												});
										}}
										renderInput={params => (
											<TextField
												{...params}
												placeholder="Select Payhead Type"
												label="Payhead Type"
												error={!!errors.payhead_type}
												required
												helperText={errors?.payhead_type?.message}
												variant="outlined"
												InputLabelProps={{
													shrink: true
												}}
												// onKeyDown={handleSubmitOnKeyDownEnter}
											/>
										)}
									/>
								)}
							/>

							<Controller
								name="group"
								control={control}
								render={({ field: { onChange, value, name } }) => (
									<Autocomplete
										className="mt-8 mb-16"
										freeSolo
										value={value ? groups.find(data => data.id === value) : null}
										options={groups}
										getOptionLabel={option => `${option?.name}`}
										onChange={(event, newValue) => {
											onChange(newValue?.id);
										}}
										renderInput={params => (
											<TextField
												{...params}
												placeholder="Select Group"
												label="Group"
												error={!!errors.group}
												required
												helperText={errors?.group?.message}
												variant="outlined"
												InputLabelProps={{
													shrink: true
												}}
												// onKeyDown={handleSubmitOnKeyDownEnter}
											/>
										)}
									/>
								)}
							/>
						</div>
					</Grid>
				)}
				{computedValue && (
					<Grid item xs={9}>
						<div className="ml-16">
							<h3 className="text-center mb-32"> Computation Information</h3>
							<Grid container>
								<Grid item xs={4} className="flex items-center">
									<div>
										<p>Compute</p>
									</div>
								</Grid>
								<Grid item xs={8}>
									<Controller
										name="compute"
										control={control}
										render={({ field: { onChange, value, name } }) => (
											<Autocomplete
												className="mt-8 mb-16"
												freeSolo
												value={value ? compute.find(data => data.id === value) : null}
												options={compute}
												getOptionLabel={option => `${option?.name}`}
												onChange={(event, newValue) => {
													onChange(newValue?.id);
													if (newValue?.name == 'On Specified Formula') {
														setPayheadsFormula(true);
													} else {
														setPayheadsFormula(false);
													}
												}}
												renderInput={params => (
													<TextField
														{...params}
														placeholder="Select Compute"
														label="Compute"
														error={!!errors.compute}
														required
														helperText={errors?.compute?.message}
														variant="outlined"
														InputLabelProps={{
															shrink: true
														}}
														// onKeyDown={handleSubmitOnKeyDownEnter}
													/>
												)}
											/>
										)}
									/>
								</Grid>
							</Grid>
							{payheadsFormula && (
								<Grid container>
									<Grid item xs={4} className="flex items-center">
										<div>
											<p>Specified Formula</p>
										</div>
									</Grid>
									<Grid item xs={8}>
										<Controller
											name="formula_payheads"
											control={control}
											render={({ field: { onChange, value } }) => {
												return (
													<Autocomplete
														className="mt-8 mb-16"
														freeSolo
														multiple
														filterSelectedOptions
														value={
															value
																? payheads.filter(data => value.includes(data.id))
																: []
														}
														options={payheads}
														getOptionLabel={option => `${option?.name}`}
														onChange={(event, newValue) => {
															const selectedValues = newValue.map(option => option.id);

															onChange(selectedValues);
														}}
														renderInput={params => (
															<TextField
																{...params}
																placeholder="Select Payhead"
																label="Payhead"
																error={!!errors.payheads}
																required
																helperText={errors?.payheads?.message}
																variant="outlined"
																InputLabelProps={{
																	shrink: true
																}}
																// onKeyDown={handleSubmitOnKeyDownEnter}
															/>
														)}
													/>
												);
											}}
										/>
									</Grid>
								</Grid>
							)}

							{/* <div style={{ display: 'flex', alignItems: 'flex-start' }}>
								<Paper
									className="w-full rounded-40 shadow p-12 mb-10"
									style={{ backgroundColor: '#e7eaed' }}
								>
									{rows.map(row => (
										<div style={{ display: 'flex', alignItems: 'center' }}>
											<div
												key={row.id}
												style={{
													display: 'grid',
													gridTemplateColumns: 'repeat(5, 1fr)',
													columnGap: '9px'
												}}
											>
												{row.fields.map((e, fieldIndex) => (
													<div key={e.name}>
														<label for={e?.name}>{e.label}</label>
														<br />
														<Controller
															name={`rows[${row.id - 1}].${e.name}`}
															control={control}
															defaultValue={e.value}
															render={({ field }) => (
																<TextField
																	{...field}
																	className="mt-8 mb-16 mr-8"
																	id={e.name}
																	// label={field.name.replace(`rows[${row.id - 1}].`, '')}
																	required
																	autoFocus={fieldIndex === 0}
																	variant="outlined"
																	InputLabelProps={field.value && { shrink: true }}
																	fullWidth
																	onKeyDown={event =>
																		handleSubmitOnKeyDownEnter(event, row.id)
																	}
																	onChange={event => {
																		field.onChange(event);
																	}}
																/>
															)}
														/>
													</div>
												))}
											</div>
											{row.id > 1 && (
												<Delete
													style={{ cursor: 'pointer' }}
													onClick={() => handleDeleteRow(row.id)}
													color="error"
												/>
											)}
										</div>
									))}
								</Paper>

								<IconButton onClick={handleAddRow}>
									<AddCircle />
								</IconButton>
							</div> */}
							<Grid xs={12}>
								<div className={classes.mainContainer}>
									<TableContainer component={Paper} className={classes.tblContainer}>
										<Table className={classes.table} aria-label="simple table">
											<TableHead className={classes.tableHead}>
												<TableRow>
													<TableCell className={classes.tableCell}>No.</TableCell>
													<TableCell className={classes.tableCell} align="center">
														Effective from
													</TableCell>
													<TableCell className={classes.tableCell} align="center">
														More than amount
													</TableCell>
													<TableCell className={classes.tableCell} align="center">
														Amount upto
													</TableCell>
													<TableCell className={classes.tableCell} align="center">
														Slab type
													</TableCell>
													<TableCell className={classes.tableCell} align="center">
														Value
													</TableCell>
													<TableCell className={classes.tableCell} align="center">
														Action
													</TableCell>
												</TableRow>
											</TableHead>

											<TableBody>
												{fields.map((item, idx) => {
													return (
														<TableRow key={item.key}>
															<TableCell
																className={classes.tableCellInBody}
																component="th"
																scope="row"
															>
																{idx + 1}
															</TableCell>
															{idx === 0 && (
																<TableCell
																	rowSpan={fields.length + 1}
																	style={{ border: '1px solid black' }}
																	className={classes.tableCell}
																>
																	<Controller
																		className="p-0"
																		name={`effective_from`}
																		control={control}
																		render={({ field }) => {
																			return (
																				<CustomDatePicker
																					className="p-0"
																					// previous_date_disable={true}
																					field={field}
																					style={{ maxWidth: '130px' }}
																					label="Effective from"
																				/>
																			);
																		}}
																	/>
																</TableCell>
															)}

															<TableCell className={classes.tableCellInBody}>
																<Controller
																	name={`items.${idx}.amount_from`}
																	control={control}
																	render={({ field }) => {
																		return (
																			<TextField
																				{...field}
																				className="mt-8 mb-16"
																				label="More than amount"
																				id="amount_from"
																				variant="outlined"
																				InputLabelProps={{ shrink: true }}
																				fullWidth
																			/>
																		);
																	}}
																/>
															</TableCell>
															<TableCell className={classes.tableCellInBody}>
																<Controller
																	name={`items.${idx}.amount_upto`}
																	control={control}
																	render={({ field }) => {
																		return (
																			<TextField
																				{...field}
																				className="mt-8 mb-16"
																				label="Amount upto"
																				id="amount_upto"
																				variant="outlined"
																				InputLabelProps={{ shrink: true }}
																				fullWidth
																				onChange={e => {
																					field.onChange(e);
																					// setUptoAmount(e.target.value);
																				}}
																			/>
																		);
																	}}
																/>
															</TableCell>
															<TableCell className={classes.tableCellInBody}>
																<Controller
																	name={`items.${idx}.slab_type`}
																	control={control}
																	render={({ field: { onChange, value, name } }) => (
																		<Autocomplete
																			className="mt-8 mb-16 p-0"
																			fullWidth
																			freeSolo
																			value={
																				value
																					? payheadValue.find(
																							data => data.id === value
																					  )
																					: null
																			}
																			options={payheadValue}
																			getOptionLabel={option => `${option?.name}`}
																			onChange={(event, newValue) => {
																				onChange(newValue?.id);
																				if (newValue?.id == 'percentage') {
																					setValueIcon(true);
																				} else {
																					setValueIcon(false);
																				}
																			}}
																			renderInput={params => (
																				<TextField
																					{...params}
																					label="Slab type"
																					id="slab_type"
																					variant="outlined"
																					InputLabelProps={{ shrink: true }}
																					fullWidth
																				/>
																			)}
																			PopperProps={{
																				style: { width: '250px' } // Adjust the width here as needed
																			}}
																		/>
																	)}
																/>
															</TableCell>
															<TableCell className={classes.tableCellInBody}>
																<Controller
																	name={`items.${idx}.value`}
																	control={control}
																	render={({ field }) => {
																		return (
																			<TextField
																				{...field}
																				className="mt-8 mb-16"
																				label="Value"
																				id="value"
																				variant="outlined"
																				InputLabelProps={{ shrink: true }}
																				fullWidth
																				InputProps={{
																					endAdornment: valueIcon ? (
																						<InputAdornment position="end">
																							%
																						</InputAdornment>
																					) : (
																						''
																					)
																				}}
																			/>
																		);
																	}}
																/>
															</TableCell>
															{idx === 0 && (
																<TableCell
																	className="p-0 md:p-0"
																	align="center"
																	component="th"
																	scope="row"
																	style={{ minWidth: '80px' }}
																>
																	<div
																		variant="outlined"
																		className={classes.btnContainer}
																		onClick={() => {
																			const values = getValues();
																			reset({
																				...values,
																				items: [
																					...values?.items,
																					{
																						effective_from: '',
																						amount_from: 0,
																						amount_upto: 0,
																						slab_type: 0,
																						value: 0
																					}
																				]
																			});
																		}}
																		onBlur={() => {}}
																	>
																		<AddIcon />
																	</div>
																</TableCell>
															)}
															{idx !== 0 && (
																<TableCell
																	className="p-0 md:p-0"
																	align="center"
																	component="th"
																	scope="row"
																	style={{ minWidth: '80px' }}
																>
																	<div>
																		<DeleteIcon
																			onClick={() => {
																				remove(idx);
																			}}
																			className="h-52 cursor-pointer"
																			style={{ color: 'red' }}
																		/>
																	</div>
																</TableCell>
															)}
														</TableRow>
													);
												})}
											</TableBody>
										</Table>
									</TableContainer>
								</div>
								{/* <div style={{ display: 'flex', justifyContent: 'space-between' }}>
									{<Typography style={{ color: 'red' }}>{ledgerMessage}</Typography>}
									{debitCreditMessage && (
										<Typography style={{ color: isDebitCreditMatched ? 'green' : 'red' }}>
											{debitCreditMessage}
										</Typography>
									)}
								</div> */}
							</Grid>
						</div>
					</Grid>
				)}
			</Grid>
		</>
	);
}

export default PyaheadForm;
