import FusePageCarded from '@fuse/core/FusePageCarded';
import { useDeepCompareEffect } from '@fuse/hooks';
import { yupResolver } from '@hookform/resolvers/yup';
import { Button, Typography } from '@material-ui/core';
import useUserInfo from 'app/@customHooks/useUserInfo';
import setIdIfValueIsObjArryData from 'app/@helpers/setIdIfValueIsObjArryData';
import setIdIfValueIsObject2 from 'app/@helpers/setIdIfValueIsObject2';
import {
	PAYMENT_VOUCHER_CREATE,
	PAYMENT_VOUCHER_DELETE,
	PAYMENT_VOUCHER_DETAILS,
	PAYMENT_VOUCHER_UPDATE
} from 'app/constant/permission/permission';
import PagenotFound from 'app/main/cashconnect/Pagenotfound/PagenotFound';
import { getUserPermissions } from 'app/store/dataSlice';
import withReducer from 'app/store/withReducer';
import { motion } from 'framer-motion';
import React, { useEffect, useState } from 'react';
import { FormProvider, useForm } from 'react-hook-form';
import { useDispatch, useSelector } from 'react-redux';
import { Link, useParams } from 'react-router-dom';
import * as yup from 'yup';
import reducer from '../store/index';
import { getPyahead, newPyahead, resetPyahead } from '../store/pyaheadSlice';

import NewPyaheadHeader from './NewPyaheadHeader';
import PyaheadForm from './PyaheadForm';

/**
 * Form Validation Schema
 */
const schema = yup.object().shape({
	// payment_date: yup.date().required('Payment Date is required')
});

const Pyahead = () => {
	const dispatch = useDispatch();
	const pyahead = useSelector(({ pyaheadsManagement }) => pyaheadsManagement.pyahead);

	const [noPyahead, setNoPyahead] = useState(false);
	const methods = useForm({
		mode: 'onChange',
		defaultValues: {},
		resolver: yupResolver(schema)
	});

	const routeParams = useParams();
	const UserPermissions = useSelector(state => state.data.UserPermissions);
	useEffect(() => {
		dispatch(getUserPermissions());
	}, []);
	const { reset } = methods;

	const [letFormSave, setLetFormSave] = useState(false);

	const { userId } = useUserInfo();

	useDeepCompareEffect(() => {
		function updatePyaheadState() {
			const { pyaheadId } = routeParams;

			if (pyaheadId === 'new') {
				localStorage.removeItem('event');
				/**
				 * Create New User data
				 */
				dispatch(newPyahead());
			} else {
				/**
				 * Get User data
				 */

				dispatch(getPyahead(pyaheadId)).then(action => {
					/**
					 * If the requested product is not exist show message
					 */
					if (!action.payload) {
						setNoPyahead(true);
					}
				});
			}
		}

		updatePyaheadState();
	}, [dispatch, routeParams]);

	useEffect(() => {}, []);

	useEffect(() => {
		if (!pyahead) {
			return;
		}
		/**
		 * Reset the form on pyahead state changes
		 */
		const convertedPyaheadItems = setIdIfValueIsObjArryData(pyahead?.items);
		const convertedPyaheadFormula_payheads = setIdIfValueIsObjArryData(pyahead?.formula_payheads);
		const convertedPyahead = setIdIfValueIsObject2(pyahead);
		reset({
			...convertedPyahead,
			items: pyahead?.items?.length !== 0 ? convertedPyaheadItems : [],
			formula_payheads: pyahead?.formula_payheads ? convertedPyaheadFormula_payheads : null
		});
	}, [pyahead, reset]);

	useEffect(() => {
		return () => {
			/**
			 * Reset Pyahead on component unload
			 */
			dispatch(resetPyahead());
			setNoPyahead(false);
		};
	}, [dispatch]);

	/**
	 * Show Message if the requested products is not exists
	 */
	if (noPyahead) {
		return (
			<motion.div
				initial={{ opacity: 0 }}
				animate={{ opacity: 1, transition: { delay: 0.1 } }}
				className="flex flex-col flex-1 items-center justify-center h-full"
			>
				<Typography color="textSecondary" variant="h5">
					There is no such pyahead!
				</Typography>
				<Button
					className="mt-24"
					component={Link}
					variant="outlined"
					to="/apps/e-commerce/products"
					color="inherit"
				>
					Go to Pyahead Page
				</Button>
			</motion.div>
		);
	}

	return (
		<FormProvider {...methods}>
			<FusePageCarded
				classes={{
					toolbar: 'p-0',
					header: 'min-h-80 h-80'
				}}
				header={<NewPyaheadHeader letFormSave={letFormSave} />}
				content={
					<div className="p-16 sm:p-24 max-w-3xl">
						<PyaheadForm setLetFormSave={setLetFormSave} />
					</div>
				}
				innerScroll
			/>
		</FormProvider>
	);
};
export default withReducer('pyaheadsManagement', reducer)(Pyahead);
