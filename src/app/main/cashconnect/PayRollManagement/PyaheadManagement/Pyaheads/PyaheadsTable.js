import FuseLoading from '@fuse/core/FuseLoading';
import FuseScrollbars from '@fuse/core/FuseScrollbars';
import _ from '@lodash';
import { Typography } from '@material-ui/core';
import Checkbox from '@material-ui/core/Checkbox';
import { makeStyles } from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TablePagination from '@material-ui/core/TablePagination';
import TableRow from '@material-ui/core/TableRow';
import DeleteIcon from '@material-ui/icons/Delete';
import EditIcon from '@material-ui/icons/Edit';
import Pagination from '@material-ui/lab/Pagination';
import { rowsPerPageOptions } from 'app/@data/data';
import { SEARCH_PYAHEAD } from 'app/constant/constants';
import { motion } from 'framer-motion';
import React, { useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { withRouter } from 'react-router-dom';
import AutoAwesomeMotionIcon from '@mui/icons-material/AutoAwesomeMotion';
import { getPyaheads, selectPyaheads } from '../store/pyaheadsSlice';
import PyaheadsTableHead from './PyaheadsTableHead';
import { getPyahead } from '../store/pyaheadSlice';

const useStyles = makeStyles(() => ({
	root: {
		display: 'flex',
		justifyContent: 'space-between',
		flexWrap: 'nowrap',
		overflow: 'auto',
		minHeight: '35px'
	},
	toolbar: {
		'& > div': {
			minHeight: 'fit-content'
		}
	}
}));

const PyaheadsTable = props => {
	const dispatch = useDispatch();
	const pyaheads = useSelector(selectPyaheads);
	const searchText = useSelector(({ pyaheadsManagement }) => pyaheadsManagement.pyaheads.searchText);
	const [searchPyahead, setSearchPyahead] = useState([]);
	const [loading, setLoading] = useState(true);
	const [selected, setSelected] = useState([]);
	const [data, setData] = useState(pyaheads);
	const [page, setPage] = useState(0);
	const [rowsPerPage, setRowsPerPage] = useState(30);
	const [order, setOrder] = useState({
		direction: 'asc',
		id: null
	});
	let serialNumber = 1;
	const user_role = localStorage.getItem('user_role');
	const [parameter, setParameter] = useState({ page: 1, size: 30 });
	const totalPages = sessionStorage.getItem('pyaheads_total_pages');
	const totalElements = sessionStorage.getItem('pyaheads_total_elements');
	const classes = useStyles();

	useEffect(() => {
		dispatch(getPyaheads(parameter)).then(() => setLoading(false));
	}, [dispatch]);

	//search pyahead
	useEffect(() => {
		searchText ? getSearchPyahead() : setSearchPyahead([]);
	}, [searchText]);

	const getSearchPyahead = () => {
		fetch(`${SEARCH_PYAHEAD}?name=${searchText}`)
			.then(response => response.json())
			.then(searchedPyaheadsData => {
				setSearchPyahead(searchedPyaheadsData.pyaheads);
			})
			.catch(() => setSearchPyahead([]));
	};

	function handleRequestSort(pyaheadEvent, property) {
		const id = property;
		let direction = 'desc';

		if (order.id === property && order.direction === 'desc') {
			direction = 'asc';
		}

		setOrder({
			direction,
			id
		});
	}

	function handleSelectAllClick(pyaheadEvent) {
		if (pyaheadEvent.target.checked) {
			setSelected(pyaheads.map(n => n.id));
			return;
		}
		setSelected([]);
	}

	function handleDeselect() {
		setSelected([]);
	}

	function handleUpdatePyahead(item) {
		localStorage.removeItem('pyaheadEvent');
		props.history.push(`/apps/pyahead-management/pyaheads/${item.id}/${item?.name}`);
	}

	function handleDeletePyahead(item, pyaheadEvent) {
		localStorage.removeItem('pyaheadEvent');
		localStorage.setItem('pyaheadEvent', pyaheadEvent);
		props.history.push(`/apps/pyahead-management/pyaheads/${item.id}/${item?.name}`);
	}

	function handleCheck(pyaheadEvent, id) {
		const selectedIndex = selected.indexOf(id);
		let newSelected = [];

		if (selectedIndex === -1) {
			newSelected = newSelected.concat(selected, id);
		} else if (selectedIndex === 0) {
			newSelected = newSelected.concat(selected.slice(1));
		} else if (selectedIndex === selected.length - 1) {
			newSelected = newSelected.concat(selected.slice(0, -1));
		} else if (selectedIndex > 0) {
			newSelected = newSelected.concat(selected.slice(0, selectedIndex), selected.slice(selectedIndex + 1));
		}

		setSelected(newSelected);
	}

	const handlePagination = (e, handlePage) => {
		setParameter({ ...parameter, page: handlePage });
		setPage(handlePage - 1);
		dispatch(getPyaheads({ ...parameter, page: handlePage }));
	};

	function handleChangePage(pyaheadEvent, value) {
		setPage(value);
		setParameter({ ...parameter, page: value + 1 });
		dispatch(getPyaheads({ ...parameter, page: value - 1 }));
	}

	function handleChangeRowsPerPage(pyaheadEvent) {
		setRowsPerPage(pyaheadEvent.target.value);
		setParameter({ ...parameter, size: pyaheadEvent.target.value });
		dispatch(getPyaheads({ ...parameter, size: pyaheadEvent.target.value }));
	}

	if (loading) {
		return <FuseLoading />;
	}

	if (pyaheads?.length === 0) {
		return (
			<motion.div
				initial={{ opacity: 0 }}
				animate={{ opacity: 1, transition: { delay: 0.1 } }}
				className="flex flex-1 items-center justify-center h-full"
			>
				<Typography color="textSecondary" variant="h5">
					There are no pyahead!
				</Typography>
			</motion.div>
		);
	}

	return (
		<div className="w-full flex flex-col">
			<FuseScrollbars className="flex-grow overflow-x-auto">
				<Table stickyHeader className="min-w-xl" aria-labelledby="tableTitle">
					<PyaheadsTableHead
						selectedPyaheadIds={selected}
						order={order}
						onSelectAllClick={handleSelectAllClick}
						onRequestSort={handleRequestSort}
						rowCount={pyaheads.length}
						onMenuItemClick={handleDeselect}
					/>

					<TableBody>
						{_.orderBy(
							searchText !== '' && !_.isEmpty(searchPyahead) ? searchPyahead : pyaheads,
							[
								o => {
									switch (order.id) {
										case 'categories': {
											return o.categories[0];
										}
										default: {
											return o[order.id];
										}
									}
								}
							],
							[order.direction]
						).map(n => {
							const isSelected = selected.indexOf(n.id) !== -1;
							return (
								<TableRow
									className="h-52 cursor-pointer"
									hover
									role="checkbox"
									aria-checked={isSelected}
									tabIndex={-1}
									key={n.id}
									selected={isSelected}
								>
									<TableCell className="whitespace-nowrap w-40 md:w-64 text-center" padding="none">
										<Checkbox
											checked={isSelected}
											onClick={pyaheadEvent => pyaheadEvent.stopPropagation()}
											onChange={pyaheadEvent => handleCheck(pyaheadEvent, n.id)}
										/>
									</TableCell>

									<TableCell
										className="whitespace-nowrap w-40 md:w-64 text-center"
										component="th"
										scope="row"
									>
										{parameter.page * parameter.size - parameter.size + serialNumber++}
									</TableCell>

									<TableCell
										className="whitespace-nowrap p-4 md:p-16 text-center"
										component="th"
										scope="row"
									>
										{n?.name}
									</TableCell>
									<TableCell
										className="whitespace-nowrap p-4 md:p-16 text-center"
										component="th"
										scope="row"
									>
										{n?.group?.name}
									</TableCell>

									<TableCell
										whitespace-nowrap
										className="p-4 md:p-16"
										align="center"
										component="th"
										scope="row"
									>
										<div>
											<EditIcon
												onClick={pyaheadEvent => handleUpdatePyahead(n)}
												className="cursor-pointer"
												style={{ color: 'green' }}
											/>
											<DeleteIcon
												onClick={event => handleDeletePyahead(n, 'Delete')}
												className="cursor-pointer"
												style={{
													color: 'red',
													// display: n?.name == 'Salary Payable' ? 'none' : 'inline-block',
													visibility:
														(user_role === 'ADMIN' || user_role === 'admin') &&
														n?.name !== 'Salary Payable'
															? 'visible'
															: 'hidden'
												}}
											/>
										</div>
									</TableCell>
								</TableRow>
							);
						})}
					</TableBody>
				</Table>
			</FuseScrollbars>

			<div className={classes.root}>
				<Pagination
					classes={{ ul: 'flex-nowrap' }}
					count={totalPages}
					defaultPage={1}
					color="primary"
					showFirstButton
					showLastButton
					variant="outlined"
					shape="rounded"
					onChange={handlePagination}
				/>

				<TablePagination
					classes={{ root: 'overflow-visible' }}
					rowsPerPageOptions={rowsPerPageOptions}
					component="div"
					count={totalElements}
					rowsPerPage={rowsPerPage}
					page={page}
					className={classes.toolbar}
					backIconButtonProps={{
						'aria-label': 'Previous Page',
						className: 'py-0'
					}}
					nextIconButtonProps={{
						'aria-label': 'Next Page',
						className: 'py-0'
					}}
					onChangePage={handleChangePage}
					onChangeRowsPerPage={handleChangeRowsPerPage}
				/>
			</div>
		</div>
	);
};

export default withRouter(PyaheadsTable);
