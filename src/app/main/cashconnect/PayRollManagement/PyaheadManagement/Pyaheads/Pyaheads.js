import FusePageCarded from '@fuse/core/FusePageCarded';
import { getUserPermissions } from 'app/store/dataSlice';
import withReducer from 'app/store/withReducer';
import React, { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import reducer from '../store/index';
import PyaheadsHeader from './PyaheadsHeader';
import PyaheadsTable from './PyaheadsTable';

const Pyaheads = () => {
	const UserPermissions = useSelector(state => state.data.UserPermissions);
	useEffect(() => {
		dispatch(getUserPermissions());
	}, []);
	const dispatch = useDispatch();
	return (
		<FusePageCarded
			classes={{
				content: 'flex',
				contentCard: 'overflow-hidden',
				header: 'min-h-74 h-64'
			}}
			header={<PyaheadsHeader />}
			content={<PyaheadsTable />}
			innerScroll
		/>
	);
};
export default withReducer('pyaheadsManagement', reducer)(Pyaheads);
