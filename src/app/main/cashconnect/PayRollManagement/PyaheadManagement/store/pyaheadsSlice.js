import { createAsyncThunk, createEntityAdapter, createSlice } from '@reduxjs/toolkit';
import { DELETE_PYAHEAD, GET_PYAHEADS } from 'app/constant/constants';
import axios from 'axios';

export const getPyaheads = createAsyncThunk('pyaheadManagement/pyaheads/getPyaheads', async parameter => {
	const { page, size } = parameter;
	axios.defaults.headers.common['Content-type'] = 'application/json';
	axios.defaults.headers.common.Authorization = localStorage.getItem('jwt_access_token');

	const response = axios.get(GET_PYAHEADS, { params: { page, size } });
	const data = await response;

	sessionStorage.setItem('pyaheads_total_elements', data.data.total_elements);
	sessionStorage.setItem('pyaheads_total_pages', data.data.total_pages);
	delete axios.defaults.headers.common['Content-type'];
	delete axios.defaults.headers.common.Authorization;

	return data.data?.payheads;
});

export const removePyaheads = createAsyncThunk(
	'pyaheadManagement/pyaheads/removePyaheads',
	async (pyaheadIds, { dispatch, getState }) => {
		const authTOKEN = {
			headers: {
				'Content-type': 'application/json',
				Authorization: localStorage.getItem('jwt_access_token')
			}
		};
		await axios.delete(`${DELETE_PYAHEAD}`, { pyaheadIds }, authTOKEN);

		return pyaheadIds;
	}
);

const pyaheadsAdapter = createEntityAdapter({});

export const { selectAll: selectPyaheads, selectById: selectPyaheadById } = pyaheadsAdapter.getSelectors(
	state => state.pyaheadsManagement.pyaheads
);

const pyaheadsSlice = createSlice({
	name: 'pyaheadManagement/pyaheads',
	initialState: pyaheadsAdapter.getInitialState({
		searchText: ''
	}),
	reducers: {
		setPyaheadsSearchText: {
			reducer: (state, action) => {
				state.searchText = action.payload;
			},
			prepare: event => ({ payload: event.target.value || '' })
		}
	},
	extraReducers: {
		[getPyaheads.fulfilled]: pyaheadsAdapter.setAll
	}
});

export const { setData, setPyaheadsSearchText } = pyaheadsSlice.actions;
export default pyaheadsSlice.reducer;
