import { combineReducers } from '@reduxjs/toolkit';
import pyahead from './pyaheadSlice';
import pyaheads from './pyaheadsSlice';

const reducer = combineReducers({
	pyahead,
	pyaheads
});

export default reducer;
