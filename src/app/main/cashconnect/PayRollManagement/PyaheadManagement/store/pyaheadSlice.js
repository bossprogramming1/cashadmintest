import { createAsyncThunk, createSlice } from '@reduxjs/toolkit';
import {
	CREATE_COMPUTATION_INFORMATION_VALUE,
	CREATE_PYAHEAD,
	DELETE_PYAHEAD,
	GET_PYAHEADID,
	UPDATE_PYAHEAD
} from 'app/constant/constants';
import axios from 'axios';

export const getPyahead = createAsyncThunk(
	'pyaheadManagement/pyahead/getPyahead',
	async (params, { rejectWithValue }) => {
		const authTOKEN = {
			headers: {
				'Content-type': 'application/json',
				Authorization: localStorage.getItem('jwt_access_token')
			}
		};

		try {
			const response = await axios.get(`${GET_PYAHEADID}${params}`, authTOKEN);
			const data = await response.data;

			return data === undefined ? null : data;
		} catch (err) {
			return rejectWithValue(params);
		}
	}
);

export const removePyahead = createAsyncThunk('pyaheadManagement/pyahead/removePyahead', async val => {
	const authTOKEN = {
		headers: {
			'Content-type': 'application/json',
			Authorization: localStorage.getItem('jwt_access_token')
		}
	};

	const pyaheadId = val.id;
	const response = await axios.delete(`${DELETE_PYAHEAD}${pyaheadId}`, authTOKEN);
	return response;
});

export const updatePyahead = createAsyncThunk(
	'pyaheadManagement/pyahead/updatePyahead',
	async (pyaheadData, { dispatch, getState }) => {
		const { pyahead } = getState().pyaheadsManagement;

		const authTOKEN = {
			headers: {
				'Content-type': 'application/json',
				Authorization: localStorage.getItem('jwt_access_token')
			}
		};
		const response = await axios.put(`${UPDATE_PYAHEAD}${pyahead.id}`, pyaheadData, authTOKEN);
		return response;
	}
);

export const savePyahead = createAsyncThunk('pyaheadManagement/pyahead/savePyahead', async pyaheadData => {
	const authTOKEN = {
		headers: {
			'Content-type': 'application/json',
			Authorization: localStorage.getItem('jwt_access_token')
		}
	};

	const response = await axios.post(`${CREATE_PYAHEAD}`, pyaheadData, authTOKEN);
	return response;
});

const pyaheadSlice = createSlice({
	name: 'pyaheadManagement/pyahead',
	initialState: null,
	reducers: {
		resetPyahead: () => null,
		newPyahead: {
			reducer: (_state, action) => action.payload,
			prepare: () => ({
				payload: {
					name: '',
					income_type: '',
					payslip_display_name: '',
					affect_net_salary: false,
					usefor_gratuity_calculation: false,
					attendance_type: '',
					formula_payheads: [],
					items: [
						{
							effective_from: '',
							amount_from: 0,
							amount_upto: 0,
							slab_type: 0,
							value: 0
						}
					]
				}
			})
		}
	},
	extraReducers: {
		[getPyahead.fulfilled]: (state, action) => action.payload.payheads,
		[savePyahead.fulfilled]: (state, action) => action.payload,
		[removePyahead.fulfilled]: (state, action) => action.payload,
		[updatePyahead.fulfilled]: (state, action) => action.payload
	}
});

export const { newPyahead, resetPyahead } = pyaheadSlice.actions;

export default pyaheadSlice.reducer;
