/* eslint-disable import/named */
import FusePageCarded from '@fuse/core/FusePageCarded';
import { useDeepCompareEffect } from '@fuse/hooks';
import { yupResolver } from '@hookform/resolvers/yup';
import { Button, Typography } from '@material-ui/core';
import useUserInfo from 'app/@customHooks/useUserInfo';
import setIdIfValueIsObjArryData from 'app/@helpers/setIdIfValueIsObjArryData';
import setIdIfValueIsObject2 from 'app/@helpers/setIdIfValueIsObject2';

import PagenotFound from 'app/main/cashconnect/Pagenotfound/PagenotFound';
import { getUserPermissions } from 'app/store/dataSlice';
import withReducer from 'app/store/withReducer';
import { motion } from 'framer-motion';
import React, { useEffect, useState } from 'react';
import { FormProvider, useForm } from 'react-hook-form';
import { useDispatch, useSelector } from 'react-redux';
import { Link, useParams } from 'react-router-dom';
import * as yup from 'yup';
import reducer from '../store/index';
import { getPaymentPF, newPaymentPF, resetPaymentPF, setUserBasedBranch } from '../store/paymentPFSlice';

import NewPaymentpfHeader from './NewPaymentPFHeader';
import PaymentpfForm from './PaymentPFForm';

/**
 * Form Validation Schema
 */
const schema = yup.object().shape({
	payment_account: yup.number().required('Account type is required')
});

const PaymentPF = () => {
	const dispatch = useDispatch();
	const paymentPF = useSelector(({ paymentPFsManagement }) => paymentPFsManagement.paymentPF);
	const [noPaymentPF, setNoPaymentPF] = useState(false);
	const methods = useForm({
		mode: 'onChange',
		defaultValues: {},
		resolver: yupResolver(schema)
	});

	const routeParams = useParams();
	const UserPermissions = useSelector(state => state.data.UserPermissions);
	useEffect(() => {
		dispatch(getUserPermissions());
	}, []);
	const { reset } = methods;

	const { userId } = useUserInfo();

	useDeepCompareEffect(() => {
		function updatePaymentPFState() {
			const { paymentPFId } = routeParams;

			if (paymentPFId === 'new') {
				localStorage.removeItem('event');
				/**
				 * Create New User data
				 */
				dispatch(newPaymentPF());
				dispatch(setUserBasedBranch(userId));
			} else {
				/**
				 * Get User data
				 */

				dispatch(getPaymentPF(paymentPFId)).then(action => {
					/**
					 * If the requested product is not exist show message
					 */
					if (!action.payload) {
						setNoPaymentPF(true);
					}
				});
			}
		}

		updatePaymentPFState();
	}, [dispatch, routeParams]);

	useEffect(() => {
		if (!paymentPF) {
			return;
		}
		/**
		 * Reset the form on paymentPF state changes
		 */
		console.log('99999paymentPF', paymentPF);
		reset({
			...paymentPF,
			employee:
				paymentPF.calculation_for == 'employees'
					? paymentPF.employee
					: paymentPF?.payment_type == 'advanced'
					? paymentPF.employee[0]
					: [],
			calculation_for: paymentPF?.calculation_for
		});
	}, [paymentPF, reset]);

	useEffect(() => {
		return () => {
			/**
			 * Reset PaymentPF on component unload
			 */
			dispatch(resetPaymentPF());
			setNoPaymentPF(false);
		};
	}, [dispatch]);

	/**
	 * Show Message if the requested products is not exists
	 */
	if (noPaymentPF) {
		return (
			<motion.div
				initial={{ opacity: 0 }}
				animate={{ opacity: 1, transition: { delay: 0.1 } }}
				className="flex flex-col flex-1 items-center justify-center h-full"
			>
				<Typography color="textSecondary" variant="h5">
					There is no such paymentPF!
				</Typography>
				<Button
					className="mt-24"
					component={Link}
					variant="outlined"
					to="/apps/paymentPF-management/paymentPFs"
					color="inherit"
				>
					Go to PaymentPF Page
				</Button>
			</motion.div>
		);
	}

	return (
		<FormProvider {...methods}>
			<FusePageCarded
				classes={{
					content: 'flex',
					contentCard: 'overflow-hidden',
					header: 'min-h-74 h-64'
				}}
				header={<NewPaymentpfHeader />}
				content={
					<div>
						<PaymentpfForm />
					</div>
				}
				innerScroll
			/>
		</FormProvider>
	);
};
export default withReducer('paymentPFsManagement', reducer)(PaymentPF);
