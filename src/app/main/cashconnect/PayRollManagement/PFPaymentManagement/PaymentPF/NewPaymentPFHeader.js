/* eslint-disable import/named */
import _ from '@lodash';
import Button from '@material-ui/core/Button';
import Icon from '@material-ui/core/Icon';
import { useTheme } from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';
import isShouldFormSave from 'app/@helpers/isShouldFormSave';
import isShouldFormUpdate from 'app/@helpers/isShouldFormUpdate';
import { motion } from 'framer-motion';
import React, { useEffect } from 'react';
import { useFormContext } from 'react-hook-form';
import { useDispatch } from 'react-redux';
import { Link, useHistory, useParams } from 'react-router-dom';
import { removePaymentPF, saveAdvancePaymentPF, savePaymentPF, updatePaymentPF } from '../store/paymentPFSlice';

const NewPaymentpfHeader = () => {
	const dispatch = useDispatch();
	const methods = useFormContext();
	const { formState, watch, getValues } = methods;
	const { isValid, dirtyFields } = formState;

	const theme = useTheme();
	const history = useHistory();

	const routeParams = useParams();
	const { paymentPFId, paymentPFName } = routeParams;

	const handleDelete = localStorage.getItem('paymentPFEvent');

	function handleSavePaymentPF() {
		const { pf_list, ...formDataWithoutPFList } = getValues();
		console.log('formDataWithoutPFList', formDataWithoutPFList);
		if (formDataWithoutPFList.payment_type === 'advanced') {
			dispatch(saveAdvancePaymentPF(formDataWithoutPFList)).then(res => {
				if (res.payload) {
					localStorage.setItem('paymentPFAlert', 'savePaymentPF');
					history.goBack('/apps/paymentPF-management/paymentPFs');
				}
			});
		} else {
			dispatch(savePaymentPF(formDataWithoutPFList)).then(res => {
				if (res.payload) {
					localStorage.setItem('paymentPFAlert', 'savePaymentPF');
					history.goBack('/apps/paymentPF-management/paymentPFs');
				}
			});
		}
	}

	function handleUpdatePaymentPF() {
		const { pf_list, ...formDataWithoutPFList } = getValues();
		dispatch(updatePaymentPF(formDataWithoutPFList)).then(res => {
			if (res.payload) {
				localStorage.setItem('paymentPFAlert', 'updatePaymentPF');
				history.goBack('/apps/paymentPF-management/paymentPFs');
			}
		});
	}

	function handleRemovePaymentPF() {
		dispatch(removePaymentPF(paymentPFName)).then(res => {
			if (res.payload) {
				localStorage.removeItem('paymentPFEvent');
				localStorage.setItem('paymentPFAlert', 'deletePaymentPF');
				history.goBack('/apps/paymentPF-management/paymentPFs');
			}
		});
	}

	function handleCancel() {
		history.goBack('/apps/paymentPF-management/paymentPFs');
	}

	// useEffect(() => {
	// 	const handleSaveAndUpdate = e => {
	// 		if (e.key === 'Enter') {
	// 			if (routeParams.paymentPFId === 'new' && !(_.isEmpty(dirtyFields) || !isValid)) {
	// 				isShouldFormSave(e) && handleSavePaymentPF();
	// 			} else if (handleDelete !== 'Delete' && routeParams?.paymentPFName) {
	// 				isShouldFormUpdate(e) && handleUpdatePaymentPF();
	// 			} else if (handleDelete == 'Delete' && routeParams.paymentPFId !== 'new') {
	// 				handleRemovePaymentPF();
	// 			}
	// 		}
	// 	};

	// 	window.addEventListener('keydown', handleSaveAndUpdate);

	// 	return () => window.removeEventListener('keydown', handleSaveAndUpdate);
	// }, [dirtyFields, isValid]);

	return (
		<div className="flex flex-1 w-full items-center justify-between">
			<div className="flex flex-col items-start max-w-full min-w-0">
				<motion.div initial={{ x: 20, opacity: 0 }} animate={{ x: 0, opacity: 1, transition: { delay: 0.3 } }}>
					<Typography
						className="flex items-center sm:mb-12"
						component={Link}
						role="button"
						to="/apps/paymentPF-management/paymentPFs"
						color="inherit"
					>
						<Icon className="text-20">{theme.direction === 'ltr' ? 'arrow_back' : 'arrow_forward'}</Icon>
						<span className="hidden sm:flex mx-4 font-medium">Payment PF</span>
					</Typography>
				</motion.div>

				<div className="flex items-center max-w-full">
					<div className="flex flex-col min-w-0 mx-8 sm:mc-16">
						<motion.div initial={{ x: -20 }} animate={{ x: 0, transition: { delay: 0.3 } }}>
							<Typography className="text-16 sm:text-20 truncate font-semibold">
								Create New Payment PF
							</Typography>
							<Typography variant="caption" className="font-medium">
								Payment PF Detail
							</Typography>
						</motion.div>
					</div>
				</div>
			</div>
			<motion.div
				className="flex"
				initial={{ opacity: 0, x: 20 }}
				animate={{ opacity: 1, x: 0, transition: { delay: 0.3 } }}
			>
				{handleDelete == 'Delete' && (
					<Typography className="mt-6" variant="subtitle2">
						Do you want to remove this Payment PF?
					</Typography>
				)}
				{handleDelete == 'Delete' && routeParams.paymentPFId !== 'new' && (
					<Button
						className="whitespace-nowrap mx-4"
						variant="contained"
						color="secondary"
						onClick={handleRemovePaymentPF}
						startIcon={<Icon className="hidden sm:flex">delete</Icon>}
						style={{ backgroundColor: '#ea5b78', color: 'white' }}
					>
						Remove
					</Button>
				)}
				{routeParams.paymentPFId == 'new' && (
					<Button
						className="whitespace-nowrap mx-4"
						variant="contained"
						color="secondary"
						disabled={_.isEmpty(dirtyFields) || !isValid}
						onClick={handleSavePaymentPF}
					>
						Save
					</Button>
				)}
				{handleDelete !== 'Delete' && routeParams?.paymentPFName && (
					<Button
						className="whitespace-nowrap mx-4"
						color="secondary"
						variant="contained"
						style={{
							backgroundColor: '#4dc08e',
							color: 'white'
						}}
						onClick={handleUpdatePaymentPF}
					>
						Update
					</Button>
				)}
				<Button
					className="whitespace-nowrap mx-4"
					variant="contained"
					style={{ backgroundColor: '#FFAA4C', color: 'white' }}
					onClick={handleCancel}
				>
					Cancel
				</Button>
			</motion.div>
		</div>
	);
};

export default NewPaymentpfHeader;
