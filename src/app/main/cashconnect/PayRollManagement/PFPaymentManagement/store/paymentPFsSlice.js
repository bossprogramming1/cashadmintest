import { createAsyncThunk, createEntityAdapter, createSlice } from '@reduxjs/toolkit';
import axios from 'axios';
import { GET_PF_ALL } from '../../../../../constant/constants';

export const getPaymentPFs = createAsyncThunk('paymentPFManagement/paymentPFs/getPaymentPFs', async pageAndSize => {
	axios.defaults.headers.common['Content-type'] = 'application/json';
	axios.defaults.headers.common.Authorization = localStorage.getItem('jwt_access_token');

	const response = axios.get(GET_PF_ALL, { params: pageAndSize });
	const data = await response;

	sessionStorage.setItem('total_paymentPFs_elements', data.data.total_elements);
	sessionStorage.setItem('total_paymentPFs_pages', data.data.total_pages);
	delete axios.defaults.headers.common['Content-type'];
	delete axios.defaults.headers.common.Authorization;

	return data.data.instances;
});

// export const removePaymentPFs = createAsyncThunk(
// 	'paymentPFManagement/paymentPFs/removePaymentPFs',
// 	async paymentPFIds => {
// 		const headers = {
// 			'Content-type': 'application/json',
// 			Authorization: localStorage.getItem('jwt_access_token')
// 		};
// 		const data = {
// 			ids: paymentPFIds
// 		};
// 		const response = await axios.delete(`${DELETE_PAYMENTPF_MULTIPLE}`, { headers, data });

// 		return response;
// 	}
// );

const paymentPFsAdapter = createEntityAdapter({});

export const { selectAll: selectPaymentPFs, selectById: selectPaymentPFById } = paymentPFsAdapter.getSelectors(
	state => state.paymentPFsManagement.paymentPFs
);

const paymentPFSlice = createSlice({
	name: 'paymentPFManagement/paymentPFs',
	initialState: paymentPFsAdapter.getInitialState({
		searchText: ''
	}),
	reducers: {
		setPaymentPFsSearchText: {
			reducer: (state, action) => {
				state.searchText = action.payload;
			},
			prepare: event => ({ payload: event.target.value || '' })
		}
	},
	extraReducers: {
		[getPaymentPFs.fulfilled]: paymentPFsAdapter.setAll
	}
});

export const { setData, setPaymentPFsSearchText } = paymentPFSlice.actions;
export default paymentPFSlice.reducer;
