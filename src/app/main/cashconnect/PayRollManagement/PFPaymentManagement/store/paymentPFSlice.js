import { createAsyncThunk, createSlice } from '@reduxjs/toolkit';
import { ledgerCashId, ledgerPayableId } from 'app/@data/data';
import { BRANCH_BY_USER_ID, CREATE_PAYMENTPF } from 'app/constant/constants';
import axios from 'axios';
import moment from 'moment';
// import {
// 	BRANCH_BY_USER_ID,
// 	CREATE_ADVANCED_PAYMENTPF,
// 	CREATE_PAYMENTPF,
// 	DELETE_PAYMENTPF,
// 	GET_PAYMENTPF_BY_ID,
// 	UPDATE_PAYMENTPF
// } from '../../../../../constant/constants';

// export const getPaymentPF = createAsyncThunk(
// 	'paymentPFManagement/paymentPF/getPaymentPF',
// 	async (params, { rejectWithValue }) => {
// 		const authTOKEN = {
// 			headers: {
// 				'Content-type': 'application/json',
// 				Authorization: localStorage.getItem('jwt_access_token')
// 			}
// 		};

// 		try {
// 			const response = await axios.get(`${GET_PAYMENTPF_BY_ID}${params}`, authTOKEN);
// 			const data = await response.data;
// 			return data === undefined ? null : data;
// 		} catch (err) {
// 			return rejectWithValue(params);
// 		}
// 	}
// );

// export const removePaymentPF = createAsyncThunk('paymentPFManagement/paymentPF/removePaymentPF', async paymentPFId => {
// 	const authTOKEN = {
// 		headers: {
// 			'Content-type': 'application/json',
// 			Authorization: localStorage.getItem('jwt_access_token')
// 		}
// 	};

// 	const response = await axios.delete(`${DELETE_PAYMENTPF}${paymentPFId}`, authTOKEN);
// 	return response;
// });

// export const updatePaymentPF = createAsyncThunk(
// 	'paymentPFManagement/paymentPF/updatePaymentPF',
// 	async paymentPFData => {
// 		const authTOKEN = {
// 			headers: {
// 				'Content-type': 'application/json',
// 				Authorization: localStorage.getItem('jwt_access_token')
// 			}
// 		};
// 		const response = await axios.put(`${UPDATE_PAYMENTPF}${paymentPFData.id}`, paymentPFData, authTOKEN);
// 		return response;
// 	}
// );

export const savePaymentPF = createAsyncThunk('paymentPFManagement/paymentPF/savePaymentPF', async paymentPFData => {
	const authTOKEN = {
		headers: {
			'Content-type': 'application/json',
			Authorization: localStorage.getItem('jwt_access_token')
		}
	};
	const response = await axios.post(`${CREATE_PAYMENTPF}`, paymentPFData, authTOKEN);
	return response;
});
// export const saveAdvancePaymentPF = createAsyncThunk(
// 	'paymentPFManagement/paymentPF/saveAdvancePaymentPF',
// 	async paymentPFData => {
// 		const authTOKEN = {
// 			headers: {
// 				'Content-type': 'application/json',
// 				Authorization: localStorage.getItem('jwt_access_token')
// 			}
// 		};
// 		const response = await axios.post(`${CREATE_ADVANCED_PAYMENTPF}`, paymentPFData, authTOKEN);
// 		return response;
// 	}
// );

export const setUserBasedBranch = createAsyncThunk('paymentPFManagement/paymentPF/setUserBasedBranch', async userId => {
	const authTOKEN = {
		headers: {
			'Content-type': 'application/json',
			Authorization: localStorage.getItem('jwt_access_token')
		}
	};
	const response = await axios.get(`${BRANCH_BY_USER_ID}${userId}`, authTOKEN);
	return response.data.id || {};
});

const paymentPFSlice = createSlice({
	name: 'paymentPFManagement/paymentPF',
	initialState: null,
	reducers: {
		resetPaymentPF: () => null,
		newPaymentPF: {
			reducer: (_state, action) => action.payload,
			prepare: () => ({
				payload: {
					date: moment(new Date()).format('YYYY-MM-DD'),

					payment_type: 'regular',
					details: '',
					// employee: [],
					// department: [],
					// total_amount: '',
					payment_account: '',
					branch: ''
				}
			})
		}
	},
	extraReducers: {
		// [getPaymentPF.fulfilled]: (_state, action) => action.payload,
		[savePaymentPF.fulfilled]: (_state, action) => action.payload,
		// [saveAdvancePaymentPF.fulfilled]: (_state, action) => action.payload,
		// [removePaymentPF.fulfilled]: (_state, action) => action.payload,
		// [updatePaymentPF.fulfilled]: (_state, action) => action.payload,
		[setUserBasedBranch.fulfilled]: (state, action) => {
			return {
				...state,
				branch: action.payload
			};
		}
	}
});

export const { newPaymentPF, resetPaymentPF } = paymentPFSlice.actions;

export default paymentPFSlice.reducer;
