import { combineReducers } from '@reduxjs/toolkit';
import paymentPF from './paymentPFSlice';
import paymentPFs from './paymentPFsSlice';

const reducer = combineReducers({
	paymentPF,
	paymentPFs
});

export default reducer;
