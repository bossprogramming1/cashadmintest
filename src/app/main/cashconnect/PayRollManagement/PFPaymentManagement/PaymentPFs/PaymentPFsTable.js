import FuseLoading from '@fuse/core/FuseLoading';
import FuseScrollbars from '@fuse/core/FuseScrollbars';
import _ from '@lodash';
import PlaylistAddCheckIcon from '@material-ui/icons/PlaylistAddCheck';
import DataUsageIcon from '@material-ui/icons/DataUsage';
import CancelIcon from '@material-ui/icons/Cancel';
import Button from '@mui/material/Button';
import Dialog from '@mui/material/Dialog';
import DialogActions from '@mui/material/DialogActions';
import DialogContent from '@mui/material/DialogContent';
import DialogContentText from '@mui/material/DialogContentText';
import DialogTitle from '@mui/material/DialogTitle';
import { Card, CardContent, Modal, TextField, Tooltip, Typography } from '@material-ui/core';
import Checkbox from '@material-ui/core/Checkbox';
import { makeStyles } from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TablePagination from '@material-ui/core/TablePagination';
import TableRow from '@material-ui/core/TableRow';
import DeleteIcon from '@material-ui/icons/Delete';
import EditIcon from '@material-ui/icons/Edit';
import PrintIcon from '@material-ui/icons/Print';
import { Controller, FormProvider, useForm, useFormContext } from 'react-hook-form';
import { Autocomplete, Pagination } from '@material-ui/lab';
import { rowsPerPageOptions } from 'app/@data/data';
import { motion } from 'framer-motion';
import React, { useEffect, useRef, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { withRouter } from 'react-router-dom';
import axios from 'axios';
import { getDepartments, getEmployees, getLedgersCashAndBank, getPayrollVoucherClass } from 'app/store/dataSlice';
import moment from 'moment';
import CustomDatePicker from 'app/@components/CustomDatePicker';
import { SEARCH_PF_PAYMENT } from '../../../../../constant/constants';
// import PrintPF from '../../AccountComponents/PrintPF';
import { getPaymentPFs, selectPaymentPFs } from '../store/paymentPFsSlice';
import PaymentPFsTableHead from './PaymentPFsTableHead';
import { savePaymentPF } from '../store/paymentPFSlice';

const useStyles = makeStyles(() => ({
	root: {
		display: 'flex',
		justifyContent: 'space-between',
		flexWrap: 'nowrap',
		overflow: 'auto',
		minHeight: '35px'
	},
	toolbar: {
		'& > div': {
			minHeight: 'fit-content'
		}
	},
	modal: {
		margin: 'auto',
		backgroundColor: 'white',
		width: '900px',
		height: 'fit-content',
		maxWidth: '540px',
		maxHeight: 'fit-content',
		borderRadius: '20px',
		overflow: 'scroll'
	}
}));

const PaymentPFsTable = props => {
	const UserPermissions = useSelector(state => state.data.UserPermissions);
	const ledgersCashAndBank = useSelector(state => state.data?.ledgersCashAndBank);
	const employees = useSelector(state => state.data.employees);
	const dispatch = useDispatch();
	const classes = useStyles();

	const methods = useForm();
	const { control, formState, watch, setError, getValues, setValue, reset, clearErrors } = methods;
	const { errors, isValid, dirtyFields } = formState;

	const paymentPFs = useSelector(selectPaymentPFs);
	const searchText = useSelector(({ paymentPFsManagement }) => paymentPFsManagement.paymentPFs.searchText);
	const [searchPaymentPF, setSearchPaymentPF] = useState([]);
	const [loading, setLoading] = useState(true);
	const [selected, setSelected] = useState([]);
	const [page, setPage] = useState(0);
	const [rowsPerPage, setRowsPerPage] = useState(30);
	const [pageAndSize, setPageAndSize] = useState({ page: 1, size: 30 });
	const currentDate = moment().format('DD-MM-YYYY');
	const [openModal, setOpenModal] = useState(false);
	const [isBank, setIsBank] = useState(false);
	const [defaultValue, setDefaultValue] = useState();

	useEffect(() => {
		dispatch(getEmployees());
		dispatch(getDepartments());
		dispatch(getLedgersCashAndBank());
	}, [dispatch]);

	const [order, setOrder] = useState({
		direction: 'asc',
		id: null
	});

	let serialNumber = 1;
	const totalPages = sessionStorage.getItem('total_paymentPFs_pages');
	const totalElements = sessionStorage.getItem('total_paymentPFs_elements');

	const printPFRef = useRef();

	useEffect(() => {
		dispatch(getPaymentPFs(pageAndSize)).then(() => setLoading(false));
	}, [dispatch]);

	useEffect(() => {
		searchText !== '' ? getSearchPaymentPF() : setSearchPaymentPF([]);
	}, [searchText]);

	const getSearchPaymentPF = () => {
		const authTOKEN = {
			headers: {
				'Content-type': 'application/json',
				Authorization: localStorage.getItem('jwt_access_token')
			}
		};

		fetch(`${SEARCH_PF_PAYMENT}?invoice_no=${searchText}`, authTOKEN)
			.then(response => response.json())
			.then(searchedPaymentPFData => {
				setSearchPaymentPF(searchedPaymentPFData?.payment_pfs);
			})
			.catch(() => setSearchPaymentPF([]));
	};

	function handleRequestSort(_e, property) {
		const id = property;
		let direction = 'desc';

		if (order.id === property && order.direction === 'desc') {
			direction = 'asc';
		}

		setOrder({
			direction,
			id
		});
	}

	function handleSelectAllClick(paymentPFEvent) {
		if (paymentPFEvent.target.checked) {
			setSelected((!_.isEmpty(searchPaymentPF) ? searchPaymentPF : paymentPFs).map(n => n.id));
			return;
		}
		setSelected([]);
	}

	function handleDeselect() {
		setSelected([]);
	}

	function handleUpdatePaymentPF(item) {
		localStorage.removeItem('paymentPFEvent');
		props.history.push(`/apps/paymentPF-management/paymentPFs/${item.id}/${item.invoice_no}`);
	}
	function handleDeletePaymentPF(item, paymentPFEvent) {
		localStorage.removeItem('paymentPFEvent');
		localStorage.setItem('paymentPFEvent', paymentPFEvent);
		props.history.push(`/apps/paymentPF-management/paymentPFs/${item.id}/${item.invoice_no}`);
	}

	function handleCheck(_e, id) {
		const selectedIndex = selected.indexOf(id);
		let newSelected = [];

		if (selectedIndex === -1) {
			newSelected = newSelected.concat(selected, id);
		} else if (selectedIndex === 0) {
			newSelected = newSelected.concat(selected.slice(1));
		} else if (selectedIndex === selected.length - 1) {
			newSelected = newSelected.concat(selected.slice(0, -1));
		} else if (selectedIndex > 0) {
			newSelected = newSelected.concat(selected.slice(0, selectedIndex), selected.slice(selectedIndex + 1));
		}

		setSelected(newSelected);
	}
	// save payment PF
	function handleSavePaymentPF() {
		console.log('gesdf', getValues());
		dispatch(savePaymentPF(getValues())).then(res => {
			if (res.payload) {
				setOpenModal(false);
				dispatch(getPaymentPFs(pageAndSize)).then(() => setLoading(false));
			}
		});
	}

	//pagination
	const handlePagination = (_e, handlePage) => {
		setPageAndSize({ ...pageAndSize, page: handlePage });
		setPage(handlePage - 1);
		dispatch(getPaymentPFs({ ...pageAndSize, page: handlePage }));
	};

	function handleChangePage(_e, value) {
		setPage(value);
		setPageAndSize({ ...pageAndSize, page: value + 1 });
		dispatch(getPaymentPFs({ ...pageAndSize, page: value + 1 }));
	}

	function handleChangeRowsPerPage(paymentPFEvent) {
		setRowsPerPage(paymentPFEvent.target.value);
		setPageAndSize({ ...pageAndSize, size: paymentPFEvent.target.value });
		dispatch(getPaymentPFs({ ...pageAndSize, size: paymentPFEvent.target.value }));
	}

	if (loading) {
		return <FuseLoading />;
	}

	if (paymentPFs?.length === 0) {
		return (
			<motion.div
				initial={{ opacity: 0 }}
				animate={{ opacity: 1, transition: { delay: 0.1 } }}
				className="flex flex-1 items-center justify-center h-full"
			>
				<Typography color="textSecondary" variant="h5">
					There are no paymentPF!
				</Typography>
			</motion.div>
		);
	}
	console.log('paymentPFserrors', isValid, dirtyFields, errors);
	return (
		<div className="w-full flex flex-col">
			<FuseScrollbars className="flex-grow overflow-x-auto">
				{/* <PrintPF ref={printPFRef} title="Payment PF" type="payment" /> */}
				<Table stickyHeader className="min-w-xl" aria-labelledby="tableTitle">
					<PaymentPFsTableHead
						selectedPaymentPFIds={selected}
						order={order}
						onSelectAllClick={handleSelectAllClick}
						onRequestSort={handleRequestSort}
						rowCount={(!_.isEmpty(searchPaymentPF) ? searchPaymentPF : paymentPFs).length}
						onMenuItemClick={handleDeselect}
						pagination={pageAndSize}
					/>

					<TableBody>
						{_.orderBy(
							searchText !== '' && !_.isEmpty(searchPaymentPF) ? searchPaymentPF : paymentPFs,
							[
								o => {
									switch (order.id) {
										case 'categories': {
											return o.categories[0];
										}
										default: {
											return o[order.id];
										}
									}
								}
							],
							[order.direction]
						).map(n => {
							const isSelected = selected.indexOf(n.id) !== -1;
							return (
								<TableRow
									className="h-52 cursor-pointer"
									hover
									role="checkbox"
									aria-checked={isSelected}
									tabIndex={-1}
									key={n.id}
									selected={isSelected}
								>
									<TableCell className="w-40 md:w-64" component="th" scope="row">
										{pageAndSize.page * pageAndSize.size - pageAndSize.size + serialNumber++}
									</TableCell>

									<TableCell className="p-4 md:p-16" component="th" scope="row">
										{n?.employee_name}
									</TableCell>

									<TableCell className="p-4 md:p-16" component="th" scope="row">
										{n.amount}
									</TableCell>

									<TableCell className="p-4 md:p-16" align="center" component="th" scope="row">
										<div className="flex flex-nowrap">
											<Tooltip title="Edit" placement="top" enterDelay={300}>
												<EditIcon
													style={{
														color: 'green'
													}}
													onClick={() => {
														setOpenModal(true);
														setValue('total_amount', n.amount);
														setDefaultValue(n.amount);
														setValue('employee', n.employee_id);
													}}
													className="cursor-pointer"
												/>
											</Tooltip>
										</div>
									</TableCell>
								</TableRow>
							);
						})}
					</TableBody>
				</Table>
			</FuseScrollbars>

			<div className={classes.root} id="pagiContainer">
				<Pagination
					classes={{ ul: 'flex-nowrap' }}
					count={totalPages}
					page={page + 1}
					defaultPage={1}
					color="primary"
					showFirstButton
					showLastButton
					variant="outlined"
					shape="rounded"
					onChange={handlePagination}
				/>

				<TablePagination
					classes={{ root: 'overflow-visible' }}
					rowsPerPageOptions={rowsPerPageOptions}
					component="div"
					count={totalElements}
					rowsPerPage={rowsPerPage}
					page={page}
					className={classes.toolbar}
					backIconButtonProps={{
						'aria-label': 'Previous Page',
						className: 'py-0'
					}}
					nextIconButtonProps={{
						'aria-label': 'Next Page',
						className: 'py-0'
					}}
					onChangePage={handleChangePage}
					onChangeRowsPerPage={handleChangeRowsPerPage}
				/>
			</div>

			<Modal
				open={openModal}
				className={classes.modal}
				onClose={() => {
					setOpenModal(false);
				}}
				keepMounted
			>
				<>
					<Card style={{ marginBottom: '10px' }}>
						<CardContent>
							<Typography
								className="whitespace-nowrap mx-4 flex justify-center"
								variant="contained"
								color="secondary"
							>
								Provident Fund Payment
							</Typography>
							<FormProvider {...methods}>
								<Controller
									className="p-0"
									name={`date`}
									control={control}
									render={({ field }) => {
										return (
											<CustomDatePicker
												// className="p-0"
												previous_date_disable={true}
												field={field}
												label="Payment Date"
												error={!!errors?.date_to} // Check if there's an error for the "date_to" field
												helperText={errors?.date_to?.message || ''}
												required
											/>
										);
									}}
								/>
								<Controller
									name="payment_account"
									control={control}
									render={({ field: { onChange, value } }) => (
										<Autocomplete
											className="mt-8 mb-16"
											freeSolo
											options={ledgersCashAndBank}
											value={value ? ledgersCashAndBank.find(data => data.id == value) : null}
											getOptionLabel={option => `${option.name}`}
											onChange={(event, newValue) => {
												onChange(newValue?.id);
												if (newValue.name !== ('Cash' || 'cash')) {
													setIsBank(true);
												} else {
													setIsBank(false);
												}
											}}
											renderInput={params => (
												<TextField
													{...params}
													placeholder="Select Ledger"
													label="Ledger"
													variant="outlined"
													InputLabelProps={{
														shrink: true
													}}
												/>
											)}
										/>
									)}
								/>
								<Controller
									name="employee"
									control={control}
									render={({ field: { onChange, value, name } }) => (
										<Autocomplete
											className="mt-8 mb-16"
											freeSolo
											value={value ? employees.find(data => data.id === value) : null}
											options={employees}
											getOptionLabel={option => `${option?.first_name} ${option?.last_name}`}
											onChange={(event, newValue) => {
												onChange(newValue?.id);
											}}
											renderInput={params => (
												<TextField
													{...params}
													placeholder="Select Employee"
													label="Employee"
													error={!!errors?.employee}
													required
													helperText={errors?.employee?.message}
													variant="outlined"
													autoFocus
													InputLabelProps={{
														shrink: true
													}}
													// onKeyDown={handleSubmitOnKeyDownEnter}
												/>
											)}
										/>
									)}
								/>
								{isBank && (
									<>
										<Controller
											name="cheque_no"
											control={control}
											render={({ field }) => {
												return (
													<TextField
														{...field}
														className="mt-8 mb-16"
														error={!!errors?.cheque_no}
														helperText={errors?.cheque_no?.message}
														label="Cheque no"
														id="cheque_no"
														required
														variant="outlined"
														InputLabelProps={field?.value && { shrink: true }}
														fullWidth
													/>
												);
											}}
										/>

										<Controller
											name="account_no"
											control={control}
											render={({ field }) => {
												return (
													<TextField
														{...field}
														className="mt-8 mb-16"
														error={!!errors?.account_no}
														helperText={errors?.account_no?.message}
														label="Account no"
														id="account_no"
														required
														variant="outlined"
														InputLabelProps={field?.value && { shrink: true }}
														fullWidth
													/>
												);
											}}
										/>

										<Controller
											className="p-0"
											name={`cheque_date`}
											control={control}
											render={({ field }) => {
												return (
													<CustomDatePicker
														// className="p-0"
														field={field}
														previous_date_disable={true}
														label="Cheque Date"
														error={!!errors?.cheque_date}
														helperText={errors?.cheque_date?.message || ''}
														required
													/>
												);
											}}
										/>
									</>
								)}
								<Controller
									name="total_amount"
									control={control}
									render={({ field }) => {
										return (
											<TextField
												{...field}
												className="mt-8 mb-16"
												error={!!errors?.total_amount}
												helperText={errors?.total_amount?.message}
												label="Amount"
												id="total_amount"
												// id="total_amount"
												required
												variant="outlined"
												InputLabelProps={field.value && { shrink: true }}
												fullWidth
												onChange={e => {
													field.onChange(e);
													const isDefaultValueOrLess = e.target.value > defaultValue; // Replace 'defaultValue' with your actual default value
													console.log(
														'isDefaultValueOrLess',
														isDefaultValueOrLess,
														defaultValue,
														e
													);
													if (isDefaultValueOrLess) {
														setError('total_amount', {
															type: 'manual',
															message: 'Amount must be less than the default value'
														}); // Show an error message
													} else {
														clearErrors('total_amount');
													}
												}}
											/>
										);
									}}
								/>
								<Controller
									name="details"
									control={control}
									render={({ field }) => {
										return (
											<TextField
												{...field}
												className="mt-8 mb-16"
												error={!!errors?.details}
												helperText={errors?.details?.message}
												label="Note"
												id="details"
												required
												multiline
												rows={2}
												variant="outlined"
												InputLabelProps={field?.value && { shrink: true }}
												fullWidth
											/>
										);
									}}
								/>
							</FormProvider>
							<Button
								className="whitespace-nowrap mx-4"
								variant="contained"
								color="secondary"
								disabled={errors?.total_amount?.message}
								onClick={handleSavePaymentPF}
							>
								Save
							</Button>
						</CardContent>
					</Card>
				</>
			</Modal>
		</div>
	);
};

export default withRouter(PaymentPFsTable);
