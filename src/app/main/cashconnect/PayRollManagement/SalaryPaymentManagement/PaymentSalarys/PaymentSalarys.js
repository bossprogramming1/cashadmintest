import FusePageCarded from '@fuse/core/FusePageCarded';
import PagenotFound from 'app/main/cashconnect/Pagenotfound/PagenotFound';
import { getUserPermissions } from 'app/store/dataSlice';
import withReducer from 'app/store/withReducer';
import React, { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import reducer from '../store/index';
import PaymentSalarysHeader from './PaymentSalarysHeader';
import PaymentSalarysTable from './PaymentSalarysTable';

const Paymentsalarys = () => {
	const UserPermissions = useSelector(state => state.data.UserPermissions);
	useEffect(() => {
		dispatch(getUserPermissions());
	}, []);
	const dispatch = useDispatch();
	return (
		<FusePageCarded
			classes={{
				content: 'flex',
				contentCard: 'overflow-hidden',
				header: 'min-h-64 h-64'
			}}
			header={<PaymentSalarysHeader />}
			content={<PaymentSalarysTable />}
			innerScroll
		/>
	);
};
export default withReducer('paymentSalarysManagement', reducer)(Paymentsalarys);
