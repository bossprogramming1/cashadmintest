import FuseLoading from '@fuse/core/FuseLoading';
import FuseScrollbars from '@fuse/core/FuseScrollbars';
import _ from '@lodash';
import PlaylistAddCheckIcon from '@material-ui/icons/PlaylistAddCheck';
import DataUsageIcon from '@material-ui/icons/DataUsage';
import CancelIcon from '@material-ui/icons/Cancel';
import Button from '@mui/material/Button';
import Dialog from '@mui/material/Dialog';
import DialogActions from '@mui/material/DialogActions';
import DialogContent from '@mui/material/DialogContent';
import DialogContentText from '@mui/material/DialogContentText';
import DialogTitle from '@mui/material/DialogTitle';
import { Tooltip, Typography } from '@material-ui/core';
import Checkbox from '@material-ui/core/Checkbox';
import { makeStyles } from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TablePagination from '@material-ui/core/TablePagination';
import TableRow from '@material-ui/core/TableRow';
import DeleteIcon from '@material-ui/icons/Delete';
import EditIcon from '@material-ui/icons/Edit';
import PrintIcon from '@material-ui/icons/Print';
import { Pagination } from '@material-ui/lab';
import { rowsPerPageOptions } from 'app/@data/data';
import { motion } from 'framer-motion';
import React, { useEffect, useRef, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { withRouter } from 'react-router-dom';
import axios from 'axios';
import { getUserPermissions } from 'app/store/dataSlice';
import moment from 'moment';
import { SEARCH_SALARY_PAYMENT } from '../../../../../constant/constants';
// import PrintSalary from '../../AccountComponents/PrintSalary';
import { getPaymentSalarys, selectPaymentSalarys } from '../store/paymentSalarysSlice';
import PaymentSalarysTableHead from './PaymentSalarysTableHead';

const useStyles = makeStyles(() => ({
	root: {
		display: 'flex',
		justifyContent: 'space-between',
		flexWrap: 'nowrap',
		overflow: 'auto',
		minHeight: '35px'
	},
	toolbar: {
		'& > div': {
			minHeight: 'fit-content'
		}
	}
}));

const PaymentSalarysTable = props => {
	const UserPermissions = useSelector(state => state.data.UserPermissions);

	const dispatch = useDispatch();
	const classes = useStyles();
	const paymentSalarys = useSelector(selectPaymentSalarys);
	const searchText = useSelector(
		({ paymentSalarysManagement }) => paymentSalarysManagement.paymentSalarys.searchText
	);
	const [searchPaymentSalary, setSearchPaymentSalary] = useState([]);
	const [loading, setLoading] = useState(true);
	const [selected, setSelected] = useState([]);
	const [page, setPage] = useState(0);
	const [rowsPerPage, setRowsPerPage] = useState(30);
	const [pageAndSize, setPageAndSize] = useState({ page: 1, size: 30 });
	const currentDate = moment().format('DD-MM-YYYY');

	const [openPendingStatusAlert, setOpenPendingStatusAlert] = React.useState(false);
	const [openSuccessStatusAlert, setOpenSuccessStatusAlert] = React.useState(false);
	const [openCanceledStatusAlert, setOpenCanceledStatusAlert] = React.useState(false);
	const [openDeleteStatusAlert, setOpenDeleteStatusAlert] = React.useState(false);

	const [value, setValue] = useState();
	useEffect(() => {
		dispatch(getUserPermissions());
	}, []);
	const refresh = () => {
		// re-renders the component
		setValue({});
	};
	const [order, setOrder] = useState({
		direction: 'asc',
		id: null
	});

	let serialNumber = 1;
	const totalPages = sessionStorage.getItem('total_paymentSalarys_pages');
	const totalElements = sessionStorage.getItem('total_paymentSalarys_elements');

	const printSalaryRef = useRef();

	useEffect(() => {
		dispatch(getPaymentSalarys(pageAndSize)).then(() => setLoading(false));
	}, [dispatch]);

	useEffect(() => {
		searchText !== '' ? getSearchPaymentSalary() : setSearchPaymentSalary([]);
	}, [searchText]);

	const getSearchPaymentSalary = () => {
		const authTOKEN = {
			headers: {
				'Content-type': 'application/json',
				Authorization: localStorage.getItem('jwt_access_token')
			}
		};

		fetch(`${SEARCH_SALARY_PAYMENT}?invoice_no=${searchText}`, authTOKEN)
			.then(response => response.json())
			.then(searchedPaymentSalaryData => {
				setSearchPaymentSalary(searchedPaymentSalaryData?.payment_salarys);
			})
			.catch(() => setSearchPaymentSalary([]));
	};

	function handleRequestSort(_e, property) {
		const id = property;
		let direction = 'desc';

		if (order.id === property && order.direction === 'desc') {
			direction = 'asc';
		}

		setOrder({
			direction,
			id
		});
	}

	function handleSelectAllClick(paymentSalaryEvent) {
		if (paymentSalaryEvent.target.checked) {
			setSelected((!_.isEmpty(searchPaymentSalary) ? searchPaymentSalary : paymentSalarys).map(n => n.id));
			return;
		}
		setSelected([]);
	}

	function handleDeselect() {
		setSelected([]);
	}

	function handleUpdatePaymentSalary(item) {
		localStorage.removeItem('paymentSalaryEvent');
		props.history.push(`/apps/paymentSalary-management/paymentSalarys/${item.id}/${item.invoice_no}`);
	}
	function handleDeletePaymentSalary(item, paymentSalaryEvent) {
		localStorage.removeItem('paymentSalaryEvent');
		localStorage.setItem('paymentSalaryEvent', paymentSalaryEvent);
		props.history.push(`/apps/paymentSalary-management/paymentSalarys/${item.id}/${item.invoice_no}`);
	}

	function handleCheck(_e, id) {
		const selectedIndex = selected.indexOf(id);
		let newSelected = [];

		if (selectedIndex === -1) {
			newSelected = newSelected.concat(selected, id);
		} else if (selectedIndex === 0) {
			newSelected = newSelected.concat(selected.slice(1));
		} else if (selectedIndex === selected.length - 1) {
			newSelected = newSelected.concat(selected.slice(0, -1));
		} else if (selectedIndex > 0) {
			newSelected = newSelected.concat(selected.slice(0, selectedIndex), selected.slice(selectedIndex + 1));
		}

		setSelected(newSelected);
	}

	//pagination
	const handlePagination = (_e, handlePage) => {
		setPageAndSize({ ...pageAndSize, page: handlePage });
		setPage(handlePage - 1);
		dispatch(getPaymentSalarys({ ...pageAndSize, page: handlePage }));
	};

	function handleChangePage(_e, value) {
		setPage(value);
		setPageAndSize({ ...pageAndSize, page: value + 1 });
		dispatch(getPaymentSalarys({ ...pageAndSize, page: value + 1 }));
	}

	function handleChangeRowsPerPage(paymentSalaryEvent) {
		setRowsPerPage(paymentSalaryEvent.target.value);
		setPageAndSize({ ...pageAndSize, size: paymentSalaryEvent.target.value });
		dispatch(getPaymentSalarys({ ...pageAndSize, size: paymentSalaryEvent.target.value }));
	}

	if (loading) {
		return <FuseLoading />;
	}

	if (paymentSalarys?.length === 0) {
		return (
			<motion.div
				initial={{ opacity: 0 }}
				animate={{ opacity: 1, transition: { delay: 0.1 } }}
				className="flex flex-1 items-center justify-center h-full"
			>
				<Typography color="textSecondary" variant="h5">
					There are no paymentSalary!
				</Typography>
			</motion.div>
		);
	}
	const role = localStorage.getItem('user_role').toLowerCase();

	return (
		<div className="w-full flex flex-col">
			<FuseScrollbars className="flex-grow overflow-x-auto">
				{/* <PrintSalary ref={printSalaryRef} title="Payment Salary" type="payment" /> */}
				<Table stickyHeader className="min-w-xl" aria-labelledby="tableTitle">
					<PaymentSalarysTableHead
						selectedPaymentSalaryIds={selected}
						order={order}
						onSelectAllClick={handleSelectAllClick}
						onRequestSort={handleRequestSort}
						rowCount={(!_.isEmpty(searchPaymentSalary) ? searchPaymentSalary : paymentSalarys).length}
						onMenuItemClick={handleDeselect}
						pagination={pageAndSize}
					/>

					<TableBody>
						{_.orderBy(
							searchText !== '' && !_.isEmpty(searchPaymentSalary) ? searchPaymentSalary : paymentSalarys,
							[
								o => {
									switch (order.id) {
										case 'categories': {
											return o.categories[0];
										}
										default: {
											return o[order.id];
										}
									}
								}
							],
							[order.direction]
						).map(n => {
							const isSelected = selected.indexOf(n.id) !== -1;
							return (
								<TableRow
									className="h-52 cursor-pointer"
									hover
									role="checkbox"
									aria-checked={isSelected}
									tabIndex={-1}
									key={n.id}
									selected={isSelected}
								>
									<TableCell className="w-40 md:w-64" component="th" scope="row">
										{pageAndSize.page * pageAndSize.size - pageAndSize.size + serialNumber++}
									</TableCell>

									<TableCell className="p-4 md:p-16 whitespace-nowrap" component="th" scope="row">
										{n.date && moment(new Date(n.date)).format('DD-MM-YYYY')}
									</TableCell>
									<TableCell className="p-4 md:p-16" component="th" scope="row">
										{n.branch?.name}
									</TableCell>
									{/* <TableCell className="p-4 md:p-16" component="th" scope="row">
										{n.name}
									</TableCell> */}
									<TableCell className="p-4 md:p-16" component="th" scope="row">
										{n.invoice_no}
									</TableCell>

									<TableCell className="p-4 md:p-16" component="th" scope="row">
										{n.payment_account?.name}
									</TableCell>

									<TableCell className="p-4 md:p-16" component="th" scope="row">
										{n.total_amount}
									</TableCell>

									<TableCell className="p-4 md:p-16" align="center" component="th" scope="row">
										<div className="flex flex-nowrap">
											{/* <Tooltip title="Print" placement="top" enterDelay={300}>
												<PrintIcon
													className="cursor-pointer mr-3"
													style={{ color: 'blue' }}
													onClick={() => printSalaryRef.current.doPrint(n)}
												/>
											</Tooltip> */}
											<Tooltip title="Edit" placement="top" enterDelay={300}>
												<EditIcon
													style={{
														color: 'green'
													}}
													onClick={() => handleUpdatePaymentSalary(n)}
													className="cursor-pointer"
												/>
											</Tooltip>

											<Tooltip title="Delete" placement="top" enterDelay={300}>
												<DeleteIcon
													onClick={() => handleDeletePaymentSalary(n, 'Delete')}
													className="cursor-pointer"
													style={{
														color: 'red'
													}}
												/>
											</Tooltip>
										</div>
									</TableCell>
								</TableRow>
							);
						})}
					</TableBody>
				</Table>
			</FuseScrollbars>

			<div className={classes.root} id="pagiContainer">
				<Pagination
					classes={{ ul: 'flex-nowrap' }}
					count={totalPages}
					page={page + 1}
					defaultPage={1}
					color="primary"
					showFirstButton
					showLastButton
					variant="outlined"
					shape="rounded"
					onChange={handlePagination}
				/>

				<TablePagination
					classes={{ root: 'overflow-visible' }}
					rowsPerPageOptions={rowsPerPageOptions}
					component="div"
					count={totalElements}
					rowsPerPage={rowsPerPage}
					page={page}
					className={classes.toolbar}
					backIconButtonProps={{
						'aria-label': 'Previous Page',
						className: 'py-0'
					}}
					nextIconButtonProps={{
						'aria-label': 'Next Page',
						className: 'py-0'
					}}
					onChangePage={handleChangePage}
					onChangeRowsPerPage={handleChangeRowsPerPage}
				/>
			</div>
		</div>
	);
};

export default withRouter(PaymentSalarysTable);
