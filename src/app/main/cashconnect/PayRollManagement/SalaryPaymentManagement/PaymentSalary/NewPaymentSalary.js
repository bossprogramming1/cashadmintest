import FusePageCarded from '@fuse/core/FusePageCarded';
import { useDeepCompareEffect } from '@fuse/hooks';
import { yupResolver } from '@hookform/resolvers/yup';
import { Button, Typography } from '@material-ui/core';
import useUserInfo from 'app/@customHooks/useUserInfo';
import setIdIfValueIsObjArryData from 'app/@helpers/setIdIfValueIsObjArryData';
import setIdIfValueIsObject2 from 'app/@helpers/setIdIfValueIsObject2';

import PagenotFound from 'app/main/cashconnect/Pagenotfound/PagenotFound';
import { getUserPermissions } from 'app/store/dataSlice';
import withReducer from 'app/store/withReducer';
import { motion } from 'framer-motion';
import React, { useEffect, useState } from 'react';
import { FormProvider, useForm } from 'react-hook-form';
import { useDispatch, useSelector } from 'react-redux';
import { Link, useParams } from 'react-router-dom';
import * as yup from 'yup';
import reducer from '../store/index';
import {
	getPaymentSalary,
	newPaymentSalary,
	resetPaymentSalary,
	setUserBasedBranch
} from '../store/paymentSalarySlice';

import NewPaymentsalaryHeader from './NewPaymentSalaryHeader';
import PaymentsalaryForm from './PaymentSalaryForm';

/**
 * Form Validation Schema
 */
const schema = yup.object().shape({
	payment_account: yup.number().required('Account type is required'),
	date: yup.string().required('Date is required')
});

const PaymentSalary = () => {
	const dispatch = useDispatch();
	const paymentSalary = useSelector(({ paymentSalarysManagement }) => paymentSalarysManagement.paymentSalary);
	const [noPaymentSalary, setNoPaymentSalary] = useState(false);
	const methods = useForm({
		mode: 'onChange',
		defaultValues: {},
		resolver: yupResolver(schema)
	});

	const routeParams = useParams();
	const UserPermissions = useSelector(state => state.data.UserPermissions);
	useEffect(() => {
		dispatch(getUserPermissions());
	}, []);
	const { reset } = methods;

	const { userId } = useUserInfo();

	useDeepCompareEffect(() => {
		function updatePaymentSalaryState() {
			const { paymentSalaryId } = routeParams;

			if (paymentSalaryId === 'new') {
				localStorage.removeItem('event');
				/**
				 * Create New User data
				 */
				dispatch(newPaymentSalary());
				dispatch(setUserBasedBranch(userId));
			} else {
				/**
				 * Get User data
				 */

				dispatch(getPaymentSalary(paymentSalaryId)).then(action => {
					/**
					 * If the requested product is not exist show message
					 */
					if (!action.payload) {
						setNoPaymentSalary(true);
					}
				});
			}
		}

		updatePaymentSalaryState();
	}, [dispatch, routeParams]);

	useEffect(() => {
		if (!paymentSalary) {
			return;
		}
		/**
		 * Reset the form on paymentSalary state changes
		 */
		console.log('99999paymentSalary', paymentSalary);
		reset({
			...paymentSalary,
			employee:
				paymentSalary.calculation_for == 'employees'
					? paymentSalary.employee
					: paymentSalary?.payment_type == 'advanced'
					? paymentSalary.employee[0]
					: [],
			calculation_for: paymentSalary?.calculation_for
		});
	}, [paymentSalary, reset]);

	useEffect(() => {
		return () => {
			/**
			 * Reset PaymentSalary on component unload
			 */
			dispatch(resetPaymentSalary());
			setNoPaymentSalary(false);
		};
	}, [dispatch]);

	/**
	 * Show Message if the requested products is not exists
	 */
	if (noPaymentSalary) {
		return (
			<motion.div
				initial={{ opacity: 0 }}
				animate={{ opacity: 1, transition: { delay: 0.1 } }}
				className="flex flex-col flex-1 items-center justify-center h-full"
			>
				<Typography color="textSecondary" variant="h5">
					There is no such paymentSalary!
				</Typography>
				<Button
					className="mt-24"
					component={Link}
					variant="outlined"
					to="/apps/paymentSalary-management/paymentSalarys"
					color="inherit"
				>
					Go to PaymentSalary Page
				</Button>
			</motion.div>
		);
	}

	return (
		<FormProvider {...methods}>
			<FusePageCarded
				classes={{
					content: 'flex',
					contentCard: 'overflow-hidden',
					header: 'min-h-74 h-64'
				}}
				header={<NewPaymentsalaryHeader />}
				content={
					<div>
						<PaymentsalaryForm />
					</div>
				}
				innerScroll
			/>
		</FormProvider>
	);
};
export default withReducer('paymentSalarysManagement', reducer)(PaymentSalary);
