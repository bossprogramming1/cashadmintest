import { createAsyncThunk, createEntityAdapter, createSlice } from '@reduxjs/toolkit';
import axios from 'axios';
import { DELETE_PAYMENTSALARY_MULTIPLE, GET_PAYMENTSALARYS } from '../../../../../constant/constants';

export const getPaymentSalarys = createAsyncThunk(
	'paymentSalaryManagement/paymentSalarys/getPaymentSalarys',
	async pageAndSize => {
		axios.defaults.headers.common['Content-type'] = 'application/json';
		axios.defaults.headers.common.Authorization = localStorage.getItem('jwt_access_token');

		const response = axios.get(GET_PAYMENTSALARYS, { params: pageAndSize });
		const data = await response;

		sessionStorage.setItem('total_paymentSalarys_elements', data.data.total_elements);
		sessionStorage.setItem('total_paymentSalarys_pages', data.data.total_pages);
		delete axios.defaults.headers.common['Content-type'];
		delete axios.defaults.headers.common.Authorization;

		return data.data.salary_payments;
	}
);

export const removePaymentSalarys = createAsyncThunk(
	'paymentSalaryManagement/paymentSalarys/removePaymentSalarys',
	async paymentSalaryIds => {
		const headers = {
			'Content-type': 'application/json',
			Authorization: localStorage.getItem('jwt_access_token')
		};
		const data = {
			ids: paymentSalaryIds
		};
		const response = await axios.delete(`${DELETE_PAYMENTSALARY_MULTIPLE}`, { headers, data });

		return response;
	}
);

const paymentSalarysAdapter = createEntityAdapter({});

export const { selectAll: selectPaymentSalarys, selectById: selectPaymentSalaryById } =
	paymentSalarysAdapter.getSelectors(state => state.paymentSalarysManagement.paymentSalarys);

const paymentSalarySlice = createSlice({
	name: 'paymentSalaryManagement/paymentSalarys',
	initialState: paymentSalarysAdapter.getInitialState({
		searchText: ''
	}),
	reducers: {
		setPaymentSalarysSearchText: {
			reducer: (state, action) => {
				state.searchText = action.payload;
			},
			prepare: event => ({ payload: event.target.value || '' })
		}
	},
	extraReducers: {
		[getPaymentSalarys.fulfilled]: paymentSalarysAdapter.setAll
	}
});

export const { setData, setPaymentSalarysSearchText } = paymentSalarySlice.actions;
export default paymentSalarySlice.reducer;
