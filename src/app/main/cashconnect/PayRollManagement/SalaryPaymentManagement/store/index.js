import { combineReducers } from '@reduxjs/toolkit';
import paymentSalary from './paymentSalarySlice';
import paymentSalarys from './paymentSalarysSlice';

const reducer = combineReducers({
	paymentSalary,
	paymentSalarys
});

export default reducer;
