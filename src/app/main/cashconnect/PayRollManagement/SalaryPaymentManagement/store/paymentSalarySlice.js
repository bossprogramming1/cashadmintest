import { createAsyncThunk, createSlice } from '@reduxjs/toolkit';
import { ledgerCashId, ledgerPayableId } from 'app/@data/data';
import axios from 'axios';
import moment from 'moment';
import {
	BRANCH_BY_USER_ID,
	CREATE_ADVANCED_PAYMENTSALARY,
	CREATE_PAYMENTSALARY,
	DELETE_PAYMENTSALARY,
	GET_PAYMENTSALARY_BY_ID,
	UPDATE_PAYMENTSALARY
} from '../../../../../constant/constants';

export const getPaymentSalary = createAsyncThunk(
	'paymentSalaryManagement/paymentSalary/getPaymentSalary',
	async (params, { rejectWithValue }) => {
		const authTOKEN = {
			headers: {
				'Content-type': 'application/json',
				Authorization: localStorage.getItem('jwt_access_token')
			}
		};

		try {
			const response = await axios.get(`${GET_PAYMENTSALARY_BY_ID}${params}`, authTOKEN);
			const data = await response.data;
			return data === undefined ? null : data;
		} catch (err) {
			return rejectWithValue(params);
		}
	}
);

export const removePaymentSalary = createAsyncThunk(
	'paymentSalaryManagement/paymentSalary/removePaymentSalary',
	async paymentSalaryId => {
		const authTOKEN = {
			headers: {
				'Content-type': 'application/json',
				Authorization: localStorage.getItem('jwt_access_token')
			}
		};

		const response = await axios.delete(`${DELETE_PAYMENTSALARY}${paymentSalaryId}`, authTOKEN);
		return response;
	}
);

export const updatePaymentSalary = createAsyncThunk(
	'paymentSalaryManagement/paymentSalary/updatePaymentSalary',
	async paymentSalaryData => {
		const authTOKEN = {
			headers: {
				'Content-type': 'application/json',
				Authorization: localStorage.getItem('jwt_access_token')
			}
		};
		const response = await axios.put(
			`${UPDATE_PAYMENTSALARY}${paymentSalaryData.id}`,
			paymentSalaryData,
			authTOKEN
		);
		return response;
	}
);

export const savePaymentSalary = createAsyncThunk(
	'paymentSalaryManagement/paymentSalary/savePaymentSalary',
	async paymentSalaryData => {
		const authTOKEN = {
			headers: {
				'Content-type': 'application/json',
				Authorization: localStorage.getItem('jwt_access_token')
			}
		};
		const response = await axios.post(`${CREATE_PAYMENTSALARY}`, paymentSalaryData, authTOKEN);
		return response;
	}
);
export const saveAdvancePaymentSalary = createAsyncThunk(
	'paymentSalaryManagement/paymentSalary/saveAdvancePaymentSalary',
	async paymentSalaryData => {
		const authTOKEN = {
			headers: {
				'Content-type': 'application/json',
				Authorization: localStorage.getItem('jwt_access_token')
			}
		};
		const response = await axios.post(`${CREATE_ADVANCED_PAYMENTSALARY}`, paymentSalaryData, authTOKEN);
		return response;
	}
);

export const setUserBasedBranch = createAsyncThunk(
	'paymentSalaryManagement/paymentSalary/setUserBasedBranch',
	async userId => {
		const authTOKEN = {
			headers: {
				'Content-type': 'application/json',
				Authorization: localStorage.getItem('jwt_access_token')
			}
		};
		const response = await axios.get(`${BRANCH_BY_USER_ID}${userId}`, authTOKEN);
		return response.data.id || {};
	}
);

const paymentSalarySlice = createSlice({
	name: 'paymentSalaryManagement/paymentSalary',
	initialState: null,
	reducers: {
		resetPaymentSalary: () => null,
		newPaymentSalary: {
			reducer: (_state, action) => action.payload,
			prepare: () => ({
				payload: {
					date: '',

					payment_type: 'regular',
					details: '',
					// employee: [],
					// department: [],
					// total_amount: '',
					payment_account: '',
					branch: ''
				}
			})
		}
	},
	extraReducers: {
		[getPaymentSalary.fulfilled]: (_state, action) => action.payload,
		[savePaymentSalary.fulfilled]: (_state, action) => action.payload,
		[saveAdvancePaymentSalary.fulfilled]: (_state, action) => action.payload,
		[removePaymentSalary.fulfilled]: (_state, action) => action.payload,
		[updatePaymentSalary.fulfilled]: (_state, action) => action.payload,
		[setUserBasedBranch.fulfilled]: (state, action) => {
			return {
				...state,
				branch: action.payload
			};
		}
	}
});

export const { newPaymentSalary, resetPaymentSalary } = paymentSalarySlice.actions;

export default paymentSalarySlice.reducer;
