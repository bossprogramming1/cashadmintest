import { combineReducers } from '@reduxjs/toolkit';
import compute from './computeSlice';
import computes from './computesSlice';

const reducer = combineReducers({
	compute,
	computes
});

export default reducer;
