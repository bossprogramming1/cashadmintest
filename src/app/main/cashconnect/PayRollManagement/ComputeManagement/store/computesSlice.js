import { createAsyncThunk, createEntityAdapter, createSlice } from '@reduxjs/toolkit';
import { DELETE_COMPUTE, GET_COMPUTES } from 'app/constant/constants';
import axios from 'axios';

export const getComputes = createAsyncThunk('computeManagement/computes/getComputes', async parameter => {
	const { page, size } = parameter;
	axios.defaults.headers.common['Content-type'] = 'application/json';
	axios.defaults.headers.common.Authorization = localStorage.getItem('jwt_access_token');

	const response = axios.get(GET_COMPUTES, { params: { page, size } });
	const data = await response;

	sessionStorage.setItem('computes_total_elements', data.data.total_elements);
	sessionStorage.setItem('computes_total_pages', data.data.total_pages);
	delete axios.defaults.headers.common['Content-type'];
	delete axios.defaults.headers.common.Authorization;

	return data.data.computes;
});

export const removeComputes = createAsyncThunk(
	'computeManagement/computes/removeComputes',
	async (computeIds, { dispatch, getState }) => {
		const authTOKEN = {
			headers: {
				'Content-type': 'application/json',
				Authorization: localStorage.getItem('jwt_access_token')
			}
		};
		await axios.delete(`${DELETE_COMPUTE}`, { computeIds }, authTOKEN);

		return computeIds;
	}
);

const computesAdapter = createEntityAdapter({});

export const { selectAll: selectComputes, selectById: selectComputeById } = computesAdapter.getSelectors(
	state => state.computesManagement.computes
);

const computesSlice = createSlice({
	name: 'computeManagement/computes',
	initialState: computesAdapter.getInitialState({
		searchText: ''
	}),
	reducers: {
		setComputesSearchText: {
			reducer: (state, action) => {
				state.searchText = action.payload;
			},
			prepare: event => ({ payload: event.target.value || '' })
		}
	},
	extraReducers: {
		[getComputes.fulfilled]: computesAdapter.setAll
	}
});

export const { setData, setComputesSearchText } = computesSlice.actions;
export default computesSlice.reducer;
