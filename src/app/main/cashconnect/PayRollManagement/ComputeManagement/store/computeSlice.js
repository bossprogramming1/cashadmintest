import { createAsyncThunk, createSlice } from '@reduxjs/toolkit';
import { CREATE_COMPUTE, DELETE_COMPUTE, GET_COMPUTEID, UPDATE_COMPUTE } from 'app/constant/constants';
import axios from 'axios';

export const getCompute = createAsyncThunk(
	'computeManagement/compute/getCompute',
	async (params, { rejectWithValue }) => {
		const authTOKEN = {
			headers: {
				'Content-type': 'application/json',
				Authorization: localStorage.getItem('jwt_access_token')
			}
		};

		try {
			const response = await axios.get(`${GET_COMPUTEID}${params}`, authTOKEN);
			const data = await response.data;
			return data === undefined ? null : data;
		} catch (err) {
			return rejectWithValue(params);
		}
	}
);

export const removeCompute = createAsyncThunk('computeManagement/compute/removeCompute', async val => {
	const authTOKEN = {
		headers: {
			'Content-type': 'application/json',
			Authorization: localStorage.getItem('jwt_access_token')
		}
	};

	const computeId = val.id;
	const response = await axios.delete(`${DELETE_COMPUTE}${computeId}`, authTOKEN);
	return response;
});

export const updateCompute = createAsyncThunk(
	'computeManagement/compute/updateCompute',
	async (computeData, { dispatch, getState }) => {
		const { compute } = getState().computesManagement;

		const authTOKEN = {
			headers: {
				'Content-type': 'application/json',
				Authorization: localStorage.getItem('jwt_access_token')
			}
		};
		const response = await axios.put(`${UPDATE_COMPUTE}${compute.id}`, computeData, authTOKEN);
		return response;
	}
);

export const saveCompute = createAsyncThunk('computeManagement/compute/saveCompute', async computeData => {
	const authTOKEN = {
		headers: {
			'Content-type': 'application/json',
			Authorization: localStorage.getItem('jwt_access_token')
		}
	};
	const response = await axios.post(`${CREATE_COMPUTE}`, computeData, authTOKEN);
	return response;
});

const computeSlice = createSlice({
	name: 'computeManagement/compute',
	initialState: null,
	reducers: {
		resetCompute: () => null,
		newCompute: {
			reducer: (state, action) => action.payload,
			prepare: event => ({
				payload: {
					name: ''
				}
			})
		}
	},
	extraReducers: {
		[getCompute.fulfilled]: (state, action) => action.payload,
		[saveCompute.fulfilled]: (state, action) => action.payload,
		[removeCompute.fulfilled]: (state, action) => action.payload,
		[updateCompute.fulfilled]: (state, action) => action.payload
	}
});

export const { newCompute, resetCompute } = computeSlice.actions;

export default computeSlice.reducer;
