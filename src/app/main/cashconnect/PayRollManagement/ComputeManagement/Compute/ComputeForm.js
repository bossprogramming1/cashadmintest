import _ from '@lodash';
import { makeStyles } from '@material-ui/core/styles';
import TextField from '@material-ui/core/TextField';
import { Autocomplete } from '@material-ui/lab';
import React, { useEffect } from 'react';
import { Controller, useFormContext } from 'react-hook-form';
import { useDispatch, useSelector } from 'react-redux';
import { useHistory, useParams } from 'react-router-dom';
// import { getCities } from '../../../../store/dataSlice';
import { saveCompute, updateCompute } from '../store/computeSlice';

const useStyles = makeStyles(theme => ({
	hidden: {
		display: 'none'
	}
}));

function ComputeForm(props) {
	const userID = localStorage.getItem('user_id');

	const citys = useSelector(state => state.data.cities);

	const classes = useStyles(props);

	const methods = useFormContext();
	const { control, formState, getValues } = methods;
	const { errors, isValid, dirtyFields } = formState;
	const routeParams = useParams();
	const { computeId } = routeParams;
	const history = useHistory();
	const handleDelete = localStorage.getItem('computeEvent');
	const dispatch = useDispatch();



	function handleSaveCompute() {
		dispatch(saveCompute(getValues())).then(res => {
			if (res.payload) {
				localStorage.setItem('computeAlert', 'saveCompute');
				history.push('/apps/compute-management/computes');
			}
		});
	}

	function handleUpdateCompute() {
		dispatch(updateCompute(getValues())).then(res => {
			if (res.payload) {
				localStorage.setItem('computeAlert', 'updateCompute');
				history.push('/apps/compute-management/computes');
			}
		});
	}

	const handleSubmitOnKeyDownEnter = ev => {
		if (ev.key === 'Enter') {
			if (routeParams.computeId === 'new' && !(_.isEmpty(dirtyFields) || !isValid)) {
				handleSaveCompute();
			} else if (handleDelete !== 'Delete' && routeParams?.computeName) {
				handleUpdateCompute();
			}
		}
	};

	return (
		<div>
			<Controller
				name="name"
				control={control}
				render={({ field }) => {
					return (
						<TextField
							{...field}
							className="mt-8 mb-16"
							error={!!errors?.name}
							helperText={errors?.name?.message}
							label="Compute Name"
							id="name"
							required
							variant="outlined"
							InputLabelProps={field.value && { shrink: true }}
							fullWidth
							onKeyDown={handleSubmitOnKeyDownEnter}
						/>
					);
				}}
			/>

			
		</div>
	);
}

export default ComputeForm;
