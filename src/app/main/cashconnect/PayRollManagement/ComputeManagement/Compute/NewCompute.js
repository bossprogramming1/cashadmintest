import FusePageCarded from '@fuse/core/FusePageCarded';
import { useDeepCompareEffect } from '@fuse/hooks';
import { yupResolver } from '@hookform/resolvers/yup';
import { Button, Typography } from '@material-ui/core';
import withReducer from 'app/store/withReducer';
import { motion } from 'framer-motion';
import React, { useEffect, useState } from 'react';
import { FormProvider, useForm } from 'react-hook-form';
import { useDispatch, useSelector } from 'react-redux';
import { Link, useParams } from 'react-router-dom';
import * as yup from 'yup';
import reducer from '../store/index';
import { getCompute, newCompute, resetCompute } from '../store/computeSlice';
import ComputeForm from './ComputeForm';
import NewComputeHeader from './NewComputeHeader';

/**
 * Form Validation Schema
 */
const schema = yup.object().shape({
	name: yup.string().required('Name is required')
});

const Compute = () => {
	const dispatch = useDispatch();
	const compute = useSelector(({ computesManagement }) => computesManagement.compute);

	const [noCompute, setNoCompute] = useState(false);
	const methods = useForm({
		mode: 'onChange',
		defaultValues: {},
		resolver: yupResolver(schema)
	});
	const routeParams = useParams();

	const { reset } = methods;

	useDeepCompareEffect(() => {
		function updateComputeState() {
			const { computeId } = routeParams;

			if (computeId === 'new') {
				localStorage.removeItem('event');
				/**
				 * Create New User data
				 */
				dispatch(newCompute());
			} else {
				/**
				 * Get User data
				 */

				dispatch(getCompute(computeId)).then(action => {
					/**
					 * If the requested product is not exist show message
					 */
					if (!action.payload) {
						setNoCompute(true);
					}
				});
			}
		}

		updateComputeState();
	}, [dispatch, routeParams]);

	useEffect(() => {}, []);

	useEffect(() => {
		if (!compute) {
			return;
		}
		/**
		 * Reset the form on compute state changes
		 */
		reset(compute);
	}, [compute, reset]);

	useEffect(() => {
		return () => {
			/**
			 * Reset Compute on component unload
			 */
			dispatch(resetCompute());
			setNoCompute(false);
		};
	}, [dispatch]);

	/**
	 * Show Message if the requested products is not exists
	 */
	if (noCompute) {
		return (
			<motion.div
				initial={{ opacity: 0 }}
				animate={{ opacity: 1, transition: { delay: 0.1 } }}
				className="flex flex-col flex-1 items-center justify-center h-full"
			>
				<Typography color="textSecondary" variant="h5">
					There is no such compute!
				</Typography>
				<Button
					className="mt-24"
					component={Link}
					variant="outlined"
					to="/apps/e-commerce/products"
					color="inherit"
				>
					Go to Compute Page
				</Button>
			</motion.div>
		);
	}

	return (
		<FormProvider {...methods}>
			<FusePageCarded
				classes={{
					toolbar: 'p-0',
					header: 'min-h-80 h-80'
				}}
				header={<NewComputeHeader />}
				content={
					<div className="p-16 sm:p-24">
						<ComputeForm />
					</div>
				}
				innerScroll
			/>
		</FormProvider>
	);
};
export default withReducer('computesManagement', reducer)(Compute);
