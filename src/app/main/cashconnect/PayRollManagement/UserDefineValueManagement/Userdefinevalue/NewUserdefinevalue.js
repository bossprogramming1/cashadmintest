import FusePageCarded from '@fuse/core/FusePageCarded';
import { useDeepCompareEffect } from '@fuse/hooks';
import { yupResolver } from '@hookform/resolvers/yup';
import { Button, Typography } from '@material-ui/core';
import withReducer from 'app/store/withReducer';
import { motion } from 'framer-motion';
import React, { useEffect, useState } from 'react';
import { FormProvider, useForm } from 'react-hook-form';
import { useDispatch, useSelector } from 'react-redux';
import { Link, useParams } from 'react-router-dom';
import * as yup from 'yup';
import reducer from '../store/index';
import { getUserdefinevalue, newUserdefinevalue, resetUserdefinevalue } from '../store/userdefinevalueSlice';
import NewUserdefinevalueHeader from './NewUserdefinevalueHeader';
import UserdefinevalueForm from './UserdefinevalueForm';

/**
 * Form Validation Schema
 */
const schema = yup.object().shape({
	value: yup.number().required('Value is required')
});

const Userdefinevalue = () => {
	const dispatch = useDispatch();
	const userdefinevalue = useSelector(({ userdefinevaluesManagement }) => userdefinevaluesManagement.userdefinevalue);

	const [noUserdefinevalue, setNoUserdefinevalue] = useState(false);
	const methods = useForm({
		mode: 'onChange',
		defaultValues: {},
		resolver: yupResolver(schema)
	});
	const routeParams = useParams();

	const { reset } = methods;

	useDeepCompareEffect(() => {
		function updateUserdefinevalueState() {
			const { userdefinevalueId } = routeParams;

			if (userdefinevalueId === 'new') {
				localStorage.removeItem('event');
				/**
				 * Create New User data
				 */
				dispatch(newUserdefinevalue());
			} else {
				/**
				 * Get User data
				 */

				dispatch(getUserdefinevalue(userdefinevalueId)).then(action => {
					/**
					 * If the requested product is not exist show message
					 */
					if (!action.payload) {
						setNoUserdefinevalue(true);
					}
				});
			}
		}

		updateUserdefinevalueState();
	}, [dispatch, routeParams]);

	useEffect(() => {}, []);

	useEffect(() => {
		if (!userdefinevalue) {
			return;
		}
		/**
		 * Reset the form on userdefinevalue state changes
		 */
		reset({
			calculation_for: userdefinevalue?.payhead_assignments?.calculation_for,
			date: userdefinevalue?.payhead_assignments?.date,
			department: userdefinevalue?.payhead_assignments?.department,
			id: userdefinevalue?.payhead_assignments?.id,
			unit: userdefinevalue?.payhead_assignments?.unit,
			value: userdefinevalue?.payhead_assignments?.value,
			payhead: userdefinevalue?.payhead_assignments?.payheads,
			employee: userdefinevalue?.payhead_assignments?.employees
		});
	}, [userdefinevalue, reset]);

	useEffect(() => {
		return () => {
			/**
			 * Reset Userdefinevalue on component unload
			 */
			dispatch(resetUserdefinevalue());
			setNoUserdefinevalue(false);
		};
	}, [dispatch]);

	/**
	 * Show Message if the requested products is not exists
	 */
	if (noUserdefinevalue) {
		return (
			<motion.div
				initial={{ opacity: 0 }}
				animate={{ opacity: 1, transition: { delay: 0.1 } }}
				className="flex flex-col flex-1 items-center justify-center h-full"
			>
				<Typography color="textSecondary" variant="h5">
					There is no such userdefinevalue!
				</Typography>
				<Button
					className="mt-24"
					component={Link}
					variant="outlined"
					to="/apps/e-commerce/products"
					color="inherit"
				>
					Go to Userdefinevalue Page
				</Button>
			</motion.div>
		);
	}

	return (
		<FormProvider {...methods}>
			<FusePageCarded
				classes={{
					toolbar: 'p-0',
					header: 'min-h-80 h-80'
				}}
				header={<NewUserdefinevalueHeader />}
				content={
					<div className="p-16 sm:p-24">
						<UserdefinevalueForm />
					</div>
				}
				innerScroll
			/>
		</FormProvider>
	);
};
export default withReducer('userdefinevaluesManagement', reducer)(Userdefinevalue);
