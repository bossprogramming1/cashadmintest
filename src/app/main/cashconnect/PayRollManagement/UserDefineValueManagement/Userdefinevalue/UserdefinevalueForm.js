import _ from '@lodash';
import { makeStyles } from '@material-ui/core/styles';
import TextField from '@material-ui/core/TextField';
import { Autocomplete } from '@material-ui/lab';
import React, { useEffect, useState } from 'react';
import { Controller, useFormContext } from 'react-hook-form';
import { useDispatch, useSelector } from 'react-redux';
import { useHistory, useParams } from 'react-router-dom';
import FormControl from '@material-ui/core/FormControl';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Radio from '@material-ui/core/Radio';
import RadioGroup from '@material-ui/core/RadioGroup';
import FormLabel from '@material-ui/core/FormLabel';
import { compound } from 'app/@data/data';
import { getDepartments, getEmployees, getPayheadOnlyUserDefineValue, getUnits } from 'app/store/dataSlice';
import { GET_PAYHEAD_ONLY_USERDEFINEVALUES } from 'app/constant/constants';
import CustomDatePicker from 'app/@components/CustomDatePicker';
import { saveUserdefinevalue, updateUserdefinevalue } from '../store/userdefinevalueSlice';

const useStyles = makeStyles(theme => ({
	hidden: {
		display: 'none'
	}
}));

function UserdefinevalueForm(props) {
	const userID = localStorage.getItem('user_id');

	const employees = useSelector(state => state.data.employees);
	const userDefinedValuePayhead = useSelector(state => state.data.userDefinedValuePayhead);
	const departments = useSelector(state => state.data?.departments);
	const units = useSelector(state => state.data?.units);

	const [selectedRadio, setSelectedRadio] = useState('');
	const [inputField, setInputField] = useState(false);

	const classes = useStyles(props);

	const methods = useFormContext();
	const { control, formState, getValues, setValue } = methods;
	const { errors, isValid, dirtyFields } = formState;
	const routeParams = useParams();
	const { userdefinevalueId } = routeParams;
	const history = useHistory();
	const handleDelete = localStorage.getItem('userdefinevalueEvent');
	const dispatch = useDispatch();

	useEffect(() => {
		dispatch(getEmployees());
		dispatch(getPayheadOnlyUserDefineValue());
		dispatch(getDepartments());
		dispatch(getUnits());
	}, []);

	useEffect(() => {
		if (userdefinevalueId !== 'new') {
			setSelectedRadio(getValues().calculation_for);
			setInputField(true);
		}
	}, [userdefinevalueId, getValues().calculation_for]);
	function handleSaveUserdefinevalue() {
		dispatch(saveUserdefinevalue(getValues())).then(res => {
			if (res.payload) {
				localStorage.setItem('userdefinevalueAlert', 'saveUserdefinevalue');
				history.push('/apps/userdefinevalue-management/userdefinevalues');
			}
		});
	}

	function handleUpdateUserdefinevalue() {
		dispatch(updateUserdefinevalue(getValues())).then(res => {
			if (res.payload) {
				localStorage.setItem('userdefinevalueAlert', 'updateUserdefinevalue');
				history.push('/apps/userdefinevalue-management/userdefinevalues');
			}
		});
	}

	const handleSubmitOnKeyDownEnter = ev => {
		if (ev.key === 'Enter') {
			if (routeParams.userdefinevalueId === 'new' && !(_.isEmpty(dirtyFields) || !isValid)) {
				handleSaveUserdefinevalue();
			} else if (handleDelete !== 'Delete' && routeParams?.userdefinevalueName) {
				handleUpdateUserdefinevalue();
			}
		}
	};

	return (
		<div>
			<Controller
				className="p-0"
				name={`date`}
				control={control}
				render={({ field }) => {
					return (
						<CustomDatePicker
							className="p-0"
							// previous_date_disable={true}
							field={field}
							// style={{ maxWidth: '130px' }}
							views={['month']}
							voucherOfDate={true}
							label="Dtae"
							error={!!errors?.voucher_date} // Check if there's an error for the "date_to" field
							helperText={errors?.voucher_date?.message || ''}
						/>
					);
				}}
			/>
			<Controller
				name="calculation_for"
				control={control}
				render={({ field }) => (
					<FormControl component="fieldset">
						<FormLabel component="legend">How would you like to show?</FormLabel>
						<RadioGroup
							row
							aria-label="position"
							name="position"
							defaultValue="top"
							onChange={event => {
								if (event.target.value == 'all') {
									setSelectedRadio(event.target.value);
									// setSelectedValues('All');
									// setSalaryTable(true);
									setInputField(true);

									setValue('department', []);
									setValue('employee', []);
								} else if (event.target.value == 'department') {
									setSelectedRadio(event.target.value);
									setInputField(true);

									setValue('employee', []);
									// setSelectedValues('');
								} else if (event.target.value == 'employees') {
									setSelectedRadio(event.target.value);
									setInputField(true);

									setValue('department', []);
									// setSelectedValues('');
								} else {
									setSelectedRadio(event.target.value);
									// setSelectedValues('');
								}
							}}
						>
							<FormControlLabel
								{...field}
								value="all"
								control={
									<Radio
										checked={field.value === 'all' ? field.value : false}
										style={{ color: '#22d3ee' }}
									/>
								}
								label="All"
							/>
							<FormControlLabel
								{...field}
								value="department"
								control={
									<Radio
										checked={field.value === 'department' ? field.value : false}
										style={{ color: 'green' }}
									/>
								}
								label="Department"
							/>
							<FormControlLabel
								{...field}
								value="employees"
								control={
									<Radio
										checked={field.value === 'employees' ? field.value : false}
										style={{ color: 'red' }}
									/>
								}
								label="Employees"
							/>
						</RadioGroup>
					</FormControl>
				)}
			/>
			{selectedRadio === 'department' && (
				<Controller
					name="department"
					control={control}
					render={({ field: { onChange, value, name } }) => (
						<Autocomplete
							className="mt-8 mb-16"
							freeSolo
							multiple
							filterSelectedOptions
							value={value ? departments.filter(data => value.includes(data.id)) : []}
							options={departments}
							getOptionLabel={option => `${option?.name}`}
							onChange={(event, newValue) => {
								const selectedValues = newValue.map(option => option.id);
								onChange(selectedValues);
							}}
							renderInput={params => {
								return (
									<TextField
										{...params}
										placeholder="Select departments"
										label="Departments"
										error={!!errors.department}
										required
										autoFocus
										helperText={errors?.department?.message}
										variant="outlined"
										InputLabelProps={{
											shrink: true
										}}
									/>
								);
							}}
						/>
					)}
				/>
			)}
			{selectedRadio === 'employees' && (
				<Controller
					name="employee"
					control={control}
					render={({ field: { onChange, value, name } }) => (
						<Autocomplete
							className="mt-8 mb-16"
							freeSolo
							multiple
							filterSelectedOptions
							value={value ? employees.filter(data => value.includes(data.id)) : []}
							options={employees}
							getOptionLabel={option => `${option?.first_name} ${option?.last_name}`}
							onChange={(event, newValue) => {
								const selectedValues = newValue.map(option => option.id);
								onChange(selectedValues);
							}}
							renderInput={params => {
								return (
									<TextField
										{...params}
										placeholder="Select Employee"
										label="Employee"
										error={!!errors.employee}
										required
										autoFocus
										helperText={errors?.employee?.message}
										variant="outlined"
										InputLabelProps={{
											shrink: true
										}}
									/>
								);
							}}
						/>
					)}
				/>
			)}

			{inputField && (
				<>
					<Controller
						name="payhead"
						control={control}
						render={({ field: { onChange, value, name } }) => (
							<Autocomplete
								className="mt-8 mb-16"
								freeSolo
								value={value ? userDefinedValuePayhead.find(data => data.id === value) : null}
								options={userDefinedValuePayhead}
								getOptionLabel={option => `${option?.name}`}
								onChange={(event, newValue) => {
									onChange(newValue?.id);
								}}
								renderInput={params => (
									<TextField
										{...params}
										placeholder="Select Payhead"
										label="Payhead"
										error={!!errors.payhead}
										required
										helperText={errors?.payhead?.message}
										variant="outlined"
										InputLabelProps={{
											shrink: true
										}}
										// onKeyDown={handleSubmitOnKeyDownEnter}
									/>
								)}
							/>
						)}
					/>

					<Controller
						name="value"
						control={control}
						render={({ field }) => {
							return (
								<TextField
									{...field}
									className="mt-8 mb-16"
									error={!!errors?.value}
									helperText={errors?.value?.message}
									label="Value"
									id="value"
									required
									autoFocus
									variant="outlined"
									InputLabelProps={field.value && { shrink: true }}
									fullWidth
									onKeyDown={handleSubmitOnKeyDownEnter}
								/>
							);
						}}
					/>
					<Controller
						name="unit"
						control={control}
						render={({ field: { onChange, value, name } }) => (
							<Autocomplete
								className="mt-8 mb-16"
								freeSolo
								value={value ? units.find(data => data.id === value) : null}
								options={units}
								getOptionLabel={option => `${option?.symbol}`}
								onChange={(event, newValue) => {
									onChange(newValue?.id);
								}}
								renderInput={params => (
									<TextField
										{...params}
										placeholder="Select unit"
										label="Unit"
										error={!!errors.unit}
										required
										helperText={errors?.unit?.message}
										variant="outlined"
										InputLabelProps={{
											shrink: true
										}}
										// onKeyDown={handleSubmitOnKeyDownEnter}
									/>
								)}
							/>
						)}
					/>
				</>
			)}

			{/* <Controller
				name="city"
				control={control}
				render={({ field: { onChange, value, name } }) => (
					<Autocomplete
						className="mt-8 mb-16"
						freeSolo
						value={value ? citys.find(data => data.id === value) : null}
						options={citys}
						getOptionLabel={option => `${option?.name}`}
						onChange={(event, newValue) => {
							onChange(newValue?.id);
						}}
						renderInput={params => (
							<TextField
								{...params}
								placeholder="Select City"
								label="City"
								error={!!errors.city}
								required
								helperText={errors?.city?.message}
								variant="outlined"
								autoFocus
								InputLabelProps={{
									shrink: true
								}}
								// onKeyDown={handleSubmitOnKeyDownEnter}
							/>
						)}
					/>
				)}
			/> */}
		</div>
	);
}

export default UserdefinevalueForm;
