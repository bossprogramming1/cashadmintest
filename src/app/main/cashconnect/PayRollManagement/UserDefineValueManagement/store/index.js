import { combineReducers } from '@reduxjs/toolkit';
import userdefinevalue from './userdefinevalueSlice';
import userdefinevalues from './userdefinevaluesSlice';

const reducer = combineReducers({
	userdefinevalue,
	userdefinevalues
});

export default reducer;
