import { createAsyncThunk, createEntityAdapter, createSlice } from '@reduxjs/toolkit';
import { DELETE_USERDEFINEVALUE, GET_USERDEFINEVALUES } from 'app/constant/constants';
import axios from 'axios';

export const getUserdefinevalues = createAsyncThunk(
	'userdefinevalueManagement/userdefinevalues/getUserdefinevalues',
	async parameter => {
		const { page, size } = parameter;
		axios.defaults.headers.common['Content-type'] = 'application/json';
		axios.defaults.headers.common.Authorization = localStorage.getItem('jwt_access_token');

		const response = axios.get(GET_USERDEFINEVALUES, { params: { page, size } });
		const data = await response;

		sessionStorage.setItem('userdefinevalues_total_elements', data.data.total_elements);
		sessionStorage.setItem('userdefinevalues_total_pages', data.data.total_pages);
		delete axios.defaults.headers.common['Content-type'];
		delete axios.defaults.headers.common.Authorization;

		return data.data.user_defined_values;
	}
);

export const removeUserdefinevalues = createAsyncThunk(
	'userdefinevalueManagement/userdefinevalues/removeUserdefinevalues',
	async (userdefinevalueIds, { dispatch, getState }) => {
		const authTOKEN = {
			headers: {
				'Content-type': 'application/json',
				Authorization: localStorage.getItem('jwt_access_token')
			}
		};
		await axios.delete(`${DELETE_USERDEFINEVALUE}`, { userdefinevalueIds }, authTOKEN);

		return userdefinevalueIds;
	}
);

const userdefinevaluesAdapter = createEntityAdapter({});

export const { selectAll: selectUserdefinevalues, selectById: selectUserdefinevalueById } =
	userdefinevaluesAdapter.getSelectors(state => state.userdefinevaluesManagement.userdefinevalues);

const userdefinevaluesSlice = createSlice({
	name: 'userdefinevalueManagement/userdefinevalues',
	initialState: userdefinevaluesAdapter.getInitialState({
		searchText: ''
	}),
	reducers: {
		setUserdefinevaluesSearchText: {
			reducer: (state, action) => {
				state.searchText = action.payload;
			},
			prepare: event => ({ payload: event.target.value || '' })
		}
	},
	extraReducers: {
		[getUserdefinevalues.fulfilled]: userdefinevaluesAdapter.setAll
	}
});

export const { setData, setUserdefinevaluesSearchText } = userdefinevaluesSlice.actions;
export default userdefinevaluesSlice.reducer;
