import { createAsyncThunk, createSlice } from '@reduxjs/toolkit';
import {
	CREATE_USERDEFINEVALUE,
	DELETE_USERDEFINEVALUE,
	GET_USERDEFINEVALUEID,
	UPDATE_USERDEFINEVALUE
} from 'app/constant/constants';
import axios from 'axios';

export const getUserdefinevalue = createAsyncThunk(
	'userdefinevalueManagement/userdefinevalue/getUserdefinevalue',
	async (params, { rejectWithValue }) => {
		const authTOKEN = {
			headers: {
				'Content-type': 'application/json',
				Authorization: localStorage.getItem('jwt_access_token')
			}
		};

		try {
			const response = await axios.get(`${GET_USERDEFINEVALUEID}${params}`, authTOKEN);
			const data = await response.data;
			return data === undefined ? null : data;
		} catch (err) {
			return rejectWithValue(params);
		}
	}
);

export const removeUserdefinevalue = createAsyncThunk(
	'userdefinevalueManagement/userdefinevalue/removeUserdefinevalue',
	async val => {
		const authTOKEN = {
			headers: {
				'Content-type': 'application/json',
				Authorization: localStorage.getItem('jwt_access_token')
			}
		};

		const userdefinevalueId = val.id;
		const response = await axios.delete(`${DELETE_USERDEFINEVALUE}${userdefinevalueId}`, authTOKEN);
		return response;
	}
);

export const updateUserdefinevalue = createAsyncThunk(
	'userdefinevalueManagement/userdefinevalue/updateUserdefinevalue',
	async (userdefinevalueData, { dispatch, getState }) => {
		// const { userdefinevalue } = getState().userdefinevaluesManagement;

		const authTOKEN = {
			headers: {
				'Content-type': 'application/json',
				Authorization: localStorage.getItem('jwt_access_token')
			}
		};
		const response = await axios.put(
			`${UPDATE_USERDEFINEVALUE}${userdefinevalueData.id}`,
			userdefinevalueData,
			authTOKEN
		);
		return response;
	}
);

// export const saveUserdefinevalue = createAsyncThunk(
// 	'userdefinevalueManagement/userdefinevalue/saveUserdefinevalue',
// 	async userdefinevalueData => {
// 		const authTOKEN = {
// 			headers: {
// 				'Content-Type': 'application/x-www-form-urlencoded',
// 				Authorization: localStorage.getItem('jwt_access_token')
// 			}
// 		};
// 		// dfsdf
// 		const response = await axios.post(`${CREATE_USERDEFINEVALUE}`, userdefinevalueData, authTOKEN);
// 		return response;
// 	}
// );

export const saveUserdefinevalue = createAsyncThunk(
	'userdefinevalueManagement/userdefinevalue/saveUserdefinevalue',
	async userdefinevalueData => {
		const authToken = {
			headers: {
				'Content-Type': 'application/json', // Corrected Content-Type header
				Authorization: localStorage.getItem('jwt_access_token')
			}
		};

		const requestOptions = {
			method: 'POST',
			headers: authToken.headers,
			body: JSON.stringify(userdefinevalueData) // Convert data to JSON string
		};

		try {
			const response = await fetch(CREATE_USERDEFINEVALUE, requestOptions);
			if (!response.ok) {
				// Handle non-2xx response here, if needed
				throw new Error('Request failed');
			}
			const responseData = await response.json(); // Parse the JSON response
			return responseData;
		} catch (error) {
			// Handle any errors that occurred during the fetch here
			throw new Error(error.message);
		}
	}
);

const userdefinevalueSlice = createSlice({
	name: 'userdefinevalueManagement/userdefinevalue',
	initialState: null,
	reducers: {
		resetUserdefinevalue: () => null,
		newUserdefinevalue: {
			reducer: (state, action) => action.payload,
			prepare: event => ({
				payload: {
					employee: [],
					department: [],
					calculation_for: '',
					payhead: '',
					unit: '',
					value: ''
				}
			})
		}
	},
	extraReducers: {
		[getUserdefinevalue.fulfilled]: (state, action) => action.payload,
		[saveUserdefinevalue.fulfilled]: (state, action) => action.payload,
		[removeUserdefinevalue.fulfilled]: (state, action) => action.payload,
		[updateUserdefinevalue.fulfilled]: (state, action) => action.payload
	}
});

export const { newUserdefinevalue, resetUserdefinevalue } = userdefinevalueSlice.actions;

export default userdefinevalueSlice.reducer;
