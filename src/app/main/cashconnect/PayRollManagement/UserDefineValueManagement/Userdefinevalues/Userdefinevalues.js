import FusePageCarded from '@fuse/core/FusePageCarded';
import { getUserPermissions } from 'app/store/dataSlice';
import withReducer from 'app/store/withReducer';
import React, { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import reducer from '../store/index';
import UserdefinevaluesHeader from './UserdefinevaluesHeader';
import UserdefinevaluesTable from './UserdefinevaluesTable';

const Userdefinevalues = () => {
	const UserPermissions = useSelector(state => state.data.UserPermissions);
	useEffect(() => {
		dispatch(getUserPermissions());
	}, []);
	const dispatch = useDispatch();
	return (
		<FusePageCarded
			classes={{
				content: 'flex',
				contentCard: 'overflow-hidden',
				header: 'min-h-74 h-64'
			}}
			header={<UserdefinevaluesHeader />}
			content={<UserdefinevaluesTable />}
			innerScroll
		/>
	);
};
export default withReducer('userdefinevaluesManagement', reducer)(Userdefinevalues);
