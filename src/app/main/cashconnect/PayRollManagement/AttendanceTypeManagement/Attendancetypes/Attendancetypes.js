import FusePageCarded from '@fuse/core/FusePageCarded';
import { getUserPermissions } from 'app/store/dataSlice';
import withReducer from 'app/store/withReducer';
import React, { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import reducer from '../store/index';
import AttendancetypesHeader from './AttendancetypesHeader';
import AttendancetypesTable from './AttendancetypesTable';

const Attendancetypes = () => {
	const UserPermissions = useSelector(state => state.data.UserPermissions);
	useEffect(() => {
		dispatch(getUserPermissions());
	}, []);
	const dispatch = useDispatch();
	return (
		<FusePageCarded
			classes={{
				content: 'flex',
				contentCard: 'overflow-hidden',
				header: 'min-h-74 h-64'
			}}
			header={<AttendancetypesHeader />}
			content={<AttendancetypesTable />}
			innerScroll
		/>
	);
};
export default withReducer('attendancetypesManagement', reducer)(Attendancetypes);
