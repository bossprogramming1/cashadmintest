import FusePageCarded from '@fuse/core/FusePageCarded';
import { useDeepCompareEffect } from '@fuse/hooks';
import { yupResolver } from '@hookform/resolvers/yup';
import { Button, Typography } from '@material-ui/core';
import withReducer from 'app/store/withReducer';
import { motion } from 'framer-motion';
import React, { useEffect, useState } from 'react';
import { FormProvider, useForm } from 'react-hook-form';
import { useDispatch, useSelector } from 'react-redux';
import { Link, useParams } from 'react-router-dom';
import * as yup from 'yup';
import reducer from '../store/index';
import { getAttendancetype, newAttendancetype, resetAttendancetype } from '../store/attendancetypeSlice';
import NewAttendancetypeHeader from './NewAttendancetypeHeader';
import AttendancetypeForm from './AttendancetypeForm';

/**
 * Form Validation Schema
 */
const schema = yup.object().shape({
	name: yup.string().required('Name is required'),
	income_type: yup.string().required('Income type is required'),
	payslip_display_name: yup.string().required('Display name is required')
});

const Attendancetype = () => {
	const dispatch = useDispatch();
	const attendancetype = useSelector(({ attendancetypesManagement }) => attendancetypesManagement.attendancetype);

	const [noAttendancetype, setNoAttendancetype] = useState(false);
	const methods = useForm({
		mode: 'onChange',
		defaultValues: {},
		resolver: yupResolver(schema)
	});
	const routeParams = useParams();

	const { reset } = methods;

	useDeepCompareEffect(() => {
		function updateAttendancetypeState() {
			const { attendancetypeId } = routeParams;

			if (attendancetypeId === 'new') {
				localStorage.removeItem('event');
				/**
				 * Create New User data
				 */
				dispatch(newAttendancetype());
			} else {
				/**
				 * Get User data
				 */

				dispatch(getAttendancetype(attendancetypeId)).then(action => {
					/**
					 * If the requested product is not exist show message
					 */
					if (!action.payload) {
						setNoAttendancetype(true);
					}
				});
			}
		}

		updateAttendancetypeState();
	}, [dispatch, routeParams]);

	useEffect(() => {}, []);

	useEffect(() => {
		if (!attendancetype) {
			return;
		}
		/**
		 * Reset the form on attendancetype state changes
		 */
		reset(attendancetype);
	}, [attendancetype, reset]);

	useEffect(() => {
		return () => {
			/**
			 * Reset Attendancetype on component unload
			 */
			dispatch(resetAttendancetype());
			setNoAttendancetype(false);
		};
	}, [dispatch]);

	/**
	 * Show Message if the requested products is not exists
	 */
	if (noAttendancetype) {
		return (
			<motion.div
				initial={{ opacity: 0 }}
				animate={{ opacity: 1, transition: { delay: 0.1 } }}
				className="flex flex-col flex-1 items-center justify-center h-full"
			>
				<Typography color="textSecondary" variant="h5">
					There is no such Attendance/Production!
				</Typography>
				<Button
					className="mt-24"
					component={Link}
					variant="outlined"
					to="/apps/e-commerce/products"
					color="inherit"
				>
					Go to Attendance/Production Page
				</Button>
			</motion.div>
		);
	}

	return (
		<FormProvider {...methods}>
			<FusePageCarded
				classes={{
					toolbar: 'p-0',
					header: 'min-h-80 h-80'
				}}
				header={<NewAttendancetypeHeader />}
				content={
					<div className="p-16 sm:p-24">
						<AttendancetypeForm />
					</div>
				}
				innerScroll
			/>
		</FormProvider>
	);
};
export default withReducer('attendancetypesManagement', reducer)(Attendancetype);
