import _ from '@lodash';
import { makeStyles } from '@material-ui/core/styles';
import TextField from '@material-ui/core/TextField';
import { Autocomplete } from '@material-ui/lab';
import React, { useEffect } from 'react';
import { Controller, useFormContext } from 'react-hook-form';
import { useDispatch, useSelector } from 'react-redux';
import { useHistory, useParams } from 'react-router-dom';
// import { getCities } from '../../../../store/dataSlice';
import { getAttendanceProductionTypes, getAttendanceTypes } from 'app/store/dataSlice';
import { saveAttendancetype, updateAttendancetype } from '../store/attendancetypeSlice';

const useStyles = makeStyles(theme => ({
	hidden: {
		display: 'none'
	}
}));

function AttendancetypeForm(props) {
	const userID = localStorage.getItem('user_id');

	const attendanceTypes = useSelector(state => state.data.attendanceTypes);
	const attendanceProductionTypes = useSelector(state => state.data.attendanceProductionTypes);

	const classes = useStyles(props);

	const methods = useFormContext();
	const { control, formState, getValues } = methods;
	const { errors, isValid, dirtyFields } = formState;
	const routeParams = useParams();
	const { attendancetypeId } = routeParams;
	const history = useHistory();
	const handleDelete = localStorage.getItem('attendancetypeEvent');
	const dispatch = useDispatch();

	function handleSaveAttendancetype() {
		dispatch(saveAttendancetype(getValues())).then(res => {
			if (res.payload) {
				localStorage.setItem('attendancetypeAlert', 'saveAttendancetype');
				history.push('/apps/attendancetype-management/attendancetypes');
			}
		});
	}

	function handleUpdateAttendancetype() {
		dispatch(updateAttendancetype(getValues())).then(res => {
			if (res.payload) {
				localStorage.setItem('attendancetypeAlert', 'updateAttendancetype');
				history.push('/apps/attendancetype-management/attendancetypes');
			}
		});
	}

	const handleSubmitOnKeyDownEnter = ev => {
		if (ev.key === 'Enter') {
			if (routeParams.attendancetypeId === 'new' && !(_.isEmpty(dirtyFields) || !isValid)) {
				handleSaveAttendancetype();
			} else if (handleDelete !== 'Delete' && routeParams?.attendancetypeName) {
				handleUpdateAttendancetype();
			}
		}
	};
	useEffect(() => {
		dispatch(getAttendanceTypes());
		dispatch(getAttendanceProductionTypes());
	}, []);

	return (
		<div>
			<Controller
				name="name"
				control={control}
				render={({ field }) => {
					return (
						<TextField
							{...field}
							className="mt-8 mb-16"
							error={!!errors?.name}
							helperText={errors?.name?.message}
							label="Attendancetype Name"
							id="name"
							required
							variant="outlined"
							InputLabelProps={field.value && { shrink: true }}
							fullWidth
							onKeyDown={handleSubmitOnKeyDownEnter}
						/>
					);
				}}
			/>
			<Controller
				name="under"
				control={control}
				render={({ field: { onChange, value, name } }) => {
					return (
						<Autocomplete
							className="mt-8 mb-16"
							freeSolo
							value={value ? attendanceProductionTypes.find(data => data.id === value) : null}
							options={attendanceProductionTypes}
							getOptionLabel={option => `${option?.name}`}
							onChange={(event, newValue) => {
								onChange(newValue?.id);
							}}
							renderInput={params => (
								<TextField
									{...params}
									placeholder="Select Attendance/Production Type"
									label="Attendance/Production Type"
									error={!!errors.under}
									required
									helperText={errors?.under?.message}
									variant="outlined"
									InputLabelProps={{
										shrink: true
									}}
								/>
							)}
						/>
					);
				}}
			/>
			<Controller
				name="attendance_type"
				control={control}
				render={({ field: { onChange, value, name } }) => {
					return (
						<Autocomplete
							className="mt-8 mb-16"
							freeSolo
							value={value ? attendanceTypes.find(data => data.id === value) : null}
							options={attendanceTypes}
							getOptionLabel={option => `${option?.name}`}
							onChange={(event, newValue) => {
								onChange(newValue?.id);
							}}
							renderInput={params => (
								<TextField
									{...params}
									placeholder="Select Attendance Type"
									label="Attendance Type"
									error={!!errors.attendance_type}
									required
									helperText={errors?.attendance_type?.message}
									variant="outlined"
									InputLabelProps={{
										shrink: true
									}}
								/>
							)}
						/>
					);
				}}
			/>
		</div>
	);
}

export default AttendancetypeForm;
