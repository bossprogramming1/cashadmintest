import { createAsyncThunk, createEntityAdapter, createSlice } from '@reduxjs/toolkit';
import { DELETE_ATTENDANCE_PRODUCTION_TYPE, GET_ATTENDANCE_PRODUCTION_TYPES } from 'app/constant/constants';
import axios from 'axios';

export const getAttendancetypes = createAsyncThunk(
	'attendancetypeManagement/attendancetypes/getAttendancetypes',
	async parameter => {
		const { page, size } = parameter;
		axios.defaults.headers.common['Content-type'] = 'application/json';
		axios.defaults.headers.common.Authorization = localStorage.getItem('jwt_access_token');

		const response = axios.get(GET_ATTENDANCE_PRODUCTION_TYPES, { params: { page, size } });
		const data = await response;

		sessionStorage.setItem('attendancetypes_total_elements', data.data.total_elements);
		sessionStorage.setItem('attendancetypes_total_pages', data.data.total_pages);
		delete axios.defaults.headers.common['Content-type'];
		delete axios.defaults.headers.common.Authorization;

		return data.data.attendance_production_types;
	}
);

export const removeAttendancetypes = createAsyncThunk(
	'attendancetypeManagement/attendancetypes/removeAttendancetypes',
	async (attendancetypeIds, { dispatch, getState }) => {
		const authTOKEN = {
			headers: {
				'Content-type': 'application/json',
				Authorization: localStorage.getItem('jwt_access_token')
			}
		};
		await axios.delete(`${DELETE_ATTENDANCE_PRODUCTION_TYPE}`, { attendancetypeIds }, authTOKEN);

		return attendancetypeIds;
	}
);

const attendancetypesAdapter = createEntityAdapter({});

export const { selectAll: selectAttendancetypes, selectById: selectAttendancetypeById } =
	attendancetypesAdapter.getSelectors(state => state.attendancetypesManagement.attendancetypes);

const attendancetypesSlice = createSlice({
	name: 'attendancetypeManagement/attendancetypes',
	initialState: attendancetypesAdapter.getInitialState({
		searchText: ''
	}),
	reducers: {
		setAttendancetypesSearchText: {
			reducer: (state, action) => {
				state.searchText = action.payload;
			},
			prepare: event => ({ payload: event.target.value || '' })
		}
	},
	extraReducers: {
		[getAttendancetypes.fulfilled]: attendancetypesAdapter.setAll
	}
});

export const { setData, setAttendancetypesSearchText } = attendancetypesSlice.actions;
export default attendancetypesSlice.reducer;
