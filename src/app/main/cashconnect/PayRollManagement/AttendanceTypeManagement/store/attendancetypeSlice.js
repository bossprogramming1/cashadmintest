import { createAsyncThunk, createSlice } from '@reduxjs/toolkit';
import {
	CREATE_ATTENDANCE_PRODUCTION_TYPE,
	DELETE_ATTENDANCE_PRODUCTION_TYPE,
	GET_ATTENDANCE_PRODUCTION_TYPEID,
	UPDATE_ATTENDANCE_PRODUCTION_TYPE
} from 'app/constant/constants';
import axios from 'axios';

export const getAttendancetype = createAsyncThunk(
	'attendancetypeManagement/attendancetype/getAttendancetype',
	async (params, { rejectWithValue }) => {
		const authTOKEN = {
			headers: {
				'Content-type': 'application/json',
				Authorization: localStorage.getItem('jwt_access_token')
			}
		};

		try {
			const response = await axios.get(`${GET_ATTENDANCE_PRODUCTION_TYPEID}${params}`, authTOKEN);
			const data = await response.data;
			return data === undefined ? null : data;
		} catch (err) {
			return rejectWithValue(params);
		}
	}
);

export const removeAttendancetype = createAsyncThunk(
	'attendancetypeManagement/attendancetype/removeAttendancetype',
	async val => {
		const authTOKEN = {
			headers: {
				'Content-type': 'application/json',
				Authorization: localStorage.getItem('jwt_access_token')
			}
		};

		const attendancetypeId = val.id;
		const response = await axios.delete(`${DELETE_ATTENDANCE_PRODUCTION_TYPE}${attendancetypeId}`, authTOKEN);
		return response;
	}
);

export const updateAttendancetype = createAsyncThunk(
	'attendancetypeManagement/attendancetype/updateAttendancetype',
	async (attendancetypeData, { dispatch, getState }) => {
		const { attendancetype } = getState().attendancetypesManagement;

		const authTOKEN = {
			headers: {
				'Content-type': 'application/json',
				Authorization: localStorage.getItem('jwt_access_token')
			}
		};
		const response = await axios.put(
			`${UPDATE_ATTENDANCE_PRODUCTION_TYPE}${attendancetype.id}`,
			attendancetypeData,
			authTOKEN
		);
		return response;
	}
);

export const saveAttendancetype = createAsyncThunk(
	'attendancetypeManagement/attendancetype/saveAttendancetype',
	async attendancetypeData => {
		const authTOKEN = {
			headers: {
				'Content-type': 'application/json',
				Authorization: localStorage.getItem('jwt_access_token')
			}
		};
		const response = await axios.post(`${CREATE_ATTENDANCE_PRODUCTION_TYPE}`, attendancetypeData, authTOKEN);
		return response;
	}
);

const attendancetypeSlice = createSlice({
	name: 'attendancetypeManagement/attendancetype',
	initialState: null,
	reducers: {
		resetAttendancetype: () => null,
		newAttendancetype: {
			reducer: (state, action) => action.payload,
			prepare: event => ({
				payload: {
					name: '',
					income_type: '',
					payslip_display_name: ''
				}
			})
		}
	},
	extraReducers: {
		[getAttendancetype.fulfilled]: (state, action) => action.payload,
		[saveAttendancetype.fulfilled]: (state, action) => action.payload,
		[removeAttendancetype.fulfilled]: (state, action) => action.payload,
		[updateAttendancetype.fulfilled]: (state, action) => action.payload
	}
});

export const { newAttendancetype, resetAttendancetype } = attendancetypeSlice.actions;

export default attendancetypeSlice.reducer;
