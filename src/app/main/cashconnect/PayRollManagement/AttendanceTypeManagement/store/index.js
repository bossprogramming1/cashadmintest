import { combineReducers } from '@reduxjs/toolkit';
import attendancetype from './attendancetypeSlice';
import attendancetypes from './attendancetypesSlice';

const reducer = combineReducers({
	attendancetype,
	attendancetypes
});

export default reducer;
