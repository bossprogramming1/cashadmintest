import FusePageCarded from '@fuse/core/FusePageCarded';
import { useDeepCompareEffect } from '@fuse/hooks';
import { yupResolver } from '@hookform/resolvers/yup';
import { Button, Typography } from '@material-ui/core';
import withReducer from 'app/store/withReducer';
import { motion } from 'framer-motion';
import React, { useEffect, useState } from 'react';
import { FormProvider, useForm } from 'react-hook-form';
import { useDispatch, useSelector } from 'react-redux';
import { Link, useParams } from 'react-router-dom';
import * as yup from 'yup';
import reducer from '../store/index';
import { getDevice, newDevice, resetDevice } from '../store/deviceSlice';
import NewDeviceHeader from './NewDeviceHeader';
import DeviceForm from './DeviceForm';

/**
 * Form Validation Schema
 */
const schema = yup.object().shape({
	name: yup.string().required('Name is required'),
	ip_address: yup.string().required('IP Address is required')
});

const Device = () => {
	const dispatch = useDispatch();
	const device = useSelector(({ devicesManagement }) => devicesManagement.device);

	const [noDevice, setNoDevice] = useState(false);
	const methods = useForm({
		mode: 'onChange',
		defaultValues: {},
		resolver: yupResolver(schema)
	});
	const routeParams = useParams();

	const { reset } = methods;

	useDeepCompareEffect(() => {
		function updateDeviceState() {
			const { deviceId } = routeParams;

			if (deviceId === 'new') {
				localStorage.removeItem('event');
				/**
				 * Create New User data
				 */
				dispatch(newDevice());
			} else {
				/**
				 * Get User data
				 */

				dispatch(getDevice(deviceId)).then(action => {
					/**
					 * If the requested product is not exist show message
					 */
					if (!action.payload) {
						setNoDevice(true);
					}
				});
			}
		}

		updateDeviceState();
	}, [dispatch, routeParams]);

	useEffect(() => {}, []);

	useEffect(() => {
		if (!device) {
			return;
		}
		/**
		 * Reset the form on device state changes
		 */
		reset(device);
	}, [device, reset]);

	useEffect(() => {
		return () => {
			/**
			 * Reset Device on component unload
			 */
			dispatch(resetDevice());
			setNoDevice(false);
		};
	}, [dispatch]);

	/**
	 * Show Message if the requested products is not exists
	 */
	if (noDevice) {
		return (
			<motion.div
				initial={{ opacity: 0 }}
				animate={{ opacity: 1, transition: { delay: 0.1 } }}
				className="flex flex-col flex-1 items-center justify-center h-full"
			>
				<Typography color="textSecondary" variant="h5">
					There is no such device!
				</Typography>
				<Button
					className="mt-24"
					component={Link}
					variant="outlined"
					to="/apps/e-commerce/products"
					color="inherit"
				>
					Go to Device Page
				</Button>
			</motion.div>
		);
	}

	return (
		<FormProvider {...methods}>
			<FusePageCarded
				classes={{
					toolbar: 'p-0',
					header: 'min-h-80 h-80'
				}}
				header={<NewDeviceHeader />}
				content={
					<div className="p-16 sm:p-24">
						<DeviceForm />
					</div>
				}
				innerScroll
			/>
		</FormProvider>
	);
};
export default withReducer('devicesManagement', reducer)(Device);
