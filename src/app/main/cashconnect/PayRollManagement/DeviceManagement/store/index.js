import { combineReducers } from '@reduxjs/toolkit';
import device from './deviceSlice';
import devices from './devicesSlice';

const reducer = combineReducers({
	device,
	devices
});

export default reducer;
