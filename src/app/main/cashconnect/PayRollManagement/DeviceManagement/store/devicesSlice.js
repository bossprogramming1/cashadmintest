import { createAsyncThunk, createEntityAdapter, createSlice } from '@reduxjs/toolkit';
import { DELETE_DEVICE_IP, GET_DEVICE_IPS } from 'app/constant/constants';
import axios from 'axios';

export const getDevices = createAsyncThunk('deviceManagement/devices/getDevices', async parameter => {
	const { page, size } = parameter;
	axios.defaults.headers.common['Content-type'] = 'application/json';
	axios.defaults.headers.common.Authorization = localStorage.getItem('jwt_access_token');

	const response = axios.get(GET_DEVICE_IPS, { params: { page, size } });
	const data = await response;

	sessionStorage.setItem('devices_total_elements', data.data.total_elements);
	sessionStorage.setItem('devices_total_pages', data.data.total_pages);
	delete axios.defaults.headers.common['Content-type'];
	delete axios.defaults.headers.common.Authorization;

	return data.data.device_ips;
});

export const removeDevices = createAsyncThunk(
	'deviceManagement/devices/removeDevices',
	async (deviceIds, { dispatch, getState }) => {
		const authTOKEN = {
			headers: {
				'Content-type': 'application/json',
				Authorization: localStorage.getItem('jwt_access_token')
			}
		};
		await axios.delete(`${DELETE_DEVICE_IP}`, { deviceIds }, authTOKEN);

		return deviceIds;
	}
);

const devicesAdapter = createEntityAdapter({});

export const { selectAll: selectDevices, selectById: selectDeviceById } = devicesAdapter.getSelectors(
	state => state.devicesManagement.devices
);

const devicesSlice = createSlice({
	name: 'deviceManagement/devices',
	initialState: devicesAdapter.getInitialState({
		searchText: ''
	}),
	reducers: {
		setDevicesSearchText: {
			reducer: (state, action) => {
				state.searchText = action.payload;
			},
			prepare: event => ({ payload: event.target.value || '' })
		}
	},
	extraReducers: {
		[getDevices.fulfilled]: devicesAdapter.setAll
	}
});

export const { setData, setDevicesSearchText } = devicesSlice.actions;
export default devicesSlice.reducer;
