import { createAsyncThunk, createSlice } from '@reduxjs/toolkit';
import { CREATE_DEVICE_IP, DELETE_DEVICE_IP, GET_DEVICE_IPID, UPDATE_DEVICE_IP } from 'app/constant/constants';
import axios from 'axios';

export const getDevice = createAsyncThunk('deviceManagement/device/getDevice', async (params, { rejectWithValue }) => {
	const authTOKEN = {
		headers: {
			'Content-type': 'application/json',
			Authorization: localStorage.getItem('jwt_access_token')
		}
	};

	try {
		const response = await axios.get(`${GET_DEVICE_IPID}${params}`, authTOKEN);
		const data = await response.data;
		return data === undefined ? null : data;
	} catch (err) {
		return rejectWithValue(params);
	}
});

export const removeDevice = createAsyncThunk('deviceManagement/device/removeDevice', async val => {
	const authTOKEN = {
		headers: {
			'Content-type': 'application/json',
			Authorization: localStorage.getItem('jwt_access_token')
		}
	};

	const deviceId = val.id;
	const response = await axios.delete(`${DELETE_DEVICE_IP}${deviceId}`, authTOKEN);
	return response;
});

export const updateDevice = createAsyncThunk(
	'deviceManagement/device/updateDevice',
	async (deviceData, { dispatch, getState }) => {
		const authTOKEN = {
			headers: {
				'Content-type': 'application/json',
				Authorization: localStorage.getItem('jwt_access_token')
			}
		};
		const response = await axios.put(`${UPDATE_DEVICE_IP}${deviceData.id}`, deviceData, authTOKEN);
		return response;
	}
);

export const saveDevice = createAsyncThunk('deviceManagement/device/saveDevice', async deviceData => {
	const authTOKEN = {
		headers: {
			'Content-type': 'application/json',
			Authorization: localStorage.getItem('jwt_access_token')
		}
	};
	const response = await axios.post(`${CREATE_DEVICE_IP}`, deviceData, authTOKEN);
	return response;
});

const deviceSlice = createSlice({
	name: 'deviceManagement/device',
	initialState: null,
	reducers: {
		resetDevice: () => null,
		newDevice: {
			reducer: (state, action) => action.payload,
			prepare: event => ({
				payload: {
					name: '',
					ip_address: ''
				}
			})
		}
	},
	extraReducers: {
		[getDevice.fulfilled]: (state, action) => action.payload,
		[saveDevice.fulfilled]: (state, action) => action.payload,
		[removeDevice.fulfilled]: (state, action) => action.payload,
		[updateDevice.fulfilled]: (state, action) => action.payload
	}
});

export const { newDevice, resetDevice } = deviceSlice.actions;

export default deviceSlice.reducer;
