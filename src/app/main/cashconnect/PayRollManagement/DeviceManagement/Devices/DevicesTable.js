import FuseLoading from '@fuse/core/FuseLoading';
import FuseScrollbars from '@fuse/core/FuseScrollbars';
import _ from '@lodash';
import { Typography, CircularProgress } from '@material-ui/core';
import Checkbox from '@material-ui/core/Checkbox';
import { makeStyles } from '@material-ui/core/styles';
import { styled } from '@mui/material/styles';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TablePagination from '@material-ui/core/TablePagination';
import TableRow from '@material-ui/core/TableRow';
import DeleteIcon from '@material-ui/icons/Delete';
import EditIcon from '@material-ui/icons/Edit';
import Pagination from '@material-ui/lab/Pagination';
import { rowsPerPageOptions } from 'app/@data/data';
import { SEARCH_DEVICE_IP, UPDATE_DEVICE_IP } from 'app/constant/constants';
import { motion } from 'framer-motion';
import React, { useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { useForm } from 'react-hook-form';
import { withRouter, useHistory } from 'react-router-dom';
import Switch from '@mui/material/Switch';
import axios from 'axios';
import { addNotification } from 'app/fuse-layouts/shared-components/notificationPanel/store/dataSlice';
import NotificationModel from 'app/fuse-layouts/shared-components/notificationPanel/model/NotificationModel';
import { getDevices, selectDevices } from '../store/devicesSlice';
import DevicesTableHead from './DevicesTableHead';

const useStyles = makeStyles(theme => ({
	root: {
		display: 'flex',
		justifyContent: 'space-between',
		flexWrap: 'nowrap',
		overflow: 'auto',
		minHeight: '35px'
	},
	toolbar: {
		'& > div': {
			minHeight: 'fit-content'
		}
	},
	modal: {
		position: 'relative',

		marginTop: '22%',
		marginLeft: '50%',
		transform: 'translate(-50%, -50%)',
		width: 350,
		height: 180,
		border: '2px solid #000',
		boxShadow: 24,
		p: 4,
		//color: theme.palette.background.paper,
		backgroundColor: theme.palette.background.paper
	}
}));
const IOSSwitch = styled(props => <Switch focusVisibleClassName=".Mui-focusVisible" disableRipple {...props} />)(
	({ theme }) => ({
		width: 42,
		height: 26,
		padding: 0,
		'& .MuiSwitch-switchBase': {
			padding: 0,
			margin: 2,
			transitionDuration: '300ms',
			'&.Mui-checked': {
				transform: 'translateX(16px)',
				color: '#fff',
				'& + .MuiSwitch-track': {
					backgroundColor: theme.palette.mode === 'dark' ? '#2ECA45' : '#65C466',
					opacity: 1,
					border: 0
				},
				'&.Mui-disabled + .MuiSwitch-track': {
					opacity: 0.5
				}
			},
			'&.Mui-focusVisible .MuiSwitch-thumb': {
				color: '#33cf4d',
				border: '6px solid #fff'
			},
			'&.Mui-disabled .MuiSwitch-thumb': {
				color: theme.palette.mode === 'light' ? theme.palette.grey[100] : theme.palette.grey[600]
			},
			'&.Mui-disabled + .MuiSwitch-track': {
				opacity: theme.palette.mode === 'light' ? 0.7 : 0.3
			}
		},
		'& .MuiSwitch-thumb': {
			boxSizing: 'border-box',
			width: 22,
			height: 22
		},
		'& .MuiSwitch-track': {
			borderRadius: 26 / 2,
			backgroundColor: theme.palette.mode === 'light' ? '#E9E9EA' : '#39393D',
			opacity: 1,
			transition: theme.transitions.create(['background-color'], {
				duration: 500
			})
		}
	})
);

const DevicesTable = props => {
	const dispatch = useDispatch();
	const devices = useSelector(selectDevices);
	const methods = useForm();
	const { control, formState, getValues, setValue, watch } = methods;
	const { errors, isValid, dirtyFields } = formState;
	const searchText = useSelector(({ devicesManagement }) => devicesManagement.devices.searchText);
	const [searchDevice, setSearchDevice] = useState([]);
	const [loading, setLoading] = useState(true);
	const [circularProgressLoading, setCircularProgressLoading] = useState(false);
	const [selected, setSelected] = useState([]);
	const [data, setData] = useState(devices);
	const [page, setPage] = useState(0);
	const [rowsPerPage, setRowsPerPage] = useState(30);
	const [order, setOrder] = useState({
		direction: 'asc',
		id: null
	});
	let serialNumber = 1;
	const user_role = localStorage.getItem('user_role');
	const [parameter, setParameter] = useState({ page: 1, size: 30 });
	const totalPages = sessionStorage.getItem('devices_total_pages');
	const totalElements = sessionStorage.getItem('devices_total_elements');
	const classes = useStyles();
	// const [open, setOpen] = useState(false);
	// const [buttonText, setButtonText] = useState('');
	// const [successMessage, setSuccessMessage] = useState('');
	// const handleClose = () => setOpen(false);
	const history = useHistory();

	useEffect(() => {
		dispatch(getDevices(parameter)).then(() => setLoading(false));
	}, [dispatch]);

	//search device
	useEffect(() => {
		searchText ? getSearchDevice() : setSearchDevice([]);
	}, [searchText]);

	//successMessage
	// useEffect(() => {
	// 	if (successMessage) {
	// 		const timer = setTimeout(() => {
	// 			setSuccessMessage('');
	// 			setButtonText('');
	// 			setOpen(false);
	// 			dispatch(getDevices(parameter)).then(() => setLoading(false));
	// 		}, 3000);

	// 		// Clean up the timer if the component unmounts or if successMessage changes before the timer expires.
	// 		return () => clearTimeout(timer);
	// 	}
	// }, [successMessage]);

	const getSearchDevice = () => {
		fetch(`${SEARCH_DEVICE_IP}?name=${searchText}`)
			.then(response => response.json())
			.then(searchedDevicesData => {
				setSearchDevice(searchedDevicesData.devices);
			})
			.catch(() => setSearchDevice([]));
	};

	function handleRequestSort(deviceEvent, property) {
		const id = property;
		let direction = 'desc';

		if (order.id === property && order.direction === 'desc') {
			direction = 'asc';
		}

		setOrder({
			direction,
			id
		});
	}

	function handleSelectAllClick(deviceEvent) {
		if (deviceEvent.target.checked) {
			setSelected(devices.map(n => n.id));
			return;
		}
		setSelected([]);
	}

	function handleDeselect() {
		setSelected([]);
	}

	function handleUpdateDevice(item) {
		localStorage.removeItem('deviceEvent');
		props.history.push(`/apps/device-management/devices/${item.id}/${item?.name}`);
	}
	function handleDeleteDevice(item, deviceEvent) {
		localStorage.removeItem('deviceEvent');
		localStorage.setItem('deviceEvent', deviceEvent);
		props.history.push(`/apps/device-management/devices/${item.id}/${item?.name}`);
	}
	// function handleDeviceSatus(item) {
	// 	setValue('status', item.status);
	// 	setValue('id', item.id);
	// 	setValue('ip_address', item.ip_address);
	// 	setValue('name', item.name);
	// 	setButtonText(item.status);
	// 	setOpen(true);
	// }
	function handleDeviceStatusSave(event, item) {
		// toast.dismiss();
		const status = event.target.checked ? 'connected' : 'disconnected';

		const { id, ip_address, name, port } = item;

		// Create an object with id and status
		const logData = {
			id,
			name,
			ip_address,
			status,
			port
		};

		console.log('logData', logData);
		const authTOKEN = {
			headers: {
				'Content-type': 'application/json',
				Authorization: localStorage.getItem('jwt_access_token')
			}
		};
		axios
			.put(`${UPDATE_DEVICE_IP}${item.id}`, logData, authTOKEN)
			.then(res => {
				dispatch(getDevices(parameter)).then(() => {
					setLoading(false);
					console.log('res.payload.data.status', res);
					dispatch(
						addNotification(
							NotificationModel({
								message: `Device has susscessfully ${res.data.status}`,
								options: { variant: 'success' },
								item_id: item.id
							})
						)
					);
				});
			})
			.catch(err =>
				dispatch(
					addNotification(
						NotificationModel({
							message: `No device found`,
							options: { variant: 'error' },
							item_id: item.id
						})
					)
				)
			);
	}

	function handleCheck(deviceEvent, id) {
		const selectedIndex = selected.indexOf(id);
		let newSelected = [];

		if (selectedIndex === -1) {
			newSelected = newSelected.concat(selected, id);
		} else if (selectedIndex === 0) {
			newSelected = newSelected.concat(selected.slice(1));
		} else if (selectedIndex === selected.length - 1) {
			newSelected = newSelected.concat(selected.slice(0, -1));
		} else if (selectedIndex > 0) {
			newSelected = newSelected.concat(selected.slice(0, selectedIndex), selected.slice(selectedIndex + 1));
		}

		setSelected(newSelected);
	}

	const handlePagination = (e, handlePage) => {
		setParameter({ ...parameter, page: handlePage });
		setPage(handlePage - 1);
		dispatch(getDevices({ ...parameter, page: handlePage }));
	};

	function handleChangePage(deviceEvent, value) {
		setPage(value);
		setParameter({ ...parameter, page: value + 1 });
		dispatch(getDevices({ ...parameter, page: value - 1 }));
	}

	function handleChangeRowsPerPage(deviceEvent) {
		setRowsPerPage(deviceEvent.target.value);
		setParameter({ ...parameter, size: deviceEvent.target.value });
		dispatch(getDevices({ ...parameter, size: deviceEvent.target.value }));
	}

	if (loading) {
		return <FuseLoading />;
	}

	if (devices?.length === 0) {
		return (
			<motion.div
				initial={{ opacity: 0 }}
				animate={{ opacity: 1, transition: { delay: 0.1 } }}
				className="flex flex-1 items-center justify-center h-full"
			>
				<Typography color="textSecondary" variant="h5">
					There are no device!
				</Typography>
			</motion.div>
		);
	}

	return (
		<div className="w-full flex flex-col">
			<FuseScrollbars className="flex-grow overflow-x-auto">
				<Table stickyHeader className="min-w-xl" aria-labelledby="tableTitle">
					<DevicesTableHead
						selectedDeviceIds={selected}
						order={order}
						onSelectAllClick={handleSelectAllClick}
						onRequestSort={handleRequestSort}
						rowCount={devices.length}
						onMenuItemClick={handleDeselect}
					/>

					<TableBody>
						{_.orderBy(
							searchText !== '' && !_.isEmpty(searchDevice) ? searchDevice : devices,
							[
								o => {
									switch (order.id) {
										case 'categories': {
											return o.categories[0];
										}
										default: {
											return o[order.id];
										}
									}
								}
							],
							[order.direction]
						).map(n => {
							const isSelected = selected.indexOf(n.id) !== -1;
							return (
								<TableRow
									className="h-52 cursor-pointer"
									hover
									role="checkbox"
									aria-checked={isSelected}
									tabIndex={-1}
									key={n.id}
									selected={isSelected}
								>
									<TableCell className="whitespace-nowrap w-40 md:w-64 text-center" padding="none">
										<Checkbox
											checked={isSelected}
											onClick={deviceEvent => deviceEvent.stopPropagation()}
											onChange={deviceEvent => handleCheck(deviceEvent, n.id)}
										/>
									</TableCell>

									<TableCell className="whitespace-nowrap w-40 md:w-64" component="th" scope="row">
										{parameter.page * parameter.size - parameter.size + serialNumber++}
									</TableCell>

									<TableCell className="whitespace-nowrap p-4 md:p-16" component="th" scope="row">
										{n?.name}
									</TableCell>

									<TableCell className="whitespace-nowrap p-4 md:p-16" component="th" scope="row">
										{n.ip_address}
									</TableCell>
									<TableCell
										className={`whitespace-nowrap p-4 md:p-16 ${
											n.status == 'disconnected' ? 'text-red-500' : 'text-green-500'
										}`}
										component="th"
										scope="row"
									>
										{n.status}
									</TableCell>

									<TableCell
										whitespace-nowrap
										className="p-4 md:p-16"
										align="center"
										component="th"
										scope="row"
									>
										<div>
											<EditIcon
												onClick={deviceEvent => handleUpdateDevice(n)}
												className="cursor-pointer"
												style={{ color: 'green' }}
											/>{' '}
											<DeleteIcon
												onClick={event => handleDeleteDevice(n, 'Delete')}
												className="cursor-pointer"
												style={{
													color: 'red',
													visibility:
														user_role === 'ADMIN' || user_role === 'admin'
															? 'visible'
															: 'hidden'
												}}
											/>
											{/* {n.status == 'connected' && (
												<MobileFriendlyIcon
													onClick={event => handleDeviceSatus(n)}
													className="cursor-pointer"
													style={{
														color: 'green'
													}}
												/>
											)}
											{n.status == 'disconnected' && (
												<MobileOffIcon
													onClick={event => handleDeviceSatus(n)}
													className="cursor-pointer"
													style={{
														color: 'red'
													}}
												/>
											)} */}
											<IOSSwitch
												sx={{ m: 1 }}
												defaultChecked={n.status == 'connected' ? true : false}
												onChange={event => {
													console.log('switch', event.target.checked);
													handleDeviceStatusSave(event, n);
												}}
											/>
										</div>
									</TableCell>
								</TableRow>
							);
						})}
					</TableBody>
				</Table>
			</FuseScrollbars>
			{/* <Modal
				open={open}
				onClose={handleClose}
				aria-labelledby="modal-modal-title"
				aria-describedby="modal-modal-description"
			>
				<Box className={classes.modal}>
					{circularProgressLoading && !successMessage ? (
						<>
							<Typography variant="h6" gutterBottom className="mt-10 text-center">
								Device Status
							</Typography>
							<Box
								style={{
									position: 'absolute',
									top: '50%',
									left: '50%',
									transform: 'translate(-50%, -50%)'
								}}
							>
								<CircularProgress size={24} />
							</Box>
						</>
					) : (
						!successMessage && (
							<>
								<Typography variant="h6" gutterBottom className="mt-10 text-center">
									Device Status
								</Typography>
								<Box style={{ display: 'flex', justifyContent: 'center' }}>
									<FormProvider {...methods}>
										<Controller
											name="status"
											control={control}
											render={({ field: { onChange, value, name } }) => (
												<Autocomplete
													className="mt-8 mb-16 px-40"
													freeSolo
													value={value ? deviceStatus.find(data => data.id === value) : null}
													options={deviceStatus}
													getOptionLabel={option => `${option?.name}`}
													onChange={(event, newValue) => {
														onChange(newValue?.id);
													}}
													fullWidth
													renderInput={params => (
														<TextField
															{...params}
															placeholder="Select Status"
															label="Status"
															fullWidth
															error={!!errors.status}
															required
															helperText={errors?.status?.message}
															variant="outlined"
															autoFocus
															InputLabelProps={{
																shrink: true
															}}
															// onKeyDown={handleSubmitOnKeyDownEnter}
														/>
													)}
												/>
											)}
										/>
									</FormProvider>
								</Box>
								<div className="flex justify-end">
									<Button
										className="whitespace-nowrap mx-4"
										variant="contained"
										color="secondary"
										// disabled={_.isEmpty(dirtyFields) || !isValid}
										onClick={() => {
											setCircularProgressLoading(true);
											handleDeviceStatusSave();
										}}
									>
										{buttonText == 'disconnected' ? 'Connect Device' : 'Disconnect Device'}
									</Button>
								</div>
							</>
						)
					)}
					{successMessage && (
						<>
							<Typography variant="h6" gutterBottom className="mt-10 text-center">
								Device Status
							</Typography>
							<Box
								style={{
									position: 'absolute',
									top: '50%',
									left: '50%',
									transform: 'translate(-50%, -50%)'
								}}
							>
								<Typography className="text-center text-green-500">{successMessage}</Typography>
							</Box>
						</>
					)}
				</Box>
			</Modal> */}

			<div className={classes.root}>
				<Pagination
					classes={{ ul: 'flex-nowrap' }}
					count={totalPages}
					defaultPage={1}
					color="primary"
					showFirstButton
					showLastButton
					variant="outlined"
					shape="rounded"
					onChange={handlePagination}
				/>

				<TablePagination
					classes={{ root: 'overflow-visible' }}
					rowsPerPageOptions={rowsPerPageOptions}
					component="div"
					count={totalElements}
					rowsPerPage={rowsPerPage}
					page={page}
					className={classes.toolbar}
					backIconButtonProps={{
						'aria-label': 'Previous Page',
						className: 'py-0'
					}}
					nextIconButtonProps={{
						'aria-label': 'Next Page',
						className: 'py-0'
					}}
					onChangePage={handleChangePage}
					onChangeRowsPerPage={handleChangeRowsPerPage}
				/>
			</div>
		</div>
	);
};

export default withRouter(DevicesTable);
