import FusePageCarded from '@fuse/core/FusePageCarded';
import { getUserPermissions } from 'app/store/dataSlice';
import withReducer from 'app/store/withReducer';
import React, { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import DownloadSchedulesHeader from './DownloadSchedulesHeader';
import DownloadSchedulesTable from './DownloadSchedulesTable';

const DownloadSchedules = () => {
	return (
		<FusePageCarded
			classes={{
				// content: 'flex',
				contentCard: 'overflow-hidden',
				header: 'min-h-74 h-64'
			}}
			header={<DownloadSchedulesHeader />}
			content={<DownloadSchedulesTable />}
			innerScroll
		/>
	);
};
export default withReducer('downloadschedulesManagement')(DownloadSchedules);
