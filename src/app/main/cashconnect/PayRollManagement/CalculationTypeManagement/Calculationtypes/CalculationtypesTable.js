import FuseLoading from '@fuse/core/FuseLoading';
import FuseScrollbars from '@fuse/core/FuseScrollbars';
import _ from '@lodash';
import { Typography } from '@material-ui/core';
import Checkbox from '@material-ui/core/Checkbox';
import { makeStyles } from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TablePagination from '@material-ui/core/TablePagination';
import TableRow from '@material-ui/core/TableRow';
import DeleteIcon from '@material-ui/icons/Delete';
import EditIcon from '@material-ui/icons/Edit';
import Pagination from '@material-ui/lab/Pagination';
import { rowsPerPageOptions } from 'app/@data/data';
import { SEARCH_CALCULATION_TYPE } from 'app/constant/constants';
import { motion } from 'framer-motion';
import React, { useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { withRouter } from 'react-router-dom';
import { getCalculationtypes, selectCalculationtypes } from '../store/calculationtypesSlice';
import CalculationtypesTableHead from './CalculationtypesTableHead';

const useStyles = makeStyles(() => ({
	root: {
		display: 'flex',
		justifyContent: 'space-between',
		flexWrap: 'nowrap',
		overflow: 'auto',
		minHeight: '35px'
	},
	toolbar: {
		'& > div': {
			minHeight: 'fit-content'
		}
	}
}));

const CalculationtypesTable = props => {
	const dispatch = useDispatch();
	const calculationtypes = useSelector(selectCalculationtypes);
	const searchText = useSelector(
		({ calculationtypesManagement }) => calculationtypesManagement.calculationtypes.searchText
	);
	const [searchCalculationtype, setSearchCalculationtype] = useState([]);
	const [loading, setLoading] = useState(true);
	const [selected, setSelected] = useState([]);
	const [data, setData] = useState(calculationtypes);
	const [page, setPage] = useState(0);
	const [rowsPerPage, setRowsPerPage] = useState(30);
	const [order, setOrder] = useState({
		direction: 'asc',
		id: null
	});
	let serialNumber = 1;
	const user_role = localStorage.getItem('user_role');
	const [parameter, setParameter] = useState({ page: 1, size: 30 });
	const totalPages = sessionStorage.getItem('calculationtypes_total_pages');
	const totalElements = sessionStorage.getItem('calculationtypes_total_elements');
	const classes = useStyles();

	useEffect(() => {
		dispatch(getCalculationtypes(parameter)).then(() => setLoading(false));
	}, [dispatch]);

	//search calculationtype
	useEffect(() => {
		searchText ? getSearchCalculationtype() : setSearchCalculationtype([]);
	}, [searchText]);

	const getSearchCalculationtype = () => {
		fetch(`${SEARCH_CALCULATION_TYPE}?name=${searchText}`)
			.then(response => response.json())
			.then(searchedCalculationtypesData => {
				setSearchCalculationtype(searchedCalculationtypesData.calculationtypes);
			})
			.catch(() => setSearchCalculationtype([]));
	};

	function handleRequestSort(calculationtypeEvent, property) {
		const id = property;
		let direction = 'desc';

		if (order.id === property && order.direction === 'desc') {
			direction = 'asc';
		}

		setOrder({
			direction,
			id
		});
	}

	function handleSelectAllClick(calculationtypeEvent) {
		if (calculationtypeEvent.target.checked) {
			setSelected(calculationtypes.map(n => n.id));
			return;
		}
		setSelected([]);
	}

	function handleDeselect() {
		setSelected([]);
	}

	function handleUpdateCalculationtype(item) {
		localStorage.removeItem('calculationtypeEvent');
		props.history.push(`/apps/calculationtype-management/calculationtypes/${item.id}/${item?.name}`);
	}
	function handleDeleteCalculationtype(item, calculationtypeEvent) {
		localStorage.removeItem('calculationtypeEvent');
		localStorage.setItem('calculationtypeEvent', calculationtypeEvent);
		props.history.push(`/apps/calculationtype-management/calculationtypes/${item.id}/${item?.name}`);
	}

	function handleCheck(calculationtypeEvent, id) {
		const selectedIndex = selected.indexOf(id);
		let newSelected = [];

		if (selectedIndex === -1) {
			newSelected = newSelected.concat(selected, id);
		} else if (selectedIndex === 0) {
			newSelected = newSelected.concat(selected.slice(1));
		} else if (selectedIndex === selected.length - 1) {
			newSelected = newSelected.concat(selected.slice(0, -1));
		} else if (selectedIndex > 0) {
			newSelected = newSelected.concat(selected.slice(0, selectedIndex), selected.slice(selectedIndex + 1));
		}

		setSelected(newSelected);
	}

	const handlePagination = (e, handlePage) => {
		setParameter({ ...parameter, page: handlePage });
		setPage(handlePage - 1);
		dispatch(getCalculationtypes({ ...parameter, page: handlePage }));
	};

	function handleChangePage(calculationtypeEvent, value) {
		setPage(value);
		setParameter({ ...parameter, page: value + 1 });
		dispatch(getCalculationtypes({ ...parameter, page: value - 1 }));
	}

	function handleChangeRowsPerPage(calculationtypeEvent) {
		setRowsPerPage(calculationtypeEvent.target.value);
		setParameter({ ...parameter, size: calculationtypeEvent.target.value });
		dispatch(getCalculationtypes({ ...parameter, size: calculationtypeEvent.target.value }));
	}

	if (loading) {
		return <FuseLoading />;
	}

	if (calculationtypes?.length === 0) {
		return (
			<motion.div
				initial={{ opacity: 0 }}
				animate={{ opacity: 1, transition: { delay: 0.1 } }}
				className="flex flex-1 items-center justify-center h-full"
			>
				<Typography color="textSecondary" variant="h5">
					There are no calculationtype!
				</Typography>
			</motion.div>
		);
	}

	return (
		<div className="w-full flex flex-col">
			<FuseScrollbars className="flex-grow overflow-x-auto">
				<Table stickyHeader className="min-w-xl" aria-labelledby="tableTitle">
					<CalculationtypesTableHead
						selectedCalculationtypeIds={selected}
						order={order}
						onSelectAllClick={handleSelectAllClick}
						onRequestSort={handleRequestSort}
						rowCount={calculationtypes.length}
						onMenuItemClick={handleDeselect}
					/>

					<TableBody>
						{_.orderBy(
							searchText !== '' && !_.isEmpty(searchCalculationtype)
								? searchCalculationtype
								: calculationtypes,
							[
								o => {
									switch (order.id) {
										case 'categories': {
											return o.categories[0];
										}
										default: {
											return o[order.id];
										}
									}
								}
							],
							[order.direction]
						).map(n => {
							const isSelected = selected.indexOf(n.id) !== -1;
							return (
								<TableRow
									className="h-52 cursor-pointer"
									hover
									role="checkbox"
									aria-checked={isSelected}
									tabIndex={-1}
									key={n.id}
									selected={isSelected}
								>
									<TableCell className="whitespace-nowrap w-40 md:w-64 text-center" padding="none">
										<Checkbox
											checked={isSelected}
											onClick={calculationtypeEvent => calculationtypeEvent.stopPropagation()}
											onChange={calculationtypeEvent => handleCheck(calculationtypeEvent, n.id)}
										/>
									</TableCell>

									<TableCell className="whitespace-nowrap w-40 md:w-64" component="th" scope="row">
										{parameter.page * parameter.size - parameter.size + serialNumber++}
									</TableCell>

									<TableCell className="whitespace-nowrap p-4 md:p-16" component="th" scope="row">
										{n?.name}
									</TableCell>

									<TableCell
										whitespace-nowrap
										className="p-4 md:p-16"
										align="center"
										component="th"
										scope="row"
									>
										<div>
											<EditIcon
												onClick={calculationtypeEvent => handleUpdateCalculationtype(n)}
												className="cursor-pointer"
												style={{ color: 'green' }}
											/>{' '}
											<DeleteIcon
												onClick={event => handleDeleteCalculationtype(n, 'Delete')}
												className="cursor-pointer"
												style={{
													color: 'red',
													visibility:
														user_role === 'ADMIN' || user_role === 'admin'
															? 'visible'
															: 'hidden'
												}}
											/>
										</div>
									</TableCell>
								</TableRow>
							);
						})}
					</TableBody>
				</Table>
			</FuseScrollbars>

			<div className={classes.root}>
				<Pagination
					classes={{ ul: 'flex-nowrap' }}
					count={totalPages}
					defaultPage={1}
					color="primary"
					showFirstButton
					showLastButton
					variant="outlined"
					shape="rounded"
					onChange={handlePagination}
				/>

				<TablePagination
					classes={{ root: 'overflow-visible' }}
					rowsPerPageOptions={rowsPerPageOptions}
					component="div"
					count={totalElements}
					rowsPerPage={rowsPerPage}
					page={page}
					className={classes.toolbar}
					backIconButtonProps={{
						'aria-label': 'Previous Page',
						className: 'py-0'
					}}
					nextIconButtonProps={{
						'aria-label': 'Next Page',
						className: 'py-0'
					}}
					onChangePage={handleChangePage}
					onChangeRowsPerPage={handleChangeRowsPerPage}
				/>
			</div>
		</div>
	);
};

export default withRouter(CalculationtypesTable);
