import FusePageCarded from '@fuse/core/FusePageCarded';
import { getUserPermissions } from 'app/store/dataSlice';
import withReducer from 'app/store/withReducer';
import React, { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import reducer from '../store/index';
import CalculationtypesHeader from './CalculationtypesHeader';
import CalculationtypesTable from './CalculationtypesTable';

const Calculationtypes = () => {
	const UserPermissions = useSelector(state => state.data.UserPermissions);
	useEffect(() => {
		dispatch(getUserPermissions());
	}, []);
	const dispatch = useDispatch();
	return (
		<FusePageCarded
			classes={{
				content: 'flex',
				contentCard: 'overflow-hidden',
				header: 'min-h-74 h-64'
			}}
			header={<CalculationtypesHeader />}
			content={<CalculationtypesTable />}
			innerScroll
		/>
	);
};
export default withReducer('calculationtypesManagement', reducer)(Calculationtypes);
