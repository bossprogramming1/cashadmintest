import _ from '@lodash';
import { makeStyles } from '@material-ui/core/styles';
import TextField from '@material-ui/core/TextField';
import { Autocomplete } from '@material-ui/lab';
import React, { useEffect } from 'react';
import { Controller, useFormContext } from 'react-hook-form';
import { useDispatch, useSelector } from 'react-redux';
import { useHistory, useParams } from 'react-router-dom';
// import { getCities } from '../../../../store/dataSlice';
import { getPayheadTypes } from 'app/store/dataSlice';
import { saveCalculationtype, updateCalculationtype } from '../store/calculationtypeSlice';

const useStyles = makeStyles(theme => ({
	hidden: {
		display: 'none'
	}
}));

function CalculationtypeForm(props) {
	const userID = localStorage.getItem('user_id');
	const payheadTypes = useSelector(state => state.data.payheadTypes);
	const classes = useStyles(props);
	const methods = useFormContext();
	const { control, formState, getValues } = methods;
	const { errors, isValid, dirtyFields } = formState;
	const routeParams = useParams();
	const { calculationtypeId } = routeParams;
	const history = useHistory();
	const handleDelete = localStorage.getItem('calculationtypeEvent');
	const dispatch = useDispatch();

	useEffect(() => {
		dispatch(getPayheadTypes());
	}, []);

	function handleSaveCalculationtype() {
		dispatch(saveCalculationtype(getValues())).then(res => {
			if (res.payload) {
				localStorage.setItem('calculationtypeAlert', 'saveCalculationtype');
				history.push('/apps/calculationtype-management/calculationtypes');
			}
		});
	}

	function handleUpdateCalculationtype() {
		dispatch(updateCalculationtype(getValues())).then(res => {
			if (res.payload) {
				localStorage.setItem('calculationtypeAlert', 'updateCalculationtype');
				history.push('/apps/calculationtype-management/calculationtypes');
			}
		});
	}

	const handleSubmitOnKeyDownEnter = ev => {
		if (ev.key === 'Enter') {
			if (routeParams.calculationtypeId === 'new' && !(_.isEmpty(dirtyFields) || !isValid)) {
				handleSaveCalculationtype();
			} else if (handleDelete !== 'Delete' && routeParams?.calculationtypeName) {
				handleUpdateCalculationtype();
			}
		}
	};

	return (
		<div>
			<Controller
				name="name"
				control={control}
				render={({ field }) => {
					return (
						<TextField
							{...field}
							className="mt-8 mb-16"
							error={!!errors?.name}
							helperText={errors?.name?.message}
							label="Calculationtype Name"
							id="name"
							required
							variant="outlined"
							InputLabelProps={field.value && { shrink: true }}
							fullWidth
							onKeyDown={handleSubmitOnKeyDownEnter}
						/>
					);
				}}
			/>
			<Controller
				name="payhead_type"
				control={control}
				render={({ field: { onChange, value, name } }) => (
					<Autocomplete
						className="mt-8 mb-16"
						freeSolo
						value={value ? payheadTypes.find(data => data.id === value) : null}
						options={payheadTypes}
						getOptionLabel={option => `${option?.name}`}
						onChange={(event, newValue) => {
							onChange(newValue?.id);
						}}
						renderInput={params => (
							<TextField
								{...params}
								placeholder="Select Payhead Type"
								label="Payhead Type"
								error={!!errors.payhead_type}
								required
								helperText={errors?.payhead_type?.message}
								variant="outlined"
								InputLabelProps={{
									shrink: true
								}}
								// onKeyDown={handleSubmitOnKeyDownEnter}
							/>
						)}
					/>
				)}
			/>

			{/* <Controller
				name="city"
				control={control}
				render={({ field: { onChange, value, name } }) => (
					<Autocomplete
						className="mt-8 mb-16"
						freeSolo
						value={value ? citys.find(data => data.id === value) : null}
						options={citys}
						getOptionLabel={option => `${option?.name}`}
						onChange={(event, newValue) => {
							onChange(newValue?.id);
						}}
						renderInput={params => (
							<TextField
								{...params}
								placeholder="Select City"
								label="City"
								error={!!errors.city}
								required
								helperText={errors?.city?.message}
								variant="outlined"
								autoFocus
								InputLabelProps={{
									shrink: true
								}}
								// onKeyDown={handleSubmitOnKeyDownEnter}
							/>
						)}
					/>
				)}
			/> */}
		</div>
	);
}

export default CalculationtypeForm;
