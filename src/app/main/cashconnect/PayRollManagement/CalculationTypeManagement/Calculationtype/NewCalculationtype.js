import FusePageCarded from '@fuse/core/FusePageCarded';
import { useDeepCompareEffect } from '@fuse/hooks';
import { yupResolver } from '@hookform/resolvers/yup';
import { Button, Typography } from '@material-ui/core';
import withReducer from 'app/store/withReducer';
import { motion } from 'framer-motion';
import React, { useEffect, useState } from 'react';
import { FormProvider, useForm } from 'react-hook-form';
import { useDispatch, useSelector } from 'react-redux';
import { Link, useParams } from 'react-router-dom';
import * as yup from 'yup';
import reducer from '../store/index';
import { getCalculationtype, newCalculationtype, resetCalculationtype } from '../store/calculationtypeSlice';
import CalculationtypeForm from './CalculationtypeForm';
import NewCalculationtypeHeader from './NewCalculationtypeHeader';

/**
 * Form Validation Schema
 */
const schema = yup.object().shape({
	name: yup.string().required('Name is required')
});

const Calculationtype = () => {
	const dispatch = useDispatch();
	const calculationtype = useSelector(({ calculationtypesManagement }) => calculationtypesManagement.calculationtype);

	const [noCalculationtype, setNoCalculationtype] = useState(false);
	const methods = useForm({
		mode: 'onChange',
		defaultValues: {},
		resolver: yupResolver(schema)
	});
	const routeParams = useParams();

	const { reset } = methods;

	useDeepCompareEffect(() => {
		function updateCalculationtypeState() {
			const { calculationtypeId } = routeParams;

			if (calculationtypeId === 'new') {
				localStorage.removeItem('event');
				/**
				 * Create New User data
				 */
				dispatch(newCalculationtype());
			} else {
				/**
				 * Get User data
				 */

				dispatch(getCalculationtype(calculationtypeId)).then(action => {
					/**
					 * If the requested product is not exist show message
					 */
					if (!action.payload) {
						setNoCalculationtype(true);
					}
				});
			}
		}

		updateCalculationtypeState();
	}, [dispatch, routeParams]);

	useEffect(() => {}, []);

	useEffect(() => {
		if (!calculationtype) {
			return;
		}
		/**
		 * Reset the form on calculationtype state changes
		 */
		reset(calculationtype);
	}, [calculationtype, reset]);

	useEffect(() => {
		return () => {
			/**
			 * Reset Calculationtype on component unload
			 */
			dispatch(resetCalculationtype());
			setNoCalculationtype(false);
		};
	}, [dispatch]);

	/**
	 * Show Message if the requested products is not exists
	 */
	if (noCalculationtype) {
		return (
			<motion.div
				initial={{ opacity: 0 }}
				animate={{ opacity: 1, transition: { delay: 0.1 } }}
				className="flex flex-col flex-1 items-center justify-center h-full"
			>
				<Typography color="textSecondary" variant="h5">
					There is no such calculationtype!
				</Typography>
				<Button
					className="mt-24"
					component={Link}
					variant="outlined"
					to="/apps/e-commerce/products"
					color="inherit"
				>
					Go to Calculationtype Page
				</Button>
			</motion.div>
		);
	}

	return (
		<FormProvider {...methods}>
			<FusePageCarded
				classes={{
					toolbar: 'p-0',
					header: 'min-h-80 h-80'
				}}
				header={<NewCalculationtypeHeader />}
				content={
					<div className="p-16 sm:p-24">
						<CalculationtypeForm />
					</div>
				}
				innerScroll
			/>
		</FormProvider>
	);
};
export default withReducer('calculationtypesManagement', reducer)(Calculationtype);
