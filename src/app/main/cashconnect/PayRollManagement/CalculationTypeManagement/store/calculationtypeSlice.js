import { createAsyncThunk, createSlice } from '@reduxjs/toolkit';
import {
	CREATE_CALCULATION_TYPE,
	DELETE_CALCULATION_TYPE,
	GET_CALCULATION_TYPEID,
	UPDATE_CALCULATION_TYPE
} from 'app/constant/constants';
import axios from 'axios';

export const getCalculationtype = createAsyncThunk(
	'calculationtypeManagement/calculationtype/getCalculationtype',
	async (params, { rejectWithValue }) => {
		const authTOKEN = {
			headers: {
				'Content-type': 'application/json',
				Authorization: localStorage.getItem('jwt_access_token')
			}
		};

		try {
			const response = await axios.get(`${GET_CALCULATION_TYPEID}${params}`, authTOKEN);
			const data = await response.data;
			return data === undefined ? null : data;
		} catch (err) {
			return rejectWithValue(params);
		}
	}
);

export const removeCalculationtype = createAsyncThunk(
	'calculationtypeManagement/calculationtype/removeCalculationtype',
	async val => {
		const authTOKEN = {
			headers: {
				'Content-type': 'application/json',
				Authorization: localStorage.getItem('jwt_access_token')
			}
		};

		const calculationtypeId = val.id;
		const response = await axios.delete(`${DELETE_CALCULATION_TYPE}${calculationtypeId}`, authTOKEN);
		return response;
	}
);

export const updateCalculationtype = createAsyncThunk(
	'calculationtypeManagement/calculationtype/updateCalculationtype',
	async (calculationtypeData, { dispatch, getState }) => {
		const { calculationtype } = getState().calculationtypesManagement;

		const authTOKEN = {
			headers: {
				'Content-type': 'application/json',
				Authorization: localStorage.getItem('jwt_access_token')
			}
		};
		const response = await axios.put(
			`${UPDATE_CALCULATION_TYPE}${calculationtype.id}`,
			calculationtypeData,
			authTOKEN
		);
		return response;
	}
);

export const saveCalculationtype = createAsyncThunk(
	'calculationtypeManagement/calculationtype/saveCalculationtype',
	async calculationtypeData => {
		const authTOKEN = {
			headers: {
				'Content-type': 'application/json',
				Authorization: localStorage.getItem('jwt_access_token')
			}
		};
		const response = await axios.post(`${CREATE_CALCULATION_TYPE}`, calculationtypeData, authTOKEN);
		return response;
	}
);

const calculationtypeSlice = createSlice({
	name: 'calculationtypeManagement/calculationtype',
	initialState: null,
	reducers: {
		resetCalculationtype: () => null,
		newCalculationtype: {
			reducer: (state, action) => action.payload,
			prepare: event => ({
				payload: {
					name: ''
				}
			})
		}
	},
	extraReducers: {
		[getCalculationtype.fulfilled]: (state, action) => action.payload,
		[saveCalculationtype.fulfilled]: (state, action) => action.payload,
		[removeCalculationtype.fulfilled]: (state, action) => action.payload,
		[updateCalculationtype.fulfilled]: (state, action) => action.payload
	}
});

export const { newCalculationtype, resetCalculationtype } = calculationtypeSlice.actions;

export default calculationtypeSlice.reducer;
