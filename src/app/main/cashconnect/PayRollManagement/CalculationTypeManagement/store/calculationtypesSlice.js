import { createAsyncThunk, createEntityAdapter, createSlice } from '@reduxjs/toolkit';
import { DELETE_CALCULATION_TYPE, GET_CALCULATION_TYPES } from 'app/constant/constants';
import axios from 'axios';

export const getCalculationtypes = createAsyncThunk(
	'calculationtypeManagement/calculationtypes/getCalculationtypes',
	async parameter => {
		const { page, size } = parameter;
		axios.defaults.headers.common['Content-type'] = 'application/json';
		axios.defaults.headers.common.Authorization = localStorage.getItem('jwt_access_token');

		const response = axios.get(GET_CALCULATION_TYPES, { params: { page, size } });
		const data = await response;

		sessionStorage.setItem('calculationtypes_total_elements', data.data.total_elements);
		sessionStorage.setItem('calculationtypes_total_pages', data.data.total_pages);
		delete axios.defaults.headers.common['Content-type'];
		delete axios.defaults.headers.common.Authorization;

		return data.data.calculation_types;
	}
);

export const removeCalculationtypes = createAsyncThunk(
	'calculationtypeManagement/calculationtypes/removeCalculationtypes',
	async (calculationtypeIds, { dispatch, getState }) => {
		const authTOKEN = {
			headers: {
				'Content-type': 'application/json',
				Authorization: localStorage.getItem('jwt_access_token')
			}
		};
		await axios.delete(`${DELETE_CALCULATION_TYPE}`, { calculationtypeIds }, authTOKEN);

		return calculationtypeIds;
	}
);

const calculationtypesAdapter = createEntityAdapter({});

export const { selectAll: selectCalculationtypes, selectById: selectCalculationtypeById } =
	calculationtypesAdapter.getSelectors(state => state.calculationtypesManagement.calculationtypes);

const calculationtypesSlice = createSlice({
	name: 'calculationtypeManagement/calculationtypes',
	initialState: calculationtypesAdapter.getInitialState({
		searchText: ''
	}),
	reducers: {
		setCalculationtypesSearchText: {
			reducer: (state, action) => {
				state.searchText = action.payload;
			},
			prepare: event => ({ payload: event.target.value || '' })
		}
	},
	extraReducers: {
		[getCalculationtypes.fulfilled]: calculationtypesAdapter.setAll
	}
});

export const { setData, setCalculationtypesSearchText } = calculationtypesSlice.actions;
export default calculationtypesSlice.reducer;
