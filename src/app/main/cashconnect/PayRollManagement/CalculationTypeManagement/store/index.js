import { combineReducers } from '@reduxjs/toolkit';
import calculationtype from './calculationtypeSlice';
import calculationtypes from './calculationtypesSlice';

const reducer = combineReducers({
	calculationtype,
	calculationtypes
});

export default reducer;
