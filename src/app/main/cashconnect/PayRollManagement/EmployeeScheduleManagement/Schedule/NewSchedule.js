import FusePageCarded from '@fuse/core/FusePageCarded';
import { useDeepCompareEffect } from '@fuse/hooks';
import { yupResolver } from '@hookform/resolvers/yup';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';
import withReducer from 'app/store/withReducer';
import { motion } from 'framer-motion';
import React, { useEffect, useState } from 'react';
import { FormProvider, useForm } from 'react-hook-form';
import { useDispatch, useSelector } from 'react-redux';
import { Link, useParams } from 'react-router-dom';
import * as yup from 'yup';
import reducer from '../store/index';
import { getSchedule, newSchedule, resetSchedule } from '../store/scheduleSlice';
import NewScheduleHeader from './NewScheduleHeader';
import ScheduleForm from './ScheduleForm';

/**
 * Form Validation Schema
 */
const schema = yup.object().shape({
	shift: yup.string().required('Shift is required'),
	start_date: yup.string().required('Start-Date is required'),
	end_date: yup.string().required('End-Date is required')
});

const NewSchedule = () => {
	const dispatch = useDispatch();
	const schedule = useSelector(({ schedulesManagement }) => schedulesManagement.schedule);
	const routeParams = useParams();
	const [noSchedule, setNoSchedule] = useState(false);
	const methods = useForm({
		mode: 'onChange',
		defaultValues: {},
		resolver: yupResolver(schema)
	});
	const { reset } = methods;

	useDeepCompareEffect(() => {
		function updateScheduleState() {
			const { scheduleId } = routeParams;
			if (scheduleId === 'new') {
				localStorage.removeItem('deleteSchedule');
				localStorage.removeItem('updateSchedule');
				/**
				 * Create New User data
				 */
				dispatch(newSchedule());
			} else {
				/**
				 * Get User data
				 */
				dispatch(getSchedule(routeParams)).then(action => {
					/**
					 * If the requested schedule is not exist show message
					 */
					if (!action.payload) {
						setNoSchedule(true);
					}
				});
			}
		}

		updateScheduleState();
	}, [dispatch, routeParams]);

	useEffect(() => {
		if (!schedule) {
			return;
		}
		/**
		 * Reset the form on schedule state changes
		 */
		reset(schedule);
	}, [schedule, reset]);

	useEffect(() => {
		return () => {
			/**
			 * Reset Schedule on component unload
			 */
			dispatch(resetSchedule());
			setNoSchedule(false);
		};
	}, [dispatch]);

	/**
	 * Show Message if the requested Schedules is not exists
	 */
	if (noSchedule) {
		return (
			<motion.div
				initial={{ opacity: 0 }}
				animate={{ opacity: 1, transition: { delay: 0.1 } }}
				className="flex flex-col flex-1 items-center justify-center h-full"
			>
				<Typography color="textSecondary" variant="h5">
					There is no such schedule!
				</Typography>
				<Button
					className="mt-24"
					component={Link}
					variant="outlined"
					to="/apps/schedules-management"
					color="inherit"
				>
					Go to Schedule Page
				</Button>
			</motion.div>
		);
	}

	/**
	 * Wait while product data is loading and form is setted
	 */

	return (
		<FormProvider {...methods}>
			<FusePageCarded
				classes={{
					toolbar: 'p-0',
					header: 'min-h-80 h-80'
				}}
				header={<NewScheduleHeader />}
				content={
					<div className="p-16 sm:p-24">
						<ScheduleForm />
					</div>
				}
				innerScroll
			/>
		</FormProvider>
	);
};
export default withReducer('schedulesManagement', reducer)(NewSchedule);
