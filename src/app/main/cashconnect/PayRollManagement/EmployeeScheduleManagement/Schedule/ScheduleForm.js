import _ from '@lodash';
import { Checkbox, FormControl, FormControlLabel } from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';
import TextField from '@material-ui/core/TextField';
import { KeyboardArrowDown, KeyboardArrowRight } from '@material-ui/icons';
import { Autocomplete } from '@material-ui/lab';
import { TimePicker } from '@material-ui/pickers';
import CustomDatePicker from 'app/@components/CustomDatePicker';
import { getEmployees, getShifts } from 'app/store/dataSlice';
import moment from 'moment';
import React, { useEffect, useState } from 'react';
import { Controller, useFormContext } from 'react-hook-form';
import InputColor from 'react-input-color';
import { motion } from 'framer-motion';
import { useDispatch, useSelector } from 'react-redux';
import { Typography } from '@mui/material';
import { useHistory, useParams } from 'react-router-dom';
import DepartmnetTable from './DepartmnetTable';
import { saveSchedule, updateSchedule } from '../store/scheduleSlice';

const useStyles = makeStyles(theme => ({
	pageContainer: {
		display: 'flex',
		marginTop: 'auto',
		marginBottom: 'auto'
	},
	clmsContainer: {
		display: 'flex',
		flexDirection: 'column',
		flexWrap: 'wrap',
		maxHeight: 'calc(75vh - 180px)',
		transition: '1s',
		alignContent: 'flex-start',
		marginTop: '5px',
		marginBottom: '5px'
	},
	clmsContainerShift: {
		display: 'flex',
		flexDirection: 'column',
		flexWrap: 'wrap',
		maxHeight: 'calc(50vh - 180px)',
		transition: '1s',
		alignContent: 'flex-start',
		marginTop: '5px',
		marginBottom: '5px'
	},
	dept: {
		backgroundColor: 'red'
	}
}));

function ScheduleForm(props) {
	const userID = localStorage.getItem('user_id');

	const employees = useSelector(({ schedulesManagement }) => schedulesManagement?.employeeList);
	const shifts = useSelector(state => state.data.shift);
	const [color, setColor] = React.useState({});
	const classes = useStyles(props);
	const methods = useFormContext();
	const { control, formState, setValue, getValues, reset } = methods;
	const { errors, isValid, dirtyFields } = formState;
	const routeParams = useParams();
	const { scheduleId } = routeParams;
	const history = useHistory();
	const handleDelete = localStorage.getItem('scheduleEvent');
	const dispatch = useDispatch();
	const [shiftChecked, setShiftChecked] = useState({});
	const handleChange = id => {
		setShiftChecked({ [id]: true });
	};

	useEffect(() => {
		dispatch(getEmployees());
		dispatch(getShifts());
	}, []);

	//set color coder
	useEffect(() => {
		setValue(`color`, color.hex || '#0032faff');
	}, [color.hex]);

	function handleSaveSchedule() {
		dispatch(saveSchedule(getValues())).then(res => {
			if (res.payload) {
				localStorage.setItem('scheduleAlert', 'saveSchedule');
				history.push('/apps/schedules-management');
			}
		});
	}

	function handleUpdateSchedule() {
		dispatch(updateSchedule(getValues())).then(res => {
			if (res.payload) {
				localStorage.setItem('scheduleAlert', 'updateSchedule');
				history.push('/apps/schedules-management');
			}
		});
	}

	const handleSubmitOnKeyDownEnter = ev => {
		if (ev.key === 'Enter') {
			if (routeParams.scheduleId === 'new' && !(_.isEmpty(dirtyFields) || !isValid)) {
				handleSaveSchedule();
			} else if (handleDelete !== 'Delete' && routeParams?.scheduleName) {
				handleUpdateSchedule();
			}
		}
	};

	return (
		<div>
			<div className="flex">
				<div className=" w-1/3 p-12">
					<DepartmnetTable />
				</div>

				{employees?.length !== 0 && employees ? (
					<div>
						<h5> Employees</h5>
						<div className={classes.pageContainer}>
							<div className={classes.clmsContainer}>
								<Controller
									name={`all`}
									control={control}
									render={({ field }) => (
										<FormControl>
											<FormControlLabel
												required
												label={`ALL`}
												control={
													<Checkbox
														{...field}
														color="primary"
														onChange={event => {
															field.onChange(event);

															let uniqPermissinsIds = _.uniq(getValues()?.employee_ids);

															if (event.target.checked) {
																employees?.map((employee, indx) => {
																	uniqPermissinsIds.push(employee.id);
																});
																reset({
																	...getValues(),
																	employee_ids: _.uniq(uniqPermissinsIds)
																});
															} else {
																reset({ ...getValues(), employee_ids: [] });
															}
														}}
														checked={field.value ? field.value : false}
													/>
												}
											/>
										</FormControl>
									)}
								/>

								{employees?.map(employee => {
									return (
										<Controller
											key={employee?.id}
											name={`employee${employee?.id}`}
											control={control}
											render={({ field }) => (
												<FormControl>
													<FormControlLabel
														required
														label={`${employee?.first_name} ${employee?.last_name}`}
														control={
															<Checkbox
																{...field}
																color="primary"
																// onChange={(event) => handleOnChange(event)}
																checked={
																	getValues()?.employee_ids?.find(
																		id => id == employee?.id
																	) || false
																}
																onChange={event => {
																	if (event.target.checked) {
																		const unicEmployeeIds = _.uniq(
																			getValues()?.employee_ids
																		);
																		reset({
																			...getValues(),
																			employee_ids: [
																				...unicEmployeeIds,
																				employee?.id
																			]
																		});
																	} else {
																		let removableId =
																			getValues()?.employee_ids?.indexOf(
																				employee?.id
																			);
																		let employeeIdAll = _.uniq(
																			getValues()?.employee_ids
																		);
																		employeeIdAll.splice(removableId, 1);
																		reset({
																			...getValues(),
																			employee_ids: employeeIdAll
																		});
																	}
																}}
															/>
														}
													/>
												</FormControl>
											)}
										/>
									);
								})}
							</div>
						</div>
					</div>
				) : (
					<motion.div
						initial={{ opacity: 0 }}
						animate={{ opacity: 1, transition: { delay: 0.1 } }}
						className="flex flex-1 items-end justify-center  h-full"
					>
						<Typography mt="100px" color="textSecondary" variant="h5">
							There are no Employees!
						</Typography>
					</motion.div>
				)}
			</div>

			{/* <div style={{ display: 'flex', flexDirection: 'column', marginTop: '20px' }}>
				<h5> Employees</h5>

				{employees.slice(0, 1).map(employee => {
					return (
						<div style={{ display: 'flex', flexDirection: 'column', marginBottom: '10px' }}>
							<Controller
								name={`employee${employee.id}`}
								control={control}
								render={({ field }) => (
									<FormControl>
										<div style={{ display: 'flex', flexDirection: 'row' }}>
											{getValues()[`employee${employee.id}`] ? (
												<KeyboardArrowDown
													style={{ marginTop: '10px', marginRight: '5px', cursor: 'pointer' }}
													onClick={() =>
														reset({ ...getValues(), [`employee${employee.id}`]: false })
													}
												/>
											) : (
												<KeyboardArrowRight
													style={{ marginTop: '10px', marginRight: '5px', cursor: 'pointer' }}
													onClick={() =>
														reset({ ...getValues(), [`employee${employee.id}`]: true })
													}
												/>
											)}

											<FormControlLabel
												required
												label={`All`}
												control={
													<>
														<Checkbox
															{...field}
															color="primary"
															onChange={event => {
																let uniqEmployeeIds = _.uniq(getValues().employee_ids);

																if (event.target.checked) {
																	employees?.map(employee => {
																		uniqEmployeeIds.push(employee.id);
																	});
																	uniqEmployeeIds.push(employee.id);

																	reset({
																		...getValues(),
																		employee_ids: _.uniq(uniqEmployeeIds)
																	});
																} else {
																	let employeeAll = _.uniq(getValues()?.employee_ids);

																	employees?.map(employee => {
																		let removableIndex = employeeAll?.indexOf(
																			employee?.id
																		);

																		if (removableIndex >= 0) {
																			employeeAll.splice(removableIndex, 1);
																		}
																	});
																	employeeAll.splice(
																		employeeAll?.indexOf(employee.id),
																		1
																	);

																	reset({
																		...getValues(),
																		employee_ids: _.uniq(employeeAll)
																	});
																}
															}}
															checked={
																getValues().employee_ids?.find(
																	id => id == employee?.id
																) || false
															}
														/>
													</>
												}
											/>
										</div>
									</FormControl>
								)}
							/>

							{employees && getValues()[`employee${employee.id}`]
								? employees?.map(employee => {
										return (
											<Controller
												key={employee?.id}
												name={`employee${employee?.id}`}
												control={control}
												render={({ field }) => (
													<FormControl style={{ marginLeft: '55px' }}>
														<FormControlLabel
															required
															label={`${employee?.first_name} ${employee?.last_name}`}
															control={
																<Checkbox
																	{...field}
																	color="primary"
																	checked={
																		getValues()?.employee_ids?.find(
																			id => id == employee.id
																		) || false
																	}
																	onChange={event => {
																		if (event.target.checked) {
																			let unicEmployeeds = _.uniq(
																				getValues()?.employee_ids
																			);
																			unicEmployeeds.push(employee.id);
																			unicEmployeeds.push(employee.id);
																			reset({
																				...getValues(),
																				employee_ids: _.uniq(unicEmployeeds)
																			});
																		} else {
																			let employeeAll = _.uniq(
																				getValues()?.employee_ids
																			);
																			let removableIndex = employeeAll?.indexOf(
																				employee.id
																			);
																			if (removableIndex >= 0) {
																				employeeAll.splice(removableIndex, 1);
																			}
																			reset({
																				...getValues(),
																				employee_ids: _.uniq(employeeAll)
																			});
																		}
																	}}
																/>
															}
														/>
													</FormControl>
												)}
											/>
										);
								  })
								: null}
						</div>
					);
				})}
			</div> */}
			<div style={{ display: 'flex', flexDirection: 'column', marginTop: '2rem' }}>
				<h5> Shift</h5>

				<div ClassName={classes.pageContainer}>
					<div className={classes.clmsContainerShift}>
						{shifts.map(e => {
							return (
								<Controller
									key={e?.id}
									name={`shift${e?.id}`}
									control={control}
									render={({ field }) => (
										<FormControl>
											<FormControlLabel
												required
												label={`${e?.name} `}
												control={
													<Checkbox
														{...field}
														color="primary"
														// onChange={(event) => handleOnChange(event)}
														// checked={getValues()?.shift?.find(id => id == e?.id) || false}
														checked={shiftChecked[e?.id] || false}
														onChange={event => {
															handleChange(e?.id);
															if (event.target.checked) {
																const unicShiftIds = _.uniq(getValues()?.shift);
																reset({
																	...getValues(),
																	shift: e?.id
																});
															}
														}}
													/>
												}
											/>
										</FormControl>
									)}
								/>
							);
						})}
					</div>
				</div>
			</div>
			{/* <div style={{ display: 'flex', flexDirection: 'column', marginTop: '20px' }}>
				<h5> Employees</h5>

				{employees.slice(0, 1).map(employee => {
					return (
						<div style={{ display: 'flex', flexDirection: 'column', marginBottom: '10px' }}>
							<Controller
								name={`employee${employee.id}`}
								control={control}
								render={({ field }) => (
									<FormControl>
										<div style={{ display: 'flex', flexDirection: 'row' }}>
											{getValues()[`employee${employee.id}`] ? (
												<KeyboardArrowDown
													style={{ marginTop: '10px', marginRight: '5px', cursor: 'pointer' }}
													onClick={() =>
														reset({ ...getValues(), [`employee${employee.id}`]: false })
													}
												/>
											) : (
												<KeyboardArrowRight
													style={{ marginTop: '10px', marginRight: '5px', cursor: 'pointer' }}
													onClick={() =>
														reset({ ...getValues(), [`employee${employee.id}`]: true })
													}
												/>
											)}

											<FormControlLabel
												required
												label={`All`}
												control={
													<>
														<Checkbox
															{...field}
															color="primary"
															onChange={event => {
																let uniqEmployeeIds = _.uniq(getValues().employee_ids);

																if (event.target.checked) {
																	employees?.map(employee => {
																		uniqEmployeeIds.push(employee.id);
																	});
																	uniqEmployeeIds.push(employee.id);

																	reset({
																		...getValues(),
																		employee_ids: _.uniq(uniqEmployeeIds)
																	});
																} else {
																	let employeeAll = _.uniq(getValues()?.employee_ids);

																	employees?.map(employee => {
																		let removableIndex = employeeAll?.indexOf(
																			employee?.id
																		);

																		if (removableIndex >= 0) {
																			employeeAll.splice(removableIndex, 1);
																		}
																	});
																	employeeAll.splice(
																		employeeAll?.indexOf(employee.id),
																		1
																	);

																	reset({
																		...getValues(),
																		employee_ids: _.uniq(employeeAll)
																	});
																}
															}}
															checked={
																getValues().employee_ids?.find(
																	id => id == employee?.id
																) || false
															}
														/>
													</>
												}
											/>
										</div>
									</FormControl>
								)}
							/>

							{employees && getValues()[`employee${employee.id}`]
								? employees?.map(employee => {
										return (
											<Controller
												key={employee?.id}
												name={`employee${employee?.id}`}
												control={control}
												render={({ field }) => (
													<FormControl style={{ marginLeft: '55px' }}>
														<FormControlLabel
															required
															label={`${employee?.first_name} ${employee?.last_name}`}
															control={
																<Checkbox
																	{...field}
																	color="primary"
																	checked={
																		getValues()?.employee_ids?.find(
																			id => id == employee.id
																		) || false
																	}
																	onChange={event => {
																		if (event.target.checked) {
																			let unicEmployeeds = _.uniq(
																				getValues()?.employee_ids
																			);
																			unicEmployeeds.push(employee.id);
																			unicEmployeeds.push(employee.id);
																			reset({
																				...getValues(),
																				employee_ids: _.uniq(unicEmployeeds)
																			});
																		} else {
																			let employeeAll = _.uniq(
																				getValues()?.employee_ids
																			);
																			let removableIndex = employeeAll?.indexOf(
																				employee.id
																			);
																			if (removableIndex >= 0) {
																				employeeAll.splice(removableIndex, 1);
																			}
																			reset({
																				...getValues(),
																				employee_ids: _.uniq(employeeAll)
																			});
																		}
																	}}
																/>
															}
														/>
													</FormControl>
												)}
											/>
										);
								  })
								: null}
						</div>
					);
				})}
			</div> */}

			{/* <div style={{ display: 'flex', flexDirection: 'column', marginTop: '20px' }}>
				<h5> Shifts</h5>

				{shifts.slice(0, 1).map(shift => {
					return (
						<div style={{ display: 'flex', flexDirection: 'column', marginBottom: '10px' }}>
							<Controller
								name={`employee${shift.id}`}
								control={control}
								render={({ field }) => (
									<FormControl>
										<div style={{ display: 'flex', flexDirection: 'row' }}>
											{getValues()[`employee${shift.id}`] ? (
												<KeyboardArrowDown
													style={{ marginTop: '10px', marginRight: '5px', cursor: 'pointer' }}
													onClick={() =>
														reset({ ...getValues(), [`employee${shift.id}`]: false })
													}
												/>
											) : (
												<KeyboardArrowRight
													style={{ marginTop: '10px', marginRight: '5px', cursor: 'pointer' }}
													onClick={() =>
														reset({ ...getValues(), [`employee${shift.id}`]: true })
													}
												/>
											)}

											<FormControlLabel
												required
												label={`All`}
												control={
													<>
														<Checkbox
															{...field}
															color="primary"
															onChange={event => {
																let uniqShiftIds = _.uniq(getValues().shift);

																if (event.target.checked) {
																	shifts?.map(shift => {
																		uniqShiftIds.push(shift.id);
																	});
																	uniqShiftIds.push(shift.id);

																	reset({
																		...getValues(),
																		shift: _.uniq(uniqShiftIds)
																	});
																} else {
																	let shiftAll = _.uniq(getValues()?.shift);

																	employees?.map(employee => {
																		let removableIndex = shiftAll?.indexOf(
																			employee?.id
																		);

																		if (removableIndex >= 0) {
																			shiftAll.splice(removableIndex, 1);
																		}
																	});
																	shiftAll.splice(shiftAll?.indexOf(shift.id), 1);

																	reset({
																		...getValues(),
																		shift: _.uniq(shiftAll)
																	});
																}
															}}
															checked={
																getValues().shift.find(id => id == shift?.id) || false
															}
														/>
													</>
												}
											/>
										</div>
									</FormControl>
								)}
							/>

							{shifts && getValues()[`shift${shift.id}`]
								? shifts?.map(shift => {
										return (
											<Controller
												key={shift?.id}
												name={`shift${shift?.id}`}
												control={control}
												render={({ field }) => (
													<FormControl style={{ marginLeft: '55px' }}>
														<FormControlLabel
															required
															label={`${shift?.name}`}
															control={
																<Checkbox
																	{...field}
																	color="primary"
																	checked={
																		getValues()?.shift?.find(
																			id => id == shift.id
																		) || false
																	}
																	onChange={event => {
																		if (event.target.checked) {
																			let unicShifts = _.uniq(getValues()?.shift);
																			unicShifts.push(shift.id);
																			unicShifts.push(shift.id);
																			reset({
																				...getValues(),
																				shift: _.uniq(unicShifts)
																			});
																		} else {
																			let shiftAll = _.uniq(getValues()?.shift);
																			let removableIndex = shiftAll?.indexOf(
																				shift.id
																			);
																			if (removableIndex >= 0) {
																				shiftAll.splice(removableIndex, 1);
																			}
																			reset({
																				...getValues(),
																				shift: _.uniq(shiftAll)
																			});
																		}
																	}}
																/>
															}
														/>
													</FormControl>
												)}
											/>
										);
								  })
								: null}
						</div>
					);
				})}
			</div> */}
			{/* <Controller
				name="shift"
				control={control}
				render={({ field: { onChange, value } }) => (
					<Autocomplete
						className="mt-8 mb-16"
						freeSolo
						value={value ? shifts.find(shift => shift.id === value) : null}
						options={shifts}
						getOptionLabel={option => `${option?.name}`}
						onChange={(event, newValue) => {
							onChange(newValue?.id);
						}}
						renderInput={params => (
							<TextField
								{...params}
								placeholder="Select a Shift"
								label="Shift"
								variant="outlined"
								required
								InputLabelProps={{
									shrink: true
								}}
							/>
						)}
					/>
				)}
			/> */}

			<div style={{ display: 'grid', gridTemplateColumns: 'repeat(2, 1fr)', columnGap: '9px' }}>
				<Controller
					name="start_date"
					control={control}
					render={({ field }) => <CustomDatePicker field={field} label="Start Date" />}
				/>
				<Controller
					name="end_date"
					control={control}
					render={({ field }) => <CustomDatePicker field={field} label="End Date" />}
				/>
			</div>
		</div>
	);
}

export default ScheduleForm;
