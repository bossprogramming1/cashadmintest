import Button from '@material-ui/core/Button';
import Icon from '@material-ui/core/Icon';
import { useTheme } from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';
import { motion } from 'framer-motion';
import _ from 'lodash';
import moment from 'moment';
import React from 'react';
import { useFormContext } from 'react-hook-form';
import { useDispatch } from 'react-redux';
import { Link, useHistory, useParams } from 'react-router-dom';
import { removeSchedule, saveSchedule, updateSchedule } from '../store/scheduleSlice';

const NewScheduleHeader = () => {
	const dispatch = useDispatch();
	const methods = useFormContext();
	const { getValues, watch, setError, formState } = methods;
	const name = watch('employee');
	const theme = useTheme();
	const history = useHistory();
	const { isValid, dirtyFields } = formState;

	const routeParams = useParams();

	const handleDelete = localStorage.getItem('deleteSchedule');
	const handleUpdate = localStorage.getItem('updateSchedule');

	function handleSaveSchedule() {
		const data = getValues();
		const confirmedData = {
			shift: data?.shift,
			employee_ids: data?.employee_ids,
			start_date: data?.start_date,
			end_date: data?.end_date
		};
		dispatch(saveSchedule(confirmedData)).then(res => {
			if (res?.payload?.status == 200 || res?.payload?.status == 201) {
				localStorage.setItem('scheduleAlertPermission', 'saveScheduleSuccessfully');
				history.push('/apps/schedules-management');
			} else if (res?.payload?.data?.detail) {
				setError('name', { type: 'manual', message: res.payload.data.detail });
			}
		});
	}

	function handleUpdateSchedule() {
		dispatch(updateSchedule(getValues())).then(res => {
			if (res.payload.data?.id) {
				localStorage.setItem('scheduleAlert', 'updateSchedule');
				history.push('/apps/schedules-management');
			} else if (res.payload.data?.detail) {
				setError('name', { type: 'manual', message: res.payload.data.detail });
			}
		});
	}

	function handleRemoveSchedule() {
		dispatch(removeSchedule(getValues())).then(res => {
			if (res.payload) {
				localStorage.removeItem('scheduleEvent');
				localStorage.setItem('scheduleAlert', 'deleteSchedule');
				history.push('/apps/schedules-management');
			}
		});
	}

	function handleCancel() {
		history.push('/apps/schedules-management');
	}



	return (
		<div className="flex flex-1 w-full items-center justify-between">
			<div className="flex flex-col items-start max-w-full min-w-0">
				<motion.div initial={{ x: 20, opacity: 0 }} animate={{ x: 0, opacity: 1, transition: { delay: 0.3 } }}>
					<Typography
						className="flex items-center sm:mb-2"
						component={Link}
						schedule="button"
						to="/apps/schedules-management"
						color="inherit"
					>
						<Icon className="text-20">{theme.direction === 'ltr' ? 'arrow_back' : 'arrow_forward'}</Icon>
						<span className="hidden sm:flex mx-4 font-medium">Schedules</span>
					</Typography>
				</motion.div>

				<div className="flex items-center max-w-full">
					<motion.div
						className="hidden sm:flex"
						initial={{ scale: 0 }}
						animate={{ scale: 1, transition: { delay: 0.3 } }}
					></motion.div>
					<div className="flex flex-col min-w-0 mx-8 sm:mc-16">
						<motion.div initial={{ x: -20 }} animate={{ x: 0, transition: { delay: 0.3 } }}>
							<Typography className="text-16 sm:text-20 truncate font-semibold">
								{name || 'Create New Schedule'}
							</Typography>
							<Typography variant="caption" className="font-medium">
								Schedules Detail
							</Typography>
						</motion.div>
					</div>
				</div>
			</div>
			<motion.div
				className="flex"
				initial={{ opacity: 0, x: 20 }}
				animate={{ opacity: 1, x: 0, transition: { delay: 0.3 } }}
			>
				{handleDelete === 'deleteSchedule' && (
					<Typography className="mt-6" variant="subtitle2">
						Do you want to remove this Schedules?
					</Typography>
				)}
				{handleDelete === 'deleteSchedule' && (
					<Button
						className="whitespace-nowrap mx-4"
						variant="contained"
						color="secondary"
						onClick={handleRemoveSchedule}
						startIcon={<Icon className="hidden sm:flex">delete</Icon>}
						style={{ backgroundColor: '#ea5b78', color: 'white' }}
					>
						Remove
					</Button>
				)}
				{handleDelete !== 'deleteSchedule' && routeParams.scheduleId === 'new' && (
					<Button
						className="whitespace-nowrap mx-4"
						variant="contained"
						color="secondary"
						disabled={_.isEmpty(dirtyFields) || !isValid}
						onClick={handleSaveSchedule}
					>
						Save
					</Button>
				)}
				{handleDelete !== 'deleteSchedule' && handleUpdate === 'updateSchedule' && (
					<Button
						className="whitespace-nowrap mx-4"
						color="secondary"
						variant="contained"
						//disabled={_.isEmpty(dirtyFields) || !isValid}
						style={{ backgroundColor: '#4dc08e', color: 'white' }}
						onClick={handleUpdateSchedule}
					>
						Update
					</Button>
				)}
				<Button
					className="whitespace-nowrap mx-4"
					variant="contained"
					style={{ backgroundColor: '#FFAA4C', color: 'white' }}
					onClick={handleCancel}
				>
					Cancel
				</Button>
			</motion.div>
		</div>
	);
};

export default NewScheduleHeader;
