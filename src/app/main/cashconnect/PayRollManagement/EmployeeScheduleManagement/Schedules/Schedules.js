import FusePageCarded from '@fuse/core/FusePageCarded';
import withReducer from 'app/store/withReducer';
import React from 'react';
import { useDispatch } from 'react-redux';
import reducer from '../store/index';
import SchedulesHeader from './SchedulesHeader';
import SchedulesTable from './SchedulesTable';

const Schedules = () => {
	return (
		<FusePageCarded
			classes={{
				content: 'flex',
				contentCard: 'overflow-hidden',
				header: 'min-h-74 h-64'
			}}
			header={<SchedulesHeader />}
			content={<SchedulesTable />}
			innerScroll
		/>
	);
};
export default withReducer('schedulesManagement', reducer)(Schedules);
