import Checkbox from '@material-ui/core/Checkbox';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import TableSortLabel from '@material-ui/core/TableSortLabel';
import Tooltip from '@material-ui/core/Tooltip';
import React from 'react';
//import {removeEmployees} from '../store/employeesSlice'

const rows = [
	{
		id: 'sl_no',
		align: 'left',
		disablePadding: false,
		label: 'SL_NO',
		sort: true
	},
	{
		id: 'name',
		align: 'left',
		disablePadding: false,
		label: 'Name',
		sort: true
	},
	{
		id: 'onduty_time',
		align: 'left',
		disablePadding: false,
		label: 'Onduty Time',
		sort: true
	},
	{
		id: 'offduty_time',
		align: 'left',
		disablePadding: false,
		label: 'Offduty Time',
		sort: true
	},
	{
		id: 'checkin_start',
		align: 'left',
		disablePadding: false,
		label: 'Checkin Start',
		sort: true
	},
	{
		id: 'checkin_end',
		align: 'left',
		disablePadding: false,
		label: 'Checkin End',
		sort: true
	},
	{
		id: 'checkout_start',
		align: 'left',
		disablePadding: false,
		label: 'Checkout Start',
		sort: true
	},
	{
		id: 'checkout_end',
		align: 'left',
		disablePadding: false,
		label: 'Checkout End',
		sort: true
	},

	{
		id: 'color',
		align: 'left',
		disablePadding: false,
		label: 'Color',
		sort: true
	},
	{
		id: 'action',
		align: 'left',
		disablePadding: false,
		label: 'Action',
		sort: true
	}
];

const SchedulesTableHead = props => {
	const { selectedEmployeeIds } = props;
	const numSelected = selectedEmployeeIds.length;

	const createSortHandler = property => event => {
		props.onRequestSort(event, property);
	};

	return (
		<TableHead>
			<TableRow className="h-48 sm:h-64">
				<TableCell padding="none" className="w-40 md:w-64 text-center z-99">
					<Checkbox
						indeterminate={numSelected > 0 && numSelected < props.rowCount}
						checked={props.rowCount !== 0 && numSelected === props.rowCount}
						onChange={props.onSelectAllClick}
					/>
				</TableCell>
				{rows.map(row => {
					return (
						<TableCell
							className="p-4 md:p-16 whitespace-nowrap"
							key={row.id}
							align={row.align}
							padding={row.disablePadding ? 'none' : 'default'}
							//sortDirection={props.order.id === row.id ? props.order.direction : false}
						>
							{row.sort && (
								<Tooltip
									title={row?.label}
									placement={row.align === 'right' ? 'bottom-end' : 'bottom-start'}
									enterDelay={300}
								>
									<TableSortLabel
										active={props.order.id === row.id}
										direction={props.order.direction}
										onClick={createSortHandler(row.id)}
										className="font-semibold"
									>
										{row.label}
									</TableSortLabel>
								</Tooltip>
							)}
						</TableCell>
					);
				}, this)}
			</TableRow>
		</TableHead>
	);
};

export default SchedulesTableHead;
