import Paper from '@material-ui/core/Paper';
import Typography from '@material-ui/core/Typography';
import _ from '@lodash';
import { useEffect, memo, useState } from 'react';
import { Box, Table, TableBody, TableCell, TableHead, TableRow } from '@mui/material';
import { makeStyles } from '@material-ui/core/styles';
import { useDispatch, useSelector } from 'react-redux';

import { getDepartments } from 'app/store/dataSlice';
import FuseScrollbars from '@fuse/core/FuseScrollbars';
import { getEmployeeSchedule } from '../store/scheduleEmployeeSlice';
// import { getShiftTimetable } from '../store/shiftSlice';

const useStyles = makeStyles(theme => ({
	tablecell: {
		fontSize: '50px'
	}
}));

function WidgetTable(props) {
	const dispatch = useDispatch();
	const classes = useStyles(props);
	const departments = useSelector(state => state.data?.departments);

	useEffect(() => {
		dispatch(getDepartments());
	}, []);
	const DeptId = e => {
		dispatch(getEmployeeSchedule(e));
	};

	return (
		<FuseScrollbars className="flex-grow overflow-x-auto">
			<Paper className="w-full rounded-40" style={{ backgroundColor: '#8eb0ceed' }}>
				<div className="flex items-center justify-between p-20 h-64">
					<Typography className="text-16 font-medium"> Departments</Typography>
				</div>
				<Box m="10px">
					<Table>
						<TableHead></TableHead>
						<TableBody>
							{departments?.map(department => {
								return (
									<TableRow hover key={department?.id}>
										<TableCell style={{ fontSize: '12px' }} onClick={() => DeptId(department?.id)}>
											{department?.name}
										</TableCell>
									</TableRow>
								);
							})}
						</TableBody>
					</Table>
				</Box>
			</Paper>
		</FuseScrollbars>
	);
}

export default memo(WidgetTable);
