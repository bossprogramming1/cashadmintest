import { combineReducers } from '@reduxjs/toolkit';
import schedule from './scheduleSlice';
import schedules from './schedulesSlice';
import employeeSchedule from './scheduleEmployeeSlice';
import employeeList from './scheduleDeptIdEmployeeSlice';
import employeeTimetableSchedule from './scheduleEmployeeTimetableSlice';

const reducer = combineReducers({
	schedule,
	schedules,
	employeeSchedule,
	employeeList,
	employeeTimetableSchedule
});

export default reducer;
