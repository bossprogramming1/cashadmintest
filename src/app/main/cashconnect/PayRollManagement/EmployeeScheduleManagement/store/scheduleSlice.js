import { createAsyncThunk, createSlice } from '@reduxjs/toolkit';
import {
	CREATE_SCHEDULE,
	DELETE_SCHEDULE,
	GET_EMPLOYEE_SCHEDULE_BY_DEPT_ID,
	GET_EMPLOYEE_TIMETABLE_BY_EMP_ID_SHIFT_ID,
	GET_SCHEDULE,
	UPDATE_SCHEDULE
} from 'app/constant/constants';
import axios from 'axios';

export const getSchedule = createAsyncThunk('scheduleManagement/schedule/getSchedule', async params => {
	const authTOKEN = {
		headers: {
			'Content-type': 'application/json',
			Authorization: localStorage.getItem('jwt_access_token')
		}
	};
	const response = await axios.get(`${GET_SCHEDULE}${params.scheduleId}`, authTOKEN);
	const data = await response.data;
	return data === undefined ? null : data;
});

export const removeSchedule = createAsyncThunk('scheduleManagement/schedule/removeSchedule', async val => {
	const authTOKEN = {
		headers: {
			'Content-type': 'application/json',
			Authorization: localStorage.getItem('jwt_access_token')
		}
	};
	const scheduleId = val.id;
	const response = await axios.delete(`${DELETE_SCHEDULE}${scheduleId}`, authTOKEN);
	return response;
});

export const updateSchedule = createAsyncThunk(
	'scheduleManagement/schedule/updateSchedule',
	async (scheduleData, { dispatch, getState }) => {
		const authTOKEN = {
			headers: {
				'Content-type': 'application/json',
				Authorization: localStorage.getItem('jwt_access_token')
			}
		};
		const { schedule } = getState().schedulesManagement;

		const response = await axios.put(`${UPDATE_SCHEDULE}${schedule.id}`, scheduleData, authTOKEN);
		return response;
	}
);

export const saveSchedule = createAsyncThunk('scheduleManagement/schedule/saveSchedule', async scheduleData => {
	const authTOKEN = {
		headers: {
			'Content-type': 'application/json',
			Authorization: localStorage.getItem('jwt_access_token')
		}
	};
	const response = await axios.post(`${CREATE_SCHEDULE}`, scheduleData, authTOKEN);
	return response;
});

const scheduleSlice = createSlice({
	name: 'scheduleManagement/schedule',
	initialState: null,
	reducers: {
		resetSchedule: () => null,
		newSchedule: {
			reducer: (state, action) => action.payload,
			prepare: event => ({
				payload: {
					employee_ids: [],
					shift: '',
					start_date: '',
					end_date: ''
				}
			})
		}
	},
	extraReducers: {
		[getSchedule.fulfilled]: (state, action) => action.payload,
		[saveSchedule.fulfilled]: (state, action) => action.payload,
		[removeSchedule.fulfilled]: (state, action) => action.payload,
		[updateSchedule.fulfilled]: (state, action) => action.payload
	}
});

export const { newSchedule, resetSchedule } = scheduleSlice.actions;

export default scheduleSlice.reducer;
