import { createAsyncThunk, createSlice } from '@reduxjs/toolkit';
import { GET_EMPLOYEE_BY_DEPT_ID } from 'app/constant/constants';
import axios from 'axios';

export const getEmployeeByDept = createAsyncThunk('scheduleManagement/employeeList/getEmployeeByDept', async params => {
	const authTOKEN = {
		headers: {
			'Content-type': 'application/json',
			Authorization: localStorage.getItem('jwt_access_token')
		}
	};
	const response = await axios.get(`${GET_EMPLOYEE_BY_DEPT_ID}${params}`, authTOKEN);
	const data = await response.data.employees;
	return data === undefined ? null : data;
});

const scheduleDeptIdEmployeeSlice = createSlice({
	name: 'scheduleManagement/employeeList',
	initialState: null,
	reducers: {
		resetSchedule: () => null,
		newSchedule: {
			reducer: (state, action) => action.payload,
			prepare: event => ({
				payload: {
					employee: '',
					shift: '',
					start_date: '',
					end_date: ''
				}
			})
		}
	},
	extraReducers: {
		[getEmployeeByDept.fulfilled]: (state, action) => action.payload
	}
});

export const { newSchedule, resetSchedule } = scheduleDeptIdEmployeeSlice.actions;

export default scheduleDeptIdEmployeeSlice.reducer;
