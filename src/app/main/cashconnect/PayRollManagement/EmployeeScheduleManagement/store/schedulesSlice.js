import { createAsyncThunk, createEntityAdapter, createSlice } from '@reduxjs/toolkit';
import { GET_SCHEDULES } from 'app/constant/constants';
import axios from 'axios';

export const getSchedules = createAsyncThunk('scheduleManagement/schedules/getSchedules', async pageAndSize => {
	axios.defaults.headers.common['Content-type'] = 'application/json';
	axios.defaults.headers.common.Authorization = localStorage.getItem('jwt_access_token');

	const response = axios.get(GET_SCHEDULES, { params: pageAndSize });
	const data = await response;

	sessionStorage.setItem('total_schedules_elements', data.data.total_elements);
	sessionStorage.setItem('total_schedules_pages', data.data.total_pages);
	delete axios.defaults.headers.common['Content-type'];
	delete axios.defaults.headers.common.Authorization;

	return data.data.employee_schedules;
});

const SchedulesAdapter = createEntityAdapter({});

export const { selectAll: selectSchedules, selectById: selectScheduleById } = SchedulesAdapter.getSelectors(
	state => state.schedulesManagement.schedules
);

const schedulesSlice = createSlice({
	name: 'scheduleManagement/schedules',
	initialState: SchedulesAdapter.getInitialState({
		searchText: ''
	}),
	reducers: {
		setSchedulesSearchText: {
			reducer: (state, action) => {
				state.searchText = action.payload;
			},
			prepare: event => ({ payload: event.target.value || '' })
		}
	},
	extraReducers: {
		[getSchedules.fulfilled]: SchedulesAdapter.setAll
	}
});

export const { setData, setSchedulesSearchText } = schedulesSlice.actions;
export default schedulesSlice.reducer;
