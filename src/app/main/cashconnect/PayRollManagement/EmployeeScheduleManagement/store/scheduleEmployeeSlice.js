import { createAsyncThunk, createSlice } from '@reduxjs/toolkit';
import { GET_EMPLOYEE_SCHEDULE_BY_DEPT_ID } from 'app/constant/constants';
import axios from 'axios';

export const getEmployeeSchedule = createAsyncThunk(
	'scheduleManagement/employeeSchedule/getEmployeeSchedule',
	async params => {
		const authTOKEN = {
			headers: {
				'Content-type': 'application/json',
				Authorization: localStorage.getItem('jwt_access_token')
			}
		};
		const response = await axios.get(`${GET_EMPLOYEE_SCHEDULE_BY_DEPT_ID}${params}`, authTOKEN);
		const data = await response.data.employee_schedules;
		return data === undefined ? null : data;
	}
);

const scheduleEmployeeSlice = createSlice({
	name: 'scheduleManagement/employeeSchedule',
	initialState: null,
	reducers: {
		resetSchedule: () => null,
		newSchedule: {
			reducer: (state, action) => action.payload,
			prepare: event => ({
				payload: {
					employee: '',
					shift: '',
					start_date: '',
					end_date: ''
				}
			})
		}
	},
	extraReducers: {
		[getEmployeeSchedule.fulfilled]: (state, action) => action.payload
	}
});

export const { newSchedule, resetSchedule } = scheduleEmployeeSlice.actions;

export default scheduleEmployeeSlice.reducer;
