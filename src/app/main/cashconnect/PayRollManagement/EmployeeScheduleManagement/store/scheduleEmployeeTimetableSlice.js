import { createAsyncThunk, createSlice } from '@reduxjs/toolkit';
import { GET_EMPLOYEE_TIMETABLE_BY_EMP_ID_SHIFT_ID } from 'app/constant/constants';
import axios from 'axios';

export const getEmployeeTimetalbe = createAsyncThunk(
	'scheduleManagement/employeeTimetableSchedule/getEmployeeTimetalbe',
	async params => {
		const authTOKEN = {
			headers: {
				'Content-type': 'application/json',
				Authorization: localStorage.getItem('jwt_access_token')
			}
		};
		const response = await axios.get(
			`${GET_EMPLOYEE_TIMETABLE_BY_EMP_ID_SHIFT_ID}${params.employee.id}/${params.shift.id}`,
			authTOKEN
		);
		const data = await response.data;
		return data === undefined ? null : data;
	}
);

const scheduleEmployeeTimetableSlice = createSlice({
	name: 'scheduleManagement/employeeTimetableSchedule',
	initialState: null,
	reducers: {
		resetScheduleTimePeriod: () => null,
		newSchedule: {
			reducer: (state, action) => action.payload,
			prepare: event => ({
				payload: {
					employee: '',
					shift: '',
					start_date: '',
					end_date: ''
				}
			})
		}
	},
	extraReducers: {
		[getEmployeeTimetalbe.fulfilled]: (state, action) => action.payload
	}
});

export const { newSchedule, resetScheduleTimePeriod } = scheduleEmployeeTimetableSlice.actions;

export default scheduleEmployeeTimetableSlice.reducer;
