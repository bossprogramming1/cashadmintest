import FusePageCarded from '@fuse/core/FusePageCarded';
import { useDeepCompareEffect } from '@fuse/hooks';
import { yupResolver } from '@hookform/resolvers/yup';
import { Button, Typography } from '@material-ui/core';
import withReducer from 'app/store/withReducer';
import { motion } from 'framer-motion';
import React, { useEffect, useState } from 'react';
import { FormProvider, useForm } from 'react-hook-form';
import { useDispatch, useSelector } from 'react-redux';
import { Link, useParams } from 'react-router-dom';
import * as yup from 'yup';
import reducer from '../store/index';
import { getUnit, newUnit, resetUnit } from '../store/unitSlice';
import NewUnitHeader from './NewUnitHeader';
import UnitForm from './UnitForm';

/**
 * Form Validation Schema
 */
const schema = yup.object().shape({
	type: yup.string().required('Type is required'),
	symbol: yup.string().required('Symble is required')
});

const Unit = () => {
	const dispatch = useDispatch();
	const unit = useSelector(({ unitsManagement }) => unitsManagement.unit);

	const [noUnit, setNoUnit] = useState(false);
	const methods = useForm({
		mode: 'onChange',
		defaultValues: {},
		resolver: yupResolver(schema)
	});
	const routeParams = useParams();

	const { reset } = methods;

	useDeepCompareEffect(() => {
		function updateUnitState() {
			const { unitId } = routeParams;

			if (unitId === 'new') {
				localStorage.removeItem('event');
				/**
				 * Create New User data
				 */
				dispatch(newUnit());
			} else {
				/**
				 * Get User data
				 */

				dispatch(getUnit(unitId)).then(action => {
					/**
					 * If the requested product is not exist show message
					 */
					if (!action.payload) {
						setNoUnit(true);
					}
				});
			}
		}

		updateUnitState();
	}, [dispatch, routeParams]);

	useEffect(() => {}, []);

	useEffect(() => {
		if (!unit) {
			return;
		}
		/**
		 * Reset the form on unit state changes
		 */
		reset(unit);
	}, [unit, reset]);

	useEffect(() => {
		return () => {
			/**
			 * Reset Unit on component unload
			 */
			dispatch(resetUnit());
			setNoUnit(false);
		};
	}, [dispatch]);

	/**
	 * Show Message if the requested products is not exists
	 */
	if (noUnit) {
		return (
			<motion.div
				initial={{ opacity: 0 }}
				animate={{ opacity: 1, transition: { delay: 0.1 } }}
				className="flex flex-col flex-1 items-center justify-center h-full"
			>
				<Typography color="textSecondary" variant="h5">
					There is no such unit!
				</Typography>
				<Button
					className="mt-24"
					component={Link}
					variant="outlined"
					to="/apps/e-commerce/products"
					color="inherit"
				>
					Go to Unit Page
				</Button>
			</motion.div>
		);
	}

	return (
		<FormProvider {...methods}>
			<FusePageCarded
				classes={{
					toolbar: 'p-0',
					header: 'min-h-80 h-80'
				}}
				header={<NewUnitHeader />}
				content={
					<div className="p-16 sm:p-24">
						<UnitForm />
					</div>
				}
				innerScroll
			/>
		</FormProvider>
	);
};
export default withReducer('unitsManagement', reducer)(Unit);
