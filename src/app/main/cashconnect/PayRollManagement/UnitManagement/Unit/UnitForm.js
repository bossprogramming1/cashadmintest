import _ from '@lodash';
import { makeStyles } from '@material-ui/core/styles';
import TextField from '@material-ui/core/TextField';
import { Autocomplete } from '@material-ui/lab';
import React, { useEffect } from 'react';
import { Controller, useFormContext } from 'react-hook-form';
import { useDispatch, useSelector } from 'react-redux';
import { useHistory, useParams } from 'react-router-dom';
// import { getCities } from '../../../../store/dataSlice';
import { compound } from 'app/@data/data';
import { saveUnit, updateUnit } from '../store/unitSlice';

const useStyles = makeStyles(theme => ({
	hidden: {
		display: 'none'
	}
}));

function UnitForm(props) {
	const userID = localStorage.getItem('user_id');

	const citys = useSelector(state => state.data.cities);

	const classes = useStyles(props);

	const methods = useFormContext();
	const { control, formState, getValues } = methods;
	const { errors, isValid, dirtyFields } = formState;
	const routeParams = useParams();
	const { unitId } = routeParams;
	const history = useHistory();
	const handleDelete = localStorage.getItem('unitEvent');
	const dispatch = useDispatch();

	function handleSaveUnit() {
		dispatch(saveUnit(getValues())).then(res => {
			if (res.payload) {
				localStorage.setItem('unitAlert', 'saveUnit');
				history.push('/apps/unit-management/units');
			}
		});
	}

	function handleUpdateUnit() {
		dispatch(updateUnit(getValues())).then(res => {
			if (res.payload) {
				localStorage.setItem('unitAlert', 'updateUnit');
				history.push('/apps/unit-management/units');
			}
		});
	}

	const handleSubmitOnKeyDownEnter = ev => {
		if (ev.key === 'Enter') {
			if (routeParams.unitId === 'new' && !(_.isEmpty(dirtyFields) || !isValid)) {
				handleSaveUnit();
			} else if (handleDelete !== 'Delete' && routeParams?.unitName) {
				handleUpdateUnit();
			}
		}
	};

	return (
		<div>
			<Controller
				name="type"
				control={control}
				render={({ field: { onChange, value, name } }) => (
					<Autocomplete
						className="mt-8 mb-16"
						freeSolo
						value={value ? compound.find(data => data.name === value) : null}
						options={compound}
						getOptionLabel={option => `${option?.name}`}
						onChange={(event, newValue) => {
							onChange(newValue?.name ? newValue?.name : '');
						}}
						renderInput={params => (
							<TextField
								{...params}
								placeholder="Select Type"
								label="Unit Type"
								error={!!errors.type}
								required
								helperText={errors?.type?.message}
								variant="outlined"
								autoFocus
								InputLabelProps={{
									shrink: true
								}}
								// onKeyDown={handleSubmitOnKeyDownEnter}
							/>
						)}
					/>
				)}
			/>
			<Controller
				name="symbol"
				control={control}
				render={({ field }) => {
					return (
						<TextField
							{...field}
							className="mt-8 mb-16"
							error={!!errors?.symbol}
							helperText={errors?.symbol?.message}
							label="Symbol"
							id="symbol"
							required
							variant="outlined"
							InputLabelProps={field.value && { shrink: true }}
							fullWidth
							onKeyDown={handleSubmitOnKeyDownEnter}
						/>
					);
				}}
			/>
			<Controller
				name="symbol_value"
				control={control}
				render={({ field }) => {
					return (
						<TextField
							{...field}
							className="mt-8 mb-16"
							error={!!errors?.symbol_value}
							helperText={errors?.symbol_value?.message}
							label="Symbol value"
							id="symbol_value"
							required
							variant="outlined"
							InputLabelProps={field.value && { shrink: true }}
							fullWidth
							onKeyDown={handleSubmitOnKeyDownEnter}
						/>
					);
				}}
			/>
			<Controller
				name="formal_name"
				control={control}
				render={({ field }) => {
					return (
						<TextField
							{...field}
							className="mt-8 mb-16"
							error={!!errors?.formal_name}
							helperText={errors?.formal_name?.message}
							label="Formal Name"
							id="formal_name"
							required
							variant="outlined"
							InputLabelProps={field.value && { shrink: true }}
							fullWidth
							onKeyDown={handleSubmitOnKeyDownEnter}
						/>
					);
				}}
			/>
			<Controller
				name="decimal_places"
				control={control}
				render={({ field }) => {
					return (
						<TextField
							{...field}
							className="mt-8 mb-16"
							error={!!errors?.decimal_places}
							helperText={errors?.decimal_places?.message}
							label="No of Decimal Places"
							id="formal_name"
							required
							variant="outlined"
							InputLabelProps={field.value && { shrink: true }}
							fullWidth
							onKeyDown={handleSubmitOnKeyDownEnter}
						/>
					);
				}}
			/>

			{/* <Controller
				name="city"
				control={control}
				render={({ field: { onChange, value, name } }) => (
					<Autocomplete
						className="mt-8 mb-16"
						freeSolo
						value={value ? citys.find(data => data.id === value) : null}
						options={citys}
						getOptionLabel={option => `${option?.name}`}
						onChange={(event, newValue) => {
							onChange(newValue?.id);
						}}
						renderInput={params => (
							<TextField
								{...params}
								placeholder="Select City"
								label="City"
								error={!!errors.city}
								required
								helperText={errors?.city?.message}
								variant="outlined"
								autoFocus
								InputLabelProps={{
									shrink: true
								}}
								// onKeyDown={handleSubmitOnKeyDownEnter}
							/>
						)}
					/>
				)}
			/> */}
		</div>
	);
}

export default UnitForm;
