import { combineReducers } from '@reduxjs/toolkit';
import unit from './unitSlice';
import units from './unitsSlice';

const reducer = combineReducers({
	unit,
	units
});

export default reducer;
