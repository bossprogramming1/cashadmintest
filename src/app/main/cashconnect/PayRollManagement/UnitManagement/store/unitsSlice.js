import { createAsyncThunk, createEntityAdapter, createSlice } from '@reduxjs/toolkit';
import { DELETE_UNIT, GET_UNITS } from 'app/constant/constants';
import axios from 'axios';

export const getUnits = createAsyncThunk('unitManagement/units/getUnits', async parameter => {
	const { page, size } = parameter;
	axios.defaults.headers.common['Content-type'] = 'application/json';
	axios.defaults.headers.common.Authorization = localStorage.getItem('jwt_access_token');

	const response = axios.get(GET_UNITS, { params: { page, size } });
	const data = await response;

	sessionStorage.setItem('units_total_elements', data.data.total_elements);
	sessionStorage.setItem('units_total_pages', data.data.total_pages);
	delete axios.defaults.headers.common['Content-type'];
	delete axios.defaults.headers.common.Authorization;

	return data.data.units;
});

export const removeUnits = createAsyncThunk(
	'unitManagement/units/removeUnits',
	async (unitIds, { dispatch, getState }) => {
		const authTOKEN = {
			headers: {
				'Content-type': 'application/json',
				Authorization: localStorage.getItem('jwt_access_token')
			}
		};
		await axios.delete(`${DELETE_UNIT}`, { unitIds }, authTOKEN);

		return unitIds;
	}
);

const unitsAdapter = createEntityAdapter({});

export const { selectAll: selectUnits, selectById: selectUnitById } = unitsAdapter.getSelectors(
	state => state.unitsManagement.units
);

const unitsSlice = createSlice({
	name: 'unitManagement/units',
	initialState: unitsAdapter.getInitialState({
		searchText: ''
	}),
	reducers: {
		setUnitsSearchText: {
			reducer: (state, action) => {
				state.searchText = action.payload;
			},
			prepare: event => ({ payload: event.target.value || '' })
		}
	},
	extraReducers: {
		[getUnits.fulfilled]: unitsAdapter.setAll
	}
});

export const { setData, setUnitsSearchText } = unitsSlice.actions;
export default unitsSlice.reducer;
