import { createAsyncThunk, createSlice } from '@reduxjs/toolkit';
import { CREATE_UNIT, DELETE_UNIT, GET_UNITID, UPDATE_UNIT } from 'app/constant/constants';
import axios from 'axios';

export const getUnit = createAsyncThunk('unitManagement/unit/getUnit', async (params, { rejectWithValue }) => {
	const authTOKEN = {
		headers: {
			'Content-type': 'application/json',
			Authorization: localStorage.getItem('jwt_access_token')
		}
	};

	try {
		const response = await axios.get(`${GET_UNITID}${params}`, authTOKEN);
		const data = await response.data;
		return data === undefined ? null : data;
	} catch (err) {
		return rejectWithValue(params);
	}
});

export const removeUnit = createAsyncThunk('unitManagement/unit/removeUnit', async val => {
	const authTOKEN = {
		headers: {
			'Content-type': 'application/json',
			Authorization: localStorage.getItem('jwt_access_token')
		}
	};

	const unitId = val.id;
	const response = await axios.delete(`${DELETE_UNIT}${unitId}`, authTOKEN);
	return response;
});

export const updateUnit = createAsyncThunk(
	'unitManagement/unit/updateUnit',
	async (unitData, { dispatch, getState }) => {
		const { unit } = getState().unitsManagement;

		const authTOKEN = {
			headers: {
				'Content-type': 'application/json',
				Authorization: localStorage.getItem('jwt_access_token')
			}
		};
		const response = await axios.put(`${UPDATE_UNIT}${unit.id}`, unitData, authTOKEN);
		return response;
	}
);

export const saveUnit = createAsyncThunk('unitManagement/unit/saveUnit', async unitData => {
	const authTOKEN = {
		headers: {
			'Content-type': 'application/json',
			Authorization: localStorage.getItem('jwt_access_token')
		}
	};
	const response = await axios.post(`${CREATE_UNIT}`, unitData, authTOKEN);
	return response;
});

const unitSlice = createSlice({
	name: 'unitManagement/unit',
	initialState: null,
	reducers: {
		resetUnit: () => null,
		newUnit: {
			reducer: (state, action) => action.payload,
			prepare: event => ({
				payload: {
					type: 'Compound',
					symbol: '',
					formal_name: '',
					symbol_value: 0,
					decimal_places: 0
				}
			})
		}
	},
	extraReducers: {
		[getUnit.fulfilled]: (state, action) => action.payload,
		[saveUnit.fulfilled]: (state, action) => action.payload,
		[removeUnit.fulfilled]: (state, action) => action.payload,
		[updateUnit.fulfilled]: (state, action) => action.payload
	}
});

export const { newUnit, resetUnit } = unitSlice.actions;

export default unitSlice.reducer;
