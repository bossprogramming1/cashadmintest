import { createAsyncThunk, createSlice } from '@reduxjs/toolkit';
import {
	CREATE_PAYHEAD_TYPE,
	DELETE_PAYHEAD_TYPE,
	GET_PAYHEAD_TYPEID,
	UPDATE_PAYHEAD_TYPE
} from 'app/constant/constants';
import axios from 'axios';

export const getPayheadtype = createAsyncThunk(
	'payheadtypeManagement/payheadtype/getPayheadtype',
	async (params, { rejectWithValue }) => {
		const authTOKEN = {
			headers: {
				'Content-type': 'application/json',
				Authorization: localStorage.getItem('jwt_access_token')
			}
		};

		try {
			const response = await axios.get(`${GET_PAYHEAD_TYPEID}${params}`, authTOKEN);
			const data = await response.data;
			return data === undefined ? null : data;
		} catch (err) {
			return rejectWithValue(params);
		}
	}
);

export const removePayheadtype = createAsyncThunk('payheadtypeManagement/payheadtype/removePayheadtype', async val => {
	const authTOKEN = {
		headers: {
			'Content-type': 'application/json',
			Authorization: localStorage.getItem('jwt_access_token')
		}
	};
	const payheadtypeId = val.id;
	const response = await axios.delete(`${DELETE_PAYHEAD_TYPE}${payheadtypeId}`, authTOKEN);
	return response;
});

export const updatePayheadtype = createAsyncThunk(
	'payheadtypeManagement/payheadtype/updatePayheadtype',
	async (payheadtypeData, { dispatch, getState }) => {
		const { payheadtype } = getState().payheadtypesManagement;

		const authTOKEN = {
			headers: {
				'Content-type': 'application/json',
				Authorization: localStorage.getItem('jwt_access_token')
			}
		};
		const response = await axios.put(`${UPDATE_PAYHEAD_TYPE}${payheadtype.id}`, payheadtypeData, authTOKEN);
		return response;
	}
);

export const savePayheadtype = createAsyncThunk(
	'payheadtypeManagement/payheadtype/savePayheadtype',
	async payheadtypeData => {
		const authTOKEN = {
			headers: {
				'Content-type': 'application/json',
				Authorization: localStorage.getItem('jwt_access_token')
			}
		};
		const response = await axios.post(`${CREATE_PAYHEAD_TYPE}`, payheadtypeData, authTOKEN);
		return response;
	}
);

const payheadtypeSlice = createSlice({
	name: 'payheadtypeManagement/payheadtype',
	initialState: null,
	reducers: {
		resetPayheadtype: () => null,
		newPayheadtype: {
			reducer: (state, action) => action.payload,
			prepare: event => ({
				payload: {
					name: ''
				}
			})
		}
	},
	extraReducers: {
		[getPayheadtype.fulfilled]: (state, action) => action.payload,
		[savePayheadtype.fulfilled]: (state, action) => action.payload,
		[removePayheadtype.fulfilled]: (state, action) => action.payload,
		[updatePayheadtype.fulfilled]: (state, action) => action.payload
	}
});

export const { newPayheadtype, resetPayheadtype } = payheadtypeSlice.actions;

export default payheadtypeSlice.reducer;
