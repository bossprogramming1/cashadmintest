import { createAsyncThunk, createEntityAdapter, createSlice } from '@reduxjs/toolkit';
import { DELETE_PAYHEAD_TYPE, GET_PAYHEAD_TYPES } from 'app/constant/constants';
import axios from 'axios';

export const getPayheadtypes = createAsyncThunk(
	'payheadtypeManagement/payheadtypes/getPayheadtypes',
	async parameter => {
		const { page, size } = parameter;
		axios.defaults.headers.common['Content-type'] = 'application/json';
		axios.defaults.headers.common.Authorization = localStorage.getItem('jwt_access_token');

		const response = axios.get(GET_PAYHEAD_TYPES, { params: { page, size } });
		const data = await response;

		sessionStorage.setItem('payheadtypes_total_elements', data.data.total_elements);
		sessionStorage.setItem('payheadtypes_total_pages', data.data.total_pages);
		delete axios.defaults.headers.common['Content-type'];
		delete axios.defaults.headers.common.Authorization;

		return data.data.payhead_types;
	}
);

export const removePayheadtypes = createAsyncThunk(
	'payheadtypeManagement/payheadtypes/removePayheadtypes',
	async (payheadtypeIds, { dispatch, getState }) => {
		const authTOKEN = {
			headers: {
				'Content-type': 'application/json',
				Authorization: localStorage.getItem('jwt_access_token')
			}
		};
		await axios.delete(`${DELETE_PAYHEAD_TYPE}`, { payheadtypeIds }, authTOKEN);

		return payheadtypeIds;
	}
);

const payheadtypesAdapter = createEntityAdapter({});

export const { selectAll: selectPayheadtypes, selectById: selectPayheadtypeById } = payheadtypesAdapter.getSelectors(
	state => state.payheadtypesManagement.payheadtypes
);

const payheadtypesSlice = createSlice({
	name: 'payheadtypeManagement/payheadtypes',
	initialState: payheadtypesAdapter.getInitialState({
		searchText: ''
	}),
	reducers: {
		setPayheadtypesSearchText: {
			reducer: (state, action) => {
				state.searchText = action.payload;
			},
			prepare: event => ({ payload: event.target.value || '' })
		}
	},
	extraReducers: {
		[getPayheadtypes.fulfilled]: payheadtypesAdapter.setAll
	}
});

export const { setData, setPayheadtypesSearchText } = payheadtypesSlice.actions;
export default payheadtypesSlice.reducer;
