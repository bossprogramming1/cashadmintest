import { combineReducers } from '@reduxjs/toolkit';
import payheadtype from './payheadtypeSlice';
import payheadtypes from './payheadtypesSlice';

const reducer = combineReducers({
	payheadtype,
	payheadtypes
});

export default reducer;
