import _ from '@lodash';
import { makeStyles } from '@material-ui/core/styles';
import TextField from '@material-ui/core/TextField';
import { Autocomplete } from '@material-ui/lab';
import React, { useEffect } from 'react';
import { Controller, useFormContext } from 'react-hook-form';
import { useDispatch, useSelector } from 'react-redux';
import { useHistory, useParams } from 'react-router-dom';
// import { getCities } from '../../../../store/dataSlice';
import { savePayheadtype, updatePayheadtype } from '../store/payheadtypeSlice';

const useStyles = makeStyles(theme => ({
	hidden: {
		display: 'none'
	}
}));

function PayheadtypeForm(props) {
	const userID = localStorage.getItem('user_id');

	const classes = useStyles(props);

	const methods = useFormContext();
	const { control, formState, getValues } = methods;
	const { errors, isValid, dirtyFields } = formState;
	const routeParams = useParams();
	const { payheadtypeId } = routeParams;
	const history = useHistory();
	const handleDelete = localStorage.getItem('payheadtypeEvent');
	const dispatch = useDispatch();

	
	function handleSavePayheadtype() {
		dispatch(savePayheadtype(getValues())).then(res => {
			if (res.payload) {
				localStorage.setItem('payheadtypeAlert', 'savePayheadtype');
				history.push('/apps/payheadtype-management/payheadtypes');
			}
		});
	}

	function handleUpdatePayheadtype() {
		dispatch(updatePayheadtype(getValues())).then(res => {
			if (res.payload) {
				localStorage.setItem('payheadtypeAlert', 'updatePayheadtype');
				history.push('/apps/payheadtype-management/payheadtypes');
			}
		});
	}

	const handleSubmitOnKeyDownEnter = ev => {
		if (ev.key === 'Enter') {
			if (routeParams.payheadtypeId === 'new' && !(_.isEmpty(dirtyFields) || !isValid)) {
				handleSavePayheadtype();
			} else if (handleDelete !== 'Delete' && routeParams?.payheadtypeName) {
				handleUpdatePayheadtype();
			}
		}
	};

	return (
		<div>
			<Controller
				name="name"
				control={control}
				render={({ field }) => {
					return (
						<TextField
							{...field}
							className="mt-8 mb-16"
							error={!!errors?.name}
							helperText={errors?.name?.message}
							label="Payheadtype Name"
							id="name"
							required
							variant="outlined"
							InputLabelProps={field.value && { shrink: true }}
							fullWidth
							onKeyDown={handleSubmitOnKeyDownEnter}
						/>
					);
				}}
			/>
		</div>
	);
}

export default PayheadtypeForm;
