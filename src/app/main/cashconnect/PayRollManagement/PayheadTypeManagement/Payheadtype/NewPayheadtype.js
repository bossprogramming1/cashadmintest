import FusePageCarded from '@fuse/core/FusePageCarded';
import { useDeepCompareEffect } from '@fuse/hooks';
import { yupResolver } from '@hookform/resolvers/yup';
import { Button, Typography } from '@material-ui/core';
import withReducer from 'app/store/withReducer';
import { motion } from 'framer-motion';
import React, { useEffect, useState } from 'react';
import { FormProvider, useForm } from 'react-hook-form';
import { useDispatch, useSelector } from 'react-redux';
import { Link, useParams } from 'react-router-dom';
import * as yup from 'yup';
import reducer from '../store/index';
import { getPayheadtype, newPayheadtype, resetPayheadtype } from '../store/payheadtypeSlice';
import NewPayheadtypeHeader from './NewPayheadtypeHeader';
import PayheadtypeForm from './PayheadtypeForm';

/**
 * Form Validation Schema
 */
const schema = yup.object().shape({
	name: yup.string().required('Name is required')
});

const Payheadtype = () => {
	const dispatch = useDispatch();
	const payheadtype = useSelector(({ payheadtypesManagement }) => payheadtypesManagement.payheadtype);

	const [noPayheadtype, setNoPayheadtype] = useState(false);
	const methods = useForm({
		mode: 'onChange',
		defaultValues: {},
		resolver: yupResolver(schema)
	});
	const routeParams = useParams();

	const { reset } = methods;

	useDeepCompareEffect(() => {
		function updatePayheadtypeState() {
			const { payheadtypeId } = routeParams;

			if (payheadtypeId === 'new') {
				localStorage.removeItem('event');
				/**
				 * Create New User data
				 */
				dispatch(newPayheadtype());
			} else {
				/**
				 * Get User data
				 */

				dispatch(getPayheadtype(payheadtypeId)).then(action => {
					/**
					 * If the requested product is not exist show message
					 */
					if (!action.payload) {
						setNoPayheadtype(true);
					}
				});
			}
		}

		updatePayheadtypeState();
	}, [dispatch, routeParams]);

	useEffect(() => {}, []);

	useEffect(() => {
		if (!payheadtype) {
			return;
		}
		/**
		 * Reset the form on payheadtype state changes
		 */
		reset(payheadtype);
	}, [payheadtype, reset]);

	useEffect(() => {
		return () => {
			/**
			 * Reset Payheadtype on component unload
			 */
			dispatch(resetPayheadtype());
			setNoPayheadtype(false);
		};
	}, [dispatch]);

	/**
	 * Show Message if the requested products is not exists
	 */
	if (noPayheadtype) {
		return (
			<motion.div
				initial={{ opacity: 0 }}
				animate={{ opacity: 1, transition: { delay: 0.1 } }}
				className="flex flex-col flex-1 items-center justify-center h-full"
			>
				<Typography color="textSecondary" variant="h5">
					There is no such payheadtype!
				</Typography>
				<Button
					className="mt-24"
					component={Link}
					variant="outlined"
					to="/apps/e-commerce/products"
					color="inherit"
				>
					Go to Payheadtype Page
				</Button>
			</motion.div>
		);
	}

	return (
		<FormProvider {...methods}>
			<FusePageCarded
				classes={{
					toolbar: 'p-0',
					header: 'min-h-80 h-80'
				}}
				header={<NewPayheadtypeHeader />}
				content={
					<div className="p-16 sm:p-24">
						<PayheadtypeForm />
					</div>
				}
				innerScroll
			/>
		</FormProvider>
	);
};
export default withReducer('payheadtypesManagement', reducer)(Payheadtype);
