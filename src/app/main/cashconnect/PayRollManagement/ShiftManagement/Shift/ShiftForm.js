import _ from '@lodash';
import { Checkbox, FormControl, FormControlLabel } from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';
import TextField from '@material-ui/core/TextField';
import { Autocomplete } from '@material-ui/lab';
import { TimePicker } from '@material-ui/pickers';
import CustomDatePicker from 'app/@components/CustomDatePicker';
import { getShifts } from 'app/store/dataSlice';
import moment from 'moment';
import React, { useEffect } from 'react';
import { Controller, useFormContext } from 'react-hook-form';
import InputColor from 'react-input-color';
import { useDispatch, useSelector } from 'react-redux';
import { useHistory, useParams } from 'react-router-dom';
import { saveShift, updateShift } from '../store/shiftSlice';

const useStyles = makeStyles(theme => ({
	hidden: {
		display: 'none'
	}
}));

function ShiftForm(props) {
	const userID = localStorage.getItem('user_id');

	const shift = useSelector(state => state.data.shift);
	const [color, setColor] = React.useState({});

	const classes = useStyles(props);

	const methods = useFormContext();
	const { control, formState, setValue, getValues } = methods;
	const { errors, isValid, dirtyFields } = formState;
	const routeParams = useParams();
	const { shiftId } = routeParams;
	const history = useHistory();
	const handleDelete = localStorage.getItem('shiftEvent');
	const dispatch = useDispatch();

	useEffect(() => {
		dispatch(getShifts());
	}, []);

	//set color coder
	useEffect(() => {
		setValue(`color`, color.hex || '#0032faff');
	}, [color.hex]);

	function handleSaveShift() {
		dispatch(saveShift(getValues())).then(res => {
			if (res.payload) {
				localStorage.setItem('shiftAlert', 'saveShift');
				history.push('/apps/shifts-management');
			}
		});
	}

	function handleUpdateShift() {
		dispatch(updateShift(getValues())).then(res => {
			if (res.payload) {
				localStorage.setItem('shiftAlert', 'updateShift');
				history.push('/apps/shifts-management');
			}
		});
	}

	const handleSubmitOnKeyDownEnter = ev => {
		if (ev.key === 'Enter') {
			if (routeParams.shiftId === 'new' && !(_.isEmpty(dirtyFields) || !isValid)) {
				handleSaveShift();
			} else if (handleDelete !== 'Delete' && routeParams?.shiftName) {
				handleUpdateShift();
			}
		}
	};

	return (
		<div>
			<Controller
				name="name"
				control={control}
				render={({ field }) => {
					return (
						<TextField
							{...field}
							className="mt-8 mb-16"
							error={!!errors?.name}
							helperText={errors?.name?.message}
							label="Name"
							id="name"
							required
							variant="outlined"
							InputLabelProps={field.value && { shrink: true }}
							fullWidth
							onKeyDown={handleSubmitOnKeyDownEnter}
						/>
					);
				}}
			/>
			<div style={{ display: 'grid', gridTemplateColumns: 'repeat(2, 1fr)', columnGap: '9px' }}>
				<Controller
					name="start_date"
					control={control}
					render={({ field }) => <CustomDatePicker field={field} label="Start Date" />}
				/>
				<Controller
					name="end_date"
					control={control}
					render={({ field }) => <CustomDatePicker field={field} label="End Date" />}
				/>
			</div>
		</div>
	);
}

export default ShiftForm;
