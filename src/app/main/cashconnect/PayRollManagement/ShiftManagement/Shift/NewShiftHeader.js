import Button from '@material-ui/core/Button';
import Icon from '@material-ui/core/Icon';
import { useTheme } from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';
import { motion } from 'framer-motion';
import _ from 'lodash';
import moment from 'moment';
import React from 'react';
import { useFormContext } from 'react-hook-form';
import { useDispatch } from 'react-redux';
import { Link, useHistory, useParams } from 'react-router-dom';
import { removeShift, saveShift, updateShift } from '../store/shiftSlice';

const NewShiftHeader = () => {
	const dispatch = useDispatch();
	const methods = useFormContext();
	const { getValues, watch, setError } = methods;
	const name = watch('name');
	const theme = useTheme();
	const history = useHistory();
	const routeParams = useParams();
	const handleDelete = localStorage.getItem('deleteShift');
	const handleUpdate = localStorage.getItem('updateShift');

	function handleSaveShift() {
		
		dispatch(saveShift(getValues())).then(res => {
			if (res.payload.data?.id) {
				localStorage.setItem('shiftAlertPermission', 'saveShiftSuccessfully');
				history.push('/apps/shifts-management');
			} else if (res.payload.data?.detail) {
				setError('name', { type: 'manual', message: res.payload.data.detail });
			}
		});
	}

	function handleUpdateShift() {
		dispatch(updateShift(getValues())).then(res => {
			if (res.payload.data?.id) {
				localStorage.setItem('shiftAlert', 'updateShift');
				history.push('/apps/shifts-management');
			} else if (res.payload.data?.detail) {
				setError('name', { type: 'manual', message: res.payload.data.detail });
			}
		});
	}

	function handleRemoveShift() {
		dispatch(removeShift(getValues())).then(res => {
			if (res.payload) {
				localStorage.removeItem('shiftEvent');
				localStorage.setItem('shiftAlert', 'deleteShift');
				history.push('/apps/shifts-management');
			}
		});
	}

	function handleCancel() {
		history.push('/apps/shifts-management');
	}



	return (
		<div className="flex flex-1 w-full items-center justify-between">
			<div className="flex flex-col items-start max-w-full min-w-0">
				<motion.div initial={{ x: 20, opacity: 0 }} animate={{ x: 0, opacity: 1, transition: { delay: 0.3 } }}>
					<Typography
						className="flex items-center sm:mb-2"
						component={Link}
						shift="button"
						to="/apps/shifts-management"
						color="inherit"
					>
						<Icon className="text-20">{theme.direction === 'ltr' ? 'arrow_back' : 'arrow_forward'}</Icon>
						<span className="hidden sm:flex mx-4 font-medium">Shifts</span>
					</Typography>
				</motion.div>

				<div className="flex items-center max-w-full">
					<motion.div
						className="hidden sm:flex"
						initial={{ scale: 0 }}
						animate={{ scale: 1, transition: { delay: 0.3 } }}
					></motion.div>
					<div className="flex flex-col min-w-0 mx-8 sm:mc-16">
						<motion.div initial={{ x: -20 }} animate={{ x: 0, transition: { delay: 0.3 } }}>
							<Typography className="text-16 sm:text-20 truncate font-semibold">
								{name || 'Create New Shift'}
							</Typography>
							<Typography variant="caption" className="font-medium">
								Shifts Detail
							</Typography>
						</motion.div>
					</div>
				</div>
			</div>
			<motion.div
				className="flex"
				initial={{ opacity: 0, x: 20 }}
				animate={{ opacity: 1, x: 0, transition: { delay: 0.3 } }}
			>
				{handleDelete === 'deleteShift' && (
					<Typography className="mt-6" variant="subtitle2">
						Do you want to remove this Shifts?
					</Typography>
				)}
				{handleDelete === 'deleteShift' && (
					<Button
						className="whitespace-nowrap mx-4"
						variant="contained"
						color="secondary"
						onClick={handleRemoveShift}
						startIcon={<Icon className="hidden sm:flex">delete</Icon>}
						style={{ backgroundColor: '#ea5b78', color: 'white' }}
					>
						Remove
					</Button>
				)}
				{handleDelete !== 'deleteShift' && routeParams.shiftId === 'new' && (
					<Button
						className="whitespace-nowrap mx-4"
						variant="contained"
						color="secondary"
						disabled={!name || _.isEmpty(name)}
						onClick={handleSaveShift}
					>
						Save
					</Button>
				)}
				{handleDelete !== 'deleteShift' && handleUpdate === 'updateShift' && (
					<Button
						className="whitespace-nowrap mx-4"
						color="secondary"
						variant="contained"
						style={{ backgroundColor: '#4dc08e', color: 'white' }}
						onClick={handleUpdateShift}
					>
						Update
					</Button>
				)}
				<Button
					className="whitespace-nowrap mx-4"
					variant="contained"
					style={{ backgroundColor: '#FFAA4C', color: 'white' }}
					onClick={handleCancel}
				>
					Cancel
				</Button>
			</motion.div>
		</div>
	);
};

export default NewShiftHeader;
