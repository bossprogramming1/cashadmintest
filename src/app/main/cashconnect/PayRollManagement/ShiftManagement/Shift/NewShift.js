import FusePageCarded from '@fuse/core/FusePageCarded';
import { useDeepCompareEffect } from '@fuse/hooks';
import { yupResolver } from '@hookform/resolvers/yup';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';
import withReducer from 'app/store/withReducer';
import { motion } from 'framer-motion';
import React, { useEffect, useState } from 'react';
import { FormProvider, useForm } from 'react-hook-form';
import { useDispatch, useSelector } from 'react-redux';
import { Link, useParams } from 'react-router-dom';
import * as yup from 'yup';
import reducer from '../store/index';
import { getShift, newShift, resetShift } from '../store/shiftSlice';
import NewShiftHeader from './NewShiftHeader';
import ShiftForm from './ShiftForm';

/**
 * Form Validation Schema
 */
const schema = yup.object().shape({
	name: yup.string().required('Name is required')
});

const NewShift = () => {
	const dispatch = useDispatch();
	const shift = useSelector(({ shiftsManagement }) => shiftsManagement.shift);
	const routeParams = useParams();
	const [noShift, setNoShift] = useState(false);
	const methods = useForm({
		mode: 'onChange',
		defaultValues: {},
		resolver: yupResolver(schema)
	});
	const { reset } = methods;

	useDeepCompareEffect(() => {
		function updateShiftState() {
			const { shiftId } = routeParams;
			if (shiftId === 'new') {
				localStorage.removeItem('deleteShift');
				localStorage.removeItem('updateShift');
				/**
				 * Create New User data
				 */
				dispatch(newShift());
			} else {
				/**
				 * Get User data
				 */
				dispatch(getShift(routeParams)).then(action => {
					/**
					 * If the requested shift is not exist show message
					 */
					if (!action.payload) {
						setNoShift(true);
					}
				});
			}
		}

		updateShiftState();
	}, [dispatch, routeParams]);

	useEffect(() => {
		if (!shift) {
			return;
		}
		/**
		 * Reset the form on shift state changes
		 */
		reset(shift);
	}, [shift, reset]);

	useEffect(() => {
		return () => {
			/**
			 * Reset Shift on component unload
			 */
			dispatch(resetShift());
			setNoShift(false);
		};
	}, [dispatch]);

	/**
	 * Show Message if the requested Shifts is not exists
	 */
	if (noShift) {
		return (
			<motion.div
				initial={{ opacity: 0 }}
				animate={{ opacity: 1, transition: { delay: 0.1 } }}
				className="flex flex-col flex-1 items-center justify-center h-full"
			>
				<Typography color="textSecondary" variant="h5">
					There is no such shift!
				</Typography>
				<Button
					className="mt-24"
					component={Link}
					variant="outlined"
					to="/apps/shifts-management"
					color="inherit"
				>
					Go to Shift Page
				</Button>
			</motion.div>
		);
	}

	/**
	 * Wait while product data is loading and form is setted
	 */
	return (
		<FormProvider {...methods}>
			<FusePageCarded
				classes={{
					toolbar: 'p-0',
					header: 'min-h-80 h-80'
				}}
				header={<NewShiftHeader />}
				content={
					<div className="p-16 sm:p-24">
						<ShiftForm />
					</div>
				}
				innerScroll
			/>
		</FormProvider>
	);
};
export default withReducer('shiftsManagement', reducer)(NewShift);
