import FusePageCarded from '@fuse/core/FusePageCarded';
import withReducer from 'app/store/withReducer';
import React from 'react';
import { useDispatch } from 'react-redux';
import reducer from '../store/index';
import ShiftsHeader from './ShiftsHeader';
import ShiftsTable from './ShiftsTable';

const Shifts = () => {
	return (
		<FusePageCarded
			classes={{
				content: 'flex',
				contentCard: 'overflow-hidden',
				header: 'min-h-74 h-64'
			}}
			header={<ShiftsHeader />}
			content={<ShiftsTable />}
			innerScroll
		/>
	);
};
export default withReducer('shiftsManagement', reducer)(Shifts);
