import Paper from '@material-ui/core/Paper';
import Typography from '@material-ui/core/Typography';
import _ from '@lodash';
import { memo } from 'react';
import { Box, Button, Table, TableBody, TableCell, TableHead, TableRow, TableSortLabel, Tooltip } from '@mui/material';
import { makeStyles } from '@material-ui/core/styles';
import { useSelector } from 'react-redux';
// import WeekCalendar from 'react-week-calendar';

const useStyles = makeStyles(theme => ({
	tablecell: {
		fontSize: '50px'
	}
}));

const weeks = [
	{ day: 'Saturday' },
	{ day: 'Sunday' },
	{ day: 'Monday' },
	{ day: 'Tuesday' },
	{ day: 'Wednesday' },
	{ day: 'Thursday' },
	{ day: 'Friday' }
];

function WeekTable() {
	const timetable = useSelector(({ shiftsManagement }) => shiftsManagement?.shift?.daytime);
	const data = [];
	data.push(timetable);

	return (
		<Paper className="w-full rounded-40 shadow" style={{ backgroundColor: '#e7eaed' }}>
			{/* sx={{ minWidth: 800 }} */}
			<div className="flex items-center justify-between p-20 h-64">
				<Typography className="text-16 font-medium"> Shift Time Period</Typography>
			</div>

			<Box className="w-full px-28">
				<table style={{ border: '1px solid gray', width: '100%' }}>
					<thead>
						<tr style={{ border: '1px solid gray' }}>
							<th> Day</th>
							{/* Display 24 hour system header */}
							{Array.from({ length: 24 }).map((_, index) => (
								<th style={{ border: '1px solid gray' }} key={index}>
									{index}
								</th>
							))}
						</tr>
					</thead>
					<tbody>
						{/* Display table data */}
						{data.map((data, index) => {
							return (
								<>
									{weeks.map(e => (
										<tr style={{ border: '1px solid gray' }} key={index}>
											<td style={{ border: '1px solid gray' }}>{e.day}</td>
											{/* Highlight cells with color */}
											{Array.from({ length: 24 }).map((_, index) => {
												const hour = index.toString().padStart(2, '0');
												const [startHour, startMinute] = (data?.timetable?.checkin_end ?? '')
													.split(':')
													.map(Number);
												const [endHour, endMinute] = (data?.timetable?.checkout_end ?? '')
													.split(':')
													.map(Number);
												const currentHour = Number(hour);

												const currentMinute = Number(0); // use 0 for the minute since it is not provided in the hour value
												let isHighlighted = false;
												if (startHour <= endHour) {
													// If start and end times are on the same day
													isHighlighted =
														(currentHour > startHour ||
															(currentHour === startHour &&
																currentMinute >= startMinute)) &&
														(currentHour < endHour ||
															(currentHour === endHour && currentMinute <= endMinute));
												} else {
													// If start and end times are on different days
													isHighlighted =
														(currentHour >= startHour &&
															currentHour < 24 &&
															currentMinute >= startMinute) ||
														(currentHour >= 0 && currentHour <= endHour) ||
														(currentHour === endHour && currentMinute <= endMinute) ||
														(currentHour >= startHour && currentHour <= 23);
													// add check for 22:00 to 23:00 on the same day
													// isHighlighted =
													// 	isHighlighted ||
													// 	(currentHour === 22 && currentMinute >= startMinute) ||
													// 	currentHour === startHour;
												}

												const isFriday = e.day === 'Friday';
												const isSaturday = e.day === 'Saturday';
												const isSunday = e.day === 'Sunday';
												const isMonday = e.day === 'Monday';
												const isTuesday = e.day === 'Tuesday';
												const isWednesday = e.day === 'Wednesday';
												const isThursday = e.day === 'Thursday';
												const isFridayUnhighlighted = isFriday && !data?.friday;
												const isSaturdayUnhighlighted = isSaturday && !data?.saturday;
												const isSundayUnhighlighted = isSunday && !data?.sunday;
												const isMondayUnhighlighted = isMonday && !data?.monday;
												const isTuesdayUnhighlighted = isTuesday && !data?.tuesday;
												const isWednesdayUnhighlighted = isWednesday && !data?.wednesday;
												const isThursdayUnhighlighted = isThursday && !data?.thursday;
												const cellStyle = {
													backgroundColor:
														isFridayUnhighlighted ||
														isSaturdayUnhighlighted ||
														isSundayUnhighlighted ||
														isMondayUnhighlighted ||
														isTuesdayUnhighlighted ||
														isWednesdayUnhighlighted ||
														isThursdayUnhighlighted
															? 'white'
															: isHighlighted
															? data?.timetable?.color
															: 'white',

													border: isHighlighted ? 'none' : '1px solid #d9cfcf'
												};
												return <td key={index} style={cellStyle}></td>;
											})}
										</tr>
									))}
								</>
							);
						})}
					</tbody>
				</table>
			</Box>
		</Paper>
	);
}

export default memo(WeekTable);
