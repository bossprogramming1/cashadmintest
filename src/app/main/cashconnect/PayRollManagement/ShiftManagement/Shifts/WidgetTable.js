import Paper from '@material-ui/core/Paper';
import Typography from '@material-ui/core/Typography';
import _ from '@lodash';
import { useEffect, memo, useState } from 'react';
import DeleteIcon from '@material-ui/icons/Delete';
import EditIcon from '@material-ui/icons/Edit';
import { format } from 'date-fns';
import {
	Autocomplete,
	Box,
	Button,
	Table,
	TableBody,
	TableCell,
	TableHead,
	TableRow,
	TableSortLabel,
	Tooltip
} from '@mui/material';
import ScheduleSendIcon from '@mui/icons-material/ScheduleSend';
import { makeStyles } from '@material-ui/core/styles';
import { useDispatch, useSelector } from 'react-redux';
import history from '@history';
import Pagination from '@material-ui/lab/Pagination';
import { getWidget6 } from 'app/main/apps/dashboards/project/store/widgetsSlice';
import { Controller, FormProvider, useForm, useFormContext } from 'react-hook-form';

import { getShifts, getTimetables } from 'app/store/dataSlice';
import { Checkbox, FormControl, FormControlLabel, Modal } from '@material-ui/core';
import { weeks } from 'app/@data/data';
import axios from 'axios';
import { CREATE_SHIFT_DAYTIME, GET_SHIFT_DAYTIME_BY_SHIFTID } from 'app/constant/constants';
import { getShiftTimetable } from '../store/shiftSlice';

const useStyles = makeStyles(theme => ({
	tablecell: {
		fontSize: '50px'
	},
	modal: {
		position: 'absolute',
		// display: 'flex',
		alignItems: 'center',
		// justifyContent: 'space-evenly',
		marginTop: '10%',
		marginLeft: '35%',

		transform: ' translate(-50%, -50)',
		// transform: 'translate(-50%, -50%)',
		width: 600,
		height: 400,
		boxShadow: 24,
		color: theme.palette.background.paper,
		backgroundColor: '#252f3e'
	}
}));

function WidgetTable(props) {
	const dispatch = useDispatch();
	const classes = useStyles(props);
	const totalPages = sessionStorage.getItem('total_shifts_pages');
	const user_role = localStorage.getItem('user_role');
	const timetable = useSelector(state => state.data.timetables);
	const [selectedRow, setSelectedRow] = useState(null);

	//Modal
	const [open, setOpen] = useState(false);
	const [timeId, setTimeId] = useState(null);
	const [shiftName, setShiftName] = useState('');
	const [timeIdUpdate, setTimeIdUpdate] = useState();
	const [newShiftId, setNewShitId] = useState(0);
	const [shiftChecked, setShiftChecked] = useState({});
	const [shifId, setShifId] = useState(null);
	const handleChange = id => {
		setShiftChecked({ [id]: true });
	};
	const [page, setPage] = useState(1);
	const handleOpen = shift => {
		setShiftName(shift.name);
		timetable.find(e => setValue(`${e.name}`, false));
		axios
			.get(`${GET_SHIFT_DAYTIME_BY_SHIFTID}${shift.id}`, {
				headers: {
					'Content-type': 'application/json',
					Authorization: localStorage.getItem('jwt_access_token')
				}
			})
			.then(res => {
				setShifId(res.data?.id);
				setTimeIdUpdate(res.data?.timetable.id);
				setValue('saturday', res.data?.saturday);
				setValue('sunday', res.data?.sunday);
				setValue('monday', res.data?.monday);
				setValue('tuesday', res.data?.tuesday);
				setValue('wednesday', res.data?.wednesday);
				setValue('thursday', res.data?.thursday);
				setValue('friday', res.data?.friday);
				setShiftChecked({ [res.data?.timetable.id]: true });
			});
		setNewShitId(shift.id);

		setOpen(true);
	};
	const handleClose = () => setOpen(false);

	const methods = useForm({
		mode: 'onChange',
		defaultValues: {
			saturday: false,
			sunday: false,
			monday: false,
			tuesday: false,
			wednesday: false,
			thursday: false,
			friday: false
		}
	});
	const { control, formState, setValue, getValues } = methods || {};

	function handleUpdateShift(item, event) {
		localStorage.removeItem('deleteShift');
		localStorage.setItem('updateShift', event);
		history.push(`/apps/shifts-management/${item.id}/${item?.name}`);
	}
	function handleDeleteShift(item, event) {
		localStorage.removeItem('updateShift');

		localStorage.setItem('deleteShift', event);
		history.push(`/apps/shifts-management/${item.id}/${item?.name}`);
	}
	useEffect(() => {
		dispatch(getTimetables());
	}, []);

	const handlePagination = (e, handlePage) => {
		setPage(handlePage - 1);
		dispatch(getShifts({ page: handlePage }));
	};

	const shiftId = e => {
		localStorage.setItem('shiftId', e);
		dispatch(getShiftTimetable(e));
	};

	function handleSaveShiftTimetable() {
		const timeData = getValues();
		timeData.id = shifId;
		timeData.timetable = timeId || timeIdUpdate;
		timeData.shift = newShiftId;

		axios
			.post(`${CREATE_SHIFT_DAYTIME}`, timeData, {
				headers: {
					'Content-type': 'application/json',
					Authorization: localStorage.getItem('jwt_access_token')
				}
			})
			.then(res => {
				setOpen(false);
				dispatch(getShiftTimetable(shifId));
			});
	}
	//colored row when clicked
	const handleRowClick = shiftId => {
		setSelectedRow(shiftId);
		// setSelectedRow(shiftId);
	};

	return (
		<Paper className="w-full rounded-40 shadow">
			<div className="flex items-center justify-between p-20 h-64">
				<Typography className="text-16 font-medium"> Shift Management</Typography>
			</div>
			<Box>
				<Modal
					open={open}
					onClose={handleClose}
					aria-labelledby="modal-modal-title"
					aria-describedby="modal-modal-description"
				>
					<Box className={classes.modal}>
						<FormProvider {...methods}>
							<div
								className="px-32"
								style={{
									display: 'flex',
									justifyContent: 'space-evenly',
									marginTop: '7rem'
								}}
							>
								<div
									style={{
										border: '2px solid white',
										width: '100%',
										height: '250px',
										color: 'black',
										backgroundColor: 'whitesmoke'
									}}
								>
									<div>
										<p className="font-medium">
											Select <span className="font-bold">{shiftName}</span> category for this time
											period
										</p>
									</div>
									{timetable?.map(support => (
										<div style={{ display: 'flex' }}>
											<Controller
												name={support?.name}
												control={control}
												render={({ field }) => {
													return (
														<FormControl>
															<FormControlLabel
																label={support?.name}
																control={
																	<Checkbox
																		{...field}
																		checked={
																			shiftChecked[support?.id] ||
																			// field.value ||
																			false
																		}
																		onChange={event => {
																			handleChange(support?.id);
																		}}
																		onClick={e => {
																			setShiftChecked({ [support?.id]: true });
																			e.target.checked === true
																				? setTimeId(support.id)
																				: 0;
																		}}
																		// onChange={e =>
																		// 	e.target.checked === true ? support.id : 0
																		// }
																	/>
																}
															/>
														</FormControl>
													);
												}}
											/>

											{/* <input
											style={{ margin: '5px' }}
											type="radio"
											id={support?.name}
											name="radio"
											value="30"
											// onChange={newValue => handleChangeTicketStatus(support?.id)}
										></input>
										<label
											// style={{ display: 'flex', alignItems: 'center' }}
											// onChange={newValue => handleChangeTicketStatus(support?.id)}
											htmlFor={support?.name}
										>
											{support?.name}
										</label> */}
										</div>
									))}
								</div>
								<div
									style={{
										border: '2px solid white',
										width: '100%',
										height: '250px',
										color: 'black',
										backgroundColor: 'whitesmoke'
									}}
								>
									{weeks?.map(day => (
										<>
											<Controller
												name={day?.name}
												control={control}
												render={({ field }) => (
													<FormControl>
														<FormControlLabel
															style={{ width: '100px', marginRight: '22px' }}
															label={day?.name}
															control={
																<Checkbox
																	{...field}
																	checked={field.value ? field.value : false}
																/>
															}
														/>
													</FormControl>
												)}
											/>
											{/* <input
											style={{ margin: '5px' }}
											type="radio"
											id={support?.name}
											name="radio"
											value="30"
											// onChange={newValue => handleChangeTicketStatus(support?.id)}
										></input>
										<label
											// style={{ display: 'flex', alignItems: 'center' }}
											// onChange={newValue => handleChangeTicketStatus(support?.id)}
											htmlFor={support?.name}
										>
											{support?.name}
										</label> */}
										</>
									))}
								</div>
							</div>

							{/* <Controller
							name="ticket_status"
							control={control}
							render={({ field: { onChange, value } }) => (
								<Autocomplete
									freeSolo
									required
									value={value ? ticketStatus.find(ticketStatu => ticketStatu.id === value) : null}
									options={ticketStatus}
									getOptionLabel={option => `${option?.name}`}
									onChange={(event, newValue) => handleChangeTicketStatus(event, newValue?.id)}
									//defaultValue={{ id: null, name: "Select a gender" }}
									renderInput={params => (
										<TextField
											{...params}
											className="mt-8 mb-16"
											error={!!errors.ticket_status}
											InputLabelProps={{ shrink: true }}
											helperText={errors?.ticket_status?.message}
											label="Ticket Status"
											placeholder="Ticket Status..."
											id="ticket_status"
											variant="outlined"
										/>
									)}
								/>
							)}
						/> */}
						</FormProvider>
						<div className="flex justify-end mr-32">
							<Button
								className="whitespace-nowrap mx-4"
								variant="contained"
								color="secondary"
								style={{ backgroundColor: '#22d3ee' }}
								// disabled={!name || _.isEmpty(name)}
								onClick={handleSaveShiftTimetable}
							>
								Save
							</Button>
						</div>
					</Box>
				</Modal>
				<Table>
					<TableHead>
						<TableRow style={{ fontSize: '16px' }}>
							<TableCell className="whitespace-nowrap text-16 font-medium">Shift Name</TableCell>
							<TableCell className="whitespace-nowrap text-16 font-medium">Beginning Data</TableCell>
							<TableCell className="whitespace-nowrap text-16 font-medium">End Data</TableCell>
							<TableCell className="whitespace-nowrap text-16 font-medium">Action</TableCell>
							<TableCell className="whitespace-nowrap text-16 font-medium">Time Period</TableCell>
						</TableRow>
					</TableHead>
					<TableBody>
						{props?.data?.map(shift => {
							return (
								<TableRow
									hover
									key={shift?.id}
									style={{
										background: selectedRow === shift?.id ? '#e7eaed' : 'transparent'
									}}
									onClick={() => {
										handleRowClick(shift?.id);
										shiftId(shift?.id);
									}}
								>
									<TableCell>{shift?.name}</TableCell>
									<TableCell>
										{shift?.start_date && format(new Date(shift?.start_date), 'dd-MM-yyyy')}
									</TableCell>
									<TableCell>
										{shift?.end_date && format(new Date(shift?.end_date), 'dd-MM-yyyy')}
									</TableCell>
									<TableCell
										whitespace-nowrap
										className="p-4 md:p-16"
										align="center"
										component="th"
										scope="row"
									>
										<div>
											<EditIcon
												onClick={shiftEvent => handleUpdateShift(shift, 'updateShift')}
												className="cursor-pointer"
												style={{ color: 'green' }}
											/>{' '}
											<DeleteIcon
												onClick={shiftEvent => handleDeleteShift(shift, 'deleteShift')}
												className="cursor-pointer"
												style={{
													color: 'red',
													visibility:
														user_role === 'ADMIN' || user_role === 'admin'
															? 'visible'
															: 'hidden'
												}}
											/>
										</div>
									</TableCell>
									<TableCell
										whitespace-nowrap
										className="p-4 md:p-16"
										align="center"
										component="th"
										scope="row"
									>
										<ScheduleSendIcon
											onClick={event => handleOpen(shift)}
											style={{ color: 'gray', fontSize: '25px' }}
										/>
									</TableCell>
								</TableRow>
							);
						})}
					</TableBody>
				</Table>
			</Box>
			{/* <Box
				sx={{
					display: 'flex',
					justifyContent: 'space-between',
					p: 2
				}}
			>
				<Pagination
					count={totalPages}
					page={page}
					defaultPage={1}
					color="primary"
					showFirstButton
					showLastButton
					variant="outlined"
					shape="rounded"
					onChange={handlePagination}
				/>
			</Box> */}
		</Paper>
	);
}

export default memo(WidgetTable);
