import { createAsyncThunk, createEntityAdapter, createSlice } from '@reduxjs/toolkit';
import { GET_SHIFTS } from 'app/constant/constants';
import axios from 'axios';

export const getShifts = createAsyncThunk('shiftManagement/shifts/getShifts', async pageAndSize => {
	axios.defaults.headers.common['Content-type'] = 'application/json';
	axios.defaults.headers.common.Authorization = localStorage.getItem('jwt_access_token');

	const response = axios.get(GET_SHIFTS, { params: pageAndSize });
	const data = await response;

	sessionStorage.setItem('total_shifts_elements', data.data.total_elements);
	sessionStorage.setItem('total_shifts_pages', data.data.total_pages);
	delete axios.defaults.headers.common['Content-type'];
	delete axios.defaults.headers.common.Authorization;

	return data.data.shifts;
});

const ShiftsAdapter = createEntityAdapter({});

export const { selectAll: selectShifts, selectById: selectShiftById } = ShiftsAdapter.getSelectors(
	state => state.shiftsManagement.shifts
);

const shiftsSlice = createSlice({
	name: 'shiftManagement/shifts',
	initialState: ShiftsAdapter.getInitialState({
		searchText: ''
	}),
	reducers: {
		setShiftsSearchText: {
			reducer: (state, action) => {
				state.searchText = action.payload;
			},
			prepare: event => ({ payload: event.target.value || '' })
		}
	},
	extraReducers: {
		[getShifts.fulfilled]: ShiftsAdapter.setAll
	}
});

export const { setData, setShiftsSearchText } = shiftsSlice.actions;
export default shiftsSlice.reducer;
