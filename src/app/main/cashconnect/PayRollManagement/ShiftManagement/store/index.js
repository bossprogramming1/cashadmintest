import { combineReducers } from '@reduxjs/toolkit';
import shift from './shiftSlice';
import shifts from './shiftsSlice';

const reducer = combineReducers({
	shift,
	shifts
});

export default reducer;
