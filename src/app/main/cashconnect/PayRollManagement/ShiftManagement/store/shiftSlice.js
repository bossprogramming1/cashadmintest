import { createAsyncThunk, createSlice } from '@reduxjs/toolkit';
import { CREATE_SHIFT, DELETE_SHIFT, GET_SHIFT, GET_TIMETABLE_BY_SHIFT_ID, UPDATE_SHIFT } from 'app/constant/constants';
import axios from 'axios';

export const getShift = createAsyncThunk('shiftManagement/shift/getShift', async params => {
	const authTOKEN = {
		headers: {
			'Content-type': 'application/json',
			Authorization: localStorage.getItem('jwt_access_token')
		}
	};
	const response = await axios.get(`${GET_SHIFT}${params.shiftId}`, authTOKEN);
	const data = await response.data;
	return data === undefined ? null : data;
});
export const getShiftTimetable = createAsyncThunk('shiftManagement/timeimetalbe/getShiftTimetable', async params => {
	const shiftId = localStorage.getItem('shiftId');

	const authTOKEN = {
		headers: {
			'Content-type': 'application/json',
			Authorization: localStorage.getItem('jwt_access_token')
		}
	};
	const response = await axios.get(`${GET_TIMETABLE_BY_SHIFT_ID}${shiftId}`, authTOKEN);
	const data = await response.data;
	return data === undefined ? null : data;
});

export const removeShift = createAsyncThunk('shiftManagement/shift/removeShift', async val => {
	const authTOKEN = {
		headers: {
			'Content-type': 'application/json',
			Authorization: localStorage.getItem('jwt_access_token')
		}
	};
	const shiftId = val.id;
	const response = await axios.delete(`${DELETE_SHIFT}${shiftId}`, authTOKEN);
	return response;
});

export const updateShift = createAsyncThunk(
	'shiftManagement/shift/updateShift',
	async (shiftData, { dispatch, getState }) => {
		const authTOKEN = {
			headers: {
				'Content-type': 'application/json',
				Authorization: localStorage.getItem('jwt_access_token')
			}
		};
		const { shift } = getState().shiftsManagement;

		const response = await axios.put(`${UPDATE_SHIFT}${shift.id}`, shiftData, authTOKEN);
		return response;
	}
);

export const saveShift = createAsyncThunk('shiftManagement/shift/saveShift', async shiftData => {
	const authTOKEN = {
		headers: {
			'Content-type': 'application/json',
			Authorization: localStorage.getItem('jwt_access_token')
		}
	};
	const response = await axios.post(`${CREATE_SHIFT}`, shiftData, authTOKEN);
	return response;
});

const shiftSlice = createSlice({
	name: 'shiftManagement/shift',
	initialState: null,
	reducers: {
		resetShift: () => null,
		newShift: {
			reducer: (state, action) => action.payload,
			prepare: event => ({
				payload: {
					name: '',
					start_date: '',
					end_date: ''
				}
			})
		}
	},
	extraReducers: {
		[getShift.fulfilled]: (state, action) => action.payload,
		[getShiftTimetable.fulfilled]: (state, action) => action.payload,
		[saveShift.fulfilled]: (state, action) => action.payload,
		[removeShift.fulfilled]: (state, action) => action.payload,
		[updateShift.fulfilled]: (state, action) => action.payload
	}
});

export const { newShift, resetShift } = shiftSlice.actions;

export default shiftSlice.reducer;
