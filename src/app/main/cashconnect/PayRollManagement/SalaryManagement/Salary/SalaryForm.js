import _ from '@lodash';
import { makeStyles } from '@material-ui/core/styles';
import TextField from '@material-ui/core/TextField';
import { Autocomplete } from '@material-ui/lab';
import React, { useEffect, useState } from 'react';
import { Controller, useFieldArray, useFormContext } from 'react-hook-form';
import { useDispatch, useSelector } from 'react-redux';
import { useHistory, useParams } from 'react-router-dom';
import { Add as AddIcon, Delete as DeleteIcon } from '@material-ui/icons';
import { getDepartments, getEmployees, getPayheads, getUnits } from 'app/store/dataSlice';
import FormControl from '@material-ui/core/FormControl';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Radio from '@material-ui/core/Radio';
import RadioGroup from '@material-ui/core/RadioGroup';
import FormLabel from '@material-ui/core/FormLabel';
import {
	Box,
	Paper,
	Table,
	TableBody,
	TableCell,
	TableContainer,
	TableHead,
	TableRow,
	Typography
} from '@material-ui/core';
import moment from 'moment';
import CustomDatePicker from 'app/@components/CustomDatePicker';
import { CHECK_USERDEFINEVALUE_BY_EMPLOYEE_PAYHEAD } from 'app/constant/constants';
import Swal from 'sweetalert2';
import { getSalary, saveSalary, updateSalary } from '../store/salarySlice';
import { getPayheadById, removePayheadById } from '../store/salaryDetailsSlice';
import { getPayrollMakeStyles } from '../../PayRollUtils/payrollMakeStyles';

const useStyles = makeStyles(theme => ({
	...getPayrollMakeStyles(theme),
	textField: {
		'& input[type=number]': {
			'-moz-appearance': 'textfield' // Firefox
		},
		'& input::-webkit-outer-spin-button, & input::-webkit-inner-spin-button': {
			'-webkit-appearance': 'none', // Chrome, Safari, Edge
			margin: 0
		}
	}
}));

function SalaryForm(props) {
	const userID = localStorage.getItem('user_id');

	const classes = useStyles(props);
	const payheadsData = useSelector(({ salarysManagement }) => salarysManagement?.payheadsList);
	const methods = useFormContext();
	const { control, formState, getValues, reset, setValue, watch } = methods;
	const { errors, isValid, dirtyFields } = formState;
	const routeParams = useParams();
	const { salaryId } = routeParams;
	const history = useHistory();
	const handleDelete = localStorage.getItem('salaryEvent');
	const employees = useSelector(state => state.data.employees);
	const payheads = useSelector(state => state.data?.payheads);
	const departments = useSelector(state => state.data?.departments);
	const watchPayhead = watch('payhead');
	// const units = useSelector(state => state.data?.units);
	// const [selectedValues, setSelectedValues] = useState('');
	// const [salaryTable, setSalaryTable] = useState(false);
	// const [fieldDisable, setFieldDisable] = useState(false);
	const [selectedRadio, setSelectedRadio] = useState('');

	// const { fields, remove } = useFieldArray({
	// 	control,
	// 	name: 'payhead_list',
	// 	keyName: 'key'
	// });
	console.log('watchPayhead', watchPayhead);
	const dispatch = useDispatch();

	useEffect(() => {
		dispatch(getEmployees());
		dispatch(getPayheads());
		dispatch(getUnits());
		dispatch(getDepartments());
	}, [dispatch]);
	useEffect(() => {
		if (salaryId !== 'new') {
			setSelectedRadio(getValues().calculation_for);
		}
	}, [salaryId, getValues().calculation_for]);

	function handleSaveSalary() {
		dispatch(saveSalary(getValues())).then(res => {
			if (res.payload) {
				localStorage.setItem('salaryAlert', 'saveSalary');
				history.push('/apps/salary-management/salarys');
			}
		});
	}

	function handleUpdateSalary() {
		dispatch(updateSalary(getValues())).then(res => {
			if (res.payload) {
				localStorage.setItem('salaryAlert', 'updateSalary');
				history.push('/apps/salary-management/salarys');
			}
		});
	}

	const handleSubmitOnKeyDownEnter = ev => {
		if (ev.key === 'Enter') {
			if (routeParams.salaryId === 'new' && !(_.isEmpty(dirtyFields) || !isValid)) {
				handleSaveSalary();
			} else if (handleDelete !== 'Delete' && routeParams?.salaryName) {
				handleUpdateSalary();
			}
		}
	};
	// const useFetchedData = () => {
	// 	const [calculationType, setCalculationType] = useState([]);
	// 	const [payHeadType, setPayHeadType] = useState([]);

	// 	const updateCalculationType = (index, data) => {
	// 		setCalculationType(prevData => {
	// 			const updatedData = [...prevData];
	// 			updatedData[index] = data;
	// 			return updatedData;
	// 		});
	// 	};

	// 	const updatePayHeadType = (index, data) => {
	// 		setPayHeadType(prevData => {
	// 			const updatedData = [...prevData];
	// 			updatedData[index] = data;
	// 			return updatedData;
	// 		});
	// 	};

	// 	return { calculationType, updateCalculationType, payHeadType, updatePayHeadType };
	// };

	// Inside your component
	// const { calculationType, updateCalculationType, payHeadType, updatePayHeadType } = useFetchedData();

	// useEffect(() => {
	// 	if (calculationType[0] === 'On Attendance' || calculationType[0] === 'On Production') {
	// 		setFieldDisable(false);
	// 	} else {
	// 		setFieldDisable(true);
	// 	}
	// }, [calculationType]);
	const checkUserDefineValue = () => {
		fetch(`${CHECK_USERDEFINEVALUE_BY_EMPLOYEE_PAYHEAD}`, {
			method: 'POST',
			headers: {
				'Content-Type': 'application/json'
			},
			body: JSON.stringify(getValues())
		})
			.then(response => {
				if (!response.ok) {
					throw new Error('Network response was not ok');
				}
				return response.json();
			})
			.then(res => {
				console.log('res', res);
				if (res.has_value === false)
					Swal.fire({
						position: 'top-center',
						icon: 'error',
						title: `${res.text}`,
						showConfirmButton: false,
						timer: 60000
					});
				else if (res.has_value === null && res.text) {
					Swal.fire({
						position: 'top-center',
						icon: 'error',
						title: `${res.text}`,
						showConfirmButton: false,
						timer: 60000
					});
				}
			})
			.catch(() => '');
	};

	return (
		<div>
			<Controller
				name="calculation_for"
				control={control}
				render={({ field }) => (
					<FormControl component="fieldset">
						<FormLabel component="legend">How would you like to show?</FormLabel>
						<RadioGroup
							row
							aria-label="position"
							name="position"
							defaultValue="top"
							onChange={event => {
								if (event.target.value == 'all') {
									setSelectedRadio(event.target.value);
									// setSelectedValues('All');
									// setSalaryTable(true);
									setValue('department', []);
									setValue('employee', []);
								} else if (event.target.value == 'department') {
									setSelectedRadio(event.target.value);

									setValue('employee', []);
									// setSelectedValues('');
								} else if (event.target.value == 'employees') {
									setSelectedRadio(event.target.value);

									setValue('department', []);
									// setSelectedValues('');
								} else {
									setSelectedRadio(event.target.value);
									// setSelectedValues('');
								}

								if (watchPayhead?.length !== 0 && watchPayhead) {
									checkUserDefineValue();
								}
							}}
						>
							<FormControlLabel
								{...field}
								value="all"
								control={
									<Radio
										checked={field.value === 'all' ? field.value : false}
										style={{ color: '#22d3ee' }}
									/>
								}
								label="All"
							/>
							<FormControlLabel
								{...field}
								value="department"
								control={
									<Radio
										checked={field.value === 'department' ? field.value : false}
										style={{ color: 'green' }}
									/>
								}
								label="Department"
							/>
							<FormControlLabel
								{...field}
								value="employees"
								control={
									<Radio
										checked={field.value === 'employees' ? field.value : false}
										style={{ color: 'red' }}
									/>
								}
								label="Employees"
							/>
						</RadioGroup>
					</FormControl>
				)}
			/>
			{selectedRadio === 'all' && (
				<>
					<Controller
						name="date"
						control={control}
						render={({ field }) => {
							return <CustomDatePicker field={field} label="Date" required />;
						}}
					/>
					<Controller
						name="payhead"
						control={control}
						render={({ field: { onChange, value, name } }) => (
							<Autocomplete
								className="mt-8 mb-16"
								freeSolo
								multiple
								filterSelectedOptions
								value={value ? payheads.filter(data => value.includes(data.id)) : []}
								options={payheads}
								getOptionLabel={option => `${option?.name}`}
								onChange={(event, newValue) => {
									const selectedValues = newValue.map(option => option.id);
									onChange(selectedValues);
									// const employee = newValue.map(option => option.first_name);
									// const employeeNames = employee.join(', ');
									// setSelectedValues(employeeNames);
									// setSalaryTable(true);

									checkUserDefineValue();
								}}
								renderInput={params => {
									return (
										<TextField
											{...params}
											placeholder="Select Payhead"
											label="Payhead"
											error={!!errors.payhead}
											required
											helperText={errors?.payhead?.message}
											variant="outlined"
											InputLabelProps={{
												shrink: true
											}}
										/>
									);
								}}
							/>
						)}
					/>
				</>
			)}

			{selectedRadio === 'department' && (
				<>
					<Controller
						name="date"
						control={control}
						render={({ field }) => {
							return <CustomDatePicker field={field} label="Date" required />;
						}}
					/>
					<Controller
						name="department"
						control={control}
						render={({ field: { onChange, value, name } }) => (
							<Autocomplete
								className="mt-8 mb-16"
								freeSolo
								multiple
								filterSelectedOptions
								value={value ? departments.filter(data => value.includes(data.id)) : []}
								options={departments}
								getOptionLabel={option => `${option?.name}`}
								onChange={(event, newValue) => {
									const selectedValues = newValue.map(option => option.id);
									onChange(selectedValues);
									// const deparment = newValue.map(option => option.name);
									// const departmentNames = deparment.join(', ');
									// setSelectedValues(departmentNames);
									// setSalaryTable(true);
									if (watchPayhead?.length !== 0) {
										checkUserDefineValue();
									}
								}}
								renderInput={params => {
									return (
										<TextField
											{...params}
											placeholder="Select departments"
											label="Departments"
											error={!!errors.departments}
											required
											helperText={errors?.departments?.message}
											variant="outlined"
											InputLabelProps={{
												shrink: true
											}}
										/>
									);
								}}
							/>
						)}
					/>
					<Controller
						name="payhead"
						control={control}
						render={({ field: { onChange, value, name } }) => (
							<Autocomplete
								className="mt-8 mb-16"
								freeSolo
								multiple
								filterSelectedOptions
								value={value ? payheads.filter(data => value.includes(data.id)) : []}
								options={payheads}
								getOptionLabel={option => `${option?.name}`}
								onChange={(event, newValue) => {
									const selectedValues = newValue.map(option => option.id);
									onChange(selectedValues);
									// const employee = newValue.map(option => option.first_name);
									// const employeeNames = employee.join(', ');
									// setSelectedValues(employeeNames);
									// setSalaryTable(true);
									checkUserDefineValue();
								}}
								renderInput={params => {
									return (
										<TextField
											{...params}
											placeholder="Select Payhead"
											label="Payhead"
											error={!!errors.payhead}
											required
											helperText={errors?.payhead?.message}
											variant="outlined"
											InputLabelProps={{
												shrink: true
											}}
										/>
									);
								}}
							/>
						)}
					/>
				</>
			)}
			{selectedRadio === 'employees' && (
				<>
					<Controller
						name="date"
						control={control}
						render={({ field }) => {
							return <CustomDatePicker field={field} label="Date" required />;
						}}
					/>
					<Controller
						name="employee"
						control={control}
						render={({ field: { onChange, value, name } }) => (
							<Autocomplete
								className="mt-8 mb-16"
								freeSolo
								multiple
								filterSelectedOptions
								value={value ? employees.filter(data => value.includes(data.id)) : []}
								options={employees}
								getOptionLabel={option => `${option?.first_name} ${option?.last_name}`}
								onChange={(event, newValue) => {
									const selectedValues = newValue.map(option => option.id);
									onChange(selectedValues);
									// const employee = newValue.map(option => option.first_name);
									// const employeeNames = employee.join(', ');
									// setSelectedValues(employeeNames);
									// setSalaryTable(true);
									if (watchPayhead?.length !== 0 && watchPayhead) {
										checkUserDefineValue();
									}
								}}
								renderInput={params => {
									return (
										<TextField
											{...params}
											placeholder="Select Employee"
											label="Employee"
											error={!!errors.employees}
											required
											helperText={errors?.employees?.message}
											variant="outlined"
											InputLabelProps={{
												shrink: true
											}}
										/>
									);
								}}
							/>
						)}
					/>
					<Controller
						name="payhead"
						control={control}
						render={({ field: { onChange, value, name } }) => (
							<Autocomplete
								className="mt-8 mb-16"
								freeSolo
								multiple
								filterSelectedOptions
								value={value ? payheads.filter(data => value.includes(data.id)) : []}
								options={payheads}
								getOptionLabel={option => `${option?.name}`}
								onChange={(event, newValue) => {
									const selectedValues = newValue.map(option => option.id);
									onChange(selectedValues);
									// const employee = newValue.map(option => option.first_name);
									// const employeeNames = employee.join(', ');
									// setSelectedValues(employeeNames);
									// setSalaryTable(true);
									checkUserDefineValue();
								}}
								renderInput={params => {
									return (
										<TextField
											{...params}
											placeholder="Select Payhead"
											label="Payhead"
											error={!!errors.payhead}
											required
											helperText={errors?.payhead?.message}
											variant="outlined"
											InputLabelProps={{
												shrink: true
											}}
										/>
									);
								}}
							/>
						)}
					/>
				</>
			)}

			{/* {salaryTable && (
				<Box className={classes.mainContainer}>
					<TableContainer component={Paper} className={classes.tblContainer}>
						<Table className={classes.table} aria-label="simple table">
							<TableHead className={classes.tableHead}>
								<TableRow style={{ fontSize: '14px' }}>
									<TableCell className={classes.tableCell} style={{ fontSize: '14px' }}>
										<Typography className={classes.tableCell}>Effective From</Typography>
									</TableCell>
									<TableCell style={{ fontSize: '14px' }} className={classes.tableCell}>
										<Typography className="text-14 font-medium">
											{selectedRadio === 'employees'
												? 'Employees'
												: selectedRadio === 'department'
												? 'Department'
												: 'All'}
										</Typography>
									</TableCell>
									<TableCell style={{ fontSize: '14px' }} className={classes.tableCell}>
										<Typography className="text-14 font-medium">Pay Head</Typography>
									</TableCell>
									<TableCell style={{ fontSize: '14px' }} className={classes.tableCell}>
										<Typography className="text-14 font-medium">Rate</Typography>
									</TableCell>
									<TableCell style={{ fontSize: '14px' }} className={classes.tableCell}>
										<Typography className="text-14 font-medium">Units</Typography>
									</TableCell>
									<TableCell style={{ fontSize: '14px' }} className={classes.tableCell}>
										<Typography className="text-14 font-medium">PayHead Type</Typography>
									</TableCell>
									<TableCell style={{ fontSize: '14px' }} className={classes.tableCell}>
										<Typography className="text-14 font-medium">Calculation Type</Typography>
									</TableCell>
									<TableCell style={{ fontSize: '14px' }} className={classes.tableCell}>
										<Typography className="text-14 font-medium">Action</Typography>
									</TableCell>
								</TableRow>
							</TableHead>
							<TableBody>
								<TableRow>
									<TableCell
										rowSpan={fields.length + 1}
										style={{ border: '1px solid #e0e0e0' }}
										className={classes.tableCell}
									>
										<Controller
											className="p-0"
											name={`effective_from`}
											control={control}
											render={({ field }) => {
												return (
													<CustomDatePicker
														className="p-0"
														previous_date_disable={true}
														field={field}
														style={{ maxWidth: '130px' }}
														label="Effective from"
													/>
												);
											}}
										/>
									</TableCell>
									<TableCell rowSpan={fields.length + 1} className={classes.tableCellInBody}>
										{selectedValues}
									</TableCell>
								</TableRow>
								{fields.map((item, idx) => {
									const handlePayheadChange = (newValue, index) => {
										dispatch(getPayheadById(newValue?.id)).then(data => {
											const calculationTypeData =
												data.payload?.payheads?.calculation_type.name || '';
											const payHeadTypeData = data.payload?.payheads?.payhead_type.name || '';

											updateCalculationType(index, calculationTypeData);
											updatePayHeadType(index, payHeadTypeData);
										});
									};

									return (
										<TableRow key={item.key}>
											<TableCell className={classes.tableCellInBody}>
												<Controller
													name={`payhead_list.${idx}.payhead`}
													control={control}
													render={({ field: { onChange, value } }) => {
														return (
															<Autocomplete
																className="mt-8 mb-16"
																freeSolo
																value={
																	value
																		? payheads.find(data => data.id === value)
																		: []
																}
																options={payheads}
																getOptionLabel={option => `${option?.name}`}
																onChange={(event, newValue) => {
																	onChange(newValue?.id);

																	handlePayheadChange(newValue, idx);
																}}
																renderInput={params => (
																	<TextField
																		{...params}
																		placeholder="Select Payhead"
																		label="Payhead"
																		error={!!errors.payhead_type}
																		required
																		helperText={errors?.payhead_type?.message}
																		variant="outlined"
																		InputLabelProps={{
																			shrink: true
																		}}
																	/>
																)}
															/>
														);
													}}
												/>
											</TableCell>
											<TableCell className={classes.tableCellInBody}>
												<Controller
													name={`payhead_list.${idx}.rate`}
													control={control}
													render={({ field }) => {
														return (
															<TextField
																{...field}
																className="mt-8 mb-16"
																label="Value"
																id={`payhead_list.${idx}.rate`}
																variant="outlined"
																InputLabelProps={{ shrink: true }}
																fullWidth
																error={!!errors.rate}
																helperText={errors?.rate?.message}
																style={{ maxWidth: '100px' }} // Adjust the width as needed
																disabled={!!fieldDisable}
															/>
														);
													}}
												/>
											</TableCell>
											<TableCell className={classes.tableCellInBody}>
												<Controller
													name={`payhead_list.${idx}.unit`}
													control={control}
													render={({ field: { onChange, value } }) => {
														return (
															<Autocomplete
																className="mt-8 mb-16"
																freeSolo
																disabled={!!fieldDisable}
																value={
																	value ? units.find(data => data.id === value) : []
																}
																options={units}
																getOptionLabel={option => `${option?.symbol}`}
																onChange={(event, newValue) => {
																	onChange(newValue?.id);
																}}
																renderInput={params => (
																	<TextField
																		{...params}
																		placeholder="Select unit"
																		className="mt-8 mb-16"
																		label="Units"
																		error={!!errors.units}
																		id={`payhead_list.${idx}.unit`}
																		required
																		helperText={errors?.units?.message}
																		variant="outlined"
																		style={{ maxWidth: '100px' }}
																		InputLabelProps={{
																			shrink: true
																		}}
																		disabled={!!fieldDisable}
																	/>
																)}
															/>
														);
													}}
												/>
											</TableCell>
											<TableCell className={classes.tableCellInBody}>
												<Controller
													className="p-0"
													name={`payhead_list.${idx}.payhead_type`}
													control={control}
													render={({ field }) => {
														return (
															<Typography {...field}>
																{payHeadType[idx] || field?.value}
															</Typography>
														);
													}}
												/>
											</TableCell>
											<TableCell className={classes.tableCellInBody}>
												<Controller
													className="p-0"
													name={`payhead_list.${idx}.calculation_type`}
													control={control}
													render={({ field }) => {
														return (
															<Typography {...field}>
																{calculationType[idx] || field?.value}
															</Typography>
														);
													}}
												/>
											</TableCell>

											<TableCell className={classes.tableCellInBody}>
												{idx === 0 ? (
													<div
														variant="outlined"
														className={classes.btnContainer}
														onClick={() => {
															const values = getValues();
															reset({
																...values,
																payhead_list: [
																	...values?.payhead_list,
																	{
																		payhead: '',
																		rate: 0.0,
																		unit: ''
																	}
																]
															});
														}}
														onBlur={() => {}}
													>
														<AddIcon />
													</div>
												) : (
													<div>
														<DeleteIcon
															onClick={() => {
																remove(idx);
															}}
															className="h-52 cursor-pointer"
															style={{ color: 'red' }}
														/>
													</div>
												)}
											</TableCell>
										</TableRow>
									);
								})}
							</TableBody>
						</Table>
					</TableContainer>

				</Box>
			)} */}
		</div>
	);
}

export default SalaryForm;
