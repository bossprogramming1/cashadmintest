import _ from '@lodash';
import Button from '@material-ui/core/Button';
import Icon from '@material-ui/core/Icon';
import { useTheme } from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';
import { motion } from 'framer-motion';
import React from 'react';
import { useFormContext } from 'react-hook-form';
import { useDispatch } from 'react-redux';
import { Link, useHistory, useParams } from 'react-router-dom';
import moment from 'moment';
import axios from 'axios';
import { CREATE_PAYHEAD_ASSIGNMENT } from 'app/constant/constants';
import { toast } from 'react-hot-toast';
import { removeSalary, saveSalary, updateSalary } from '../store/salarySlice';

const NewSalaryHeader = () => {
	const dispatch = useDispatch();
	const methods = useFormContext();
	const { formState, watch, getValues, setError } = methods;
	const { isValid, dirtyFields } = formState;
	const name = watch('name');
	const theme = useTheme();
	const history = useHistory();

	const routeParams = useParams();

	const handleDelete = localStorage.getItem('salaryEvent');

	const handleSaveSalary = async () => {
		const authTOKEN = {
			headers: {
				'Content-type': 'application/json',
				Authorization: localStorage.getItem('jwt_access_token')
			}
		};

		await axios
			.post(`${CREATE_PAYHEAD_ASSIGNMENT}`, getValues(), authTOKEN)
			.then(res => {
				if (res.data.response) {
					localStorage.setItem('salaryAlert', res.data.response);
					history.push('/apps/salary-management/salarys');
				}
			})
			.catch(err => {
				setError('payhead', {
					type: 'manual',
					message: err.response.data
				});
			});
		// const formData = getValues();
		// console.log('salary', getValues());

		// const today = moment();
		// formData.effective_from = formData.effective_from ? formData.effective_from : today.format('YYYY-MM-DD');
		// // const modifiedData = {
		// 	payhead_list: formData.payhead_list,
		// 	employees: formData.employees,
		// 	effective_from: formData.effective_from ? formData.effective_from : today.format('YYYY-MM-DD')
		// };
		// dispatch(saveSalary(getValues()))
		// 	.then(res => {
		//
		// 		if (res.payload) {
		// 			localStorage.setItem('salaryAlert', 'saveSalary');
		// 			history.push('/apps/salary-management/salarys');
		// 		}
		// 	})
		// 	.catch(err => console.log(';sdsdf', err));
	};

	function handleUpdateSalary() {
		dispatch(updateSalary(getValues())).then(res => {
			if (res.payload) {
				localStorage.setItem('salaryAlert', 'updateSalary');
				history.push('/apps/salary-management/salarys');
			}
		});
	}

	function handleRemoveSalary() {
		dispatch(removeSalary(getValues())).then(res => {
			if (res.payload) {
				localStorage.removeItem('salaryEvent');
				localStorage.setItem('salaryAlert', 'deleteSalary');
				history.push('/apps/salary-management/salarys');
			}
		});
	}

	function handleCancel() {
		history.push('/apps/salary-management/salarys');
	}

	return (
		<div className="flex flex-1 w-full items-center justify-between">
			<div className="flex flex-col items-start max-w-full min-w-0">
				<motion.div initial={{ x: 20, opacity: 0 }} animate={{ x: 0, opacity: 1, transition: { delay: 0.3 } }}>
					<Typography
						className="flex items-center sm:mb-2"
						component={Link}
						role="button"
						to="/apps/salary-management/salarys"
						color="inherit"
					>
						<Icon className="text-20">{theme.direction === 'ltr' ? 'arrow_back' : 'arrow_forward'}</Icon>
						<span className="hidden sm:flex mx-4 font-medium"> Payhead Assign Details</span>
					</Typography>
				</motion.div>

				<div className="flex items-center max-w-full">
					<motion.div
						className="hidden sm:flex"
						initial={{ scale: 0 }}
						animate={{ scale: 1, transition: { delay: 0.3 } }}
					/>

					<div className="flex flex-col min-w-0 mx-8 sm:mc-16">
						<motion.div initial={{ x: -20 }} animate={{ x: 0, transition: { delay: 0.3 } }}>
							<Typography className="text-16 sm:text-20 truncate font-semibold">
								{name || 'Create New Payhead Assign'}
							</Typography>
							<Typography variant="caption" className="font-medium">
								Payhead Assign Details
							</Typography>
						</motion.div>
					</div>
				</div>
			</div>
			<motion.div
				className="flex"
				initial={{ opacity: 0, x: 20 }}
				animate={{ opacity: 1, x: 0, transition: { delay: 0.3 } }}
			>
				{handleDelete === 'Delete' && (
					<Typography className="mt-6" variant="subtitle2">
						Do you want to remove this Payhead Assign?
					</Typography>
				)}
				{handleDelete === 'Delete' && routeParams.salaryId !== 'new' && (
					<Button
						className="whitespace-nowrap mx-4"
						variant="contained"
						color="secondary"
						onClick={handleRemoveSalary}
						startIcon={<Icon className="hidden sm:flex">delete</Icon>}
						style={{ backgroundColor: '#ea5b78', color: 'white' }}
					>
						Remove
					</Button>
				)}
				{routeParams.salaryId === 'new' && (
					<Button
						className="whitespace-nowrap mx-4"
						variant="contained"
						color="secondary"
						disabled={_.isEmpty(dirtyFields) || !isValid}
						onClick={handleSaveSalary}
					>
						Save
					</Button>
				)}
				{handleDelete !== 'Delete' && routeParams?.salaryName && (
					<Button
						className="whitespace-nowrap mx-4"
						color="secondary"
						variant="contained"
						style={{ backgroundColor: '#4dc08e', color: 'white' }}
						onClick={handleUpdateSalary}
					>
						Update
					</Button>
				)}
				<Button
					className="whitespace-nowrap mx-4"
					variant="contained"
					style={{ backgroundColor: '#FFAA4C', color: 'white' }}
					onClick={handleCancel}
				>
					Cancel
				</Button>
			</motion.div>
		</div>
	);
};

export default NewSalaryHeader;
