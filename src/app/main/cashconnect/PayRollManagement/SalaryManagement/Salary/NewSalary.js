import FusePageCarded from '@fuse/core/FusePageCarded';
import { useDeepCompareEffect } from '@fuse/hooks';
import { yupResolver } from '@hookform/resolvers/yup';
import { Button, Typography } from '@material-ui/core';
import withReducer from 'app/store/withReducer';
import { motion } from 'framer-motion';
import React, { useEffect, useState } from 'react';
import { FormProvider, useForm } from 'react-hook-form';
import { useDispatch, useSelector } from 'react-redux';
import { Link, useParams } from 'react-router-dom';
import * as yup from 'yup';
import setIdIfValueIsObjArryData from 'app/@helpers/setIdIfValueIsObjArryData';
import setIdIfValueIsObject2 from 'app/@helpers/setIdIfValueIsObject2';
import reducer from '../store/index';
import { getSalary, newSalary, resetSalary } from '../store/salarySlice';
import NewSalaryHeader from './NewSalaryHeader';
import SalaryForm from './SalaryForm';

/**
 * Form Validation Schema
 */
const schema = yup.object().shape({
	// payheads: yup.array().of(yup.number().required('payhead must be a number')).required('payheads is required')
});

const Salary = () => {
	const dispatch = useDispatch();
	const salary = useSelector(({ salarysManagement }) => salarysManagement.salary);

	const [noSalary, setNoSalary] = useState(false);
	const methods = useForm({
		mode: 'onChange',
		defaultValues: {},
		resolver: yupResolver(schema)
	});
	const routeParams = useParams();
	const [letFormSave, setLetFormSave] = useState(false);

	const { reset } = methods;

	useDeepCompareEffect(() => {
		function updateSalaryState() {
			const { salaryId } = routeParams;

			if (salaryId === 'new') {
				localStorage.removeItem('event');
				/**
				 * Create New User data
				 */
				dispatch(newSalary());
			} else {
				/**
				 * Get User data
				 */

				dispatch(getSalary(salaryId)).then(action => {
					/**
					 * If the requested product is not exist show message
					 */
					if (!action.payload) {
						setNoSalary(true);
					}
				});
			}
		}

		updateSalaryState();
	}, [dispatch, routeParams]);

	useEffect(() => {}, []);

	useEffect(() => {
		if (!salary) {
			return;
		}

		/**
		 * Reset the form on salary state changes
		 */
		// const convertedPyaheadItems = setIdIfValueIsObjArryData(salary?.payhead_list);
		// const convertedPyaheadEmployee = setIdIfValueIsObjArryData(salary?.employees);
		// const convertedPyahead = setIdIfValueIsObject2(salary);
		reset({
			...salary,
			calculation_for: salary?.payhead_assignments?.calculation_for,
			date: salary?.payhead_assignments?.date,
			payhead: salary?.payheads,
			employee: salary?.employees,
			department: salary?.payhead_assignments?.department
		});
	}, [salary, reset]);

	useEffect(() => {
		return () => {
			/**
			 * Reset Salary on component unload
			 */
			dispatch(resetSalary());
			setNoSalary(false);
		};
	}, [dispatch]);

	/**
	 * Show Message if the requested products is not exists
	 */
	if (noSalary) {
		return (
			<motion.div
				initial={{ opacity: 0 }}
				animate={{ opacity: 1, transition: { delay: 0.1 } }}
				className="flex flex-col flex-1 items-center justify-center h-full"
			>
				<Typography color="textSecondary" variant="h5">
					There is no such salary!
				</Typography>
				<Button
					className="mt-24"
					component={Link}
					variant="outlined"
					to="/apps/e-commerce/products"
					color="inherit"
				>
					Go to Payhead Assign Page
				</Button>
			</motion.div>
		);
	}

	return (
		<FormProvider {...methods}>
			<FusePageCarded
				classes={{
					toolbar: 'p-0',
					header: 'min-h-80 h-80'
				}}
				header={<NewSalaryHeader letFormSave={letFormSave} />}
				content={
					<div className="p-16 sm:p-24">
						<SalaryForm setLetFormSave={setLetFormSave} />
					</div>
				}
				innerScroll
			/>
		</FormProvider>
	);
};
export default withReducer('salarysManagement', reducer)(Salary);
