import { createAsyncThunk, createSlice } from '@reduxjs/toolkit';
import { GET_PYAHEADID } from 'app/constant/constants';
import axios from 'axios';

export const getPayheadById = createAsyncThunk('salaryManagement/payheadsList/getPayheadById', async params => {
	const authTOKEN = {
		headers: {
			'Content-type': 'application/json',
			Authorization: localStorage.getItem('jwt_access_token')
		}
	};
	const response = await axios.get(`${GET_PYAHEADID}${params}`, authTOKEN);
	const data = await response.data;
	return data === undefined ? null : data;
});

export const removePayheadById = payheadId => {
	return { type: 'salaryManagement/payheadsList/removePayheadById', payload: payheadId };
};

const salaryDetailsSlice = createSlice({
	name: 'salaryManagement/payheadsList',
	initialState: null,
	reducers: {
		resetSchedule: () => null,
		newSchedule: {
			reducer: (state, action) => action.payload,
			prepare: event => ({
				payload: {
					calculation_type: '',
					payhead_type: ''
				}
			})
		}
	},
	extraReducers: {
		[getPayheadById.fulfilled]: (state, action) => action.payload,
		removePayheadById: (state, action) => {
			const payheadIdToRemove = action.payload;
			return state.filter(payhead => payhead.id !== payheadIdToRemove);
		}
	}
});

export const { newSchedule, resetSchedule } = salaryDetailsSlice.actions;

export default salaryDetailsSlice.reducer;
