import { createAsyncThunk, createSlice } from '@reduxjs/toolkit';

import {
	CREATE_PAYHEAD_ASSIGNMENT,
	DELETE_PAYHEAD_ASSIGNMENT,
	GET_PAYHEAD_ASSIGNMENTID,
	UPDATE_PAYHEAD_ASSIGNMENT
} from 'app/constant/constants';
import axios from 'axios';
import { toast } from 'react-hot-toast';
// import { toast } from 'react-toastify';

export const getSalary = createAsyncThunk('salaryManagement/salary/getSalary', async (params, { rejectWithValue }) => {
	const authTOKEN = {
		headers: {
			'Content-type': 'application/json',
			Authorization: localStorage.getItem('jwt_access_token')
		}
	};

	try {
		const response = await axios.get(`${GET_PAYHEAD_ASSIGNMENTID}${params}`, authTOKEN);
		const data = await response.data;
		return data === undefined ? null : data;
	} catch (err) {
		return rejectWithValue(params);
	}
});

export const removeSalary = createAsyncThunk('salaryManagement/salary/removeSalary', async val => {
	const authTOKEN = {
		headers: {
			'Content-type': 'application/json',
			Authorization: localStorage.getItem('jwt_access_token')
		}
	};

	const salaryId = val.payhead_assignments?.id;
	const response = await axios.delete(`${DELETE_PAYHEAD_ASSIGNMENT}${salaryId}`, authTOKEN);
	return response;
});

export const updateSalary = createAsyncThunk(
	'salaryManagement/salary/updateSalary',
	async (salaryData, { dispatch, getState }) => {
		const { salary } = getState().salarysManagement;

		const authTOKEN = {
			headers: {
				'Content-type': 'application/json',
				Authorization: localStorage.getItem('jwt_access_token')
			}
		};
		const response = await axios.put(
			`${UPDATE_PAYHEAD_ASSIGNMENT}${salary.payhead_assignments?.id}`,
			salaryData,
			authTOKEN
		);
		return response;
	}
);

export const saveSalary = createAsyncThunk('salaryManagement/salary/saveSalary', async salaryData => {
	const authTOKEN = {
		headers: {
			'Content-type': 'application/json',
			Authorization: localStorage.getItem('jwt_access_token')
		}
	};

	const response = await axios.post(`${CREATE_PAYHEAD_ASSIGNMENT}`, salaryData, authTOKEN).catch(err => {
		toast.error('err?.response?.data', {
			duration: 3000
		});
	});
	return response;
});

// export const saveSalary = createAsyncThunk('salaryManagement/salary/saveSalary', async salaryData => {
// 	const authTOKEN = {
// 		headers: {
// 			'Content-type': 'application/json',
// 			Authorization: localStorage.getItem('jwt_access_token')
// 		}
// 	};

// 	try {
// 		const response = await axios.post(`${CREATE_PAYHEAD_ASSIGNMENT}`, salaryData, authTOKEN);
// 		return response;
// 	} catch (error) {
// 		if (error.response && error.response.data && error.response.data) {
// 			// Dispatch an action to set the error message in the slice
// 			const errorMessage = error.response.data.response; // Adjust this based on the structure of your error response
// 			return { error: errorMessage };
// 		} else {
// 			// Handle other types of errors here if needed
//
// 			return { error: 'An error occurred while saving the salary.' };
// 		}
// 	}
// });

const salarySlice = createSlice({
	name: 'salaryManagement/salary',
	initialState: null,
	reducers: {
		resetSalary: () => null,
		newSalary: {
			reducer: (state, action) => action.payload,
			prepare: event => ({
				payload: {
					// payhead_list: [
					// 	{
					// 		payhead: '',
					// 		rate: 0.0,
					// 		unit: ''
					// 	}
					// ],
					calculation_for: '',
					department: [],
					employee: [],
					payhead: []
					// effective_from: moment().format('YYYY-MM-DD')
				}
			})
		}
	},
	extraReducers: {
		[getSalary.fulfilled]: (state, action) => action.payload,
		[saveSalary.fulfilled]: (state, action) => {
			action.payhead;
		},
		// [saveSalary.rejected]: (state, action) => {
		//
		// },
		[removeSalary.fulfilled]: (state, action) => action.payload,
		[updateSalary.fulfilled]: (state, action) => action.payload
	}
});

export const { newSalary, resetSalary } = salarySlice.actions;

export default salarySlice.reducer;
