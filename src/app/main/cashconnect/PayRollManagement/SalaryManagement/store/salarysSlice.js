import { createAsyncThunk, createEntityAdapter, createSlice } from '@reduxjs/toolkit';
import { DELETE_PAYHEAD_ASSIGNMENT, GET_PAYHEAD_ASSIGNMENTS } from 'app/constant/constants';
import axios from 'axios';

export const getSalarys = createAsyncThunk('salaryManagement/salarys/getSalarys', async parameter => {
	const { page, size } = parameter;
	axios.defaults.headers.common['Content-type'] = 'application/json';
	axios.defaults.headers.common.Authorization = localStorage.getItem('jwt_access_token');

	const response = axios.get(GET_PAYHEAD_ASSIGNMENTS, { params: { page, size } });
	const data = await response;

	sessionStorage.setItem('salarys_total_elements', data.data.total_elements);
	sessionStorage.setItem('salarys_total_pages', data.data.total_pages);
	delete axios.defaults.headers.common['Content-type'];
	delete axios.defaults.headers.common.Authorization;

	return data.data.payhead_assignments;
});

export const removeSalarys = createAsyncThunk(
	'salaryManagement/salarys/removeSalarys',
	async (salaryIds, { dispatch, getState }) => {
		const authTOKEN = {
			headers: {
				'Content-type': 'application/json',
				Authorization: localStorage.getItem('jwt_access_token')
			}
		};
		await axios.delete(`${DELETE_PAYHEAD_ASSIGNMENT}`, { salaryIds }, authTOKEN);

		return salaryIds;
	}
);

const salarysAdapter = createEntityAdapter({});

export const { selectAll: selectSalarys, selectById: selectSalaryById } = salarysAdapter.getSelectors(
	state => state.salarysManagement.salarys
);

const salarysSlice = createSlice({
	name: 'salaryManagement/salarys',
	initialState: salarysAdapter.getInitialState({
		searchText: ''
	}),
	reducers: {
		setSalarysSearchText: {
			reducer: (state, action) => {
				state.searchText = action.payload;
			},
			prepare: event => ({ payload: event.target.value || '' })
		}
	},
	extraReducers: {
		[getSalarys.fulfilled]: salarysAdapter.setAll
	}
});

export const { setData, setSalarysSearchText } = salarysSlice.actions;
export default salarysSlice.reducer;
