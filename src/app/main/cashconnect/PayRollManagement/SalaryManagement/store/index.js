import { combineReducers } from '@reduxjs/toolkit';
import salary from './salarySlice';
import salarys from './salarysSlice';
import payheadsList from './salaryDetailsSlice';

const reducer = combineReducers({
	salary,
	salarys,
	payheadsList
});

export default reducer;
