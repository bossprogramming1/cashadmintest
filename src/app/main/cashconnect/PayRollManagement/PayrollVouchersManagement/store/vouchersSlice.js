import { createAsyncThunk, createEntityAdapter, createSlice } from '@reduxjs/toolkit';
import { DELETE_PAYROLL_VOUCHER, GET_PAYROLL_VOUCHERS } from 'app/constant/constants';
import axios from 'axios';

export const getVouchers = createAsyncThunk('voucherManagement/vouchers/getVouchers', async parameter => {
	const { page, size } = parameter;
	axios.defaults.headers.common['Content-type'] = 'application/json';
	axios.defaults.headers.common.Authorization = localStorage.getItem('jwt_access_token');

	const response = axios.get(GET_PAYROLL_VOUCHERS, { params: { page, size } });
	const data = await response;

	sessionStorage.setItem('vouchers_total_elements', data.data.total_elements);
	sessionStorage.setItem('vouchers_total_pages', data.data.total_pages);
	delete axios.defaults.headers.common['Content-type'];
	delete axios.defaults.headers.common.Authorization;

	return data.data.payroll_vouchers;
});

export const removeVouchers = createAsyncThunk(
	'voucherManagement/vouchers/removeVouchers',
	async (voucherIds, { dispatch, getState }) => {
		const authTOKEN = {
			headers: {
				'Content-type': 'application/json',
				Authorization: localStorage.getItem('jwt_access_token')
			}
		};
		await axios.delete(`${DELETE_PAYROLL_VOUCHER}`, { voucherIds }, authTOKEN);

		return voucherIds;
	}
);

const vouchersAdapter = createEntityAdapter({});

export const { selectAll: selectVouchers, selectById: selectVoucherById } = vouchersAdapter.getSelectors(
	state => state.vouchersManagement.vouchers
);

const vouchersSlice = createSlice({
	name: 'voucherManagement/vouchers',
	initialState: vouchersAdapter.getInitialState({
		searchText: ''
	}),
	reducers: {
		setVouchersSearchText: {
			reducer: (state, action) => {
				state.searchText = action.payload;
			},
			prepare: event => ({ payload: event.target.value || '' })
		}
	},
	extraReducers: {
		[getVouchers.fulfilled]: vouchersAdapter.setAll
	}
});

export const { setData, setVouchersSearchText } = vouchersSlice.actions;
export default vouchersSlice.reducer;
