import { combineReducers } from '@reduxjs/toolkit';
import voucher from './voucherSlice';
import vouchers from './vouchersSlice';

const reducer = combineReducers({
	voucher,
	vouchers
});

export default reducer;
