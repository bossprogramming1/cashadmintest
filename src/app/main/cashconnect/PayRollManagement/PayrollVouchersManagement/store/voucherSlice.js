import { createAsyncThunk, createSlice } from '@reduxjs/toolkit';
import {
	CREATE_PAYROLL_VOUCHER,
	DELETE_PAYROLL_VOUCHER,
	GET_PAYROLL_VOUCHERID,
	UPDATE_PAYROLL_VOUCHER
} from 'app/constant/constants';
import axios from 'axios';

export const getVoucher = createAsyncThunk(
	'voucherManagement/voucher/getVoucher',
	async (params, { rejectWithValue }) => {
		const authTOKEN = {
			headers: {
				'Content-type': 'application/json',
				Authorization: localStorage.getItem('jwt_access_token')
			}
		};

		try {
			const response = await axios.get(`${GET_PAYROLL_VOUCHERID}${params}`, authTOKEN);
			const data = await response.data;
			return data === undefined ? null : data;
		} catch (err) {
			return rejectWithValue(params);
		}
	}
);

export const removeVoucher = createAsyncThunk('voucherManagement/voucher/removeVoucher', async val => {
	const authTOKEN = {
		headers: {
			'Content-type': 'application/json',
			Authorization: localStorage.getItem('jwt_access_token')
		}
	};

	const response = await axios.delete(`${DELETE_PAYROLL_VOUCHER}${val}`, authTOKEN);
	return response;
});

export const updateVoucher = createAsyncThunk(
	'voucherManagement/voucher/updateVoucher',
	async (voucherData, { dispatch, getState }) => {
		const authTOKEN = {
			headers: {
				'Content-type': 'application/json',
				Authorization: localStorage.getItem('jwt_access_token')
			}
		};
		const response = await axios.put(`${UPDATE_PAYROLL_VOUCHER}${voucherData.id}`, voucherData, authTOKEN);

		return response;
	}
);

export const saveVoucher = createAsyncThunk('voucherManagement/voucher/saveVoucher', async voucherData => {
	const authTOKEN = {
		headers: {
			'Content-type': 'application/json',
			Authorization: localStorage.getItem('jwt_access_token')
		}
	};
	const response = await axios.post(`${CREATE_PAYROLL_VOUCHER}`, voucherData, authTOKEN);
	console.log('response', response);
	return response;
});

const voucherSlice = createSlice({
	name: 'voucherManagement/voucher',
	initialState: null,
	reducers: {
		resetVoucher: () => null,
		newVoucher: {
			reducer: (state, action) => action.payload,
			prepare: event => ({
				payload: {
					name: '',
					voucher_date: ''
				}
			})
		}
	},
	extraReducers: {
		[getVoucher.fulfilled]: (state, action) => action.payload,
		[saveVoucher.fulfilled]: (state, action) => action.payload,
		[removeVoucher.fulfilled]: (state, action) => action.payload,
		[updateVoucher.fulfilled]: (state, action) => action.payload
	}
});

export const { newVoucher, resetVoucher } = voucherSlice.actions;

export default voucherSlice.reducer;
