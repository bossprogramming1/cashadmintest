import _ from '@lodash';
import FuseScrollbars from '@fuse/core/FuseScrollbars';
import { Box, Button, Grid, Paper, TableContainer, TableHead, TextField, Typography } from '@material-ui/core';
import Checkbox from '@material-ui/core/Checkbox';
import { makeStyles } from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TablePagination from '@material-ui/core/TablePagination';
import TableRow from '@material-ui/core/TableRow';
import FormControl from '@material-ui/core/FormControl';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Radio from '@material-ui/core/Radio';
import RadioGroup from '@material-ui/core/RadioGroup';
import { Autocomplete } from '@material-ui/lab';
import React, { useEffect, useState } from 'react';
import { Controller, useFormContext } from 'react-hook-form';
import { useDispatch, useSelector } from 'react-redux';
import { useParams } from 'react-router-dom';
import { getDepartments, getEmployees, getPayrollVoucherClass } from 'app/store/dataSlice';
import axios from 'axios';
import CustomDatePicker from 'app/@components/CustomDatePicker';
import { GET_PAYROLL_VOUCHER_GENERATE } from 'app/constant/constants';
import { addNotification } from 'app/fuse-layouts/shared-components/notificationPanel/store/dataSlice';
import NotificationModel from 'app/fuse-layouts/shared-components/notificationPanel/model/NotificationModel';
import FuseLoading from '@fuse/core/FuseLoading';
import { getPayrollMakeStyles } from '../../PayRollUtils/payrollMakeStyles';
import { saveVoucher, updateVoucher } from '../store/voucherSlice';

const useStyles = makeStyles(theme => ({
	...getPayrollMakeStyles(theme),

	root: {
		display: 'flex',
		justifyContent: 'space-between',
		flexWrap: 'nowrap',
		overflow: 'auto',
		minHeight: '35px'
	},
	toolbar: {
		'& > div': {
			minHeight: 'fit-content'
		}
	},
	box: {
		background: '#fff',
		border: '1px solid',
		borderColor: 'grey',
		borderRadius: 2,
		fontSize: '0.875rem',
		fontWeight: '700',
		width: '50%',
		padding: '20px',
		height: 'fit-content'
	},
	tableBox: {
		background: '#fff',
		border: '1px solid',
		borderColor: 'grey',
		borderRadius: 2,
		fontSize: '0.875rem',
		fontWeight: '700',
		padding: '20px',
		height: 'fit-content',
		margin: '20px'
	},

	itemHead: {
		display: 'flex',
		flexDirection: 'column',
		justifyContent: 'center',
		alignItems: 'center'
	}
}));

function VoucherForm(props) {
	const classes = useStyles(props);
	const methods = useFormContext();
	const { control, formState, getValues, setValue, watch } = methods;
	const { errors, isValid, dirtyFields } = formState;
	const routeParams = useParams();
	const { voucherId } = routeParams;
	const handleDelete = localStorage.getItem('voucherEvent');
	const [getVoucher, setGetVoucher] = useState([]);
	const employees = useSelector(state => state.data.employees);
	const departments = useSelector(state => state.data?.departments);
	const calCulationFor = watch('calculation_for');
	const salaryLists = watch('salary_list');
	const [selectedRadio, setSelectedRadio] = useState('');
	const [fatchTrue, setFatchTrue] = useState(false);
	const [loading, setLoading] = useState(null);

	const dispatch = useDispatch();

	useEffect(() => {
		dispatch(getPayrollVoucherClass());
		dispatch(getEmployees());
		dispatch(getDepartments());
	}, [dispatch]);
	useEffect(() => {
		if (voucherId !== 'new' && calCulationFor && salaryLists && !fatchTrue) {
			setSelectedRadio(calCulationFor);
			const modifiedData = Object.entries(salaryLists ? salaryLists : {}).map(([salaryKey, salaryValue]) => ({
				...salaryValue
			}));
			setGetVoucher(modifiedData);
			setFatchTrue(true);

			// console.log('getValues', getValues(), modifiedData);
		}
	}, [voucherId !== 'new', calCulationFor, salaryLists, fatchTrue]);

	function handleSavePayrollVoucher() {
		setLoading(true);
		axios
			.post(`${GET_PAYROLL_VOUCHER_GENERATE}`, getValues(), {
				headers: {
					'Content-type': 'application/json',
					Authorization: localStorage.getItem('jwt_access_token')
				}
			})
			.then(res => {
				setLoading(false);

				setGetVoucher(res.data.salary_list);
				const data = getValues();
				data.salary_list = res.data.salary_list || [];
				setValue('salary_list', res.data.salary_list || []);
				const total = res.data.salary_list.reduce((total, item) => total + item.net_salary, 0);
				setValue('total_amount', total || 0.0);
			})
			.catch(err => {
				console.log('err', err.response);
				setLoading(false);

				dispatch(
					addNotification(
						NotificationModel({
							message: `${err?.response?.data}`,
							options: { variant: 'error' }
							// item_id: 1
						})
					)
				);
			});

		// dispatch(getVoucher(values)).then(data => {
		//
	}

	console.log('getVoucher', getVoucher);
	return (
		<div className="">
			<FuseScrollbars className="flex-grow overflow-x-auto">
				<Box className="w-full flex justify-center my-6">
					<Box className={classes.box}>
						<Typography className="flex justify-center" variant="h5">
							Payroll Autofill
						</Typography>
						<Grid container spacing={2}>
							<Grid item xs={4} className={classes.itemHead}>
								<Typography>Name</Typography>
							</Grid>
							<Grid item xs={8}>
								<Controller
									name="name"
									control={control}
									render={({ field }) => {
										return (
											<TextField
												{...field}
												className="mt-8 mb-16"
												error={!!errors?.name}
												helperText={errors?.name?.message}
												label="Voucher Name"
												id="name"
												required
												variant="outlined"
												InputLabelProps={field?.value && { shrink: true }}
												fullWidth
											/>
										);
									}}
								/>
							</Grid>
							{/* <Grid item xs={4} className={classes.itemHead}>
								<Typography>From (blank for beginning)</Typography>
							</Grid>
							<Grid item xs={8}>
								<Controller
									className="p-0"
									name={`date_from`}
									control={control}
									// rules={{
									// 	validate: value => {
									// 		const selectedDate = moment(value);
									// 		if (selectedDate.date() !== 1) {
									// 			return 'Please select the first date of the month.';
									// 		}
									// 		return true;
									// 	}
									// }}
									render={({ field }) => {
										return (
											<CustomDatePicker
												className="p-0"
												// previous_date_disable={true}
												field={field}
												style={{ maxWidth: '130px' }}
												startOfDate={true}
												views={['month']}
												label="Date From"
												error={!!errors?.date_from} // Check if there's an error for the "date_from" field
												helperText={errors?.date_from?.message || ''} // Display the error message if available
											/>
										);
									}}
								/>
							</Grid> */}
							{/* <Grid item xs={4} className={classes.itemHead}>
								<Typography>To (blank for end)</Typography>
							</Grid>
							<Grid item xs={8}>
								<Controller
									className="p-0"
									name={`date_to`}
									control={control}
									// rules={{
									// 	validate: value => {
									// 		const selectedDate = moment(value);
									// 		if (selectedDate.date() !== 1) {
									// 			return 'Please select the first date of the month.';
									// 		}
									// 		return true;
									// 	}
									// }}
									render={({ field }) => {
										return (
											<CustomDatePicker
												className="p-0"
												// previous_date_disable={true}
												field={field}
												style={{ maxWidth: '130px' }}
												views={['month']}
												startOfDate={false}
												label="Dtae To"
												error={!!errors?.date_to} // Check if there's an error for the "date_to" field
												helperText={errors?.date_to?.message || ''}
											/>
										);
									}}
								/>
							</Grid> */}
							<Grid item xs={4} className={classes.itemHead}>
								<Typography>Select Month</Typography>
							</Grid>
							<Grid item xs={8}>
								<Controller
									className="p-0"
									name={`voucher_date`}
									control={control}
									render={({ field }) => {
										console.log('month_value', field);
										return (
											<CustomDatePicker
												className="p-0"
												// previous_date_disable={true}
												field={field}
												// style={{ maxWidth: '130px' }}
												views={['month']}
												voucherOfDate={true}
												label="Voucher Dtae"
												error={!!errors?.voucher_date} // Check if there's an error for the "date_to" field
												helperText={errors?.voucher_date?.message || ''}
											/>
										);
									}}
								/>
							</Grid>
							{/* <Grid item xs={4} className={classes.itemHead}>
								<Typography>Class</Typography>
							</Grid>
							<Grid item xs={8}>
								<Typography>Payroll</Typography>
							</Grid> */}
							<Grid item xs={4} className={classes.itemHead}>
								<Typography>Process Assign</Typography>
							</Grid>
							<Grid item xs={8}>
								<Controller
									name="calculation_for"
									control={control}
									render={({ field }) => (
										<FormControl component="fieldset">
											<RadioGroup
												row
												aria-label="position"
												name="position"
												defaultValue="top"
												onChange={event => {
													if (event.target.value == 'all') {
														setSelectedRadio(event.target.value);
														// setSelectedValues('All');
														// setSalaryTable(true);
														handleSavePayrollVoucher();

														setValue('department', []);
														setValue('employee', []);
													} else if (event.target.value == 'department') {
														setSelectedRadio(event.target.value);

														setValue('employee', []);
														// setSelectedValues('');
													} else if (event.target.value == 'employees') {
														setSelectedRadio(event.target.value);

														setValue('department', []);
														// setSelectedValues('');
													} else {
														setSelectedRadio(event.target.value);
														// setSelectedValues('');
													}
												}}
											>
												<FormControlLabel
													{...field}
													value="all"
													control={
														<Radio
															checked={field.value === 'all' ? field.value : false}
															style={{ color: '#22d3ee' }}
														/>
													}
													label="All"
												/>
												<FormControlLabel
													{...field}
													value="department"
													control={
														<Radio
															checked={field.value === 'department' ? field.value : false}
															style={{ color: 'green' }}
														/>
													}
													label="Department"
												/>
												<FormControlLabel
													{...field}
													value="employees"
													control={
														<Radio
															checked={field.value === 'employees' ? field.value : false}
															style={{ color: 'red' }}
														/>
													}
													label="Employees"
												/>
											</RadioGroup>
										</FormControl>
									)}
								/>
							</Grid>
							<Grid item xs={4} className={classes.itemHead}>
								<Typography>Process for</Typography>
							</Grid>
							<Grid item xs={8}>
								{selectedRadio === 'department' && (
									<Controller
										name="department"
										control={control}
										render={({ field: { onChange, value, name } }) => (
											<Autocomplete
												className="mt-8 mb-16"
												freeSolo
												multiple
												filterSelectedOptions
												value={value ? departments.filter(data => value.includes(data.id)) : []}
												options={departments}
												getOptionLabel={option => `${option?.name}`}
												onChange={(event, newValue) => {
													const selectedValues = newValue.map(option => option.id);
													onChange(selectedValues);
													handleSavePayrollVoucher();
												}}
												renderInput={params => {
													return (
														<TextField
															{...params}
															placeholder="Select departments"
															label="Departments"
															error={!!errors?.department}
															required
															autoFocus
															helperText={errors?.department?.message}
															variant="outlined"
															InputLabelProps={{
																shrink: true
															}}
														/>
													);
												}}
											/>
										)}
									/>
								)}
								{selectedRadio === 'employees' && (
									<Controller
										name="employee"
										control={control}
										render={({ field: { onChange, value, name } }) => (
											<Autocomplete
												className="mt-8 mb-16"
												freeSolo
												multiple
												filterSelectedOptions
												value={value ? employees.filter(data => value.includes(data.id)) : []}
												options={employees}
												getOptionLabel={option => `${option?.first_name} ${option?.last_name}`}
												onChange={(event, newValue) => {
													const selectedValues = newValue.map(option => option.id);
													onChange(selectedValues);
													handleSavePayrollVoucher();
												}}
												renderInput={params => {
													return (
														<TextField
															{...params}
															placeholder="Select Employee"
															label="Employee"
															error={!!errors?.employee}
															required
															autoFocus
															helperText={errors?.employee?.message}
															variant="outlined"
															InputLabelProps={{
																shrink: true
															}}
														/>
													);
												}}
											/>
										)}
									/>
								)}
								{selectedRadio === 'all' && <Typography>All Emloyees</Typography>}
							</Grid>
						</Grid>
					</Box>
				</Box>

				{getVoucher.length !== 0 && (
					<>
						<Box
							style={{
								margin: '0 50px 50px 50px',
								border: '2px solid #1b2330',
								height: 'fit-content',
								display: 'flex',
								// className={classes.mainContainer}
								padding: '10px',
								alignItems: 'flex-start',
								borderRadius: '5px',
								justifyContent: 'space-between'
							}}
						>
							<TableContainer component={Paper} className={classes.tblContainer}>
								<Table className={`${classes.table}`} aria-label="simple table">
									<TableHead className={classes.tableHead}>
										<TableRow hover style={{ fontSize: '14px' }}>
											<TableCell className={classes.tableCell} style={{ fontSize: '14px' }}>
												<Typography className="text-14 font-medium">Employee Name</Typography>
											</TableCell>
											<TableCell style={{ fontSize: '14px' }} className={classes.tableCell}>
												<Typography className="text-14 font-medium">Payhead </Typography>
											</TableCell>
											<TableCell style={{ fontSize: '14px' }} className={classes.tableCell}>
												<Typography className="text-14 font-medium">Amount</Typography>
											</TableCell>
											<TableCell style={{ fontSize: '14px' }} className={classes.tableCell}>
												<Typography className="text-14 font-medium">Total</Typography>
											</TableCell>
										</TableRow>
									</TableHead>
									<TableBody>
										{getVoucher.map(item => {
											return (
												<React.Fragment key={item.employee_name}>
													{item.payheads.map((e, index) => {
														const isLastRow = index === item.payheads.length - 1;

														return (
															<TableRow hover key={e.payhead}>
																{index === 0 && (
																	<TableCell
																		rowSpan={item.payheads.length}
																		className={classes.tableCellInBody}
																		align="center"
																	>
																		{item.employee_name}
																	</TableCell>
																)}
																<TableCell
																	className={`text-12 font-medium p-5 ${
																		isLastRow ? classes.lastRow : null
																	}`}
																>
																	{e.payhead_name}
																</TableCell>
																<TableCell
																	className={`text-12 font-medium p-5 ${
																		isLastRow ? classes.lastRow : null
																	}`}
																>
																	{`${e.payhead_amount} ${e.transaction_type} `}
																</TableCell>
																{index === 0 && (
																	<TableCell
																		rowSpan={item.payheads.length}
																		className={classes.tableCellInBody}
																		align="center"
																	>
																		{/* {item.payheads.reduce(
																			(total, item) =>
																				total + item.payhead_amount,
																			0
																		)} */}
																		{item?.net_salary}
																	</TableCell>
																)}
															</TableRow>
														);
													})}
												</React.Fragment>
											);
										})}

										{/* Total Row */}
										<TableRow hover>
											<TableCell className={classes.tableCellInBody} />
											<TableCell className={classes.tableCellInBody} />
											<TableCell className={classes.tableCellInBody}>
												<Typography className={classes.tableCellInBody}>Total</Typography>
											</TableCell>
											<TableCell className={classes.tableCellInBody} align="center">
												<Typography className={classes.tableCellInBody}>
													{/* {voucherDemoData.reduce(
												(total, item) => total + item.totalPayheadAmount,
												0
											)} */}

													{getVoucher.reduce((total, item) => total + item.net_salary, 0)}
												</Typography>
											</TableCell>
										</TableRow>
									</TableBody>
								</Table>
							</TableContainer>
						</Box>
					</>
				)}
				{loading && <FuseLoading />}
			</FuseScrollbars>
		</div>
	);
}

export default VoucherForm;
