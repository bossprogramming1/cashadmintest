import FusePageCarded from '@fuse/core/FusePageCarded';
import { useDeepCompareEffect } from '@fuse/hooks';
import { yupResolver } from '@hookform/resolvers/yup';
import { Button, Typography } from '@material-ui/core';
import withReducer from 'app/store/withReducer';
import { motion } from 'framer-motion';
import React, { useEffect, useState } from 'react';
import { FormProvider, useForm } from 'react-hook-form';
import { useDispatch, useSelector } from 'react-redux';
import { Link, useParams } from 'react-router-dom';
import * as yup from 'yup';
import reducer from '../store/index';
import { getVoucher, newVoucher, resetVoucher } from '../store/voucherSlice';
import VoucherForm from './VoucherForm';
import NewVoucherHeader from './NewVoucherHeader';

/**
 * Form Validation Schema
 */
const schema = yup.object().shape({
	name: yup.string().required('Name is required'),
	voucher_date: yup.string().required('voucher date is required')
});

const Voucher = () => {
	const dispatch = useDispatch();
	const voucher = useSelector(({ vouchersManagement }) => vouchersManagement.voucher);

	const [noVoucher, setNoVoucher] = useState(false);
	const methods = useForm({
		mode: 'onChange',
		defaultValues: {},
		resolver: yupResolver(schema)
	});
	const routeParams = useParams();

	const { reset } = methods;

	useDeepCompareEffect(() => {
		function updateVoucherState() {
			const { voucherId } = routeParams;

			if (voucherId === 'new') {
				localStorage.removeItem('event');
				/**
				 * Create New User data
				 */
				dispatch(newVoucher());
			} else {
				/**
				 * Get User data
				 */

				dispatch(getVoucher(voucherId)).then(action => {
					/**
					 * If the requested product is not exist show message
					 */
					if (!action.payload) {
						setNoVoucher(true);
					}
				});
			}
		}

		updateVoucherState();
	}, [dispatch, routeParams]);

	useEffect(() => {}, []);

	useEffect(() => {
		if (!voucher) {
			// If voucher is undefined, reset the form with default values
			reset({
				calculation_for: '',
				name: '',
				date_from: '',
				date_to: '',
				employee: [],
				department: []
			});
		} else {
			// If voucher is defined, populate the form with voucher data
			reset({
				...voucher,
				calculation_for: voucher?.calculation_for,
				name: voucher?.name,
				voucher_date: voucher?.date,
				// employee: voucher?.calculation_for === 'employees' ? voucher?.employees : [],
				department: voucher?.department || []
			});
		}
	}, [voucher, reset]);

	useEffect(() => {
		return () => {
			/**
			 * Reset Voucher on component unload
			 */

			dispatch(resetVoucher());
			setNoVoucher(false);
		};
	}, [dispatch]);

	/**
	 * Show Message if the requested products is not exists
	 */
	if (noVoucher) {
		return (
			<motion.div
				initial={{ opacity: 0 }}
				animate={{ opacity: 1, transition: { delay: 0.1 } }}
				className="flex flex-col flex-1 items-center justify-center h-full"
			>
				<Typography color="textSecondary" variant="h5">
					There is no such Payroll Voucher!
				</Typography>
				<Button
					className="mt-24"
					component={Link}
					variant="outlined"
					to="/apps/e-commerce/products"
					color="inherit"
				>
					Go to Payroll Voucher Page
				</Button>
			</motion.div>
		);
	}

	return (
		<FormProvider {...methods}>
			<FusePageCarded
				classes={{
					content: 'flex',
					contentCard: 'overflow-hidden',
					header: 'min-h-74 h-64'
				}}
				header={<NewVoucherHeader />}
				content={<VoucherForm />}
				innerScroll
			/>
		</FormProvider>
	);
};
export default withReducer('vouchersManagement', reducer)(Voucher);
