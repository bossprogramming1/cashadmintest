import _ from '@lodash';
import Button from '@material-ui/core/Button';
import Icon from '@material-ui/core/Icon';
import { useTheme } from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';
import { motion } from 'framer-motion';
import React from 'react';
import { useFormContext } from 'react-hook-form';
import { useDispatch } from 'react-redux';
import { useHistory, useParams } from 'react-router-dom';
import { removeVoucher, saveVoucher, updateVoucher } from '../store/voucherSlice';

const NewVoucherHeader = () => {
	const dispatch = useDispatch();
	const methods = useFormContext();
	const { formState, watch, getValues } = methods;
	const { isValid, dirtyFields } = formState; // const name = watch('name');
	const theme = useTheme();
	const history = useHistory();

	const { voucherId, voucherName } = useParams();

	const handleDelete = localStorage.getItem('voucherEvent');

	function handleSaveVoucher() {
		dispatch(saveVoucher(getValues())).then(res => {
			if (res.payload) {
				localStorage.setItem('voucherAlert', 'saveVoucher');
				history.goBack('/apps/voucher-managements/vouchers');
			}
		});
	}

	function handleUpdateVoucher() {
		const data = getValues();
		// data.id = data.payroll_voucher?.id;
		console.log('getValues', data);
		const fieldsToRemove = ['employees', 'payheads', 'payroll_voucher'];

		for (const field of fieldsToRemove) {
			delete data[field];
		}

		dispatch(updateVoucher(data)).then(res => {
			if (res.payload) {
				console.log();
				localStorage.setItem('voucherAlert', 'updateVoucher');
				history?.goBack('/apps/voucher-managements/vouchers');
			}

			console.log('histtory', res, 'history', history);
		});
	}

	function handleRemoveVoucher() {
		const data = getValues();
		// data.id = data?.payroll_voucher?.id;
		const fieldsToRemove = ['employees', 'payheads', 'payroll_voucher'];

		for (const field of fieldsToRemove) {
			delete data[field];
		}

		dispatch(removeVoucher(data.id)).then(res => {
			if (res.payload) {
				localStorage.removeItem('voucherEvent');
				localStorage.setItem('voucherAlert', 'deleteVoucher');
				history.goBack('/apps/voucher-managements/vouchers');
			}
		});
	}

	function handleCancel() {
		history.push('/apps/voucher-managements/vouchers');
	}

	return (
		<div className="flex flex-1 w-full items-center justify-between">
			<div className="flex flex-col items-start max-w-full min-w-0">
				<motion.div initial={{ x: 20, opacity: 0 }} animate={{ x: 0, opacity: 1, transition: { delay: 0.3 } }}>
					<Typography
						className="flex items-center sm:mb-2"
						// component={Link}
						role="button"
						// to="/apps/voucher-managements/vouchers"
						color="inherit"
					>
						<Icon className="text-20">{theme.direction === 'ltr' ? 'arrow_back' : 'arrow_forward'}</Icon>
						<span className="hidden sm:flex mx-4 font-medium">Vouchers</span>
					</Typography>
				</motion.div>

				<div className="flex items-center max-w-full">
					<motion.div
						className="hidden sm:flex"
						initial={{ scale: 0 }}
						animate={{ scale: 1, transition: { delay: 0.3 } }}
					/>

					<div className="flex flex-col min-w-0 mx-8 sm:mc-16">
						<motion.div initial={{ x: -20 }} animate={{ x: 0, transition: { delay: 0.3 } }}>
							<Typography className="text-16 sm:text-20 truncate font-semibold">
								Create New Voucher
							</Typography>
							<Typography variant="caption" className="font-medium">
								Vouchers Detail
							</Typography>
						</motion.div>
					</div>
				</div>
			</div>
			<motion.div
				className="flex"
				initial={{ opacity: 0, x: 20 }}
				animate={{ opacity: 1, x: 0, transition: { delay: 0.3 } }}
			>
				{handleDelete === 'Delete' && (
					<Typography className="mt-6" variant="subtitle2">
						Do you want to remove this Voucher?
					</Typography>
				)}
				{handleDelete === 'Delete' && voucherId !== 'new' && (
					<Button
						className="whitespace-nowrap mx-4"
						variant="contained"
						color="secondary"
						onClick={handleRemoveVoucher}
						startIcon={<Icon className="hidden sm:flex">delete</Icon>}
						style={{ backgroundColor: '#ea5b78', color: 'white' }}
					>
						Remove
					</Button>
				)}
				{voucherId === 'new' && (
					<Button
						className="whitespace-nowrap mx-4"
						variant="contained"
						color="secondary"
						disabled={_.isEmpty(dirtyFields) || !isValid}
						onClick={handleSaveVoucher}
					>
						Save
					</Button>
				)}
				{handleDelete !== 'Delete' && voucherName && (
					<Button
						className="whitespace-nowrap mx-4"
						color="secondary"
						variant="contained"
						style={{ backgroundColor: '#4dc08e', color: 'white' }}
						onClick={handleUpdateVoucher}
					>
						Update
					</Button>
				)}
				<Button
					className="whitespace-nowrap mx-4"
					variant="contained"
					style={{ backgroundColor: '#FFAA4C', color: 'white' }}
					onClick={handleCancel}
				>
					Cancel
				</Button>
			</motion.div>
		</div>
	);
};

export default NewVoucherHeader;
