import { combineReducers } from '@reduxjs/toolkit';
import information from './informationSlice';
import informations from './informationsSlice';

const reducer = combineReducers({
	information,
	informations
});

export default reducer;
