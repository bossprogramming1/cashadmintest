import { createAsyncThunk, createEntityAdapter, createSlice } from '@reduxjs/toolkit';
import { DELETE_COMPUTATION_INFORMATION, GET_COMPUTATION_INFORMATIONS } from 'app/constant/constants';
import axios from 'axios';

export const getInformations = createAsyncThunk(
	'informationManagement/informations/getInformations',
	async parameter => {
		const { page, size } = parameter;
		axios.defaults.headers.common['Content-type'] = 'application/json';
		axios.defaults.headers.common.Authorization = localStorage.getItem('jwt_access_token');

		const response = axios.get(GET_COMPUTATION_INFORMATIONS, { params: { page, size } });
		const data = await response;

		sessionStorage.setItem('informations_total_elements', data.data.total_elements);
		sessionStorage.setItem('informations_total_pages', data.data.total_pages);
		delete axios.defaults.headers.common['Content-type'];
		delete axios.defaults.headers.common.Authorization;

		return data.data.computation_informations;
	}
);

export const removeInformations = createAsyncThunk(
	'informationManagement/informations/removeInformations',
	async (informationIds, { dispatch, getState }) => {
		const authTOKEN = {
			headers: {
				'Content-type': 'application/json',
				Authorization: localStorage.getItem('jwt_access_token')
			}
		};
		await axios.delete(`${DELETE_COMPUTATION_INFORMATION}`, { informationIds }, authTOKEN);

		return informationIds;
	}
);

const informationsAdapter = createEntityAdapter({});

export const { selectAll: selectInformations, selectById: selectInformationById } = informationsAdapter.getSelectors(
	state => state.informationsManagement.informations
);

const informationsSlice = createSlice({
	name: 'informationManagement/informations',
	initialState: informationsAdapter.getInitialState({
		searchText: ''
	}),
	reducers: {
		setInformationsSearchText: {
			reducer: (state, action) => {
				state.searchText = action.payload;
			},
			prepare: event => ({ payload: event.target.value || '' })
		}
	},
	extraReducers: {
		[getInformations.fulfilled]: informationsAdapter.setAll
	}
});

export const { setData, setInformationsSearchText } = informationsSlice.actions;
export default informationsSlice.reducer;
