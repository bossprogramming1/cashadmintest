import { createAsyncThunk, createSlice } from '@reduxjs/toolkit';
import {
	CREATE_COMPUTATION_INFORMATION,
	DELETE_COMPUTATION_INFORMATION,
	GET_COMPUTATION_INFORMATIONID,
	UPDATE_COMPUTATION_INFORMATION
} from 'app/constant/constants';
import axios from 'axios';

export const getInformation = createAsyncThunk(
	'informationManagement/information/getInformation',
	async (params, { rejectWithValue }) => {
		const authTOKEN = {
			headers: {
				'Content-type': 'application/json',
				Authorization: localStorage.getItem('jwt_access_token')
			}
		};

		try {
			const response = await axios.get(`${GET_COMPUTATION_INFORMATIONID}${params}`, authTOKEN);
			const data = await response.data;
			return data === undefined ? null : data;
		} catch (err) {
			return rejectWithValue(params);
		}
	}
);

export const removeInformation = createAsyncThunk('informationManagement/information/removeInformation', async val => {
	const authTOKEN = {
		headers: {
			'Content-type': 'application/json',
			Authorization: localStorage.getItem('jwt_access_token')
		}
	};

	const informationId = val.id;
	const response = await axios.delete(`${DELETE_COMPUTATION_INFORMATION}${informationId}`, authTOKEN);
	return response;
});

export const updateInformation = createAsyncThunk(
	'informationManagement/information/updateInformation',
	async (informationData, { dispatch, getState }) => {
		const { information } = getState().informationsManagement;

		const authTOKEN = {
			headers: {
				'Content-type': 'application/json',
				Authorization: localStorage.getItem('jwt_access_token')
			}
		};
		const response = await axios.put(
			`${UPDATE_COMPUTATION_INFORMATION}${information.id}`,
			informationData,
			authTOKEN
		);
		return response;
	}
);

export const saveInformation = createAsyncThunk(
	'informationManagement/information/saveInformation',
	async informationData => {
		const authTOKEN = {
			headers: {
				'Content-type': 'application/json',
				Authorization: localStorage.getItem('jwt_access_token')
			}
		};
		const response = await axios.post(`${CREATE_COMPUTATION_INFORMATION}`, informationData, authTOKEN);
		return response;
	}
);

const informationSlice = createSlice({
	name: 'informationManagement/information',
	initialState: null,
	reducers: {
		resetInformation: () => null,
		newInformation: {
			reducer: (state, action) => action.payload,
			prepare: event => ({
				payload: {
					name: '',
					ip_address: ''
				}
			})
		}
	},
	extraReducers: {
		[getInformation.fulfilled]: (state, action) => action.payload,
		[saveInformation.fulfilled]: (state, action) => action.payload,
		[removeInformation.fulfilled]: (state, action) => action.payload,
		[updateInformation.fulfilled]: (state, action) => action.payload
	}
});

export const { newInformation, resetInformation } = informationSlice.actions;

export default informationSlice.reducer;
