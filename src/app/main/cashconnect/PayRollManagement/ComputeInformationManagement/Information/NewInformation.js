import FusePageCarded from '@fuse/core/FusePageCarded';
import { useDeepCompareEffect } from '@fuse/hooks';
import { yupResolver } from '@hookform/resolvers/yup';
import { Button, Typography } from '@material-ui/core';
import withReducer from 'app/store/withReducer';
import { motion } from 'framer-motion';
import React, { useEffect, useState } from 'react';
import { FormProvider, useForm } from 'react-hook-form';
import { useDispatch, useSelector } from 'react-redux';
import { Link, useParams } from 'react-router-dom';
import * as yup from 'yup';
import reducer from '../store/index';
import { getInformation, newInformation, resetInformation } from '../store/informationSlice';
import NewInformationHeader from './NewInformationHeader';
import InformationForm from './InformationForm';

/**
 * Form Validation Schema
 */
const schema = yup.object().shape({
	name: yup.string().required('Name is required'),
	ip_address: yup.string().required('IP Address is required')
});

const Information = () => {
	const dispatch = useDispatch();
	const information = useSelector(({ informationsManagement }) => informationsManagement.information);

	const [noInformation, setNoInformation] = useState(false);
	const methods = useForm({
		mode: 'onChange',
		defaultValues: {},
		resolver: yupResolver(schema)
	});
	const routeParams = useParams();

	const { reset } = methods;

	useDeepCompareEffect(() => {
		function updateInformationState() {
			const { informationId } = routeParams;

			if (informationId === 'new') {
				localStorage.removeItem('event');
				/**
				 * Create New User data
				 */
				dispatch(newInformation());
			} else {
				/**
				 * Get User data
				 */

				dispatch(getInformation(informationId)).then(action => {
					/**
					 * If the requested product is not exist show message
					 */
					if (!action.payload) {
						setNoInformation(true);
					}
				});
			}
		}

		updateInformationState();
	}, [dispatch, routeParams]);

	useEffect(() => {}, []);

	useEffect(() => {
		if (!information) {
			return;
		}
		/**
		 * Reset the form on information state changes
		 */
		reset(information);
	}, [information, reset]);

	useEffect(() => {
		return () => {
			/**
			 * Reset Information on component unload
			 */
			dispatch(resetInformation());
			setNoInformation(false);
		};
	}, [dispatch]);

	/**
	 * Show Message if the requested products is not exists
	 */
	if (noInformation) {
		return (
			<motion.div
				initial={{ opacity: 0 }}
				animate={{ opacity: 1, transition: { delay: 0.1 } }}
				className="flex flex-col flex-1 items-center justify-center h-full"
			>
				<Typography color="textSecondary" variant="h5">
					There is no such information!
				</Typography>
				<Button
					className="mt-24"
					component={Link}
					variant="outlined"
					to="/apps/e-commerce/products"
					color="inherit"
				>
					Go to Information Page
				</Button>
			</motion.div>
		);
	}

	return (
		<FormProvider {...methods}>
			<FusePageCarded
				classes={{
					toolbar: 'p-0',
					header: 'min-h-80 h-80'
				}}
				header={<NewInformationHeader />}
				content={
					<div className="p-16 sm:p-24">
						<InformationForm />
					</div>
				}
				innerScroll
			/>
		</FormProvider>
	);
};
export default withReducer('informationsManagement', reducer)(Information);
