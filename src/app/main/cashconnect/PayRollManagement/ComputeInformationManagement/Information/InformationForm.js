import _ from '@lodash';
import { makeStyles } from '@material-ui/core/styles';
import TextField from '@material-ui/core/TextField';
import { Autocomplete } from '@material-ui/lab';
import React, { useEffect } from 'react';
import { Controller, useFormContext } from 'react-hook-form';
import { useDispatch, useSelector } from 'react-redux';
import { useHistory, useParams } from 'react-router-dom';
// import { getCities } from '../../../../store/dataSlice';
import { saveInformation, updateInformation } from '../store/informationSlice';

const useStyles = makeStyles(theme => ({
	hidden: {
		display: 'none'
	}
}));

function InformationForm(props) {
	const userID = localStorage.getItem('user_id');

	const citys = useSelector(state => state.data.cities);

	const classes = useStyles(props);

	const methods = useFormContext();
	const { control, formState, getValues } = methods;
	const { errors, isValid, dirtyFields } = formState;
	const routeParams = useParams();
	const { informationId } = routeParams;
	const history = useHistory();
	const handleDelete = localStorage.getItem('informationEvent');
	const dispatch = useDispatch();



	function handleSaveInformation() {
		dispatch(saveInformation(getValues())).then(res => {
			if (res.payload) {
				localStorage.setItem('informationAlert', 'saveInformation');
				history.push('/apps/information-management/informations');
			}
		});
	}

	function handleUpdateInformation() {
		dispatch(updateInformation(getValues())).then(res => {
			if (res.payload) {
				localStorage.setItem('informationAlert', 'updateInformation');
				history.push('/apps/information-management/informations');
			}
		});
	}

	const handleSubmitOnKeyDownEnter = ev => {
		if (ev.key === 'Enter') {
			if (routeParams.informationId === 'new' && !(_.isEmpty(dirtyFields) || !isValid)) {
				handleSaveInformation();
			} else if (handleDelete !== 'Delete' && routeParams?.informationName) {
				handleUpdateInformation();
			}
		}
	};

	return (
		<div>
			<Controller
				name="name"
				control={control}
				render={({ field }) => {
					return (
						<TextField
							{...field}
							className="mt-8 mb-16"
							error={!!errors?.name}
							helperText={errors?.name?.message}
							label="Information Name"
							id="name"
							required
							variant="outlined"
							InputLabelProps={field.value && { shrink: true }}
							fullWidth
							onKeyDown={handleSubmitOnKeyDownEnter}
						/>
					);
				}}
			/>
			<Controller
				name="ip_address"
				control={control}
				render={({ field }) => {
					return (
						<TextField
							{...field}
							className="mt-8 mb-16"
							error={!!errors?.ip_address}
							helperText={errors?.ip_address?.message}
							label="IP Address"
							id="ip_address"
							required
							variant="outlined"
							InputLabelProps={field.value && { shrink: true }}
							fullWidth
							onKeyDown={handleSubmitOnKeyDownEnter}
						/>
					);
				}}
			/>

			
		</div>
	);
}

export default InformationForm;
