/* eslint-disable jsx-a11y/label-has-associated-control */
import _ from '@lodash';
import { Checkbox, FormControl, FormControlLabel } from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';
import TextField from '@material-ui/core/TextField';
import { Autocomplete } from '@material-ui/lab';
import { TimePicker } from '@material-ui/pickers';
import { getTimetables } from 'app/store/dataSlice';
import moment from 'moment';
import React, { useEffect, useState } from 'react';
import { Controller, useFormContext } from 'react-hook-form';
import axios from 'axios';
import InputColor from 'react-input-color';
import { useDispatch, useSelector } from 'react-redux';
import { useHistory, useParams } from 'react-router-dom';
import { CHECK_COLOR_CODE } from 'app/constant/constants';
import { saveTimetable, updateTimetable } from '../store/timetableSlice';

const useStyles = makeStyles(theme => ({
	hidden: {
		display: 'none'
	}
}));

function TimetableForm(props) {
	const userID = localStorage.getItem('user_id');

	const timetable = useSelector(state => state.data.timetable);
	const [color, setColor] = React.useState('');
	const [colorError, setColorError] = React.useState('');
	const [initial, setInitial] = useState('#5e72e4');

	const classes = useStyles(props);

	const methods = useFormContext();
	const { control, formState, setValue, getValues, watch } = methods;
	const colorCode = watch('color');
	const { errors, isValid, dirtyFields } = formState;
	const routeParams = useParams();
	const { timetableId } = routeParams;
	const history = useHistory();
	const handleDelete = localStorage.getItem('timetableEvent');
	const dispatch = useDispatch();

	useEffect(() => {
		dispatch(getTimetables());
	}, []);

	//set color coder
	useEffect(() => {
		axios
			.get(`${CHECK_COLOR_CODE}${color?.replace(/#/g, '')}`, {
				headers: {
					'Content-type': 'application/json',
					Authorization: localStorage.getItem('jwt_access_token')
				}
			})
			.then(response => {

				if (response.data.color_exist === false) {
					setColorError('');
					setValue(`color`, color);
				} else {
					setColorError('already exist');
				}
			});
	}, [color]);

	function handleSaveTimetable() {
		dispatch(saveTimetable(getValues())).then(res => {
			if (res.payload) {
				localStorage.setItem('timetableAlert', 'saveTimetable');
				history.push('/apps/timetables-management');
			}
		});
	}

	function handleUpdateTimetable() {
		dispatch(updateTimetable(getValues())).then(res => {
			if (res.payload) {
				localStorage.setItem('timetableAlert', 'updateTimetable');
				history.push('/apps/timetables-management');
			}
		});
	}

	const handleSubmitOnKeyDownEnter = ev => {
		if (ev.key === 'Enter') {
			if (routeParams.timetableId === 'new' && !(_.isEmpty(dirtyFields) || !isValid)) {
				handleSaveTimetable();
			} else if (handleDelete !== 'Delete' && routeParams?.timetableName) {
				handleUpdateTimetable();
			}
		}
	};

	return (
		<div>
			<Controller
				name="name"
				control={control}
				render={({ field }) => {
					return (
						<TextField
							{...field}
							className="mt-8 mb-16"
							error={!!errors?.name}
							helperText={errors?.name?.message}
							label="Name"
							id="name"
							required
							variant="outlined"
							InputLabelProps={field.value && { shrink: true }}
							fullWidth
							onKeyDown={handleSubmitOnKeyDownEnter}
						/>
					);
				}}
			/>
			<div style={{ display: 'grid', gridTemplateColumns: 'repeat(8, 1fr)', columnGap: '9px' }}>
				<Controller
					name="onduty_time"
					control={control}
					render={({ field }) => {
						return (
							<TextField
								{...field}
								className="mt-8 mb-16"
								error={!!errors?.onduty_time}
								helperText={errors?.onduty_time?.message}
								label="On Duty Time"
								id="onduty_time"
								required
								variant="outlined"
								InputLabelProps={field.value && { shrink: true }}
								fullWidth
								onKeyDown={handleSubmitOnKeyDownEnter}
							/>
						);
					}}
				/>
				<Controller
					name="offduty_time"
					control={control}
					render={({ field }) => {
						return (
							<TextField
								{...field}
								className="mt-8 mb-16"
								error={!!errors?.offduty_time}
								helperText={errors?.offduty_time?.message}
								label="Off Duty Time"
								id="offduty_time"
								required
								variant="outlined"
								InputLabelProps={field.value && { shrink: true }}
								fullWidth
								onKeyDown={handleSubmitOnKeyDownEnter}
							/>
						);
					}}
				/>
				<Controller
					name="checkin_start"
					control={control}
					render={({ field }) => {
						return (
							<TextField
								{...field}
								className="mt-8 mb-16"
								error={!!errors?.checkin_start}
								helperText={errors?.checkin_start?.message}
								label="Check In Start"
								id="checkin_start"
								required
								variant="outlined"
								InputLabelProps={field.value && { shrink: true }}
								fullWidth
								onKeyDown={handleSubmitOnKeyDownEnter}
							/>
						);
					}}
				/>
				<Controller
					name="checkin_end"
					control={control}
					render={({ field }) => {
						return (
							<TextField
								{...field}
								className="mt-8 mb-16"
								error={!!errors?.checkin_end}
								helperText={errors?.checkin_end?.message}
								label="Check In End"
								id="checkin_end"
								required
								variant="outlined"
								InputLabelProps={field.value && { shrink: true }}
								fullWidth
								onKeyDown={handleSubmitOnKeyDownEnter}
							/>
						);
					}}
				/>
				<Controller
					name="checkout_start"
					control={control}
					render={({ field }) => {
						return (
							<TextField
								{...field}
								className="mt-8 mb-16"
								error={!!errors?.checkout_start}
								helperText={errors?.checkout_start?.message}
								label="Check Out Start"
								id="checkout_start"
								required
								variant="outlined"
								InputLabelProps={field.value && { shrink: true }}
								fullWidth
								onKeyDown={handleSubmitOnKeyDownEnter}
							/>
						);
					}}
				/>
				<Controller
					name="checkout_end"
					control={control}
					render={({ field }) => {
						return (
							<TextField
								{...field}
								className="mt-8 mb-16"
								error={!!errors?.checkout_end}
								helperText={errors?.checkout_end?.message}
								label="Check Out End"
								id="checkout_end"
								required
								variant="outlined"
								InputLabelProps={field.value && { shrink: true }}
								fullWidth
								onKeyDown={handleSubmitOnKeyDownEnter}
							/>
						);
					}}
				/>
				<Controller
					name="late_time"
					control={control}
					render={({ field }) => {
						return (
							<TextField
								{...field}
								className="mt-8 mb-16"
								error={!!errors?.late_time}
								helperText={errors?.late_time?.message}
								label="Late Time"
								id="late_time"
								required
								variant="outlined"
								InputLabelProps={field.value && { shrink: true }}
								fullWidth
								onKeyDown={handleSubmitOnKeyDownEnter}
							/>
						);
					}}
				/>
				<Controller
					name="leave_early_time"
					control={control}
					render={({ field }) => {
						return (
							<TextField
								{...field}
								className="mt-8 mb-16"
								error={!!errors?.leave_early_time}
								helperText={errors?.leave_early_time?.message}
								label="Leave Early Time"
								id="leave_early_time"
								required
								variant="outlined"
								InputLabelProps={field.value && { shrink: true }}
								fullWidth
								onKeyDown={handleSubmitOnKeyDownEnter}
							/>
						);
					}}
				/>
				<Controller
					name="count_as_workday"
					control={control}
					render={({ field }) => {
						return (
							<TextField
								{...field}
								className="mt-8 mb-16"
								error={!!errors?.count_as_workday}
								helperText={errors?.count_as_workday?.message}
								label="Count as Workday"
								id="count_as_workday"
								required
								variant="outlined"
								InputLabelProps={field.value && { shrink: true }}
								fullWidth
								onKeyDown={handleSubmitOnKeyDownEnter}
							/>
						);
					}}
				/>
				<Controller
					name="count_as_minute"
					control={control}
					render={({ field }) => {
						return (
							<TextField
								{...field}
								className="mt-8 mb-16"
								error={!!errors?.count_as_minute}
								helperText={errors?.count_as_minute?.message}
								label="Count as min"
								id="count_as_minute"
								required
								variant="outlined"
								InputLabelProps={field.value && { shrink: true }}
								fullWidth
								onKeyDown={handleSubmitOnKeyDownEnter}
							/>
						);
					}}
				/>
				<Controller
					name="must_checkin"
					control={control}
					render={({ field }) => (
						<FormControl>
							<FormControlLabel
								label="Must Check In"
								control={<Checkbox {...field} checked={field.value ? field.value : false} />}
							/>
						</FormControl>
					)}
				/>
				<Controller
					name="must_checkout"
					control={control}
					render={({ field }) => (
						<FormControl>
							<FormControlLabel
								label="Must Check Out"
								control={<Checkbox {...field} checked={field.value ? field.value : false} />}
							/>
						</FormControl>
					)}
				/>
				<div className="flex" style={{ alignItems: 'center', justifyContent: 'space-between' }}>
					<Controller
						name="color"
						control={control}
						render={({ field }) => {
							return (
								<TextField
									{...field}
									className="mt-8 mb-16"
									error={colorError}
									helperText={colorError}
									label="Color Code"
									id="color"
									variant="outlined"
									onChange={e => setInitial(e.target.value)}
									InputLabelProps={field.value && { shrink: true }}
									fullWidth
									onKeyDown={handleSubmitOnKeyDownEnter}
								/>
							);
						}}
					/>

					<input
						type="color"
						id="body"
						name="body"
						onChange={e => setColor(e?.target?.value)}
						value={colorCode || '#121212'}
					></input>

					
				</div>
			
			</div>

			
		</div>
	);
}

export default TimetableForm;
