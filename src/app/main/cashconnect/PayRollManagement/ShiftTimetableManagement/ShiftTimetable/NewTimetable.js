import FusePageCarded from '@fuse/core/FusePageCarded';
import { useDeepCompareEffect } from '@fuse/hooks';
import { yupResolver } from '@hookform/resolvers/yup';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';
import withReducer from 'app/store/withReducer';
import { motion } from 'framer-motion';
import React, { useEffect, useState } from 'react';
import { FormProvider, useForm } from 'react-hook-form';
import { useDispatch, useSelector } from 'react-redux';
import { Link, useParams } from 'react-router-dom';
import * as yup from 'yup';
import reducer from '../store/index';
import { getTimetable, newTimetable, resetTimetable } from '../store/timetableSlice';
import NewTimetableHeader from './NewTimetableHeader';
import TimetableForm from './TimetableForm';

/**
 * Form Validation Schema
 */
const schema = yup.object().shape({
	name: yup.string().required('Name is required')
});

const NewTimetable = () => {
	const dispatch = useDispatch();
	const timetable = useSelector(({ timetablesManagement }) => timetablesManagement.timetable);
	const routeParams = useParams();
	const [noTimetable, setNoTimetable] = useState(false);
	const methods = useForm({
		mode: 'onChange',
		defaultValues: {},
		resolver: yupResolver(schema)
	});
	const { reset } = methods;

	useDeepCompareEffect(() => {
		function updateTimetableState() {
			const { timetableId } = routeParams;
			if (timetableId === 'new') {
				localStorage.removeItem('deleteTimetable');
				localStorage.removeItem('updateTimetable');
				/**
				 * Create New User data
				 */
				dispatch(newTimetable());
			} else {
				/**
				 * Get User data
				 */
				dispatch(getTimetable(routeParams)).then(action => {
					/**
					 * If the requested timetable is not exist show message
					 */
					if (!action.payload) {
						setNoTimetable(true);
					}
				});
			}
		}

		updateTimetableState();
	}, [dispatch, routeParams]);

	useEffect(() => {
		if (!timetable) {
			return;
		}
		/**
		 * Reset the form on timetable state changes
		 */
		reset(timetable);
	}, [timetable, reset]);

	useEffect(() => {
		return () => {
			/**
			 * Reset Timetable on component unload
			 */
			dispatch(resetTimetable());
			setNoTimetable(false);
		};
	}, [dispatch]);

	/**
	 * Show Message if the requested Timetables is not exists
	 */
	if (noTimetable) {
		return (
			<motion.div
				initial={{ opacity: 0 }}
				animate={{ opacity: 1, transition: { delay: 0.1 } }}
				className="flex flex-col flex-1 items-center justify-center h-full"
			>
				<Typography color="textSecondary" variant="h5">
					There is no such timetable!
				</Typography>
				<Button
					className="mt-24"
					component={Link}
					variant="outlined"
					to="/apps/timetables-management"
					color="inherit"
				>
					Go to Timetable Page
				</Button>
			</motion.div>
		);
	}

	/**
	 * Wait while product data is loading and form is setted
	 */

	return (
		<FormProvider {...methods}>
			<FusePageCarded
				classes={{
					toolbar: 'p-0',
					header: 'min-h-80 h-80'
				}}
				header={<NewTimetableHeader />}
				content={
					<div className="p-16 sm:p-24">
						<TimetableForm />
					</div>
				}
				innerScroll
			/>
		</FormProvider>
	);
};
export default withReducer('timetablesManagement', reducer)(NewTimetable);
