import Button from '@material-ui/core/Button';
import Icon from '@material-ui/core/Icon';
import { useTheme } from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';
import { motion } from 'framer-motion';
import _ from 'lodash';
import moment from 'moment';
import React from 'react';
import { useFormContext } from 'react-hook-form';
import { useDispatch } from 'react-redux';
import { Link, useHistory, useParams } from 'react-router-dom';
import { removeTimetable, saveTimetable, updateTimetable } from '../store/timetableSlice';

const NewTimetableHeader = () => {
	const dispatch = useDispatch();
	const methods = useFormContext();
	const { getValues, watch, setError } = methods;
	const name = watch('name');
	const theme = useTheme();
	const history = useHistory();

	const routeParams = useParams();

	const handleDelete = localStorage.getItem('deleteTimetable');
	const handleUpdate = localStorage.getItem('updateTimetable');

	function handleSaveTimetable() {
		dispatch(saveTimetable(getValues())).then(res => {
			if (res.payload.data?.id) {
				localStorage.setItem('timetableAlertPermission', 'saveTimetableSuccessfully');
				history.push('/apps/timetables-management');
			} else if (res.payload.data?.detail) {
				setError('name', { type: 'manual', message: res.payload.data.detail });
			}
		});
	}

	function handleUpdateTimetable() {
		dispatch(updateTimetable(getValues())).then(res => {
			if (res.payload.data?.id) {
				localStorage.setItem('timetableAlert', 'updateTimetable');
				history.push('/apps/timetables-management');
			} else if (res.payload.data?.detail) {
				setError('name', { type: 'manual', message: res.payload.data.detail });
			}
		});
	}

	function handleRemoveTimetable() {
		dispatch(removeTimetable(getValues())).then(res => {
			if (res.payload) {
				localStorage.removeItem('timetableEvent');
				localStorage.setItem('timetableAlert', 'deleteTimetable');
				history.push('/apps/timetables-management');
			}
		});
	}

	function handleCancel() {
		history.push('/apps/timetables-management');
	}


	return (
		<div className="flex flex-1 w-full items-center justify-between">
			<div className="flex flex-col items-start max-w-full min-w-0">
				<motion.div initial={{ x: 20, opacity: 0 }} animate={{ x: 0, opacity: 1, transition: { delay: 0.3 } }}>
					<Typography
						className="flex items-center sm:mb-2"
						component={Link}
						timetable="button"
						to="/apps/timetables-management"
						color="inherit"
					>
						<Icon className="text-20">{theme.direction === 'ltr' ? 'arrow_back' : 'arrow_forward'}</Icon>
						<span className="hidden sm:flex mx-4 font-medium">Timetables</span>
					</Typography>
				</motion.div>

				<div className="flex items-center max-w-full">
					<motion.div
						className="hidden sm:flex"
						initial={{ scale: 0 }}
						animate={{ scale: 1, transition: { delay: 0.3 } }}
					></motion.div>
					<div className="flex flex-col min-w-0 mx-8 sm:mc-16">
						<motion.div initial={{ x: -20 }} animate={{ x: 0, transition: { delay: 0.3 } }}>
							<Typography className="text-16 sm:text-20 truncate font-semibold">
								{name || 'Create New Timetable'}
							</Typography>
							<Typography variant="caption" className="font-medium">
								Timetables Detail
							</Typography>
						</motion.div>
					</div>
				</div>
			</div>
			<motion.div
				className="flex"
				initial={{ opacity: 0, x: 20 }}
				animate={{ opacity: 1, x: 0, transition: { delay: 0.3 } }}
			>
				{handleDelete === 'deleteTimetable' && (
					<Typography className="mt-6" variant="subtitle2">
						Do you want to remove this Timetables?
					</Typography>
				)}
				{handleDelete === 'deleteTimetable' && (
					<Button
						className="whitespace-nowrap mx-4"
						variant="contained"
						color="secondary"
						onClick={handleRemoveTimetable}
						startIcon={<Icon className="hidden sm:flex">delete</Icon>}
						style={{ backgroundColor: '#ea5b78', color: 'white' }}
					>
						Remove
					</Button>
				)}
				{handleDelete !== 'deleteTimetable' && routeParams.timetableId === 'new' && (
					<Button
						className="whitespace-nowrap mx-4"
						variant="contained"
						color="secondary"
						disabled={!name || _.isEmpty(name)}
						onClick={handleSaveTimetable}
					>
						Save
					</Button>
				)}
				{handleDelete !== 'deleteTimetable' && handleUpdate === 'updateTimetable' && (
					<Button
						className="whitespace-nowrap mx-4"
						color="secondary"
						variant="contained"
						//disabled={_.isEmpty(dirtyFields) || !isValid}
						style={{ backgroundColor: '#4dc08e', color: 'white' }}
						onClick={handleUpdateTimetable}
					>
						Update
					</Button>
				)}
				<Button
					className="whitespace-nowrap mx-4"
					variant="contained"
					style={{ backgroundColor: '#FFAA4C', color: 'white' }}
					onClick={handleCancel}
				>
					Cancel
				</Button>
			</motion.div>
		</div>
	);
};

export default NewTimetableHeader;
