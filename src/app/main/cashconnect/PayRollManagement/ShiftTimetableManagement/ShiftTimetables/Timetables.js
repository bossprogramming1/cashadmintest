import FusePageCarded from '@fuse/core/FusePageCarded';
import withReducer from 'app/store/withReducer';
import React from 'react';
import { useDispatch } from 'react-redux';
import reducer from '../store/index';
import TimetablesHeader from './TimetablesHeader';
import TimetablesTable from './TimetablesTable';

const Timetables = () => {
	return (
		<FusePageCarded
			classes={{
				content: 'flex',
				contentCard: 'overflow-hidden',
				header: 'min-h-74 h-64'
			}}
			header={<TimetablesHeader />}
			content={<TimetablesTable />}
			innerScroll
		/>
	);
};
export default withReducer('timetablesManagement', reducer)(Timetables);
