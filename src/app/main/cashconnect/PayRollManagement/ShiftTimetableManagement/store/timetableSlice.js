import { createAsyncThunk, createSlice } from '@reduxjs/toolkit';
import { CREATE_TIMETABLE, DELETE_TIMETABLE, GET_TIMETABLE, UPDATE_TIMETABLE } from 'app/constant/constants';
import axios from 'axios';

export const getTimetable = createAsyncThunk('timetableManagement/timetable/getTimetable', async params => {
	const authTOKEN = {
		headers: {
			'Content-type': 'application/json',
			Authorization: localStorage.getItem('jwt_access_token')
		}
	};
	const response = await axios.get(`${GET_TIMETABLE}${params.timetableId}`, authTOKEN);
	const data = await response.data;
	return data === undefined ? null : data;
});

export const removeTimetable = createAsyncThunk('timetableManagement/timetable/removeTimetable', async val => {
	const authTOKEN = {
		headers: {
			'Content-type': 'application/json',
			Authorization: localStorage.getItem('jwt_access_token')
		}
	};
	const timetableId = val.id;
	const response = await axios.delete(`${DELETE_TIMETABLE}${timetableId}`, authTOKEN);
	return response;
});

export const updateTimetable = createAsyncThunk(
	'timetableManagement/timetable/updateTimetable',
	async (timetableData, { dispatch, getState }) => {
		const authTOKEN = {
			headers: {
				'Content-type': 'application/json',
				Authorization: localStorage.getItem('jwt_access_token')
			}
		};
		const { timetable } = getState().timetablesManagement;

		const response = await axios.put(`${UPDATE_TIMETABLE}${timetable.id}`, timetableData, authTOKEN);
		return response;
	}
);

export const saveTimetable = createAsyncThunk('timetableManagement/timetable/saveTimetable', async timetableData => {
	const authTOKEN = {
		headers: {
			'Content-type': 'application/json',
			Authorization: localStorage.getItem('jwt_access_token')
		}
	};
	const response = await axios.post(`${CREATE_TIMETABLE}`, timetableData, authTOKEN);
	return response;
});

const timetableSlice = createSlice({
	name: 'timetableManagement/timetable',
	initialState: null,
	reducers: {
		resetTimetable: () => null,
		newTimetable: {
			reducer: (state, action) => action.payload,
			prepare: event => ({
				payload: {
					name: '',
					offduty_time: '',
					onduty_time: '',
					checkin_start: '',
					checkin_end: '',
					checkout_start: '',
					checkout_end: '',
					late_time: '',
					leave_early_time: '',
					count_as_workday: '',
					count_as_minute: '',
					must_checkin: false,
					must_checkout: false,
					color: ''
				}
			})
		}
	},
	extraReducers: {
		[getTimetable.fulfilled]: (state, action) => action.payload,
		[saveTimetable.fulfilled]: (state, action) => action.payload,
		[removeTimetable.fulfilled]: (state, action) => action.payload,
		[updateTimetable.fulfilled]: (state, action) => action.payload
	}
});

export const { newTimetable, resetTimetable } = timetableSlice.actions;

export default timetableSlice.reducer;
