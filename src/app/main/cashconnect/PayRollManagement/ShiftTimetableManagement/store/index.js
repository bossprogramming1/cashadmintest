import { combineReducers } from '@reduxjs/toolkit';
import timetable from './timetableSlice';
import timetables from './timetablesSlice';

const reducer = combineReducers({
	timetable,
	timetables
});

export default reducer;
