import { createAsyncThunk, createEntityAdapter, createSlice } from '@reduxjs/toolkit';
import { GET_TIMETABLES } from 'app/constant/constants';
import axios from 'axios';

export const getTimetables = createAsyncThunk('timetableManagement/timetables/getTimetables', async pageAndSize => {
	axios.defaults.headers.common['Content-type'] = 'application/json';
	axios.defaults.headers.common.Authorization = localStorage.getItem('jwt_access_token');

	const response = axios.get(GET_TIMETABLES, { params: pageAndSize });
	const data = await response;

	sessionStorage.setItem('total_timetables_elements', data.data.total_elements);
	sessionStorage.setItem('total_timetables_pages', data.data.total_pages);
	delete axios.defaults.headers.common['Content-type'];
	delete axios.defaults.headers.common.Authorization;

	return data.data.shift_timetables;
});

const TimetablesAdapter = createEntityAdapter({});

export const { selectAll: selectTimetables, selectById: selectTimetableById } = TimetablesAdapter.getSelectors(
	state => state.timetablesManagement.timetables
);

const timetablesSlice = createSlice({
	name: 'timetableManagement/timetables',
	initialState: TimetablesAdapter.getInitialState({
		searchText: ''
	}),
	reducers: {
		setTimetablesSearchText: {
			reducer: (state, action) => {
				state.searchText = action.payload;
			},
			prepare: event => ({ payload: event.target.value || '' })
		}
	},
	extraReducers: {
		[getTimetables.fulfilled]: TimetablesAdapter.setAll
	}
});

export const { setData, setTimetablesSearchText } = timetablesSlice.actions;
export default timetablesSlice.reducer;
