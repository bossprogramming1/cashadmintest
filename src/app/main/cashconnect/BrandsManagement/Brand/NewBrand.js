import FusePageCarded from '@fuse/core/FusePageCarded';
import { useDeepCompareEffect } from '@fuse/hooks';
import { yupResolver } from '@hookform/resolvers/yup';
import { Button, Typography } from '@material-ui/core';
import { BRAND_CREATE, BRAND_DELETE, BRAND_DETAILS, BRAND_UPDATE } from 'app/constant/permission/permission';
import { getUserPermissions } from 'app/store/dataSlice';
import withReducer from 'app/store/withReducer';
import { motion } from 'framer-motion';
import React, { useEffect, useState } from 'react';
import { FormProvider, useForm } from 'react-hook-form';
import { useDispatch, useSelector } from 'react-redux';
import { Link, useParams } from 'react-router-dom';
import * as yup from 'yup';
import PagenotFound from '../../Pagenotfound/PagenotFound';
import { getBrand, newBrand, resetBrand } from '../store/brandSlice';
import reducer from '../store/index';
import BrandForm from './BrandForm';
import NewBrandHeader from './NewBrandHeader';

/**
 * Form Validation Schema
 */
const schema = yup.object().shape({
	name: yup.string().required('Name is required')
});

const Brand = () => {
	const dispatch = useDispatch();
	const brand = useSelector(({ brandsManagement }) => brandsManagement.brand);

	const [noBrand, setNoBrand] = useState(false);
	const methods = useForm({
		mode: 'onChange',
		defaultValues: {},
		resolver: yupResolver(schema)
	});
	const routeParams = useParams();
	const UserPermissions = useSelector(state => state.data.UserPermissions);
	useEffect(() => {
		dispatch(getUserPermissions());
	}, []);
	const { reset, watch, control, onChange, formState } = methods;
	const form = watch();

	useDeepCompareEffect(() => {
		function updateBrandState() {
			const { brandId } = routeParams;

			if (brandId === 'new') {
				localStorage.removeItem('deleteBrand');
				localStorage.removeItem('updateBrand');
				/**
				 * Create New User data
				 */
				dispatch(newBrand());
			} else {
				/**
				 * Get User data
				 */

				dispatch(getBrand(brandId)).then(action => {
					/**
					 * If the requested product is not exist show message
					 */
					if (!action.payload) {
						setNoBrand(true);
					}
				});
			}
		}

		updateBrandState();
	}, [dispatch, routeParams]);

	useEffect(() => {}, []);

	useEffect(() => {
		if (!brand) {
			return;
		}
		/**
		 * Reset the form on brand state changes
		 */
		reset(brand);
	}, [brand, reset]);

	useEffect(() => {
		return () => {
			/**
			 * Reset Brand on component unload
			 */
			dispatch(resetBrand());
			setNoBrand(false);
		};
	}, [dispatch]);

	/**
	 * Show Message if the requested products is not exists
	 */
	if (noBrand) {
		return (
			<motion.div
				initial={{ opacity: 0 }}
				animate={{ opacity: 1, transition: { delay: 0.1 } }}
				className="flex flex-col flex-1 items-center justify-center h-full"
			>
				<Typography color="textSecondary" variant="h5">
					There is no such brand!
				</Typography>
				<Button
					className="mt-24"
					component={Link}
					variant="outlined"
					to="/apps/e-commerce/products"
					color="inherit"
				>
					Go to Brand Page
				</Button>
			</motion.div>
		);
	}

	return (
		<FormProvider {...methods}>
			{UserPermissions.includes(BRAND_CREATE) ||
			UserPermissions.includes(BRAND_UPDATE) ||
			UserPermissions.includes(BRAND_DELETE) ||
			UserPermissions.includes(BRAND_DETAILS) ? (
				<FusePageCarded
					classes={{
						content: 'flex',
						contentCard: 'overflow-hidden',
						header: 'min-h-74 h-64'
					}}
					header={<NewBrandHeader />}
					content={
						<div className="p-16 sm:p-24" style={{ width: '100%' }}>
							<BrandForm />
						</div>
					}
					innerScroll
				/>
			) : (
				<PagenotFound />
			)}
		</FormProvider>
	);
};
export default withReducer('brandsManagement', reducer)(Brand);
