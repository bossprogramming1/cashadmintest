import FuseLoading from '@fuse/core/FuseLoading';
import FuseScrollbars from '@fuse/core/FuseScrollbars';
import _ from '@lodash';
import Checkbox from '@material-ui/core/Checkbox';
import { makeStyles } from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TablePagination from '@material-ui/core/TablePagination';
import TableRow from '@material-ui/core/TableRow';
import DeleteIcon from '@material-ui/icons/Delete';
import EditIcon from '@material-ui/icons/Edit';
import Pagination from '@material-ui/lab/Pagination';
import React, { useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { withRouter } from 'react-router-dom';
import { Tooltip } from '@material-ui/core';
import { BASE_URL, SEARCH_BRAND } from '../../../../constant/constants';
import { getBrands, selectBrands } from '../store/brandsSlice';
import BrandsTableHead from './BrandsTableHead';

const useStyles = makeStyles(theme => ({
	root: {
		display: 'flex',
		justifyContent: 'space-between',
		flexWrap: 'wrap',
		'& > *': {
			marginTop: theme.spacing(2),
			marginBottom: theme.spacing(3)
		}
	}
}));

const BrandsTable = props => {
	const dispatch = useDispatch();
	const brands = useSelector(selectBrands);
	const searchText = useSelector(({ brandsManagement }) => brandsManagement.brands.searchText);
	const [searchBrand, setSearchBrand] = useState([]);
	const [loading, setLoading] = useState(true);
	const [selected, setSelected] = useState([]);
	const [data, setData] = useState(brands);
	const [page, setPage] = useState(0);
	const [rowsPerPage, setRowsPerPage] = useState(30);
	const [order, setOrder] = useState({
		direction: 'asc',
		id: null
	});
	let serialNumber = 1;
	const user_role = localStorage.getItem('user_role');

	const [parameter, setParameter] = useState({ page: 1, size: 30 });
	const totalPages = sessionStorage.getItem('brands_total_pages');
	const totalElements = sessionStorage.getItem('brands_total_elements');
	const classes = useStyles();

	useEffect(() => {
		dispatch(getBrands(parameter)).then(() => setLoading(false));
	}, [dispatch]);

	//search brand
	useEffect(() => {
		searchText !== '' && getSearchBrand();
	}, [searchText]);

	const getSearchBrand = () => {
		fetch(`${SEARCH_BRAND}?name=${searchText}`)
			.then(response => response.json())
			.then(searchedBrandData => {
				setSearchBrand(searchedBrandData.brands);
			});
	};

	function handleRequestSort(brandEvent, property) {
		const id = property;
		let direction = 'desc';

		if (order.id === property && order.direction === 'desc') {
			direction = 'asc';
		}

		setOrder({
			direction,
			id
		});
	}

	function handleSelectAllClick(brandEvent) {
		if (brandEvent.target.checked) {
			setSelected(brands.map(n => n.id));
			return;
		}
		setSelected([]);
	}

	function handleDeselect() {
		setSelected([]);
	}

	function handleUpdateBrand(item, event) {
		localStorage.removeItem('deleteBrand');
		localStorage.setItem('updateBrand', event);
		props.history.push(`/apps/brand-management/brands/${item.id}/${item?.name}`);
	}
	function handleDeleteBrand(item, event) {
		localStorage.removeItem('updateBrand');
		localStorage.setItem('deleteBrand', event);
		props.history.push(`/apps/brand-management/brands/${item.id}/${item?.name}`);
	}

	function handleCheck(brandEvent, id) {
		const selectedIndex = selected.indexOf(id);
		let newSelected = [];

		if (selectedIndex === -1) {
			newSelected = newSelected.concat(selected, id);
		} else if (selectedIndex === 0) {
			newSelected = newSelected.concat(selected.slice(1));
		} else if (selectedIndex === selected.length - 1) {
			newSelected = newSelected.concat(selected.slice(0, -1));
		} else if (selectedIndex > 0) {
			newSelected = newSelected.concat(selected.slice(0, selectedIndex), selected.slice(selectedIndex + 1));
		}

		setSelected(newSelected);
	}

	const handlePagination = (e, handlePage) => {
		setParameter({ ...parameter, page: handlePage });
		setPage(handlePage - 1);
		dispatch(getBrands({ ...parameter, page: handlePage }));
	};

	function handleChangePage(brandEvent, value) {
		setPage(value);
		setParameter({ ...parameter, page: value + 1 });
		dispatch(getBrands({ ...parameter, page: value - 1 }));
	}

	function handleChangeRowsPerPage(brandEvent) {
		setRowsPerPage(brandEvent.target.value);
		setParameter({ ...parameter, size: brandEvent.target.value });
		dispatch(getBrands({ ...parameter, size: brandEvent.target.value }));
	}

	if (loading) {
		return <FuseLoading />;
	}

	return (
		<div className="w-full flex flex-col">
			<FuseScrollbars className="flex-grow overflow-x-auto">
				<Table stickyHeader className="min-w-xl" aria-labelledby="tableTitle">
					<BrandsTableHead
						selectedBrandIds={selected}
						order={order}
						onSelectAllClick={handleSelectAllClick}
						onRequestSort={handleRequestSort}
						rowCount={brands.length}
						onMenuItemClick={handleDeselect}
					/>

					<TableBody>
						{_.orderBy(
							searchText !== '' && searchBrand ? searchBrand : brands,
							[
								o => {
									switch (order.id) {
										case 'categories': {
											return o.categories[0];
										}
										default: {
											return o[order.id];
										}
									}
								}
							],
							[order.direction]
						).map(n => {
							const isSelected = selected.indexOf(n.id) !== -1;
							return (
								<TableRow
									hover
									role="checkbox"
									aria-checked={isSelected}
									tabIndex={-1}
									key={n.id}
									selected={isSelected}
								>
									<TableCell className="whitespace-nowrap w-40 md:w-64 text-center" padding="none">
										<Checkbox
											checked={isSelected}
											onClick={brandEvent => brandEvent.stopPropagation()}
											onChange={brandEvent => handleCheck(brandEvent, n.id)}
										/>
									</TableCell>

									<TableCell className="whitespace-nowrap p-4 md:p-16" component="th" scope="row">
										{parameter.page * parameter.size - parameter.size + serialNumber++}
									</TableCell>
									<TableCell
										whitespace-nowrap
										className="w-52 px-4 md:px-0"
										component="th"
										scope="row"
										padding="none"
									>
										{n.length > 0 && n.featuredImageId ? (
											<img
												className="w-full block rounded"
												src={_.find(n?.image, { id: n.featuredImageId }).url}
												alt={n?.name}
											/>
										) : (
											<img
												style={{ borderRadius: '50%' }}
												className="w-full block rounded"
												src={`${BASE_URL}${n?.image}`}
												alt={n?.name}
											/>
										)}
									</TableCell>
									<TableCell className="whitespace-nowrap p-4 md:p-16" component="th" scope="row">
										{n?.name}
									</TableCell>

									<TableCell
										whitespace-nowrap
										className="p-4 md:p-16"
										align="center"
										component="th"
										scope="row"
									>
										<div>
											<Tooltip title="Edit" placement="top">
												<EditIcon
													onClick={brandEvent => handleUpdateBrand(n, 'updateBrand')}
													className="h-52 cursor-pointer"
													style={{ color: 'green' }}
												/>
											</Tooltip>
											<Tooltip title="Delete" placement="top-start">
												<DeleteIcon
													onClick={event => handleDeleteBrand(n, 'deleteBrand')}
													className="h-52 cursor-pointer"
													style={{
														color: 'red',
														visibility:
															user_role === 'ADMIN' || user_role === 'admin'
																? 'visible'
																: 'hidden'
													}}
												/>
											</Tooltip>
										</div>
									</TableCell>
								</TableRow>
							);
						})}
					</TableBody>
				</Table>
			</FuseScrollbars>

			<div className={classes.root}>
				<Pagination
					count={totalPages}
					defaultPage={1}
					color="primary"
					showFirstButton
					showLastButton
					variant="outlined"
					shape="rounded"
					onChange={handlePagination}
				/>

				<TablePagination
					className="flex-shrink-0 border-t-1"
					rowsPerPageOptions={[30, 50, 100]}
					component="div"
					count={totalElements}
					rowsPerPage={rowsPerPage}
					page={page}
					backIconButtonProps={{
						'aria-label': 'Previous Page'
					}}
					nextIconButtonProps={{
						'aria-label': 'Next Page'
					}}
					onChangePage={handleChangePage}
					onChangeRowsPerPage={handleChangeRowsPerPage}
				/>
			</div>
		</div>
	);
};

export default withRouter(BrandsTable);
