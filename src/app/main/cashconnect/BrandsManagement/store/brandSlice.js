import { createAsyncThunk, createSlice } from '@reduxjs/toolkit';
import axios from 'axios';
import { CREATE_BRAND, DELETE_BRAND, GET_BRANDID, UPDATE_BRAND } from '../../../../constant/constants';

export const getBrand = createAsyncThunk('brandManagement/brand/getBrand', async (params, { rejectWithValue }) => {
	const authTOKEN = {
		headers: {
			'Content-type': 'application/json',
			Authorization: localStorage.getItem('jwt_access_token')
		}
	};

	try {
		const response = await axios.get(`${GET_BRANDID}${params}`, authTOKEN);
		const data = await response.data;
		return data === undefined ? null : data;
	} catch (err) {
		return rejectWithValue(params);
	}
});

export const removeBrand = createAsyncThunk(
	'brandManagement/brand/removeBrand',
	async (val, { dispatch, getState }) => {
		const authTOKEN = {
			headers: {
				'Content-type': 'application/json',
				Authorization: localStorage.getItem('jwt_access_token')
			}
		};

		const brandId = val.id;
		await axios.delete(`${DELETE_BRAND}${brandId}`, authTOKEN);
	}
);

export const updateBrand = createAsyncThunk(
	'brandManagement/brand/updateBrand',
	async (brandData, { dispatch, getState }) => {
		const { brand } = getState().brandsManagement;
		const brandDataToFormData = jsonToFormData(brandData);
		const authTOKEN = {
			headers: {
				'Content-type': 'application/json',
				Authorization: localStorage.getItem('jwt_access_token')
			}
		};
		const response = await axios.put(`${UPDATE_BRAND}${brand.id}`, brandDataToFormData, authTOKEN);
	}
);

export const saveBrand = createAsyncThunk(
	'brandManagement/brand/saveBrand',
	async (brandData, { dispatch, getState }) => {
		const brandDataToFormData = jsonToFormData(brandData);
		const authTOKEN = {
			headers: {
				'Content-type': 'application/json',
				Authorization: localStorage.getItem('jwt_access_token')
			}
		};
		const response = await axios.post(`${CREATE_BRAND}`, brandDataToFormData, authTOKEN);
	}
);

//buildformdata
function buildFormData(formData, data, parentKey) {
	if (data && typeof data === 'object' && !(data instanceof Date) && !(data instanceof File)) {
		Object.keys(data).forEach(key => {
			buildFormData(formData, data[key], parentKey ? `${parentKey}[${key}]` : key);
		});
	} else {
		const value = data === null ? '' : data;

		formData.append(parentKey, value);
	}
}

function jsonToFormData(data) {
	const formData = new FormData();

	buildFormData(formData, data);

	return formData;
}

const brandSlice = createSlice({
	name: 'brandManagement/brand',
	initialState: null,
	reducers: {
		resetBrand: () => null,
		newBrand: {
			reducer: (state, action) => action.payload,
			prepare: event => ({
				payload: {
					name: '',
					image: ''
				}
			})
		}
	},
	extraReducers: {
		[getBrand.fulfilled]: (state, action) => action.payload,
		[saveBrand.fulfilled]: (state, action) => {
			localStorage.setItem('brandAlert', 'saveBrand');
			return action.payload;
		},
		[removeBrand.fulfilled]: () => {
			localStorage.setItem('brandAlert', 'deleteBrand');
			return null;
		},
		[updateBrand.fulfilled]: () => {
			localStorage.setItem('brandAlert', 'updateBrand');
		}
	}
});

export const { newBrand, resetBrand } = brandSlice.actions;

export default brandSlice.reducer;
