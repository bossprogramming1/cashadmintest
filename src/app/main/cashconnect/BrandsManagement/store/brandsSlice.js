import { createAsyncThunk, createEntityAdapter, createSlice } from '@reduxjs/toolkit';
import axios from 'axios';
import { DELETE_BRAND, GET_BRANDS } from '../../../../constant/constants';

export const getBrands = createAsyncThunk('brandManagement/brands/geBrands', async parameter => {

	const { page, size } = parameter

	axios.defaults.headers.common['Content-type'] = 'application/json'
	axios.defaults.headers.common.Authorization = localStorage.getItem('jwt_access_token')

	const response = axios.get(GET_BRANDS, { params: { page, size } });
	const data = await response;

	sessionStorage.setItem('brands_total_elements', data.data.total_elements);
	sessionStorage.setItem('brands_total_pages', data.data.total_pages);
	delete axios.defaults.headers.common["Content-type"]
	delete axios.defaults.headers.common.Authorization

	return data.data.brands;
});

export const removeBrands = createAsyncThunk(
	'brandManagement/brands/removeBrands',
	async (brandIds, { dispatch, getState }) => {
		const authTOKEN = {
			headers: {
				'Content-type': 'application/json',
				Authorization: localStorage.getItem('jwt_access_token')
			}
		};
		await axios.delete(`${DELETE_BRAND}`, { brandIds }, authTOKEN);

		return brandIds;
	}
);

const brandsAdapter = createEntityAdapter({});

export const { selectAll: selectBrands, selectById: selectBrandById } = brandsAdapter.getSelectors(
	state => state.brandsManagement.brands
);

const brandsSlice = createSlice({
	name: 'brandManagement/brands',
	initialState: brandsAdapter.getInitialState({
		searchText: ''
	}),
	reducers: {
		setBrandsSearchText: {
			reducer: (state, action) => {
				state.searchText = action.payload;
			},
			prepare: event => ({ payload: event.target.value || '' })
		}
	},
	extraReducers: {
		[getBrands.fulfilled]: brandsAdapter.setAll
	}
});

export const { setData, setBrandsSearchText } = brandsSlice.actions;
export default brandsSlice.reducer;
