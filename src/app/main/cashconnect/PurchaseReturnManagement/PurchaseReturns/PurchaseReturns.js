import FusePageCarded from '@fuse/core/FusePageCarded';
import { PURCHASERETURN_LIST } from 'app/constant/permission/permission';
import { getUserPermissions } from 'app/store/dataSlice';
import withReducer from 'app/store/withReducer';
import React, { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import PagenotFound from '../../Pagenotfound/PagenotFound';
import reducer from '../store/index';
import PurchaseReturnsHeader from './PurchaseReturnsHeader';
import PurchaseReturnsTable from './PurchaseReturnsTable';

const PurchaseReturns = () => {
	const UserPermissions = useSelector(state => state.data.UserPermissions);
	useEffect(() => {
		dispatch(getUserPermissions());
	}, []);
	const dispatch = useDispatch();
	return (
		<FusePageCarded
			classes={{
				content: 'flex',
				contentCard: 'overflow-hidden',
				header: 'min-h-74 h-64'
			}}
			header={UserPermissions.includes(PURCHASERETURN_LIST) && <PurchaseReturnsHeader />}
			content={UserPermissions.includes(PURCHASERETURN_LIST) ? <PurchaseReturnsTable /> : <PagenotFound />}
			innerScroll
		/>
	);
};
export default withReducer('purchasereturnsManagement', reducer)(PurchaseReturns);
