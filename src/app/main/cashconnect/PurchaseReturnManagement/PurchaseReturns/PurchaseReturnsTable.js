import FuseLoading from '@fuse/core/FuseLoading';
import FuseScrollbars from '@fuse/core/FuseScrollbars';
import _ from '@lodash';
import { Tooltip, Typography } from '@material-ui/core';
import Checkbox from '@material-ui/core/Checkbox';
import { makeStyles } from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TablePagination from '@material-ui/core/TablePagination';
import TableRow from '@material-ui/core/TableRow';
import DeleteIcon from '@material-ui/icons/Delete';
import EditIcon from '@material-ui/icons/Edit';
import Pagination from '@material-ui/lab/Pagination';
import { rowsPerPageOptions } from 'app/@data/data';
import { motion } from 'framer-motion';
import React, { useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { withRouter } from 'react-router-dom';
import { SEARCH_PURCHASERETURN } from '../../../../constant/constants';
import { getPurchaseReturns, selectPurchaseReturns } from '../store/purchaseReturnsSlice';
import PurchaseReturnsTableHead from './PurchaseReturnsTableHead';

const useStyles = makeStyles(() => ({
	root: {
		display: 'flex',
		justifyContent: 'space-between',
		flexWrap: 'nowrap',
		overflow: 'auto',
		minHeight: '35px'
	},
	toolbar: {
		'& > div': {
			minHeight: 'fit-content'
		}
	}
}));

const PurchaseReturnsTable = props => {
	const dispatch = useDispatch();
	const purchasereturns = useSelector(selectPurchaseReturns);
	const searchText = useSelector(
		({ purchasereturnsManagement }) => purchasereturnsManagement.purchasereturns.searchText
	);
	const [searchPurchaseReturn, setSearchPurchaseReturn] = useState([]);
	const [loading, setLoading] = useState(true);
	const [selected, setSelected] = useState([]);
	const [page, setPage] = useState(0);
	const [rowsPerPage, setRowsPerPage] = useState(30);
	const [order, setOrder] = useState({
		direction: 'asc',
		id: null
	});
	let serialNumber = 1;
	const user_role = localStorage.getItem('user_role');

	const [parameter, setParameter] = useState({ page: 1, size: 30 });
	const totalPages = sessionStorage.getItem('purchasereturns_total_pages');
	const totalElements = sessionStorage.getItem('purchasereturns_total_elements');
	const classes = useStyles();

	console.log('purchasereturns', purchasereturns);
	useEffect(() => {
		dispatch(getPurchaseReturns(parameter)).then(() => setLoading(false));
	}, [dispatch]);

	//search purchasereturn
	useEffect(() => {
		searchText ? getSearchPurchaseReturn() : setSearchPurchaseReturn([]);
	}, [searchText]);

	const getSearchPurchaseReturn = () => {
		fetch(`${SEARCH_PURCHASERETURN}?name=${searchText}`)
			.then(response => response.json())
			.then(searchedPurchaseReturnData => {
				setSearchPurchaseReturn(searchedPurchaseReturnData.purchasereturns);
			})
			.catch(() => setSearchPurchaseReturn([]));
	};

	function handleRequestSort(purchasereturnEvent, property) {
		const id = property;
		let direction = 'desc';

		if (order.id === property && order.direction === 'desc') {
			direction = 'asc';
		}

		setOrder({
			direction,
			id
		});
	}

	function handleSelectAllClick(purchasereturnEvent) {
		if (purchasereturnEvent.target.checked) {
			setSelected(purchasereturns.map(n => n.id));
			return;
		}
		setSelected([]);
	}

	function handleDeselect() {
		setSelected([]);
	}

	function handleUpdatePurchaseReturn(item) {
		localStorage.removeItem('purchasereturnEvent');
		props.history.push(`/apps/purchasereturn-management/purchasereturn/${item.id}/${item?.name}`);
	}
	function handleDeletePurchaseReturn(item, purchasereturnEvent) {
		localStorage.removeItem('purchasereturnEvent');
		localStorage.setItem('purchasereturnEvent', purchasereturnEvent);
		props.history.push(`/apps/purchasereturn-management/purchasereturn/${item.id}/${item?.name}`);
	}

	function handleCheck(purchasereturnEvent, id) {
		const selectedIndex = selected.indexOf(id);
		let newSelected = [];

		if (selectedIndex === -1) {
			newSelected = newSelected.concat(selected, id);
		} else if (selectedIndex === 0) {
			newSelected = newSelected.concat(selected.slice(1));
		} else if (selectedIndex === selected.length - 1) {
			newSelected = newSelected.concat(selected.slice(0, -1));
		} else if (selectedIndex > 0) {
			newSelected = newSelected.concat(selected.slice(0, selectedIndex), selected.slice(selectedIndex + 1));
		}

		setSelected(newSelected);
	}

	const handlePagination = (e, handlePage) => {
		setParameter({ ...parameter, page: handlePage });
		setPage(handlePage - 1);
		dispatch(getPurchaseReturns({ ...parameter, page: handlePage }));
	};

	function handleChangePage(purchasereturnEvent, value) {
		setPage(value);
		setParameter({ ...parameter, page: value + 1 });
		dispatch(getPurchaseReturns({ ...parameter, page: value - 1 }));
	}

	function handleChangeRowsPerPage(purchasereturnEvent) {
		setRowsPerPage(purchasereturnEvent.target.value);
		setParameter({ ...parameter, size: purchasereturnEvent.target.value });
		dispatch(getPurchaseReturns({ ...parameter, size: purchasereturnEvent.target.value }));
	}

	if (loading) {
		return <FuseLoading />;
	}

	if (purchasereturns.length === 0) {
		return (
			<motion.div
				initial={{ opacity: 0 }}
				animate={{ opacity: 1, transition: { delay: 0.1 } }}
				className="flex flex-1 items-center justify-center h-full"
			>
				<Typography color="textSecondary" variant="h5">
					There are no purchasereturn!
				</Typography>
			</motion.div>
		);
	}

	return (
		<div className="w-full flex flex-col">
			<FuseScrollbars className="flex-grow overflow-x-auto">
				<Table stickyHeader className="min-w-xl" aria-labelledby="tableTitle">
					<PurchaseReturnsTableHead
						selectedPurchaseReturnIds={selected}
						order={order}
						onSelectAllClick={handleSelectAllClick}
						onRequestSort={handleRequestSort}
						rowCount={purchasereturns.length}
						onMenuItemClick={handleDeselect}
					/>

					<TableBody>
						{_.orderBy(
							searchText !== '' && !_.isEmpty(searchPurchaseReturn)
								? searchPurchaseReturn
								: purchasereturns,
							[
								o => {
									switch (order.id) {
										case 'categories': {
											return o.categories[0];
										}
										default: {
											return o[order.id];
										}
									}
								}
							],
							[order.direction]
						).map(n => {
							const isSelected = selected.indexOf(n.id) !== -1;
							return (
								<TableRow
									className="h-52 cursor-pointer"
									hover
									role="checkbox"
									aria-checked={isSelected}
									tabIndex={-1}
									key={n.id}
									selected={isSelected}
								>
									<TableCell className="whitespace-nowrap w-40 md:w-64 text-center" padding="none">
										<Checkbox
											checked={isSelected}
											onClick={purchasereturnEvent => purchasereturnEvent.stopPropagation()}
											onChange={purchasereturnEvent => handleCheck(purchasereturnEvent, n.id)}
										/>
									</TableCell>

									<TableCell className="whitespace-nowrap p-4 md:p-16" component="th" scope="row">
										{parameter.page * parameter.size - parameter.size + serialNumber++}
									</TableCell>

									<TableCell className="whitespace-nowrap p-4 md:p-16" component="th" scope="row">
										{n?.product?.name}
									</TableCell>
									<TableCell className="whitespace-nowrap p-4 md:p-16" component="th" scope="row">
										{`${n?.color?.name} ${n?.size?.name ? `[${n?.size?.name}]` : ''}`}
									</TableCell>
									<TableCell className="whitespace-nowrap p-4 md:p-16" component="th" scope="row">
										{n?.quantity}
									</TableCell>
									<TableCell className="whitespace-nowrap p-4 md:p-16" component="th" scope="row">
										{n?.unit_price}
									</TableCell>
									<TableCell className="whitespace-nowrap p-4 md:p-16" component="th" scope="row">
										{n?.subtotal}
									</TableCell>

									<TableCell
										whitespace-nowrap
										className="p-4 md:p-16"
										align="center"
										component="th"
										scope="row"
									>
										<div>
											<Tooltip title="Edit" placement="top" enterDelay={300}>
												<EditIcon
													onClick={purchasereturnEvent => handleUpdatePurchaseReturn(n)}
													className="cursor-pointer"
													style={{ color: 'green' }}
												/>
											</Tooltip>
											<Tooltip title="Delete" placement="top" enterDelay={300}>
												<DeleteIcon
													onClick={event => handleDeletePurchaseReturn(n, 'Delete')}
													className="cursor-pointer"
													style={{
														color: 'red',
														visibility:
															user_role === 'ADMIN' || user_role === 'admin'
																? 'visible'
																: 'hidden'
													}}
												/>
											</Tooltip>
										</div>
									</TableCell>
								</TableRow>
							);
						})}
					</TableBody>
				</Table>
			</FuseScrollbars>

			<div className={classes.root} id="pagiContainer">
				<Pagination
					classes={{ ul: 'flex-nowrap' }}
					count={totalPages}
					page={page}
					defaultPage={1}
					color="primary"
					showFirstButton
					showLastButton
					variant="outlined"
					shape="rounded"
					onChange={handlePagination}
				/>

				<TablePagination
					classes={{ root: 'overflow-visible' }}
					rowsPerPageOptions={rowsPerPageOptions}
					component="div"
					count={totalElements}
					rowsPerPage={rowsPerPage}
					page={page}
					className={classes.toolbar}
					backIconButtonProps={{
						'aria-label': 'Previous Page',
						className: 'py-0'
					}}
					nextIconButtonProps={{
						'aria-label': 'Next Page',
						className: 'py-0'
					}}
					onChangePage={handleChangePage}
					onChangeRowsPerPage={handleChangeRowsPerPage}
				/>
			</div>
		</div>
	);
};

export default withRouter(PurchaseReturnsTable);
