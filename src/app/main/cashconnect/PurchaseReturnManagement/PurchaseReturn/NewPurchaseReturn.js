import FusePageCarded from '@fuse/core/FusePageCarded';
import { useDeepCompareEffect } from '@fuse/hooks';
import { yupResolver } from '@hookform/resolvers/yup';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';
import {
	PURCHASERETURN_CREATE,
	PURCHASERETURN_DELETE,
	PURCHASERETURN_DETAILS,
	PURCHASERETURN_UPDATE
} from 'app/constant/permission/permission';
import { getUserPermissions } from 'app/store/dataSlice';
import withReducer from 'app/store/withReducer';
import { motion } from 'framer-motion';
import React, { useEffect, useState } from 'react';
import { FormProvider, useForm } from 'react-hook-form';
import { useDispatch, useSelector } from 'react-redux';
import { Link, useParams } from 'react-router-dom';
import * as yup from 'yup';
import PagenotFound from '../../Pagenotfound/PagenotFound';
import { getPurchaseReturn, newPurchaseReturn, resetPurchaseReturn } from '../store/purchaseReturnSlice';
import reducer from '../store/index';
import PurchaseReturnForm from './PurchaseReturnForm';
import NewPurchaseReturnHeader from './NewPurchaseReturnHeader';

/**
 * Form Validation Schema
 */
const schema = yup.object().shape({
	// purchase_return_items: yup.string().required('Return item is required')
});

const PurchaseReturn = () => {
	const dispatch = useDispatch();
	const purchasereturn = useSelector(({ purchasereturnsManagement }) => purchasereturnsManagement.purchasereturn);

	const [noPurchaseReturn, setNoPurchaseReturn] = useState(false);
	const methods = useForm({
		mode: 'onChange',
		defaultValues: {},
		resolver: yupResolver(schema)
	});
	const routeParams = useParams();
	const UserPermissions = useSelector(state => state.data.UserPermissions);
	useEffect(() => {
		dispatch(getUserPermissions());
	}, []);
	const { reset, watch } = methods;
	const { purchasereturnId } = routeParams;
	useDeepCompareEffect(() => {
		function updatePurchaseReturnState() {
			if (purchasereturnId === 'new') {
				localStorage.removeItem('event');
				/**
				 * Create New User data
				 */
				dispatch(newPurchaseReturn());
			} else {
				/**
				 * Get User data
				 */
				dispatch(getPurchaseReturn(purchasereturnId)).then(action => {
					/**
					 * If the requested product is not exist show message
					 */
					if (!action.payload) {
						setNoPurchaseReturn(true);
					}
				});
			}
		}

		updatePurchaseReturnState();
	}, [dispatch, routeParams]);

	useEffect(() => {}, []);

	useEffect(() => {
		if (!purchasereturn) {
			return;
		}
		/**
		 * Reset the form on purchasereturn state changes
		 */
		if (purchasereturnId === 'new') {
			reset(purchasereturn);
		} else {
			reset({
				invoice_no: purchasereturn?.invoice_no,
				id: purchasereturn?.id,
				purchase_return_items: [purchasereturn]
			});
		}
	}, [purchasereturn, reset]);

	useEffect(() => {
		return () => {
			/**
			 * Reset PurchaseReturn on component unload
			 */
			dispatch(resetPurchaseReturn());
			setNoPurchaseReturn(false);
		};
	}, [dispatch]);

	/**
	 * Show Message if the requested products is not exists
	 */
	if (noPurchaseReturn) {
		return (
			<motion.div
				initial={{ opacity: 0 }}
				animate={{ opacity: 1, transition: { delay: 0.1 } }}
				className="flex flex-col flex-1 items-center justify-center h-full"
			>
				<Typography color="textSecondary" variant="h5">
					There is no such purchasereturn!
				</Typography>
				<Button
					className="mt-24"
					component={Link}
					variant="outlined"
					to="/apps/e-commerce/products"
					color="inherit"
				>
					Go to Purchase Return Page
				</Button>
			</motion.div>
		);
	}

	return (
		<FormProvider {...methods}>
			{UserPermissions.includes(PURCHASERETURN_CREATE) ||
			UserPermissions.includes(PURCHASERETURN_UPDATE) ||
			UserPermissions.includes(PURCHASERETURN_DELETE) ||
			UserPermissions.includes(PURCHASERETURN_DETAILS) ? (
				<FusePageCarded
					classes={{
						toolbar: 'p-0',
						header: 'min-h-80 h-80'
					}}
					header={<NewPurchaseReturnHeader />}
					content={
						<div className="p-16 sm:p-24">
							<PurchaseReturnForm />
						</div>
					}
					innerScroll
				/>
			) : (
				<PagenotFound />
			)}
		</FormProvider>
	);
};
export default withReducer('purchasereturnsManagement', reducer)(PurchaseReturn);
