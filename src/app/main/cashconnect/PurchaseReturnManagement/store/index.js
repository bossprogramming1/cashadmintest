import { combineReducers } from '@reduxjs/toolkit';
import purchasereturn from './purchaseReturnSlice';
import purchasereturns from './purchaseReturnsSlice';

const reducer = combineReducers({
	purchasereturn,
	purchasereturns
});

export default reducer;
