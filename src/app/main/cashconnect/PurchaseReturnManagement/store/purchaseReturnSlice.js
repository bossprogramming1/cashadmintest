import { createAsyncThunk, createSlice } from '@reduxjs/toolkit';
import axios from 'axios';
import {
	CREATE_PURCHASERETURN,
	DELETE_PURCHASERETURN,
	GET_PURCHASERETURNID,
	GET_PURCHASE_FINALS_ITMES_BY_INVOICE,
	UPDATE_PURCHASERETURN
} from '../../../../constant/constants';

export const getPurchaseReturn = createAsyncThunk(
	'purchasereturnManagement/purchasereturn/getPurchaseReturn',
	async (params, { rejectWithValue }) => {
		const authTOKEN = {
			headers: {
				'Content-type': 'application/json',
				Authorization: localStorage.getItem('jwt_access_token')
			}
		};

		try {
			const response = await axios.get(`${GET_PURCHASERETURNID}${params}`, authTOKEN);
			const data = await response.data;
			return data === undefined ? null : data;
		} catch (err) {
			return rejectWithValue(params);
		}
	}
);
export const getPurchaseFinalItem = createAsyncThunk(
	'purchasereturnManagement/purchasereturn/getPurchaseFinalItem',
	async (params, { rejectWithValue }) => {
		const authTOKEN = {
			headers: {
				'Content-type': 'application/json',
				Authorization: localStorage.getItem('jwt_access_token')
			}
		};

		try {
			const response = await axios.get(`${GET_PURCHASE_FINALS_ITMES_BY_INVOICE}${params}`, authTOKEN);
			const data = await response.data;
			return data === undefined ? null : data;
		} catch (err) {
			return rejectWithValue(params);
		}
	}
);

export const removePurchaseReturn = createAsyncThunk(
	'purchasereturnManagement/purchasereturn/removePurchaseReturn',
	async val => {
		const authTOKEN = {
			headers: {
				'Content-type': 'application/json',
				Authorization: localStorage.getItem('jwt_access_token')
			}
		};

		const purchasereturnId = val.id;
		const response = await axios.delete(`${DELETE_PURCHASERETURN}${purchasereturnId}`, authTOKEN);
		return response;
	}
);

export const updatePurchaseReturn = createAsyncThunk(
	'purchasereturnManagement/purchasereturn/updatePurchaseReturn',
	async (purchasereturnData, { dispatch, getState }) => {
		const { purchasereturn } = getState().purchasereturnsManagement;

		const authTOKEN = {
			headers: {
				'Content-type': 'application/json',
				Authorization: localStorage.getItem('jwt_access_token')
			}
		};
		const response = await axios.put(`${UPDATE_PURCHASERETURN}${purchasereturn.id}`, purchasereturnData, authTOKEN);
		return response;
	}
);

export const savePurchaseReturn = createAsyncThunk(
	'purchasereturnManagement/purchasereturn/savePurchaseReturn',
	async (purchasereturnData, { dispatch, getState }) => {
		const authTOKEN = {
			headers: {
				'Content-type': 'application/json',
				Authorization: localStorage.getItem('jwt_access_token')
			}
		};
		const response = await axios.post(`${CREATE_PURCHASERETURN}`, purchasereturnData, authTOKEN);
		return response;
	}
);

const purchasereturnSlice = createSlice({
	name: 'purchasereturnManagement/purchasereturn',
	initialState: null,
	reducers: {
		resetPurchaseReturn: () => null,
		newPurchaseReturn: {
			reducer: (state, action) => action.payload,
			prepare: event => ({
				payload: {
					invoice_no: ''
				}
			})
		}
	},
	extraReducers: {
		[getPurchaseReturn.fulfilled]: (state, action) => action.payload,
		[getPurchaseFinalItem.fulfilled]: (state, action) => action.payload,
		[savePurchaseReturn.fulfilled]: (state, action) => action.payload,
		[removePurchaseReturn.fulfilled]: (state, action) => action.payload,
		[updatePurchaseReturn.fulfilled]: (state, action) => action.payload
	}
});

export const { newPurchaseReturn, resetPurchaseReturn } = purchasereturnSlice.actions;

export default purchasereturnSlice.reducer;
