import { createAsyncThunk, createEntityAdapter, createSlice } from '@reduxjs/toolkit';
import axios from 'axios';
import { DELETE_PURCHASERETURN, GET_PURCHASERETURNS } from '../../../../constant/constants';

export const getPurchaseReturns = createAsyncThunk(
	'purchasereturnManagement/purchasereturns/getPurchaseReturns',
	async parameter => {
		const { page, size } = parameter;

		axios.defaults.headers.common['Content-type'] = 'application/json';
		axios.defaults.headers.common.Authorization = localStorage.getItem('jwt_access_token');

		try {
			const response = await axios.get(GET_PURCHASERETURNS, { params: { page, size } });

			sessionStorage.setItem('purchasereturns_total_elements', response.data.total_elements);
			sessionStorage.setItem('purchasereturns_total_pages', response.data.total_pages);

			delete axios.defaults.headers.common['Content-type'];
			delete axios.defaults.headers.common.Authorization;
			console.log('purchasereturnssf', response.da);

			return response.data?.purchase_return_items || [];
		} catch (error) {
			// Handle the error, log it or throw it if needed
			throw error;
		}
	}
);

export const removePurchaseReturns = createAsyncThunk(
	'purchasereturnManagement/purchasereturns/removePurchaseReturns',
	async (purchasereturnIds, { dispatch, getState }) => {
		const authTOKEN = {
			headers: {
				'Content-type': 'application/json',
				Authorization: localStorage.getItem('jwt_access_token')
			}
		};
		await axios.delete(`${DELETE_PURCHASERETURN}`, { purchasereturnIds }, authTOKEN);

		return purchasereturnIds;
	}
);

const purchasereturnsAdapter = createEntityAdapter({});

export const { selectAll: selectPurchaseReturns, selectById: selectPurchaseReturnById } =
	purchasereturnsAdapter.getSelectors(state => state.purchasereturnsManagement.purchasereturns);

const purchasereturnsSlice = createSlice({
	name: 'purchasereturnManagement/purchasereturns',
	initialState: purchasereturnsAdapter.getInitialState({
		searchText: ''
	}),
	reducers: {
		setPurchaseReturnsSearchText: {
			reducer: (state, action) => {
				state.searchText = action.payload;
			},
			prepare: event => ({ payload: event.target.value || '' })
		}
	},
	extraReducers: {
		[getPurchaseReturns.fulfilled]: purchasereturnsAdapter.setAll
	}
});

export const { setData, setPurchaseReturnsSearchText } = purchasereturnsSlice.actions;
export default purchasereturnsSlice.reducer;
