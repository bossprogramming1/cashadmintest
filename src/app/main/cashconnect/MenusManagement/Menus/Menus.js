import FusePageCarded from '@fuse/core/FusePageCarded';
import { MENU_ITEM_LIST } from 'app/constant/permission/permission';
import { getUserPermissions } from 'app/store/dataSlice';
import withReducer from 'app/store/withReducer';
import React, { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import PagenotFound from '../../Pagenotfound/PagenotFound';
import reducer from '../store/index';
import MenusHeader from './MenusHeader';
import MenusTable from './MenusTable';

const Menus = () => {
	const UserPermissions = useSelector(state => state.data.UserPermissions);
	useEffect(() => {
		dispatch(getUserPermissions());
	}, []);
	const dispatch = useDispatch();
	return (
		<FusePageCarded
			classes={{
				content: 'flex',
				contentCard: 'overflow-hidden',
				header: 'min-h-74 h-64'
			}}
			header={UserPermissions.includes(MENU_ITEM_LIST) && <MenusHeader />}
			content={UserPermissions.includes(MENU_ITEM_LIST) ? <MenusTable /> : <PagenotFound />}
			innerScroll
		/>
	);
};
export default withReducer('menusManagement', reducer)(Menus);
