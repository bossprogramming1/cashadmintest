import FusePageCarded from '@fuse/core/FusePageCarded';
import withReducer from 'app/store/withReducer';
import React from 'react';
import reducer from '../store/index';
import PermissionsHeader from './PermissionsHeader';
import PermissionsTable from './PermissionsTable';

const Permissions = () => {
	return (
		<FusePageCarded
			classes={{
				content: 'flex',
				contentCard: 'overflow-hidden',
				header: 'min-h-74 h-64'
			}}
			header={<PermissionsHeader />}
			content={<PermissionsTable />}
			innerScroll
		/>
	);
};
export default withReducer('permissionsManagement', reducer)(Permissions);
