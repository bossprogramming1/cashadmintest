import { createAsyncThunk, createEntityAdapter, createSlice } from '@reduxjs/toolkit';
import axios from 'axios';
import { DELETE_VENDOR, GET_VENDORS } from '../../../../constant/constants';

export const getVendors = createAsyncThunk('vendorManagement/vendors/geVendors', async () => {
	const authTOKEN = {
		headers: {
			'Content-type': 'application/json',
			Authorization: localStorage.getItem('jwt_access_token')
		}
	};
	const response = axios.get(GET_VENDORS, authTOKEN);
	const data = await response;
	return data.data.vendors;
});

export const removeVendors = createAsyncThunk(
	'vendorManagement/vendors/removeVendors',
	async (vendorIds, { dispatch, getState }) => {
		const authTOKEN = {
			headers: {
				'Content-type': 'application/json',
				Authorization: localStorage.getItem('jwt_access_token')
			}
		};
		await axios.delete(`${DELETE_VENDOR}`, { vendorIds }, authTOKEN);

		return vendorIds;
	}
);

const vendorsAdapter = createEntityAdapter({});

export const { selectAll: selectVendors, selectById: selectVendorById } = vendorsAdapter.getSelectors(
	state => state.vendorsManagement.vendors
);

const vendorsSlice = createSlice({
	name: 'vendorManagement/vendors',
	initialState: vendorsAdapter.getInitialState({
		searchText: ''
	}),
	reducers: {
		setVendorsSearchText: {
			reducer: (state, action) => {
				state.searchText = action.payload;
			},
			prepare: event => ({ payload: event.target.value || '' })
		}
	},
	extraReducers: {
		[getVendors.fulfilled]: vendorsAdapter.setAll
	}
});

export const { setData, setVendorsSearchText } = vendorsSlice.actions;
export default vendorsSlice.reducer;
