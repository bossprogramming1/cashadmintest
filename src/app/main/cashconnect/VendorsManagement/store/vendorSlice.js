import { createAsyncThunk, createSlice } from '@reduxjs/toolkit';
import axios from 'axios';
import { CREATE_VENDOR, DELETE_VENDOR, GET_VENDORID, UPDATE_VENDOR } from '../../../../constant/constants';

export const getVendor = createAsyncThunk('vendorManagement/vendor/getVendor', async (params, { rejectWithValue }) => {
	const authTOKEN = {
		headers: {
			'Content-type': 'application/json',
			Authorization: localStorage.getItem('jwt_access_token')
		}
	};

	try {
		const response = await axios.get(`${GET_VENDORID}${params}`, authTOKEN);
		const data = await response.data;
		return data === undefined ? null : data;
	} catch (err) {
		return rejectWithValue(params);
	}
});

export const removeVendor = createAsyncThunk(
	'vendorManagement/vendor/removeVendor',
	async (val, { dispatch, getState }) => {
		const authTOKEN = {
			headers: {
				'Content-type': 'application/json',
				Authorization: localStorage.getItem('jwt_access_token')
			}
		};

		const vendorId = val.id;
		await axios.delete(`${DELETE_VENDOR}${vendorId}`, authTOKEN);
	}
);

export const updateVendor = createAsyncThunk(
	'vendorManagement/vendor/updateVendor',
	async (vendorData, { dispatch, getState }) => {
		const { vendor } = getState().vendorsManagement;

		const convertVendorDataToFormData = jsonToFormData(vendorData);
		const authTOKEN = {
			headers: {
				'Content-type': 'application/json',
				Authorization: localStorage.getItem('jwt_access_token')
			}
		};
		const response = await axios.put(`${UPDATE_VENDOR}${vendor.id}`, convertVendorDataToFormData, authTOKEN);
	}
);

//buildformdata
const buildFormData = (formData, data, parentKey) => {
	if (data && typeof data === 'object' && !(data instanceof Date) && !(data instanceof File)) {
		Object.keys(data).forEach(key => {
			buildFormData(formData, data[key], parentKey ? `${parentKey}[${key}]` : key);
		});
	} else {
		const value = data === null ? '' : data;

		formData.append(parentKey, value);
	}
};

//convertJsonToFormData
const jsonToFormData = data => {
	const formData = new FormData();

	buildFormData(formData, data);
	return formData;
};

export const saveVendor = createAsyncThunk(
	'vendorManagement/vendor/saveVendor',
	async (vendorData, { dispatch, getState }) => {
		const convertVendorDataToFormData = jsonToFormData(vendorData);
		const authTOKEN = {
			headers: {
				'Content-type': 'application/json',
				Authorization: localStorage.getItem('jwt_access_token')
			}
		};
		const response = await axios.post(`${CREATE_VENDOR}`, convertVendorDataToFormData, authTOKEN);
	}
);

const vendorSlice = createSlice({
	name: 'vendorManagement/vendor',
	initialState: null,
	reducers: {
		resetVendor: () => null,
		newVendor: {
			reducer: (state, action) => action.payload,
			prepare: event => ({
				payload: {
					first_name: '',
					last_name: '',
					username: '',
					email: '',
					password: '',
					confirmPassword: '',
					gender: 0,
					primary_phone: '',
					secondary_phone: '',
					date_of_birth: '',
					is_active: false,
					street_address_one: '',
					street_address_two: '',
					thana: 0,
					city: 0,
					country: 0,
					postal_code: '',
					nid: '',
					branch: 0,
					is_online: false,
					image: '',
					country_code1: '+880',
					country_code2: '+880',
					show_country_code1: '+880',
					show_country_code2: '+880'
				}
			})
		}
	},
	extraReducers: {
		[getVendor.fulfilled]: (state, action) => action.payload,
		[saveVendor.fulfilled]: (state, action) => {
			localStorage.setItem('vendorAlert', 'saveVendor');
			return action.payload;
		},
		[removeVendor.fulfilled]: () => {
			localStorage.setItem('vendorAlert', 'deleteVendor');
		},
		[updateVendor.fulfilled]: () => {
			localStorage.setItem('vendorAlert', 'updateVendor');
		}
	}
});

export const { newVendor, resetVendor } = vendorSlice.actions;
export default vendorSlice.reducer;
