import { combineReducers } from '@reduxjs/toolkit';
import vendor from './vendorSlice';
import vendors from './vendorsSlice';

const reducer = combineReducers({
	vendor,
	vendors,
});

export default reducer;