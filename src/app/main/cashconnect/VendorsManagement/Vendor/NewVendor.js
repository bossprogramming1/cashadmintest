import FusePageCarded from '@fuse/core/FusePageCarded';
import { useDeepCompareEffect } from '@fuse/hooks';
import { yupResolver } from '@hookform/resolvers/yup';
import { Button, Typography } from '@material-ui/core';
import {
	CUSTOMER_CREATE,
	CUSTOMER_DELETE,
	CUSTOMER_DETAILS,
	CUSTOMER_UPDATE
} from 'app/constant/permission/permission';
import { getUserPermissions } from 'app/store/dataSlice';
import withReducer from 'app/store/withReducer';
import { motion } from 'framer-motion';
import React, { useEffect, useState } from 'react';
import { FormProvider, useForm } from 'react-hook-form';
import { useDispatch, useSelector } from 'react-redux';
import { Link, useParams } from 'react-router-dom';
import * as yup from 'yup';
import PagenotFound from '../../Pagenotfound/PagenotFound';
import reducer from '../store/index';
import { getVendor, newVendor, resetVendor } from '../store/vendorSlice';
import NewVendorHeader from './NewVendorHeader';
import VendorForm from './VendorForm';

/**
 * Form Validation Schema
 */
const schema = yup.object().shape({
	first_name: yup.string().required('First name is required').min(5, 'First name must be at least 5 characters'),
	last_name: yup.string().required('Last name is required'),

	password: yup.string().min(6, 'Password must be at least 6 characters').required('Password is required'),
	confirmPassword: yup
		.string()
		.required('Confirm password is required')
		.oneOf([yup.ref('password'), null], 'Passwords must match'),
	primary_phone: yup.string().min(11, 'Please enter your number with 0').required('Phone number is required')
	// thana: yup.number()
	//     .required('Police station is required'),
	// city: yup.number()
	//     .required('District is required'),
	// country: yup.number()
	//     .required('Country is required'),
	// cusotmer_type: yup.number()
	//     .required('Country is required')
});

const Vendor = () => {
	const dispatch = useDispatch();
	const vendor = useSelector(({ vendorsManagement }) => vendorsManagement.vendor);

	const [noVendor, setNoVendor] = useState(false);
	const methods = useForm({
		mode: 'onChange',
		defaultValues: {},
		resolver: yupResolver(schema)
	});
	const routeParams = useParams();
	const UserPermissions = useSelector(state => state.data.UserPermissions);
	useEffect(() => {
		dispatch(getUserPermissions());
	}, []);
	const { reset, watch, control, onChange, formState } = methods;
	const form = watch();

	useDeepCompareEffect(() => {
		function updateVendorState() {
			const { vendorId } = routeParams;

			if (vendorId === 'new') {
				localStorage.removeItem('event');
				/**
				 * Create New User data
				 */
				dispatch(newVendor());
			} else {
				/**
				 * Get User data
				 */

				dispatch(getVendor(vendorId)).then(action => {
					/**
					 * If the requested product is not exist show message
					 */
					if (!action.payload) {
						setNoVendor(true);
					}
				});
			}
		}

		updateVendorState();
	}, [dispatch, routeParams]);

	useEffect(() => {
		if (!vendor) {
			return;
		}
		/**
		 * Reset the form on vendor state changes
		 */
		reset({
			...vendor,
			branch: vendor?.branch?.id,
			thana: vendor?.thana?.id,
			city: vendor?.city?.id,
			country: vendor?.country?.id,
			role: vendor?.role?.id,
			user_type: vendor?.user_type?.id,
			country_code1: '+880',
			country_code2: '+880',
			show_country_code1: '+880',
			show_country_code2: '+880'
		});
	}, [vendor, reset]);

	useEffect(() => {
		return () => {
			/**
			 * Reset Vendor on component unload
			 */
			dispatch(resetVendor());
			setNoVendor(false);
		};
	}, [dispatch]);

	/**
	 * Show Message if the requested products is not exists
	 */
	if (noVendor) {
		return (
			<motion.div
				initial={{ opacity: 0 }}
				animate={{ opacity: 1, transition: { delay: 0.1 } }}
				className="flex flex-col flex-1 items-center justify-center h-full"
			>
				<Typography color="textSecondary" variant="h5">
					There is no such vendor!
				</Typography>
				<Button
					className="mt-24"
					component={Link}
					variant="outlined"
					to="/apps/e-commerce/products"
					color="inherit"
				>
					Go to Vendor Page
				</Button>
			</motion.div>
		);
	}

	return (
		<FormProvider {...methods}>
			{UserPermissions.includes(CUSTOMER_CREATE) ||
			UserPermissions.includes(CUSTOMER_UPDATE) ||
			UserPermissions.includes(CUSTOMER_DELETE) ||
			UserPermissions.includes(CUSTOMER_DETAILS) ? (
				<FusePageCarded
					classes={{
						toolbar: 'p-0',
						header: 'min-h-52 h-52 sm:h-136 sm:min-h-136'
					}}
					header={<NewVendorHeader />}
					content={
						<div className="p-16 sm:p-24">
							<VendorForm />
						</div>
					}
					innerScroll
				/>
			) : (
				<PagenotFound />
			)}
		</FormProvider>
	);
};
export default withReducer('vendorsManagement', reducer)(Vendor);
