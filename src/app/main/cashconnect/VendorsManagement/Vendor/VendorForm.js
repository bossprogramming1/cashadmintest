import Box from '@material-ui/core/Box';
import Checkbox from '@material-ui/core/Checkbox';
import FormControl from '@material-ui/core/FormControl';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Icon from '@material-ui/core/Icon';
import IconButton from '@material-ui/core/IconButton';
import InputAdornment from '@material-ui/core/InputAdornment';
import { makeStyles } from '@material-ui/core/styles';
import { KeyboardDatePicker } from '@material-ui/pickers';
import TextField from '@material-ui/core/TextField';
import { Autocomplete } from '@material-ui/lab';
import clsx from 'clsx';
import React, { useEffect, useState } from 'react';
import countryCodes from 'app/@data/countrycodes';
import useRemoveCountryCode from 'app/@helpers/removeCountryCode';
import { Controller, useFormContext } from 'react-hook-form';
import { useDispatch, useSelector } from 'react-redux';
import { useParams } from 'react-router-dom';
import { getBranches, getCities, getCountries, getCusotmerTypes, getRoles, getThanas } from 'app/store/dataSlice';
import { createTheme } from '@mui/material';
import CustomDatePicker from 'app/@components/CustomDatePicker';
import {
	BASE_URL,
	CHECK_EMAIL,
	CHECK_EMAIL_UPDATE,
	CHECK_PRIMARY_PHONE,
	CHECK_PRIMARY_PHONE_UPDATE,
	CHECK_SECONDARY_PHONE,
	CHECK_SECONDARY_PHONE_UPDATE,
	CHECK_USERNAME,
	CHECK_USERNAME_UPDATE
} from '../../../../constant/constants';

const useStyles = makeStyles(theme => ({
	hidden: {
		display: 'none'
	},
	productImageUpload: {
		transitionProperty: 'box-shadow',
		transitionDuration: theme.transitions.duration.short,
		transitionTimingFunction: theme.transitions.easing.easeInOut
	}
}));
const formLabelsTheme = createTheme({
	overrides: {
		MuiFormLabel: {
			asterisk: {
				color: '#db3131',
				'&$error': {
					color: '#db3131'
				}
			}
		}
	}
});
function VendorForm(props) {
	const [previewImage, setPreviewImage] = useState();
	const userID = localStorage.getItem('UserID');
	const classes = useStyles(props);
	const methods = useFormContext();
	const { control, formState, watch, setError, getValues, reset } = methods;
	const { errors } = formState;
	const routeParams = useParams();
	const { vendorId } = routeParams;
	const updateVendor = localStorage.getItem('updateVendor');
	const image = watch('image');
	const [showPassword, setShowPassword] = useState(false);
	const dispatch = useDispatch();
	const genders = [
		{ id: 'male', name: 'Male' },
		{ id: 'female', name: 'Female' },
		{ id: 'others', name: 'Others' }
	];
	const roles = useSelector(state => state.data.roles);
	const thanas = useSelector(state => state.data.thanas);
	const cities = useSelector(state => state.data.cities);
	const countrys = useSelector(state => state.data.countries);
	const branchs = useSelector(state => state.data.branches);
	const vendorTypes = useSelector(state => state.data.cusotmerTypes);
	const getVendorData = useSelector(({ vendorsManagement }) => vendorsManagement.vendor);
	const phoneNoPrimary = getVendorData?.primary_phone;
	const phoneNoSecondary = getVendorData?.secondary_phone;

	useEffect(() => {
		dispatch(getRoles());
		dispatch(getThanas());
		dispatch(getCountries());
		dispatch(getCities());
		dispatch(getBranches());
		dispatch(getCusotmerTypes());
		reset({
			...formState,
			country_code1: '+880',
			show_country_code1: '+880',
			country_code2: '+880',
			show_country_code2: '+880'
		});
	}, []);

	const getCountryCode1 = watch('country_code1');
	const getCountryCode2 = watch('country_code2');
useEffect(() => {
		reset({
			...getVendorData,

			country_code1: '+880',
			show_country_code1: '+880',
			country_code2: '+880',
			show_country_code2: '+880'
		});
	}, []);

	//check is already exists
	const handleOnChange = (field, event) => {

		if (vendorId === 'new') {
			//username
			if (event.target?.name === 'username') {
				const username = event.target?.value;
				fetch(`${CHECK_USERNAME}?username=${username}`)
					.then(res => res.json())
					.then(data => {
						if (data.username_exists) {
							setError('username', {
								type: 'manual',
								message: 'Username already exists'
							});
						}
					});
			}

			//email
			if (event.target?.name === 'email') {
				const email = event.target?.value;
				fetch(`${CHECK_EMAIL}?email=${email}`)
					.then(res => res.json())
					.then(data => {
						if (data.email_exists) {
							setError('email', {
								type: 'manual',
								message: 'Email already exists'
							});
						}
					});
			}

			//primary_phone
			if (event.target?.name === 'primary_phone') {
				const primaryPhn = event.target?.value;
				fetch(`${CHECK_PRIMARY_PHONE}?primary_phone=${getCountryCode1}${primaryPhn}`)
					.then(res => res.json())
					.then(data => {
						if (data.primary_phone_exists) {
							setError('primary_phone', {
								type: 'manual',
								message: 'Primary phone already exists'
							});
						}
					});
			}

			//secondary_phone
			if (event.target?.name === 'secondary_phone') {

				const secondaryPhn = event.target?.value;
				fetch(`${CHECK_SECONDARY_PHONE}?secondary_phone=${secondaryPhn}`)
					.then(res => res.json())
					.then(data => {
						if (data.secondary_phone_exists) {
							setError('secondary_phone', {
								type: 'manual',
								message: 'Secondary phone already exists'
							});
						}
					});
			}
		}
		if (vendorId !== 'new') {
			//username
			if (event.target?.name === 'username') {
				const username = event.target?.value;
				fetch(`${CHECK_USERNAME_UPDATE}?username=${username}&user=${vendorId}`)
					.then(res => res.json())
					.then(data => {
						if (data.username_exists) {
							setError('username', {
								type: 'manual',
								message: 'Username already exists'
							});
						}
					});
			}

			//email
			if (event.target?.name === 'email') {
				const email = event.target?.value;
				fetch(`${CHECK_EMAIL_UPDATE}?email=${email}&user=${vendorId}`)
					.then(res => res.json())
					.then(data => {
						if (data.email_exists) {
							setError('email', {
								type: 'manual',
								message: 'Email already exists'
							});
						}
					});
			}

			//primary_phone
			if (event.target?.name === 'primary_phone') {
				const primaryPhn = event.target?.value;
				fetch(`${CHECK_PRIMARY_PHONE_UPDATE}?primary_phone=${primaryPhn}&user=${vendorId}`)
					.then(res => res.json())
					.then(data => {
						if (data.primary_phone_exists) {
							setError('primary_phone', {
								type: 'manual',
								message: 'Primary phone already exists'
							});
						}
					});
			}

			//secondary_phone
			if (event.target?.name === 'secondary_phone') {

				const secondaryPhn = event.target?.value;
				fetch(`${CHECK_SECONDARY_PHONE_UPDATE}?secondary_phone=${secondaryPhn}&user=${vendorId}`)
					.then(res => res.json())
					.then(data => {
						if (data.secondary_phone_exists) {
							setError('secondary_phone', {
								type: 'manual',
								message: 'Secondary phone already exists'
							});
						}
					});
			}
		}
	};

	return (
		<div>
			<Controller
				name="first_name"
				control={control}
				render={({ field }) => {
					return (
						<TextField
							{...field}
							className="mt-8 mb-16"
							error={!!errors.first_name}
							helperText={errors?.first_name?.message}
							label="First Name"
							id="first_name"
							required
							autoFocus
							variant="outlined"
							InputLabelProps={field.value && { shrink: true }}
							fullWidth
						/>
					);
				}}
			/>
			<Controller
				name="last_name"
				control={control}
				render={({ field }) => {
					return (
						<TextField
							{...field}
							className="mt-8 mb-16"
							error={!!errors.last_name}
							helperText={errors?.last_name?.message}
							label="Last Name"
							id="last_name"
							required
							variant="outlined"
							InputLabelProps={field.value && { shrink: true }}
							fullWidth
						/>
					);
				}}
			/>
			<Box style={{ display: 'flex' }}>
				<Controller
					name="country_code1"
					control={control}
					render={({ field }) => (
						<TextField
							{...field}
							style={{ width: '150px' }}
							className="mt-8 mb-16"
							error={!!errors?.country_code1}
							readOnly
							helperText={errors?.country_code1?.message}
							onBlur={event => handleOnChange('country_code1', event)}
							label="Country"
							InputProps={{
								startAdornment: <img src="https://flagcdn.com/w20/bd.png" />
							}}
							id="show_country_code1"
							variant="outlined"
							InputLabelProps={{ shrink: true }}
						/>
					)}
				/>
				<Controller
					name="primary_phone"
					control={control}
					render={({ field }) => (
						<TextField
							{...field}
							className="mt-8 mb-16"
							error={!!errors.primary_phone}
							// value={getCountryCode1}
							// onChange={e=>{setCountry_code(e.target.value)}}
							helperText={errors?.primary_phone?.message}
							onBlur={event => handleOnChange('primary_phone', event)}
							label="Phone"
							required
							// autocomplete={false}
							id="primary_phone"
							variant="outlined"
							fullWidth
							InputLabelProps={{ shrink: true }}
						/>
					)}
				/>
			</Box>
			<Controller
				name="password"
				control={control}
				render={({ field }) => (
					<TextField
						{...field}
						className="mt-8 mb-16"
						label="Password"
						type="password"
						error={!!errors.password}
						style={vendorId !== 'new' ? { display: 'none' } : { display: 'block' }}
						helperText={errors?.password?.message}
						variant="outlined"
						fullWidth
						InputProps={{
							className: 'pr-2',
							type: showPassword ? 'text' : 'password',
							endAdornment: (
								<InputAdornment position="end">
									<IconButton onClick={() => setShowPassword(!showPassword)}>
										<Icon className="text-20" color="action">
											{showPassword ? 'visibility' : 'visibility_off'}
										</Icon>
									</IconButton>
								</InputAdornment>
							)
						}}
						required
						InputLabelProps={{ shrink: true }}
					/>
				)}
			/>
			<Controller
				name="confirmPassword"
				control={control}
				render={({ field }) => (
					<TextField
						{...field}
						className="mt-8 mb-16"
						label="Confirm Password"
						type="password"
						error={!!errors.confirmPassword}
						style={vendorId !== 'new' ? { display: 'none' } : { display: 'block' }}
						helperText={errors?.confirmPassword?.message}
						variant="outlined"
						fullWidth
						InputProps={{
							className: 'pr-2',
							type: showPassword ? 'text' : 'password',
							endAdornment: (
								<InputAdornment position="end">
									<IconButton onClick={() => setShowPassword(!showPassword)}>
										<Icon className="text-20" color="action">
											{showPassword ? 'visibility' : 'visibility_off'}
										</Icon>
									</IconButton>
								</InputAdornment>
							)
						}}
						required
						InputLabelProps={{ shrink: true }}
					/>
				)}
			/>

			<Controller
				name="username"
				control={control}
				render={({ field }) => {
					return (
						<TextField
							{...field}
							className="mt-8 mb-16"
							error={!!errors.username}
							helperText={errors?.username?.message}
							onBlur={event => handleOnChange('username', event)}
							label="User Name"
							id="username"
							variant="outlined"
							InputLabelProps={field.value && { shrink: true }}
							fullWidth
						/>
					);
				}}
			/>

			<Controller
				name="email"
				control={control}
				render={({ field }) => {
					return (
						<TextField
							{...field}
							className="mt-8 mb-16"
							error={!!errors.email}
							helperText={errors?.email?.message}
							onBlur={event => handleOnChange('email', event)}
							label="Email"
							id="email"
							variant="outlined"
							InputLabelProps={field.value && { shrink: true }}
							fullWidth
						/>
					);
				}}
			/>
			<Controller
				name="gender"
				control={control}
				render={({ field: { onChange, value, name } }) => (
					<Autocomplete
						className="mt-8 mb-16"
						freeSolo
						value={value ? genders.find(data => data.id === value) : null}
						options={genders}
						getOptionLabel={option => `${option?.name}`}
						onChange={(event, newValue) => {
							onChange(newValue?.id);
						}}
						renderInput={params => (
							<TextField
								{...params}
								placeholder="Select Gender"
								label="Gender"
								error={!!errors.gender}
								helperText={errors?.gender?.message}
								variant="outlined"
								InputLabelProps={{
									shrink: true
								}}
							/>
						)}
					/>
				)}
			/>

			<Controller
				name="date_of_birth"
				control={control}
				render={({ field }) => <CustomDatePicker field={field} label="Birthday" />}
			/>
			<Controller
				name="is_active"
				control={control}
				render={({ field }) => (
					<FormControl>
						<FormControlLabel
							label="Is active"
							control={<Checkbox {...field} checked={field.value ? field.value : false} />}
						/>
					</FormControl>
				)}
			/>

			<Controller
				name="street_address_one"
				control={control}
				render={({ field }) => {
					return (
						<TextField
							{...field}
							className="mt-8 mb-16"
							error={!!errors?.street_address_one}
							helperText={errors?.street_address_one?.message}
							label="Primary address"
							id="street_address_one"
							variant="outlined"
							InputLabelProps={field.value && { shrink: true }}
							fullWidth
						/>
					);
				}}
			/>
			<Controller
				name="street_address_two"
				control={control}
				render={({ field }) => (
					<TextField
						{...field}
						className="mt-8 mb-16"
						error={!!errors.street_address_two}
						helperText={errors?.street_address_two?.message}
						label="Secondary address"
						id="street_address_two"
						variant="outlined"
						fullWidth
						InputLabelProps={{ shrink: true }}
					/>
				)}
			/>

			<Controller
				name="thana"
				control={control}
				render={({ field: { onChange, value, name } }) => (
					<Autocomplete
						className="mt-8 mb-16"
						freeSolo
						value={value ? thanas.find(data => data.id === value) : null}
						options={thanas}
						getOptionLabel={option => `${option?.name}`}
						onChange={(event, newValue) => {
							onChange(newValue?.id);
						}}
						renderInput={params => (
							<TextField
								{...params}
								placeholder="Select Thana"
								label="Thana"
								error={!!errors?.thana}
								helperText={errors?.thana?.message}
								variant="outlined"
								InputLabelProps={{
									shrink: true
								}}
							/>
						)}
					/>
				)}
			/>

			<Controller
				name="city"
				control={control}
				render={({ field: { onChange, value, name } }) => (
					<Autocomplete
						className="mt-8 mb-16"
						freeSolo
						value={value ? cities?.find(data => data.id === value) : null}
						options={cities}
						getOptionLabel={option => `${option?.name}`}
						onChange={(event, newValue) => {
							onChange(newValue?.id);
						}}
						renderInput={params => (
							<TextField
								{...params}
								placeholder="Select City"
								label="City"
								error={!!errors?.city}
								helperText={errors?.city?.message}
								variant="outlined"
								InputLabelProps={{
									shrink: true
								}}
							/>
						)}
					/>
				)}
			/>

			<Controller
				name="country"
				control={control}
				render={({ field: { onChange, value, name } }) => (
					<Autocomplete
						className="mt-8 mb-16"
						freeSolo
						value={value ? countrys.find(data => data.id === value) : null}
						options={countrys}
						getOptionLabel={option => `${option?.name}`}
						onChange={(event, newValue) => {
							onChange(newValue?.id);
						}}
						renderInput={params => (
							<TextField
								{...params}
								placeholder="Select Country"
								label="Country"
								error={!!errors?.country}
								helperText={errors?.country?.message}
								variant="outlined"
								InputLabelProps={{
									shrink: true
								}}
							/>
						)}
					/>
				)}
			/>

			<Controller
				name="postal_code"
				control={control}
				render={({ field }) => {
					return (
						<TextField
							{...field}
							className="mt-8 mb-16"
							error={!!errors.postal_code}
							helperText={errors?.postal_code?.message}
							label="Postal Code"
							id="postal_code"
							variant="outlined"
							InputLabelProps={field.value && { shrink: true }}
							fullWidth
						/>
					);
				}}
			/>

			<Controller
				name="nid"
				control={control}
				render={({ field }) => {
					return (
						<TextField
							{...field}
							className="mt-8 mb-16"
							error={!!errors.nid}
							helperText={errors?.nid?.message}
							label="NID"
							id="nid"
							variant="outlined"
							InputLabelProps={field.value && { shrink: true }}
							fullWidth
						/>
					);
				}}
			/>

			<Controller
				name="branch"
				control={control}
				render={({ field: { onChange, value, name } }) => (
					<Autocomplete
						className="mt-8 mb-16"
						freeSolo
						value={value ? branchs.find(data => data.id === value) : null}
						options={branchs}
						getOptionLabel={option => `${option?.name}`}
						onChange={(event, newValue) => {
							onChange(newValue?.id);
						}}
						renderInput={params => (
							<TextField
								{...params}
								placeholder="Select Branch"
								label="Branch"
								error={!!errors?.branch}
								helperText={errors?.branch?.message}
								variant="outlined"
								InputLabelProps={{
									shrink: true
								}}
							/>
						)}
					/>
				)}
			/>

			<Controller
				name="is_online"
				control={control}
				render={({ field }) => (
					<FormControl>
						<FormControlLabel
							label="Is Online"
							control={<Checkbox {...field} checked={field.value ? field.value : false} />}
						/>
					</FormControl>
				)}
			/>

			<div className="flex justify-center sm:justify-start flex-wrap -mx-16">
				<Controller
					name="image"
					control={control}
					render={({ field: { onChange, value } }) => (
						<label
							htmlFor="button-file"
							className={clsx(
								classes.productImageUpload,
								'flex items-center justify-center relative w-128 h-128 rounded-16 mx-12 mb-24 overflow-hidden cursor-pointer shadow hover:shadow-lg'
							)}
						>
							<input
								accept="image/*"
								className="hidden"
								id="button-file"
								type="file"
								onChange={async e => {
									const reader = new FileReader();
									reader.onload = () => {
										if (reader.readyState === 2) {
											setPreviewImage(reader.result);
										}
									};
									reader.readAsDataURL(e.target.files[0]);

									const file = e.target.files[0];
									onChange(file);
								}}
							/>
							<Icon fontSize="large" color="action">
								cloud_upload
							</Icon>
						</label>
					)}
				/>
				{image && !previewImage && <img src={`${BASE_URL}${image}`} alt="Not Found" />}

				<div style={{ width: '100px', height: '100px' }}>
					<img src={previewImage} alt="Not Found" />
				</div>
			</div>
		</div>
	);
}

export default VendorForm;
