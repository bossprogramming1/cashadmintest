import FuseLoading from '@fuse/core/FuseLoading';
import FuseScrollbars from '@fuse/core/FuseScrollbars';
import _ from '@lodash';
import Checkbox from '@material-ui/core/Checkbox';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import LockOpenIcon from '@mui/icons-material/LockOpen';
import TablePagination from '@material-ui/core/TablePagination';
import TableRow from '@material-ui/core/TableRow';
import DeleteIcon from '@material-ui/icons/Delete';
import EditIcon from '@material-ui/icons/Edit';
import React, { useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { withRouter } from 'react-router-dom';
import { BASE_URL, SEARCH_VENDOR } from '../../../../constant/constants';
import { getVendors, selectVendors } from '../store/vendorsSlice';
import VendorsTableHead from './VendorsTableHead';

const VendorsTable = props => {
	const dispatch = useDispatch();
	const vendors = useSelector(selectVendors);
	const searchText = useSelector(({ vendorsManagement }) => vendorsManagement.vendors.searchText);
	const [searchVendor, setSearchVendor] = useState([]);
	const [loading, setLoading] = useState(true);
	const [selected, setSelected] = useState([]);
	const [page, setPage] = useState(0);
	const [rowsPerPage, setRowsPerPage] = useState(10);
	const [order, setOrder] = useState({
		direction: 'asc',
		id: null
	});
	let serialNumber = 1;
	const [deleteItem, setDeleteItem] = useState('');
	useEffect(() => {
		dispatch(getVendors()).then(() => setLoading(false));
	}, [dispatch]);
	const user_role = localStorage.getItem('user_role');

	//searchVendor
	useEffect(() => {
		searchText !== '' && getSearchVendor();
	}, [searchText]);

	const getSearchVendor = () => {
		fetch(`${SEARCH_VENDOR}?username=${searchText}`)
			.then(response => response.json())
			.then(searchedVendorData => {
				setSearchVendor(searchedVendorData.vendors);
			});
	};

	function handleRequestSort(vendorEvent, property) {
		const id = property;
		let direction = 'desc';

		if (order.id === property && order.direction === 'desc') {
			direction = 'asc';
		}

		setOrder({
			direction,
			id
		});
	}

	function handleSelectAllClick(vendorEvent) {
		if (vendorEvent.target.checked) {
			setSelected(vendors.map(n => n.id));
			return;
		}
		setSelected([]);
	}

	function handleDeselect() {
		setSelected([]);
	}

	function handleUpdateVendor(item, event) {
		localStorage.removeItem('vendorEvent');
		localStorage.setItem('updateVendor', event);
		props.history.push(`/apps/vendor-management/vendors/${item.id}/${item?.name}`);
	}
	function handleDeleteVendor(item, vendorEvent) {
		localStorage.removeItem('vendorEvent');
		localStorage.removeItem('updateVendor');
		localStorage.setItem('vendorEvent', vendorEvent);
		props.history.push(`/apps/vendor-management/vendors/${item.id}/${item?.name}`);
	}
	function handleOTPsend(item, otpEvent) {
		localStorage.removeItem('vendorEvent');
		localStorage.removeItem('updateVendor');
		localStorage.setItem('otpEvent', otpEvent);
		props.history.push(`/apps/vendor-otp-management/vendors/${item.id}/${item?.name}`);
	}

	function handleCheck(vendorEvent, id) {
		const selectedIndex = selected.indexOf(id);
		let newSelected = [];

		if (selectedIndex === -1) {
			newSelected = newSelected.concat(selected, id);
		} else if (selectedIndex === 0) {
			newSelected = newSelected.concat(selected.slice(1));
		} else if (selectedIndex === selected.length - 1) {
			newSelected = newSelected.concat(selected.slice(0, -1));
		} else if (selectedIndex > 0) {
			newSelected = newSelected.concat(selected.slice(0, selectedIndex), selected.slice(selectedIndex + 1));
		}

		setSelected(newSelected);
	}

	function handleChangePage(vendorEvent, value) {
		setPage(value);
	}

	function handleChangeRowsPerPage(vendorEvent) {
		setRowsPerPage(vendorEvent.target.value);
	}

	if (loading) {
		return <FuseLoading />;
	}

	return (
		<div className="w-full flex flex-col">
			<FuseScrollbars className="flex-grow overflow-x-auto">
				<Table stickyHeader className="min-w-xl" aria-labelledby="tableTitle">
					<VendorsTableHead
						selectedVendorIds={selected}
						order={order}
						onSelectAllClick={handleSelectAllClick}
						onRequestSort={handleRequestSort}
						rowCount={vendors.length}
						onMenuItemClick={handleDeselect}
					/>

					<TableBody>
						{_.orderBy(
							searchText !== '' && searchVendor ? searchVendor : vendors,
							[
								o => {
									switch (order.id) {
										case 'categories': {
											return o.categories[0];
										}
										default: {
											return o[order.id];
										}
									}
								}
							],
							[order.direction]
						)
							.slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
							.map(n => {
								const isSelected = selected.indexOf(n.id) !== -1;
								return (
									<TableRow
										hover
										role="checkbox"
										aria-checked={isSelected}
										tabIndex={-1}
										key={n.id}
										selected={isSelected}
									>
										<TableCell
											whitespace-nowrap
											className="w-40 md:w-64 text-center"
											padding="none"
										>
											<Checkbox
												checked={isSelected}
												onClick={vendorEvent => vendorEvent.stopPropagation()}
												onChange={vendorEvent => handleCheck(vendorEvent, n.id)}
											/>
										</TableCell>

										<TableCell
											whitespace-nowrap
											className="w-40 md:w-64"
											component="th"
											scope="row"
										>
											{serialNumber++}
										</TableCell>

										<TableCell
											whitespace-nowrap
											className="w-52 px-4 md:px-4"
											component="th"
											scope="row"
										>
											{n.length > 0 && n.featuredImageId ? (
												<img
													className="w-full block rounded"
													style={{ borderRadius: '50%', height: '50px', width: '50px' }}
													src={_.find(n.image, { id: n.featuredImageId }).url}
													alt={n?.name}
												/>
											) : (
												<img
													className="w-full block rounded"
													style={{ borderRadius: '50%', height: '50px', width: '50px' }}
													src={`${BASE_URL}${n.image}`}
													alt={n?.name}
												/>
											)}
										</TableCell>

										<TableCell className="whitespace-nowrap p-4 md:p-16" component="th" scope="row">
											{n?.branch?.name}
										</TableCell>

										<TableCell className="whitespace-nowrap p-4 md:p-16" component="th" scope="row">
											{n.first_name} &nbsp; {n.last_name}
										</TableCell>

										<TableCell className="whitespace-nowrap p-4 md:p-16" component="th" scope="row">
											{n.username}
										</TableCell>
										<TableCell className="whitespace-nowrap p-4 md:p-16" component="th" scope="row">
											{n.email}
										</TableCell>

										<TableCell className="whitespace-nowrap p-4 md:p-16" component="th" scope="row">
											{n.primary_phone}
										</TableCell>

										<TableCell
											whitespace-nowrap
											className="p-4 md:p-16"
											align="center"
											component="th"
											scope="row"
										>
											<div>
												<EditIcon
													onClick={vendorEvent => handleUpdateVendor(n, 'Update')}
													className="h-52 cursor-pointer"
													style={{ color: 'green' }}
												/>
												<DeleteIcon
													onClick={event => handleDeleteVendor(n, 'Delete')}
													className="h-52 cursor-pointer"
													style={{
														color: 'red',
														visibility:
															user_role === 'ADMIN' || user_role === 'admin'
																? 'visible'
																: 'hidden'
													}}
												/>
												<LockOpenIcon
													onClick={event => handleOTPsend(n, 'sendOTP')}
													className="h-52 cursor-pointer"
													style={{ color: 'black' }}
												/>
											</div>
										</TableCell>
									</TableRow>
								);
							})}
					</TableBody>
				</Table>
			</FuseScrollbars>

			<TablePagination
				className="flex-shrink-0 border-t-1"
				rowsPerPageOptions={[5, 10, 25]}
				component="div"
				count={vendors.length}
				rowsPerPage={rowsPerPage}
				page={page}
				backIconButtonProps={{
					'aria-label': 'Previous Page'
				}}
				nextIconButtonProps={{
					'aria-label': 'Next Page'
				}}
				onChangePage={handleChangePage}
				onChangeRowsPerPage={handleChangeRowsPerPage}
			/>
		</div>
	);
};

export default withRouter(VendorsTable);
