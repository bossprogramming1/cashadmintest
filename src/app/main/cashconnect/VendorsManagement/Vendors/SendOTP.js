import Box from '@material-ui/core/Box';
import Checkbox from '@material-ui/core/Checkbox';
import FormControl from '@material-ui/core/FormControl';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Icon from '@material-ui/core/Icon';
import IconButton from '@material-ui/core/IconButton';
import InputAdornment from '@material-ui/core/InputAdornment';
import { makeStyles } from '@material-ui/core/styles';
import { KeyboardDatePicker } from '@material-ui/pickers';
import TextField from '@material-ui/core/TextField';
import { Autocomplete } from '@material-ui/lab';
import clsx from 'clsx';
import ArrowBackIcon from '@mui/icons-material/ArrowBack';

import React, { useEffect, useState } from 'react';
import countryCodes from 'app/@data/countrycodes';
import useRemoveCountryCode from 'app/@helpers/removeCountryCode';
import { Controller, useFormContext } from 'react-hook-form';
import { useDispatch, useSelector } from 'react-redux';
import { useParams, useHistory } from 'react-router-dom';
import { getBranches, getCities, getCountries, getCusotmerTypes, getRoles, getThanas } from 'app/store/dataSlice';
import { Button, Typography } from '@material-ui/core';
import axios from 'axios';
import {
	BASE_URL,
	CHECK_EMAIL,
	CHECK_EMAIL_UPDATE,
	CHECK_PRIMARY_PHONE,
	CHECK_PRIMARY_PHONE_UPDATE,
	CHECK_SECONDARY_PHONE,
	CHECK_SECONDARY_PHONE_UPDATE,
	CHECK_USERNAME,
	CHECK_USERNAME_UPDATE,
	GET_PHONE_VARIFICATION_CODE_BY_VENDOR,
	GET_VENDORID
} from '../../../../constant/constants';

const useStyles = makeStyles(theme => ({
	hidden: {
		display: 'none'
	},
	productImageUpload: {
		transitionProperty: 'box-shadow',
		transitionDuration: theme.transitions.duration.short,
		transitionTimingFunction: theme.transitions.easing.easeInOut
	}
}));
function VendorForm(props) {
	const history = useHistory();
	const routeParams = useParams();
	const { vendorId } = routeParams;
	const [vendor, setVendor] = useState([]);
	const [OTP, setSetOTP] = useState('');

	const authTOKEN = {
		headers: {
			'Content-type': 'application/json',
			Authorization: localStorage.getItem('jwt_access_token')
		}
	};

	const handleOtpSubmit = async () => {
		if (localStorage.getItem('otpEvent')) {
			axios
				.get(
					`${GET_PHONE_VARIFICATION_CODE_BY_VENDOR}?primary_phone=${vendor.primary_phone}&phone_otp=${OTP}`,
					authTOKEN
				)
				.then(res => {});

			localStorage.removeItem('otpEvent');
			localStorage.setItem('sendConfirmOTP', 'sendConfirmOTP');
		}

		history.goBack();
	};

	useEffect(() => {
		fetch(`${GET_VENDORID}${vendorId}`, authTOKEN)
			.then(response => response.json())
			.then(data => setVendor(data));
	}, []);

	//check is already exists
	const handleOnChange = (field, event) => {};

	return (
		<>
			<Box
				style={{
					display: 'flex',
					justifyContent: 'space-between',
					flexDirection: 'row',
					marginRight: '150px',
					marginLeft: '150px'
				}}
			>
				<Button onClick={() => history.goBack()} style={{ fontSize: '17px' }}>
					<ArrowBackIcon
						className="h-52 cursor-pointer"
						style={{
							color: 'black',
							textAlign: 'left',

							height: '30px',
							width: '30px'
						}}
					/>
					Go Back
				</Button>
			</Box>

			<Box>
				<div
					style={{
						display: 'flex',
						flexDirection: 'column',
						alignItems: 'center',
						marginTop: '80px'
					}}
				>
					<form>
						<Typography variant="h6">Send OTP</Typography>
						<br />
						<TextField
							style={{ width: '250px', margin: '5px' }}
							name="name"
							type="text"
							label="Email"
							variant="outlined"
							InputLabelProps={{
								shrink: true
							}}
							value={`${vendor.primary_phone || ''}`}
						/>
						<br />
						<TextField
							style={{ width: '250px', margin: '5px' }}
							name="phone_otp"
							type="text"
							label="OTP"
							variant="outlined"
							onChange={event => {
								setSetOTP(event.target.value);
							}}
							InputLabelProps={{
								shrink: true
							}}
						/>

						<br />
						<Button variant="contained" color="primary" onClick={handleOtpSubmit}>
							Send OTP
						</Button>
					</form>
				</div>
			</Box>
		</>
	);
}

export default VendorForm;
