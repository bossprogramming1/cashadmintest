import FusePageCarded from '@fuse/core/FusePageCarded';
import { CUSTOMER_LIST } from 'app/constant/permission/permission';
import { getUserPermissions } from 'app/store/dataSlice';
import withReducer from 'app/store/withReducer';
import React, { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import PagenotFound from '../../Pagenotfound/PagenotFound';
import reducer from '../store/index';
import VendorsHeader from './VendorsHeader';
import VendorsTable from './VendorsTable';

const Vendors = () => {
	const UserPermissions = useSelector(state => state.data.UserPermissions);
	useEffect(() => {
		dispatch(getUserPermissions());
	}, []);
	const dispatch = useDispatch();
	return (
		<FusePageCarded
			classes={{
				content: 'flex',
				contentCard: 'overflow-hidden',
				header: 'min-h-74 h-64'
			}}
			header={UserPermissions.includes(CUSTOMER_LIST) && <VendorsHeader />}
			content={UserPermissions.includes(CUSTOMER_LIST) ? <VendorsTable /> : <PagenotFound />}
			innerScroll
		/>
	);
};
export default withReducer('vendorsManagement', reducer)(Vendors);
