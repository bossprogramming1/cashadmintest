import FusePageCarded from '@fuse/core/FusePageCarded';
import { useDeepCompareEffect } from '@fuse/hooks';
import { yupResolver } from '@hookform/resolvers/yup';
import { Button, Typography } from '@material-ui/core';
import {
	MANUFACTURER_CREATE,
	MANUFACTURER_DELETE,
	MANUFACTURER_DETAILS,
	MANUFACTURER_UPDATE
} from 'app/constant/permission/permission';
import { getUserPermissions } from 'app/store/dataSlice';
import withReducer from 'app/store/withReducer';
import { motion } from 'framer-motion';
import React, { useEffect, useState } from 'react';
import { FormProvider, useForm } from 'react-hook-form';
import { useDispatch, useSelector } from 'react-redux';
import { Link, useParams } from 'react-router-dom';
import * as yup from 'yup';
import PagenotFound from '../../Pagenotfound/PagenotFound';
import reducer from '../store/index';
import { getManufacturer, newManufacturer, resetManufacturer } from '../store/manufacturerSlice';
import ManufacturerForm from './ManufacturerForm';
import NewManufacturerHeader from './NewManufacturerHeader';

/**
 * Form Validation Schema
 */
const schema = yup.object().shape({
	name: yup.string().required('Name is required')
});

const Manufacturer = () => {
	const dispatch = useDispatch();
	const manufacturer = useSelector(({ manufacturersManagement }) => manufacturersManagement.manufacturer);

	const [noManufacturer, setNoManufacturer] = useState(false);
	const methods = useForm({
		mode: 'onChange',
		defaultValues: {},
		resolver: yupResolver(schema)
	});
	const routeParams = useParams();
	const UserPermissions = useSelector(state => state.data.UserPermissions);
	useEffect(() => {
		dispatch(getUserPermissions());
	}, []);
	const { reset, watch, control, onChange, formState } = methods;
	const form = watch();

	useDeepCompareEffect(() => {
		function updateManufacturerState() {
			const { manufacturerId } = routeParams;

			if (manufacturerId === 'new') {
				localStorage.removeItem('event');
				/**
				 * Create New User data
				 */
				dispatch(newManufacturer());
			} else {
				/**
				 * Get User data
				 */

				dispatch(getManufacturer(manufacturerId)).then(action => {
					/**
					 * If the requested product is not exist show message
					 */
					if (!action.payload) {
						setNoManufacturer(true);
					}
				});
			}
		}

		updateManufacturerState();
	}, [dispatch, routeParams]);

	useEffect(() => {}, []);

	useEffect(() => {
		if (!manufacturer) {
			return;
		}
		/**
		 * Reset the form on manufacturer state changes
		 */
		reset(manufacturer);
	}, [manufacturer, reset]);

	useEffect(() => {
		return () => {
			/**
			 * Reset Manufacturer on component unload
			 */
			dispatch(resetManufacturer());
			setNoManufacturer(false);
		};
	}, [dispatch]);

	/**
	 * Show Message if the requested products is not exists
	 */
	if (noManufacturer) {
		return (
			<motion.div
				initial={{ opacity: 0 }}
				animate={{ opacity: 1, transition: { delay: 0.1 } }}
				className="flex flex-col flex-1 items-center justify-center h-full"
			>
				<Typography color="textSecondary" variant="h5">
					There is no such manufacturer!
				</Typography>
				<Button
					className="mt-24"
					component={Link}
					variant="outlined"
					to="/apps/e-commerce/products"
					color="inherit"
				>
					Go to Manufacturer Page
				</Button>
			</motion.div>
		);
	}

	return (
		<FormProvider {...methods}>
			{UserPermissions.includes(MANUFACTURER_CREATE) ||
			UserPermissions.includes(MANUFACTURER_UPDATE) ||
			UserPermissions.includes(MANUFACTURER_DELETE) ||
			UserPermissions.includes(MANUFACTURER_DETAILS) ? (
				<FusePageCarded
					classes={{
						content: 'flex',
						contentCard: 'overflow-hidden',
						header: 'min-h-74 h-64'
					}}
					header={<NewManufacturerHeader />}
					content={
						<div className="p-16 sm:p-24" style={{ width: '100%' }}>
							<ManufacturerForm />
						</div>
					}
					innerScroll
				/>
			) : (
				<PagenotFound />
			)}
		</FormProvider>
	);
};
export default withReducer('manufacturersManagement', reducer)(Manufacturer);
