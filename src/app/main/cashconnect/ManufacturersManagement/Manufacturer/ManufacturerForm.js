import Icon from '@material-ui/core/Icon';
import { makeStyles } from '@material-ui/core/styles';
import TextField from '@material-ui/core/TextField';
import clsx from 'clsx';
import React, { useState } from 'react';
import { Controller, useFormContext } from 'react-hook-form';
import { useParams } from 'react-router-dom';
import { BASE_URL } from '../../../../constant/constants';

const useStyles = makeStyles(theme => ({
	hidden: {
		display: 'none'
	},
	productImageUpload: {
		transitionProperty: 'box-shadow',
		transitionDuration: theme.transitions.duration.short,
		transitionTimingFunction: theme.transitions.easing.easeInOut
	}
}));

function ManufacturerForm(props) {
	const userID = localStorage.getItem('UserID');

	const classes = useStyles(props);

	const methods = useFormContext();
	const { control, formState, watch } = methods;
	const { errors } = formState;
	const image = watch('image');
	const [previewImage, setPreviewImage] = useState();
	const routeParams = useParams();
	const { manufacturerId } = routeParams;

	return (
		<div>
			<Controller
				name="name"
				control={control}
				render={({ field }) => {
					return (
						<TextField
							{...field}
							className="mt-8 mb-16"
							error={!!errors?.name}
							helperText={errors?.name?.message}
							label="Name"
							id="name"
							required
							variant="outlined"
							InputLabelProps={field.value && { shrink: true }}
							fullWidth
						/>
					);
				}}
			/>

			<Controller
				name="description"
				control={control}
				render={({ field }) => {
					return (
						<TextField
							{...field}
							className="mt-8 mb-16"
							error={!!errors.description}
							required
							helperText={errors?.description?.message}
							label="Description"
							id="description"
							variant="outlined"
							InputLabelProps={field.value && { shrink: true }}
							fullWidth
						/>
					);
				}}
			/>
			<div className="flex justify-center sm:justify-start flex-wrap -mx-16">
				<Controller
					name="image"
					control={control}
					render={({ field: { onChange, value } }) => (
						<label
							htmlFor="button-file"
							className={clsx(
								classes.productImageUpload,
								'flex items-center justify-center relative w-128 h-128 rounded-16 mx-12 mb-24 overflow-hidden cursor-pointer shadow hover:shadow-lg'
							)}
						>
							<input
								accept="image/*"
								className="hidden"
								id="button-file"
								type="file"
								onChange={async e => {
									
									const reader = new FileReader();
									reader.onload = () => {
										if (reader.readyState === 2) {
											setPreviewImage(reader.result);
										}
									};
									reader.readAsDataURL(e.target.files[0]);
									const file = e.target.files[0];
									onChange(file);
								}}
							/>
							<Icon fontSize="large" color="action">
								cloud_upload
							</Icon>
						</label>
					)}
				/>
				{image && !previewImage && (
					<img
						src={`${BASE_URL}${image}`}
						id="image"
						style={{ width: '100px', height: '100px' }}
						alt="Not found"
					/>
				)}

				<div>
					<img
						style={{ width: '100px', height: '100px' }}
						src={previewImage}
						alt="Not found"
						//alt="no image found"
					/>
				</div>
			</div>
		</div>
	);
}

export default ManufacturerForm;
