import { createAsyncThunk, createSlice } from '@reduxjs/toolkit';
import axios from 'axios';
import {
	CREATE_MANUFACTURER,
	DELETE_MANUFACTURER,
	GET_MANUFACTURERID,
	UPDATE_MANUFACTURER
} from '../../../../constant/constants';

export const getManufacturer = createAsyncThunk(
	'manufacturerManagement/manufacturer/getManufacturer',
	async (params, { rejectWithValue }) => {
		const authTOKEN = {
			headers: {
				'Content-type': 'application/json',
				Authorization: localStorage.getItem('jwt_access_token')
			}
		};

		try {
			const response = await axios.get(`${GET_MANUFACTURERID}${params}`, authTOKEN);
			const data = await response.data;
			return data === undefined ? null : data;
		} catch (err) {
			return rejectWithValue(params);
		}
	}
);

export const removeManufacturer = createAsyncThunk(
	'manufacturerManagement/manufacturer/removeManufacturer',
	async (val, { dispatch, getState }) => {
		const authTOKEN = {
			headers: {
				'Content-type': 'application/json',
				Authorization: localStorage.getItem('jwt_access_token')
			}
		};

		const manufacturerId = val.id;
		await axios.delete(`${DELETE_MANUFACTURER}${manufacturerId}`, authTOKEN);
	}
);

export const updateManufacturer = createAsyncThunk(
	'manufacturerManagement/manufacturer/updateManufacturer',
	async (manufacturerData, { dispatch, getState }) => {
		const { manufacturer } = getState().manufacturersManagement;

		function buildFormData(formData, data, parentKey) {
			if (data && typeof data === 'object' && !(data instanceof Date) && !(data instanceof File)) {
				Object.keys(data).forEach(key => {
					buildFormData(formData, data[key], parentKey ? `${parentKey}[${key}]` : key);
				});
			} else {
				const value = data === null ? '' : data;

				formData.append(parentKey, value);
			}
		}

		function jsonToFormData(data) {
			const formData = new FormData();

			buildFormData(formData, data);

			return formData;
		}

		const getFormDateFJ = jsonToFormData(manufacturerData);

		const authTOKEN = {
			headers: {
				'Content-type': 'multipart/form-data',
				Authorization: localStorage.getItem('jwt_access_token')
			}
		};

		const response = await axios.put(`${UPDATE_MANUFACTURER}${manufacturer.id}`, getFormDateFJ, authTOKEN);
	}
);

export const saveManufacturer = createAsyncThunk(
	'manufacturerManagement/manufacturer/saveManufacturer',
	async (manufacturerData, { dispatch, getState }) => {
		function buildFormData(formData, data, parentKey) {
			if (data && typeof data === 'object' && !(data instanceof Date) && !(data instanceof File)) {
				Object.keys(data).forEach(key => {
					buildFormData(formData, data[key], parentKey ? `${parentKey}[${key}]` : key);
				});
			} else {
				const value = data === null ? '' : data;

				formData.append(parentKey, value);
			}
		}

		function jsonToFormData(data) {
			const formData = new FormData();

			buildFormData(formData, data);

			return formData;
		}

		const getFormDateFJ = jsonToFormData(manufacturerData);

		const authTOKEN = {
			headers: {
				'Content-type': 'multipart/form-data',
				Authorization: localStorage.getItem('jwt_access_token')
			}
		};
		const response = await axios.post(`${CREATE_MANUFACTURER}`, getFormDateFJ, authTOKEN);
	}
);

const manufacturerSlice = createSlice({
	name: 'manufacturerManagement/manufacturer',
	initialState: null,
	reducers: {
		resetManufacturer: () => null,
		newManufacturer: {
			reducer: (state, action) => action.payload,
			prepare: event => ({
				payload: {
					name: '',
					description: '',
					image: ''
				}
			})
		}
	},
	extraReducers: {
		[getManufacturer.fulfilled]: (state, action) => action.payload,
		[saveManufacturer.fulfilled]: (state, action) => {
			localStorage.setItem('manufacturerAlert', 'saveManufacturer');
			return action.payload;
		},
		[removeManufacturer.fulfilled]: () => {
			localStorage.setItem('manufacturerAlert', 'deleteManufacturer');
			return null;
		},
		[updateManufacturer.fulfilled]: () => {
			localStorage.setItem('manufacturerAlert', 'updateManufacturer');
		}
	}
});

export const { newManufacturer, resetManufacturer } = manufacturerSlice.actions;

export default manufacturerSlice.reducer;
