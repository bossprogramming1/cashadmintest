import { createAsyncThunk, createEntityAdapter, createSlice } from '@reduxjs/toolkit';
import axios from 'axios';
import { DELETE_MANUFACTURER, GET_MANUFACTURERS } from '../../../../constant/constants';

export const getManufacturers = createAsyncThunk(
    'manufacturerManagement/manufacturers/geManufacturers', async (parameter) => {

        const { page, size } = parameter
        axios.defaults.headers.common['Content-type'] = 'application/json'
        axios.defaults.headers.common.Authorization = localStorage.getItem('jwt_access_token')

        const response = axios.get(GET_MANUFACTURERS, { params: { page, size } });
        const data = await response;

        sessionStorage.setItem("manufacturers_total_elements", data.data.total_elements)
        sessionStorage.setItem("manufacturers_total_pages", data.data.total_pages)
        delete axios.defaults.headers.common["Content-type"]
        delete axios.defaults.headers.common.Authorization

        return data.data.manufacturers
    });


export const removeManufacturers = createAsyncThunk(
    'manufacturerManagement/manufacturers/removeManufacturers',
    async (manufacturerIds, { dispatch, getState }) => {
        const authTOKEN = {
            headers: {
                'Content-type': 'application/json',
                Authorization: localStorage.getItem('jwt_access_token'),
            }
        }
        await axios.delete(`${DELETE_MANUFACTURER}`, { manufacturerIds }, authTOKEN);

        return manufacturerIds;
    }
);



const manufacturersAdapter = createEntityAdapter({});

export const { selectAll: selectManufacturers, selectById: selectManufacturerById } = manufacturersAdapter.getSelectors(
    state => state.manufacturersManagement.manufacturers
);

const manufacturersSlice = createSlice({
    name: 'manufacturerManagement/manufacturers',
    initialState: manufacturersAdapter.getInitialState({
        searchText: ''
    }),
    reducers: {
        setManufacturersSearchText: {
            reducer: (state, action) => {
                state.searchText = action.payload;
            },
            prepare: event => ({ payload: event.target.value || '' })
        }
    },
    extraReducers: {
        [getManufacturers.fulfilled]: manufacturersAdapter.setAll
    }
});

export const { setData, setManufacturersSearchText } = manufacturersSlice.actions;
export default manufacturersSlice.reducer;