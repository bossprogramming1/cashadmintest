import FuseLoading from '@fuse/core/FuseLoading';
import FuseScrollbars from '@fuse/core/FuseScrollbars';
import _ from '@lodash';
import Checkbox from '@material-ui/core/Checkbox';
import { makeStyles } from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TablePagination from '@material-ui/core/TablePagination';
import TableRow from '@material-ui/core/TableRow';
import DeleteIcon from '@material-ui/icons/Delete';
import EditIcon from '@material-ui/icons/Edit';
import Pagination from '@material-ui/lab/Pagination';
import React, { useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { withRouter } from 'react-router-dom';
import { Tooltip } from '@material-ui/core';
import { BASE_URL, SEARCH_MANUFACTURER } from '../../../../constant/constants';
import { getManufacturers, selectManufacturers } from '../store/manufacturersSlice';
import ManufacturersTableHead from './ManufacturersTableHead';

const useStyles = makeStyles(theme => ({
	root: {
		display: 'flex',
		justifyContent: 'space-between',
		flexWrap: 'wrap',
		'& > *': {
			marginTop: theme.spacing(2),
			marginBottom: theme.spacing(3)
		}
	}
}));

const ManufacturersTable = props => {
	const dispatch = useDispatch();
	const manufacturers = useSelector(selectManufacturers);
	const searchText = useSelector(({ manufacturersManagement }) => manufacturersManagement.manufacturers.searchText);
	const [searchManufacturer, setSearchManufacturer] = useState([]);
	const [loading, setLoading] = useState(true);
	const [selected, setSelected] = useState([]);
	const [data, setData] = useState(manufacturers);
	const [page, setPage] = useState(0);
	const [rowsPerPage, setRowsPerPage] = useState(30);
	const [order, setOrder] = useState({
		direction: 'asc',
		id: null
	});
	let serialNumber = 1;
	const user_role = localStorage.getItem('user_role');
	const [parameter, setParameter] = useState({ page: 1, size: 30 });
	const totalPages = sessionStorage.getItem('manufacturers_total_pages');
	const totalElements = sessionStorage.getItem('manufacturers_total_elements');
	const classes = useStyles();

	useEffect(() => {
		dispatch(getManufacturers(parameter)).then(() => setLoading(false));
	}, [dispatch]);

	//search manufacturer
	useEffect(() => {
		searchText !== '' && getSearchManufacturer();
	}, [searchText]);

	const getSearchManufacturer = () => {
		fetch(`${SEARCH_MANUFACTURER}?name=${searchText}`)
			.then(response => response.json())
			.then(manufacturersData => {
				setSearchManufacturer(manufacturersData.manufacturers);
			});
	};

	function handleRequestSort(manufacturerEvent, property) {
		const id = property;
		let direction = 'desc';

		if (order.id === property && order.direction === 'desc') {
			direction = 'asc';
		}

		setOrder({
			direction,
			id
		});
	}

	function handleSelectAllClick(manufacturerEvent) {
		if (manufacturerEvent.target.checked) {
			setSelected(manufacturers.map(n => n.id));
			return;
		}
		setSelected([]);
	}

	function handleDeselect() {
		setSelected([]);
	}

	function handleUpdateManufacturer(item) {
		localStorage.removeItem('manufacturerEvent');
		props.history.push(`/apps/manufacturer-management/manufacturers/${item.id}/${item?.name}`);
	}
	function handleDeleteManufacturer(item, manufacturerEvent) {
		localStorage.removeItem('manufacturerEvent');
		localStorage.setItem('manufacturerEvent', manufacturerEvent);
		props.history.push(`/apps/manufacturer-management/manufacturers/${item.id}/${item?.name}`);
	}

	function handleCheck(manufacturerEvent, id) {
		const selectedIndex = selected.indexOf(id);
		let newSelected = [];

		if (selectedIndex === -1) {
			newSelected = newSelected.concat(selected, id);
		} else if (selectedIndex === 0) {
			newSelected = newSelected.concat(selected.slice(1));
		} else if (selectedIndex === selected.length - 1) {
			newSelected = newSelected.concat(selected.slice(0, -1));
		} else if (selectedIndex > 0) {
			newSelected = newSelected.concat(selected.slice(0, selectedIndex), selected.slice(selectedIndex + 1));
		}

		setSelected(newSelected);
	}

	const handlePagination = (e, handlePage) => {
		setParameter({ ...parameter, page: handlePage });
		setPage(handlePage - 1);
		dispatch(getManufacturers({ ...parameter, page: handlePage }));
	};

	function handleChangePage(manufacturerEvent, value) {
		setPage(value);
		setParameter({ ...parameter, page: value + 1 });
		dispatch(getManufacturers({ ...parameter, page: value - 1 }));
	}

	function handleChangeRowsPerPage(manufacturerEvent) {
		setRowsPerPage(manufacturerEvent.target.value);
		setParameter({ ...parameter, size: manufacturerEvent.target.value });
		dispatch(getManufacturers({ ...parameter, size: manufacturerEvent.target.value }));
	}

	if (loading) {
		return <FuseLoading />;
	}

	return (
		<div className="w-full flex flex-col">
			<FuseScrollbars className="flex-grow overflow-x-auto">
				<Table stickyHeader className="min-w-xl" aria-labelledby="tableTitle">
					<ManufacturersTableHead
						selectedManufacturerIds={selected}
						order={order}
						onSelectAllClick={handleSelectAllClick}
						onRequestSort={handleRequestSort}
						rowCount={manufacturers.length}
						onMenuItemClick={handleDeselect}
					/>

					<TableBody>
						{_.orderBy(
							searchText !== '' && searchManufacturer ? searchManufacturer : manufacturers,
							[
								o => {
									switch (order.id) {
										case 'categories': {
											return o.categories[0];
										}
										default: {
											return o[order.id];
										}
									}
								}
							],
							[order.direction]
						).map(n => {
							const isSelected = selected.indexOf(n.id) !== -1;
							return (
								<TableRow
									hover
									role="checkbox"
									aria-checked={isSelected}
									tabIndex={-1}
									key={n.id}
									selected={isSelected}
								>
									<TableCell className="whitespace-nowrap w-40 md:w-64 text-center" padding="none">
										<Checkbox
											checked={isSelected}
											onClick={manufacturerEvent => manufacturerEvent.stopPropagation()}
											onChange={manufacturerEvent => handleCheck(manufacturerEvent, n.id)}
										/>
									</TableCell>

									<TableCell
										whitespace-nowrap
										className="w-40 md:w-64 text-left"
										component="th"
										scope="row"
									>
										{parameter.page * parameter.size - parameter.size + serialNumber++}
									</TableCell>

									<TableCell
										whitespace-nowrap
										className="w-52 px-4 md:px-4"
										component="th"
										scope="row"
									>
										{n.length > 0 && n.featuredImageId ? (
											<img
												className="block rounded"
												style={{ borderRadius: '50%', height: '50px', width: '50px' }}
												src={_.find(n.image, { id: n.featuredImageId }).url}
												alt={n?.name}
											/>
										) : (
											<img
												className="w-full block rounded"
												style={{ borderRadius: '50%', height: '50px', width: '50px' }}
												src={`${BASE_URL}${n.image}`}
												alt={n?.name}
											/>
										)}
									</TableCell>

									<TableCell className="whitespace-nowrap p-4 md:p-16" component="th" scope="row">
										{n?.name}
									</TableCell>

									<TableCell className="whitespace-nowrap p-4 md:p-16" component="th" scope="row">
										{n.description}
									</TableCell>

									<TableCell
										whitespace-nowrap
										className="p-4 md:p-16"
										align="center"
										component="th"
										scope="row"
									>
										<div>
											<Tooltip title="Edit" placement="top">
												<EditIcon
													onClick={manufacturerEvent => handleUpdateManufacturer(n)}
													className="h-52 cursor-pointer"
													style={{ color: 'green' }}
												/>
											</Tooltip>
											<Tooltip title="Delete" placement="top">
												<DeleteIcon
													onClick={event => handleDeleteManufacturer(n, 'Delete')}
													className="h-52 cursor-pointer"
													style={{
														color: 'red',
														visibility:
															user_role === 'ADMIN' || user_role === 'admin'
																? 'visible'
																: 'hidden'
													}}
												/>
											</Tooltip>
										</div>
									</TableCell>
								</TableRow>
							);
						})}
					</TableBody>
				</Table>
			</FuseScrollbars>

			<div className={classes.root}>
				<Pagination
					count={totalPages}
					defaultPage={1}
					color="primary"
					showFirstButton
					showLastButton
					variant="outlined"
					shape="rounded"
					onChange={handlePagination}
				/>

				<TablePagination
					className="flex-shrink-0 border-t-1"
					rowsPerPageOptions={[30, 50, 100]}
					component="div"
					count={totalElements}
					rowsPerPage={rowsPerPage}
					page={page}
					backIconButtonProps={{
						'aria-label': 'Previous Page'
					}}
					nextIconButtonProps={{
						'aria-label': 'Next Page'
					}}
					onChangePage={handleChangePage}
					onChangeRowsPerPage={handleChangeRowsPerPage}
				/>
			</div>
		</div>
	);
};

export default withRouter(ManufacturersTable);
