import FusePageCarded from '@fuse/core/FusePageCarded';
import { useDeepCompareEffect } from '@fuse/hooks';
import { yupResolver } from '@hookform/resolvers/yup';
import { Button, Typography } from '@material-ui/core';
import {
	CUSTOMER_TYPE_CREATE,
	CUSTOMER_TYPE_DELETE,
	CUSTOMER_TYPE_DETAILS,
	CUSTOMER_TYPE_UPDATE
} from 'app/constant/permission/permission';
import { getUserPermissions } from 'app/store/dataSlice';
import withReducer from 'app/store/withReducer';
import { motion } from 'framer-motion';
import React, { useEffect, useState } from 'react';
import { FormProvider, useForm } from 'react-hook-form';
import { useDispatch, useSelector } from 'react-redux';
import { Link, useParams } from 'react-router-dom';
import * as yup from 'yup';
import PagenotFound from '../../Pagenotfound/PagenotFound';
import { getCustomertype, newCustomertype, resetCustomertype } from '../store/customertypeSlice';
import reducer from '../store/index';
import CustomertypeForm from './CustomertypeForm';
import NewCustomertypeHeader from './NewCustomertypeHeader';

/**
 * Form Validation Schema
 */
const schema = yup.object().shape({
	name: yup.string().required('Group Name is required')
});

const Customertype = () => {
	const dispatch = useDispatch();
	const customertype = useSelector(({ customertypesManagement }) => customertypesManagement.customertype);

	const [noCustomertype, setNoCustomertype] = useState(false);
	const methods = useForm({
		mode: 'onChange',
		defaultValues: {},
		resolver: yupResolver(schema)
	});
	const routeParams = useParams();
	const UserPermissions = useSelector(state => state.data.UserPermissions);
	useEffect(() => {
		dispatch(getUserPermissions());
	}, []);
	const { reset, watch, control, onChange, formState } = methods;
	const form = watch();

	useDeepCompareEffect(() => {
		function updateCustomertypeState() {
			const { customertypeId } = routeParams;

			if (customertypeId === 'new') {
				localStorage.removeItem('event');
				/**
				 * Create New User data
				 */
				dispatch(newCustomertype());
			} else {
				/**
				 * Get User data
				 */

				dispatch(getCustomertype(customertypeId)).then(action => {
					/**
					 * If the requested product is not exist show message
					 */
					if (!action.payload) {
						setNoCustomertype(true);
					}
				});
			}
		}

		updateCustomertypeState();
	}, [dispatch, routeParams]);

	useEffect(() => {
		if (!customertype) {
			return;
		}
		/**
		 * Reset the form on customertype state changes
		 */
		reset(customertype);
	}, [customertype, reset]);

	useEffect(() => {
		return () => {
			/**
			 * Reset Customertype on component unload
			 */
			dispatch(resetCustomertype());
			setNoCustomertype(false);
		};
	}, [dispatch]);

	/**
	 * Show Message if the requested products is not exists
	 */
	if (noCustomertype) {
		return (
			<motion.div
				initial={{ opacity: 0 }}
				animate={{ opacity: 1, transition: { delay: 0.1 } }}
				className="flex flex-col flex-1 items-center justify-center h-full"
			>
				<Typography color="textSecondary" variant="h5">
					There is no such customertype!
				</Typography>
				<Button
					className="mt-24"
					component={Link}
					variant="outlined"
					to="/apps/e-commerce/products"
					color="inherit"
				>
					Go to Customertype Page
				</Button>
			</motion.div>
		);
	}

	return (
		<FormProvider {...methods}>
			{UserPermissions.includes(CUSTOMER_TYPE_CREATE) ||
			UserPermissions.includes(CUSTOMER_TYPE_UPDATE) ||
			UserPermissions.includes(CUSTOMER_TYPE_DELETE) ||
			UserPermissions.includes(CUSTOMER_TYPE_DETAILS) ? (
				<FusePageCarded
					classes={{
						toolbar: 'p-0',
						header: 'min-h-52 h-52 sm:h-136 sm:min-h-136'
					}}
					header={<NewCustomertypeHeader />}
					content={
						<div className="p-16 sm:p-24">
							<CustomertypeForm />
						</div>
					}
					innerScroll
				/>
			) : (
				<PagenotFound />
			)}
		</FormProvider>
	);
};
export default withReducer('customertypesManagement', reducer)(Customertype);
