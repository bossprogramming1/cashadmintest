import FuseLoading from '@fuse/core/FuseLoading';
import FuseScrollbars from '@fuse/core/FuseScrollbars';
import _ from '@lodash';
import Checkbox from '@material-ui/core/Checkbox';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TablePagination from '@material-ui/core/TablePagination';
import TableRow from '@material-ui/core/TableRow';
import DeleteIcon from '@material-ui/icons/Delete';
import EditIcon from '@material-ui/icons/Edit';
import React, { useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { withRouter } from 'react-router-dom';
import { getCustomertypes, selectCustomertypes } from '../store/customertypesSlice';
import CustomertypesTableHead from './CustomertypesTableHead';

const CustomertypesTable = props => {
	const dispatch = useDispatch();
	const customertypes = useSelector(selectCustomertypes);
	const searchText = useSelector(({ customertypesManagement }) => customertypesManagement.customertypes.searchText);
	const [loading, setLoading] = useState(true);
	const [selected, setSelected] = useState([]);
	const [data, setData] = useState(customertypes);
	const [page, setPage] = useState(0);
	const [rowsPerPage, setRowsPerPage] = useState(10);
	const [order, setOrder] = useState({
		direction: 'asc',
		id: null
	});
	let serialNumber = 1;
	const user_role = localStorage.getItem('user_role');

	const [deleteItem, setDeleteItem] = useState('');
	useEffect(() => {
		dispatch(getCustomertypes()).then(() => setLoading(false));
	}, [dispatch]);

	useEffect(() => {
		if (searchText.length !== 0) {
			setData(_.filter(customertypes, item => item?.name.toLowerCase().includes(searchText.toLowerCase())));
			setPage(0);
		} else {
			setData(customertypes);
		}
	}, [customertypes, searchText]);

	function handleRequestSort(customertypeEvent, property) {
		const id = property;
		let direction = 'desc';

		if (order.id === property && order.direction === 'desc') {
			direction = 'asc';
		}

		setOrder({
			direction,
			id
		});
	}

	function handleSelectAllClick(customertypeEvent) {
		if (customertypeEvent.target.checked) {
			setSelected(data.map(n => n.id));
			return;
		}
		setSelected([]);
	}

	function handleDeselect() {
		setSelected([]);
	}

	function handleUpdateCustomertype(item) {
		localStorage.removeItem('customertypeEvent');
		props.history.push(`/apps/customertype-management/customertypes/${item.id}/${item?.name}`);
	}
	function handleDeleteCustomertype(item, customertypeEvent) {
		localStorage.removeItem('customertypeEvent');
		localStorage.setItem('customertypeEvent', customertypeEvent);
		props.history.push(`/apps/customertype-management/customertypes/${item.id}/${item?.name}`);
	}

	function handleCheck(customertypeEvent, id) {
		const selectedIndex = selected.indexOf(id);
		let newSelected = [];

		if (selectedIndex === -1) {
			newSelected = newSelected.concat(selected, id);
		} else if (selectedIndex === 0) {
			newSelected = newSelected.concat(selected.slice(1));
		} else if (selectedIndex === selected.length - 1) {
			newSelected = newSelected.concat(selected.slice(0, -1));
		} else if (selectedIndex > 0) {
			newSelected = newSelected.concat(selected.slice(0, selectedIndex), selected.slice(selectedIndex + 1));
		}

		setSelected(newSelected);
	}

	function handleChangePage(customertypeEvent, value) {
		setPage(value);
	}

	function handleChangeRowsPerPage(customertypeEvent) {
		setRowsPerPage(customertypeEvent.target.value);
	}

	if (loading) {
		return <FuseLoading />;
	}

	return (
		<div className="w-full flex flex-col">
			<FuseScrollbars className="flex-grow overflow-x-auto">
				<Table stickyHeader className="min-w-xl" aria-labelledby="tableTitle">
					<CustomertypesTableHead
						selectedCustomertypeIds={selected}
						order={order}
						onSelectAllClick={handleSelectAllClick}
						onRequestSort={handleRequestSort}
						rowCount={data.length}
						onMenuItemClick={handleDeselect}
					/>

					<TableBody>
						{_.orderBy(
							data,
							[
								o => {
									switch (order.id) {
										case 'categories': {
											return o.categories[0];
										}
										default: {
											return o[order.id];
										}
									}
								}
							],
							[order.direction]
						)
							.slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
							.map(n => {
								const isSelected = selected.indexOf(n.id) !== -1;
								return (
									<TableRow
										hover
										role="checkbox"
										aria-checked={isSelected}
										tabIndex={-1}
										key={n.id}
										selected={isSelected}
									>
										<TableCell
											whitespace-nowrap
											className="w-40 md:w-64 text-center"
											padding="none"
										>
											<Checkbox
												checked={isSelected}
												onClick={customertypeEvent => customertypeEvent.stopPropagation()}
												onChange={customertypeEvent => handleCheck(customertypeEvent, n.id)}
											/>
										</TableCell>

										<TableCell
											whitespace-nowrap
											className="w-40 md:w-64"
											component="th"
											scope="row"
										>
											{serialNumber++}
										</TableCell>

										<TableCell className="whitespace-nowrap p-4 md:p-16" component="th" scope="row">
											{n?.name}
										</TableCell>

										<TableCell
											whitespace-nowrap
											className="p-4 md:p-16"
											align="center"
											component="th"
											scope="row"
										>
											<div>
												<EditIcon
													onClick={customertypeEvent => handleUpdateCustomertype(n)}
													className="h-52 cursor-pointer"
													style={{ color: 'green' }}
												/>{' '}
												<DeleteIcon
													onClick={event => handleDeleteCustomertype(n, 'Delete')}
													className="h-52 cursor-pointer"
													style={{
														color: 'red',
														visibility:
															user_role === 'ADMIN' || user_role === 'admin'
																? 'visible'
																: 'hidden'
													}}
												/>
											</div>
										</TableCell>
									</TableRow>
								);
							})}
					</TableBody>
				</Table>
			</FuseScrollbars>

			<TablePagination
				className="flex-shrink-0 border-t-1"
				rowsPerPageOptions={[5, 10, 25]}
				component="div"
				count={data.length}
				rowsPerPage={rowsPerPage}
				page={page}
				backIconButtonProps={{
					'aria-label': 'Previous Page'
				}}
				nextIconButtonProps={{
					'aria-label': 'Next Page'
				}}
				onChangePage={handleChangePage}
				onChangeRowsPerPage={handleChangeRowsPerPage}
			/>
		</div>
	);
};

export default withRouter(CustomertypesTable);
