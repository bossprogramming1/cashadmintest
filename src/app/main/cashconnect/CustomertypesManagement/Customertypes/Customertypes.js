import FusePageCarded from '@fuse/core/FusePageCarded';
import { CUSTOMER_TYPE_LIST } from 'app/constant/permission/permission';
import { getUserPermissions } from 'app/store/dataSlice';
import withReducer from 'app/store/withReducer';
import React, { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import PagenotFound from '../../Pagenotfound/PagenotFound';
import reducer from '../store/index';
import CustomertypesHeader from './CustomertypesHeader';
import CustomertypesTable from './CustomertypesTable';

const Customertypes = () => {
	const UserPermissions = useSelector(state => state.data.UserPermissions);
	useEffect(() => {
		dispatch(getUserPermissions());
	}, []);
	const dispatch = useDispatch();
	return (
		<FusePageCarded
			classes={{
				content: 'flex',
				contentCard: 'overflow-hidden',
				header: 'min-h-74 h-64'
			}}
			header={UserPermissions.includes(CUSTOMER_TYPE_LIST) && <CustomertypesHeader />}
			content={UserPermissions.includes(CUSTOMER_TYPE_LIST) ? <CustomertypesTable /> : <PagenotFound />}
			innerScroll
		/>
	);
};
export default withReducer('customertypesManagement', reducer)(Customertypes);
