import { createAsyncThunk, createSlice } from '@reduxjs/toolkit';
import axios from 'axios';
import {
	CREATE_CUSTOMERTYPE,
	DELETE_CUSTOMERTYPE,
	GET_CUSTOMERTYPEID,
	UPDATE_CUSTOMERTYPE
} from '../../../../constant/constants';

export const getCustomertype = createAsyncThunk(
	'customertypeManagement/customertype/getCustomertype',
	async (params, { rejectWithValue }) => {
		const authTOKEN = {
			headers: {
				'Content-type': 'application/json',
				Authorization: localStorage.getItem('jwt_access_token')
			}
		};

		try {
			const response = await axios.get(`${GET_CUSTOMERTYPEID}${params}`, authTOKEN);
			const data = await response.data;
			return data === undefined ? null : data;
		} catch (err) {
			return rejectWithValue(params);
		}
	}
);

export const removeCustomertype = createAsyncThunk(
	'customertypeManagement/customertype/removeCustomertype',
	async (val, { dispatch, getState }) => {
		const authTOKEN = {
			headers: {
				'Content-type': 'application/json',
				Authorization: localStorage.getItem('jwt_access_token')
			}
		};

		const customertypeId = val.id;
		await axios.delete(`${DELETE_CUSTOMERTYPE}${customertypeId}`, authTOKEN);
	}
);

export const updateCustomertype = createAsyncThunk(
	'customertypeManagement/customertype/updateCustomertype',
	async (customertypeData, { dispatch, getState }) => {
		const { customertype } = getState().customertypesManagement;

		const authTOKEN = {
			headers: {
				'Content-type': 'application/json',
				Authorization: localStorage.getItem('jwt_access_token')
			}
		};
		const response = await axios.put(`${UPDATE_CUSTOMERTYPE}${customertype.id}`, customertypeData, authTOKEN);
	}
);

export const saveCustomertype = createAsyncThunk(
	'customertypeManagement/customertype/saveCustomertype',
	async (customertypeData, { dispatch, getState }) => {
		const authTOKEN = {
			headers: {
				'Content-type': 'application/json',
				Authorization: localStorage.getItem('jwt_access_token')
			}
		};
		const response = await axios.post(`${CREATE_CUSTOMERTYPE}`, customertypeData, authTOKEN);
	}
);

const customertypeSlice = createSlice({
	name: 'customertypeManagement/customertype',
	initialState: null,
	reducers: {
		resetCustomertype: () => null,
		newCustomertype: {
			reducer: (state, action) => action.payload,
			prepare: event => ({
				payload: {
					name: ''
				}
			})
		}
	},
	extraReducers: {
		[getCustomertype.fulfilled]: (state, action) => action.payload,
		[saveCustomertype.fulfilled]: (state, action) => {
			localStorage.setItem('customertypeAlert', 'saveCustomertype');
			return action.payload;
		},
		[removeCustomertype.fulfilled]: () => {
			localStorage.setItem('customertypeAlert', 'deleteCustomertype');
		},
		[updateCustomertype.fulfilled]: () => {
			localStorage.setItem('customertypeAlert', 'updateCustomertype');
		}
	}
});

export const { newCustomertype, resetCustomertype } = customertypeSlice.actions;

export default customertypeSlice.reducer;
