import { combineReducers } from '@reduxjs/toolkit';
import customertype from './customertypeSlice';
import customertypes from './customertypesSlice';

const reducer = combineReducers({
	customertype,
	customertypes,
});

export default reducer;