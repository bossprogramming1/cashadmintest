import { createAsyncThunk, createEntityAdapter, createSlice } from '@reduxjs/toolkit';
import axios from 'axios';
import { DELETE_CUSTOMERTYPE, GET_CUSTOMERTYPES } from '../../../../constant/constants';

export const getCustomertypes = createAsyncThunk('customertypeManagement/customertypes/geCustomertypes', async () => {
	const authTOKEN = {
		headers: {
			'Content-type': 'application/json',
			Authorization: localStorage.getItem('jwt_access_token')
		}
	};

	const response = axios.get(GET_CUSTOMERTYPES, authTOKEN);
	const data = await response;
	return data?.data?.customer_types;
});

export const removeCustomertypes = createAsyncThunk(
	'customertypeManagement/customertypes/removeCustomertypes',
	async (customertypeIds, { dispatch, getState }) => {
		const authTOKEN = {
			headers: {
				'Content-type': 'application/json',
				Authorization: localStorage.getItem('jwt_access_token')
			}
		};
		await axios.delete(`${DELETE_CUSTOMERTYPE}`, { customertypeIds }, authTOKEN);

		return customertypeIds;
	}
);

const customertypesAdapter = createEntityAdapter({});

export const { selectAll: selectCustomertypes, selectById: selectCustomertypeById } = customertypesAdapter.getSelectors(
	state => state.customertypesManagement.customertypes
);

const customertypesSlice = createSlice({
	name: 'customertypeManagement/customertypes',
	initialState: customertypesAdapter.getInitialState({
		searchText: ''
	}),
	reducers: {
		setCustomertypesSearchText: {
			reducer: (state, action) => {
				state.searchText = action.payload;
			},
			prepare: event => ({ payload: event.target.value || '' })
		}
	},
	extraReducers: {
		[getCustomertypes.fulfilled]: customertypesAdapter.setAll
	}
});

export const { setData, setCustomertypesSearchText } = customertypesSlice.actions;
export default customertypesSlice.reducer;
