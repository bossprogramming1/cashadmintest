import FusePageCarded from '@fuse/core/FusePageCarded';
import { useDeepCompareEffect } from '@fuse/hooks';
import { yupResolver } from '@hookform/resolvers/yup';
import useUserInfo from 'app/@customHooks/useUserInfo';
import {
	PURCHASE_CREATE,
	PURCHASE_DELETE,
	PURCHASE_DETAILS,
	PURCHASE_UPDATE
} from 'app/constant/permission/permission';
import PagenotFound from 'app/main/cashconnect/Pagenotfound/PagenotFound';
import { getUserPermissions } from 'app/store/dataSlice';
import withReducer from 'app/store/withReducer';
import { useEffect, useState } from 'react';
import { FormProvider, useForm } from 'react-hook-form';
import { useDispatch, useSelector } from 'react-redux';
import { useParams } from 'react-router-dom';
import * as yup from 'yup';
import reducer from '../store/index';
import { getPurchase, newPurchase, resetPurchase, setUserBasedBranch } from '../store/purchaseSlice';
import NewPurchaseHeader from './NewPurchaseHeader';
import PurchaseForm from './PurchaseForm';
import PurchaseItemContext from './PurchaseItemContext';

/**
 * Form Validation Schema
 */
const schema = yup.object().shape({
	branch: yup.number().required('Branch is required'),
	sub_ledger: yup.number().required('Payment type is required'),
	purchase_date: yup.date().required('Payment date is required')
});

const NewPurchase = () => {
	const [receiptVoucherItems, setReceiptVoucherItems] = useState([
		{ ledger: 0, debit_amount: 0, credit_amount: 0 },
		{ ledger: 0, debit_amount: 0, credit_amount: 0 }
	]);
	const dispatch = useDispatch();
	const purchase = useSelector(({ purchasesBillManagement }) => purchasesBillManagement.purchase);
	const [noReceiptvoucher, setNoReceiptvoucher] = useState(false);
	const methods = useForm({
		mode: 'onChange',
		defaultValues: {},
		resolver: yupResolver(schema)
	});
	const routeParams = useParams();
	const UserPermissions = useSelector(state => state.data.UserPermissions);
	useEffect(() => {
		dispatch(getUserPermissions());
	}, []);
	const { reset, watch, control, onChange, formState } = methods;
	const form = watch();

	const { userId } = useUserInfo();

	useDeepCompareEffect(() => {
		function updateReceiptvoucherState() {
			const { purchaseId, purchaseName } = routeParams;

			if (purchaseId === 'new') {
				localStorage.removeItem('event');
				localStorage.removeItem('updatePaymentvoucher');
				localStorage.removeItem('deletePurchase');
				/**
				 * Create New User data
				 */
				dispatch(newPurchase());
				dispatch(setUserBasedBranch(userId));
			} else {
				/**
				 * Get User data
				 */

				dispatch(getPurchase(purchaseName)).then(action => {
					/**
					 * If the requested product is not exist show message
					 */
					if (!action.payload) {
						setNoReceiptvoucher(true);
					}
				});
			}
		}

		updateReceiptvoucherState();
	}, [dispatch, routeParams]);

	//setFormState
	const setFormState = () => {
		const updateReceiptVoucherData = {
			branch: purchase?.branch?.id,
			sub_ledger: purchase?.sub_ledger?.id,
			purchase_date: purchase?.purchase_date,
			details: purchase?.details
		};
		purchase?.items?.map((item, idx) => {
			if (idx === 0 || idx === 1) {
				receiptVoucherItems[idx].id = item.id;
				receiptVoucherItems[idx].invoice_no = item?.invoice_no;
				receiptVoucherItems[idx].ledger = item?.ledger?.id;
				receiptVoucherItems[idx].debit_amount = item?.debit_amount;
				receiptVoucherItems[idx].credit_amount = item?.credit_amount;

				updateReceiptVoucherData[`ledger${idx}`] = item?.ledger?.id;
				updateReceiptVoucherData[`debit_amount${idx}`] = item?.debit_amount;
				updateReceiptVoucherData[`credit_amount${idx}`] = item?.credit_amount;
			} else {
				const prevItems = receiptVoucherItems;
				prevItems.push({ id: 0, ledger: 0, debit_amount: 0, credit_amount: 0 });
				setReceiptVoucherItems(prevItems); //items.concat([{ item_name: "", item_price: 0, item_quantity: 0 }])
				receiptVoucherItems[idx].id = item?.id;
				receiptVoucherItems[idx].invoice_no = item?.invoice_no;
				receiptVoucherItems[idx].ledger = item?.ledger?.id;
				receiptVoucherItems[idx].debit_amount = item?.debit_amount;
				receiptVoucherItems[idx].credit_amount = item?.credit_amount;

				updateReceiptVoucherData[`ledger${idx}`] = item?.ledger?.id;
				updateReceiptVoucherData[`debit_amount${idx}`] = item?.debit_amount;
				updateReceiptVoucherData[`credit_amount${idx}`] = item?.credit_amount;
			}
			return null;
		});
		return updateReceiptVoucherData;
	};

	useEffect(() => {
		if (!purchase) {
			return;
		}
		/**
		 * Reset the form on receiptvoucher state changes
		 */
		const receiptVoucherData = setFormState();
		reset(receiptVoucherData);
	}, [purchase, reset]);

	useEffect(() => {
		return () => {
			/**
			 * Reset Receiptvoucher on component unload
			 */
			dispatch(resetPurchase());
			setNoReceiptvoucher(false);
		};
	}, [dispatch]);

	return (
		<PurchaseItemContext.Provider value={[receiptVoucherItems, setReceiptVoucherItems]}>
			<FormProvider {...methods}>
				{UserPermissions.includes(PURCHASE_CREATE) ||
				UserPermissions.includes(PURCHASE_UPDATE) ||
				UserPermissions.includes(PURCHASE_DELETE) ||
				UserPermissions.includes(PURCHASE_DETAILS) ? (
					<FusePageCarded
						classes={{
							content: 'flex',
							contentCard: 'overflow-hidden',
							header: 'min-h-74 h-64'
						}}
						header={<NewPurchaseHeader />}
						content={
							<div className="p-16 sm:p-24" style={{ width: '100%' }}>
								<PurchaseForm />
							</div>
						}
						innerScroll
					/>
				) : (
					<PagenotFound />
				)}
			</FormProvider>
		</PurchaseItemContext.Provider>
	);
};
export default withReducer('purchasesBillManagement', reducer)(NewPurchase);
