import { createContext } from 'react';

const PurchaseItemContext = createContext({});

export default PurchaseItemContext;