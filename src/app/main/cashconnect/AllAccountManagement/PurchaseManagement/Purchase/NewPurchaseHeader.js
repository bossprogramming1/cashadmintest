import Button from '@material-ui/core/Button';
import Icon from '@material-ui/core/Icon';
import { useTheme } from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';
import { motion } from 'framer-motion';
import moment from 'moment';
import React, { useContext } from 'react';
import { useFormContext } from 'react-hook-form';
import { useDispatch, useSelector } from 'react-redux';
import { Link, useHistory, useParams } from 'react-router-dom';
import { removePurchase, savePurchase, updatePurchase } from '../store/purchaseSlice';
import PurchaseItemContext from './PurchaseItemContext';

const NewPurchaseHeader = () => {
	const [receiptVoucherItems, setReceiptVoucherItems] = useContext(PurchaseItemContext);
	const getPurchase = useSelector(({ purchasesBillManagement }) => purchasesBillManagement?.purchase);
	const dispatch = useDispatch();
	const methods = useFormContext();
	const { formState, watch, getValues } = methods;
	const { isValid, dirtyFields } = formState;
	const name = watch('name');
	const theme = useTheme();
	const history = useHistory();
	const routeParams = useParams();
	const { purchaseId, purchaseName } = routeParams;
	const handleDelete = localStorage.getItem('purchaseEvent');
	const DebitCreditMatched = localStorage.getItem('DebitCreditMatched');
	const ledgerError = localStorage.getItem('ledgerError');

	function handleSavePurchase() {
		const data = getValues();
		const date = data.payment_date;
		const convertedPurchaseDate = moment(date).format('YYYY-MM-DD');
		const receiptVourcherData = {
			branch: data.branch,
			sub_ledger: data.sub_ledger,
			purchase_date: convertedPurchaseDate,
			details: data.details,
			items: receiptVoucherItems
		};
		dispatch(savePurchase(receiptVourcherData)).then(() => {
			history.push('/apps/accounts/payable_bills');
		});
	}

	function handleUpdatePurchase() {
		const updateReceiptVoucherFormData = getValues();
		const updateReceiveVourcherData = {
			branch: updateReceiptVoucherFormData.branch,
			sub_ledger: updateReceiptVoucherFormData.sub_ledger,
			purchase_date: updateReceiptVoucherFormData.purchase_date,
			details: updateReceiptVoucherFormData.details,
			invoice_no: getPurchase?.invoice_no,
			items: receiptVoucherItems
		};
		dispatch(updatePurchase(updateReceiveVourcherData)).then(() => {
			history.push('/apps/accounts/payable_bills');
		});
	}

	function handleRemovePurchase() {
		dispatch(removePurchase(purchaseName)).then(() => {
			localStorage.removeItem('purchaseEvent');
			history.push('/apps/accounts/payable_bills');
		});
	}

	function handleCancel() {
		history.push('/apps/accounts/payable_bills');
	}

	return (
		<div className="flex flex-1 w-full items-center justify-between">
			<div className="flex flex-col items-start max-w-full min-w-0">
				<motion.div initial={{ x: 20, opacity: 0 }} animate={{ x: 0, opacity: 1, transition: { delay: 0.3 } }}>
					<Typography
						className="flex items-center sm:mb-12"
						component={Link}
						role="button"
						to="/apps/accounts/payable_bills"
						color="inherit"
					>
						<Icon className="text-20">{theme.direction === 'ltr' ? 'arrow_back' : 'arrow_forward'}</Icon>
						<span className="hidden sm:flex mx-4 font-medium">Payable Bill</span>
					</Typography>
				</motion.div>

				<div className="flex items-center max-w-full">
					<motion.div
						className="hidden sm:flex"
						initial={{ scale: 0 }}
						animate={{ scale: 1, transition: { delay: 0.3 } }}
					/>
					<div className="flex flex-col min-w-0 mx-8 sm:mc-16">
						<motion.div initial={{ x: -20 }} animate={{ x: 0, transition: { delay: 0.3 } }}>
							<Typography className="text-16 sm:text-20 truncate font-semibold">
								{name || 'Create New Payable Bill'}
							</Typography>
							<Typography variant="caption" className="font-medium">
								Payable Bill Detail
							</Typography>
						</motion.div>
					</div>
				</div>
			</div>
			<motion.div
				className="flex"
				initial={{ opacity: 0, x: 20 }}
				animate={{ opacity: 1, x: 0, transition: { delay: 0.3 } }}
			>
				{handleDelete === 'Delete' && (
					<Typography className="mt-6" variant="subtitle2">
						Do you want to remove this Payable Bill?
					</Typography>
				)}
				{handleDelete === 'Delete' && routeParams.purchaseId !== 'new' && (
					<Button
						className="whitespace-nowrap mx-4"
						variant="contained"
						color="secondary"
						onClick={handleRemovePurchase}
						startIcon={<Icon className="hidden sm:flex">delete</Icon>}
						style={{ backgroundColor: '#ea5b78', color: 'white' }}
					>
						Remove
					</Button>
				)}
				{routeParams.purchaseId === 'new' && (
					<Button
						className="whitespace-nowrap mx-4"
						variant="contained"
						color="secondary"
						disabled={!DebitCreditMatched || ledgerError || !isValid}
						onClick={handleSavePurchase}
					>
						Save
					</Button>
				)}
				{handleDelete !== 'Delete' && routeParams?.purchaseName && (
					<Button
						className="whitespace-nowrap mx-4"
						color="secondary"
						variant="contained"
						style={{ backgroundColor: '#4dc08e', color: 'white' }}
						onClick={handleUpdatePurchase}
					>
						Update
					</Button>
				)}
				<Button
					className="whitespace-nowrap mx-4"
					variant="contained"
					style={{ backgroundColor: '#FFAA4C', color: 'white' }}
					onClick={handleCancel}
				>
					Cancel
				</Button>
			</motion.div>
		</div>
	);
};

export default NewPurchaseHeader;
