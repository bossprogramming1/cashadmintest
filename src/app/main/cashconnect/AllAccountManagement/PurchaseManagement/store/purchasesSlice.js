import { createAsyncThunk, createEntityAdapter, createSlice } from '@reduxjs/toolkit';
import axios from 'axios';
import { DELETE_ACCOUNT_PURCHASE, GET_ACCOUNT_PURCHASES } from '../../../../../constant/constants';

export const getPurchases = createAsyncThunk('purchasesBillManagement/purchases/geSaless', async () => {
	const authTOKEN = {
		headers: {
			'Content-type': 'application/json',
			Authorization: localStorage.getItem('jwt_access_token')
		}
	};

	const response = axios.get(GET_ACCOUNT_PURCHASES, authTOKEN);
	const data = await response;
	return data?.data?.purchases;
});

export const removePurchases = createAsyncThunk(
	'purchasesBillManagement/purchases/removeSaless',
	async (purchaseIds, { dispatch, getState }) => {
		const authTOKEN = {
			headers: {
				'Content-type': 'application/json',
				Authorization: localStorage.getItem('jwt_access_token')
			}
		};
		await axios.delete(`${DELETE_ACCOUNT_PURCHASE}`, { purchaseIds }, authTOKEN);

		return purchaseIds;
	}
);

const purchasesAdapter = createEntityAdapter({});

export const { selectAll: selectPurchases, selectById: selectPurchaseById } = purchasesAdapter.getSelectors(
	state => state.purchasesBillManagement.purchases
);

const purchasesSlice = createSlice({
	name: 'purchasesBillManagement/purchases',
	initialState: purchasesAdapter.getInitialState({
		searchText: ''
	}),
	reducers: {
		setPurchasesSearchText: {
			reducer: (state, action) => {
				state.searchText = action.payload;
			},
			prepare: event => ({ payload: event.target.value || '' })
		}
	},
	extraReducers: {
		[getPurchases.fulfilled]: purchasesAdapter.setAll
	}
});

export const { setData, setPurchasesSearchText } = purchasesSlice.actions;
export default purchasesSlice.reducer;
