import { createAsyncThunk, createSlice } from '@reduxjs/toolkit';
import axios from 'axios';
import {
	BRANCH_BY_USER_ID,
	CREATE_ACCOUNT_PURCHASE,
	DELETE_ACCOUNT_PURCHASE,
	GET_ACCOUNT_PURCHASE_BY_INVOICE_NUMBER,
	UPDATE_ACCOUNT_PURCHASE
} from '../../../../../constant/constants';

export const getPurchase = createAsyncThunk(
	'purchaseBillManagement/purchase/getpurchase',
	async (params, { rejectWithValue }) => {
		const authTOKEN = {
			headers: {
				'Content-type': 'application/json',
				Authorization: localStorage.getItem('jwt_access_token')
			}
		};

		try {
			const response = await axios.get(`${GET_ACCOUNT_PURCHASE_BY_INVOICE_NUMBER}${params}`, authTOKEN);
			const data = await response.data;
			return data === undefined ? null : data;
		} catch (err) {
			return rejectWithValue(params);
		}
	}
);

export const removePurchase = createAsyncThunk(
	'purchaseBillManagement/purchase/removePurchase',
	async (purchaseInvoice, { dispatch, getState }) => {
		const authTOKEN = {
			headers: {
				'Content-type': 'application/json',
				Authorization: localStorage.getItem('jwt_access_token')
			}
		};

		await axios.delete(`${DELETE_ACCOUNT_PURCHASE}${purchaseInvoice}`, authTOKEN);
	}
);

export const updatePurchase = createAsyncThunk(
	'purchaseBillManagement/purchase/updatePurchase',
	async (purchaseData, { dispatch, getState }) => {
		const authTOKEN = {
			headers: {
				'Content-type': 'application/json',
				Authorization: localStorage.getItem('jwt_access_token')
			}
		};
		const response = await axios.put(`${UPDATE_ACCOUNT_PURCHASE}`, purchaseData, authTOKEN);
	}
);

export const savePurchase = createAsyncThunk(
	'purchaseBillManagement/purchase/savePurchase',
	async (purchaseData, { dispatch, getState }) => {
		const authTOKEN = {
			headers: {
				'Content-type': 'application/json',
				Authorization: localStorage.getItem('jwt_access_token')
			}
		};
		const response = await axios.post(`${CREATE_ACCOUNT_PURCHASE}`, purchaseData, authTOKEN);
	}
);

export const setUserBasedBranch = createAsyncThunk(
	'purchaseBillManagement/purchase/setUserBasedBranch',
	async userId => {
		const authTOKEN = {
			headers: {
				'Content-type': 'application/json',
				Authorization: localStorage.getItem('jwt_access_token')
			}
		};
		const response = await axios.get(`${BRANCH_BY_USER_ID}${userId}`, authTOKEN);
		return response.data || {};
	}
);

const purchaseSlice = createSlice({
	name: 'purchaseBillManagement/purchase',
	initialState: null,
	reducers: {
		resetPurchase: () => null,
		newPurchase: {
			reducer: (state, action) => action.payload,
			prepare: event => ({
				payload: {
					branch: 0,
					sub_ledger: 0,
					purchase_date: '',
					details: '',
					items: [
						{ ledger: 2, debit_amount: 0, credit_amount: 0 },
						{ ledger: 0, debit_amount: 0, credit_amount: 0 }
					]
					//ledger: 2,
					// debit_amount: "",
					// credit_amount: ""
				}
			})
		}
	},
	extraReducers: {
		[getPurchase.fulfilled]: (state, action) => action.payload,
		[savePurchase.fulfilled]: (state, action) => {
			localStorage.setItem('purchaseAlert', 'savePurchase');
			return action.payload;
		},
		[removePurchase.fulfilled]: () => {
			localStorage.setItem('purchaseAlert', 'deletePurchase');
		},
		[updatePurchase.fulfilled]: () => {
			localStorage.setItem('purchaseAlert', 'updatePurchase');
		},
		[setUserBasedBranch.fulfilled]: (state, action) => {
			return {
				...state,
				branch: action.payload
			};
		}
	}
});

export const { newPurchase, resetPurchase } = purchaseSlice.actions;
export default purchaseSlice.reducer;
