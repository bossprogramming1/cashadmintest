import React, { useEffect } from 'react';
import FusePageCarded from '@fuse/core/FusePageCarded';
import withReducer from 'app/store/withReducer';
import { useDispatch, useSelector } from 'react-redux';
import { getUserPermissions } from 'app/store/dataSlice';
import { PURCHASE_LIST } from 'app/constant/permission/permission';
import PagenotFound from 'app/main/cashconnect/Pagenotfound/PagenotFound';
import reducer from '../store/index';
import PurchasesHeader from './PurchasesHeader';
import PurchasesTable from './PurchasesTable';

const Purchases = () => {
	const UserPermissions = useSelector(state => state.data.UserPermissions);
	useEffect(() => {
		dispatch(getUserPermissions());
	}, []);
	const dispatch = useDispatch();
	return (
		<FusePageCarded
			classes={{
				content: 'flex',
				contentCard: 'overflow-hidden',
				header: 'min-h-74 h-64'
			}}
			header={UserPermissions.includes(PURCHASE_LIST) && <PurchasesHeader />}
			content={UserPermissions.includes(PURCHASE_LIST) ? <PurchasesTable /> : <PagenotFound />}
			innerScroll
		/>
	);
};
export default withReducer('purchasesBillManagement', reducer)(Purchases);
