import FusePageCarded from '@fuse/core/FusePageCarded';
import { JOURNAL_LIST } from 'app/constant/permission/permission';
import PagenotFound from 'app/main/cashconnect/Pagenotfound/PagenotFound';
import { getUserPermissions } from 'app/store/dataSlice';
import withReducer from 'app/store/withReducer';
import React, { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import reducer from '../store/index';
import JournalsHeader from './JournalsHeader';
import JournalsTable from './JournalsTable';

const Journals = () => {
	const dispatch = useDispatch();

	const UserPermissions = useSelector(state => state.data.UserPermissions);
	useEffect(() => {
		dispatch(getUserPermissions());
	}, []);
	return (
		<FusePageCarded
			classes={{
				content: 'flex',
				contentCard: 'overflow-hidden',
				header: 'min-h-74 h-64'
			}}
			header={UserPermissions.includes(JOURNAL_LIST) && <JournalsHeader />}
			content={UserPermissions.includes(JOURNAL_LIST) ? <JournalsTable /> : <PagenotFound />}
			innerScroll
		/>
	);
};
export default withReducer('journalsManagement', reducer)(Journals);
