import FusePageCarded from '@fuse/core/FusePageCarded';
import { useDeepCompareEffect } from '@fuse/hooks';
import { yupResolver } from '@hookform/resolvers/yup';
import { Button, Typography } from '@material-ui/core';
import useUserInfo from 'app/@customHooks/useUserInfo';
import setIdIfValueIsObjArryData from 'app/@helpers/setIdIfValueIsObjArryData';
import setIdIfValueIsObject2 from 'app/@helpers/setIdIfValueIsObject2';
import { JOURNAL_CREATE, JOURNAL_DELETE, JOURNAL_DETAILS, JOURNAL_UPDATE } from 'app/constant/permission/permission';
import PagenotFound from 'app/main/cashconnect/Pagenotfound/PagenotFound';
import { getUserPermissions } from 'app/store/dataSlice';
import withReducer from 'app/store/withReducer';
import { motion } from 'framer-motion';
import React, { useEffect, useState } from 'react';
import { FormProvider, useForm } from 'react-hook-form';
import { useDispatch, useSelector } from 'react-redux';
import { Link, useParams } from 'react-router-dom';
import * as yup from 'yup';
import reducer from '../store/index';
import { getJournal, newJournal, resetJournal, setUserBasedBranch } from '../store/journalSlice';
import JournalForm from './JournalForm';
import NewJournalHeader from './NewJournalHeader';

/**
 * Form Validation Schema
 */
const schema = yup.object().shape({
	journal_date: yup.date().required('Journal Date is required')
});

const Journal = () => {
	const dispatch = useDispatch();
	const journal = useSelector(({ journalsManagement }) => journalsManagement.journal);
	const [noJournal, setNoJournal] = useState(false);
	const methods = useForm({
		mode: 'onChange',
		defaultValues: {},
		resolver: yupResolver(schema)
	});

	const UserPermissions = useSelector(state => state.data.UserPermissions);
	useEffect(() => {
		dispatch(getUserPermissions());
	}, []);
	const routeParams = useParams();

	const { reset } = methods;

	const [letFormSave, setLetFormSave] = useState(false);

	const { userId } = useUserInfo();

	useDeepCompareEffect(() => {
		function updateJournalState() {
			const { journalId, journalName } = routeParams;

			if (journalId === 'new') {
				localStorage.removeItem('event');
				/**
				 * Create New User data
				 */
				dispatch(newJournal());
				dispatch(setUserBasedBranch(userId));
			} else {
				/**
				 * Get User data
				 */

				dispatch(getJournal(journalName)).then(action => {
					/**
					 * If the requested product is not exist show message
					 */
					if (!action.payload) {
						setNoJournal(true);
					}
				});
			}
		}

		updateJournalState();
	}, [dispatch, routeParams]);

	useEffect(() => {}, []);

	useEffect(() => {
		if (!journal) {
			return;
		}
		/**
		 * Reset the form on journal state changes
		 */
		const convertedJournalItems = setIdIfValueIsObjArryData(journal?.items);
		const convertedJournal = setIdIfValueIsObject2(journal);
		reset({ ...convertedJournal, items: convertedJournalItems });
	}, [journal, reset]);

	useEffect(() => {
		return () => {
			/**
			 * Reset Journal on component unload
			 */
			dispatch(resetJournal());
			setNoJournal(false);
		};
	}, [dispatch]);

	/**
	 * Show Message if the requested products is not exists
	 */
	if (noJournal) {
		return (
			<motion.div
				initial={{ opacity: 0 }}
				animate={{ opacity: 1, transition: { delay: 0.1 } }}
				className="flex flex-col flex-1 items-center justify-center h-full"
			>
				<Typography color="textSecondary" variant="h5">
					There is no such journal!
				</Typography>
				<Button
					className="mt-24"
					component={Link}
					variant="outlined"
					to="/apps/e-commerce/products"
					color="inherit"
				>
					Go to Journal Page
				</Button>
			</motion.div>
		);
	}

	return (
		<FormProvider {...methods}>
			{UserPermissions.includes(JOURNAL_CREATE) ||
			UserPermissions.includes(JOURNAL_UPDATE) ||
			UserPermissions.includes(JOURNAL_DELETE) ||
			UserPermissions.includes(JOURNAL_DETAILS) ? (
				<FusePageCarded
					classes={{
						toolbar: 'p-0',
						header: 'min-h-80 h-80'
					}}
					header={<NewJournalHeader letFormSave={letFormSave} />}
					content={
						<div className="p-16 sm:p-24">
							<JournalForm setLetFormSave={setLetFormSave} />
						</div>
					}
					innerScroll
				/>
			) : (
				<PagenotFound />
			)}
		</FormProvider>
	);
};
export default withReducer('journalsManagement', reducer)(Journal);
