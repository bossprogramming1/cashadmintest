import FusePageCarded from '@fuse/core/FusePageCarded';
import { GROUP_LIST } from 'app/constant/permission/permission';
import PagenotFound from 'app/main/cashconnect/Pagenotfound/PagenotFound';
import { getUserPermissions } from 'app/store/dataSlice';
import withReducer from 'app/store/withReducer';
import React, { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import reducer from '../store/index';
import GroupsHeader from './GroupsHeader';
import GroupsTable from './GroupsTable';

const Groups = () => {
	const UserPermissions = useSelector(state => state.data.UserPermissions);
	useEffect(() => {
		dispatch(getUserPermissions());
	}, []);
	const dispatch = useDispatch();

	return (
		<FusePageCarded
			classes={{
				content: 'flex',
				contentCard: 'overflow-hidden',
				header: 'min-h-64 h-64'
			}}
			header={UserPermissions.includes(GROUP_LIST) && <GroupsHeader />}
			content={UserPermissions.includes(GROUP_LIST) ? <GroupsTable /> : <PagenotFound />}
			innerScroll
		/>
	);
};
export default withReducer('groupsManagement', reducer)(Groups);
