import FusePageCarded from '@fuse/core/FusePageCarded';
import { useDeepCompareEffect } from '@fuse/hooks';
import { yupResolver } from '@hookform/resolvers/yup';
import { Button, Typography } from '@material-ui/core';
import useUserInfo from 'app/@customHooks/useUserInfo';
import setIdIfValueIsObjArryData from 'app/@helpers/setIdIfValueIsObjArryData';
import setIdIfValueIsObject2 from 'app/@helpers/setIdIfValueIsObject2';
import {
	RECEIPT_VOUCHER_CREATE,
	RECEIPT_VOUCHER_DELETE,
	RECEIPT_VOUCHER_DETAILS,
	RECEIPT_VOUCHER_UPDATE
} from 'app/constant/permission/permission';
import PagenotFound from 'app/main/cashconnect/Pagenotfound/PagenotFound';

import { getUserPermissions } from 'app/store/dataSlice';
import withReducer from 'app/store/withReducer';
import { motion } from 'framer-motion';
import React, { useEffect, useState } from 'react';
import { FormProvider, useForm } from 'react-hook-form';
import { useDispatch, useSelector } from 'react-redux';
import { Link, useParams } from 'react-router-dom';
import * as yup from 'yup';
import reducer from '../store/index';
import {
	getReceiptVoucher,
	newReceiptVoucher,
	resetReceiptVoucher,
	setUserBasedBranch
} from '../store/receiptVoucherSlice';
import NewReceiptVoucherHeader from './NewReceiptVoucherHeader';
import ReceiptVoucherForm from './ReceiptVoucherForm';

const schema = yup.object().shape({
	receipt_date: yup.date().required('Receipt Date is required')
});

const ReceiptVoucher = () => {
	const dispatch = useDispatch();
	const receiptVoucher = useSelector(({ receiptVouchersManagement }) => receiptVouchersManagement.receiptVoucher);
	const [noReceiptVoucher, setNoReceiptVoucher] = useState(false);
	const methods = useForm({
		mode: 'onChange',
		defaultValues: {},
		resolver: yupResolver(schema)
	});
	const UserPermissions = useSelector(state => state.data.UserPermissions);
	useEffect(() => {
		dispatch(getUserPermissions());
	}, []);
	const routeParams = useParams();

	const { reset } = methods;

	const [letFormSave, setLetFormSave] = useState(false);

	const { userId } = useUserInfo();

	useDeepCompareEffect(() => {
		function updateReceiptVoucherState() {
			const { receiptVoucherId, receiptVoucherName } = routeParams;

			if (receiptVoucherId === 'new') {
				localStorage.removeItem('event');
				/**
				 * Create New User data
				 */

				dispatch(newReceiptVoucher());
				dispatch(setUserBasedBranch(userId));
			} else {
				/**
				 * Get User data
				 */

				dispatch(getReceiptVoucher(receiptVoucherName)).then(action => {
					/**
					 * If the requested product is not exist show message
					 */
					if (!action.payload) {
						setNoReceiptVoucher(true);
					}
				});
			}
		}

		updateReceiptVoucherState();
	}, [dispatch, routeParams]);

	useEffect(() => {}, []);

	useEffect(() => {
		if (!receiptVoucher) {
			return;
		}
		/**
		 * Reset the form on receiptVoucher state changes
		 */
		const convertedReceiptVoucherItems = setIdIfValueIsObjArryData(receiptVoucher?.items);
		const convertedReceiptVoucher = setIdIfValueIsObject2(receiptVoucher);
		reset({ ...convertedReceiptVoucher, items: convertedReceiptVoucherItems });
	}, [receiptVoucher, reset]);

	useEffect(() => {
		return () => {
			/**
			 * Reset ReceiptVoucher on component unload
			 */
			dispatch(resetReceiptVoucher());
			setNoReceiptVoucher(false);
		};
	}, [dispatch]);

	/**
	 * Show Message if the requested products is not exists
	 */
	if (noReceiptVoucher) {
		return (
			<motion.div
				initial={{ opacity: 0 }}
				animate={{ opacity: 1, transition: { delay: 0.1 } }}
				className="flex flex-col flex-1 items-center justify-center h-full"
			>
				<Typography color="textSecondary" variant="h5">
					There is no such receiptVoucher!
				</Typography>
				<Button
					className="mt-24"
					component={Link}
					variant="outlined"
					to="/apps/e-commerce/products"
					color="inherit"
				>
					Go to ReceiptVoucher Page
				</Button>
			</motion.div>
		);
	}

	return (
		<FormProvider {...methods}>
			{UserPermissions.includes(RECEIPT_VOUCHER_CREATE) ||
			UserPermissions.includes(RECEIPT_VOUCHER_UPDATE) ||
			UserPermissions.includes(RECEIPT_VOUCHER_DELETE) ||
			UserPermissions.includes(RECEIPT_VOUCHER_DETAILS) ? (
				<FusePageCarded
					classes={{
						toolbar: 'p-0',
						header: 'min-h-80 h-80'
					}}
					header={<NewReceiptVoucherHeader letFormSave={letFormSave} />}
					content={
						<div className="p-16 sm:p-24">
							<ReceiptVoucherForm setLetFormSave={setLetFormSave} />
						</div>
					}
					innerScroll
				/>
			) : (
				<PagenotFound />
			)}
		</FormProvider>
	);
};
export default withReducer('receiptVouchersManagement', reducer)(ReceiptVoucher);
