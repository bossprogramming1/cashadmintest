import FuseLoading from '@fuse/core/FuseLoading';
import FuseScrollbars from '@fuse/core/FuseScrollbars';
import _ from '@lodash';
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import PlaylistAddCheckIcon from '@material-ui/icons/PlaylistAddCheck';
import DataUsageIcon from '@material-ui/icons/DataUsage';
import CancelIcon from '@material-ui/icons/Cancel';
import { Tooltip, Typography } from '@material-ui/core';
import Checkbox from '@material-ui/core/Checkbox';
import { makeStyles } from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TablePagination from '@material-ui/core/TablePagination';
import TableRow from '@material-ui/core/TableRow';
import DeleteIcon from '@material-ui/icons/Delete';
import EditIcon from '@material-ui/icons/Edit';
import PrintIcon from '@material-ui/icons/Print';
import { Pagination } from '@material-ui/lab';
import { rowsPerPageOptions } from 'app/@data/data';
import { motion } from 'framer-motion';
import React, { useEffect, useRef, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { withRouter } from 'react-router-dom';
import axios from 'axios';
import { getUserPermissions } from 'app/store/dataSlice';
import { RECEIPT_VOUCHER_DELETE, RECEIPT_VOUCHER_UPDATE } from 'app/constant/permission/permission';
import moment from 'moment';
import { SEARCH_RECEIPTVOUCHER, UPDATE_RECEIPTVOUCHER } from '../../../../../constant/constants';
import PrintVoucher from '../../AccountComponents/PrintVoucher';
import { getReceiptVouchers, selectReceiptVouchers } from '../store/receiptVouchersSlice';
import ReceiptVouchersTableHead from './ReceiptVouchersTableHead';

const useStyles = makeStyles(() => ({
	root: {
		display: 'flex',
		justifyContent: 'space-between',
		flexWrap: 'nowrap',
		overflow: 'auto',
		minHeight: '35px'
	},
	toolbar: {
		'& > div': {
			minHeight: 'fit-content'
		}
	}
}));

const ReceiptVouchersTable = props => {
	const UserPermissions = useSelector(state => state.data.UserPermissions);
	useEffect(() => {
		dispatch(getUserPermissions());
	}, []);
	const dispatch = useDispatch();
	const classes = useStyles();
	const receiptVouchers = useSelector(selectReceiptVouchers);
	const searchText = useSelector(
		({ receiptVouchersManagement }) => receiptVouchersManagement.receiptVouchers.searchText
	);
	const [searchReceiptVoucher, setSearchReceiptVoucher] = useState([]);
	const [loading, setLoading] = useState(true);
	const [selected, setSelected] = useState([]);
	const [page, setPage] = useState(0);
	const [rowsPerPage, setRowsPerPage] = useState(30);
	const [pageAndSize, setPageAndSize] = useState({ page: 1, size: 30 });
	const [openPendingStatusAlert, setOpenPendingStatusAlert] = React.useState(false);
	const [openSuccessStatusAlert, setOpenSuccessStatusAlert] = React.useState(false);
	const [openCanceledStatusAlert, setOpenCanceledStatusAlert] = React.useState(false);
	const [openDeleteStatusAlert, setOpenDeleteStatusAlert] = React.useState(false);
	const [order, setOrder] = useState({
		direction: 'asc',
		id: null
	});

	let serialNumber = 1;
	const totalPages = sessionStorage.getItem('total_receiptVouchers_pages');
	const totalElements = sessionStorage.getItem('total_receiptVouchers_elements');
	const currentDate = moment().format('DD-MM-YYYY');

	const printVoucherRef = useRef();

	useEffect(() => {
		dispatch(getReceiptVouchers(pageAndSize)).then(() => setLoading(false));
	}, [dispatch]);

	useEffect(() => {
		searchText !== '' ? getSearchReceiptVoucher() : setSearchReceiptVoucher([]);
	}, [searchText]);

	const getSearchReceiptVoucher = () => {
		const authTOKEN = {
			headers: {
				'Content-type': 'application/json',
				Authorization: localStorage.getItem('jwt_access_token')
			}
		};
		fetch(`${SEARCH_RECEIPTVOUCHER}?invoice_no=${searchText}`, authTOKEN)
			.then(response => response.json())
			.then(searchedReceiptVoucherData => {
				setSearchReceiptVoucher(searchedReceiptVoucherData?.receipt_vouchers);
			})
			.catch(() => setSearchReceiptVoucher([]));
	};

	function handleRequestSort(_e, property) {
		const id = property;
		let direction = 'desc';

		if (order.id === property && order.direction === 'desc') {
			direction = 'asc';
		}

		setOrder({
			direction,
			id
		});
	}

	function handleSelectAllClick(receiptVoucherEvent) {
		if (receiptVoucherEvent.target.checked) {
			setSelected((!_.isEmpty(searchReceiptVoucher) ? searchReceiptVoucher : receiptVouchers).map(n => n.id));
			return;
		}
		setSelected([]);
	}

	function handleDeselect() {
		setSelected([]);
	}

	function handleUpdateReceiptVoucher(item) {
		localStorage.removeItem('receiptVoucherEvent');
		props.history.push(`/apps/receiptVoucher-management/receiptVouchers/${item.id}/${item.invoice_no}`);
	}
	function handleDeleteReceiptVoucher(item, receiptVoucherEvent) {
		localStorage.removeItem('receiptVoucherEvent');
		localStorage.setItem('receiptVoucherEvent', receiptVoucherEvent);
		props.history.push(`/apps/receiptVoucher-management/receiptVouchers/${item.id}/${item.invoice_no}`);
	}

	function handleCheck(_e, id) {
		const selectedIndex = selected.indexOf(id);
		let newSelected = [];

		if (selectedIndex === -1) {
			newSelected = newSelected.concat(selected, id);
		} else if (selectedIndex === 0) {
			newSelected = newSelected.concat(selected.slice(1));
		} else if (selectedIndex === selected.length - 1) {
			newSelected = newSelected.concat(selected.slice(0, -1));
		} else if (selectedIndex > 0) {
			newSelected = newSelected.concat(selected.slice(0, selectedIndex), selected.slice(selectedIndex + 1));
		}

		setSelected(newSelected);
	}

	//pagination
	const handlePagination = (_e, handlePage) => {
		setPageAndSize({ ...pageAndSize, page: handlePage });
		setPage(handlePage - 1);
		dispatch(getReceiptVouchers({ ...pageAndSize, page: handlePage }));
	};

	function handleChangePage(_e, value) {
		setPage(value);
		setPageAndSize({ ...pageAndSize, page: value + 1 });
		dispatch(getReceiptVouchers({ ...pageAndSize, page: value + 1 }));
	}

	function handleChangeRowsPerPage(receiptVoucherEvent) {
		setRowsPerPage(receiptVoucherEvent.target.value);
		setPageAndSize({ ...pageAndSize, size: receiptVoucherEvent.target.value });
		dispatch(getReceiptVouchers({ ...pageAndSize, size: receiptVoucherEvent.target.value }));
	}

	if (loading) {
		return <FuseLoading />;
	}

	if (receiptVouchers?.length === 0) {
		return (
			<motion.div
				initial={{ opacity: 0 }}
				animate={{ opacity: 1, transition: { delay: 0.1 } }}
				className="flex flex-1 items-center justify-center h-full"
			>
				<Typography color="textSecondary" variant="h5">
					There are no receiptVoucher!
				</Typography>
			</motion.div>
		);
	}
	const role = localStorage.getItem('user_role').toLowerCase();

	return (
		<div className="w-full flex flex-col">
			<FuseScrollbars className="flex-grow overflow-x-auto">
				<PrintVoucher ref={printVoucherRef} title="Receipt Voucher" type="receipt" />
				<Table stickyHeader className="min-w-xl" aria-labelledby="tableTitle">
					<ReceiptVouchersTableHead
						selectedReceiptVoucherIds={selected}
						order={order}
						onSelectAllClick={handleSelectAllClick}
						onRequestSort={handleRequestSort}
						rowCount={(!_.isEmpty(searchReceiptVoucher) ? searchReceiptVoucher : receiptVouchers).length}
						onMenuItemClick={handleDeselect}
						pagination={pageAndSize}
					/>

					<TableBody>
						{_.orderBy(
							searchText !== '' && !_.isEmpty(searchReceiptVoucher)
								? searchReceiptVoucher
								: receiptVouchers,
							[
								o => {
									switch (order.id) {
										case 'categories': {
											return o.categories[0];
										}
										default: {
											return o[order.id];
										}
									}
								}
							],
							[order.direction]
						).map(n => {
							const isSelected = selected.indexOf(n.id) !== -1;
							return (
								<TableRow
									className="h-52 cursor-pointer"
									hover
									role="checkbox"
									aria-checked={isSelected}
									tabIndex={-1}
									key={n.id}
									selected={isSelected}
								>
									<TableCell className="w-40 md:w-64" component="th" scope="row">
										{pageAndSize.page * pageAndSize.size - pageAndSize.size + serialNumber++}
									</TableCell>
									<TableCell className="p-4 md:p-16 whitespace-nowrap" component="th" scope="row">
										{n.receipt_date && moment(new Date(n.receipt_date)).format('DD-MM-YYYY')}{' '}
									</TableCell>

									<TableCell className="p-4 md:p-16" component="th" scope="row">
										{n.branch?.name}
									</TableCell>
									<TableCell className="p-4 md:p-16" component="th" scope="row">
										{n.invoice_no}
									</TableCell>

									<TableCell className="p-4 md:p-16" component="th" scope="row">
										{n?.related_ledgers?.toString()}
									</TableCell>

									<TableCell className="p-4 md:p-16" component="th" scope="row">
										{n.sub_ledger?.name}
									</TableCell>

									<TableCell className="p-4 md:p-16" component="th" scope="row">
										{`${n?.details ? n?.details + ',' : ''} ${n.ledger?.name || ''}`}
									</TableCell>

									<TableCell className="p-4 md:p-16" component="th" scope="row">
										{n.amount}
									</TableCell>
									<TableCell className="p-4 md:p-16" align="center" component="th" scope="row">
										<div className="flex flex-nowrap">
											<Tooltip title="Print" placement="top" enterDelay={300}>
												<PrintIcon
													className="cursor-pointer mr-3"
													style={{ color: 'blue' }}
													onClick={() => printVoucherRef.current.doPrint(n)}
												/>
											</Tooltip>
											<Tooltip title="Edit" placement="top" enterDelay={300}>
												<EditIcon
													style={{
														color: 'green'
													}}
													onClick={() => handleUpdateReceiptVoucher(n)}
													className="cursor-pointer"
												/>
											</Tooltip>
											<Tooltip title="Delete" placement="top" enterDelay={300}>
												<DeleteIcon
													onClick={() => handleDeleteReceiptVoucher(n, 'Delete')}
													className="cursor-pointer"
													style={{
														color: 'red'
													}}
												/>
											</Tooltip>
										</div>
									</TableCell>
								</TableRow>
							);
						})}
					</TableBody>
				</Table>
			</FuseScrollbars>

			<div className={classes.root}>
				<Pagination
					classes={{ ul: 'flex-nowrap' }}
					count={totalPages}
					defaultPage={1}
					color="primary"
					showFirstButton
					showLastButton
					variant="outlined"
					shape="rounded"
					onChange={handlePagination}
				/>

				<TablePagination
					classes={{ root: 'overflow-visible' }}
					rowsPerPageOptions={rowsPerPageOptions}
					component="div"
					count={totalElements}
					rowsPerPage={rowsPerPage}
					page={page}
					className={classes.toolbar}
					backIconButtonProps={{
						'aria-label': 'Previous Page',
						className: 'py-0'
					}}
					nextIconButtonProps={{
						'aria-label': 'Next Page',
						className: 'py-0'
					}}
					onChangePage={handleChangePage}
					onChangeRowsPerPage={handleChangeRowsPerPage}
				/>
			</div>
		</div>
	);
};

export default withRouter(ReceiptVouchersTable);
