import FusePageCarded from '@fuse/core/FusePageCarded';
import { RECEIPT_VOUCHER_LIST } from 'app/constant/permission/permission';
import PagenotFound from 'app/main/cashconnect/Pagenotfound/PagenotFound';
import { getUserPermissions } from 'app/store/dataSlice';
import withReducer from 'app/store/withReducer';
import React, { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import reducer from '../store/index';
import ReceiptVouchersHeader from './ReceiptVouchersHeader';
import ReceiptVouchersTable from './ReceiptVouchersTable';

const ReceiptVouchers = () => {
	const UserPermissions = useSelector(state => state.data.UserPermissions);
	useEffect(() => {
		dispatch(getUserPermissions());
	}, []);
	const dispatch = useDispatch();
	return (
		<FusePageCarded
			classes={{
				content: 'flex',
				contentCard: 'overflow-hidden',
				header: 'min-h-64 h-64'
			}}
			header={UserPermissions.includes(RECEIPT_VOUCHER_LIST) && <ReceiptVouchersHeader />}
			content={UserPermissions.includes(RECEIPT_VOUCHER_LIST) ? <ReceiptVouchersTable /> : <PagenotFound />}
			innerScroll
		/>
	);
};
export default withReducer('receiptVouchersManagement', reducer)(ReceiptVouchers);
