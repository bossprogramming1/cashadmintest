import FusePageCarded from '@fuse/core/FusePageCarded';
import { LEDGER_ACCOUNT_LIST } from 'app/constant/permission/permission';
import PagenotFound from 'app/main/cashconnect/Pagenotfound/PagenotFound';
import { getUserPermissions } from 'app/store/dataSlice';
import withReducer from 'app/store/withReducer';
import React, { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import reducer from '../store/index';
import LedgersHeader from './LedgersHeader';
import LedgersTable from './LedgersTable';

const Ledgers = () => {
	const UserPermissions = useSelector(state => state.data.UserPermissions);
	useEffect(() => {
		dispatch(getUserPermissions());
	}, []);
	const dispatch = useDispatch();
	return (
		<FusePageCarded
			classes={{
				content: 'flex',
				contentCard: 'overflow-hidden',
				header: 'min-h-74 h-64'
			}}
			header={UserPermissions.includes(LEDGER_ACCOUNT_LIST) && <LedgersHeader />}
			content={UserPermissions.includes(LEDGER_ACCOUNT_LIST) ? <LedgersTable /> : <PagenotFound />}
			innerScroll
		/>
	);
};
export default withReducer('ledgersManagement', reducer)(Ledgers);
