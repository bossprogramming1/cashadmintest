import FusePageCarded from '@fuse/core/FusePageCarded';
import { useDeepCompareEffect } from '@fuse/hooks';
import { yupResolver } from '@hookform/resolvers/yup';
import { Button, Typography } from '@material-ui/core';
import setIdIfValueIsObject2 from 'app/@helpers/setIdIfValueIsObject2';
import {
	LEDGER_ACCOUNT_CREATE,
	LEDGER_ACCOUNT_DELETE,
	LEDGER_ACCOUNT_DETAILS,
	LEDGER_ACCOUNT_UPDATE
} from 'app/constant/permission/permission';
import PagenotFound from 'app/main/cashconnect/Pagenotfound/PagenotFound';
import { getUserPermissions } from 'app/store/dataSlice';
import withReducer from 'app/store/withReducer';
import { motion } from 'framer-motion';
import React, { useEffect, useState } from 'react';
import { FormProvider, useForm } from 'react-hook-form';
import { useDispatch, useSelector } from 'react-redux';
import { Link, useParams } from 'react-router-dom';
import * as yup from 'yup';
import reducer from '../store/index';
import { getLedger, newLedger, resetLedger } from '../store/ledgerSlice';
import LedgerForm from './LedgerForm';
import NewLedgerHeader from './NewLedgerHeader';

/**
 * Form Validation Schema
 */
const schema = yup.object().shape({
	name: yup.string().required('Name is required')
});

const Ledger = () => {
	const dispatch = useDispatch();
	const ledger = useSelector(({ ledgersManagement }) => ledgersManagement.ledger);

	const [noLedger, setNoLedger] = useState(false);
	const methods = useForm({
		mode: 'onChange',
		defaultValues: {},
		resolver: yupResolver(schema)
	});
	const routeParams = useParams();

	const { reset } = methods;
	const UserPermissions = useSelector(state => state.data.UserPermissions);
	useEffect(() => {
		dispatch(getUserPermissions());
	}, []);
	useDeepCompareEffect(() => {
		function updateLedgerState() {
			const { ledgerId } = routeParams;

			if (ledgerId === 'new') {
				localStorage.removeItem('event');
				/**
				 * Create New User data
				 */
				dispatch(newLedger());
			} else {
				/**
				 * Get User data
				 */

				dispatch(getLedger(ledgerId)).then(action => {
					/**
					 * If the requested product is not exist show message
					 */
					if (!action.payload) {
						setNoLedger(true);
					}
				});
			}
		}

		updateLedgerState();
	}, [dispatch, routeParams]);

	useEffect(() => {}, []);

	useEffect(() => {
		if (!ledger) {
			return;
		}
		/**
		 * Reset the form on ledger state changes
		 */
		reset(setIdIfValueIsObject2(ledger));
	}, [ledger, reset]);

	useEffect(() => {
		return () => {
			/**
			 * Reset Ledger on component unload
			 */
			dispatch(resetLedger());
			setNoLedger(false);
		};
	}, [dispatch]);

	/**
	 * Show Message if the requested products is not exists
	 */
	if (noLedger) {
		return (
			<motion.div
				initial={{ opacity: 0 }}
				animate={{ opacity: 1, transition: { delay: 0.1 } }}
				className="flex flex-col flex-1 items-center justify-center h-full"
			>
				<Typography color="textSecondary" variant="h5">
					There is no such ledger!
				</Typography>
				<Button
					className="mt-24"
					component={Link}
					variant="outlined"
					to="/apps/e-commerce/products"
					color="inherit"
				>
					Go to Ledger Page
				</Button>
			</motion.div>
		);
	}

	return (
		<FormProvider {...methods}>
			{UserPermissions.includes(LEDGER_ACCOUNT_CREATE) ||
			UserPermissions.includes(LEDGER_ACCOUNT_UPDATE) ||
			UserPermissions.includes(LEDGER_ACCOUNT_DELETE) ||
			UserPermissions.includes(LEDGER_ACCOUNT_DETAILS) ? (
				<FusePageCarded
					classes={{
						toolbar: 'p-0',
						header: 'min-h-74 h-64'
					}}
					header={<NewLedgerHeader />}
					content={
						<div className="p-16 sm:p-24">
							<LedgerForm />
						</div>
					}
					innerScroll
				/>
			) : (
				<PagenotFound />
			)}
		</FormProvider>
	);
};
export default withReducer('ledgersManagement', reducer)(Ledger);
