import FusePageCarded from '@fuse/core/FusePageCarded';
import { PAYMENT_VOUCHER_LIST } from 'app/constant/permission/permission';
import PagenotFound from 'app/main/cashconnect/Pagenotfound/PagenotFound';
import { getUserPermissions } from 'app/store/dataSlice';
import withReducer from 'app/store/withReducer';
import React, { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import reducer from '../store/index';
import PaymentvouchersHeader from './PaymentvouchersHeader';
import PaymentvouchersTable from './PaymentvouchersTable';

const Paymentvouchers = () => {
	const UserPermissions = useSelector(state => state.data.UserPermissions);
	useEffect(() => {
		dispatch(getUserPermissions());
	}, []);
	const dispatch = useDispatch();
	return (
		<FusePageCarded
			classes={{
				content: 'flex',
				contentCard: 'overflow-hidden',
				header: 'min-h-64 h-64'
			}}
			header={UserPermissions.includes(PAYMENT_VOUCHER_LIST) && <PaymentvouchersHeader />}
			content={UserPermissions.includes(PAYMENT_VOUCHER_LIST) ? <PaymentvouchersTable /> : <PagenotFound />}
			innerScroll
		/>
	);
};
export default withReducer('paymentVouchersManagement', reducer)(Paymentvouchers);
