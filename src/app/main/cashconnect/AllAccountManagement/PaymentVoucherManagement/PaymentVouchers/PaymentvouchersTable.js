import FuseLoading from '@fuse/core/FuseLoading';
import FuseScrollbars from '@fuse/core/FuseScrollbars';
import _ from '@lodash';
import PlaylistAddCheckIcon from '@material-ui/icons/PlaylistAddCheck';
import DataUsageIcon from '@material-ui/icons/DataUsage';
import CancelIcon from '@material-ui/icons/Cancel';
import Button from '@mui/material/Button';
import Dialog from '@mui/material/Dialog';
import DialogActions from '@mui/material/DialogActions';
import DialogContent from '@mui/material/DialogContent';
import DialogContentText from '@mui/material/DialogContentText';
import DialogTitle from '@mui/material/DialogTitle';
import { Tooltip, Typography } from '@material-ui/core';
import Checkbox from '@material-ui/core/Checkbox';
import { makeStyles } from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TablePagination from '@material-ui/core/TablePagination';
import TableRow from '@material-ui/core/TableRow';
import DeleteIcon from '@material-ui/icons/Delete';
import EditIcon from '@material-ui/icons/Edit';
import PrintIcon from '@material-ui/icons/Print';
import { Pagination } from '@material-ui/lab';
import { rowsPerPageOptions } from 'app/@data/data';
import { motion } from 'framer-motion';
import React, { useEffect, useRef, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { withRouter } from 'react-router-dom';
import axios from 'axios';
import { PAYMENT_VOUCHER_DELETE, PAYMENT_VOUCHER_UPDATE } from 'app/constant/permission/permission';
import { getUserPermissions } from 'app/store/dataSlice';
import moment from 'moment';
import { SEARCH_PAYMENTVOUCHER, UPDATE_PAYMENTVOUCHER } from '../../../../../constant/constants';
import PrintVoucher from '../../AccountComponents/PrintVoucher';
import { getPaymentVouchers, selectPaymentVouchers } from '../store/paymentvouchersSlice';
import PaymentvouchersTableHead from './PaymentvouchersTableHead';

const useStyles = makeStyles(() => ({
	root: {
		display: 'flex',
		justifyContent: 'space-between',
		flexWrap: 'nowrap',
		overflow: 'auto',
		minHeight: '35px'
	},
	toolbar: {
		'& > div': {
			minHeight: 'fit-content'
		}
	}
}));

const PaymentvouchersTable = props => {
	const UserPermissions = useSelector(state => state.data.UserPermissions);

	const dispatch = useDispatch();
	const classes = useStyles();
	const paymentVouchers = useSelector(selectPaymentVouchers);
	const searchText = useSelector(
		({ paymentVouchersManagement }) => paymentVouchersManagement.paymentVouchers.searchText
	);
	const [searchPaymentVoucher, setSearchPaymentVoucher] = useState([]);
	const [loading, setLoading] = useState(true);
	const [selected, setSelected] = useState([]);
	const [page, setPage] = useState(0);
	const [rowsPerPage, setRowsPerPage] = useState(30);
	const [pageAndSize, setPageAndSize] = useState({ page: 1, size: 30 });
	const currentDate = moment().format('DD-MM-YYYY');

	const [openPendingStatusAlert, setOpenPendingStatusAlert] = React.useState(false);
	const [openSuccessStatusAlert, setOpenSuccessStatusAlert] = React.useState(false);
	const [openCanceledStatusAlert, setOpenCanceledStatusAlert] = React.useState(false);
	const [openDeleteStatusAlert, setOpenDeleteStatusAlert] = React.useState(false);

	const [value, setValue] = useState();
	useEffect(() => {
		dispatch(getUserPermissions());
	}, []);
	const refresh = () => {
		// re-renders the component
		setValue({});
	};
	const [order, setOrder] = useState({
		direction: 'asc',
		id: null
	});

	let serialNumber = 1;
	const totalPages = sessionStorage.getItem('total_paymentVouchers_pages');
	const totalElements = sessionStorage.getItem('total_paymentVouchers_elements');

	const printVoucherRef = useRef();

	useEffect(() => {
		dispatch(getPaymentVouchers(pageAndSize)).then(() => setLoading(false));
	}, [dispatch]);

	useEffect(() => {
		searchText !== '' ? getSearchPaymentVoucher() : setSearchPaymentVoucher([]);
	}, [searchText]);

	const getSearchPaymentVoucher = () => {
		const authTOKEN = {
			headers: {
				'Content-type': 'application/json',
				Authorization: localStorage.getItem('jwt_access_token')
			}
		};

		fetch(`${SEARCH_PAYMENTVOUCHER}?invoice_no=${searchText}`, authTOKEN)
			.then(response => response.json())
			.then(searchedPaymentVoucherData => {
				setSearchPaymentVoucher(searchedPaymentVoucherData?.payment_vouchers);
			})
			.catch(() => setSearchPaymentVoucher([]));
	};

	function handleRequestSort(_e, property) {
		const id = property;
		let direction = 'desc';

		if (order.id === property && order.direction === 'desc') {
			direction = 'asc';
		}

		setOrder({
			direction,
			id
		});
	}

	function handleSelectAllClick(paymentVoucherEvent) {
		if (paymentVoucherEvent.target.checked) {
			setSelected((!_.isEmpty(searchPaymentVoucher) ? searchPaymentVoucher : paymentVouchers).map(n => n.id));
			return;
		}
		setSelected([]);
	}

	function handleDeselect() {
		setSelected([]);
	}

	function handleUpdatePaymentVoucher(item) {
		localStorage.removeItem('paymentVoucherEvent');
		props.history.push(`/apps/paymentVoucher-management/paymentVouchers/${item.id}/${item.invoice_no}`);
	}
	function handleDeletePaymentVoucher(item, paymentVoucherEvent) {
		localStorage.removeItem('paymentVoucherEvent');
		localStorage.setItem('paymentVoucherEvent', paymentVoucherEvent);
		props.history.push(`/apps/paymentVoucher-management/paymentVouchers/${item.id}/${item.invoice_no}`);
	}

	function handleCheck(_e, id) {
		const selectedIndex = selected.indexOf(id);
		let newSelected = [];

		if (selectedIndex === -1) {
			newSelected = newSelected.concat(selected, id);
		} else if (selectedIndex === 0) {
			newSelected = newSelected.concat(selected.slice(1));
		} else if (selectedIndex === selected.length - 1) {
			newSelected = newSelected.concat(selected.slice(0, -1));
		} else if (selectedIndex > 0) {
			newSelected = newSelected.concat(selected.slice(0, selectedIndex), selected.slice(selectedIndex + 1));
		}

		setSelected(newSelected);
	}

	//pagination
	const handlePagination = (_e, handlePage) => {
		setPageAndSize({ ...pageAndSize, page: handlePage });
		setPage(handlePage - 1);
		dispatch(getPaymentVouchers({ ...pageAndSize, page: handlePage }));
	};

	function handleChangePage(_e, value) {
		setPage(value);
		setPageAndSize({ ...pageAndSize, page: value + 1 });
		dispatch(getPaymentVouchers({ ...pageAndSize, page: value + 1 }));
	}

	function handleChangeRowsPerPage(paymentVoucherEvent) {
		setRowsPerPage(paymentVoucherEvent.target.value);
		setPageAndSize({ ...pageAndSize, size: paymentVoucherEvent.target.value });
		dispatch(getPaymentVouchers({ ...pageAndSize, size: paymentVoucherEvent.target.value }));
	}

	if (loading) {
		return <FuseLoading />;
	}

	if (paymentVouchers?.length === 0) {
		return (
			<motion.div
				initial={{ opacity: 0 }}
				animate={{ opacity: 1, transition: { delay: 0.1 } }}
				className="flex flex-1 items-center justify-center h-full"
			>
				<Typography color="textSecondary" variant="h5">
					There are no paymentVoucher!
				</Typography>
			</motion.div>
		);
	}
	const role = localStorage.getItem('user_role').toLowerCase();

	return (
		<div className="w-full flex flex-col">
			<FuseScrollbars className="flex-grow overflow-x-auto">
				<PrintVoucher ref={printVoucherRef} title="Payment Voucher" type="payment" />
				<Table stickyHeader className="min-w-xl" aria-labelledby="tableTitle">
					<PaymentvouchersTableHead
						selectedPaymentVoucherIds={selected}
						order={order}
						onSelectAllClick={handleSelectAllClick}
						onRequestSort={handleRequestSort}
						rowCount={(!_.isEmpty(searchPaymentVoucher) ? searchPaymentVoucher : paymentVouchers).length}
						onMenuItemClick={handleDeselect}
						pagination={pageAndSize}
					/>

					<TableBody>
						{_.orderBy(
							searchText !== '' && !_.isEmpty(searchPaymentVoucher)
								? searchPaymentVoucher
								: paymentVouchers,
							[
								o => {
									switch (order.id) {
										case 'categories': {
											return o.categories[0];
										}
										default: {
											return o[order.id];
										}
									}
								}
							],
							[order.direction]
						).map(n => {
							const isSelected = selected.indexOf(n.id) !== -1;
							return (
								<TableRow
									className="h-52 cursor-pointer"
									hover
									role="checkbox"
									aria-checked={isSelected}
									tabIndex={-1}
									key={n.id}
									selected={isSelected}
								>
									<TableCell className="w-40 md:w-64" component="th" scope="row">
										{pageAndSize.page * pageAndSize.size - pageAndSize.size + serialNumber++}
									</TableCell>

									<TableCell className="p-4 md:p-16 whitespace-nowrap" component="th" scope="row">
										{n.payment_date && moment(new Date(n.payment_date)).format('DD-MM-YYYY')}{' '}
									</TableCell>
									<TableCell className="p-4 md:p-16" component="th" scope="row">
										{n.branch?.name}
									</TableCell>
									<TableCell className="p-4 md:p-16" component="th" scope="row">
										{n.invoice_no}
									</TableCell>

									<TableCell className="p-4 md:p-16" component="th" scope="row">
										{n?.related_ledgers?.toString()}
									</TableCell>

									<TableCell className="p-4 md:p-16" component="th" scope="row">
										{n.sub_ledger?.name}
									</TableCell>

									<TableCell className="p-4 md:p-16" component="th" scope="row">
										{`${n?.details ? n?.details + ',' : ''} ${n.ledger?.name || ''}`}
									</TableCell>

									<TableCell className="p-4 md:p-16" component="th" scope="row">
										{n.amount}
									</TableCell>

									<TableCell className="p-4 md:p-16" align="center" component="th" scope="row">
										<div className="flex flex-nowrap">
											<Tooltip title="Print" placement="top" enterDelay={300}>
												<PrintIcon
													className="cursor-pointer mr-3"
													style={{ color: 'blue' }}
													onClick={() => printVoucherRef.current.doPrint(n)}
												/>
											</Tooltip>
											{UserPermissions.includes(PAYMENT_VOUCHER_UPDATE) && (
												<Tooltip title="Edit" placement="top" enterDelay={300}>
													<EditIcon
														style={{
															color: 'green'
														}}
														onClick={() => handleUpdatePaymentVoucher(n)}
														className="cursor-pointer"
													/>
												</Tooltip>
											)}
											{UserPermissions.includes(PAYMENT_VOUCHER_DELETE) && (
												<Tooltip title="Delete" placement="top" enterDelay={300}>
													<DeleteIcon
														onClick={() => handleDeletePaymentVoucher(n, 'Delete')}
														className="cursor-pointer"
														style={{
															color: 'red'
														}}
													/>
												</Tooltip>
											)}
										</div>
									</TableCell>
								</TableRow>
							);
						})}
					</TableBody>
				</Table>
			</FuseScrollbars>

			<div className={classes.root} id="pagiContainer">
				<Pagination
					classes={{ ul: 'flex-nowrap' }}
					count={totalPages}
					page={page + 1}
					defaultPage={1}
					color="primary"
					showFirstButton
					showLastButton
					variant="outlined"
					shape="rounded"
					onChange={handlePagination}
				/>

				<TablePagination
					classes={{ root: 'overflow-visible' }}
					rowsPerPageOptions={rowsPerPageOptions}
					component="div"
					count={totalElements}
					rowsPerPage={rowsPerPage}
					page={page}
					className={classes.toolbar}
					backIconButtonProps={{
						'aria-label': 'Previous Page',
						className: 'py-0'
					}}
					nextIconButtonProps={{
						'aria-label': 'Next Page',
						className: 'py-0'
					}}
					onChangePage={handleChangePage}
					onChangeRowsPerPage={handleChangeRowsPerPage}
				/>
			</div>
		</div>
	);
};

export default withRouter(PaymentvouchersTable);
