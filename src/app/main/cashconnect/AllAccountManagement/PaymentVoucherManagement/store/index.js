import { combineReducers } from '@reduxjs/toolkit';
import paymentVoucher from './paymentvoucherSlice';
import paymentVouchers from './paymentvouchersSlice';

const reducer = combineReducers({
	paymentVoucher,
	paymentVouchers
});

export default reducer;
