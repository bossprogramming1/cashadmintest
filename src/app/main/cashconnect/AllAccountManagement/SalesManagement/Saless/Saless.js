import React, { useEffect } from 'react';
import FusePageCarded from '@fuse/core/FusePageCarded';
import withReducer from 'app/store/withReducer';
import { useDispatch, useSelector } from 'react-redux';
import { getUserPermissions } from 'app/store/dataSlice';
import { SALES_LIST } from 'app/constant/permission/permission';
import PagenotFound from 'app/main/cashconnect/Pagenotfound/PagenotFound';
import SalessHeader from './SalessHeader';
import SalessTable from './SalessTable';
import reducer from '../store/index';

const Saless = () => {
	const UserPermissions = useSelector(state => state.data.UserPermissions);
	useEffect(() => {
		dispatch(getUserPermissions());
	}, []);
	const dispatch = useDispatch();
	return (
		<FusePageCarded
			classes={{
				content: 'flex',
				contentCard: 'overflow-hidden',
				header: 'min-h-74 h-64'
			}}
			header={UserPermissions.includes(SALES_LIST) && <SalessHeader />}
			content={UserPermissions.includes(SALES_LIST) ? <SalessTable /> : <PagenotFound />}
			innerScroll
		/>
	);
};
export default withReducer('salessManagement', reducer)(Saless);
