import FuseLoading from '@fuse/core/FuseLoading';
import FuseScrollbars from '@fuse/core/FuseScrollbars';
import _ from '@lodash';
import Checkbox from '@material-ui/core/Checkbox';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TablePagination from '@material-ui/core/TablePagination';
import TableRow from '@material-ui/core/TableRow';
import DeleteIcon from '@material-ui/icons/Delete';
import PrintIcon from '@material-ui/icons/Print';
import EditIcon from '@material-ui/icons/Edit';
import { SEARCH_SALES } from 'app/constant/constants';
import { useEffect, useRef, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { withRouter } from 'react-router-dom';
import { Tooltip } from '@material-ui/core';
import PrintVoucher from '../../AccountComponents/PrintVoucher';
import { getSaless, selectSaless } from '../store/salessSlice';
import SalessTableHead from './SalessTableHead';

const SalessTable = props => {
	const dispatch = useDispatch();
	const saless = useSelector(selectSaless);
	const searchText = useSelector(({ salessManagement }) => salessManagement.saless.searchText);
	const [loading, setLoading] = useState(true);
	const [selected, setSelected] = useState([]);
	const [page, setPage] = useState(0);
	const [rowsPerPage, setRowsPerPage] = useState(10);
	const [order, setOrder] = useState({
		direction: 'asc',
		id: null
	});
	let serialNumber = 1;
	// For Print
	const printVoucherRef = useRef();
	const user_role = localStorage.getItem('user_role');

	useEffect(() => {
		dispatch(getSaless()).then(() => setLoading(false));
	}, [dispatch]);

	const [searchSales, setSearchSales] = useState([]);

	useEffect(() => {
		searchText !== '' ? getSearchSales() : setSearchSales([]);
	}, [searchText]);

	const getSearchSales = () => {
		fetch(`${SEARCH_SALES}?invoice_no=${searchText}`)
			.then(response => response.json())
			.then(searchedSalesData => {
				setSearchSales(searchedSalesData?.sales);
			})
			.catch(() => setSearchSales([]));
	};

	function handleRequestSort(_receiptvoucherEvent, property) {
		const id = property;
		let direction = 'desc';

		if (order.id === property && order.direction === 'desc') {
			direction = 'asc';
		}

		setOrder({
			direction,
			id
		});
	}

	function handleSelectAllClick(receiptvoucherEvent) {
		if (receiptvoucherEvent.target.checked) {
			setSelected(saless.map(n => n.id));
			return;
		}
		setSelected([]);
	}

	function handleDeselect() {
		setSelected([]);
	}

	function handleUpdateSales(item, salesEvent) {
		localStorage.removeItem('salesEvent');
		localStorage.setItem('salesEvent', salesEvent);
		props.history.push(`/apps/accounts/receivable_bills/${item.id}/${item.invoice_no}`);
	}
	function handleDeleteSales(item, salesEvent) {
		localStorage.removeItem('salesEvent');
		localStorage.setItem('salesEvent', salesEvent);
		props.history.push(`/apps/accounts/receivable_bills/${item.id}/${item.invoice_no}`);
	}

	function handleCheck(receiptvoucherEvent, id) {
		const selectedIndex = selected.indexOf(id);
		let newSelected = [];

		if (selectedIndex === -1) {
			newSelected = newSelected.concat(selected, id);
		} else if (selectedIndex === 0) {
			newSelected = newSelected.concat(selected.slice(1));
		} else if (selectedIndex === selected.length - 1) {
			newSelected = newSelected.concat(selected.slice(0, -1));
		} else if (selectedIndex > 0) {
			newSelected = newSelected.concat(selected.slice(0, selectedIndex), selected.slice(selectedIndex + 1));
		}

		setSelected(newSelected);
	}

	function handleChangePage(receiptvoucherEvent, value) {
		setPage(value);
	}

	function handleChangeRowsPerPage(receiptvoucherEvent) {
		setRowsPerPage(receiptvoucherEvent.target.value);
	}

	if (loading) {
		return <FuseLoading />;
	}

	return (
		<div className="w-full flex flex-col">
			<FuseScrollbars className="flex-grow overflow-x-auto">
				<PrintVoucher ref={printVoucherRef} title="Bill Receipt" type="sales" />

				<Table stickyHeader className="min-w-xl" aria-labelledby="tableTitle">
					<SalessTableHead
						selectedReceiptvoucherIds={selected}
						order={order}
						onSelectAllClick={handleSelectAllClick}
						onRequestSort={handleRequestSort}
						rowCount={saless.length}
						onMenuItemClick={handleDeselect}
					/>

					<TableBody>
						{_.orderBy(
							searchText !== '' && !_.isEmpty(searchSales) ? searchSales : saless,
							[
								o => {
									switch (order.id) {
										case 'categories': {
											return o.categories[0];
										}
										default: {
											return o[order.id];
										}
									}
								}
							],
							[order.direction]
						).map(n => {
							const isSelected = selected.indexOf(n.id) !== -1;
							return (
								<TableRow
									hover
									role="checkbox"
									aria-checked={isSelected}
									tabIndex={-1}
									key={n.id}
									selected={isSelected}
								>
									<TableCell className="whitespace-nowrap w-40 md:w-64 text-center" padding="none">
										<Checkbox
											checked={isSelected}
											onClick={receiptvoucherEvent => receiptvoucherEvent.stopPropagation()}
											onChange={receiptvoucherEvent => handleCheck(receiptvoucherEvent, n.id)}
										/>
									</TableCell>

									<TableCell className="whitespace-nowrap w-40 md:w-64" component="th" scope="row">
										{serialNumber++}
									</TableCell>

									<TableCell className="whitespace-nowrap p-4 md:p-16" component="th" scope="row">
										{n?.branch?.name}
									</TableCell>

									<TableCell className="whitespace-nowrap p-4 md:p-16" component="th" scope="row">
										{n?.invoice_no}
									</TableCell>

									<TableCell className="whitespace-nowrap p-4 md:p-16" component="th" scope="row">
										{n.related_ledgers?.toString()}
									</TableCell>

									<TableCell className="whitespace-nowrap p-4 md:p-16" component="th" scope="row">
										{n?.sub_ledger?.name}
									</TableCell>

									<TableCell className="whitespace-nowrap p-4 md:p-16" component="th" scope="row">
										{`${n.details || ''}, ${n.ledger?.name || ''}`}
									</TableCell>

									<TableCell className="whitespace-nowrap p-4 md:p-16" component="th" scope="row">
										{n?.amount}
									</TableCell>

									<TableCell
										whitespace-nowrap
										className="p-4 md:p-16"
										align="center"
										component="th"
										scope="row"
									>
										<div className="flex flex-nowrap">
											<Tooltip title="Print" placement="top" enterDelay={300}>
												<PrintIcon
													className="h-52 cursor-pointer mr-3"
													style={{ color: 'blue' }}
													onClick={() => printVoucherRef.current.doPrint(n)}
												/>
											</Tooltip>
											<Tooltip title="Edit" placement="top" enterDelay={300}>
												<EditIcon
													onClick={paymentvoucherEvent => handleUpdateSales(n, 'Update')}
													className="h-52 cursor-pointer"
													style={{ color: 'green' }}
												/>
											</Tooltip>
											<Tooltip title="Delete" placement="top" enterDelay={300}>
												<DeleteIcon
													onClick={event => handleDeleteSales(n, 'Delete')}
													className="h-52 cursor-pointer"
													style={{
														color: 'red',
														visibility:
															user_role === 'ADMIN' || user_role === 'admin'
																? 'visible'
																: 'hidden'
													}}
												/>
											</Tooltip>
										</div>
									</TableCell>
								</TableRow>
							);
						})}
					</TableBody>
				</Table>
			</FuseScrollbars>

			<TablePagination
				className="flex-shrink-0 border-t-1"
				rowsPerPageOptions={[5, 10, 25]}
				component="div"
				count={saless.length}
				rowsPerPage={rowsPerPage}
				page={page}
				backIconButtonProps={{
					'aria-label': 'Previous Page'
				}}
				nextIconButtonProps={{
					'aria-label': 'Next Page'
				}}
				onChangePage={handleChangePage}
				onChangeRowsPerPage={handleChangeRowsPerPage}
			/>
		</div>
	);
};

export default withRouter(SalessTable);
