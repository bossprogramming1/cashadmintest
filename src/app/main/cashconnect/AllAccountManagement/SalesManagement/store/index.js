import { combineReducers } from '@reduxjs/toolkit';
import sales from './salesSlice';
import saless from './salessSlice';

const reducer = combineReducers({
	sales,
	saless,
});

export default reducer;