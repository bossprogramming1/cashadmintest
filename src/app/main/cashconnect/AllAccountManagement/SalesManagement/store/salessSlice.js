import { createAsyncThunk, createEntityAdapter, createSlice } from '@reduxjs/toolkit';
import axios from 'axios';
import { DELETE_SALES, GET_ALLSALES } from '../../../../../constant/constants';

export const getSaless = createAsyncThunk('salessManagement/saless/getSaless', async () => {
	const authTOKEN = {
		headers: {
			'Content-type': 'application/json',
			Authorization: localStorage.getItem('jwt_access_token')
		}
	};

	const response = axios.get(GET_ALLSALES, authTOKEN);
	const data = await response;
	return data?.data?.sales;
});

export const removeSaless = createAsyncThunk(
	'salessManagement/saless/removeSaless',
	async (salesIds, { dispatch, getState }) => {
		const authTOKEN = {
			headers: {
				'Content-type': 'application/json',
				Authorization: localStorage.getItem('jwt_access_token')
			}
		};
		await axios.delete(`${DELETE_SALES}`, { salesIds }, authTOKEN);

		return salesIds;
	}
);

const salesssAdapter = createEntityAdapter({});

export const { selectAll: selectSaless, selectById: selectSalesById } = salesssAdapter.getSelectors(
	state => state.salessManagement.saless
);

const salessSlice = createSlice({
	name: 'salessManagement/salesss',
	initialState: salesssAdapter.getInitialState({
		searchText: ''
	}),
	reducers: {
		setSalessSearchText: {
			reducer: (state, action) => {
				state.searchText = action.payload;
			},
			prepare: event => ({ payload: event.target.value || '' })
		}
	},
	extraReducers: {
		[getSaless.fulfilled]: salesssAdapter.setAll
	}
});

export const { setData, setSalessSearchText } = salessSlice.actions;
export default salessSlice.reducer;
