import { createAsyncThunk, createSlice } from '@reduxjs/toolkit';
import axios from 'axios';
import {
	BRANCH_BY_USER_ID,
	CREATE_SALES,
	DELETE_SALES,
	GET_SALES_BY_INVOICE_NUMBER,
	UPDATE_SALES
} from '../../../../../constant/constants';

export const getSales = createAsyncThunk('salesManagement/sales/getSales', async (params, { rejectWithValue }) => {
	const authTOKEN = {
		headers: {
			'Content-type': 'application/json',
			Authorization: localStorage.getItem('jwt_access_token')
		}
	};

	try {
		const response = await axios.get(`${GET_SALES_BY_INVOICE_NUMBER}${params}`, authTOKEN);
		const data = await response.data;
		return data === undefined ? null : data;
	} catch (err) {
		return rejectWithValue(params);
	}
});

export const removeSales = createAsyncThunk(
	'salesManagement/sales/removeSales',
	async (salesInvoice, { dispatch, getState }) => {
		const authTOKEN = {
			headers: {
				'Content-type': 'application/json',
				Authorization: localStorage.getItem('jwt_access_token')
			}
		};

		await axios.delete(`${DELETE_SALES}${salesInvoice}`, authTOKEN);
	}
);

export const updateSales = createAsyncThunk(
	'salesManagement/sales/updateSales',
	async (salesData, { dispatch, getState }) => {
		const authTOKEN = {
			headers: {
				'Content-type': 'application/json',
				Authorization: localStorage.getItem('jwt_access_token')
			}
		};
		const response = await axios.put(`${UPDATE_SALES}`, salesData, authTOKEN);
	}
);

export const saveSales = createAsyncThunk(
	'salesManagement/sales/saveSales',
	async (salesData, { dispatch, getState }) => {
		const authTOKEN = {
			headers: {
				'Content-type': 'application/json',
				Authorization: localStorage.getItem('jwt_access_token')
			}
		};
		const response = await axios.post(`${CREATE_SALES}`, salesData, authTOKEN);
	}
);

export const setUserBasedBranch = createAsyncThunk('salesManagement/sales/setUserBasedBranch', async userId => {
	const authTOKEN = {
		headers: {
			'Content-type': 'application/json',
			Authorization: localStorage.getItem('jwt_access_token')
		}
	};
	const response = await axios.get(`${BRANCH_BY_USER_ID}${userId}`, authTOKEN);
	return response.data || {};
});

const salesSlice = createSlice({
	name: 'salesManagement/sales',
	initialState: null,
	reducers: {
		resetSales: () => null,
		newSales: {
			reducer: (state, action) => action.payload,
			prepare: event => ({
				payload: {
					branch: 0,
					sub_ledger: 0,
					sales_date: '',
					details: '',
					items: [
						{ ledger: 2, debit_amount: 0, credit_amount: 0 },
						{ ledger: 0, debit_amount: 0, credit_amount: 0 }
					]
				}
			})
		}
	},
	extraReducers: {
		[getSales.fulfilled]: (state, action) => action.payload,
		[saveSales.fulfilled]: (state, action) => {
			localStorage.setItem('salesAlert', 'saveSales');
			return action.payload;
		},
		[removeSales.fulfilled]: () => {
			localStorage.setItem('salesAlert', 'deleteSales');
		},
		[updateSales.fulfilled]: () => {
			localStorage.setItem('salesAlert', 'updateSales');
		},
		[setUserBasedBranch.fulfilled]: (state, action) => {
			return {
				...state,
				branch: action.payload
			};
		}
	}
});

export const { newSales, resetSales } = salesSlice.actions;
export default salesSlice.reducer;
