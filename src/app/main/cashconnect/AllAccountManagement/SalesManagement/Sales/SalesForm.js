import { Typography } from '@material-ui/core';
import Button from '@material-ui/core/Button';
import Grid from '@material-ui/core/Grid';
import Paper from '@material-ui/core/Paper';
import { makeStyles, styled, useTheme } from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import TextField from '@material-ui/core/TextField';
import AddIcon from '@material-ui/icons/Add';
import DeleteIcon from '@material-ui/icons/Delete';
import { Autocomplete } from '@material-ui/lab';
import { KeyboardDatePicker } from '@material-ui/pickers';
import React, { useContext, useEffect, useState } from 'react';
import { Controller, useFormContext } from 'react-hook-form';
import { useDispatch, useSelector } from 'react-redux';
import { useParams } from 'react-router-dom';
import { getBranches, getLedgers, getSubLedgers } from 'app/store/dataSlice';
import SalesItemContext from './SalesItemContext';

const useStyles = makeStyles(theme => ({
	hidden: {
		display: 'none'
	},
	tblContainer: {
		borderRadius: '0px'
	},
	table: {
		minWidth: 600
	},
	tableHead: {
		backgroundColor: theme.palette.primary[500]
	},
	dateOfSaleContainer: {
		display: 'flex',
		flexWrap: 'nowrap',
		justifyContent: 'space-between',
		minWidth: '250px'
	},
	dateOfSale: {
		width: '45px',
		background: theme.palette.background.paper,
		'& > div': {
			'&:before': {
				borderBottom: '0px solid black !important'
			}
		},
		'& input': {
			boxSizing: 'inherit',
			marginLeft: '-11px',
			marginTop: '2px'
		}
	},
	paytypeLabel: {
		'&& > label': {
			fontSize: '16px'
		}
	},
	paidAmount: {
		'&& > p': {
			marginTop: '-27px'
		}
	},
	modal: {
		display: 'flex',
		alignItems: 'center',
		justifyContent: 'center'
	},
	paper: {
		backgroundColor: theme.palette.background.paper,
		border: `2px solid ${theme.palette.primary[800]}`,
		boxShadow: theme.shadows[5],
		padding: theme.spacing(2, 4, 3)
	}
}));

function SalesForm(props) {
	const [salesTemporaryItem, setSalesTemporaryItem] = useState([
		{ ledger: 0, debit_amount: 0, credit_amount: 0 },
		{ ledger: 0, debit_amount: 0, credit_amount: 0 }
	]);
	const [receiptVoucherItems, setReceiptVoucherItems] = useContext(SalesItemContext);
	const dispatch = useDispatch();
	const userID = localStorage.getItem('UserID');
	const classes = useStyles(props);
	const methods = useFormContext();
	const { control, reset, watch, getValues, formState, setError } = methods;
	const formStat = getValues();
	const sales = useSelector(({ salessManagement }) => salessManagement.sales);
	const { errors, dirtyFields } = formState;
	const routeParams = useParams();
	const { salesId } = routeParams;
	const user_role = localStorage.getItem('user_role');
	const [productListRow, setProductListRow] = useState([]);
	let serialNumber = 1;
	const Theme = useTheme();
	const PrimaryBg = Theme.palette.primary[500];
	const PrimaryDark = Theme.palette.primary[700];
	const PrimaryLight = Theme.palette.primary.light;
	const PaperColor = Theme.palette.background.paper;
	const primaryBlack = Theme.palette.primary.dark;
	const cellStyle = { border: `1px solid ${Theme.palette.grey[600]}`, fontWeight: 'bold', color: primaryBlack };
	const [itm, setItm] = useState(true);
	const [addItm, setAddItm] = useState('');
	const [debitAndCreditMatched, setDebitAndCreditMatched] = useState(false);
	const [debitCreditErrorMessage, setDebitCreditErrorMessage] = useState('');
	const [ledgerError, setLedgerError] = useState('');
	const branches = useSelector(state => state.data.branches);
	const ledgers = useSelector(state => state.data.ledgers);
	const subLedgers = useSelector(state => state.data.subLedgers);
	useEffect(() => {
		dispatch(getBranches());
		dispatch(getLedgers());
		dispatch(getSubLedgers());
	}, []);

	//handleAddReceiptVoucherRow
	const handleAddReceiptVoucherRow = () => {
		const currentReceiptVoucherItems = receiptVoucherItems;
		const salesTemporaryItemCopy = salesTemporaryItem;
		currentReceiptVoucherItems.push({ ledger: 0, debit_amount: 0, credit_amount: 0 });
		salesTemporaryItemCopy.push({ ledger: 0, debit_amount: 0, credit_amount: 0 });
		setSalesTemporaryItem(salesTemporaryItemCopy);
		setReceiptVoucherItems(currentReceiptVoucherItems);
		setAddItm('Item Added');
		setItm(!itm);
	};

	//handleDeleteItem
	const handleDeleteItem = id => {
		const allReceiptVoucherItems = [...receiptVoucherItems];
		allReceiptVoucherItems.splice(id, 1);
		setReceiptVoucherItems(allReceiptVoucherItems);

		receiptVoucherItems.map((item, idx) => {
			if (idx >= id) {
				reset({
					...formStat,
					[`ledger${idx}`]: [`ledger${idx + 1}`],
					[`debit_amount${idx}`]: [`debit_amount${idx + 1}`],
					[`credit_amount${idx}`]: [`credit_amount${idx + 1}`]
				});
				if (receiptVoucherItems.length - 1 === idx) {
					reset({
						...formStat,
						[`ledger${idx}`]: '',
						[`debit_amount${idx}`]: '',
						[`credit_amount${idx}`]: ''
					});
				}
			}
			return null;
		});
	};

	useEffect(() => {}, [receiptVoucherItems]);

	useEffect(() => {
		CalculateDebitAndCredit();
	}, [receiptVoucherItems]);

	//Calculate debit & credit
	const CalculateDebitAndCredit = () => {
		const resultDebit = receiptVoucherItems.reduce(
			(totalDr, receiptVoucherItm) => totalDr + parseInt(receiptVoucherItm.debit_amount, 10),
			0
		);
		const resultCredit = receiptVoucherItems.reduce(
			(totalCr, receiptVoucherItm) => totalCr + parseInt(receiptVoucherItm.credit_amount, 10),
			0
		);
		const debitAmountPlusCreditAmount = resultDebit + resultCredit;

		if (debitAmountPlusCreditAmount === 0 || Number.isNaN(debitAmountPlusCreditAmount)) {
			setDebitCreditErrorMessage('');
			setDebitAndCreditMatched(false);
			setItm(!itm);
		} else if (resultDebit !== resultCredit) {
			setDebitCreditErrorMessage("Sorry, Debit and Credit doesn't match...");
			setDebitAndCreditMatched(false);
			setItm(!itm);
		} else {
			setDebitCreditErrorMessage('Congratulations, Debit & Credit match...');
			setDebitAndCreditMatched(true);
			localStorage.setItem('DebitCreditMatched', true);
			setItm(!itm);
		}
	};

	//handleOnChange
	const handleOnChange = (idx, event, dropdownEventName, newValueId) => {
		const result = 0;
		const newItems = receiptVoucherItems.map((receiptVoucherItem, sidx) => {
			if (idx !== sidx) return receiptVoucherItems;

			const newItem = { ...receiptVoucherItem };
			if (dropdownEventName === `ledger${idx}`) {
				newItem.ledger = newValueId;
				receiptVoucherItems[sidx] = newItem;
				setReceiptVoucherItems(receiptVoucherItems);
				const { ledger } = newItem;
				setLedgerError('');

				receiptVoucherItems.map((data, i) => {
					if (idx === i && data.ledger !== 0) {
						localStorage.removeItem('ledgerError');
						setLedgerError('');
					}
					return null;
				});

				if (ledger === undefined || ledger === '') {
					setLedgerError('Account type is required');
					localStorage.setItem('ledgerError', true);
					setItm(!itm);
				}
			} else {
				receiptVoucherItems.map((data, i) => {
					if (idx === i && data.ledger === 0) {
						localStorage.setItem('ledgerError', true);
						setLedgerError('Account type is required');
						setItm(!itm);
					}
					return null;
				});

				if (isNaN(event.target.value)) {
					if (event.target?.name === `debit_amount${idx}`) {
						reset({
							...formStat,
							[`debit_amount${idx}`]: formStat?.[`debit_amount${idx}`] || ''
						});
					} else if (event.target?.name === `credit_amount${idx}`) {
						reset({
							...formStat,
							[`credit_amount${idx}`]: formStat?.[`credit_amount${idx}`] || ''
						});
					}
				} else {
					localStorage.removeItem('DebitCreditMatched');
					const ledger = watch(`ledger${idx}`);
					if (event.target?.name === `debit_amount${idx}` && !isNaN(event.target.value)) {
						const debit = event.target.value;
						if (debit === '') {
							newItem.debit_amount = 0;
						} else {
							if (debit?.slice(-1) == '.') {
								newItem.debit_amount = debit;
							} else {
								newItem.debit_amount = parseFloat(debit);
							}
							const companySales = ledgers.find(led => led?.name === 'Company Sales');
							const copyReceiptVoucherItem = { ...receiptVoucherItems[0] };
							copyReceiptVoucherItem.ledger = companySales?.id;
							receiptVoucherItems[0] = copyReceiptVoucherItem;
							setReceiptVoucherItems(receiptVoucherItems);
							const salesTemporaryItemCopy = { ...salesTemporaryItem[idx] };
							salesTemporaryItemCopy.debit_amount = parseFloat(debit);
							salesTemporaryItemCopy.credit_amount = 0;
							salesTemporaryItem[idx] = salesTemporaryItemCopy;
							setSalesTemporaryItem(salesTemporaryItem);
						}
						const debitAmount = newItem.debit_amount;

						if (ledger === undefined || ledger === '') {
							setLedgerError('Account type is required');
							localStorage.setItem('ledgerError', true);
							setItm(!itm);
						}

						if (debitAmount !== undefined && debitAmount !== 0) {
							const previousReceiptVoucherItem = { ...receiptVoucherItems[0] };
							const resultSalesDebit = salesTemporaryItem.reduce(
								(totalDr, salesItem) => totalDr + parseInt(salesItem.debit_amount, 10),
								0
							);
							previousReceiptVoucherItem.credit_amount = resultSalesDebit;
							previousReceiptVoucherItem.debit_amount = 0;
							receiptVoucherItems[0] = previousReceiptVoucherItem;
							setReceiptVoucherItems(receiptVoucherItems);
							reset({
								...formStat,
								[`credit_amount${idx}`]: 0,
								[`debit_amount${idx}`]: debitAmount,
								credit_amount0: previousReceiptVoucherItem.credit_amount,
								debit_amount0: 0
							});
						}
						if (debitAmount === 0) {
							newItem.credit_amount = 0;
							reset({
								...formStat,
								[`credit_amount${idx}`]: '',
								[`debit_amount${idx}`]: debitAmount
							});
						}
						receiptVoucherItems[sidx] = newItem;
						setReceiptVoucherItems(receiptVoucherItems);
					}
					if (event.target?.name === `credit_amount${idx}` && !isNaN(event.target.value)) {
						const credit = event.target.value;
						if (credit === '') {
							newItem.credit_amount = 0;
						} else if (credit?.slice(-1) == '.') {
							newItem.credit_amount = credit;
						} else {
							newItem.credit_amount = parseFloat(credit);
						}
						const creditAmount = newItem.credit_amount;

						if (ledger === undefined || ledger === '') {
							setLedgerError('Account type is required');
							localStorage.setItem('ledgerError', true);
							setItm(!itm);
						}

						if (creditAmount !== undefined && creditAmount !== 0) {
							newItem.debit_amount = 0;
							reset({
								...formStat,
								[`debit_amount${idx}`]: 0,
								[`credit_amount${idx}`]: creditAmount
							});
						}
						if (creditAmount === 0) {
							newItem.debit_amount = 0;
							reset({
								...formStat,
								[`debit_amount${idx}`]: '',
								[`credit_amount${idx}`]: creditAmount
							});
						}
						receiptVoucherItems[sidx] = newItem;
						setReceiptVoucherItems(receiptVoucherItems);
					}
				}
				CalculateDebitAndCredit();
			}

			const resultDebit = receiptVoucherItems.reduce(
				(totalDr, receiptVoucherItm) => totalDr + parseInt(receiptVoucherItm.debit_amount, 10),
				0
			);
			const resultCredit = receiptVoucherItems.reduce(
				(totalCr, receiptVoucherItm) => totalCr + parseInt(receiptVoucherItm.credit_amount, 10),
				0
			);
			const debitAmountPlusCreditAmount = resultDebit + resultCredit;
			if (debitAmountPlusCreditAmount !== 0 && resultDebit === resultCredit) {
				receiptVoucherItems.map((data, i) => {
					if (data.ledger === 0) {
						setLedgerError('Account type is required');
						localStorage.setItem('ledgerError', true);
						setItm(!itm);
					}
					return null;
				});
			}

			return null;
		});
		return null;
	};

	return (
		<div>
			<Controller
				name="branch"
				control={control}
				render={({ field: { onChange, value, name } }) => (
					<Autocomplete
						className="mt-8 mb-16"
						freeSolo
						value={value ? branches.find(data => data.id === value) : null}
						options={branches}
						disabled={true}
						getOptionLabel={option => `${option?.name}`}
						onChange={(event, newValue) => {
							onChange(newValue?.id);
						}}
						renderInput={params => (
							<TextField
								{...params}
								placeholder="Select Branch"
								label="Branch"
								error={!!errors.branch}
								required
								disabled={true}
								helperText={errors?.branch?.message}
								variant="outlined"
								autoFocus
								InputLabelProps={{
									shrink: true
								}}
							/>
						)}
					/>
				)}
			/>
			<Controller
				name="sub_ledger"
				control={control}
				render={({ field: { onChange, value, name } }) => (
					<Autocomplete
						className="mt-8 mb-16"
						freeSolo
						value={value ? subLedgers.find(subLedger => subLedger.id === value) : null}
						options={subLedgers}
						getOptionLabel={option => `${option?.name}`}
						onChange={(event, newValue) => {
							onChange(newValue?.id);
						}}
						renderInput={params => (
							<TextField
								{...params}
								placeholder="Select Sub Ledger"
								label="Sub Ledger"
								error={!!errors.sub_ledger}
								required
								helperText={errors?.sub_ledger?.message}
								variant="outlined"
								autoFocus
								InputLabelProps={{
									shrink: true
								}}
							/>
						)}
					/>
				)}
			/>
			<Controller
				name="sales_date"
				control={control}
				render={({ field }) => (
					<KeyboardDatePicker
						{...field}
						autoOk
						className="w-full mt-8 mb-16"
						variant="inline"
						inputVariant="outlined"
						label="label"
						format="dd/MM/yyyy"
						error={!!errors.sales_date}
						helperText={errors.sales_date?.message || ''}
						InputAdornmentProps={{ position: 'start' }}
					/>
				)}
			/>
			<Controller
				name="details"
				control={control}
				render={({ field }) => (
					<TextField
						{...field}
						className="mt-8 mb-16"
						label="Details"
						placeholder="Write details.."
						error={!!errors.details}
						helperText={errors?.details?.message}
						multiline
						InputLabelProps={{
							shrink: true
						}}
						rows="6"
						variant="outlined"
						fullWidth
					/>
				)}
			/>
			<br />
			<Grid xs={12}>
				<Div h="fit-content" pd="2" minHeight="272px" alignItems="flex-start" border>
					<TableContainer component={Paper} className={classes.tblContainer}>
						<Table className={classes.table} aria-label="simple table">
							<TableHead className={classes.tableHead}>
								<TableRow>
									<TableCell style={{ color: PaperColor }}>No.</TableCell>
									<TableCell style={{ color: PaperColor }} align="center">
										Account Type
									</TableCell>
									<TableCell style={{ color: PaperColor }} align="center">
										Dr
									</TableCell>
									<TableCell style={{ color: PaperColor }} align="center">
										Cr
									</TableCell>
									<TableCell style={{ color: PaperColor }} align="center">
										Action
									</TableCell>
								</TableRow>
							</TableHead>

							<TableBody>
								{receiptVoucherItems.map((item, idx) => {
									return (
										<>
											{idx !== 0 && (
												<TableRow>
													<TableCell
														whitespace-nowrap
														style={cellStyle}
														component="th"
														scope="row"
													>
														{serialNumber++}
													</TableCell>
													<TableCell style={cellStyle}>
														<Controller
															name={`ledger${idx}`}
															control={control}
															render={({ field: { onChange, value } }) => (
																<Autocomplete
																	className="mt-8 mb-16"
																	freeSolo
																	disabled={idx === 0}
																	value={
																		value
																			? ledgers.find(
																					ledger => ledger.id === value
																			  )
																			: null
																	}
																	options={ledgers}
																	//defaultValue={ledgers[1]}
																	getOptionLabel={option => `${option?.name}`}
																	InputLabelProps={{ shrink: true }}
																	//onChange={(event, newValue) => handleOnChange(idx, event, `ledger${idx}`, newValue?.id)}
																	onChange={(event, newValue) => {
																		onChange(newValue?.id);
																		handleOnChange(
																			idx,
																			event,
																			`ledger${idx}`,
																			newValue?.id
																		);
																	}}
																	//value={employee && employee.branch}
																	//defaultValue={{ id: null, name: "Select a branch" }}
																	renderInput={params => (
																		<TextField
																			{...params}
																			//placeholder="Select a account"
																			label="Account"
																			error={!!errors.ledger}
																			// required
																			helperText={errors?.ledger?.message}
																			variant="outlined"
																			autoFocus
																			InputLabelProps={{
																				shrink: true
																			}}
																		/>
																	)}
																/>
															)}
														/>
													</TableCell>
													<TableCell style={cellStyle}>
														<Controller
															name={`debit_amount${idx}`}
															control={control}
															render={({ field }) => {
																return (
																	<TextField
																		{...field}
																		disabled={idx === 0}
																		className="mt-8 mb-16"
																		error={!!errors.debit_amount}
																		helperText={errors?.debit_amount?.message}
																		onChange={event => handleOnChange(idx, event)}
																		label="Debit"
																		type="text"
																		id="debit"
																		required
																		variant="outlined"
																		InputLabelProps={{ shrink: true }}
																		fullWidth
																	/>
																);
															}}
														/>
													</TableCell>
													<TableCell style={cellStyle}>
														<Controller
															name={`credit_amount${idx}`}
															control={control}
															render={({ field }) => {
																return (
																	<TextField
																		{...field}
																		disabled
																		className="mt-8 mb-16"
																		error={!!errors.credit_amount}
																		helperText={errors?.credit_amount?.message}
																		onChange={event => handleOnChange(idx, event)}
																		label="Credit"
																		type="text"
																		id="credit"
																		required
																		variant="outlined"
																		InputLabelProps={{ shrink: true }}
																		fullWidth
																	/>
																);
															}}
														/>
													</TableCell>
													{idx === 1 && (
														<TableCell
															className="p-0 md:p-0"
															align="center"
															component="th"
															scope="row"
															style={{ minWidth: '80px' }}
														>
															<div>
																<Btn
																	h="50px"
																	bg="white"
																	size="small"
																	style={{
																		backgroundColor: '#00002e',
																		color: 'white'
																	}}
																	onClick={handleAddReceiptVoucherRow}
																>
																	<AddIcon />
																</Btn>
															</div>
														</TableCell>
													)}
													{idx !== 0 && idx !== 1 && (
														<TableCell
															className="p-0 md:p-0"
															align="center"
															component="th"
															scope="row"
															style={{ minWidth: '80px' }}
														>
															<div>
																{/* <EditIcon
                                                                //onClick={orderEvent => handleUpdatePurchaseFinal(n)}
                                                                className="h-52 cursor-pointer"
                                                                style={{ color: 'green' }}
                                                            />{' '} */}
																<DeleteIcon
																	onClick={event => handleDeleteItem(idx)}
																	className="h-52 cursor-pointer"
																	style={{
																		color: 'red',
																		visibility:
																			user_role === 'ADMIN' ||
																			user_role === 'admin'
																				? 'visible'
																				: 'hidden'
																	}}
																/>
															</div>
														</TableCell>
													)}
												</TableRow>
											)}
										</>
									);
								})}
							</TableBody>
						</Table>
					</TableContainer>
				</Div>
				<div style={{ display: 'flex', justifyContent: 'space-between' }}>
					{ledgerError && <Typography style={{ color: 'red' }}>{ledgerError}</Typography>}
					{debitCreditErrorMessage && (
						<Typography style={{ color: debitAndCreditMatched ? 'green' : 'red' }}>
							{debitCreditErrorMessage}
						</Typography>
					)}
				</div>
			</Grid>
		</div>
	);
}

export default SalesForm;

const btn = styled(({ color, ...other }) => <Button variant="outlined" {...other} />)({});
const Btn = styled(btn)(props => ({
	borderRadius: '3px',
	fontWeight: 'bold',
	borderWidth: '2px',
	height: props.h ? props.h : '40px',
	background: props.bg
}));

const Div = styled('div')(props => ({
	background: props.bg,
	height: props.h === '1' ? '25px' : props.h === '2' ? '35px' : props.h === 3 ? '50px' : props.h && props.h,
	width: props.w ? props.w : '100%',
	display: 'flex',
	justifyContent: props.justCont ? props.justCont : 'space-between',
	alignItems: props.alignItems ? props.alignItems : 'center',
	padding: props.pd === '1' ? '10px' : props.pd === '2' ? '20px' : props.pd && props.pd,
	flexWrap: props.wrap && 'wrap',
	border: props.border && '2px solid gray',
	borderTop: `4px solid ${props.top}`,
	borderLeft: `4px solid ${props.left}`,
	minHeight: props.minHeight
}));
