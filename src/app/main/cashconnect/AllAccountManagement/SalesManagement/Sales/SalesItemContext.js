import { createContext } from 'react';

const SalesItemContext = createContext({});

export default SalesItemContext;