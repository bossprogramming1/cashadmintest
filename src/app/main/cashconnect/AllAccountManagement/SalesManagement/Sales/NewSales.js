import FusePageCarded from '@fuse/core/FusePageCarded';
import { useDeepCompareEffect } from '@fuse/hooks';
import { yupResolver } from '@hookform/resolvers/yup';
import useUserInfo from 'app/@customHooks/useUserInfo';
import { SALES_CREATE, SALES_DELETE, SALES_DETAILS, SALES_UPDATE } from 'app/constant/permission/permission';
import PagenotFound from 'app/main/cashconnect/Pagenotfound/PagenotFound';
import { getUserPermissions } from 'app/store/dataSlice';
import withReducer from 'app/store/withReducer';
import { useEffect, useState } from 'react';
import { FormProvider, useForm } from 'react-hook-form';
import { useDispatch, useSelector } from 'react-redux';
import { useParams } from 'react-router-dom';
import * as yup from 'yup';
import reducer from '../store/index';
import { getSales, newSales, resetSales, setUserBasedBranch } from '../store/salesSlice';
import NewSalesHeader from './NewSalesHeader';
import SalesForm from './SalesForm';
import SalesItemContext from './SalesItemContext';

/**
 * Form Validation Schema
 */
const schema = yup.object().shape({
	branch: yup.number().required('Branch is required'),
	sub_ledger: yup.number().required('Payment type is required'),
	receipt_date: yup.date().required('Payment date is required')
});

const NewSales = () => {
	const [receiptVoucherItems, setReceiptVoucherItems] = useState([
		{ ledger: 0, debit_amount: 0, credit_amount: 0 },
		{ ledger: 0, debit_amount: 0, credit_amount: 0 }
	]);
	const dispatch = useDispatch();
	const sales = useSelector(({ salessManagement }) => salessManagement.sales);
	const [noReceiptvoucher, setNoReceiptvoucher] = useState(false);
	const methods = useForm({
		mode: 'onChange',
		defaultValues: {},
		resolver: yupResolver(schema)
	});
	const routeParams = useParams();
	const UserPermissions = useSelector(state => state.data.UserPermissions);
	useEffect(() => {
		dispatch(getUserPermissions());
	}, []);
	const { reset, watch, control, onChange, formState, getValues } = methods;
	const form = watch();
	const formStatee = getValues();

	const { userId } = useUserInfo();
	useDeepCompareEffect(() => {
		function updateReceiptvoucherState() {
			const { salesId, salesName } = routeParams;
			if (salesId === 'new') {
				localStorage.removeItem('salesEvent');
				/**
				 * Create New User data
				 */
				dispatch(newSales());
				dispatch(setUserBasedBranch(userId));
			} else {
				/**
				 * Get User data
				 */
				dispatch(getSales(salesName)).then(action => {
					/**
					 * If the requested product is not exist show message
					 */
					if (!action.payload) {
						setNoReceiptvoucher(true);
					}
				});
			}
		}

		updateReceiptvoucherState();
	}, [dispatch, routeParams]);

	//setFormState
	const setFormState = () => {
		const updateReceiptVoucherData = {
			branch: sales?.branch?.id,
			sub_ledger: sales?.sub_ledger?.id,
			sales_date: sales.sales_date,
			details: sales.details
		};
		sales?.items?.map((item, idx) => {
			if (idx === 0 || idx === 1) {
				receiptVoucherItems[idx].id = item.id;
				receiptVoucherItems[idx].invoice_no = item.invoice_no;
				receiptVoucherItems[idx].ledger = item?.ledger?.id;
				receiptVoucherItems[idx].debit_amount = item?.debit_amount;
				receiptVoucherItems[idx].credit_amount = item?.credit_amount;

				updateReceiptVoucherData[`ledger${idx}`] = item.ledger?.id;
				updateReceiptVoucherData[`debit_amount${idx}`] = item?.debit_amount;
				updateReceiptVoucherData[`credit_amount${idx}`] = item?.credit_amount;
			} else {
				const prevItems = receiptVoucherItems;
				prevItems.push({ id: 0, ledger: 0, debit_amount: 0, credit_amount: 0 });
				setReceiptVoucherItems(prevItems); //items.concat([{ item_name: "", item_price: 0, item_quantity: 0 }])

				receiptVoucherItems[idx].id = item.id;
				receiptVoucherItems[idx].invoice_no = item.invoice_no;
				receiptVoucherItems[idx].ledger = item.ledger?.id;
				receiptVoucherItems[idx].debit_amount = item.debit_amount;
				receiptVoucherItems[idx].credit_amount = item.credit_amount;

				updateReceiptVoucherData[`ledger${idx}`] = item.ledger?.id;
				updateReceiptVoucherData[`debit_amount${idx}`] = item?.debit_amount;
				updateReceiptVoucherData[`credit_amount${idx}`] = item?.credit_amount;
			}
			return null;
		});
		return updateReceiptVoucherData;
	};

	useEffect(() => {
		if (!sales) {
			return;
		}
		/**
		 * Reset the form on receiptvoucher state changes
		 */
		const receiptVoucherData = setFormState();
		reset(receiptVoucherData);
	}, [sales, reset]);

	useEffect(() => {
		return () => {
			/**
			 * Reset Receiptvoucher on component unload
			 */
			dispatch(resetSales());
			setNoReceiptvoucher(false);
		};
	}, [dispatch]);

	return (
		<SalesItemContext.Provider value={[receiptVoucherItems, setReceiptVoucherItems]}>
			<FormProvider {...methods}>
				{UserPermissions.includes(SALES_CREATE) ||
				UserPermissions.includes(SALES_UPDATE) ||
				UserPermissions.includes(SALES_DELETE) ||
				UserPermissions.includes(SALES_DETAILS) ? (
					<FusePageCarded
						classes={{
							content: 'flex',
							contentCard: 'overflow-hidden',
							header: 'min-h-74 h-64'
						}}
						header={<NewSalesHeader />}
						content={
							<div className="p-16 sm:p-24" style={{ width: '100%' }}>
								<SalesForm />
							</div>
						}
						innerScroll
					/>
				) : (
					<PagenotFound />
				)}
			</FormProvider>
		</SalesItemContext.Provider>
	);
};
export default withReducer('salessManagement', reducer)(NewSales);
