import FusePageCarded from '@fuse/core/FusePageCarded';
import { useDeepCompareEffect } from '@fuse/hooks';
import { yupResolver } from '@hookform/resolvers/yup';
import { Button, Typography } from '@material-ui/core';
import setIdIfValueIsObject2 from 'app/@helpers/setIdIfValueIsObject2';
import withReducer from 'app/store/withReducer';
import { motion } from 'framer-motion';
import React, { useEffect, useState } from 'react';
import { FormProvider, useForm } from 'react-hook-form';
import { useDispatch, useSelector } from 'react-redux';
import { Link, useParams } from 'react-router-dom';
import * as yup from 'yup';
import { getUserPermissions } from 'app/store/dataSlice';
import PagenotFound from 'app/main/cashconnect/Pagenotfound/PagenotFound';
import {
	SUBLEDGER_ACCOUNT_CREATE,
	SUBLEDGER_ACCOUNT_DELETE,
	SUBLEDGER_ACCOUNT_DETAILS,
	SUBLEDGER_ACCOUNT_UPDATE
} from 'app/constant/permission/permission';
import reducer from '../store/index';
import { getSubLedger, newSubLedger, resetSubLedger } from '../store/subLedgerSlice';
import SubLedgerForm from './SubLedgerForm';
import NewSubLedgerHeader from './NewSubLedgerHeader';

/**
 * Form Validation Schema
 */
const schema = yup.object().shape({
	name: yup.string().required('Name is required')
});

const SubLedger = () => {
	const dispatch = useDispatch();
	const subledger = useSelector(({ subledgersManagement }) => subledgersManagement.subledger);

	const [noSubLedger, setNoSubLedger] = useState(false);
	const methods = useForm({
		mode: 'onChange',
		defaultValues: {},
		resolver: yupResolver(schema)
	});
	const routeParams = useParams();
	const UserPermissions = useSelector(state => state.data.UserPermissions);
	useEffect(() => {
		dispatch(getUserPermissions());
	}, []);
	const { reset } = methods;

	useDeepCompareEffect(() => {
		function updateSubLedgerState() {
			const { subledgerId } = routeParams;

			if (subledgerId === 'new') {
				localStorage.removeItem('event');
				/**
				 * Create New User data
				 */
				dispatch(newSubLedger());
			} else {
				/**
				 * Get User data
				 */

				dispatch(getSubLedger(subledgerId)).then(action => {
					/**
					 * If the requested product is not exist show message
					 */
					if (!action.payload) {
						setNoSubLedger(true);
					}
				});
			}
		}

		updateSubLedgerState();
	}, [dispatch, routeParams]);

	useEffect(() => {}, []);

	useEffect(() => {
		if (!subledger) {
			return;
		}
		/**
		 * Reset the form on subledger state changes
		 */
		reset(setIdIfValueIsObject2(subledger));
	}, [subledger, reset]);

	useEffect(() => {
		return () => {
			/**
			 * Reset SubLedger on component unload
			 */
			dispatch(resetSubLedger());
			setNoSubLedger(false);
		};
	}, [dispatch]);

	/**
	 * Show Message if the requested products is not exists
	 */
	if (noSubLedger) {
		return (
			<motion.div
				initial={{ opacity: 0 }}
				animate={{ opacity: 1, transition: { delay: 0.1 } }}
				className="flex flex-col flex-1 items-center justify-center h-full"
			>
				<Typography color="textSecondary" variant="h5">
					There is no such subledger!
				</Typography>
				<Button
					className="mt-24"
					component={Link}
					variant="outlined"
					to="/apps/e-commerce/products"
					color="inherit"
				>
					Go to SubLedger Page
				</Button>
			</motion.div>
		);
	}

	return (
		<FormProvider {...methods}>
			{UserPermissions.includes(SUBLEDGER_ACCOUNT_CREATE) ||
			UserPermissions.includes(SUBLEDGER_ACCOUNT_UPDATE) ||
			UserPermissions.includes(SUBLEDGER_ACCOUNT_DELETE) ||
			UserPermissions.includes(SUBLEDGER_ACCOUNT_DETAILS) ? (
				<FusePageCarded
					classes={{
						toolbar: 'p-0',
						header: 'min-h-74 h-64'
					}}
					header={<NewSubLedgerHeader />}
					content={
						<div className="p-16 sm:p-24">
							<SubLedgerForm />
						</div>
					}
					innerScroll
				/>
			) : (
				<PagenotFound />
			)}
		</FormProvider>
	);
};
export default withReducer('subledgersManagement', reducer)(SubLedger);
