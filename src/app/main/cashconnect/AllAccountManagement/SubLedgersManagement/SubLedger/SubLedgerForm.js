import TextField from '@material-ui/core/TextField';
import { Autocomplete } from '@material-ui/lab';
import React, { useEffect } from 'react';
import { Controller, useFormContext } from 'react-hook-form';
import { useDispatch, useSelector } from 'react-redux';
import { getGroups } from '../../../../../store/dataSlice';

function SubLedgerForm(props) {
	const methods = useFormContext();
	const { control, formState } = methods;
	const { errors } = formState;
	const dispatch = useDispatch();

	const groups = useSelector(state => state.data.groups);

	useEffect(() => {
		dispatch(getGroups());
	}, []);

	return (
		<div>
			<Controller
				name="name"
				control={control}
				render={({ field }) => {
					return (
						<TextField
							{...field}
							value={field.value || ''}
							className="mt-8 mb-16"
							error={!!errors?.name}
							helperText={errors?.name?.message}
							label="Name"
							id="name"
							variant="outlined"
							InputLabelProps={field.value && { shrink: true }}
							fullWidth
							required
							autoFocus
						/>
					);
				}}
			/>
		</div>
	);
}

export default SubLedgerForm;
