import FusePageCarded from '@fuse/core/FusePageCarded';
import { SUBLEDGER_ACCOUNT_LIST } from 'app/constant/permission/permission';
import PagenotFound from 'app/main/cashconnect/Pagenotfound/PagenotFound';
import { getUserPermissions } from 'app/store/dataSlice';
import withReducer from 'app/store/withReducer';
import React, { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import reducer from '../store/index';
import SubLedgersHeader from './SubLedgersHeader';
import SubLedgersTable from './SubLedgersTable';

const SubLedgers = () => {
	const UserPermissions = useSelector(state => state.data.UserPermissions);
	useEffect(() => {
		dispatch(getUserPermissions());
	}, []);
	const dispatch = useDispatch();
	return (
		<FusePageCarded
			classes={{
				content: 'flex',
				contentCard: 'overflow-hidden',
				header: 'min-h-74 h-64'
			}}
			header={UserPermissions.includes(SUBLEDGER_ACCOUNT_LIST) && <SubLedgersHeader />}
			content={UserPermissions.includes(SUBLEDGER_ACCOUNT_LIST) ? <SubLedgersTable /> : <PagenotFound />}
			innerScroll
		/>
	);
};
export default withReducer('subledgersManagement', reducer)(SubLedgers);
