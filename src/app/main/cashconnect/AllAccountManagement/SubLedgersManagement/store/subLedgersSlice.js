import { createAsyncThunk, createEntityAdapter, createSlice } from '@reduxjs/toolkit';
import axios from 'axios';
import { DELETE_SUBLEDGER, GET_SUBLEDGERS } from '../../../../../constant/constants';

export const getSubLedgers = createAsyncThunk('subledgerManagement/subledgers/getSubLedgers', async pageAndSize => {
	axios.defaults.headers.common['Content-type'] = 'application/json';
	axios.defaults.headers.common.Authorization = localStorage.getItem('jwt_access_token');

	const response = axios.get(GET_SUBLEDGERS, { params: pageAndSize });
	const data = await response;

	sessionStorage.setItem('total_subledgers_elements', data.data.total_elements);
	sessionStorage.setItem('total_subledgers_pages', data.data.total_pages);
	delete axios.defaults.headers.common['Content-type'];
	delete axios.defaults.headers.common.Authorization;

	return data.data.sub_ledger_accounts;
});

export const removeSubLedgers = createAsyncThunk(
	'subledgerManagement/subledgers/removeSubLedgers',
	async subledgerIds => {
		const headers = {
			'Content-type': 'application/json',
			Authorization: localStorage.getItem('jwt_access_token')
		};
		const data = {
			ids: subledgerIds
		};
		const response = await axios.delete(`${DELETE_SUBLEDGER}`, { headers, data });

		return response;
	}
);

const subledgersAdapter = createEntityAdapter({});

export const { selectAll: selectSubLedgers, selectById: selectSubLedgerById } = subledgersAdapter.getSelectors(
	state => state.subledgersManagement.subledgers
);

const subledgersSlice = createSlice({
	name: 'subledgerManagement/subledgers',
	initialState: subledgersAdapter.getInitialState({
		searchText: ''
	}),
	reducers: {
		setSubLedgersSearchText: {
			reducer: (state, action) => {
				state.searchText = action.payload;
			},
			prepare: event => ({ payload: event.target.value || '' })
		}
	},
	extraReducers: {
		[getSubLedgers.fulfilled]: subledgersAdapter.setAll
	}
});

export const { setData, setSubLedgersSearchText } = subledgersSlice.actions;
export default subledgersSlice.reducer;
