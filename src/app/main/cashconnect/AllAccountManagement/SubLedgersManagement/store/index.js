import { combineReducers } from '@reduxjs/toolkit';
import subledger from './subLedgerSlice';
import subledgers from './subLedgersSlice';

const reducer = combineReducers({
	subledger,
	subledgers
});

export default reducer;
