import { createAsyncThunk, createSlice } from '@reduxjs/toolkit';
import axios from 'axios';
import {
	CREATE_SUBLEDGER,
	DELETE_SUBLEDGER,
	GET_SUBLEDGER_BY_ID,
	UPDATE_SUBLEDGER
} from '../../../../../constant/constants';

export const getSubLedger = createAsyncThunk(
	'subledgerManagement/subledger/getSubLedger',
	async (params, { rejectWithValue }) => {
		const authTOKEN = {
			headers: {
				'Content-type': 'application/json',
				Authorization: localStorage.getItem('jwt_access_token')
			}
		};

		try {
			const response = await axios.get(`${GET_SUBLEDGER_BY_ID}${params}`, authTOKEN);
			const data = await response.data;
			return data === undefined ? null : data;
		} catch (err) {
			return rejectWithValue(params);
		}
	}
);

export const removeSubLedger = createAsyncThunk('subledgerManagement/subledger/removeSubLedger', async val => {
	const authTOKEN = {
		headers: {
			'Content-type': 'application/json',
			Authorization: localStorage.getItem('jwt_access_token')
		}
	};

	const subledgerId = val.id;
	const response = await axios.delete(`${DELETE_SUBLEDGER}${subledgerId}`, authTOKEN);
	return response;
});

export const updateSubLedger = createAsyncThunk(
	'subledgerManagement/subledger/updateSubLedger',
	async (subledgerData, { dispatch, getState }) => {
		const { subledger } = getState().subledgersManagement;

		const authTOKEN = {
			headers: {
				'Content-type': 'application/json',
				Authorization: localStorage.getItem('jwt_access_token')
			}
		};
		const response = await axios.put(`${UPDATE_SUBLEDGER}${subledger.id}`, subledgerData, authTOKEN);
		return response;
	}
);

export const saveSubLedger = createAsyncThunk('subledgerManagement/subledger/saveSubLedger', async subledgerData => {
	const authTOKEN = {
		headers: {
			'Content-type': 'application/json',
			Authorization: localStorage.getItem('jwt_access_token')
		}
	};
	const response = await axios.post(`${CREATE_SUBLEDGER}`, subledgerData, authTOKEN);
	return response;
});

const subledgerSlice = createSlice({
	name: 'subledgerManagement/subledger',
	initialState: null,
	reducers: {
		resetSubLedger: () => null,
		newSubLedger: {
			reducer: (_state, action) => action.payload,
			prepare: () => ({
				payload: {}
			})
		}
	},
	extraReducers: {
		[getSubLedger.fulfilled]: (_state, action) => action.payload,
		[saveSubLedger.fulfilled]: (_state, action) => action.payload,
		[removeSubLedger.fulfilled]: (_state, action) => action.payload,
		[updateSubLedger.fulfilled]: (_state, action) => action.payload
	}
});

export const { newSubLedger, resetSubLedger } = subledgerSlice.actions;

export default subledgerSlice.reducer;
