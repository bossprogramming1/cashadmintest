import FuseUtils from '@fuse/utils/FuseUtils';
import axios from 'axios';
import jwtDecode from 'jwt-decode';
/* eslint-disable camelcase */
import { LOGIN_URL } from '../../constant/constants';

class JwtService extends FuseUtils.EventEmitter {
	init() {
		this.setInterceptors();
		this.handleAuthentication();
	}

	setInterceptors = () => {
		axios.interceptors.response.use(
			response => {
				return response;
			},
			err => {
				return new Promise((resolve, reject) => {
					// if (err.response?.status === 401 && err.config && !err.config.__isRetryRequest) {

					if (err.response?.status === 401) {
						// if you ever get an unauthorized response, logout the user
						this.emit('onAutoLogout', 'User Not Found');
						this.setSession(null);
					}
					// if (err.response?.status === 403) {
					// 	this.emit('onAutoLogout', 'Phone Number is not verifed');
					// 	this.setSession(null);
					// }
					throw err;
				});
			}
		);
	};

	handleAuthentication = () => {
		const access_token = this.getAccessToken();

		if (!access_token) {
			this.emit('onNoAccessToken');

			return;
		}

		if (this.isAuthTokenValid(access_token)) {
			this.setSession(access_token);
			this.emit('onAutoLogin', true);
		} else {
			this.setSession(null);
			this.emit('onAutoLogout', 'access_token expired');
		}
	};

	createUser = data => {
		return new Promise((resolve, reject) => {
			axios.post('/api/auth/register', data).then(response => {
				if (response.data.user) {
					this.setSession(response.data.access_token);
					resolve(response.data.user);
				} else {
					reject(response.data.error);
				}
			});
		});
	};

	signInWithEmailAndPassword = (username, password) => {
		return new Promise((resolve, reject) => {
			axios
				.post(`${LOGIN_URL}`, { username, password })
				.then(response => {
					// if (response) {
					// this.setSession();
					localStorage.setItem('jwt_access_token', `Bearer ${response.data.access}`);
					axios.defaults.headers.common.Authorization = `Bearer ${response.data.access}`;

					const user = {
						primary_phone: response.data.primary_phone,
						username: response.data.username,
						email: response.data.email,
						displayName: response.data.username,
						photoURL: response.data.image,
						role: response.data.role?.name,
						id: response.data.id
					};
					this.setSession(localStorage.setItem('jwt_access_token', `Bearer ${response.data.access}`));

					// localStorage.setItem("jwt_access_token", response.data.access)
					localStorage.setItem('user_id', response.data.id);
					// localStorage.setItem("user_email", response.data.email)
					// localStorage.setItem("user_name", response.data.username)
					localStorage.setItem('user_role', `${response.data.role ? response.data.role.name : 'user'}`);
					// localStorage.setItem("user_image", `${BASE_URL}${response.data.image}`)
					window.dispatchEvent(new CustomEvent('storage', { detail: { name: 'login_event' } }));

					resolve(user);

					// } else {
					// 	reject(response);
					// }
				})
				.catch(rer => {
					if (rer.response?.status == 403) {
						localStorage.setItem('loginError', 'loginError');
					} else {
						reject(rer);
					}
				});
		});
	};

	signInWithToken = () => {
		return new Promise((resolve, reject) => {
			axios
				.get('/api/auth/access-token', {
					data: {
						access_token: this.getAccessToken()
					}
				})
				.then(response => {
					if (response.data.user) {
						this.setSession(response.data.access_token);
						resolve(response.data.user);
					} else {
						this.logout();
						reject(new Error('Failed to login with token..'));
					}
				})
				.catch(error => {
					this.logout();
					reject(new Error('Failed to login with token.'));
				});
		});
	};

	updateUserData = user => {
		return axios.post('/api/auth/user/update', {
			user
		});
	};

	setSession = access_token => {
		if (access_token) {
			localStorage.setItem('jwt_access_token', access_token);
			axios.defaults.headers.common.Authorization = `Bearer ${access_token}`;
		} else {
			delete axios.defaults.headers.common.Authorization;
		}
	};

	logout = () => {
		this.setSession(null);
	};

	isAuthTokenValid = access_token => {
		if (!access_token) {
			return false;
		}
		const decoded = jwtDecode(access_token);
		const currentTime = Date.now() / 1000;
		if (decoded.exp < currentTime) {
			console.warn('access token expired');
			return false;
		}

		return true;
	};

	getAccessToken = () => {
		return window.localStorage.getItem('jwt_access_token');
	};
}

const instance = new JwtService();

export default instance;
