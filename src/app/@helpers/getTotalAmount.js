/* eslint-disable no-prototype-builtins */
const getTotalAmount = (listArray = [], sumKey = '') => {
	let totalAmount = 0;

	if (Array.isArray(listArray)) {
		totalAmount = listArray.reduce((total, valueObj) => {
			// Check if the valueObj has a valid sumKey before adding it to the total
			if (valueObj.hasOwnProperty(sumKey) && valueObj[sumKey] !== null && valueObj[sumKey] !== '') {
				return total + Number(valueObj[sumKey]);
			}
			return total;
		}, 0);
	}

	return totalAmount;
};

export default getTotalAmount;
