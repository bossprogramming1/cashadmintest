//http://api.cashconnectbd.com
//http://192.168.0.8:8002

const isProduction = process.env.NODE_ENV === 'production';

export const BASE_URL = isProduction ? 'https://api.cashconnect.com.bd' : 'http://192.168.0.151:8010';

export const WS_URL = isProduction ? 'api.cashconnect.com.bd' : 'ws://192.168.0.151:8010';

// // //base url
// export const BASE_URL = 'http://192.168.0.151:8000';
// export const WS_URL = 'ws://192.168.0.151:8000';
// export const BASE_URL = 'https://api.cashconnect.com.bd';
// export const WS_URL = 'api.cashconnect.com.bd';

//login
export const LOGIN_URL = `${BASE_URL}/user/api/v1/user/login/`;

//user
export const USER_BY_ID = `${BASE_URL}/user/api/v1/user/me/`;

export const RESET_PASSWORD = `${BASE_URL}/auth/user/reset_password/`;

export const CONFIRM_RESET_PASSWORD = `${BASE_URL}/auth/user/reset_password_confirm/`;

// user
export const ALL_USERS = `${BASE_URL}/user/api/v1/user/all/`;

export const GET_USERS_WITHOUT_PAGINATION = `${BASE_URL}/user/api/v1/user/without_pagination/all/`;

export const SEARCH_USER = `${BASE_URL}/user/api/v1/user/search/`;

export const CHECK_EMAIL = `${BASE_URL}/user/api/v1/user/check_email_when_create/`;

export const CHECK_PRIMARY_PHONE = `${BASE_URL}/user/api/v1/user/check_primary_phone_when_create/`;

export const CHECK_SECONDARY_PHONE = `${BASE_URL}/user/api/v1/user/check_secondary_phone_when_create/`;

export const CHECK_USERNAME = `${BASE_URL}/user/api/v1/user/check_username_when_create/`;

// check_update
export const CHECK_EMAIL_UPDATE = `${BASE_URL}/user/api/v1/user/check_email_when_update/`;

export const CHECK_PRIMARY_PHONE_UPDATE = `${BASE_URL}/user/api/v1/user/check_primary_phone_when_update/`;

export const CHECK_SECONDARY_PHONE_UPDATE = `${BASE_URL}/user/api/v1/user/check_secondary_phone_when_update/`;

export const CHECK_USERNAME_UPDATE = `${BASE_URL}/user/api/v1/user/check_username_when_update/`;

// discount

export const CREATE_DISCOUNT = `${BASE_URL}/discount/api/v1/discount/create/`;

export const GET_DISCOUNTID = `${BASE_URL}/discount/api/v1/discount/`;

export const GET_DISCOUNTS = `${BASE_URL}/discount/api/v1/discount/all/`;

export const UPDATE_DISCOUNT = `${BASE_URL}/discount/api/v1/discount/update/`;

export const DELETE_DISCOUNT = `${BASE_URL}/discount/api/v1/discount/delete/`;

export const SEARCH_DISCOUNT = `${BASE_URL}/discount/api/v1/discount/search/`;

// branch
export const BRANCHES = `${BASE_URL}/branch/api/v1/branch/all/`;

// thana
export const THANAS = `${BASE_URL}/thana/api/v1/thana/all/`;

export const THANAS_BASED_CITY = `${BASE_URL}/thana/api/v1/thana/all_thana_by_city_id/`;

// // cities
// export const CITIES = `${BASE_URL}/city/api/v1/city/all/`;

//  country
export const COUNTRIES = `${BASE_URL}/country/api/v1/country/all/`;

export const GET_COUNTRIES_WITHOUT_PAGINATION = `${BASE_URL}/country/api/v1/country/without_pagination/all/`;

// role
export const ROLES = `${BASE_URL}/role/api/v1/role/all/`;

// department
export const DEPARTMENTS = `${BASE_URL}/department/api/v1/department/all/`;

// user
export const CREATE_USER = `${BASE_URL}/user/api/v1/user/create/`;

// employees
export const EMPLOYEES = `${BASE_URL}/employee/api/v1/employee/all/`;

export const GET_EMPLOYEES_WITHOUT_PAGINATION = `${BASE_URL}/employee/api/v1/employee/without_paginaiton/all/`;

export const BRANCH_BY_USER_ID = `${BASE_URL}/branch/api/v1/branch/get_a_branch_by_user_id/`;

// citys
export const CITIES = `${BASE_URL}/city/api/v1/city/all/`;

// slidersettings
export const CREATE_SLIDERSETTING = `${BASE_URL}/homepage_slider/api/v1/homepage_slider/create/`;

export const GET_SLIDERSETTINGID = `${BASE_URL}/homepage_slider/api/v1/homepage_slider/`;

export const GET_SLIDERSETTINGS = `${BASE_URL}/homepage_slider/api/v1/homepage_slider/all/`;

export const UPDATE_SLIDERSETTING = `${BASE_URL}/homepage_slider/api/v1/homepage_slider/update/`;

export const DELETE_SLIDERSETTING = `${BASE_URL}/homepage_slider/api/v1/homepage_slider/delete/`;

// customer types
export const CREATE_CUSTOMERTYPE = `${BASE_URL}/customer_type/api/v1/customer_type/create/`;

export const GET_CUSTOMERTYPEID = `${BASE_URL}/customer_type/api/v1/customer_type/`;

export const GET_CUSTOMERTYPES = `${BASE_URL}/customer_type/api/v1/customer_type/all/`;

export const UPDATE_CUSTOMERTYPE = `${BASE_URL}/customer_type/api/v1/customer_type/update/`;

export const DELETE_CUSTOMERTYPE = `${BASE_URL}/customer_type/api/v1/customer_type/delete/`;

// users
export const USERS = `${BASE_URL}/user/api/v1/user/all/`;

//  export const ALL_USERS = `${BASE_URL}/users/api/v1/users/all/`;
export const USERS_PASSWORDCHANGE = `${BASE_URL}/user/api/v1/user/passwordchange/`;

//group
export const GROUPS_WITHOUT_PAGINATION = `${BASE_URL}/group/api/v1/group/without_pagination/all/`;

//primary group
export const PRIMARY_GROUPS_WITHOUT_PAGINATION = `${BASE_URL}/primary_group/api/v1/primary_group/all/`;

//designation
export const DESIGNATIONS_WITHOUT_PAGINATION = `${BASE_URL}/designation/api/v1/designation/without_pagination/all/`;

//  employee
export const CREATE_EMPLOYEE = `${BASE_URL}/employee/api/v1/employee/create/`;

export const GET_EMPLOYEES = `${BASE_URL}/employee/api/v1/employee/all/`;

export const GET_EMPLOYEE_BY_ID = `${BASE_URL}/employee/api/v1/employee/`;

export const UPDATE_EMPLOYEE = `${BASE_URL}/employee/api/v1/employee/update/`;

export const DELETE_EMPLOYEE = `${BASE_URL}/employee/api/v1/employee/delete/`;

export const SEARCH_EMPLOYEE = `${BASE_URL}/employee/api/v1/employee/search/`;

//  departments
export const CREATE_DEPARTMENT = `${BASE_URL}/department/api/v1/department/create/`;

export const GET_DEPARTMENTS = `${BASE_URL}/department/api/v1/department/all/`;

export const GET_DEPARTMENTS_WITHOUT_PAGINATION = `${BASE_URL}/department/api/v1/department/without_pagination/all/`;

export const GET_DEPARTMENTID = `${BASE_URL}/department/api/v1/department/`;

export const UPDATE_DEPARTMENT = `${BASE_URL}/department/api/v1/department/update/`;

export const DELETE_DEPARTMENT = `${BASE_URL}/department/api/v1/department/delete/`;

export const SEARCH_DEPARTMENT = `${BASE_URL}/department/api/v1/department/search/`;

//  purchasereturns
export const CREATE_PURCHASERETURN = `${BASE_URL}/purchase_item_return/api/v1/purchase_return_item/create/`;

export const GET_PURCHASERETURNS = `${BASE_URL}/purchase_item_return/api/v1/purchase_return_item/all/`;

export const GET_PURCHASERETURNS_WITHOUT_PAGINATION = `${BASE_URL}/purchase_item_return/api/v1/purchase_return_item/without_pagination/all/`;

export const GET_PURCHASERETURNID = `${BASE_URL}/purchase_item_return/api/v1/purchase_return_item/`;

export const UPDATE_PURCHASERETURN = `${BASE_URL}/purchase_item_return/api/v1/purchase_return_item/update/`;

export const DELETE_PURCHASERETURN = `${BASE_URL}/purchase_item_return/api/v1/purchase_return_item/delete/`;

export const SEARCH_PURCHASERETURN = `${BASE_URL}/purchase_item_return/api/v1/purchase_return_item/search/`;

//  sellreturns
export const CREATE_SELLRETURN = `${BASE_URL}/orderitem_return/api/v1/orderitem_return/create/`;

export const GET_SELLRETURNS = `${BASE_URL}/orderitem_return/api/v1/orderitem_return/all/`;

export const GET_SELLRETURNS_WITHOUT_PAGINATION = `${BASE_URL}/orderitem_return/api/v1/orderitem_return/without_pagination/all/`;

export const GET_SELLRETURNID = `${BASE_URL}/orderitem_return/api/v1/orderitem_return/`;

export const UPDATE_SELLRETURN = `${BASE_URL}/orderitem_return/api/v1/orderitem_return/update/`;

export const DELETE_SELLRETURN = `${BASE_URL}/orderitem_return/api/v1/orderitem_return/delete/`;

export const SEARCH_SELLRETURN = `${BASE_URL}/orderitem_return/api/v1/orderitem_return/search/`;

export const GET_SELL_FINALS_ITMES_BY_INVOICE = `${BASE_URL}/order/api/v1/order/get_order_by_invoice_no/`;

//  permission
export const CREATE_PERMISSION = `${BASE_URL}/permission/api/v1/permission/create/`;

export const GET_PERMISSIONS = `${BASE_URL}/permission/api/v1/permission/all/`;

export const GET_PERMISSIONS_WITHOUT_PAGINATION = `${BASE_URL}/permission/api/v1/permission/without_pagination/all/`;

export const GET_PERMISSIONID = `${BASE_URL}/permission/api/v1/permission/`;

export const UPDATE_PERMISSION = `${BASE_URL}/permission/api/v1/permission/update/`;

export const DELETE_PERMISSION = `${BASE_URL}/permission/api/v1/permission/delete/`;

export const SEARCH_PERMISSION = `${BASE_URL}/permission/api/v1/permission/search/`;

//  brands
export const CREATE_BRAND = `${BASE_URL}/brand/api/v1/brand/create/`;

export const GET_BRANDS = `${BASE_URL}/brand/api/v1/brand/all/`;

export const GET_BRANDID = `${BASE_URL}/brand/api/v1/brand/`;

export const GET_BRANDS_WITHOUT_PAGINATION = `${BASE_URL}/brand/api/v1/brand/without_pagination/all/`;

export const UPDATE_BRAND = `${BASE_URL}/brand/api/v1/brand/update/`;

export const DELETE_BRAND = `${BASE_URL}/brand/api/v1/brand/delete/`;

export const SEARCH_BRAND = `${BASE_URL}/brand/api/v1/brand/search/`;

//  qualification
export const CREATE_QUALIFICATION = `${BASE_URL}/qualification/api/v1/qualification/create/`;

export const GET_QUALIFICATIONS = `${BASE_URL}/qualification/api/v1/qualification/all/`;

export const GET_QUALIFICATIONID = `${BASE_URL}/qualification/api/v1/qualification/`;

export const UPDATE_QUALIFICATION = `${BASE_URL}/qualification/api/v1/qualification/update/`;

export const DELETE_QUALIFICATION = `${BASE_URL}/qualification/api/v1/qualification/delete/`;

export const SEARCH_QUALIFICATON = `${BASE_URL}/qualification/api/v1/qualification/search/`;

//  city
export const CREATE_CITY = `${BASE_URL}/city/api/v1/city/create/`;

export const GET_CITYS = `${BASE_URL}/city/api/v1/city/all/`;

export const GET_CITYS_WITHOUT_PAGINATION = `${BASE_URL}/city/api/v1/city/without_pagination/all/`;

export const GET_CITYID = `${BASE_URL}/city/api/v1/city/`;

export const UPDATE_CITY = `${BASE_URL}/city/api/v1/city/update/`;

export const DELETE_CITY = `${BASE_URL}/city/api/v1/city/delete/`;

export const SEARCH_CITY = `${BASE_URL}/city/api/v1/city/search/`;

//  color
export const CREATE_COLOR = `${BASE_URL}/color/api/v1/color/create/`;

export const GET_COLORS = `${BASE_URL}/color/api/v1/color/all/`;

export const GET_COLORS_WITHOUT_PAGINATION = `${BASE_URL}/color/api/v1/color/without_pagination/all/`;

export const GET_COLORID = `${BASE_URL}/color/api/v1/color/`;

export const GET_COLOR_PRODUCT_ID = `${BASE_URL}/color/api/v1/color/get_by_product_id/`;

export const UPDATE_COLOR = `${BASE_URL}/color/api/v1/color/update/`;

export const DELETE_COLOR = `${BASE_URL}/color/api/v1/color/delete/`;

export const SEARCH_COLOR = `${BASE_URL}/color/api/v1/color/search/`;

//  size
export const CREATE_SIZE = `${BASE_URL}/size/api/v1/size/create/`;

export const GET_SIZES = `${BASE_URL}/size/api/v1/size/all/`;

export const GET_SIZES_WITHOUT_PAGINATION = `${BASE_URL}/size/api/v1/size/without_pagination/all/`;

export const GET_SIZEID = `${BASE_URL}/size/api/v1/size/`;

export const UPDATE_SIZE = `${BASE_URL}/size/api/v1/size/update/`;

export const DELETE_SIZE = `${BASE_URL}/size/api/v1/size/delete/`;

export const SEARCH_SIZE = `${BASE_URL}/size/api/v1/size/search/`;

//  thana
export const CREATE_THANA = `${BASE_URL}/thana/api/v1/thana/create/`;

export const GET_THANAS = `${BASE_URL}/thana/api/v1/thana/all/`;

export const GET_THANAS_WITHOUT_PAGINATION = `${BASE_URL}/thana/api/v1/thana/without_pagination/all/`;

export const GET_THANAID = `${BASE_URL}/thana/api/v1/thana/`;

export const UPDATE_THANA = `${BASE_URL}/thana/api/v1/thana/update/`;

export const DELETE_THANA = `${BASE_URL}/thana/api/v1/thana/delete/`;

export const SEARCH_THANA = `${BASE_URL}/thana/api/v1/thana/search/`;

//  role
export const CREATE_ROLE = `${BASE_URL}/role/api/v1/role/create/`;

export const GET_ROLES = `${BASE_URL}/role/api/v1/role/all/`;

export const GET_ROLES_WITHOUT_PAGINATION = `${BASE_URL}/role/api/v1/role/without_pagination/all/`;

export const GET_ROLE = `${BASE_URL}/role/api/v1/role/`;

export const UPDATE_ROLE = `${BASE_URL}/role/api/v1/role/update/`;

export const DELETE_ROLE = `${BASE_URL}/role/api/v1/role/delete/`;

export const SEARCH_ROLE = `${BASE_URL}/role/api/v1/role/search/`;

//payroll
//  timetable
export const CREATE_TIMETABLE = `${BASE_URL}/shift_timetable/api/v1/shift_timetable/create/`;

export const GET_TIMETABLES = `${BASE_URL}/shift_timetable/api/v1/shift_timetable/all/`;

export const GET_TIMETABLES_WITHOUT_PAGINATION = `${BASE_URL}/shift_timetable/api/v1/shift_timetable_wp/all/`;

export const GET_TIMETABLE = `${BASE_URL}/shift_timetable/api/v1/shift_timetable/`;

export const GET_TIMETABLE_BY_SHIFT_ID = `${BASE_URL}/shift/api/v1/get_shift_day_timetable_by_shift_id/`;

export const CHECK_COLOR_CODE = `${BASE_URL}/shift_timetable/api/v1/check_shift_timetable_color/`;

export const UPDATE_TIMETABLE = `${BASE_URL}/shift_timetable/api/v1/shift_timetable/update/`;

export const DELETE_TIMETABLE = `${BASE_URL}/shift_timetable/api/v1/shift_timetable/delete/`;

export const SEARCH_TIMETABLE = `${BASE_URL}/shift_timetable/api/v1/shift_timetable/search/`;

//  shift
export const CREATE_SHIFT = `${BASE_URL}/shift/api/v1/shift/create/`;

export const GET_SHIFTS = `${BASE_URL}/shift/api/v1/shift/all/`;

export const GET_SHIFTS_WITHOUT_PAGINATION = `${BASE_URL}/shift/api/v1/shift/all/`;

export const GET_SHIFT = `${BASE_URL}/shift/api/v1/shift/`;

export const UPDATE_SHIFT = `${BASE_URL}/shift/api/v1/shift/update/`;

export const DELETE_SHIFT = `${BASE_URL}/shift/api/v1/shift/delete/`;

export const SEARCH_SHIFT = `${BASE_URL}/shift/api/v1/shift/search/`;

//  schedule
export const CREATE_SCHEDULE = `${BASE_URL}/employee_schedule/api/v1/employee_schedule/create/`;

export const GET_SCHEDULES = `${BASE_URL}/employee_schedule/api/v1/employee_schedule/all/`;

export const GET_SCHEDULES_WITHOUT_PAGINATION = `${BASE_URL}/employee_schedule/api/v1/employee_schedule/all/`;

export const GET_SCHEDULE = `${BASE_URL}/employee_schedule/api/v1/employee_schedule/`;

export const GET_EMPLOYEE_SCHEDULE_BY_DEPT_ID = `${BASE_URL}/employee_schedule/api/v1/employee_schedule_by_department_id/`;

export const GET_EMPLOYEE_BY_DEPT_ID = `${BASE_URL}/employee/api/v1/employee/get_all_by_department_id/`;

export const GET_EMPLOYEE_TIMETABLE_BY_EMP_ID_SHIFT_ID = `${BASE_URL}/shift_daytime/api/v1/get_shift_daytime_timetable_by_employeeid_shiftid/`;

export const UPDATE_SCHEDULE = `${BASE_URL}/employee_schedule/api/v1/employee_schedule/update/`;

export const DELETE_SCHEDULE = `${BASE_URL}/employee_schedule/api/v1/employee_schedule/delete/`;

export const SEARCH_SCHEDULE = `${BASE_URL}/schedule/api/v1/schedule/search/`;

//  shift_daytime
export const CREATE_SHIFT_DAYTIME = `${BASE_URL}/shift_daytime/api/v1/shift_daytime/create/`;

export const GET_SHIFT_DAYTIMES = `${BASE_URL}/shift_daytime/api/v1/shift_daytime/all/`;

export const GET_SHIFT_DAYTIME = `${BASE_URL}/shift_daytime/api/v1/shift_daytime/`;

export const GET_SHIFT_DAYTIME_BY_SHIFTID = `${BASE_URL}/shift_daytime/api/v1/get_shift_daytime_by_shift_id/`;

export const UPDATE_SHIFT_DAYTIME = `${BASE_URL}/shift_daytime/api/v1/shift_daytime/update/`;

export const DELETE_SHIFT_DAYTIME = `${BASE_URL}/shift_daytime/api/v1/shift_daytime/delete/`;

//  Device
export const CREATE_DEVICE_IP = `${BASE_URL}/device_ip/api/v1/device_ip/create/`;

export const GET_DEVICE_IPS = `${BASE_URL}/device_ip/api/v1/device_ip/all/`;

export const GET_DEVICE_IPS_WITHOUT_PAGINATION = `${BASE_URL}/device_ip/api/v1/device_ip_wp/all/`;

export const GET_DEVICE_IPID = `${BASE_URL}/device_ip/api/v1/device_ip/`;

export const UPDATE_DEVICE_IP = `${BASE_URL}/device_ip/api/v1/device_ip/update/`;

export const DELETE_DEVICE_IP = `${BASE_URL}/device_ip/api/v1/device_ip/delete/`;

export const SEARCH_DEVICE_IP = `${BASE_URL}/device_ip/api/v1/device_ip/search/`;

//  Unit
export const CREATE_UNIT = `${BASE_URL}/unit/api/v1/unit/create/`;

export const GET_UNITS = `${BASE_URL}/unit/api/v1/unit/all/`;

export const GET_UNITS_WITHOUT_PAGINATION = `${BASE_URL}/unit/api/v1/unit_wp/all/`;

export const GET_UNITID = `${BASE_URL}/unit/api/v1/unit/`;

export const UPDATE_UNIT = `${BASE_URL}/unit/api/v1/unit/update/`;

export const DELETE_UNIT = `${BASE_URL}/unit/api/v1/unit/delete/`;

export const SEARCH_UNIT = `${BASE_URL}/unit/api/v1/unit/search/`;

//  Userdefinevalue
export const CREATE_USERDEFINEVALUE = `${BASE_URL}/user_defined_value/api/v1/user_defined_value/create/`;

export const GET_USERDEFINEVALUES = `${BASE_URL}/user_defined_value/api/v1/user_defined_value/all/`;

export const GET_USERDEFINEVALUES_WITHOUT_PAGINATION = `${BASE_URL}/user_defined_value/api/v1/user_defined_value/all/`;

export const GET_PAYHEAD_ONLY_USERDEFINEVALUES = `${BASE_URL}/user_defined_value/api/v1/user_defined_value/payhead/all/`;

export const GET_USERDEFINEVALUEID = `${BASE_URL}/user_defined_value/api/v1/user_defined_value/`;

export const UPDATE_USERDEFINEVALUE = `${BASE_URL}/user_defined_value/api/v1/user_defined_value/update/`;

export const DELETE_USERDEFINEVALUE = `${BASE_URL}/user_defined_value/api/v1/user_defined_value/delete/`;

export const SEARCH_USERDEFINEVALUE = `${BASE_URL}/user_defined_value/api/v1/user_defined_value/search/`;

export const CHECK_USERDEFINEVALUE_BY_EMPLOYEE_PAYHEAD = `${BASE_URL}/user_defined_value/api/v1/user_defined_value/check_if_value_exists_for_employee/`;

//  Computation_information
export const CREATE_COMPUTATION_INFORMATION = `${BASE_URL}/computation_information/api/v1/computation_information/create/`;

export const GET_COMPUTATION_INFORMATIONS = `${BASE_URL}/computation_information/api/v1/computation_information/all/`;

export const GET_COMPUTATION_INFORMATIONS_WITHOUT_PAGINATION = `${BASE_URL}/computation_information/api/v1/computation_information/without_pagination/all/`;

export const GET_COMPUTATION_INFORMATIONID = `${BASE_URL}/computation_information/api/v1/computation_information/`;

export const UPDATE_COMPUTATION_INFORMATION = `${BASE_URL}/computation_information/api/v1/computation_information/update/`;

export const DELETE_COMPUTATION_INFORMATION = `${BASE_URL}/computation_information/api/v1/computation_information/delete/`;

export const SEARCH_COMPUTATION_INFORMATION = `${BASE_URL}/computation_information/api/v1/computation_information/search/`;

//  Computation_information_value
export const CREATE_COMPUTATION_INFORMATION_VALUE = `${BASE_URL}/computation_information_value/api/v1/computation_information_value/create/`;

export const GET_COMPUTATION_INFORMATION_VALUES = `${BASE_URL}/computation_information_value/api/v1/computation_information_value/all/`;

export const GET_COMPUTATION_INFORMATION_VALUES_WITHOUT_PAGINATION = `${BASE_URL}/computation_information_value/api/v1/computation_information_value/without_pagination/all/`;

export const GET_COMPUTATION_INFORMATION_VALUEID = `${BASE_URL}/computation_information_value/api/v1/computation_information_value/`;

export const UPDATE_COMPUTATION_INFORMATION_VALUE = `${BASE_URL}/computation_information_value/api/v1/computation_information_value/update/`;

export const DELETE_COMPUTATION_INFORMATION_VALUE = `${BASE_URL}/computation_information_value/api/v1/computation_information_value/delete/`;

export const SEARCH_COMPUTATION_INFORMATION_VALUE = `${BASE_URL}/computation_information_value/api/v1/computation_information_value/search/`;

//  Compute
export const CREATE_COMPUTE = `${BASE_URL}/compute/api/v1/compute/create/`;

export const GET_COMPUTES = `${BASE_URL}/compute/api/v1/compute/all/`;

export const GET_COMPUTES_WITHOUT_PAGINATION = `${BASE_URL}/compute/api/v1/compute_wp/all/`;

export const GET_COMPUTEID = `${BASE_URL}/compute/api/v1/compute/`;

export const UPDATE_COMPUTE = `${BASE_URL}/compute/api/v1/compute/update/`;

export const DELETE_COMPUTE = `${BASE_URL}/compute/api/v1/compute/delete/`;

export const SEARCH_COMPUTE = `${BASE_URL}/compute/api/v1/compute/search/`;

//  Payhead
export const CREATE_PYAHEAD = `${BASE_URL}/payhead/api/v1/payhead/create/`;

export const GET_PYAHEADS = `${BASE_URL}/payhead/api/v1/payhead/all/`;

export const GET_PYAHEADS_WITHOUT_PAGINATION = `${BASE_URL}/payhead/api/v1/payhead_wp/all/`;

export const GET_PYAHEADID = `${BASE_URL}/payhead/api/v1/payhead/`;

export const UPDATE_PYAHEAD = `${BASE_URL}/payhead/api/v1/payhead/update/`;

export const DELETE_PYAHEAD = `${BASE_URL}/payhead/api/v1/payhead/delete/`;

export const SEARCH_PYAHEAD = `${BASE_URL}/payhead/api/v1/payhead/search/`;

//  attendancevoucher
export const CREATE_ATTENDANCEVOUCHER = `${BASE_URL}/attendancevoucher/api/v1/attendancevoucher/create/`;

export const GET_ATTENDANCEVOUCHERS = `${BASE_URL}/attendancevoucher/api/v1/attendancevoucher/all/`;

export const GET_ATTENDANCEVOUCHERS_WITHOUT_PAGINATION = `${BASE_URL}/attendancevoucher/api/v1/attendancevoucher_wp/all/`;

export const GET_ATTENDANCEVOUCHERID = `${BASE_URL}/attendancevoucher/api/v1/attendancevoucher/`;

export const UPDATE_ATTENDANCEVOUCHER = `${BASE_URL}/attendancevoucher/api/v1/attendancevoucher/update/`;

export const DELETE_ATTENDANCEVOUCHER = `${BASE_URL}/attendancevoucher/api/v1/attendancevoucher/delete/`;

export const SEARCH_ATTENDANCEVOUCHER = `${BASE_URL}/attendancevoucher/api/v1/attendancevoucher/search/`;

//  leave_application
export const CREATE_APPLICATION = `${BASE_URL}/leave_application/api/v1/leave_application/create/`;

export const GET_APPLICATIONS = `${BASE_URL}/leave_application/api/v1/leave_application/all/`;

export const GET_APPLICATIONS_WITHOUT_PAGINATION = `${BASE_URL}/leave_application/api/v1/application_wp/all/`;

export const GET_APPLICATIONID = `${BASE_URL}/leave_application/api/v1/leave_application/`;

export const UPDATE_APPLICATION = `${BASE_URL}/leave_application/api/v1/leave_application/update/`;

export const DELETE_APPLICATION = `${BASE_URL}/leave_application/api/v1/leave_application/delete/`;

export const SEARCH_APPLICATION = `${BASE_URL}/leave_application/api/v1/leave_application/search/`;

//  Attendance/Production
export const CREATE_ATTENDANCE_PRODUCTION_TYPE = `${BASE_URL}/attendance_production/api/v1/attendance_production/create/`;

export const GET_ATTENDANCE_PRODUCTION_TYPES = `${BASE_URL}/attendance_production/api/v1/attendance_production/all/`;

export const GET_ATTENDANCE_PRODUCTION_TYPES_WITHOUT_PAGINATION = `${BASE_URL}/attendance_production/api/v1/attendance_production_wp/all/`;

export const GET_ATTENDANCE_PRODUCTION_TYPEID = `${BASE_URL}/attendance_production/api/v1/attendance_production/`;

export const GET_ATTENDANCE_PRODUCTION_TYPE_CALCULATION_TYPE_ID = `${BASE_URL}/attendance_production/api/v1/attendance_production_types/`;

export const UPDATE_ATTENDANCE_PRODUCTION_TYPE = `${BASE_URL}/attendance_production/api/v1/attendance_production/update/`;

export const DELETE_ATTENDANCE_PRODUCTION_TYPE = `${BASE_URL}/attendance_production/api/v1/attendance_production/delete/`;

export const SEARCH_ATTENDANCE_PRODUCTION_TYPE = `${BASE_URL}/attendance_production/api/v1/attendance_production/search/`;

// Attendance type

export const GET_ATTENDANCE_TYPES_WITHOUT_PAGINATION_W_PRODUCTION = `${BASE_URL}/attendance_type/api/v1/attendance_type_wp/all/`;

//  Payhead_type
export const CREATE_PAYHEAD_TYPE = `${BASE_URL}/payhead_type/api/v1/payhead_type/create/`;

export const GET_PAYHEAD_TYPES = `${BASE_URL}/payhead_type/api/v1/payhead_type/all/`;

export const GET_PAYHEAD_TYPES_WITHOUT_PAGINATION = `${BASE_URL}/payhead_type/api/v1/payhead_type_wp/all/`;

export const GET_PAYHEAD_TYPEID = `${BASE_URL}/payhead_type/api/v1/payhead_type/`;

export const UPDATE_PAYHEAD_TYPE = `${BASE_URL}/payhead_type/api/v1/payhead_type/update/`;

export const DELETE_PAYHEAD_TYPE = `${BASE_URL}/payhead_type/api/v1/payhead_type/delete/`;

export const SEARCH_PAYHEAD_TYPE = `${BASE_URL}/payhead_type/api/v1/payhead_type/search/`;

//  Payhead_assignment
export const CREATE_PAYHEAD_ASSIGNMENT = `${BASE_URL}/payhead_assignment/api/v1/payhead_assignment/create/`;

export const GET_PAYHEAD_ASSIGNMENTS = `${BASE_URL}/payhead_assignment/api/v1/payhead_assignment/all/`;

export const GET_PAYHEAD_ASSIGNMENTS_WITHOUT_PAGINATION = `${BASE_URL}/payhead_assignment/api/v1/payhead_assignment_wp/all/`;

export const GET_PAYHEAD_ASSIGNMENTID = `${BASE_URL}/payhead_assignment/api/v1/payhead_assignment/`;

export const GET_PAYHEAD_ASSIGNMENT_EMPLOYEE_LIST_BY_SALARY_ID = `${BASE_URL}/payhead_assignment/api/v1/employee_by_payhead_assignment/`;

export const UPDATE_PAYHEAD_ASSIGNMENT = `${BASE_URL}/payhead_assignment/api/v1/payhead_assignment/update/`;

export const DELETE_PAYHEAD_ASSIGNMENT = `${BASE_URL}/payhead_assignment/api/v1/payhead_assignment/delete/`;

export const SEARCH_PAYHEAD_ASSIGNMENT = `${BASE_URL}/payhead_assignment/api/v1/payhead_assignment/search/`;

//  Calculation_type
export const CREATE_CALCULATION_TYPE = `${BASE_URL}/calculation_type/api/v1/calculation_type/create/`;

export const GET_CALCULATION_TYPES = `${BASE_URL}/calculation_type/api/v1/calculation_type/all/`;

export const GET_CALCULATION_TYPES_WITHOUT_PAGINATION = `${BASE_URL}/calculation_type/api/v1/calculation_type_wp/all/`;

export const GET_CALCULATION_TYPEID = `${BASE_URL}/calculation_type/api/v1/calculation_type/`;

export const UPDATE_CALCULATION_TYPE = `${BASE_URL}/calculation_type/api/v1/calculation_type/update/`;

export const DELETE_CALCULATION_TYPE = `${BASE_URL}/calculation_type/api/v1/calculation_type/delete/`;

export const SEARCH_CALCULATION_TYPE = `${BASE_URL}/calculation_type/api/v1/calculation_type/search/`;

//  Voucher_type
export const CREATE_VOUCHER_TYPE = `${BASE_URL}/voucher_type/api/v1/voucher_type/create/`;

export const GET_VOUCHER_TYPES = `${BASE_URL}/voucher_type/api/v1/voucher_type/all/`;

export const GET_VOUCHER_TYPES_WITHOUT_PAGINATION = `${BASE_URL}/voucher_type/api/v1/voucher_type_wp/all/`;

export const GET_VOUCHER_TYPEID = `${BASE_URL}/voucher_type/api/v1/voucher_type/`;

export const UPDATE_VOUCHER_TYPE = `${BASE_URL}/voucher_type/api/v1/voucher_type/update/`;

export const DELETE_VOUCHER_TYPE = `${BASE_URL}/voucher_type/api/v1/voucher_type/delete/`;

export const SEARCH_VOUCHER_TYPE = `${BASE_URL}/voucher_type/api/v1/voucher_type/search/`;

// /payrollvoucher
export const GET_PAYROLL_VOUCHER_GENERATE = `${BASE_URL}/payrollvoucher/api/v1/payrollvoucher/generate/`;

export const CREATE_PAYROLL_VOUCHER = `${BASE_URL}/payrollvoucher/api/v1/payrollvoucher/create/`;

export const GET_PAYROLL_VOUCHERS = `${BASE_URL}/payrollvoucher/api/v1/payrollvoucher/all/`;

export const GET_PAYROLL_VOUCHERS_WITHOUT_PAGINATION = `${BASE_URL}/payrollvoucher/api/v1/payrollvoucher_wp/all/`;

export const GET_PAYROLL_VOUCHERID = `${BASE_URL}/payrollvoucher/api/v1/payrollvoucher/`;

export const UPDATE_PAYROLL_VOUCHER = `${BASE_URL}/payrollvoucher/api/v1/payrollvoucher/update/`;

export const DELETE_PAYROLL_VOUCHER = `${BASE_URL}/payrollvoucher/api/v1/payrollvoucher/delete/`;

export const SEARCH_PAYROLL_VOUCHER = `${BASE_URL}/payrollvoucher/api/v1/payrollvoucher/search/`;

//  Voucher_type_class
export const CREATE_VOUCHER_TYPE_CLASS = `${BASE_URL}/voucher_type_class/api/v1/voucher_type_class/create/`;

export const GET_VOUCHER_TYPE_CLASSS = `${BASE_URL}/voucher_type_class/api/v1/voucher_type_class/all/`;

export const GET_VOUCHER_TYPE_CLASSS_WITHOUT_PAGINATION = `${BASE_URL}/voucher_type_class/api/v1/voucher_type_class_wp/all/`;

export const GET_VOUCHER_TYPE_CLASSID = `${BASE_URL}/voucher_type_class/api/v1/voucher_type_class/`;

export const GET_VOUCHER_TYPE_CLASS_LEDGER_ALL = `${BASE_URL}/voucher_type_class/api/v1/voucher_type_class/ledger_accounts/`;

export const UPDATE_VOUCHER_TYPE_CLASS = `${BASE_URL}/voucher_type_class/api/v1/voucher_type_class/update/`;

export const DELETE_VOUCHER_TYPE_CLASS = `${BASE_URL}/voucher_type_class/api/v1/voucher_type_class/delete/`;

export const SEARCH_VOUCHER_TYPE_CLASS = `${BASE_URL}/voucher_type_class/api/v1/voucher_type_class/search/`;

// Attendance
export const FILTER_ATTENDANCE_REPORT = `${BASE_URL}/attendance/api/v1/attendance_report/filter/`;

// Attendance summary
export const FILTER_ATTENDANCESUMMARY_REPORT = `${BASE_URL}/checkin_checkout/api/v1/checkin_checkout/report/`;

// Attendance Import
export const ATTENDANCE_IMPORT = `${BASE_URL}/attendance_machine/api/v1/attendance_machine/fetch_attendaces/`;

// Attendance Import
export const IMPORT_EMPLOYEE_INTO_ATTENDANCE_MACHINE = `${BASE_URL}/attendance_machine/api/v1/attendance_machine/import_user_into_attendance_machine/`;
export const IMPORT_EMPLOYEE_FROM_ATTENDANCE_MACHINE = `${BASE_URL}/attendance_machine/api/v1/attendance_machine/import_user_from_attendance_machine/`;

// Accounts
// primary_group
export const PRIMARY_GROUP = `${BASE_URL}/primary_group/api/v1/primary_group/all/`;

//paymentVoucher
export const CREATE_PAYMENTVOUCHER = `${BASE_URL}/payment_voucher/api/v1/payment_voucher/create/`;

export const GET_PAYMENTVOUCHERS = `${BASE_URL}/payment_voucher/api/v1/payment_voucher/all`;

export const GET_PAYMENTVOUCHER_BY_ID = `${BASE_URL}/payment_voucher/api/v1/payment_voucher/`;

export const UPDATE_PAYMENTVOUCHER = `${BASE_URL}/payment_voucher/api/v1/payment_voucher/update/`;

export const DELETE_PAYMENTVOUCHER = `${BASE_URL}/payment_voucher/api/v1/payment_voucher/delete/`;

export const DELETE_PAYMENTVOUCHER_MULTIPLE = `${BASE_URL}/payment_voucher/api/v1/payment_voucher/delete_multiple/`;

export const SEARCH_PAYMENT_VOUCHER = `${BASE_URL}/payment_voucher/api/v1/payment_voucher/search/`;

export const SEARCH_PAYMENTVOUCHER = `${BASE_URL}/payment_voucher/api/v1/payment_voucher/search/`;

export const GET_PAYMENT_VOUCHER_BY_INVOICE_NO = `${BASE_URL}/payment_voucher/api/v1/payment_voucher/payment_voucher_by_invoice_no/`;

export const GET_PAYMENT_VOUCHER_ID_NAME_BY = `${BASE_URL}/payment_voucher/api/v1/payment_voucher/payment_voucher_with_id_name_dict_by_invoice_no/`;

export const GET_PAYMENTVOUCHERID = `${BASE_URL}/payment_voucher/api/v1/payment_voucher/payment_voucher_by_invoice_no/`;

//receiptVoucher
export const CREATE_RECEIPTVOUCHER = `${BASE_URL}/receipt_voucher/api/v1/receipt_voucher/create/`;

export const GET_RECEIPTVOUCHERS = `${BASE_URL}/receipt_voucher/api/v1/receipt_voucher/all`;

export const GET_RECEIPTVOUCHER_BY_ID = `${BASE_URL}/receipt_voucher/api/v1/receipt_voucher/`;

export const UPDATE_RECEIPTVOUCHER = `${BASE_URL}/receipt_voucher/api/v1/receipt_voucher/update/`;

export const DELETE_RECEIPTVOUCHER = `${BASE_URL}/receipt_voucher/api/v1/receipt_voucher/delete/`;

export const DELETE_RECEIPTVOUCHER_MULTIPLE = `${BASE_URL}/receipt_voucher/api/v1/receipt_voucher/delete_multiple/`;

export const SEARCH_RECEIPTVOUCHER = `${BASE_URL}/receipt_voucher/api/v1/receipt_voucher/search/`;

export const GET_RECEIPT_VOUCHER_BY_INVOICE_NO = `${BASE_URL}/receipt_voucher/api/v1/receipt_voucher/receipt_voucher_by_invoice_no/`;

export const GET_RECEIPT_VOUCHER_ID_NAME_BY = `${BASE_URL}/receipt_voucher/api/v1/receipt_voucher/receipt_voucher_by_invoice_no/`;

export const GET_RECEIPTVOUCHERID = `${BASE_URL}/receipt_voucher/api/v1/receipt_voucher/receipt_voucher_by_invoice_no/`;

export const SEARCH_RECEIPT_VOUCHER = `${BASE_URL}/receipt_voucher/api/v1/receipt_voucher/search/`;

export const GET_RECEIPT_VOUCHER_BY_INVOICE_NUMBER = `${BASE_URL}/receipt_voucher/api/v1/receipt_voucher/receipt_voucher_by_invoice_no/`;

//sales
export const CREATE_RECEIVABLEBILL = `${BASE_URL}/sales/api/v1/sales/create/`;

export const GET_RECEIVABLEBILLS = `${BASE_URL}/sales/api/v1/sales/all`;

export const GET_RECEIVABLEBILL_BY_ID = `${BASE_URL}/sales/api/v1/sales/`;

export const UPDATE_RECEIVABLEBILL = `${BASE_URL}/sales/api/v1/sales/update/`;

export const DELETE_RECEIVABLEBILL = `${BASE_URL}/sales/api/v1/sales/delete/`;

export const DELETE_RECEIVABLEBILL_MULTIPLE = `${BASE_URL}/sales/api/v1/sales/delete_multiple/`;

export const SEARCH_RECEIVABLEBILL = `${BASE_URL}/sales/api/v1/sales/search/`;

export const GET_RECEIVABLEBILL_BY_INVOICE_NO = `${BASE_URL}/sales/api/v1/sales/receipt_voucher_by_invoice_no/`;

// accounts/sales
export const CREATE_SALES = `${BASE_URL}/sales/api/v1/sales/create/`;

export const GET_SALESID = `${BASE_URL}/sales/api/v1/sales/`;

export const GET_ALLSALES = `${BASE_URL}/sales/api/v1/sales/all/`;

export const UPDATE_SALES = `${BASE_URL}/sales/api/v1/sales/update/`;

export const DELETE_SALES = `${BASE_URL}/sales/api/v1/sales/delete/`;

export const SEARCH_SALES = `${BASE_URL}/sales/api/v1/sales/search/`;

export const GET_SALES_BY_INVOICE_NUMBER = `${BASE_URL}/sales/api/v1/sales/sales_by_invoice_no/`;

//purchases

export const CREATE_PAYABLEBILL = `${BASE_URL}/purchase/api/v1/purchase/create/`;

export const GET_PAYABLEBILLS = `${BASE_URL}/purchase/api/v1/purchase/all`;

export const GET_PAYABLEBILL_BY_ID = `${BASE_URL}/purchase/api/v1/purchase/`;

export const UPDATE_PAYABLEBILL = `${BASE_URL}/purchase/api/v1/purchase/update/`;

export const DELETE_PAYABLEBILL = `${BASE_URL}/purchase/api/v1/purchase/delete/`;

export const DELETE_PAYABLEBILL_MULTIPLE = `${BASE_URL}/purchase/api/v1/purchase/delete_multiple/`;

export const SEARCH_PAYABLEBILL = `${BASE_URL}/purchase/api/v1/purchase/search/`;

export const GET_PAYABLEBILL_BY_INVOICE_NO = `${BASE_URL}/purchase/api/v1/purchase/purchase_by_invoice_no/`;

// accounts/purchase

export const CREATE_ACCOUNT_PURCHASE = `${BASE_URL}/purchase/api/v1/purchase/create/`;

export const GET_ACCOUNT_PURCHASEID = `${BASE_URL}/purchase/api/v1/purchase/`;

export const GET_ACCOUNT_PURCHASES = `${BASE_URL}/purchase/api/v1/purchase/all/`;

export const UPDATE_ACCOUNT_PURCHASE = `${BASE_URL}/purchase/api/v1/purchase/update/`;

export const DELETE_ACCOUNT_PURCHASE = `${BASE_URL}/purchase/api/v1/purchase/delete/`;

export const SEARCH_ACCOUNT_PURCHASE = `${BASE_URL}/purchase/api/v1/purchase/search/`;

export const GET_ACCOUNT_PURCHASE_BY_INVOICE_NUMBER = `${BASE_URL}/purchase/api/v1/purchase/purchase_by_invoice_no/`;

// Purchase Req

export const SEARCH_PURCHASE_REQ = `${BASE_URL}/purchase_request/api/v1/purchase_request/search/`;

export const GET_PURCHASE_BY_INVOICE = `${BASE_URL}/purchase_request/api/v1/purchase_request/get_invoice_by_purchase_request_id/`;

export const DELETE_PURCHASE_REQ_ITEM = `${BASE_URL}/purchase_req_item/api/v1/purchase_req_item/delete/`;

export const CREATE_PURCHASE = `${BASE_URL}/purchase_request/api/v1/purchase_request/create_vendor_purchase_request/`;

export const GET_PURCHASES = `${BASE_URL}/purchase_request/api/v1/purchase_request/all/`;

export const DELETE_PURCHASE = `${BASE_URL}/purchase_request/api/v1/purchase_request/delete/`;

export const GET_PURCHASEID = `${BASE_URL}/purchase_request/api/v1/purchase_request_with_items_images/`;
export const GET_PURCHASE_ITEMS = `${BASE_URL}/purchase_request/api/v1/purchase_request_with_items/`;

export const UPDATE_PURCHASE = `${BASE_URL}/purchase_request/api/v1/purchase_request/update/`;

// Purchase Final

export const CREATE_PURCHASE_FINAL = `${BASE_URL}/purchase_final/api/v1/purchase_final/create_from_purchase_request/`;

export const DELETE_PURCHASE_FINAL_ITEM = `${BASE_URL}/purchase_final/api/v1/purchase_final/delete_item/`;

export const GET_PURCHASE_FINAL_WITH_ITEMS = `${BASE_URL}/purchase_final/api/v1/purchase_final/get_a_purchase_final_with_items/`;

export const PAY_AND_CONFIRM_PURCHASE_FINAL = `${BASE_URL}/purchase_final/api/v1/purchase_final/paybill_and_confirm/`;

export const UPDATE_PURCHASE_FINAL_ITEM = `${BASE_URL}/purchase_final/api/v1/purchase_final/update_item/`;

export const UPDATE_PURCHASE_FINAL = `${BASE_URL}/purchase_final/api/v1/purchase_final/update/`;

export const GET_PURCHASE_FINALS = `${BASE_URL}/purchase_final/api/v1/purchase_final/all/`;

export const GET_PURCHASE_FINALS_ITMES_BY_INVOICE = `${BASE_URL}/purchase_final/api/v1/purchase_final/get_purchase_final_by_invoice_no/`;

// Inventory
export const GET_INVENTORY_BY_ID = `${BASE_URL}/inventory/api/v1/inventory/`;
export const GET_STOCK_INVENTORY_BY_PRODUCT_ID = `${BASE_URL}/inventory/api/v1/inventory_by_product_id/`;

export const GET_INVENTORY_BY_PRODUCT_ID = `${BASE_URL}/inventory/api/v1/inventory/check_by_product_id/`;

//contras
export const CREATE_CONTRA = `${BASE_URL}/contra/api/v1/contra/create/`;

export const GET_CONTRAS = `${BASE_URL}/contra/api/v1/contra/all`;

export const GET_CONTRA_BY_ID = `${BASE_URL}/contra/api/v1/contra/`;

export const UPDATE_CONTRA = `${BASE_URL}/contra/api/v1/contra/update/`;

export const DELETE_CONTRA = `${BASE_URL}/contra/api/v1/contra/delete/`;

export const DELETE_CONTRA_MULTIPLE = `${BASE_URL}/contra/api/v1/contra/delete_multiple/`;

export const SEARCH_CONTRA = `${BASE_URL}/contra/api/v1/contra/search/`;

export const GET_CONTRA_BY_INVOICE_NO = `${BASE_URL}/contra/api/v1/contra/contra_by_invoice_no/`;

//journals
export const CREATE_JOURNAL = `${BASE_URL}/journal/api/v1/journal/create/`;

export const GET_JOURNALS = `${BASE_URL}/journal/api/v1/journal/all`;

export const GET_JOURNAL_BY_ID = `${BASE_URL}/journal/api/v1/journal/`;

export const UPDATE_JOURNAL = `${BASE_URL}/journal/api/v1/journal/update/`;

export const DELETE_JOURNAL = `${BASE_URL}/journal/api/v1/journal/delete/`;

export const DELETE_JOURNAL_MULTIPLE = `${BASE_URL}/journal/api/v1/journal/delete_multiple/`;

export const SEARCH_JOURNAL = `${BASE_URL}/journal/api/v1/journal/search/`;

export const GET_JOURNAL_BY_INVOICE_NO = `${BASE_URL}/journal/api/v1/journal/journal_by_invoice_no/`;

// site_settings
export const CREATE_SITESETTING = `${BASE_URL}/general_setting/api/v1/general_setting/create/`;

export const GET_SITESETTINGID = `${BASE_URL}/general_setting/api/v1/general_setting/`;

export const GET_SITESETTINGS = `${BASE_URL}/general_setting/api/v1/general_setting/all/`;

export const UPDATE_SITESETTING = `${BASE_URL}/general_setting/api/v1/general_setting/update/`;

export const DELETE_SITESETTING = `${BASE_URL}/general_setting/api/v1/general_setting/delete/`;

// branch
export const CREATE_BRANCH = `${BASE_URL}/branch/api/v1/branch/create/`;

export const GET_BRANCHID = `${BASE_URL}/branch/api/v1/branch/`;

export const GET_BRANCHS = `${BASE_URL}/branch/api/v1/branch/all/`;

export const GET_BRANCH_WITHOUT_PAGINATION = `${BASE_URL}/branch/api/v1/branch/without_pagination/all/`;

export const UPDATE_BRANCH = `${BASE_URL}/branch/api/v1/branch/update/`;

export const DELETE_BRANCH = `${BASE_URL}/branch/api/v1/branch/delete/`;

export const SEARCH_BRANCH = `${BASE_URL}/branch/api/v1/branch/search/`;

//menu item
export const MENU_ITEMS = `${BASE_URL}/menu_item/api/v1/menu_item/nested_menu_item_by_user_role/`;

export const CREATE_MENU = `${BASE_URL}/menu_item/api/v1/menu_item/create/`;

export const DELETE_MENU = `${BASE_URL}/menu_item/api/v1/menu_item/delete/`;

export const GET_MENUS = `${BASE_URL}/menu_item/api/v1/menu_item/`;

export const UPDATE_MENU = `${BASE_URL}/menu_item/api/v1/menu_item/update/`;

export const GET_MENUS_ALL = `${BASE_URL}/menu_item/api/v1/menu_item/all/`;

export const GET_MENUS_ALL_NESTED = `${BASE_URL}/menu_item/api/v1/menu_item/nested_menu_item_without_pagination/all/`;

export const GET_MENUS_BY_ROLE = `${BASE_URL}/menu_item/api/v1/menu_item/nested_menu_item_by_role_id/`;

export const SEARCH_MENU = `${BASE_URL}/menu_item/api/v1/menu_item/search/`;

export const GET_MENUS_WITHOUT_PAGINATION = `${BASE_URL}/menu_item/api/v1/menu_item/without_pagination/all/`;

//User_Permission

export const GET_USER_PERMISSION = `${BASE_URL}/permission/api/v1/permission/get_all_permission_by_user_role/`;

//role menu
export const CREATE_ROLEMENU = `${BASE_URL}/role_menu/api/v1/role_menu/create/`;

export const GET_ROLEMENUS = `${BASE_URL}/role_menu/api/v1/role_menu/all`;

export const GET_ROLEMENU_BY_ID = `${BASE_URL}/role_menu/api/v1/role_menu/`;

export const UPDATE_ROLEMENU = `${BASE_URL}/role_menu/api/v1/role_menu/update/`;

export const DELETE_ROLEMENU = `${BASE_URL}/role_menu/api/v1/role_menu/delete/`;

// export const SEARCH_ROLEMENU = `${BASE_URL}/rolemenu/api/v1/rolemenu/search/`;

//ledger
export const CREATE_LEDGER = `${BASE_URL}/ledger_account/api/v1/ledger_account/create/`;

export const GET_LEDGERS = `${BASE_URL}/ledger_account/api/v1/ledger_account/all`;

export const GET_LEDGER_ACCOUNT_CASH_AND_BANK = `${BASE_URL}/ledger_account/api/v1/ledger_account/get_cash_and_bank_ledgers/`;

export const GET_LEDGER_BY_ID = `${BASE_URL}/ledger_account/api/v1/ledger_account/`;

export const UPDATE_LEDGER = `${BASE_URL}/ledger_account/api/v1/ledger_account/update/`;

export const DELETE_LEDGER = `${BASE_URL}/ledger_account/api/v1/ledger_account/delete/`;

export const DELETE_LEDGER_MULTIPLE = `${BASE_URL}/ledger_account/api/v1/ledger_account/delete_multiple/`;

export const SEARCH_LEDGER = `${BASE_URL}/ledger_account/api/v1/ledger_account/search/`;

export const LEDGERS_WITHOUT_PAGINATION = `${BASE_URL}/ledger_account/api/v1/ledger_account/without_pagination/all/`;

//subLedger
export const CREATE_SUBLEDGER = `${BASE_URL}/sub_ledger_account/api/v1/sub_ledger_account/create/`;

export const GET_SUBLEDGERS = `${BASE_URL}/sub_ledger_account/api/v1/sub_ledger_account/all`;

export const GET_SUBLEDGER_BY_ID = `${BASE_URL}/sub_ledger_account/api/v1/sub_ledger_account/`;

export const UPDATE_SUBLEDGER = `${BASE_URL}/sub_ledger_account/api/v1/sub_ledger_account/update/`;

export const DELETE_SUBLEDGER = `${BASE_URL}/sub_ledger_account/api/v1/sub_ledger_account/delete/`;

export const DELETE_SUBLEDGER_MULTIPLE = `${BASE_URL}/sub_ledger_account/api/v1/sub_ledger_account/delete_multiple/`;

export const SEARCH_SUBLEDGER = `${BASE_URL}/sub_ledger_account/api/v1/sub_ledger_account/search/`;

export const SUBLEDGERS_WITHOUT_PAGINATION = `${BASE_URL}/sub_ledger_account/api/v1/sub_ledger_account/without_pagination/all/`;

//group
export const CREATE_GROUP = `${BASE_URL}/group/api/v1/group/create/`;

export const GET_GROUPS = `${BASE_URL}/group/api/v1/group/all`;

export const GET_GROUP_BY_ID = `${BASE_URL}/group/api/v1/group/`;

export const GET_GROUP_BY_PAYHEAD_ID = `${BASE_URL}/group/api/v1/group/groupby_payhead/`;

export const UPDATE_GROUP = `${BASE_URL}/group/api/v1/group/update/`;

export const DELETE_GROUP = `${BASE_URL}/group/api/v1/group/delete/`;

export const DELETE_GROUP_MULTIPLE = `${BASE_URL}/group/api/v1/group/delete_multiple/`;

export const SEARCH_GROUP = `${BASE_URL}/group/api/v1/group/search/`;

// category
export const GET_PERENT_CATEGORIES = `${BASE_URL}/category/api/v1/category/get_all_parent_category_without_pagination/`;

export const GET_SUB_CATEGORIES_BY_ID = `${BASE_URL}/category/api/v1/category/get_all_sub_category_by_category_id_without_pagination/`;

export const GET_SUB_SUB_CATEGORIES_BY_ID = `${BASE_URL}/category/api/v1/category/get_all_sub_sub_category_by_category_id_without_pagination/`;

export const GET_CATEGORIES_WITHOUT_PAGINATION = `${BASE_URL}/category/api/v1/category/without_pagination/all/`;

export const SEARCH_CATEGORY = `${BASE_URL}/category/api/v1/category/search/`;

export const GET_CATEGORIES = `${BASE_URL}/category/api/v1/category/all/`;

export const CREATE_CATEGORY = `${BASE_URL}/category/api/v1/category/create/`;

export const GET_CATEGORY = `${BASE_URL}/category/api/v1/category/`;

export const UPDATE_CATEGORY = `${BASE_URL}/category/api/v1/category/update/`;

export const DELETE_CATEGORY = `${BASE_URL}/category/api/v1/category/delete/`;

// Order
export const CREATE_ORDER = `${BASE_URL}/order/api/v1/order/create/`;

export const GET_ORDERS = `${BASE_URL}/order/api/v1/order/all`;

export const GET_ORDERID = `${BASE_URL}/order/api/v1/order/`;

export const UPDATE_ORDER = `${BASE_URL}/order/api/v1/order/update/`;

export const DELETE_ORDER = `${BASE_URL}/customerorder/api/v1/customerorder_delete_order/`;

export const SEARCH_ORDER = `${BASE_URL}/customerorder/api/v1/customerorder/search/`;

export const SHIPPING_ADDRESS_BY_ORDERID = `${BASE_URL}/shippingaddress/api/v1/shippingaddress_using_order_id/`;

// Customer Order
export const CREATE_CUSTOMER_ORDER = `${BASE_URL}/customerorder/api/v1/customerorder_admin/create/`;

export const CONFIRM_CUSTOMER_ORDER = `${BASE_URL}/customerorder/api/v1/customerorder/confirm/`;

export const UPDATE_CUSTOMER_ORDER = `${BASE_URL}/customerorder/api/v1/customerorder/update_order/`;

export const GET_A_CUSTOMER_ORDER = `${BASE_URL}/customerorder/api/v1/customerorder/get_a_customer_order/`;

export const GET_INVOICE_BY_ORDERID = `${BASE_URL}/customerorder/api/v1/customerorder/get_invoice_by_order_id/`;

// Billing Address

export const CREATE_BILLING_ADDRESS = `${BASE_URL}/billingaddress/api/v1/billingaddress/create/`;

export const GET_BILLING_ADDRESS_ID = `${BASE_URL}/billingaddress/api/v1/billingaddress/`;

export const GET_BILLING_ADDRESSS = `${BASE_URL}/billingaddress/api/v1/billingaddress/all/`;

export const UPDATE_BILLING_ADDRESSS = `${BASE_URL}/billingaddress/api/v1/billingaddress/update/`;

export const DELETE_BILLING_ADDRESS = `${BASE_URL}/billingaddress/api/v1/billingaddress/delete/`;

export const SEARCH_BILLING_ADDRESS = `${BASE_URL}/billingaddress/api/v1/billingaddress/search/`;

//  manufacturers
export const CREATE_MANUFACTURER = `${BASE_URL}/manufacturer/api/v1/manufacturer/create/`;

export const GET_MANUFACTURERS_WITHOUT_PAGINATION = `${BASE_URL}/manufacturer/api/v1/manufacturer/without_pagination/all/`;

export const GET_MANUFACTURERS = `${BASE_URL}/manufacturer/api/v1/manufacturer/all/`;

export const GET_MANUFACTURERID = `${BASE_URL}/manufacturer/api/v1/manufacturer/`;

export const UPDATE_MANUFACTURER = `${BASE_URL}/manufacturer/api/v1/manufacturer/update/`;

export const DELETE_MANUFACTURER = `${BASE_URL}/manufacturer/api/v1/manufacturer/delete/`;

export const SEARCH_MANUFACTURER = `${BASE_URL}/manufacturer/api/v1/manufacturer/search/`;

// Vendor
export const CREATE_VENDOR = `${BASE_URL}/vendor/api/v1/vendor/create/`;

export const GET_VENDORS = `${BASE_URL}/vendor/api/v1/vendor/all/`;

export const GET_VENDORS_WITHOUT_PAGINATION = `${BASE_URL}/vendor/api/v1/vendor/without_pagination/all/`;

export const GET_VENDORID = `${BASE_URL}/vendor/api/v1/vendor/`;

export const UPDATE_VENDOR = `${BASE_URL}/vendor/api/v1/vendor/update/`;

export const DELETE_VENDOR = `${BASE_URL}/vendor/api/v1/vendor/delete/`;

export const SEARCH_VENDOR = `${BASE_URL}/vendor/api/v1/vendor/search/`;

// Purchase Final
export const GET_PURCHASE_FINAL_BY_INVOICE = `${BASE_URL}/purchase_final/api/v1/purchase_final/get_invoice_by_purchase_final_id/`;

export const SEARCH_PURCHASE_FINAL = `${BASE_URL}/purchase_final/api/v1/purchase_final/search/`;

export const CREATE_PURCHASE_FINAL_MANUALLY = `${BASE_URL}/purchase_final/api/v1/purchase_final/create_directly_from_admin_panel/`;

export const DELETE_PURCHASE_FINAL = `${BASE_URL}/purchase_final/api/v1/purchase_final/delete/`;

export const GET_PURCHASE_FINAL = `${BASE_URL}/purchase_final/api/v1/purchase_final/`;

//  Order
export const ORDERSTATUS = `${BASE_URL}/orderstatus/api/v1/orderstatus/all/`;

// Customer
export const CREATE_CUSTOMER = `${BASE_URL}/customer/api/v1/customer/create/`;

export const GET_CUSTOMERS = `${BASE_URL}/customer/api/v1/customer/all`;

export const GET_LATEST_CUSTOMERS = `${BASE_URL}/customer/api/v1/customer/get_new_customer_of_last_month`;

export const GET_CUSTOMERID = `${BASE_URL}/customer/api/v1/customer/`;

export const UPDATE_CUSTOMER = `${BASE_URL}/customer/api/v1/customer/update/`;

export const DELETE_CUSTOMER = `${BASE_URL}/customer/api/v1/customer/delete/`;

export const SEARCH_CUSTOMER = `${BASE_URL}/customer/api/v1/customer/search/`;

//Get customer without pagination
export const GET_CUSTOMER_WITHOUT_PAGINATION = `${BASE_URL}/customer/api/v1/customer/without_pagination/all/`;

export const GET_CUSTOMER_ORDER_DETAILS = `${BASE_URL}/customerorder/api/v1/customerorder/get_a_customer_order_detail/`;

export const UPDATE_PRODUC_TITEM = `${BASE_URL}/customerorder/api/v1/customerorder/update_item/`;

export const REMOVE_PRODUC_TITEM = `${BASE_URL}/customerorder/api/v1/customerorder_delete_item/`;

export const CUSTOMERORDERDETAILSWITHSHIPPINGADDRESS = `${BASE_URL}/customerorder/api/v1/customerorder/get_a_customer_order_detail_with_order_orderitem_shipping/`;

//  Customer types
export const CUSOTMERTYPES = `${BASE_URL}/customer_type/api/v1/customer_type/all/`;

// Inventory
export const GET_INVENTORY_SEARCH = `${BASE_URL}/inventory/api/v1/inventory/search/`;

// ticket
export const CREATE_TICKET_DETAIL = `${BASE_URL}/ticket_detail/api/v1/ticket_detail/create/`;

export const GET_TICKET_DETAILS = `${BASE_URL}/ticket_detail/api/v1/ticket_detail/all/`;

export const GET_TICKET_DETAILS_BY_ID = `${BASE_URL}/ticket_detail/api/v1/ticket_detail/get_all_by_ticket_id/`;

export const GET_TICKETS = `${BASE_URL}/ticket/api/v1/ticket/all/`;

export const DELETE_TICKET = `${BASE_URL}/ticket/api/v1/ticket/delete/`;

export const UPDATE_TICKET = `${BASE_URL}/ticket/api/v1/ticket/update/`;

export const GET_ACCOUNT_TICKETS = `${BASE_URL}/ticket/api/v1/ticket/ticket_of_account_department`;

export const GET_SALES_TICKETS = `${BASE_URL}/ticket/api/v1/ticket/ticket_of_sales_department`;

export const GET_SUPPORT_TICKETS = `${BASE_URL}/ticket/api/v1/ticket/ticket_of_support_department`;

//chat
export const GET_All_CHATS = `${BASE_URL}/chat/api/v1/chat/get_all_chats`;

export const GET_All_CHATS_FOR_EMPLOYEE = `${BASE_URL}/chat/api/v1/chat/get_all_employee_chats/`;

export const CHAT_ASSIGIN_TO_USER = `${BASE_URL}/chat/api/v1/chat/assign_employee_to_chat/`;

export const DELETE_CHAT = `${BASE_URL}/chat/api/v1/chat/delete/`;

export const GET_All_EMPLOYEE_THROUGH_CHAT = `${BASE_URL}/chat/api/v1/chat/get_all_employee/`;

export const GET_All_ASSIGNED_CONTACT = `${BASE_URL}/chat/api/v1/chat/get_assigned_chats_for_employee/`;

//ticket_status
export const GET_TICKET_STATUS = `${BASE_URL}/ticket_status/api/v1/ticket_status/all`;

export const UPDATE_TICKET_STATUS = `${BASE_URL}/ticket_status/api/v1/ticket_status/update/`;

// Purchase
export const GET_PURCHASESTATUS = `${BASE_URL}/purchase_status/api/v1/purchase_status/all/`;

//  product
export const SEARCH_PRODUCT = `${BASE_URL}/product/api/v1/product/search/`;

export const CREATE_PRODUCT = `${BASE_URL}/product/api/v1/product/create/`;

export const GET_PRODUCTS = `${BASE_URL}/product/api/v1/product/all/`;

export const GET_PRODUCT = `${BASE_URL}/product/api/v1/product/`;

export const UPDATE_PRODUCT = `${BASE_URL}/product/api/v1/product/update/`;

export const DELETE_PRODUCT = `${BASE_URL}/product/api/v1/product/delete/`;

export const GET_ALL_PRODUCT_WITHOUT_PG = `${BASE_URL}/product/api/v1/product/without_pagination/all/`;

export const GET_ATTRIBUTE_SET_WITHOUT_PAGINATION = `${BASE_URL}/attributeset/api/v1/attributeset/without_pagination/all/`;

//  product-color

export const CREATE_PRODUCT_COLOR = `${BASE_URL}/productcolor/api/v1/productcolor/create/`;
export const GET_PRODUCT_COLORS_BY_ID = `${BASE_URL}/productcolor/api/v1/productcolor/get_all_by_product_id/`;
export const DELETE_PRODUCT_COLOR = `${BASE_URL}/productcolor/api/v1/productcolor/delete/`;

// product-img
export const GET_PRODUCT_BY_ID = `${BASE_URL}/productimage/api/v1/productimage/all_productimage_by_product_id/`;

export const CREATE_PRODUCT_IMAGE = `${BASE_URL}/productimage/api/v1/productimage/create/`;

export const GET_PRODUCT_IMAGES = `${BASE_URL}/productimage/api/v1/productimage/all/`;

export const DELETE_PRODUCT_IMAGE = `${BASE_URL}/productimage/api/v1/productimage/delete/`;

//  product size
export const CREATE_PRODUCT_SIZE = `${BASE_URL}/productsize/api/v1/productsize/create/`;

export const GET_PRODUCT_SIZE = `${BASE_URL}/productsize/api/v1/productsize/all/`;

export const GET_PRODUCT_SIZE_BY_ID = `${BASE_URL}/productsize/api/v1/productsize/get_by_product_id`;

export const GET_SIZE_BY_PRODUCTID_COLORID = `${BASE_URL}/inventory/api/v1/inventory/check_sizes_by_product_and_color/`;

export const DELETE_PRODUCT_SIZE = `${BASE_URL}/productsize/api/v1/productsize/remove_size_from_froductsize/`;

//  paymentMathods
export const PAYMENT_MATHODS = `${BASE_URL}/paymentmethod/api/v1/paymentmethod/all/`;

//reports

//account statement report
export const ACCOUNTSTATEMENT_FILTER_BY = `${BASE_URL}/account_log_report/api/v1/account_log_report/by_ledger_type/`;

export const ACCOUNTSTATEMENT_FILTER_WITHOUT_PG = `${BASE_URL}/account_log_report/api/v1/account_log_report/by_ledger_type_wp/`;

//account statement summary report
export const ACCOUNTSUMMARY_FILTER_BY = `${BASE_URL}/account_log_report/api/v1/account_log_report/get_total_cash_dr_cr_bank_dr_cr/`;

export const ACCOUNTSUMMARY_FILTER_WITHOUT_PG = `${BASE_URL}/account_log_report/api/v1/account_log_report/get_total_cash_dr_cr_bank_dr_cr/`;

//ledger report
export const LEDGER_FILTER_BY = `${BASE_URL}/account_log_report/api/v1/account_log_report/general/`;

export const LEDGER_FILTER_WITHOUT_PG = `${BASE_URL}/account_log_report/api/v1/account_log_report/general_wp/`;

//sub ledger report
export const PAYMENT_FILTER_BY = `${BASE_URL}/account_report/api/v1/account_report/report_for_payment_voucher/`;

export const PAYMENT_FILTER_WITHOUT_PG = `${BASE_URL}/account_report/api/v1/account_report/report_for_payment_voucher_wp/`;

// payment-summary
export const PAYMENT_SUMMARY_FILTER_BY = `${BASE_URL}/account_report/api/v1/account_report/summary_for_payment_voucher/`;

export const PAYMENT_SUMMARY_FILTER_WITHOUT_PG = `${BASE_URL}/account_report/api/v1/account_report/summary_for_payment_voucher_wp/`;

//receipt voucher report
export const RECEIPT_FILTER_BY = `${BASE_URL}/account_report/api/v1/account_report/report_for_receipt_voucher/`;

export const RECEIPT_FILTER_WITHOUT_PG = `${BASE_URL}/account_report/api/v1/account_report/report_for_receipt_voucher_wp/`;

// receipt-summary report
export const RECEIPT_SUMMARY_FILTER_BY = `${BASE_URL}/account_report/api/v1/account_report/summary_for_receipt_voucher/`;

export const RECEIPT_SUMMARY_FILTER_WITHOUT_PG = `${BASE_URL}/account_report/api/v1/account_report/summary_for_receipt_voucher_wp/`;

// order report
export const ORDER_FILTER_BY = `${BASE_URL}/report/api/v1/order_report_with_profit/filter/`;

export const ORDER_FILTER_WITHOUT_PG = `${BASE_URL}/report/api/v1/order_report_with_profit_wp/filter/`;

// order-summary report
export const ORDER_SUMMARY_FILTER_BY = `${BASE_URL}/summary_report/api/v1/order_summary_report/filter/`;

export const ORDER_SUMMARY_FILTER_WITHOUT_PG = `${BASE_URL}/summary_report/api/v1/order_summary_report_wp/filter/`;

// Stock Summary Report

export const FILTER_SUMMARY_INVENTORY_REPORT = `${BASE_URL}/summary_report/api/v1/inventory_summary_report/filter/`;

export const FILTER_SUMMARY_INVENTORY_REPORT_WITHOUT_PG = `${BASE_URL}/summary_report/api/v1/inventory_summary_report_wp/filter/`;
// Low Stock  Summary Report

export const FILTER_SUMMARY_LOW_INVENTORY_REPORT = `${BASE_URL}/dashboard/api/v1/get_all_low_stock_products/`;

export const FILTER_SUMMARY_LOW_INVENTORY_REPORT_WITHOUT_PG = `${BASE_URL}/dashboard/api/v1/get_all_low_stock_products_wp/`;

// Gross Profit  Summary Report

export const FILTER_SUMMARY_GROSS_PROFIT_REPORT = `${BASE_URL}/account_report/api/v1/account_report/gross_profit_and_loss_report/`;

export const FILTER_SUMMARY_GROSS_PROFIT_REPORT_WITHOUT_PG = `${BASE_URL}/account_report/api/v1/account_report/gross_profit_and_loss_report_wp/`;
// Out of Stock  Summary Report

export const FILTER_SUMMARY_OUT_OF_INVENTORY_REPORT = `${BASE_URL}/dashboard/api/v1/get_all_out_of_stock_products/`;

export const FILTER_SUMMARY_OUT_OF_INVENTORY_REPORT_WITHOUT_PG = `${BASE_URL}/summary_report/api/v1/inventory_summary_report/filter_wp/`;

// Purchase Final Report

export const FILTER_PURCHASE_FINAL_REPORT = `${BASE_URL}/summary_report/api/v1/purchase_final_summary_report/filter/`;

export const FILTER_PURCHASE_FINAL_REPOR_WITHOUT_PG = `${BASE_URL}/summary_report/api/v1/purchase_final_summary_report_wp/filter/`;

// Purchage Req Report

export const FILTER_PURCHASE_REQ_REPORT = `${BASE_URL}/summary_report/api/v1/purchase_request_summary_report/filter/`;

export const FILTER_PURCHASE_REQ_REPORT_WITHOUT_PG = `${BASE_URL}/summary_report/api/v1/purchase_request_summary_report_wp/filter/`;

// Purchase Return Report

export const FILTER_PURCHASE_RETURN_REPORT = `${BASE_URL}/purchase_item_return/api/v1/purchase_return_item/report/`;

export const FILTER_PURCHASE_RETURN_REPOR_WITHOUT_PG = `${BASE_URL}/purchase_item_return/api/v1/purchase_return_item/report_wp/`;

// Sell Return Report

export const FILTER_SELL_RETURN_REPORT = `${BASE_URL}/orderitem_return/api/v1/orderitem_return/report/`;

export const FILTER_SELL_RETURN_REPOR_WITHOUT_PG = `${BASE_URL}/orderitem_return/api/v1/orderitem_return/report_wp/`;

// Salary Ledger Report

export const FILTER_EMPLOYEE_SALARY_LEDGER_REPORT = `${BASE_URL}/salary_payment/api/v1/salary_payment/employee_salary_ledger_report/`;

export const FILTER_EMPLOYEE_SALARY_LEDGER_REPORT_WITHOUT_PG = `${BASE_URL}/salary_payment/api/v1/salary_payment/employee_salary_ledger_report_wp/`;

// Salary Report

export const FILTER_EMPLOYEE_SALARY_REPORT = `${BASE_URL}/salary_payment/api/v1/salary_payment/salary_report/`;

export const FILTER_EMPLOYEE_SALARY_REPORT_WITHOUT_PG = `${BASE_URL}/salary_payment/api/v1/salary_payment/salary_report_wp/`;

// Salary Pyament Report

export const FILTER_EMPLOYEE_SALARY_PAYMENT_REPORT = `${BASE_URL}/salary_payment/api/v1/salary_payment/salary_payment_report/`;

export const FILTER_EMPLOYEE_SALARY_PAYMENT_REPORT_WITHOUT_PG = `${BASE_URL}/salary_payment/api/v1/salary_payment/salary_payment_report_wp/`;

// Salary Slip Report

export const FILTER_EMPLOYEE_SALARY_SLIP_REPORT = `${BASE_URL}/salary_payment/api/v1/salary_payment/payslip_for_employee/`;

export const FILTER_EMPLOYEE_SALARY_SLIP_REPORT_WITHOUT_PG = `${BASE_URL}/salary_payment/api/v1/salary_payment/payslip_for_employee_wp/`;

//Profit Loss summary Report
export const PROFITLOSSINCOME_FILTER_BY = `${BASE_URL}/account_report/api/v1/account_report/net_profit_and_loss_report/`;

export const PROFITLOSSINCOME_FILTER_WITHOUT_PG = `${BASE_URL}/account_report/api/v1/account_report/net_profit_and_loss_report/`;

//DashBoard
// Get order count paid
export const GET_ORDERS_COUNT_PAID = `${BASE_URL}/dashboard/api/v1/get_total_count_of_paid_orders/`;

// Get order count un-paid
export const GET_ORDERS_COUNT_UNPAID = `${BASE_URL}/dashboard/api/v1/get_total_count_of_unpaid_orders/`;
// Get total registered customer
export const GET_REGISTERED_CUSTOMERS = `${BASE_URL}/dashboard/api/v1/get_total_count_of_customers/`;
// Get total stock
export const GET_TOTAL_LOW_STOCKS = `${BASE_URL}/dashboard/api/v1/get_total_count_of_low_stock_products/`;
// Get chart for order
export const GET_ORDERS_CHART = `${BASE_URL}/dashboard/api/v1/get_counting_of_order_per_day/`;
// Get total stock
export const GET_TOTAL_LATEST_ORDERS = `${BASE_URL}/dashboard/api/v1/get_all_latest_orders/`;
// Get total orders
export const GET_TOTAL_ORDERS = `${BASE_URL}/dashboard/api/v1/get_order_totals/`;
// Get total Incomplete orders
export const GET_TOTAL_INCOMPLETE_ORDERS = `${BASE_URL}/dashboard/api/v1/get_all_incomplete_orders/`;
// Get Employee
export const GET_ALL_EMPLOYEE_WITHOUT_PAGINATION = `${BASE_URL}/employee/api/v1/employee/without_paginaiton/all/`;
// Get total paid orders
export const GET_ALL_PAID_ORDERS = `${BASE_URL}/dashboard/api/v1/get_all_paid_orders/`;
// Get total unPaid orders
export const GET_ALL_UNPAID_ORDERS = `${BASE_URL}/dashboard/api/v1/get_all_unpaid_orders/`;
// Get total low stocks
export const GET_ALL_LOW_STOCK_PRODUCTS = `${BASE_URL}/dashboard/api/v1/get_all_low_stock_products/                  `;
export const GET_ALL_CATEGORIZED_ORDER_ITEM_COUNT = `${BASE_URL}/dashboard/api/v1/get_all_categorized_order_item_count/`;
// Get Best seller Amount
export const GET_BEST_SELLER_AMOUNT = `${BASE_URL}/dashboard/api/v1/get_best_seller_by_amount/`;
// Get Best seller Quantity
export const GET_BEST_SELLER_QUANTITY = `${BASE_URL}/dashboard/api/v1/get_best_seller_by_quantity/`;

// OTP
export const GET_PHONE_VARIFICATION_CODE_BY_CUSTOMER = `${BASE_URL}/customer/api/v1/customer/verify_primary_phone/`;
export const GET_PHONE_VARIFICATION_CODE_BY_VENDOR = `${BASE_URL}/vendor/api/v1/vendor/verify_primary_phone/`;

// Email for invoice
export const SEND_ORDER_EMAIL = `${BASE_URL}/customerorder/api/v1/customerorder/send_order_invoice_via_mail/`;
// sms for confirmation
export const SEND_CONFIRMATION_SMS = `${BASE_URL}/customerorder/api/v1/customerorder/send_custom_sms/`;

// Notification
export const GET_NOTIFICATIONS_ALL = `${BASE_URL}/notifications/api/v1/notifications/all/`;

export const DELETE_NOTIFICATION_BY_ID = `${BASE_URL}/notifications/api/v1/notifications/delete/`;

// some error hangaleing but not api
export const GET_ALL_MESSAGES_BY_ID = `${BASE_URL}///`;
export const POST_MESSAGES = `${BASE_URL}///`;

//paymentSalary
export const CREATE_PAYMENTSALARY = `${BASE_URL}/salary_payment/api/v1/salary_payment/create/`;

export const CREATE_ADVANCED_PAYMENTSALARY = `${BASE_URL}/salary_payment/api/v1/advance_salary_payment/create/`;

export const GET_PAYMENTSALARYS = `${BASE_URL}/salary_payment/api/v1/salary_payment/all`;

export const GET_PAYMENTSALARY_BY_ID = `${BASE_URL}/salary_payment/api/v1/salary_payment/`;

export const GET_PAYMENTSALARY_BY_INVOICE = `${BASE_URL}/salary_payment/api/v1/salary_payment/salary_payment_by_invoice_no/`;

export const GET_PAYMENTSALARY_AMOUNT = `${BASE_URL}/salary_payment/api/v1/salary_payment/get_salary_amount/`;

export const UPDATE_PAYMENTSALARY = `${BASE_URL}/salary_payment/api/v1/salary_payment/update/`;

export const DELETE_PAYMENTSALARY = `${BASE_URL}/salary_payment/api/v1/salary_payment/delete/`;

export const DELETE_PAYMENTSALARY_MULTIPLE = `${BASE_URL}/salary_payment/api/v1/salary_payment/delete_multiple/`;

export const SEARCH_SALARY_PAYMENT = `${BASE_URL}/salary_payment/api/v1/salary_payment/search/`;

export const GENERATE_SALARY_PAYMENT = `${BASE_URL}/salary_payment/api/v1/salary_payment/generate/`;

export const READY_TO_PAYMENT_SALARY_EMPOLOYEE_LIST = `${BASE_URL}/salary_payment/api/v1/salary_payment/employee_list_for_salary_payment/`;

// PF Payment
export const GET_PF_ALL = `${BASE_URL}/provident_fund/api/v1/provident_fund/all/`;

export const SEARCH_PF_PAYMENT = `${BASE_URL}/provident_fund/api/v1/provident_fund/search/`;

export const CREATE_PAYMENTPF = `${BASE_URL}/provident_fund/api/v1/provident_fund/return_provident_fund/`;
