import i18next from 'i18next';
import ar from './navigation-i18n/ar';
import en from './navigation-i18n/en';
import tr from './navigation-i18n/tr';

i18next.addResourceBundle('en', 'navigation', en);
i18next.addResourceBundle('tr', 'navigation', tr);
i18next.addResourceBundle('ar', 'navigation', ar);

const navigationConfig = [
	// Dashboard
	{
		id: 'dashboards',
		title: 'Dashboards',
		translate: 'DASHBOARDS',
		type: 'item',
		icon: 'dashboard',
		url: '/apps/dashboards/project'
	},
	// e-commerce
	{
		id: 'e-commerce',
		title: 'E-Commerce',
		translate: 'E-Commerce',
		type: 'collapse',
		icon: 'shopping_cart',
		exact: true,
		children: [
			{
				id: 'e-commerce-products',
				title: 'Products',
				type: 'item',
				url: '/apps/e-commerce/products',
				exact: true
			},

			{
				id: 'orders',
				title: 'Orders',
				translate: 'Orders',
				type: 'item',
				url: '/apps/order-managements/orders',
				exact: true
			},
			// purchase request
			{
				id: 'purchaserequest',
				title: 'purchaserequest',
				translate: 'Purchase Requests',
				type: 'item',
				url: '/apps/purchase-management/purchase-requests',
				exact: true
			},

			// purchase final
			{
				id: 'purchasefinal',
				title: 'purchasefinal',
				translate: 'Purchase Final',
				type: 'item',
				url: '/apps/purchasefinal-management/purchase-final',
				exact: true
			},
			// purchase return
			{
				id: 'purchasereturn',
				title: 'purchasereturn',
				translate: 'Purchase Return',
				type: 'item',
				url: '/apps/purchasereturn-management/purchasereturn',
				exact: true
			},
			// sell return
			{
				id: 'sellreturn',
				title: 'sellreturn',
				translate: 'Sell Return',
				type: 'item',
				url: '/apps/sellreturn-management/sellreturn',
				exact: true
			},

			// settings
			{
				id: 'Settings',
				title: 'E-Commerce Settings',
				translate: 'E-Commerce Settings',
				type: 'collapse',
				icon: 'settings_applications',
				exact: true,
				children: [
					// categories
					{
						id: 'categories',
						title: 'Categories',
						translate: 'Categories',
						type: 'item',
						url: '/apps/categories-management/categories'
					},
					// brands
					{
						id: 'brands',
						title: 'Brands',
						translate: 'Brands',
						type: 'item',
						url: '/apps/brand-management/brands',
						exact: true
					},
					// manufacturers
					{
						id: 'manufacturers',
						title: 'Manufacturers',
						translate: 'Manufacturers',
						type: 'item',
						url: '/apps/manufacturer-management/manufacturers',
						exact: true
					},
					//Colors
					{
						id: 'Colors',
						title: 'Colors',
						translate: 'Product Colors',
						type: 'item',
						url: '/apps/color-management/colors',
						exact: true
					},
					//Size
					{
						id: 'Sizes',
						title: 'Sizes',
						translate: 'Product Sizes',
						type: 'item',
						url: '/apps/size-management/sizes',
						exact: true
					},
					//discount
					{
						id: 'discount',
						title: 'Discount',
						translate: 'Discount',
						type: 'item',
						url: '/apps/discount-management/discounts',
						exact: true
					},
					// Billing Address
					{
						id: 'billingAddress',
						title: 'billingAddress',
						translate: 'Billing Address',
						type: 'item',
						url: '/apps/shippingAddress-management/shippingAddresses',
						exact: true
					}
				]
			}
		]
	},
	//employee
	{
		id: 1,
		title: 'Employee Management',
		translate: 'Employee Management',
		type: 'collapse',
		icon: 'supervised_user_circle',
		exact: true,
		children: [
			//employees
			{
				id: 2,
				title: 'Emloyees',
				translate: 'Employees',
				type: 'item',
				url: '/apps/employee-management/employees',
				exact: true
			},
			//departments
			{
				id: 3,
				title: 'Departments',
				translate: 'Departments',
				type: 'item',
				url: '/apps/department-management/departments',
				exact: true
			},

			//qualifications
			{
				id: 4,
				title: 'Qualifications',
				translate: 'Qualifications',
				type: 'item',
				url: '/apps/qualification-management/qualifications',
				exact: true
			}
		]
	},

	//setting
	{
		id: 5,
		title: 'Settings',
		translate: 'Settings',
		type: 'collapse',
		icon: 'admin_panel_settings',
		exact: true,
		children: [
			//customer
			{
				id: 'customers',
				title: 'Customers',
				translate: 'Customers',
				type: 'item',
				url: '/apps/customer-management/customers',
				exact: true
			},
			// {
			// 	id: 'vendors',
			// 	title: 'Vendors',
			// 	translate: 'Vendors',
			// 	type: 'item',
			// 	url: '/apps/vendor-management/vendors',
			// 	exact: true
			// },
			//cities
			{
				id: 6,
				title: 'Districts',
				translate: 'Districts',
				type: 'item',
				url: '/apps/city-management/cities',
				exact: true
			},
			//thanas
			{
				id: 7,
				title: 'Police Stations',
				translate: 'Police Stations',
				type: 'item',
				url: '/apps/thana-management/thanas',
				exact: true
			},
			//branch
			{
				id: 8,
				title: 'branches',
				translate: 'Branches',
				type: 'item',
				url: '/apps/branch-management/branchs',
				exact: true
			},

			//site setting
			{
				id: 9,
				title: 'siteSettings',
				translate: 'Site Settings',
				type: 'item',
				url: '/apps/sitesettings-management/sitesettings',
				exact: true
			},
			{
				id: 'slidersetting',
				title: 'sliderSetting',
				translate: 'Slider Setting',
				type: 'item',
				url: '/apps/slidersetting-management/slidersettings',
				exact: true
			},
			{
				id: 'customertypes',
				title: 'customertypes',
				translate: 'Customer Types',
				type: 'item',
				url: '/apps/customertype-management/customertypes',
				exact: true
			}
		]
	},

	//user config
	{
		id: 10,
		title: 'User Config',
		translate: 'User Config',
		type: 'collapse',
		icon: 'account_circle',
		exact: true,
		children: [
			//permissions
			{
				id: 11,
				title: 'Permissions',
				translate: 'Permissions',
				type: 'item',
				url: '/apps/permission-management/permissions',
				exact: true
			},
			//roles
			{
				id: 12,
				title: 'roles',
				translate: 'Roles',
				type: 'item',
				url: '/apps/roles-management/roles'
			},
			//users
			{
				id: 13,
				title: 'userslist',
				translate: 'User List',
				type: 'item',
				url: '/apps/users-management/userslist',
				exact: true
			},
			//menu
			{
				id: 14,
				title: 'Menus',
				translate: 'Menus',
				type: 'item',
				url: '/apps/menu-management/menus',
				exact: true
			},
			//roleMenu
			{
				id: 15,
				title: 'RoleMenus',
				translate: 'RoleMenus',
				type: 'item',
				url: '/apps/roleMenu-management/roleMenus',
				exact: true
			}
		]
	},
	//Acounts
	{
		id: 'accounts',
		title: 'Acounts',
		translate: 'Acounts',
		type: 'collapse',
		icon: 'account_balance',
		exact: true,

		children: [
			//group
			{
				id: 'groups',
				title: 'Groups',
				translate: 'Groups',
				type: 'item',
				url: '/apps/group-management/groups',
				exact: true
			},
			//ledger
			{
				id: 'ledgers',
				title: 'Ledgers',
				translate: 'Ledgers',
				type: 'item',
				url: '/apps/ledger-management/ledgers',
				exact: true
			},
			//subLedger
			{
				id: 'subLedgers',
				title: 'SubLedgers',
				translate: 'SubLedgers',
				type: 'item',
				url: '/apps/subLedger-management/subLedgers',
				exact: true
			},
			{
				id: 'receivable_bill',
				title: 'Receivable Bill',
				translate: 'Receivable Bill',
				type: 'item',
				url: '/apps/accounts/receivable_bills',
				exact: true
			},
			{
				id: 'payable_bill',
				title: 'Payable Bill',
				translate: 'Payable Bill',
				type: 'item',
				url: '/apps/accounts/payable_bills',
				exact: true
			},
			{
				id: 'paymentvoucher',
				title: 'PaymentVoucher',
				translate: 'Payment Voucher',
				type: 'item',
				url: '/apps/accounts/paymentvouchers',
				exact: true
			},
			{
				id: 'receiptvoucher',
				title: 'ReceiptVoucher',
				translate: 'Receipt Voucher',
				type: 'item',
				url: '/apps/receiptVoucher-management/receiptVouchers',
				exact: true
			},
			//contra
			{
				id: 'contras',
				title: 'Contras',
				translate: 'Contras',
				type: 'item',
				url: '/apps/contra-management/contras',
				exact: true
			},
			//journal
			{
				id: 'journals',
				title: 'Journals',
				translate: 'Journals',
				type: 'item',
				url: '/apps/journal-management/journals',
				exact: true
			}
		]
	},

	// CHATS
	{
		id: 'chats',
		title: 'Chats',
		translate: 'Chats',
		type: 'item',
		icon: 'chat',
		url: '/apps/chat-management/chats'
	},
	// CHATS
	{
		id: 'chat',
		title: 'Chat',
		translate: 'Chat',
		type: 'item',
		icon: 'chat',
		url: '/apps/chat'
	},
	//payroll
	{
		id: 'MainPayroll',
		title: 'PayRoll',
		translate: 'PayRoll',
		type: 'collapse',
		icon: 'account_tree',
		exact: true,
		children: [
			//timetable
			{
				id: 'shiftImetable',
				title: 'Shift Timetable',
				translate: 'Shift Timetable',
				type: 'item',
				url: '/apps/timetables-management',
				exact: true
			},
			//Shift
			{
				id: 'Shift',
				title: 'Shift',
				translate: 'Shift',
				type: 'item',
				url: '/apps/shifts-management',
				exact: true
			},
			//Schedule
			{
				id: 'Schedule',
				title: 'Schedule',
				translate: 'Employee Schedule ',
				type: 'item',
				url: '/apps/schedules-management',
				exact: true
			},
			//Pyahead
			{
				id: 'Pyahead',
				title: 'Pyahead',
				translate: 'Pyahead',
				type: 'item',
				url: '/apps/pyahead-management/pyaheads',
				exact: true
			},
			//Userdefinevalue
			{
				id: 'Userdefinevalue',
				title: 'User Define Value',
				translate: 'User Define Value',
				type: 'item',
				url: '/apps/userdefinevalue-management/userdefinevalues',
				exact: true
			},

			//Assign Payhead
			{
				id: 'AssignPayhead',
				title: 'Assign Payhead',
				translate: 'Assign Payhead',
				type: 'item',
				url: '/apps/salary-management/salarys',
				exact: true
			},
			//Attendance Vouchertype
			{
				id: 'leaveApplication',
				title: 'Leave Application',
				translate: 'Leave Application',
				type: 'item',
				url: '/apps/application-management/applications',
				exact: true
			},
			//Voucher
			{
				id: 'Voucher',
				title: 'Payroll Voucher',
				translate: 'Payroll Voucher',
				type: 'item',
				url: '/apps/voucher-managements/vouchers',
				exact: true
			},
			//SalaryPayment
			{
				id: 'SalaryPayment',
				title: ' Payment Salary',
				translate: 'Payment Salary ',
				type: 'item',
				url: '/apps/paymentSalary-management/paymentSalarys',
				exact: true
			},
			//PF
			{
				id: 'PFPayment',
				title: 'Payment Provident Fund ',
				translate: 'Payment Provident Fund',
				type: 'item',
				url: '/apps/paymentPF-management/paymenPFs',
				exact: true
			},

			//payroll setting
			{
				id: 'payrollSettint',
				title: 'PayRoll Setting',
				translate: 'PayRoll Setting',
				type: 'collapse',
				icon: 'settings',
				exact: true,
				children: [
					//Unit
					{
						id: 'Unit',
						title: 'Unit',
						translate: 'Unit',
						type: 'item',
						url: '/apps/unit-management/units',
						exact: true
					},

					//ComputeInformation Information
					// {
					// 	id: 'ComputeInformation',
					// 	title: 'Compute Information',
					// 	translate: 'Compute Information',
					// 	type: 'item',
					// 	url: '/apps/information-management/informations',
					// 	exact: true
					// },

					//Compute
					{
						id: 'Compute',
						title: 'Compute',
						translate: 'Compute',
						type: 'item',
						url: '/apps/compute-management/computes',
						exact: true
					},

					//Payheadtype
					{
						id: 'Payheadtype',
						title: 'Payhead Type',
						translate: 'Payhead Type',
						type: 'item',
						url: '/apps/payheadtype-management/payheadtypes',
						exact: true
					},
					//Attendancetype
					{
						id: 'Attendancetype',
						title: 'Attendance Type',
						translate: 'Attendance Type',
						type: 'item',
						url: '/apps/attendancetype-management/attendancetypes',
						exact: true
					},
					//Calculationtype
					{
						id: 'Calculationtype',
						title: 'Calculation Type',
						translate: 'Calculation Type',
						type: 'item',
						url: '/apps/calculationtype-management/calculationtypes',
						exact: true
					}
					// //Vouchertype
					// {
					// 	id: 'Vouchertype',
					// 	title: 'Payroll Voucher Type',
					// 	translate: 'Payroll Voucher Type',
					// 	type: 'item',
					// 	url: '/apps/vouchertype-management/vouchertypes',
					// 	exact: true
					// },
					// //Vouchertypeclass
					// {
					// 	id: 'Vouchertype',
					// 	title: 'Voucher Type Class',
					// 	translate: 'Voucher Type Class',
					// 	type: 'item',
					// 	url: '/apps/vouchertypeclass-management/vouchertypeclasss',
					// 	exact: true
					// }
				]
			},
			// //payroll report
			{
				id: 'payrollReport',
				title: 'PayRoll Report',
				translate: 'PayRoll Report',
				type: 'collapse',
				icon: 'report sharp',
				exact: true,
				children: [
					// Salary Leger Payment Report
					{
						id: 'salaryPaymentLedgerReport',
						title: 'Salary Payment Ledger Report',
						translate: 'Salary Payment Ledger Report',
						type: 'item',
						url: '/apps/report-management/employee-salary-ledger-reports',
						exact: true
					},
					// Salary  Report
					{
						id: 'salaryReport',
						title: 'Salary  Report',
						translate: 'Salary Report',
						type: 'item',
						url: '/apps/report-management/employee-salary-reports',
						exact: true
					},
					// Salary Payment Report
					{
						id: 'salaryPaymentReport',
						title: 'Salary Payment Report',
						translate: 'Salary Payment Report',
						type: 'item',
						url: '/apps/report-management/employee-salary-payment-reports',
						exact: true
					},
					// Salary Slip  Report
					{
						id: 'salarySlipReport',
						title: 'Salary Slip Report',
						translate: 'Salary Slip Report',
						type: 'item',
						url: '/apps/report-management/employee-salary-slip-reports',
						exact: true
					},
					//Attendance Calculation
					{
						id: 'Attendance',
						title: 'Attendance Calculation',
						translate: 'Attendance Calculation',
						type: 'item',
						url: '/apps/attendance-management/attendance-reports',
						exact: true
					},
					//Attendance Summary
					{
						id: 'AttendanceSummary',
						title: 'Attendance Summary Report',
						translate: 'Attendance Summary Report',
						type: 'item',
						url: '/apps/attendancesummary-management/attendancesummary-reports',
						exact: true
					}
				]
			}
		]
	},
	{
		id: 'deviceSetting',
		title: 'Attendance Machine',
		translate: 'Attendance Machine',
		type: 'collapse',
		icon: 'devices_other',
		exact: true,
		children: [
			//attendance
			{
				id: 'Import_Schedule',
				title: 'Import Employee Attendance',
				translate: 'Import Employee Attendance',
				type: 'item',
				url: '/apps/import-schedule-management/',
				exact: true
			},
			//Upload-attendance
			{
				id: 'Upload_Schedule',
				title: 'Upload & Download Employee info',
				translate: 'Upload & Download Employee info',
				type: 'item',
				url: '/apps/upload-employee-schedule-management/',
				exact: true
			},

			//Device
			{
				id: 'Device',
				title: 'Device',
				translate: 'Device',
				type: 'item',
				url: '/apps/device-management/devices',
				exact: true
			}
		]
	},

	//support
	{
		id: 'supportTicket',
		title: 'supportTicket',
		translate: 'Support Ticket',
		type: 'collapse',
		icon: 'support_agent',
		children: [
			{
				id: 'newsupport',
				title: 'newsupport',
				translate: 'Supports',
				type: 'item',
				url: '/apps/support-management/supports',
				exact: true
			},
			{
				id: 'newsale',
				title: 'newsale',
				translate: 'Sales',
				type: 'item',
				url: '/apps/sale-management/sales',
				exact: true
			},
			{
				id: 'newaccount',
				title: 'newaccount',
				translate: 'Accounts',
				type: 'item',
				url: '/apps/acccount-management/accounts',
				exact: true
			}
		]
	},
	//Report
	{
		id: 'report',
		title: 'Reports',
		translate: 'Reports',
		type: 'collapse',
		icon: 'report sharp',
		exact: true,
		children: [
			//acount reports
			{
				id: 'accountReport',
				title: 'Account Reports',
				translate: 'Account Reports',
				type: 'collapse',
				icon: 'account_balance_wallet',
				url: '',
				exact: true,
				children: [
					// receipt
					{
						id: 'receiptreport',
						title: 'Receipt Report',
						translate: 'Receipt Report',
						type: 'item',
						url: '/apps/report-management/receipt-reports',
						exact: true
					},
					// receipt summary
					{
						id: 'receiptsummaryreport',
						title: 'Receipt Summary Report',
						translate: 'Receipt Summary Report',
						type: 'item',
						url: '/apps/report-management/receipt-summary-reports',
						exact: true
					},
					// payment
					{
						id: 'paymentreport',
						title: 'Payment Report',
						translate: 'Payment Report',
						type: 'item',
						url: '/apps/report-management/payment-reports',
						exact: true
					},
					// payment summary
					{
						id: 'paymentsummaryreport',
						title: 'Payment Summary Report',
						translate: 'Payment Summary Report',
						type: 'item',
						url: '/apps/report-management/payment-summary-reports',
						exact: true
					},
					// ledger
					{
						id: 'ledgerreport',
						title: 'Ledger Report',
						translate: 'Ledger Report',
						type: 'item',
						url: '/apps/report-management/ledger-reports',
						exact: true
					},
					// account statement
					{
						id: 'accountstatementreport',
						title: 'Account Statement Report',
						translate: 'Account Statement Report',
						type: 'item',
						url: '/apps/report-management/account-statement-reports',
						exact: true
					},
					// account statement summary
					{
						id: 'accountSummaryReport',
						title: 'Account Statement Summary Report',
						translate: 'Account Statement Summary Report',
						type: 'item',
						url: '/apps/report-management/account-statement-summary-reports',
						exact: true
					}
				]
			},
			{
				id: 'eCommerceReport',
				title: 'eCommerce Reports',
				translate: 'E-Commerce Reports',
				type: 'collapse',
				icon: 'shopping_basket',
				url: '',
				exact: true,
				children: [
					{
						id: 'orderreport',
						title: 'Order Report',
						translate: 'Order Report',
						type: 'item',
						url: '/apps/report-management/order-reports',
						exact: true
					},
					{
						id: 'ordersummaryreport',
						title: 'Order Summary Report',
						translate: 'Order Summary Report',
						type: 'item',
						url: '/apps/report-management/order-summary-reports',
						exact: true
					},
					{
						id: 'stockreport',
						title: 'Stock Report',
						translate: 'Stock Report',
						type: 'item',
						url: '/apps/report-management/stocks-reports',
						exact: true
					},
					{
						id: 'lowstockreport',
						title: 'Low Stock Report',
						translate: 'Low Stock Report',
						type: 'item',
						url: '/apps/report-management/low-stocks-reports',
						exact: true
					},
					{
						id: 'outofstockreport',
						title: 'Out Of Stock Report',
						translate: 'Out of Stock Report',
						type: 'item',
						url: '/apps/report-management/out_of-stocks-reports',
						exact: true
					},
					{
						id: 'purchagereport',
						title: 'Purchase Final Report',
						translate: 'Purchase Final Report',
						type: 'item',
						url: '/apps/report-management/purchage-reports',
						exact: true
					},
					// {
					// 	id: 'purchagesummaryreport',
					// 	title: 'Purchase Final Report',
					// 	translate: 'Purchase Final Report',
					// 	type: 'item',
					// 	url: '/apps/report-management/purchage-summary-reports',
					// 	exact: true
					// },

					{
						id: 'purchagereqrsummaryeport',
						title: 'Purchase Req Report',
						translate: 'Purchase Req Report',
						type: 'item',
						url: '/apps/report-management/purchage-req-summary-reports',
						exact: true
					},
					{
						id: 'purchageReturnreport',
						title: 'Purchase Return Report',
						translate: 'Purchase Return Report',
						type: 'item',
						url: '/apps/report-management/purchage-return-reports',
						exact: true
					},
					{
						id: 'sellReturnreport',
						title: 'Sell Return Report',
						translate: 'Sell Return Report',
						type: 'item',
						url: '/apps/report-management/sell-return-reports',
						exact: true
					},
					{
						id: 'ProfitLossIncome',
						title: 'Profit Loss Rpt',
						translate: 'Profit Loss  Rpt',
						type: 'item',
						url: '/apps/report-management/profit-loss-reports',
						exact: true
					},
					{
						id: 'grossProfit',
						title: 'Gross Profit Loss Rpt',
						translate: 'Gross Profit Loss Rpt',
						type: 'item',
						url: '/apps/report-management/gross-profit-reports',
						exact: true
					}
				]
			}
		]
	}
];

export default navigationConfig;
