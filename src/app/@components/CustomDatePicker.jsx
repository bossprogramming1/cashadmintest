import { KeyboardDatePicker } from '@material-ui/pickers';
import moment from 'moment';
import React from 'react';
import { useFormContext } from 'react-hook-form';

function CustomDatePicker(props) {
	const methods = useFormContext();
	const { formState } = methods;
	const { errors } = formState;
	// For Disable dates before today
	const today = moment();
	const iconButtonStyles = {
		// Example of inline styles for IconButton
		root: {
			padding: 0
			// Add other styles as needed
		}
	};
	const dateFormatter = str => {
		return str;
	};
	// Use the current date as the default value
	let defaultValue = props.value || props.field.value || today.toDate();

	if (props.views && moment(defaultValue).date() !== 1 && props.voucherOfDate === true) {
		// If views is enabled and the current date is not the first of the month
		defaultValue = moment(defaultValue).startOf('month').toDate();
	}
	if (props.views && moment(defaultValue).date() !== 1 && props.startOfDate === true) {
		// If views is enabled and the current date is not the first of the month
		defaultValue = moment(defaultValue).startOf('month').toDate();
	}
	if (
		props.views &&
		moment(defaultValue).date() !== moment(defaultValue).daysInMonth() &&
		props.startOfDate === false
	) {
		// If views is enabled and the current date is not the last day of the month
		defaultValue = moment(defaultValue).endOf('month').toDate();
	}

	const { field, views, ...otherProps } = props;

	console.log('props.previous_date_disable', props.previous_date_disable);

	return (
		<KeyboardDatePicker
			{...props.field}
			{...props}
			{...otherProps}
			className={props?.className || 'mt-8 mb-16 w-full'}
			autoOk
			required={!!props.required}
			variant="inline"
			inputVariant="outlined"
			format={
				props?.format || props.previous_date_disable
					? 'dd-MM-yy'
					: props.startOfDate === false
					? 'MMMM, yyyy'
					: props.voucherOfDate === true
					? 'MMMM, yyyy'
					: 'dd/MM/yyyy'
			}
			placeholder={props?.placeholder || props.previous_date_disable ? 'YY-MM-DD' : 'dd/MM/yyyy'}
			// value={props.value || props.field.value || null}
			value={defaultValue}
			error={!!errors[props.field?.name] || props.required ? !(props.value || props.field.value) : false}
			helperText={errors[props.field?.name]?.message || ''}
			onChange={value => {
				value ? props.field.onChange(moment(new Date(value)).format('YYYY-MM-DD')) : props.field.onChange('');
				props?.onChange && props?.onChange(value);
			}}
			InputAdornmentProps={{ position: 'start' }}
			minDate={props.previous_date_disable ? today.toDate() : false} // Disable dates before today
			KeyboardButtonProps={{
				style: iconButtonStyles.root
			}}
			rifmFormatter={dateFormatter}
			views={views}
			InputLabelProps={props.field.value ? { shrink: true } : { style: { color: 'red' } }}
		/>
	);
}

export default CustomDatePicker;
