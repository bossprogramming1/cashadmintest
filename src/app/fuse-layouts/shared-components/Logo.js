/* eslint-disable import/no-duplicates */
import { makeStyles } from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';
import { BASE_URL, GET_SITESETTINGS } from 'app/constant/constants';
import clsx from 'clsx';
import { useEffect } from 'react';
import { useState } from 'react';

const useStyles = makeStyles(theme => ({
	root: {
		'& .logo-icon': {
			transition: theme.transitions.create(['width', 'height'], {
				duration: theme.transitions.duration.shortest,
				easing: theme.transitions.easing.easeInOut
			})
		},
		'& .react-badge, & .logo-text': {
			transition: theme.transitions.create('opacity', {
				duration: theme.transitions.duration.shortest,
				easing: theme.transitions.easing.easeInOut
			})
		}
	},
	reactBadge: {
		backgroundColor: '#121212',
		color: '#61DAFB'
	}
}));

function Logo() {
	const classes = useStyles();
	const [generalData, setGeneralData] = useState({});

	//get general setting data
	useEffect(() => {
		const authTOKEN = {
			headers: {
				'Content-type': 'application/json',
				Authorization: localStorage.getItem('jwt_access_token')
			}
		};

		fetch(`${GET_SITESETTINGS}`, authTOKEN)
			.then(response => response.json())
			.then(data => setGeneralData(data.general_settings[0] || {}))
			.catch(() => setGeneralData({}));
	}, []);
	return (
		<div className={clsx(classes.root, 'flex items-center')}>
			<img
				style={{ marginTop: '6.4rem' }}
				className="logo-icon w-50"
				src={`${BASE_URL}${generalData?.logo}`}
				alt="logo"
			/>
			{/* <Typography className="text-16 leading-none mx-12 font-medium logo-text" color="inherit">
				CASH CONNECT
			</Typography> */}
		</div>
	);
}

export default Logo;
