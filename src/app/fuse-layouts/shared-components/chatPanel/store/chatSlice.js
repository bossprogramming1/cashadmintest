import { createAsyncThunk, createSlice } from '@reduxjs/toolkit';
import jsonToFormData from 'app/@helpers/jsonToFormData';
import { GET_ALL_MESSAGES_BY_ID, POST_MESSAGES } from 'app/constant/constants';
import axios from 'axios';
import { setSelectedContactId } from './contactsSlice';
import { closeChatPanel } from './stateSlice';

export const getChat = createAsyncThunk('chatPanel/chat/getChat', async ({ contactId }, { dispatch, getState }) => {
	const { id } = getState().chatPanel.user;

	const response = await axios.get(`${GET_ALL_MESSAGES_BY_ID}${contactId}`);

	const { messages } = await response.data;
	const dialog = [];
	messages.map(chat => {
		dialog.push({
			who: chat.id,
			message: chat.message,
			time: chat.updated_at,
			file: chat.file
		});
	});
	const chat = {
		id,
		dialog
	};
	const chatList = [];
	messages.map(chat => {
		chatList.push({
			chatId: chat.sender,
			contactId,
			file: chat.file

			// lastMessageTime: chat.created_at,
		});
	});

	dispatch(setSelectedContactId(contactId));
	// dispatch(updateUserChatList(chatList));

	return chat;
});

export const sendMessage = createAsyncThunk(
	'chatPanel/chat/sendMessage',
	async ({ messageText, file, contactId }, { dispatch, getState }) => {
		const formData = jsonToFormData({ message: messageText, receiver: contactId, file });
		const response = await axios.post(POST_MESSAGES, formData);

		const chat = await response.data;

		const massage = chat?.id
			? {
					who: chat.sender,
					message: chat.message,
					time: chat.updated_at,
					file: chat.file
			  }
			: null;
		return massage;
	}
);

const chatSlice = createSlice({
	name: 'chatPanel/chat',
	initialState: null,

	reducers: {
		removeChat: (state, action) => null
	},
	extraReducers: {
		[getChat.fulfilled]: (state, action) => action.payload,
		[sendMessage.fulfilled]: (state, action) => {
			if (action.payload) {
				state.dialog = [...state.dialog, action.payload];
			} else {
				state.dialog = [...state.dialog];
			}
		},
		[closeChatPanel]: (state, action) => null
	}
});

export const { removeChat } = chatSlice.actions;

export default chatSlice.reducer;
