import { createAsyncThunk, createSlice } from '@reduxjs/toolkit';
import useUserInfo from 'app/@customHooks/useUserInfo';
import axios from 'axios';
import { BASE_URL, USER_BY_ID } from '../../../../constant/constants';

export const getUserData = createAsyncThunk('chatPanel/user/getUserData', async userId => {
	const { authToken } = useUserInfo();

	const response = await axios.get(`${USER_BY_ID}`, authToken);
	const user = await response.data;
	return {
		id: user.id,
		name: `${user.first_name || ''} ${user.last_name}`,
		avatar: user.image ? `${BASE_URL}${user.image}` : null,
		role: user.role ? user.role?.name : 'Guest',
		status: 'online',
		mode: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit',
		chatList: []
	};
});

export const updateUserData = createAsyncThunk('chatPanel/user/updateUserData', async newData => {
	const response = await axios.post('/api/chat/user/data', newData);
	const data = await response.data;

	return data;
});

const userSlice = createSlice({
	name: 'chatPanel/user',
	initialState: null,
	reducers: {
		updateUserChatList: (state, action) => {
			state.chatList = action.payload;
		}
	},
	extraReducers: {
		[getUserData.fulfilled]: (state, action) => action.payload,
		[updateUserData.fulfilled]: (state, action) => action.payload
		// [getUnreadMessageData.fulfilled]: (state, action) => action.payload
	}
});

export const { updateUserChatList } = userSlice.actions;

export default userSlice.reducer;
