/* eslint-disable import/named */
import { createAsyncThunk, createEntityAdapter, createSlice } from '@reduxjs/toolkit';
import { BASE_URL, UNREAD_MESSAGES_WITH_ALL_USERS } from 'app/constant/constants';
import axios from 'axios';
import { closeChatPanel } from './stateSlice';

export const getContacts = createAsyncThunk('chatPanel/contacts/getContacts', async contactId => {
	const authTOKEN = {
		headers: {
			'Content-type': 'application/json',
			Authorization: localStorage.getItem('jwt_access_token')
		}
	};
	const response = await axios.get(`${UNREAD_MESSAGES_WITH_ALL_USERS}${contactId}`, authTOKEN);
	const data = await response.data;
	const modifiedData = [];
	// eslint-disable-next-line array-callback-return
	data?.users?.map(user => {
		modifiedData.push({
			id: user.id,
			name: `${user.first_name || ''} ${user.last_name}`,
			avatar: user.image ? `${BASE_URL}${user.image}` : null,
			status: user.is_active ? 'online' : 'offline',
			mode: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit',
			unread: user.unseen_count || ''
		});
	});

	return modifiedData;
});

const contactsAdapter = createEntityAdapter({});

export const { selectAll: selectContacts, selectById: selectContactById } = contactsAdapter.getSelectors(
	state => state.chatPanel.contacts
);

const contactsSlice = createSlice({
	name: 'chatPanel/contacts',
	initialState: contactsAdapter.getInitialState({
		selectedContactId: null
	}),
	reducers: {
		setSelectedContactId: (state, action) => {
			state.selectedContactId = action.payload;
		},
		removeSelectedContactId: (state, action) => {
			state.selectedContactId = null;
		}
	},
	extraReducers: {
		[getContacts.fulfilled]: contactsAdapter.setAll,
		[closeChatPanel]: (state, action) => {
			state.selectedContactId = null;
		}
	}
});

export const { setSelectedContactId, removeSelectedContactId } = contactsSlice.actions;

export default contactsSlice.reducer;
