/* eslint-disable func-names */
/* eslint-disable jsx-a11y/anchor-is-valid */

import useUserInfo from 'app/@customHooks/useUserInfo';
import { BASE_URL, WS_URL } from 'app/constant/constants';
import React, { useEffect, useState } from 'react';
import ScrollableFeed from 'react-scrollable-feed';
import './Chat.css';

// let W3CWebSocket = require('websocket').w3cwebsocket;

const authTOKEN = localStorage.getItem('jwt_access_token');
const token = authTOKEN?.slice(7);
const senderIdd = localStorage.getItem('senderId');
// let ws = new WebSocket(`${WS_URL}/ws/chat/?token=${token}&conn_to=${senderIdd}`);

export default function Chat({ senderId }) {
	const [newMessage, setNewMessage] = useState('');
	const userId = localStorage.getItem('user_id');
	const [receiverImage, setReceiverImage] = useState('');
	const topImage = receiverImage ? BASE_URL + receiverImage : '/no_image.png';
	const [messageRef, setMessageRef] = useState([]);

	const { authToken } = useUserInfo();
	const token = authToken?.headers?.Authorization?.slice(7);

	const newId = [];
	newId.push(senderIdd);
	function openForm() {
		document.getElementById('myForm').style.display = 'block';
		document.getElementById('chat').style.display = 'none';
	}
	function closeForm() {
		document.getElementById('myForm').style.display = 'none';
		document.getElementById('chat').style.display = 'inline-block';
	}
	const onkeydown = e => {
		if (e.key == 'Enter') {
			handleMessageSubmit(e);
		}
	};

	// useEffect(() => {
	// 	if (senderId) {
	// 		const authTOKEN = localStorage.getItem('jwt_access_token');
	// 		const token = authTOKEN?.slice(7);
	// 		// let ws = new WebSocket(`${WS_URL}/ws/chat/?token=${token}&conn_to=${senderId}`);
	// 		openForm();
	// 		ws.onopen = function (e) {};
	// 		ws.onmessage = function (event) {
	// 			const data = JSON.parse(event.data);
	// 			const messages = data.messages;
	// 			if (data.msg_type == 'old') {
	// 				for (let message of messages) {
	// 					if (message.user.id == userId) {
	// 						document.getElementById('messages').innerHTML += `<div class=messageElemSender
	// 					><p class=sender>${message.text}</p>  <img src=${`${message.user.image ? BASE_URL : `/no_image.png`}`}${
	// 							message.user.image ? message.user.image : ''
	// 						} alt='imgae'/></div>`;
	// 					} else if (message.user.id == senderId) {
	// 						setReceiverImage(message?.user?.image);
	// 						document.getElementById('messages').innerHTML += `<div  class=messageElemReceiver>
	// 			     <img src='${BASE_URL}${message.user.image}' alt=""/>
	// 					<p class=receiver>${message.text}</p>
	// 					</div>`;
	// 					}
	// 				}
	// 			}
	// 		};
	// 	}
	// }, [senderId]);

	const handleMessageSubmit = async values => {
		values.preventDefault();
		console.log('newMessage', newMessage, values);
		// ws.send(JSON.stringify({ message: newMessage }));

		setNewMessage('');
	};
	// 	ws.onmessage = function (event) {
	// 		let message = JSON.parse(event.data);

	// 		const messages = [];
	// 		messages.push(message);

	// 		setMessageRef(messages);

	// 		if (message.msg_type == 'new') {
	// 			if (message.user.id == userId) {
	// 				document.getElementById('messages').innerHTML += `<div class=messageElemSender
	// 					><p class=sender>${message.message}</p>  <img src=${`${message.user.image ? BASE_URL : `/no_image.png`}`}${
	// 					message.user.image ? message.user.image : ''
	// 				} alt='imgae'/>

	// 				</div>`;
	// 			} else if (message.user.id == senderId) {
	// 				setReceiverImage(message?.user?.image);
	// 				document.getElementById('messages').innerHTML += `<div  class=messageElemReceiver>
	// 			     <img src='${BASE_URL}${message.user.image}' alt=""/>
	// 					<p class=receiver>${message.message}</p>

	// 					</div>`;
	// 			}
	// 		}
	// 	};
	// 	window.clearInterval(function () {
	// 		const element = document.getElementById('messages');
	// 		element.scrollTop = element?.scrollHeight;
	// 	});
	// };
	// ws.onerror = function (e) {
	// 	console.log('wsErrorSS', e);
	// };

	// ws.onopen = function (e) {
	// 	console.log('wsOpen', e);
	// };

	// ws.onclose = function (e) {
	// 	console.log('wsClose', e);
	// };

	return (
		<div>
			<button className="open_button" id="chat" onClick={openForm}>
				<svg
					className="button_icon"
					height="24"
					viewBox="0 0 24 24"
					width="24"
					xmlns="http://www.w3.org/2000/svg"
				>
					<path d="M20 2H4c-1.1 0-2 .9-2 2v18l4-4h14c1.1 0 2-.9 2-2V4c0-1.1-.9-2-2-2z"></path>
					<path d="M0 0h24v24H0z" fill="none"></path>
				</svg>
			</button>

			<div className="chat_popup" id="myForm">
				<div className="form_container">
					<div className="top_chat">
						<img src={topImage} alt="" />
						<div style={{ display: 'flex' }}>
							<a onClick={closeForm}>
								<svg height="24" viewBox="0 0 24 24" width="22" xmlns="http://www.w3.org/2000/svg">
									<g fill="none">
										<path stroke="white" stroke-width="3" d="M5 20 l15 0"></path>
									</g>
									Sorry, your browser does not support inline SVG.
								</svg>
							</a>
							<a onClick={closeForm}>
								<svg height="24" viewBox="0 0 24 24" width="24" xmlns="http://www.w3.org/2000/svg">
									<path d="M19 6.41L17.59 5 12 10.59 6.41 5 5 6.41 10.59 12 5 17.59 6.41 19 12 13.41 17.59 19 19 17.59 13.41 12z"></path>
									<path d="M0 0h24v24H0z" fill="none"></path>
								</svg>
							</a>
						</div>
					</div>
					<div id="fromValue" className="formFeild">
						<div id="messages" style={{ height: '300px' }}></div>
					</div>
					<div style={{ display: 'flex' }}>
						<textarea
							id="fromValue"
							className="textArea"
							name="message"
							autoComplete="off"
							placeholder="Type message.."
							onKeyDown={onkeydown}
							onChange={e => setNewMessage(e.target.value)}
							value={newMessage}
						/>

						<button onClick={handleMessageSubmit} type="submit" className="btn">
							<svg
								version="1.1"
								xmlns="http://www.w3.org/2000/svg"
								width="20"
								height="20"
								viewBox="0 0 500 500"
							>
								<g>
									<g>
										<polygon points="0,497.25 535.5,267.75 0,38.25 0,216.75 382.5,267.75 0,318.75"></polygon>
									</g>
								</g>
							</svg>
						</button>
					</div>
				</div>
			</div>
		</div>
	);
}
