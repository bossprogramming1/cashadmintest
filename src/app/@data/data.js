export const genders = [
	{ id: 'male', name: 'Male' },
	{ id: 'female', name: 'Female' },
	{ id: 'others', name: 'Others' }
];
export const condition = [
	{ id: 'new', name: 'New' },
	{ id: 'used', name: 'Preowned' }
];
export const payheadValue = [
	{ id: 'percentage', name: 'Percentage' },
	{ id: 'value', name: 'Value' }
];
export const discountValue = [
	{ id: 'percentage', name: 'Percentage' },
	{ id: 'value', name: 'Value' }
];
export const compound = [
	{ id: 1, name: 'Simple' },
	{ id: 2, name: 'Compound' }
];
export const incomeType = [
	{ id: 1, name: 'Fixed' },
	{ id: 2, name: 'Variable' }
];

export const maritalStatuses = [
	{ id: 'Single', name: 'Single' },
	{ id: 'Married', name: 'Married' }
];
export const deviceStatus = [
	{ id: 'disconnected', name: 'Disconnected' },
	{ id: 'connected', name: 'Connected' }
];
export const weeks = [
	{ name: 'saturday' },
	{ name: 'sunday' },
	{ name: 'monday' },
	{ name: 'tuesday' },
	{ name: 'wednesday' },
	{ name: 'thursday' },
	{ name: 'friday' }
];

export const religions = [
	{ id: 'Muslim', name: 'Muslim' },
	{ id: 'Non Muslim', name: 'Non Muslim' }
];

export const passportTypes = [
	{ id: 'Ordinary', name: 'Ordinary' },
	{ id: 'Business', name: 'Business' },
	{ id: 'Exclusive', name: 'Exclusive' }
];
export const calculationPeriod = [
	{ id: 'Days', name: 'Days' },
	{ id: 'Fortnights', name: 'Fortnights' },
	{ id: 'Months', name: 'Months' },
	{ id: 'Weeks', name: 'Weeks' }
];

export const doneNotDone = [
	{ id: 'Done', name: 'Done' },
	{ id: 'Not Done', name: 'Not Done', default: true }
];

export const medicalResults = [
	{ id: 'Fit', name: 'Fit' },
	{ id: 'UnFit', name: 'UnFit' },
	{ id: 'Meet', name: 'Meet', default: true }
];

export const activeRetrnCncl = [
	{ id: 'Active', name: 'Active', default: true },
	{ id: 'Return', name: 'Return' },
	{ id: 'Cancel', name: 'Cancel' }
];
export const booleanTypes = [
	{ id: true, name: 'Yes' },
	{ id: false, name: 'No' }
];
export const rowsPerPageOptions = [5, 10, 30, 50, 100];

export const saveAlertMsg = 'Save Success';

export const updateAlertMsg = 'Update Success';

export const removeAlertMsg = 'Remove Success';

export const imgExtensionsArr = ['.jpg', '.jpeg', '.png'];

export const allowedExtensions = [
	'.jpg',
	'.jpeg',
	'.png',
	'.PNG',
	'.JPG',
	'.JPEG',
	'.webp',
	'.doc',
	'.pdf',
	'.txt',
	'.docs',
	'.docx',
	'.xls',
	'.xlsx'
];

export const bankAndCash = [
	{ id: 'Cash', name: 'Cash' },
	{ id: 'Bank', name: 'Bank' }
];

export const ledgerCashId = 2;
export const ledgerPayableId = 96;
export const ledgerBankId = 1;
export const ledgerCashName = 'Cash';
export const ledgerBankName = 'Bank';

export const ticketfileExtension = '.jpg, .jpeg, .png, .doc, .pdf, .txt, .docs , .docx, .xls, .xlsx';

export const months = [
	'January',
	'February',
	'March',
	'April',
	'May',
	'June',
	'July',
	'August',
	'September',
	'October',
	'November',
	'December'
];
export const voucherDemoData = [
	{
		employeeName: 'Rakibul Islam',
		payheadList: [
			{ payhead: 'HRA', amount: 1500 },
			{ payhead: 'DA', amount: 1250 },
			{ payhead: 'OVER TIME', amount: 1100 },
			{ payhead: 'LEAVE', amount: 1480 },
			{ payhead: 'PF', amount: 500 },
			{ payhead: 'TF', amount: 200 },
			{ payhead: 'TA', amount: 900 }
		],
		totalPayheadAmount: 50000
	},
	{
		employeeName: 'Meheady Hasan',
		payheadList: [
			{ payhead: 'HRA', amount: 600 },
			{ payhead: 'DA', amount: 1300 },
			{ payhead: 'OVER TIME', amount: 640 },
			{ payhead: 'LEAVE', amount: 970 },
			{ payhead: 'PF', amount: 300 },
			{ payhead: 'TF', amount: 540 },
			{ payhead: 'TA', amount: 680 }
		],
		totalPayheadAmount: 47500
	},
	{
		employeeName: 'Meheady Hasan',
		payheadList: [
			{ payhead: 'HRA', amount: 600 },
			{ payhead: 'DA', amount: 1300 },
			{ payhead: 'OVER TIME', amount: 640 },
			{ payhead: 'LEAVE', amount: 970 },
			{ payhead: 'PF', amount: 300 },
			{ payhead: 'TF', amount: 540 },
			{ payhead: 'TA', amount: 680 }
		],
		totalPayheadAmount: 55000
	},
	{
		employeeName: 'Meheady Hasan',
		payheadList: [
			{ payhead: 'HRA', amount: 600 },
			{ payhead: 'DA', amount: 1300 },
			{ payhead: 'OVER TIME', amount: 640 },
			{ payhead: 'LEAVE', amount: 970 },
			{ payhead: 'PF', amount: 300 },
			{ payhead: 'TF', amount: 540 },
			{ payhead: 'TA', amount: 680 }
		],
		totalPayheadAmount: 70000
	},
	{
		employeeName: 'Meheady Hasan',
		payheadList: [
			{ payhead: 'HRA', amount: 600 },
			{ payhead: 'DA', amount: 1300 },
			{ payhead: 'OVER TIME', amount: 640 },
			{ payhead: 'LEAVE', amount: 970 },
			{ payhead: 'PF', amount: 300 },
			{ payhead: 'TF', amount: 540 },
			{ payhead: 'TA', amount: 680 }
		],
		totalPayheadAmount: 90000
	}
];
