import { createSlice } from '@reduxjs/toolkit';
import {
	DESIGNATIONS_WITHOUT_PAGINATION,
	GET_BRANCH_WITHOUT_PAGINATION,
	GET_CITYS_WITHOUT_PAGINATION,
	GET_COUNTRIES_WITHOUT_PAGINATION,
	GET_DEPARTMENTS_WITHOUT_PAGINATION,
	GET_EMPLOYEES_WITHOUT_PAGINATION,
	GET_MENUS_ALL_NESTED,
	READY_TO_PAYMENT_SALARY_EMPOLOYEE_LIST,
	GET_MENUS_WITHOUT_PAGINATION,
	GET_PERMISSIONS_WITHOUT_PAGINATION,
	GET_ROLES_WITHOUT_PAGINATION,
	GET_THANAS_WITHOUT_PAGINATION,
	GET_USERS_WITHOUT_PAGINATION,
	GROUPS_WITHOUT_PAGINATION,
	LEDGERS_WITHOUT_PAGINATION,
	PRIMARY_GROUPS_WITHOUT_PAGINATION,
	SUBLEDGERS_WITHOUT_PAGINATION,
	THANAS_BASED_CITY,
	GET_BRANDS_WITHOUT_PAGINATION,
	GET_PERENT_CATEGORIES,
	ORDERSTATUS,
	GET_CUSTOMER_WITHOUT_PAGINATION,
	GET_SUB_CATEGORIES_BY_ID,
	GET_SUB_SUB_CATEGORIES_BY_ID,
	GET_PURCHASESTATUS,
	GET_ALL_PRODUCT_WITHOUT_PG,
	PAYMENT_MATHODS,
	CUSOTMERTYPES,
	GET_All_EMPLOYEE_THROUGH_CHAT,
	GET_USER_PERMISSION,
	GET_TIMETABLES_WITHOUT_PAGINATION,
	GET_SHIFTS_WITHOUT_PAGINATION,
	GET_PAYHEAD_TYPES_WITHOUT_PAGINATION,
	GET_CALCULATION_TYPES_WITHOUT_PAGINATION,
	GET_COMPUTES_WITHOUT_PAGINATION,
	GET_PYAHEADS_WITHOUT_PAGINATION,
	GET_ATTENDANCE_TYPES_WITHOUT_PAGINATION_W_PRODUCTION,
	GET_ATTENDANCE_PRODUCTION_TYPES_WITHOUT_PAGINATION,
	GET_UNITS_WITHOUT_PAGINATION,
	GET_VOUCHER_TYPE_CLASS_LEDGER_ALL,
	GET_VOUCHER_TYPE_CLASSS_WITHOUT_PAGINATION,
	GET_PAYHEAD_ONLY_USERDEFINEVALUES,
	GET_LEDGER_ACCOUNT_CASH_AND_BANK,
	GET_DEVICE_IPS_WITHOUT_PAGINATION,
	GET_COLORS_WITHOUT_PAGINATION,
	GET_SIZES_WITHOUT_PAGINATION
} from 'app/constant/constants';
import axios from 'axios';

export const getBranches = () => dispatch => {
	fetch(GET_BRANCH_WITHOUT_PAGINATION)
		.then(response => response.json())
		.then(data => dispatch(setBranches(data.branches)))
		.catch(() => {});
};

export const getThanas = () => dispatch => {
	fetch(GET_THANAS_WITHOUT_PAGINATION)
		.then(response => response.json())
		.then(data => dispatch(setThanas(data.thanas)))
		.catch(() => {});
};
export const getThanasBasedOnCity = cityId => dispatch => {
	fetch(`${THANAS_BASED_CITY}${cityId}`)
		.then(response => response.json())
		.then(data => dispatch(setThanas(data.thanas || [])))
		.catch(() => {});
};
export const getCities = () => dispatch => {
	fetch(GET_CITYS_WITHOUT_PAGINATION)
		.then(response => response.json())
		.then(data => dispatch(setCities(data.cities)))
		.catch(() => {});
};

export const getCountries = () => dispatch => {
	fetch(GET_COUNTRIES_WITHOUT_PAGINATION)
		.then(response => response.json())
		.then(data => dispatch(setCountries(data.countries)))
		.catch(() => {});
};

export const getRoles = () => dispatch => {
	fetch(GET_ROLES_WITHOUT_PAGINATION)
		.then(response => response.json())
		.then(data => dispatch(setRoles(data.roles)))
		.catch(() => {});
};

export const getUserPermissions = () => dispatch => {
	const authTOKEN = {
		headers: {
			'Content-type': 'application/json',
			Authorization: localStorage.getItem('jwt_access_token')
		}
	};
	fetch(GET_USER_PERMISSION, authTOKEN)
		.then(response => response.json())
		.then(data => dispatch(setUserPermissions(data.user_permissions)))
		.catch(() => {});
};

export const getAllColor = () => dispatch => {
	fetch(GET_COLORS_WITHOUT_PAGINATION)
		.then(response => response.json())
		.then(data => dispatch(setColors(data?.colors)));
};
export const getAllSize = () => dispatch => {
	fetch(GET_SIZES_WITHOUT_PAGINATION)
		.then(response => response.json())
		.then(data => dispatch(setSizes(data?.sizes)));
};
export const getDepartments = () => dispatch => {
	fetch(`${GET_DEPARTMENTS_WITHOUT_PAGINATION}`)
		.then(response => response.json())
		.then(data => dispatch(setDepartments(data.departments)))
		.catch(() => {});
};
export const getEmployees = () => dispatch => {
	const authTOKEN = {
		headers: {
			'Content-type': 'application/json',
			Authorization: localStorage.getItem('jwt_access_token')
		}
	};
	fetch(GET_EMPLOYEES_WITHOUT_PAGINATION, authTOKEN)
		.then(response => response.json())
		.then(data => dispatch(setEmployees(data.employees)))
		.catch(() => {});
};
export const getEmployeesReadyToPayment = () => dispatch => {
	const authTOKEN = {
		headers: {
			'Content-type': 'application/json',
			Authorization: localStorage.getItem('jwt_access_token')
		}
	};
	fetch(READY_TO_PAYMENT_SALARY_EMPOLOYEE_LIST, authTOKEN)
		.then(response => response.json())
		.then(data => dispatch(setEmployeesReadyToPayment(data)))
		.catch(() => {});
};
export const getPayheads = () => dispatch => {
	const authTOKEN = {
		headers: {
			'Content-type': 'application/json',
			Authorization: localStorage.getItem('jwt_access_token')
		}
	};
	fetch(GET_PYAHEADS_WITHOUT_PAGINATION, authTOKEN)
		.then(response => response.json())
		.then(data => dispatch(setPayheads(data.payheads)))
		.catch(() => {});
};
export const getTimetables = () => dispatch => {
	const authTOKEN = {
		headers: {
			'Content-type': 'application/json',
			Authorization: localStorage.getItem('jwt_access_token')
		}
	};
	fetch(GET_TIMETABLES_WITHOUT_PAGINATION, authTOKEN)
		.then(response => response.json())
		.then(data => dispatch(setTimetables(data.shift_timetables)))
		.catch(() => {});
};
export const getEmployeesThroughChat = () => dispatch => {
	const authTOKEN = {
		headers: {
			'Content-type': 'application/json',
			Authorization: localStorage.getItem('jwt_access_token')
		}
	};
	fetch(GET_All_EMPLOYEE_THROUGH_CHAT, authTOKEN)
		.then(response => response.json())
		.then(data => dispatch(setEmployeesThroughChat(data.employees)))
		.catch(() => {});
};

export const getShifts = () => dispatch => {
	const authTOKEN = {
		headers: {
			'Content-type': 'application/json',
			Authorization: localStorage.getItem('jwt_access_token')
		}
	};
	fetch(GET_SHIFTS_WITHOUT_PAGINATION, authTOKEN)
		.then(response => response.json())
		.then(data => dispatch(setShifts(data.shifts)))
		.catch(() => {});
};

export const getPermissions = () => dispatch => {
	fetch(GET_PERMISSIONS_WITHOUT_PAGINATION)
		.then(response => response.json())
		.then(data => dispatch(setPermissions(data.permissions)))
		.catch(() => {});
};

export const getUsers = () => dispatch => {
	const authTOKEN = {
		headers: {
			'Content-type': 'application/json',
			Authorization: localStorage.getItem('jwt_access_token')
		}
	};
	fetch(GET_USERS_WITHOUT_PAGINATION, authTOKEN)
		.then(response => response.json())
		.then(data => dispatch(setUsers(data.users)))
		.catch(() => {});
};

export const getParentMenus = () => dispatch => {
	const authTOKEN = {
		headers: {
			'Content-type': 'application/json',
			Authorization: localStorage.getItem('jwt_access_token')
		}
	};
	fetch(GET_MENUS_WITHOUT_PAGINATION, authTOKEN)
		.then(response => response.json())
		.then(data => {
			dispatch(setParentMenus(data.menu_items));
		})
		.catch(e => {});
};

export const getAllMenuNested = () => dispatch => {
	const authTOKEN = {
		headers: {
			'Content-type': 'application/json',
			Authorization: localStorage.getItem('jwt_access_token')
		}
	};
	axios
		.get(GET_MENUS_ALL_NESTED, authTOKEN)
		.then(res => {
			dispatch(setAllMenuNested(res.data?.menu_items));
		})
		.catch(() => {
			dispatch(setAllMenuNested([]));
		});
};

export const getGroups = () => dispatch => {
	fetch(GROUPS_WITHOUT_PAGINATION)
		.then(response => response.json())
		.then(data => dispatch(setGroups(data.groups)))
		.catch(() => {});
};
export const getAttendanceTypes = () => dispatch => {
	fetch(GET_ATTENDANCE_TYPES_WITHOUT_PAGINATION_W_PRODUCTION)
		.then(response => response.json())
		.then(data => dispatch(setAttendanceTypes(data)))
		.catch(() => {});
};
export const getAttendanceProductionTypes = () => dispatch => {
	fetch(GET_ATTENDANCE_PRODUCTION_TYPES_WITHOUT_PAGINATION)
		.then(response => response.json())
		.then(data => dispatch(setAttendanceProductionTypes(data.attendance_production_types)))
		.catch(() => {});
};
export const getPayheadTypes = () => dispatch => {
	fetch(GET_PAYHEAD_TYPES_WITHOUT_PAGINATION)
		.then(response => response.json())
		.then(data => dispatch(setPayheadTypes(data)))
		.catch(() => {});
};
export const getCalculationTypes = () => dispatch => {
	fetch(GET_CALCULATION_TYPES_WITHOUT_PAGINATION)
		.then(response => response.json())
		.then(data => dispatch(setCalculationTypes(data.calculation_types)))
		.catch(() => {});
};
export const getCompute = () => dispatch => {
	fetch(GET_COMPUTES_WITHOUT_PAGINATION)
		.then(response => response.json())
		.then(data => dispatch(setCompute(data.computes)))
		.catch(() => {});
};

export const getPrimaryGroups = () => dispatch => {
	fetch(PRIMARY_GROUPS_WITHOUT_PAGINATION)
		.then(response => response.json())
		.then(data => dispatch(setPrimaryGroups(data.primary_groups)))
		.catch(() => {});
};

export const getDesignations = () => dispatch => {
	const authTOKEN = {
		headers: {
			'Content-type': 'application/json',
			Authorization: localStorage.getItem('jwt_access_token')
		}
	};
	fetch(DESIGNATIONS_WITHOUT_PAGINATION, authTOKEN)
		.then(response => response.json())
		.then(data => {
			dispatch(setDesignations(data.designations));
		})
		.catch(() => {});
};

export const getPayrollVoucherClass = () => dispatch => {
	fetch(GET_VOUCHER_TYPE_CLASSS_WITHOUT_PAGINATION)
		.then(response => response.json())
		.then(data => dispatch(setPayrollVoucherClass(data.voucher_type_classes)))
		.catch(() => {});
};
export const getLedgerAsPayhead = () => dispatch => {
	fetch(GET_VOUCHER_TYPE_CLASS_LEDGER_ALL)
		.then(response => response.json())
		.then(data => dispatch(setLedgersAsPayhead(data.ledger_account_list)))
		.catch(() => {});
};
export const getLedgers = () => dispatch => {
	fetch(LEDGERS_WITHOUT_PAGINATION)
		.then(response => response.json())
		.then(data => dispatch(setLedgers(data.ledger_accounts)))
		.catch(() => {});
};
export const getLedgersCashAndBank = () => dispatch => {
	const authTOKEN = {
		headers: {
			'Content-type': 'application/json',
			Authorization: localStorage.getItem('jwt_access_token')
		}
	};
	fetch(GET_LEDGER_ACCOUNT_CASH_AND_BANK, authTOKEN)
		.then(response => response.json())
		.then(data => dispatch(setLedgersCashAndBank(data.ledger_accounts)))
		.catch(() => {});
};

export const getSubLedgers = () => dispatch => {
	fetch(SUBLEDGERS_WITHOUT_PAGINATION)
		.then(response => response.json())
		.then(data => dispatch(setSubLedgers(data?.sub_ledger_accounts)))
		.catch(() => {});
};

export const getBrand = () => dispatch => {
	fetch(GET_BRANDS_WITHOUT_PAGINATION)
		.then(response => response.json())
		.then(data => dispatch(setBrands(data?.brands)));
};

export const getOrdersStatus = () => dispatch => {
	const authTOKEN = {
		headers: {
			'Content-type': 'application/json',
			Authorization: localStorage.getItem('jwt_access_token')
		}
	};
	fetch(ORDERSTATUS, authTOKEN)
		.then(response => response.json())
		.then(data => dispatch(setOrdersStatus(data?.orderstatuses)));
};
export const getDeviceAll = () => dispatch => {
	const authTOKEN = {
		headers: {
			'Content-type': 'application/json',
			Authorization: localStorage.getItem('jwt_access_token')
		}
	};
	fetch(GET_DEVICE_IPS_WITHOUT_PAGINATION, authTOKEN)
		.then(response => response.json())
		.then(data => dispatch(setDeviceAll(data?.device_ips)));
};

export const getCusotmerDetails = () => dispatch => {
	fetch(GET_CUSTOMER_WITHOUT_PAGINATION)
		.then(response => response.json())
		.then(data => dispatch(setCusotmerDetails(data?.customers)));
};

export const getCategory = () => dispatch => {
	fetch(GET_PERENT_CATEGORIES)
		.then(response => response.json())
		.then(data => {
			dispatch(setCategories(data?.parent_categories));
		});
};
export const getSubCategory = id => dispatch => {
	fetch(`${GET_SUB_CATEGORIES_BY_ID}${id}`)
		.then(response => response.json())
		.then(data => {
			dispatch(setSubCategories(data?.sub_categories));
		});
};

export const getSubSubCategory = id => dispatch => {
	fetch(`${GET_SUB_SUB_CATEGORIES_BY_ID}${id}`)
		.then(response => response.json())
		.then(data => {
			dispatch(setSubSubCategories(data?.sub_sub_categories));
		});
};

export const getPurchaseRequestStatus = () => dispatch => {
	const authTOKEN = {
		headers: {
			'Content-type': 'application/json',
			Authorization: localStorage.getItem('jwt_access_token')
		}
	};
	fetch(GET_PURCHASESTATUS, authTOKEN)
		.then(response => response.json())
		.then(data => dispatch(setPurchaseStatus(data?.purchase_statuses)));
};

export const getProducts = () => dispatch => {
	const authTOKEN = {
		headers: {
			'Content-type': 'application/json',
			Authorization: localStorage.getItem('jwt_access_token')
		}
	};
	fetch(GET_ALL_PRODUCT_WITHOUT_PG, authTOKEN)
		.then(response => response.json())
		.then(data => dispatch(setProducts(data?.products)));
};

export const getPaymentMathods = () => dispatch => {
	const authTOKEN = {
		headers: {
			'Content-type': 'application/json',
			Authorization: localStorage.getItem('jwt_access_token')
		}
	};
	fetch(PAYMENT_MATHODS, authTOKEN)
		.then(response => response.json())
		.then(data => dispatch(setPaymentMathods(data?.paymentmethods)));
};

export const getVendors = () => dispatch => {
	const authTOKEN = {
		headers: {
			'Content-type': 'application/json',
			Authorization: localStorage.getItem('jwt_access_token')
		}
	};
	fetch(GET_CUSTOMER_WITHOUT_PAGINATION, authTOKEN)
		.then(response => response.json())
		.then(data => dispatch(setVendors(data?.customers)));
};

export const getCusotmerTypes = () => dispatch => {
	const authTOKEN = {
		headers: {
			'Content-type': 'application/json',
			Authorization: localStorage.getItem('jwt_access_token')
		}
	};
	fetch(CUSOTMERTYPES, authTOKEN)
		.then(response => response.json())
		.then(data => dispatch(setCusotmerTypes(data?.customer_types)));
};
export const getUnits = () => dispatch => {
	const authTOKEN = {
		headers: {
			'Content-type': 'application/json',
			Authorization: localStorage.getItem('jwt_access_token')
		}
	};
	fetch(GET_UNITS_WITHOUT_PAGINATION, authTOKEN)
		.then(response => response.json())
		.then(data => dispatch(setUnits(data?.units)));
};
export const getPayheadOnlyUserDefineValue = () => dispatch => {
	const authTOKEN = {
		headers: {
			'Content-type': 'application/json',
			Authorization: localStorage.getItem('jwt_access_token')
		}
	};
	fetch(GET_PAYHEAD_ONLY_USERDEFINEVALUES, authTOKEN)
		.then(response => response.json())
		.then(data => dispatch(setPayheadOnlyUserDefineValues(data?.user_defined_payheads)));
};

const dataSlice = createSlice({
	name: 'employeeManagement/data',
	initialState: {
		branches: [],
		genders: [],
		thanas: [],
		cities: [],
		districts: [],
		countries: [],
		roles: [],
		UserPermissions: [],
		departments: [],
		employees: [],
		payheads: [],
		timetables: [],
		employeesChat: [],
		employeesReadyToPayment: [],
		shift: [],
		citys: [],
		permissions: [],
		attributesets: [],
		users: [],
		parentMenus: [],
		nestedMenus: [],
		groups: [],
		devices: [],
		attendanceTypes: [],
		attendanceProductionTypes: [],
		payheadTypes: [],
		calculationTypes: [],
		computes: [],
		primaryGroups: [],
		designations: [],
		ledgers: [],
		ledgersCashAndBank: [],
		ledgersAsPayhead: [],
		payrollVoucherClass: [],
		subLedgers: [],
		brands: [],
		parent_categories: [],
		ordersStatus: [],
		cusotmerDetails: [],
		sub_categories: [],
		sub_sub_categories: [],
		purchaseStatus: [],
		products: [],
		paymentMathods: [],
		vendors: [],
		units: [],
		userDefinedValuePayhead: [],
		cusotmerTypes: [],
		colors: [],
		sizes: []
	},
	reducers: {
		setBranches: (state, action) => {
			state.branches = action.payload;
		},
		setGenders: (state, action) => {
			state.genders = action.payload;
		},
		setThanas: (state, action) => {
			state.thanas = action.payload;
		},
		setCities: (state, action) => {
			state.cities = action.payload;
		},
		setCountries: (state, action) => {
			state.countries = action.payload;
		},
		setRoles: (state, action) => {
			state.roles = action.payload;
		},
		setUserPermissions: (state, action) => {
			state.UserPermissions = action.payload;
		},
		setDepartments: (state, action) => {
			state.departments = action.payload;
		},
		setEmployees: (state, action) => {
			state.employees = action.payload ? action.payload : [];
		},
		setEmployeesReadyToPayment: (state, action) => {
			state.employeesReadyToPayment = action.payload ? action.payload : [];
		},
		setPayheads: (state, action) => {
			state.payheads = action.payload ? action.payload : [];
		},
		setTimetables: (state, action) => {
			state.timetables = action.payload ? action.payload : [];
		},
		setEmployeesThroughChat: (state, action) => {
			state.employeesChat = action.payload ? action.payload : [];
		},

		// setTimetables: (state, action) => {
		// 	state.timetables = action.payload ? action.payload : [];
		// },
		setShifts: (state, action) => {
			state.shift = action.payload ? action.payload : [];
		},
		// setProducts: (state, action) => {
		// 	state.products = action.payload ? action.payload : [];
		// },
		setAttributes: (state, action) => {
			state.attributes = action.payload ? action.payload : [];
		},
		setCitys: (state, action) => {
			state.citys = action.payload ? action.payload : [];
		},
		setPermissions: (state, action) => {
			state.permissions = action.payload;
		},

		setUsers: (state, action) => {
			state.users = action.payload ? action.payload : [];
		},
		setVendors: (state, action) => {
			state.vendors = action.payload ? action.payload : [];
		},

		setParentMenus: (state, action) => {
			state.parentMenus = action.payload ? action.payload : [];
		},
		setAllMenuNested: (state, action) => {
			state.nestedMenus = action.payload ? action.payload : [];
		},
		setGroups: (state, action) => {
			state.groups = action.payload ? action.payload : [];
		},
		setAttendanceTypes: (state, action) => {
			state.attendanceTypes = action.payload ? action.payload : [];
		},
		setAttendanceProductionTypes: (state, action) => {
			state.attendanceProductionTypes = action.payload ? action.payload : [];
		},
		setPayheadTypes: (state, action) => {
			state.payheadTypes = action.payload ? action.payload : [];
		},
		setCalculationTypes: (state, action) => {
			state.calculationTypes = action.payload ? action.payload : [];
		},
		setCompute: (state, action) => {
			state.computes = action.payload ? action.payload : [];
		},
		setPrimaryGroups: (state, action) => {
			state.primaryGroups = action.payload ? action.payload : [];
		},
		setDesignations: (state, action) => {
			state.designations = action.payload ? action.payload : [];
		},

		setPayrollVoucherClass: (state, action) => {
			state.payrollVoucherClass = action.payload ? action.payload : [];
		},
		setLedgersAsPayhead: (state, action) => {
			state.ledgersAsPayhead = action.payload ? action.payload : [];
		},

		setLedgers: (state, action) => {
			state.ledgers = action.payload ? action.payload : [];
		},
		setLedgersCashAndBank: (state, action) => {
			state.ledgersCashAndBank = action.payload ? action.payload : [];
		},
		setSubLedgers: (state, action) => {
			state.subLedgers = action.payload ? action.payload : [];
		},
		setBrands: (state, action) => {
			state.brands = action.payload ? action.payload : [];
		},
		setCategories: (state, action) => {
			state.parent_categories = action.payload ? action.payload : [];
		},
		setOrdersStatus: (state, action) => {
			state.ordersStatus = action.payload ? action.payload : [];
		},
		setDeviceAll: (state, action) => {
			state.devices = action.payload ? action.payload : [];
		},
		setCusotmerDetails: (state, action) => {
			state.cusotmerDetails = action.payload ? action.payload : [];
		},
		setSubCategories: (state, action) => {
			state.sub_categories = action.payload ? action.payload : [];
		},
		setSubSubCategories: (state, action) => {
			state.sub_sub_categories = action.payload ? action.payload : [];
		},
		setPurchaseStatus: (state, action) => {
			state.purchaseStatus = action.payload ? action.payload : [];
		},
		setProducts: (state, action) => {
			state.products = action.payload ? action.payload : [];
		},
		setPaymentMathods: (state, action) => {
			state.paymentMathods = action.payload ? action.payload : [];
		},
		setCusotmerTypes: (state, action) => {
			state.cusotmerTypes = action.payload ? action.payload : [];
		},
		setUnits: (state, action) => {
			state.units = action.payload ? action.payload : [];
		},
		setPayheadOnlyUserDefineValues: (state, action) => {
			state.userDefinedValuePayhead = action.payload ? action.payload : [];
		},

		setColors: (state, action) => {
			state.colors = action.payload;
		},
		setSizes: (state, action) => {
			state.sizes = action.payload;
		}
	}
});

const {
	setBranches,
	setThanas,
	setRoles,
	setUserPermissions,
	setDepartments,
	setCities,
	setCountries,
	setPermissions,
	setEmployees,
	setEmployeesReadyToPayment,
	setPayheads,
	setTimetables,
	setEmployeesThroughChat,
	setShifts,

	// setCitys,
	setUsers,

	setParentMenus,
	setAllMenuNested,
	setGroups,
	setAttendanceTypes,
	setAttendanceProductionTypes,
	setPayheadTypes,
	setCalculationTypes,
	setCompute,
	setPrimaryGroups,
	setDesignations,
	setLedgersCashAndBank,
	setLedgers,
	setPayrollVoucherClass,
	setLedgersAsPayhead,
	setSubLedgers,
	setBrands,
	setCategories,
	setOrdersStatus,
	setDeviceAll,
	setCusotmerDetails,
	setSubCategories,
	setSubSubCategories,
	setPurchaseStatus,
	setProducts,
	setPaymentMathods,
	setVendors,
	setCusotmerTypes,
	setPayheadOnlyUserDefineValues,
	setUnits,
	setColors,
	setSizes
} = dataSlice.actions;
export default dataSlice.reducer;
